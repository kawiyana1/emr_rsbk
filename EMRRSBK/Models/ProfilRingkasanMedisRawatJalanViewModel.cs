﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class ProfilRingkasanMedisRawatJalanViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Diagnosa { get; set; }
        public string Temuan { get; set; }
        public string Evaluasi { get; set; }
        public string DPJP { get; set; }
        public string DPJPNama { get; set; }
        public string Username { get; set; }
    }
}