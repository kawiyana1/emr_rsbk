﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class EvaluasiPraOperasiViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string KodeOperator { get; set; }
        public string KodeOperatorNama { get; set; }
        public string KodeAnestesi { get; set; }
        public string KodeAnestesiNama { get; set; }
        public string SpesialisTerkait_1 { get; set; }
        public string SpesialisTerkait_1Nama { get; set; }
        public string SpesialisTerkait_2 { get; set; }
        public string SpesialisTerkait_2Nama { get; set; }
        public string KodePerawat { get; set; }
        public string KodePerawatNama { get; set; }
        public string Diagnosis_Utama { get; set; }
        public string Diagnosis_Komplikasi { get; set; }
        public string Diagnosis_Penyerta { get; set; }
        public string NamaOperasi_1 { get; set; }
        public string NamaOperasi_2 { get; set; }
        public string KIEKeluarga { get; set; }
        public string KIEKeluarga_Rek { get; set; }
        public string SiteMarking { get; set; }
        public string SiteMarking_Rek { get; set; }
        public string InformedConsent { get; set; }
        public string InformedConsent_Rek { get; set; }
        public string DarahSiap { get; set; }
        public string DarahSiap_Rek { get; set; }
        public string Antibiotika { get; set; }
        public string Antibiotika_Rek { get; set; }
        public string AlergiObat { get; set; }
        public string AlergiObat_Rek { get; set; }
        public string MandiBesar { get; set; }
        public string MandiBesar_Rek { get; set; }
        public string PemeriksaanPenunjang { get; set; }
        public string PemeriksaanPenunjang_Rek { get; set; }
        public string Username { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        public string NamaPasien { get; set; }
        public int? Umur { get; set; }
        public string Alamat { get; set; }
        public string JenisKelamin { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
        public string TandaTanganOperator { get; set; }
        public int SudahRegOperator { get; set; }
    }
}