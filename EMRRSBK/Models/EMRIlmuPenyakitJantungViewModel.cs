﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EMRRSBK.Models
{
    public class EMRIlmuPenyakitJantungViewModel
    {
        public ListDetail<AssesmentIlmuPenyakitJantungModelDetail> IPJntung_List { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public bool Rujukan_Ya { get; set; }
        public string Rujukan_Ya_Ket { get; set; }
        public string Rujukan_Ya_Dx { get; set; }
        public bool Rujukan_Tidak { get; set; }
        public string NamaKeluarga { get; set; }
        public string AlamatKeluarga { get; set; }
        public string TeleponKeluarga { get; set; }
        public string Transportasi { get; set; }
        public string Transportasi_Ket { get; set; }
        public string Nyeri { get; set; }
        public string Lokasi { get; set; }
        public string Intensitas { get; set; }
        public string Jenis { get; set; }
        public string Riwayat_Alergi { get; set; }
        public string Riwayat_Alergi_Ket { get; set; }
        public string KeluhanUtama { get; set; }
        public string RiwayatPenyakitSekarang { get; set; }
        public bool RiwayatPenyakitDahulu_Hepertensi { get; set; }
        public bool RiwayatPenyakitDahulu_DM { get; set; }
        public bool RiwayatPenyakitDahulu_Jantung { get; set; }
        public bool RiwayatPenyakitDahulu_Asthma { get; set; }
        public bool RiwayatPenyakitDahulu_Stroke { get; set; }
        public bool RiwayatPenyakitDahulu_Liver { get; set; }
        public bool RiwayatPenyakitDahulu_Ginjal { get; set; }
        public bool RiwayatPenyakitDahulu_TBC { get; set; }
        public bool RiwayatPenyakitDahulu_Lainnya { get; set; }
        public string RiwayatPenyakitDahulu_Lainnya_Ket { get; set; }
        public bool RiwayatPenyakitKeluarga_Hepertensi { get; set; }
        public bool RiwayatPenyakitKeluarga_DM { get; set; }
        public bool RiwayatPenyakitKeluarga_Jantung { get; set; }
        public bool RiwayatPenyakitKeluarga_Asthma { get; set; }
        public bool RiwayatPenyakitKeluarga_Lainnya { get; set; }
        public string RiwayatPenyakitKeluarga_Lainnya_Ket { get; set; }
        public bool RiwayatSosial_Merokok { get; set; }
        public bool RiwayatSosial_Alkohol { get; set; }
        public bool RiwayatSosial_Lainnya { get; set; }
        public string RiwayatSosial_Lainnya_Ket { get; set; }
        public string KeadaanUmum { get; set; }
        public string Gizi { get; set; }
        public string GCS_E { get; set; }
        public string GCS_M { get; set; }
        public string GCS_V { get; set; }
        public string Resusitas { get; set; }
        public string BB { get; set; }
        public string TB { get; set; }
        public string Tensi { get; set; }
        public string SuhuAxila { get; set; }
        public string SuhuRectal { get; set; }
        public string Nadi { get; set; }
        public string Respirasi { get; set; }
        public string Saturasi { get; set; }
        public bool Pada_SuhuRuangan { get; set; }
        public bool Pada_Nasal { get; set; }
        public bool Pada_NRB { get; set; }
        public string Conjunctiva { get; set; }
        public string Scklera { get; set; }
        public string Cyanosis { get; set; }
        public string OdemaPalpebrae { get; set; }
        public string Pupil { get; set; }
        public bool Leher_JVP { get; set; }
        public string Leher_JVP_Ket { get; set; }
        public bool Leher_Pembesaran { get; set; }
        public string Leher_Pembesaran_Ket { get; set; }
        public bool Leher_Thyroid { get; set; }
        public string Leher_Thyroid_Ket { get; set; }
        public bool Thorax { get; set; }
        public string Thorax_Ket { get; set; }
        public string InspeksiIktus { get; set; }
        public string InspeksiIktus_Ket { get; set; }
        public string InspeksiIktus_Lokasi { get; set; }
        public bool InspeksiPulsasi_Apex { get; set; }
        public bool InspeksiPulsasi_Prekordium { get; set; }
        public bool InspeksiPulsasi_Epigastrium { get; set; }
        public bool InspeksiPulsasi_Lainnya { get; set; }
        public string InspeksiPulsasi_Lainnya_Ket { get; set; }
        public bool PalpasiIktus_Normal { get; set; }
        public bool PalpasiIktus_Kuat { get; set; }
        public bool PalpasiIktus_Meluas { get; set; }
        public string PalpasiIktus_Lokasi { get; set; }
        public string PalpasiThrill { get; set; }
        public string PalpasiThrill_Lokasi { get; set; }
        public string PerkusiAtas { get; set; }
        public string PerkusiBawah { get; set; }
        public string PerkusiKanan { get; set; }
        public string PerkusiKiri { get; set; }
        public string S1 { get; set; }
        public string S2 { get; set; }
        public bool Reguler { get; set; }
        public bool Ireguler { get; set; }
        public string ExtraSystole { get; set; }
        public string Gallop { get; set; }
        public bool Fase_Sistolik { get; set; }
        public bool Fase_Diastolik { get; set; }
        public bool Fase_Lainnya { get; set; }
        public string Fase_Lainnya_Ket { get; set; }
        public bool Lokasi_Apex { get; set; }
        public bool Lokasi_ICSIIKiri { get; set; }
        public bool Lokasi_ICSIIKanan { get; set; }
        public bool Lokasi_ICSIVKiri { get; set; }
        public bool Lokasi_Lainnya { get; set; }
        public string Lokasi_Lainnya_Ket { get; set; }
        public bool Kualitas_Rumbling { get; set; }
        public bool Kualitas_Blowing { get; set; }
        public bool Kualitas_Ejection { get; set; }
        public bool Kualitas_Lainnya { get; set; }
        public string Kualitas_Lainnya_Ket { get; set; }
        public bool Grade_I { get; set; }
        public bool Grade_II { get; set; }
        public bool Grade_III { get; set; }
        public bool Grade_IV { get; set; }
        public bool Grade_V { get; set; }
        public bool Grade_VI { get; set; }
        public bool Penjalaran_Axilla { get; set; }
        public bool Penjalaran_Punggung { get; set; }
        public bool Penjalaran_Skitarnya { get; set; }
        public bool Penjalaran_Lainnya { get; set; }
        public string Penjalaran_Lainnya_Ket { get; set; }
        public string OpeningSnap { get; set; }
        public string FrictionRub { get; set; }
        public string SuaraNafas { get; set; }
        public bool Ronchi { get; set; }
        public string Ronchi_Ket { get; set; }
        public bool Wheezing { get; set; }
        public string Wheezing_Ket { get; set; }
        public string Hepar { get; set; }
        public string Hepar_Ket { get; set; }
        public string Lien { get; set; }
        public string Lien_Ket { get; set; }
        public bool Ascites_Negatif { get; set; }
        public bool Ascites_Minimal { get; set; }
        public bool Ascites_Permagna { get; set; }
        public string Extrimitas { get; set; }
        public string Odema { get; set; }
        public string Odema_Lokasi { get; set; }
        public string JariTumbuh { get; set; }
        public string Plegia { get; set; }
        public string ECG { get; set; }
        public string ThoraxFoto { get; set; }
        public string Echocardiografi { get; set; }
        public string Laboratorium { get; set; }
        public string Etiologis { get; set; }
        public string Anatomis { get; set; }
        public string Fungsional { get; set; }
        public string Komplikasi { get; set; }
        public string Diagnosa { get; set; }
        public string Diagnosa_Utama { get; set; }
        public string Terapi { get; set; }
        public string RencanaKerja { get; set; }
        public bool Disposisi_Poliklinik { get; set; }
        public string KontrolPoliklinik_Ket { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> KontrolPoliklinik_Tanggal { get; set; }
        public bool Disposisi_Dirawat { get; set; }
        public bool Disposisi_Intensif { get; set; }
        public bool Disposisi_MS { get; set; }
        public bool Disposisi_Lainnya { get; set; }
        public string Disposisi_Lainnya_Ket { get; set; }
        public string DokterBPJP { get; set; }
        public string DokterBPJPNama { get; set; }
        public bool Disposisi_Pulang { get; set; }
        public string Username { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        public string TandaTanganDPJP { get; set; }
        public string PemeriksaanFisik_Kepala { get; set; }
        public string PemeriksaanFisik_Kepala_Ket { get; set; }
        public string PemeriksaanFisik_Mata { get; set; }
        public string PemeriksaanFisik_Mata_Ket { get; set; }
        public string PemeriksaanFisik_THT { get; set; }
        public string PemeriksaanFisik_THT_Ket { get; set; }
        public string PemeriksaanFisik_Leher { get; set; }
        public string PemeriksaanFisik_Leher_Ket { get; set; }
        public string PemeriksaanFisik_Dada { get; set; }
        public string PemeriksaanFisik_Dada_Ket { get; set; }
        public string PemeriksaanFisik_Jantung { get; set; }
        public string PemeriksaanFisik_Jantung_Ket { get; set; }
        public string PemeriksaanFisik_Paru { get; set; }
        public string PemeriksaanFisik_Paru_Ket { get; set; }
        public string PemeriksaanFisik_Perut { get; set; }
        public string PemeriksaanFisik_Perut_Ket { get; set; }
        public string PemeriksaanFisik_Heper { get; set; }
        public string PemeriksaanFisik_Heper_Ket { get; set; }
        public string PemeriksaanFisik_Lien { get; set; }
        public string PemeriksaanFisik_Lien_Ket { get; set; }
        public string PemeriksaanFisik_Genital { get; set; }
        public string PemeriksaanFisik_Genital_Ket { get; set; }
        public string PemeriksaanFisik_Ekstremitas { get; set; }
        public string PemeriksaanFisik_Ekstremitas_Ket { get; set; }
        public string PemeriksaanFisik_RectalToucher { get; set; }
        public string PemeriksaanFisik_RectalToucher_Ket { get; set; }

        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public List<SelectItemListPenyakitJantung> ListTemplate { get; set; }
        // disable all form
        public int MODEVIEW { get; set; } 
    }

    public class SelectItemListPenyakitJantung
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}