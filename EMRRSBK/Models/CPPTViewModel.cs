﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class CPPTViewModel
    {
        public string _METHOD { get; set; }
        public string NoBukti { get; set; }
        public string NoReg { get; set; }
        public string NRM { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Jam_View { get; set; }
        public string Profesi { get; set; }
        public string CPPT_S { get; set; }
        public string CPPT_O { get; set; }
        public string CPPT_A { get; set; }
        public string CPPT_P { get; set; }
        public string CPPT_I { get; set; }
        public string DPJP { get; set; }
        public string DPJPNama { get; set; }
        public string Edit { get; set; }
        public bool saveTemplate { get; set; }
        public string templateName { get; set; }
        public string CPPT_OGambar { get; set; }
        public string TandaTangan { get; set; }
        public string TTDDPJP { get; set; }
        public string TTDCPPT { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalBacaUlang { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalInput { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalKonfirmasi { get; set; }


    }
}