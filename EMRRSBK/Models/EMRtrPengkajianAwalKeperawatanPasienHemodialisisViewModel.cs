﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class EMRtrPengkajianAwalKeperawatanPasienHemodialisisViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public bool JenisPasien_RawatJalan { get; set; }
        public bool JenisPasien_Traveling { get; set; }
        public bool JenisPasien_PostMRS { get; set; }
        public bool JenisPasien_Rujukan { get; set; }
        public string JenisPasien_RujukanKet { get; set; }
        public bool StatusPsikologi_Emosi { get; set; }
        public bool StatusPsikologi_HargaDiri { get; set; }
        public bool StatusPsikologi_Gangguan { get; set; }
        public string StatusPernikahan { get; set; }
        public string Warganegara { get; set; }
        public string WarganegaraKet { get; set; }
        public bool TinggalBersama_Anak { get; set; }
        public bool TinggalBersama_OrangTua { get; set; }
        public bool TinggalBersama_Sendiri { get; set; }
        public bool TinggalBersama_SuamiIstri { get; set; }
        public bool Kebiasaan_Merokok { get; set; }
        public bool Kebiasaan_Alkohol { get; set; }
        public bool Kebiasaan_Lainnya { get; set; }
        public string Kebiasaan_LainnyaKet { get; set; }
        public string Agama { get; set; }
        public string BahasaIbu { get; set; }
        public string Bicara { get; set; }
        public string BicaraKet { get; set; }
        public string BahasaSehariHari { get; set; }
        public string PerluPenterjemah { get; set; }
        public bool Hambatan_Bahasa { get; set; }
        public bool Hambatan_Pendengaran { get; set; }
        public bool Hambatan_HilangMemori { get; set; }
        public bool Hambatan_MotivasiBuruk { get; set; }
        public bool Hambatan_MasalahPenglihatan { get; set; }
        public bool Hambatan_TidakDitemukanHambatan { get; set; }
        public bool Hambatan_Cemas { get; set; }
        public bool Hambatan_Emosi { get; set; }
        public bool Hambatan_KesulitanBicara { get; set; }
        public bool Hambatan_TidakAdaPartisipasi { get; set; }
        public bool Hambatan_SecaraFisiologi { get; set; }
        public bool CaraEdukasi_Menulis { get; set; }
        public bool CaraEdukasi_Audio { get; set; }
        public bool CaraEdukasi_Diskusi { get; set; }
        public bool CaraEdukasi_Membaca { get; set; }
        public bool CaraEdukasi_Mendengar { get; set; }
        public bool CaraEdukasi_Demonstrasi { get; set; }
        public bool KebutuhanEdukasi_ProsesPenyakit { get; set; }
        public bool KebutuhanEdukasi_Pengobatan { get; set; }
        public bool KebutuhanEdukasi_Nutrisi { get; set; }
        public bool KebutuhanEdukasi_LainLain { get; set; }
        public string KebutuhanEdukasi_LainLainKet { get; set; }
        public string DietSaatIni { get; set; }
        public string PenurunanAtauKenaikanBeratBadan { get; set; }
        public string BeratBadan { get; set; }
        public string TinggiBadan { get; set; }
        public string PerhatikanCaraBerjalan { get; set; }
        public string ApakahPasienMemegang { get; set; }
        public bool TidakBerisiko { get; set; }
        public bool RisikoRendah { get; set; }
        public bool RisikoTinggi { get; set; }
        public string EvaluasiAksesDialysis { get; set; }
        public bool StatusAksesPasienHemodialisis { get; set; }
        public bool Akses_AvShunt { get; set; }
        public bool Akses_Graft { get; set; }
        public bool Akses_Tunnel { get; set; }
        public bool Akses_Temporer { get; set; }
        public bool Akses_Femoral { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalPembuatanAvShunt1 { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalPembuatanAvShunt2 { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalPembuatanAvShunt3 { get; set; }
        public string KondisiAksesVaskuler { get; set; }
        public string KomplikasiAksesVaskuler { get; set; }
        public string KomplikasiAksesVaskuler_Jenis { get; set; }
        public bool StatusAksesPasienCAPD { get; set; }
        public string JenisKateter { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalPemasangan { get; set; }
        public string KondisiKateter { get; set; }
        public string KondisiExitSite { get; set; }
        public string EvaluasiDialisis { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalMulaiHd { get; set; }
        public string FrekuensiHd { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> LamaWaktu { get; set; }
        public string DialiserYangDigunakan { get; set; }
        public string LuasMembrane { get; set; }
        public bool LowFlux { get; set; }
        public bool HighFlux { get; set; }
        public bool Jenis_Baru { get; set; }
        public bool Jenis_Reuse { get; set; }
        public string Jenis_Reuse_Ket { get; set; }
        public string VolumePriming { get; set; }
        public string HasilTesRenalin { get; set; }
        public string HasilTesRenalin_Petugas1 { get; set; }
        public string HasilTesRenalin_Petugas1Nama { get; set; }
        public string HasilTesRenalin_Petugas2 { get; set; }
        public string HasilTesRenalin_Petugas2Nama { get; set; }
        public bool AlarmTest_OK { get; set; }
        public bool AlarmTest_Tidak { get; set; }
        public string Conductivity { get; set; }
        public string SuhuMesin { get; set; }
        public bool Anticoagulan_Kontinyu { get; set; }
        public bool Anticoagulan_FreeHeparin { get; set; }
        public string DosisAwal { get; set; }
        public string DosisPemeliharaan { get; set; }
        public string Total { get; set; }
        public bool Keluhan_Kram { get; set; }
        public bool Keluhan_Dizziness { get; set; }
        public bool Keluhan_TidakAdaKeluhan { get; set; }
        public bool Keluhan_MualMuntah { get; set; }
        public bool Keluhan_Hipoksemia { get; set; }
        public bool Keluhan_Lainnya { get; set; }
        public string Keluhan_LainnyaKet { get; set; }
        public bool Keluhan_Hipertensi { get; set; }
        public bool Keluhan_Komplikasi { get; set; }
        public bool Keluhan_Hipotensi { get; set; }
        public string UrrTerakhir { get; set; }
        public string Adekuasi_UrrTerakhirKet { get; set; }
        public string Adekuasi_Kt { get; set; }
        public string TotalCairanKeluar { get; set; }
        public string FrekuensiPergantian { get; set; }
        public string KonsentrasiCairan { get; set; }
        public string AdekuasiDialisis { get; set; }
        public string HasilPet { get; set; }
        public string PemakainanEritropein { get; set; }
        public string PemakainanEritropein_Sejak { get; set; }
        public string PemakainanEritropein_Dosis { get; set; }
        public string PemakaianZatBesi { get; set; }
        public string PemakaianZatBesi_Sejak { get; set; }
        public string PemakaianZatBesi_Jenis { get; set; }
        public string RiwayatTranfusi { get; set; }
        public string RiwayatTranfusi_Pertama { get; set; }
        public string RiwayatTranfusi_Terakhir { get; set; }
        public string ProduksiUrine { get; set; }
        public string Warna { get; set; }
        public string KeluhanUtama { get; set; }
        public string RiwayatPenyakitSekarang { get; set; }
        public string RiwayatPenyakitDahulu { get; set; }
        public string IndikasiHemodialisis { get; set; }
        public string Hemodialisis { get; set; }
        public string SkriningPasien { get; set; }
        public string PemakainanEritropein_Jenis { get; set; }
        public string RiwayatPengobatan { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
        
    }
}