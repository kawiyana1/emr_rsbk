﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class SuratJawabanKonsulViewModel
    {
        public int No { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public string TS_Dr_Konsul { get; set; }
        public string TS_Dr_KonsulNama { get; set; }
        public string Nama_Pasien_Konsul { get; set; }
        public string TS_Dr_JawabanKonsul { get; set; }
        public string TS_Dr_JawabanKonsulNama { get; set; }
        public string Diagnosa_JawabanKonsul { get; set; }
        public string Tindakan_JawabanKonsul { get; set; }
        public string TTD_Konsultasi { get; set; }
        public string TTD_Jawaban { get; set; }
        public string Petugas { get; set; }
        public string PetugasNama { get; set; }
        public string TTD_Petugas { get; set; }
        public string _METHOD { get; set; }

        public string Petugas_Jawaban { get; set; }
        public string Petugas_JawabanNama { get; set; }
        public int Report { get; set; }
    }
}