﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class SuratKeteranganRujukanAmbulanViewModel
    {

        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string NamaPasien { get; set; }
        public string NoRMPasien { get; set; }
        public string Diagnosa { get; set; }
        public string Penanganan { get; set; }
        public string DirujukDi { get; set; }
        public string Dari { get; set; }
        public string Alasan { get; set; }
        public string PenerimaRujukan { get; set; }
        public string Rumkit { get; set; }
        public string KmAwal { get; set; }
        public string KmSetelahRujukan { get; set; }
        public string Username { get; set; }
    }
}