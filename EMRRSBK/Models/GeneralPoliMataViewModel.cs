﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class GeneralPoliMataViewModel
    { 
        public ListDetail<CatatanEdukasiTerintegrasiLanjutanRIRJViewModel> CETLRIRJ_List { get; set; }
        public ListDetail<KunjunganPoliklinikViewModel> KnjngPoli_List { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        public int Nama_tamplet { get; set; }
        public string Dokter { get; set; }
        public string DokterNama { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal_View { get; set; }

        

    }
}