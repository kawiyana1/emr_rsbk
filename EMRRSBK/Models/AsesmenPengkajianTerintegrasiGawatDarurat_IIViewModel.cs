﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class AsesmenPengkajianTerintegrasiGawatDarurat_IIViewModel
    {
        public ListDetail<TindakanKeperawatanViewModelDetail> TKepewaratan_List { get; set; }
        public ListDetail<DiagnosaViewModelDetail> Diagnosa_List { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public bool Diagnosa_KetidakefektifanBersihan { get; set; }
        public bool Diagnosa_Risiko { get; set; }
        public bool Diagnosa_KetidakefektifanPola { get; set; }
        public bool Diagnosa_GangguanPertukaran { get; set; }
        public bool Diagnosa_PenurunanCurahJantung { get; set; }
        public bool Diagnosa_KetidakefektifanPerfusi { get; set; }
        public bool Diagnosa_KekuranganVolumeCairan { get; set; }
        public bool Diagnosa_KelebihanVolumenCairan { get; set; }
        public bool Diagnosa_RisikoKekurangan { get; set; }
        public bool Diagnosa_Diare { get; set; }
        public bool Diagnosa_RetensisUrin { get; set; }
        public bool Diagnosa_NyeriAkut { get; set; }
        public bool Diagnosa_Hipertermia { get; set; }
        public bool Diagnosa_KerusakanMobilitas { get; set; }
        public bool Diagnosa_RisikoInfeksi { get; set; }
        public bool Diagnosa_Konstipasi { get; set; }
        public bool Rencana_LakukanManuever { get; set; }
        public bool Rencana_KeluarkanBendaAsing { get; set; }
        public bool Rencana_PasangOPA { get; set; }
        public bool Rencana_BerikanBantuan { get; set; }
        public bool Rencana_BerikanO2 { get; set; }
        public bool Rencana_MonitorSaO2 { get; set; }
        public bool Rencana_MonitorTandaVital { get; set; }
        public bool Rencana_MonitorTingkatKesadaran { get; set; }
        public bool Rencana_MonitorEKG { get; set; }
        public bool Rencana_PasangInfus { get; set; }
        public bool Rencana_HentikanPendarahan { get; set; }
        public bool Rencana_BerikanPosisiSemiflower { get; set; }
        public bool Rencana_PasangDowerCateter { get; set; }
        public bool Rencana_BerikanCairanIntravena { get; set; }
        public bool Rencana_KajiTurgorKulit { get; set; }
        public bool Rencana_AwasiTetesanCairan { get; set; }
        public bool Rencana_PasangNGT { get; set; }
        public bool Rencana_AtasiNyeri { get; set; }
        public bool Rencana_LakukanPerawatanLuka { get; set; }
        public bool Rencana_BerikanKompresHangat { get; set; }
        public bool Rencana_BerikanPosisiSemiflowerBila { get; set; }
        public bool Rencana_Delegatif { get; set; }
        public bool Rencana_MonitorIntake { get; set; }
        public bool Rencana_PasangPengaman { get; set; }
        public bool Rencana_KajiTanda { get; set; }
        public string Username { get; set; }


        public int Report { get; set; }
        public int Nomor { get; set; }
        public int SudahRegPasien { get; set; }
        public int SudahRegPegawai { get; set; }
        public int SudahRegDokter { get; set; }
        public string TandaTangan { get; set; }
        public string TandaTanganPetugas { get; set; }
        public string TandaTanganDokterPengkaji { get; set; }
        public string Template { get; set; }
        public bool save_template { get; set; }
        public string nama_template { get; set; }
        public string DokumenID { get; set; }
        public string KategoriKasus { get; set; }
    }
}