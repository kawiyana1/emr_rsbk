﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class EMRFormulirKreteriaMasukICUViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string Username { get; set; }
        public string VitalSign_a { get; set; }
        public string VitalSign_b { get; set; }
        public string VitalSign_c { get; set; }
        public string VitalSign_d { get; set; }
        public string NilaiLab_a { get; set; }
        public string NilaiLab_b { get; set; }
        public string NilaiLab_c { get; set; }
        public string NilaiLab_d { get; set; }
        public string NilaiLab_e { get; set; }
        public string NilaiLab_f { get; set; }
        public string NilaiLab_g { get; set; }
        public string HasilRad_a { get; set; }
        public string HasilRad_b { get; set; }
        public string HasilRad_c { get; set; }
        public string ECG_a { get; set; }
        public string ECG_b { get; set; }
        public string ECG_c { get; set; }
        public string PemeriksaanFisik_a { get; set; }
        public string PemeriksaanFisik_b { get; set; }
        public string PemeriksaanFisik_c { get; set; }
        public string PemeriksaanFisik_d { get; set; }
        public string PemeriksaanFisik_e { get; set; }
        public string PemeriksaanFisik_f { get; set; }
        public string PemeriksaanFisik_g { get; set; }
        public string PemeriksaanFisik_h { get; set; }
        public string MasukICU { get; set; }
        public string AlatTransport { get; set; }
        public string Pendamping { get; set; }
        public string AlatMedis { get; set; }
        public string AlatMedisKet { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string DPJP { get; set; }
        public string DPJPNama { get; set; }

        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public List<SelectItemListFormICU> ListTemplate { get; set; }
        public int MODEVIEW { get; set; }
        
    }

    public class SelectItemListFormICU
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}