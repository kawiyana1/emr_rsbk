﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class ResumeMedisBPJSViewModel
    {

        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Unit { get; set; }
        public string NoBpjs { get; set; }
        public string Anamnesa { get; set; }
        public string Diagnosa { get; set; }
        public string Kode { get; set; }
        public string HasilPemeriksaan { get; set; }
        public string Konsultasi { get; set; }
        public string Tindakan { get; set; }
        public string Obat { get; set; }
        public string ObatNama { get; set; }
        public string Adm { get; set; }
        public string Total { get; set; }
        public string Pasien { get; set; }
        public string DokterPemeriksa { get; set; }
        public string DokterPemeriksaNama { get; set; }
        public string NamaDataPasien { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahirDataPasien { get; set; }
        public string AlamatDataPasien { get; set; }
        public string NRMDataPasien { get; set; }
        public int SudahRegDokterPemeriksa { get; set; }
        public int SudahRegPasien { get; set; }
        public string TandaTanganDokterPemeriksa { get; set; }
        public string TandaTangan { get; set; }
    }
}