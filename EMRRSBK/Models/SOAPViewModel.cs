﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class SOAPViewModel
    {
        public int No { get; set; }

        [Required]
        [Display(Name = "Tanggal")]
        [DataType(DataType.Date)]
        public DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string Jam_View { get; set; }
        public string T_TanggalView { get; set; }
        public string B_TanggalView { get; set; }
        public string K_TanggalView { get; set; }

        [Display(Name = "Tanggal Input")]
        public DateTime TanggalInput { get; set; }
        public string TanggalInput_View { get; set; }

        [Display(Name = "Nama Section")]
        public string SectionName { get; set; }

        [Display(Name = "Profesi")]
        public string DokterID { get; set; }

        [Display(Name = "Dokter/Perawat")]
        public string SOAPDokterID { get; set; }

        [Display(Name = "Nama Dokter/Perawat")]
        public string NamaDOkter { get; set; }

        [Display(Name = "Subject")]
        public string Soap_S { get; set; }

        [Display(Name = "Objective")]
        public string Soap_O { get; set; }

        [Display(Name = "Assessment")]
        public string Soap_A { get; set; }

        [Display(Name = "Planning")]
        public string Soap_P { get; set; }

        public string NoReg { get; set; }
        public string NRM { get; set; }
        public string SectionID { get; set; }
        public int Nomor { get; set; }
        public string DPJP { get; set; }
        public string DPJPNama { get; set; }
        public string Instruksi { get; set; }
        public string MenyerahkanPasien { get; set; }
        public string MenyerahkanPasienNama { get; set; }
        public string MenerimaPasien { get; set; }
        public string MenerimaPasienNama { get; set; }
        public string PetugasBacaUlang { get; set; }
        public string PetugasBacaUlangNama { get; set; }
        public string PetugasKonfirmasi { get; set; }
        public string PetugasKonfirmasiNama { get; set; }

        public Nullable<System.DateTime> TanggalBacaUlang { get; set; }
        public Nullable<System.DateTime> TanggalKonfirmasi { get; set; }
        public Nullable<System.DateTime> JamBacaUlang { get; set; }
        public Nullable<System.DateTime> JamKonfirmasi { get; set; }
        public string TanggalBacaUlang_View { get; set; }
        public string JamBacaUlang_View { get; set; }
        public string TanggalKonfirmasi_View { get; set; }
        public string JamKonfirmasi_View { get; set; }
        public bool ValidasiDokter { get; set; }
        public bool ValidasiDPJP { get; set; }
        public string Username { get; set; }
        public string NamaProfesi { get; set; }
        public string Tipe { get; set; }
        public string Bebas { get; set; }
        [Required]
        [DataType(DataType.Time)]
        public System.TimeSpan Jam { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.TimeSpan> T_Jam { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> T_Tanggal { get; set; }
        public string T_Inisial { get; set; }
        public string T_InisialNama { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.TimeSpan> B_Jam { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> B_Tanggal { get; set; }
        public string B_Inisial { get; set; }
        public string B_InisialNama { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.TimeSpan> K_Jam { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> K_Tanggal { get; set; }
        public string K_Inisial { get; set; }
        public string K_InisialNama { get; set; }

        [Display(Name = "Assesment")]
        public string Adime_A { get; set; }

        [Display(Name = "Diagnosis gizi")]
        public string Adime_D { get; set; }

        [Display(Name = "Intervensi")]
        public string Adime_I { get; set; }

        [Display(Name = "Monotoring dan Evaluasi")]
        public string Adime_ME { get; set; }

        [Display(Name = "Situation")]
        public string Sbar_S { get; set; }

        [Display(Name = "Background")]
        public string Sbar_B { get; set; }

        [Display(Name = "Assesment")]
        public string Sbar_A { get; set; }

        [Display(Name = "Recommendation")]
        public string Sbar_R { get; set; }

        public Nullable<bool> ValidasiInisial_T { get; set; }
        public Nullable<bool> ValidasiInisial_B { get; set; }
        public Nullable<bool> ValidasiInisial_K { get; set; }

        public string Menyerahkan_Shift { get; set; }
        [Display(Name = "Nama petugas yang menyerahkan")]
        public string Menyerahkan_Petugas { get; set; }
        public string Menyerahkan_Petugas_Nama { get; set; }
        public Nullable<bool> Menyerahkan_Validasi { get; set; }
        public string Menerima_Shift { get; set; }
        [Display(Name = "Nama petugas yang menerima")]
        public string Menerima_Petugas { get; set; }
        public string Menerima_Petugas_Nama { get; set; }
        public Nullable<bool> Menerima_Validasi { get; set; }
        public string KaRU_Shift { get; set; }
        [Display(Name = "Nama Ka.RU atau Ka.Tim")]
        public string KaRU_Petugas { get; set; }
        public string KaRU_Petugas_Nama { get; set; }
        public string Profesi { get; set; }
        public int SudahRegDokter { get; set; }
        public string TandaTanganDPJP { get; set; }
        public string TandaTanganPetugasKonfirmasi { get; set; }
        public string TandaTanganPetugasBacaUlang { get; set; }
        public string TandaTanganMenerimaPasien { get; set; }
        public string TandaTanganMenyerahkanPasien { get; set; }
        public int SudahRegDkt { get; set; }
        public int SudahRegDPJP { get; set; }
        public int SudahRegMenerimaPasien { get; set; }
        public int SudahRegMenyerahkanPasien { get; set; }
        public int SudahRegPetugasBacaUlang { get; set; }
        public int SudahRegPetugasKonfirmasi { get; set; }
        public Nullable<bool> KaRU_Validasi { get; set; }
        public string Ruangan { get; set; }
        public string Kelas { get; set; }
        public bool? Kritis { get; set; }
        public bool? VerifikasiDPJP { get; set; }
        public string _METHOD { get; set; }

    }
}