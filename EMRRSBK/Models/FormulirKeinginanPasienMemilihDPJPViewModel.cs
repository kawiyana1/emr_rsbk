﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class FormulirKeinginanPasienMemilihDPJPViewModel
    {

        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Nama { get; set; }
        public string Umur { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalLahir { get; set; }
        public string TempatLahir { get; set; }
        public string Alamat { get; set; }
        public string Petugas { get; set; }
        public string PetugasNama { get; set; }
        public string YangMenyatakan { get; set; }
        public string TTD_Menyatakan { get; set; }
        public string Username { get; set; }
        public string HubunganPasien { get; set; }
        public string Dokter { get; set; }
        public string DokterNama { get; set; }
        public string NamaPasien { get; set; }
        public string TempatLahirPasien { get; set; }
        public string NoRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahirPasien { get; set; }
        public string DiriSendiri { get; set; }
        public string DPJP { get; set; }
        public string DPJPNama { get; set; }
        public string TandaTanganPetugas { get; set; }
        public string TandaTangan { get; set; }
        public int SudahRegPetugas { get; set; }
        public int SudahRegPasien { get; set; }
        public string DiriSendiriKet { get; set; }
    }
}