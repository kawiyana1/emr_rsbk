﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class HistoryResepViewModel
    {
        public string NoResep { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string NoRegistrasi { get; set; }
        public string NRM { get; set; }
        public string Deskripsi { get; set; } 
        public string DokterID { get; set; } 
        public string DokterNama { get; set; } 
        public string JenisResep { get; set; } 
    }
}