﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class RencanaKeperawatanHipertermiViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public bool Diagnosa_Peningkatan { get; set; }
        public bool Diagnosa_Dehidrasi { get; set; }
        public bool Diagnosa_Penyakit { get; set; }
        public bool Diagnosa_Gangguan { get; set; }
        public bool DS_PasienPanas { get; set; }
        public bool DS_Meriang { get; set; }
        public bool DS_Lainnya { get; set; }
        public string DS_Lainnya_Ket { get; set; }
        public bool DO_Suhu { get; set; }
        public string DO_Suhu_Ket { get; set; }
        public bool DO_Nadi { get; set; }
        public string DO_Nadi_Ket { get; set; }
        public bool DO_Respirasi { get; set; }
        public string DO_Respirasi_Ket { get; set; }
        public bool DO_Kulit { get; set; }
        public bool DO_TerabaPanas { get; set; }
        public string Tujuan_Waktu1 { get; set; }
        public string Tujuan_Waktu2 { get; set; }
        public bool Mandiri_1 { get; set; }
        public string Mandiri_1_Jam { get; set; }
        public bool Mandiri_2 { get; set; }
        public bool Mandiri_3 { get; set; }
        public bool Mandiri_4 { get; set; }
        public bool Mandiri_5 { get; set; }
        public bool Mandiri_6 { get; set; }
        public bool Mandiri_7 { get; set; }
        public bool Kolaborasi_1 { get; set; }
        public string Kolaborasi_1_Ket { get; set; }
        public string KodeDokter { get; set; }
        public string Username { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
    }
}