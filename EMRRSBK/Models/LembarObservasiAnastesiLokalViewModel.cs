﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class LembarObservasiAnastesiLokalViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Diagnosa { get; set; }
        public string RencanaTindakan { get; set; }
        public string Dokter { get; set; }
        public string DokterNama { get; set; }
        public string Asisten { get; set; }
        public string AnestesiLokal { get; set; }
        public string CaraPemberian { get; set; }
        public string Dosis { get; set; }
        public string StatusFisikAsa { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Pre_Kesadaran { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Pre_Tensi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Pre_Nadi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Pre_Respirasi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Pre_Suhu { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Pre_RiwayatAlergi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Pre_SkalaNyeri { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Intra_Kesadaran { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Intra_Tensi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Intra_Nadi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Intra_Respirasi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Intra_Suhu { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Intra_ReaksiAlergi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Intra_SkalaNyeri { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Post_Kesadaran_Dari { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Post_Kesadaran_Sampai { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Post_Tensi_Dari { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Post_Tensi_Sampai { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Post_Nadi_Dari { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Post_Nadi_Sampai { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Post_Respirasi_Dari { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Post_Respirasi_Sampai { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Post_Suhu_Dari { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Post_Suhu_Sampai { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Post_ReaksiAlergi_Dari { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Post_ReaksiAlergi_Sampai { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Post_SkalaNyeri_Dari { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Post_SkalaNyeri_Sampai { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
        public string Username { get; set; }
    }
}