﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class PemeriksaanPenunjangRIModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tgl_PemeriksaanPenunjang { get; set; }
        public string JenisPemeriksaan_PemeriksaanPenunjang { get; set; }
        public string Nilai_PemeriksaanPenunjang { get; set; }
        public string ParafVerivikator_PemeriksaanPenunjang { get; set; }
        public string ParafVerivikator_PemeriksaanPenunjangNama { get; set; }
        public string Username { get; set; }
    }
}
