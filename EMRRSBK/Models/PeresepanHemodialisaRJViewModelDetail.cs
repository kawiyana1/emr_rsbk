﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class PeresepanHemodialisaRJViewModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string BBKering { get; set; }
        public string FrekuensiHD { get; set; }
        public string LamaHD { get; set; }
        public string Dialiser { get; set; }
        public string DialisatFlow { get; set; }
        public string KecepatanAliranDarah { get; set; }
        public string JenisAksesVaskuler { get; set; }
        public string UkuranJarumFistula { get; set; }
        public string HeparinAwal { get; set; }
        public string Pemeliharaan { get; set; }
        public string Dokter { get; set; }
        public string DokterNama { get; set; }
        public string Username { get; set; }
    }
}