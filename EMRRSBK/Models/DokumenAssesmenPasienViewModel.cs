﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class DokumenAssesmenPasienViewModel
    {
        public string NoBukti { get; set; }
        public string NoReg { get; set; }
        public string NRM { get; set; }
        public Nullable<System.DateTime> TanggalCreate { get; set; }
        public string TanggalCreate_View { get; set; }
        public string DateDisplay { get; set; }
        public Nullable<System.DateTime> TanggalUpdate { get; set; }
        public string TanggalUpdate_View { get; set; }
        public Nullable<System.DateTime> TanggalCanceled { get; set; }
        public string CretaedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string UserIdWeb_Created { get; set; }
        public string UserIdWeb_Updated { get; set; }
        public bool Simpan { get; set; }
        public bool Batal { get; set; }
        public int NoDokumen { get; set; }
        public string DokumenID { get; set; }
        public string NamaDokumen { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public string DokterID { get; set; }
        public string NamaDOkter { get; set; }
        public Nullable<System.DateTime> Jam { get; set; }
        public string Jam_View { get; set; }
        public string CPPT_S { get; set; }
        public string CPPT_O { get; set; }
        public string CPPT_A { get; set; }
        public string CPPT_P { get; set; }
        public string CPPT_I { get; set; }
        public string Profesi { get; set; }
        public string Edit { get; set; }
        public string CPPT_OGambar { get; set; }
        public string TTDCPPT { get; set; }
    }
}