﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class AssesmentAwalPerawatNewViewModel
    {
        public ListDetail<RencanaKerjaMedisDetailModel> RencanaKerja_List { get; set; }

        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamKedatangan { get; set; }
        public string Rujukan { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> WaktuPengkajian { get; set; }
        public bool IRJ { get; set; }
        public bool IGD { get; set; }
        public bool LuarRS { get; set; }
        public string Poliklinik { get; set; }
        public string RsPengirim { get; set; }
        public string KeluhanUtama { get; set; }
        public string RiwayatPenyakit { get; set; }
        public string RiwayatPengobatan { get; set; }
        public string RiwayatAlergi { get; set; }
        public string RiwayatKeluarga { get; set; }
        public string RiwayatMrs { get; set; }
        public string Lama { get; set; }
        public string MRSAlasan { get; set; }
        public string RiwayatOperasi { get; set; }
        public string Jenisnya { get; set; }
        public string E { get; set; }
        public string V { get; set; }
        public string M { get; set; }
        public string TB { get; set; }
        public string BB { get; set; }
        public string TandaVital { get; set; }
        public string TekananDarah { get; set; }
        public string Nadi { get; set; }
        public string Pernafasan { get; set; }
        public string Suhu { get; set; }
        public string Saturasi { get; set; }
        public string StatusPerkawinan { get; set; }
        public string TinggalBersama { get; set; }
        public bool RiwayatMerokok { get; set; }
        public bool RiwayatAlkohol { get; set; }
        public bool RiwayatLainLain { get; set; }
        public string JenisDanJumlah { get; set; }
        public string ResikoMencederai { get; set; }
        public string KebiasaanSembahyang { get; set; }
        public bool Agama { get; set; }
        public string AgamaKet { get; set; }
        public bool NilaiKepercayaan { get; set; }
        public string Pekerjaan { get; set; }
        public string Asuransi { get; set; }
        public bool InfusIntravena { get; set; }
        public string InfusDipasangDi { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> InfusTanggal { get; set; }
        public bool CentralLine { get; set; }
        public string CentralDipasangDi { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> CentralTanggal { get; set; }
        public bool DowerCateter { get; set; }
        public string DowerDipasangDi { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> DowerTanggal { get; set; }
        public bool NGT { get; set; }
        public string NGTDipasangDi { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> NGTTanggal { get; set; }
        public bool CystotomyChateter { get; set; }
        public string CystotomyDipasangDi { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> CystotomyTanggal { get; set; }
        public bool Tracheostomy { get; set; }
        public string TracheostomyDipasangDi { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TracheostomyTanggal { get; set; }
        public bool InvasifLain { get; set; }
        public string InvasifLainKet { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> InvasifLainTanggal { get; set; }
        public string Status { get; set; }
        public bool MRSA { get; set; }
        public bool VRE { get; set; }
        public bool StatusTB { get; set; }
        public bool Infeksi { get; set; }
        public bool Lainnya { get; set; }
        public string LainnyaKet { get; set; }
        public bool Droplet { get; set; }
        public bool Airbone { get; set; }
        public bool Contact { get; set; }
        public bool Skin { get; set; }
        public bool ContactMulti { get; set; }
        public string SkriningPasienA { get; set; }
        public string SkriningPasienB { get; set; }
        public string PersonalHygiene { get; set; }
        public string Toileting { get; set; }
        public string Berpakaian { get; set; }
        public string Makan { get; set; }
        public string Mobilisasi { get; set; }
        public bool Tongkat { get; set; }
        public bool Walker { get; set; }
        public bool KursiRoda { get; set; }
        public bool Kruk { get; set; }
        public bool Penopang { get; set; }
        public bool Protesis { get; set; }
        public string Alasan { get; set; }
        public string Nyeri { get; set; }
        public string Lokasi { get; set; }
        public string Intensitas { get; set; }
        public string Jenis { get; set; }
        public string SkriningGizi1 { get; set; }
        public string SkriningGiziApakahAsupanMengalamiKet { get; set; }
        public string SkriningGizi1Skor { get; set; }
        public string SkriningGizi2 { get; set; }
        public string SkriningGizi2Skor { get; set; }
        public string TotalSkor { get; set; }
        public string SkriningGiziKriteriaGizi { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> EstimasiTanggal { get; set; }
        public string PasienPulangKe { get; set; }
        public string KesediaanMenerima { get; set; }
        public string EdukasiPasien { get; set; }
        public string MasalahKeperawatan { get; set; }
        public string PasienAtauKeluarga { get; set; }
        public string TTDPasienAtauKeluarga { get; set; }
        public string Perawat { get; set; }
        public string PerawatNama { get; set; }
        public string TTDPerawat { get; set; }
        public string Pengkajian_1a { get; set; }
        public string Pengkajian_1b { get; set; }
        public string Pengkajian_2a { get; set; }
        public string Hasil { get; set; }
        public string HasilKet { get; set; }
        public bool Usia { get; set; }
        public bool Tinggal { get; set; }
        public bool Stroke { get; set; }
        public bool PasienBerasal { get; set; }
        public bool Tunawisma { get; set; }
        public bool Dirawat { get; set; }
        public bool Percobaan { get; set; }
        public bool PasienTidakDiKenal { get; set; }
        public bool KorbanDariKasus { get; set; }
        public bool Trauma { get; set; }
        public bool TidakBekerja { get; set; }

        public int Report { get; set; }
        public int Nomor { get; set; }
        public string Template { get; set; }
        public bool save_template { get; set; }
        public string nama_template { get; set; }
        public string DokumenID { get; set; }
        public string KategoriKasus { get; set; }
        public bool DischargePlanning { get; set; }
    }
}