﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class TindakanKeperawatanViewModelDetail
    { 
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Nomor { get; set; }
        public bool MengukurTandaVital { get; set; }
        public bool MemberiObatInstalasi { get; set; }
        public bool MemasangInfus { get; set; }
        public bool MerawatLuka { get; set; }
        public bool MemasangDowerKateter { get; set; }
        public bool MemasangNGT { get; set; }
        public bool MelaksanakanGasticLavage { get; set; }
        public bool MelaksanakanFisioterapi { get; set; }
        public bool MemberiKompresHangat { get; set; }
        public bool MemberiPosisi { get; set; }
        public bool MemberiPosisiWaktu { get; set; }
        public bool MemberiOksigenasi { get; set; }
        public bool MelaksanakanSuction { get; set; }
        public bool MemberikanLatihanPernafasan { get; set; }
        public bool MelatihTeknikRelaksasi { get; set; }
        public bool MemeriksaEKG { get; set; }
        public bool MelakukanRJP { get; set; }
        public bool MemasangTraksi { get; set; }
        public bool MelakukanPenjahitanLuka { get; set; }
        public bool MemasangBalutBidai { get; set; }
        public bool PengambilanSampelLab { get; set; }
        public bool EkterpasiKuku { get; set; }
        public bool Penyuluhan { get; set; }
        public bool MengambilSampelDarah { get; set; }
        public bool MemasukanObatRektalwaktu { get; set; }
        public bool MemasangSeringPump { get; set; }
        public bool CekgulaAcak { get; set; }
        public bool AffNasogastrictube { get; set; }
        public bool MengambilSampledarahcross { get; set; }
        public bool MemberiObatTetesMata { get; set; }
        public bool MemberiObatMelaluiPump { get; set; }
        public bool PemasanganBedSideMonitor { get; set; }
        public bool SpolingIrigasiWaktu { get; set; }
        public string NamaPerawat { get; set; }
        public string Username { get; set; }
    }
}