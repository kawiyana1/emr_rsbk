﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class FingerViewModel
    {
        public string finger_data { get; set; }
        public int finger_id { get; set; }
        public string userid { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }
        public string TglLahir { get; set; }
        public string Alamat { get; set; }
        public string Phone { get; set; }
        public string Gambar1 { get; set; }
        public string id_View { get; set; }
        public string Kategori { get; set; }
    }
}