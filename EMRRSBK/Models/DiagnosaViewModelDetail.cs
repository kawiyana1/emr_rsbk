﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class DiagnosaViewModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }
        public int Nomor { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string TindakanKeperawatan { get; set; }
        public string Evaluasi { get; set; }
        public string NamaTerang { get; set; }
        public string NamaTerangNama { get; set; }
        public string Username { get; set; }
        public int SudahRegDkt { get; set; }
        public string Viewbag { get; set; }
        public string TTDDokter { get; set; }
    }
}