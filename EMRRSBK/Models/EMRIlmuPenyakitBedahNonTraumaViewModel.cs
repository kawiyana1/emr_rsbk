﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class EMRIlmuPenyakitBedahNonTraumaViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public bool Rujukan_Ya { get; set; }
        public string Rujukan_Ya_Ket { get; set; }
        public string DxRujukan { get; set; }
        public bool Rujuan_Tidak { get; set; }
        public string NamaKeluarga { get; set; }
        public string AlamatKeluarga { get; set; }
        public string TeleponKeluarga { get; set; }
        public string Transportasi { get; set; }
        public string Transportasi_Ket { get; set; }
        public string RiwayatAlergi { get; set; }
        public string RiwayatAlergi_Ket { get; set; }
        public string KeluhanUtama { get; set; }
        public string RiwayatPenyakitSekarang { get; set; }
        public string RiwayatPengobatan { get; set; }
        public string Nyeri { get; set; }
        public string Lokasi { get; set; }
        public string Intensitas { get; set; }
        public string Jenis { get; set; }
        public bool RiwayatPenyakit_Hipertensi { get; set; }
        public bool RiwayatPenyakit_Diabetes { get; set; }
        public bool RiwayatPenyakit_Jantung { get; set; }
        public bool RiwayatPenyakit_Stroke { get; set; }
        public bool RiwayatPenyakit_Dialysis { get; set; }
        public bool RiwayatPenyakit_Asma { get; set; }
        public bool RiwayatPenyakit_Kejang { get; set; }
        public bool RiwayatPenyakit_Liver { get; set; }
        public bool RiwayatPenyakit_Cancer { get; set; }
        public bool RiwayatPenyakit_TBC { get; set; }
        public bool RiwayatPenyakit_Glaukoma { get; set; }
        public bool RiwayatPenyakit_STD { get; set; }
        public bool RiwayatPenyakit_Perdarahan { get; set; }
        public bool RiwayatPenyakit_Lainnya { get; set; }
        public string RiwayatPenyakit_Lainnya_Ket { get; set; }
        public string RiwayatOperasi { get; set; }
        public string RiwayatOperasi_JenisKapan { get; set; }
        public string RiwayatTransfusi { get; set; }
        public string ReaksiTransfusi { get; set; }
        public string ReaksiYangTimbul { get; set; }
        public string RiwayatPenyakitKeluarga { get; set; }
        public string KeadaanUmum { get; set; }
        public string Gizi { get; set; }
        public string GCS_E { get; set; }
        public string GCS_V { get; set; }
        public string GCS_M { get; set; }
        public string TindakanResusitasi { get; set; }
        public string TindakanResusitasi_Ket { get; set; }
        public string Respirasi { get; set; }
        public string Saturasi { get; set; }
        public bool Saturasi_SuhuRuangan { get; set; }
        public bool Saturasi_NasalCanule { get; set; }
        public bool Saturasi_NRB { get; set; }
        public bool Saturasi_Lainnya { get; set; }
        public string Saturasi_Lainnya_Ket { get; set; }
        public string SuhuAxilla { get; set; }
        public string SuhuRectal { get; set; }
        public string BeratBadan { get; set; }
        public string PemeriksaanFisik_Kepala { get; set; }
        public string PemeriksaanFisik_Kepala_Ket { get; set; }
        public string PemeriksaanFisik_Mata { get; set; }
        public string PemeriksaanFisik_Mata_Ket { get; set; }
        public string PemeriksaanFisik_THT { get; set; }
        public string PemeriksaanFisik_THT_Ket { get; set; }
        public string PemeriksaanFisik_Leher { get; set; }
        public string PemeriksaanFisik_Leher_Ket { get; set; }
        public string PemeriksaanFisik_Dada { get; set; }
        public string PemeriksaanFisik_Dada_Ket { get; set; }
        public string PemeriksaanFisik_Jantung { get; set; }
        public string PemeriksaanFisik_Jantung_Ket { get; set; }
        public string PemeriksaanFisik_Paru { get; set; }
        public string PemeriksaanFisik_Paru_Ket { get; set; }
        public string PemeriksaanFisik_Perut { get; set; }
        public string PemeriksaanFisik_Perut_Ket { get; set; }
        public string PemeriksaanFisik_Heper { get; set; }
        public string PemeriksaanFisik_Heper_Ket { get; set; }
        public string PemeriksaanFisik_Lien { get; set; }
        public string PemeriksaanFisik_Lien_Ket { get; set; }
        public string PemeriksaanFisik_Punggung { get; set; }
        public string PemeriksaanFisik_Punggung_Ket { get; set; }
        public string PemeriksaanFisik_Genital { get; set; }
        public string PemeriksaanFisik_Genital_Ket { get; set; }
        public string PemeriksaanFisik_Ekstremitas { get; set; }
        public string PemeriksaanFisik_Ekstremitas_Ket { get; set; }
        public string PemeriksaanFisik_RectalToucher { get; set; }
        public string RencanaKerja { get; set; }
        public string HasilPemeriksaanPenunjang { get; set; }
        public string DiagnosaKerja { get; set; }
        public string Terapi { get; set; }
        public string Rekomendasi { get; set; }
        public string CatatanPenting { get; set; }
        public bool Disposisi_BolehPulang { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Disposisi_BolehPulang_Jam { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Disposisi_BolehPulang_Tanggal { get; set; }
        public string Disposisi_KontrolPoliklinik_Ya_Tidak { get; set; }
        public string Disposisi_KontrolPoliklinik_Ya_Ket { get; set; }
        public Nullable<System.DateTime> Disposisi_KontrolPoliklinik_Tanggal { get; set; }
        public bool Disposisi_Dirawat { get; set; }
        public bool Disposisi_Dirawat_Intensif { get; set; }
        public bool Disposisi_Dirawat_Ruang { get; set; }
        public string Disposisi_Dirawat_Lain { get; set; }
        public string DPJP { get; set; }
        public string DPJPNama { get; set; }
        public string Username { get; set; }
        public string PemeriksaanFisik_RectalToucher_Ket { get; set; }
        public string Diagnosa_Kerja { get; set; }
        public string PemeriksaanFisik_Anus { get; set; }
        public string PemeriksaanFisik_Anus_Ket { get; set; }
        public string TandaTangan { get; set; }
        public string TandaTanganDPJP { get; set; }

        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public List<SelectItemListPenyakitBedahNonTrauma> ListTemplate { get; set; } 
        // disable all form
        public int MODEVIEW { get; set; } 
    }

    public class SelectItemListPenyakitBedahNonTrauma
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}