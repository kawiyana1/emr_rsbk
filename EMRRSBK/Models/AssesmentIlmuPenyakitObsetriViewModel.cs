﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class AssesmentIlmuPenyakitObsetriViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public bool Rujukan_Ya { get; set; }
        public string Rujukan_Ya_Ket { get; set; }
        public string Rujukan_Ya_Dx { get; set; }
        public bool Rujukan_Tidak { get; set; }
        public string NamaKeluarga { get; set; }
        public string AlamatKeluarga { get; set; }
        public string TeleponKeluarga { get; set; }
        public string Transportasi { get; set; }
        public string Transportasi_Ket { get; set; }
        public string Riwayat_Alergi { get; set; }
        public string Nyeri { get; set; }
        public string Lokasi { get; set; }
        public string Intensitas { get; set; }
        public string Jenis { get; set; }
        public string Riwayat_Alergi_Ket { get; set; }
        public string KeluhanUtama { get; set; }
        public string Komentar { get; set; }
        public string Diagnosa { get; set; }
        public string Terapi { get; set; }
        public string RencanaKerja { get; set; }
        public bool Disposisi_BolehPulang { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> BolehPulangJam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> BolehPulangTgl { get; set; }
        public string KontrolPoliklinik { get; set; }
        public string KontrolPoliklinik_Ket { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> KontrolPoliklinik_Tanggal { get; set; }
        public bool Disposisi_Dirawat { get; set; }
        public string Disposisi_Dirawat_Ket { get; set; }
        public string RuanganLain { get; set; }
        public string DokterBPJP { get; set; }
        public string DokterBPJPNama { get; set; }
        public string Username { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        public int SudahRegDokter { get; set; }
        public string TandaTanganDPJP { get; set; }
    }
}