﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class TindakanKedokteranIGDViewModel 
    {
        public int No { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string DokterPelaksananTindakan { get; set; }
        public string DokterPelaksananTindakanNama { get; set; }
        public string PemberiInformasi { get; set; }
        public string PemberiInformasiNama { get; set; }
        public string Penerima_Nama { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Penerima_Tgl { get; set; }
        public string Penerima_Hubungan { get; set; }
        public bool Diagnosis { get; set; }
        public string Diagnosis_Ket { get; set; }
        public bool DasarDiagnosis { get; set; }
        public string DasarDiagnosis_Ket { get; set; }
        public bool TindakanKedokteran { get; set; }
        public string TindakanKedokteran_Ket { get; set; }
        public bool IndikasiTindakan { get; set; }
        public string IndikasiTindakan_Ket { get; set; }
        public bool TataCara { get; set; }
        public string TataCara_Ket { get; set; }
        public bool Tujuan { get; set; }
        public string Tujuan_Ket { get; set; }
        public bool Risiko { get; set; }
        public string Risiko_Ket { get; set; }
        public bool Komplikasi { get; set; }
        public string Komplikasi_Ket { get; set; }
        public bool Prognosis { get; set; }
        public string Prognosis_Ket { get; set; }
        public bool AlternatifRisiko { get; set; }
        public string AlternatifRisiko_Ket { get; set; }
        public bool Lainlain { get; set; }
        public string Lainlain_Ket { get; set; }
        public string Persetujuan_Nama { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Persetujuan_TglLahir { get; set; }
        public string Persetujuan_JK { get; set; }
        public string Persetujuan_Alamat { get; set; }
        public string Persetujuan_Pernyataan { get; set; }
        public string Persetujuan_Terhadap { get; set; }
        public string Persetujuan_Terhadap_Ket { get; set; }
        public Nullable<System.DateTime> Persetujuan_Tgl { get; set; }
        public string Persetujuan_Saksi1 { get; set; }
        public string Persetujuan_Saksi2 { get; set; }
        public string Penolakan_Nama { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Penolakan_TglLahir { get; set; }
        public string Penolakan_JK { get; set; }
        public string Penolakan_Alamat { get; set; }
        public string Penolakan_Pernyataan { get; set; }
        public string Penolakan_Terhadap { get; set; }
        public string Penolakan_Terhadap_Ket { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Penolakan_Tgl { get; set; }
        public string Penolakan_Saksi1 { get; set; }
        public string Penolakan_Saksi2 { get; set; }
        public string Username { get; set; }
        public string NamaPasien { get; set; }
        public string NamaPasien1 { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir1 { get; set; }

        public string  JenisKelamin { get; set; }
        public string  JenisKelamin1 { get; set; }
        public string AlamatPasien { get; set; }
        public string AlamatPasien1 { get; set; }
        public string Merangkan { get; set; }
        public string MerangkanNama { get; set; }
        public string Menerima { get; set; }
        public string TTD_Saksi1 { get; set; }
        public string TTD_Saksi2 { get; set; }
        public string TTD_Menerangkan_PemberianInfromasi { get; set; }
        public string TTD_Menerima_PemberianInfromasi { get; set; }
        public string Lainnya_Persetujuan { get; set; }
        public string Menerangkan_Persetujuan { get; set; }
        public string Menerangkan_PersetujuanNama { get; set; }
        public string Menerima_Persetujuan { get; set; }
        public string TTD_Menerangkan_Persetujuan { get; set; }
        public string TTD_Menerima_Persetujuan { get; set; }
        public string Lainnya_Penolakan { get; set; }
        public string TTD_Menerangkan_Penolakan { get; set; }
        public string TTD_Menerima_Penolakan { get; set; }
        public string Menerangkan_Penolakan { get; set; }
        public string Menerima_Penolakan { get; set; }
        public string TTD_Saksi1_Penolakan { get; set; }
        public string TTD_Saksi2_Penolakan { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Persetujuan_Jam { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Penolakan_Jam { get; set; }
        public string _METHOD { get; set; }
        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public List<SelectItemListTindakanKedokteran> ListTemplate { get; set; }
    }

    public class SelectItemListTindakanKedokteran
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}