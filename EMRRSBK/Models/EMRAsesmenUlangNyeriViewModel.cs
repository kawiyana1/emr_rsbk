﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class EMRAsesmenUlangNyeriViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string SkorNyeri { get; set; }
        public string SkorSedasi { get; set; }
        public string Obat { get; set; }
        public string ObatNama { get; set; }
        public string Dosis { get; set; }
        public string Rute { get; set; }
        public string Farmakologi { get; set; }
        public string Bidan { get; set; }
        public string BidanNama { get; set; }
        public string Paraf { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> KajianUlang { get; set; }
        public string KajianUlangKet { get; set; }
        public string Keterangan { get; set; }
        public string Username { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }


        public int MODEVIEW { get; set; }
        
    }
}