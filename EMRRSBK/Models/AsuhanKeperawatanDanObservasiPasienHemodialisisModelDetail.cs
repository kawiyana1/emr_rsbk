﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class AsuhanKeperawatanDanObservasiPasienHemodialisisModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }
        public string Username { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string TekananDarah { get; set; }
        public string Nadi { get; set; }
        public string Blood { get; set; }
        public string Vena { get; set; }
        public string UltraGoal { get; set; }
        public string UltraRate { get; set; }
        public string UltraRemove { get; set; }
        public string Tindakan { get; set; }
        public string Petugas { get; set; }
        public string PetugasNama { get; set; }
    }
}