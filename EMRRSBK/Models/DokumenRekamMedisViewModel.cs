﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class DokumenRekamMedisViewModel
    {
        public ListDetail<DokumenRekamMedisDetailViewModel> FileUpload { get; set; }
        public string NoBukti { get; set; }
        [DataType(DataType.Date)]
        public System.DateTime TanggalDibuat { get; set; }
        public string NoReg { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }
        public string Alamat { get; set; }
        public string NoTelepon { get; set; }
        public string TanggalView { get; set; }
        [DataType(DataType.Date)]
        public System.DateTime TglReg { get; set; }
        public string TglReg_View { get; set; }
        public Nullable<int> JenisKerjasamaID { get; set; }
        public string JenisKerjasama { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public string UserID { get; set; }
        public Nullable<bool> RawatJalan { get; set; }
        public Nullable<bool> RawatInap { get; set; }
        public string Catatan { get; set; }
        public HttpPostedFileBase[] FilePDF { get; set; }
        public string Pelayanan { get; set; }
    }
}