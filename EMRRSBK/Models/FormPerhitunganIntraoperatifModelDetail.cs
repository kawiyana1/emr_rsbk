﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class FormPerhitunganIntraoperatifModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string NamaItem { get; set; }
        public string NamaItemNama { get; set; }
        public string PerhitunganAwal { get; set; }
        public string PenambahanPertama_1 { get; set; }
        public string PenambahanPertama_2 { get; set; }
        public string PenambahanPertama_3 { get; set; }
        public string PenambahanPertama_4 { get; set; }
        public string Total_Pertama { get; set; }
        public string PerhitunganPertama { get; set; }
        public string PenambahanKedua_1 { get; set; }
        public string PenambahanKedua_2 { get; set; }
        public string PenambahanKedua_3 { get; set; }
        public string PenambahanKedua_4 { get; set; }
        public string Total_Kedua { get; set; }
        public string PerhitunganAkhir { get; set; }
        public string Username { get; set; }
    }
}
