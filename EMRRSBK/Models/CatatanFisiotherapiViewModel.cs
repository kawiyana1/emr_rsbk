﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class CatatanFisiotherapiViewModel
    {
        public ListDetail<KunjunganFisiotherapiViewModelDetail> Kunjungan_List { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string UserName { get; set; }
        public string Keluhan_utama { get; set; }
        public string Riwayat_Penyakit { get; set; }
        public string Periksa_fisik { get; set; }
        public string Periksa_khusus { get; set; }
        public string Diag_fisiotherapi { get; set; }
        public string Tindak_fisiotherapi { get; set; }
        public string Nama_DPJP { get; set; }
        public string Nama_DPJPNama { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        public string NamaPasien { get; set; }
        public int? Umur { get; set; }
        public string Alamat { get; set; }

        public string DokumenID { get; set; }
        public string Template { get; set; }
        public bool save_template { get; set; }
        public string nama_template { get; set; }
    }
}