﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class AssesmentPraAnestesiDanSedasiViewModel
    {
        public ListDetail<AssesmentPraAnestesiDanSedasiModelDetail> Saran_List { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string UserName { get; set; }
        public string Anamnesis { get; set; }
        public string Diagnosa { get; set; }
        public bool RP_DM { get; set; }
        public bool RP_Asma { get; set; }
        public bool RP_Hipertensi { get; set; }
        public bool RP_PenyakitSSP { get; set; }
        public bool RP_PenyakitJantung { get; set; }
        public bool RP_PenyakitHati { get; set; }
        public bool RP_PenyakitGangguanPerdarahan { get; set; }
        public bool RP_PenyakitGinjal { get; set; }
        public bool RP_Lain { get; set; }
        public bool Pernah { get; set; }
        public bool P_Antibiotika { get; set; }
        public bool P_AntiHipertens { get; set; }
        public bool P_AntiDiabetik { get; set; }
        public bool P_Lain { get; set; }
        public string P_KetAntibiotika { get; set; }
        public string P_KetAntiHipertens { get; set; }
        public string P_KetAntiDiabetik { get; set; }
        public string P_KetLain { get; set; }
        public bool SedangDiKonsumsi { get; set; }
        public bool SDK_Antibiotika { get; set; }
        public bool SDK_AntiHipertensi { get; set; }
        public bool SDK_AntlDiabetik { get; set; }
        public bool SDK_Lain { get; set; }
        public string SDK_KetAntibiotika { get; set; }
        public string SDK_KetAntiHipertensi { get; set; }
        public string SDK_KetAntlDiabetik { get; set; }
        public string SDK_KetLain { get; set; }
        public string Alergi { get; set; }
        public string Ket_Alergi { get; set; }
        public string RB_JenisOperasi { get; set; }
        public string RB_JenisAnestesi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> RB_Waktu { get; set; }
        public string RB_OperasiKomplikasi { get; set; }
        public string RB_AnestesiKomplikasi { get; set; }
        public bool RK_Perokok { get; set; }
        public bool RK_Alkoholic { get; set; }
        public bool RK_PecanduNarkotic { get; set; }
        public bool RK_GigiPalsu { get; set; }
        public bool RK_Lain { get; set; }
        public bool RPK_DM { get; set; }
        public bool RPK_Hipertensi { get; set; }
        public bool RPK_Asma { get; set; }
        public bool RPK_PenyakitSSP { get; set; }
        public bool RPK_PenyakitJantung { get; set; }
        public bool RPK_PenyakitHati { get; set; }
        public bool RPK_PenyakitGangguanPerdarahan { get; set; }
        public bool RPK_PenyakitGinjal { get; set; }
        public bool RPK_Lain { get; set; }
        public string PFDP_KeadaanUmum { get; set; }
        public string PFDP_BB { get; set; }
        public string PFDP_TD { get; set; }
        public string PFDP_Nadi { get; set; }
        public string PFDP_TB { get; set; }
        public string PFDP_RR { get; set; }
        public string PFDP_GolDarah { get; set; }
        public string PFDP_Suhu { get; set; }
        public string PFDP_Rh { get; set; }
        public string PFDP_Hb { get; set; }
        public string B1_Breath { get; set; }
        public string B2_Blood { get; set; }
        public string B3_Brain { get; set; }
        public string B4_Blader { get; set; }
        public string B5_Bowel { get; set; }
        public string B6_bone { get; set; }
        public string Ket_Lainlain { get; set; }
        public string Kesimpulan { get; set; }
        public string StatusFisikAsa { get; set; }
        public string PenyulitPraAnestesi { get; set; }
        public string TindakanAnestesi { get; set; }
        public bool PA_GA { get; set; }
        public bool PA_BSA { get; set; }
        public bool PA_Epidural { get; set; }
        public bool PA_CSE { get; set; }
        public bool PA_Lokal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamBerakhirAssesmen { get; set; }
        public string NamaDokter { get; set; }
        public string NamaDokterNama { get; set; }
        public string JenisKelamin { get; set; }
        public string NamaPasien { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        public int SudahRegDokter { get; set; }
        public string TandaTanganDokter { get; set; }
        public int MODEVIEW { get; set; }
        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public List<SelectItemListPraAnestesiSedasi> ListTemplate { get; set; }

    }

    public class SelectItemListPraAnestesiSedasi
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}