﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class ReturnFingerViewModel
    {
        public string Petugas { get; set; }
        public string Pasien { get; set; }
        public string Saksi1 { get; set; }
        public string Saksi2 { get; set; }
        public string DokterPengkaji { get; set; }
        public string Dokter { get; set; }
        public string Perawat { get; set; }
        public string Bidan { get; set; }
        public string DPJP { get; set; }
        public string DokterAnak { get; set; }
        public string DokterAnestesi { get; set; }
        public string PerawatSirkuler { get; set; }
        public string PerawatAnestesi { get; set; }
        public string Instrumen { get; set; }
        public string Operator { get; set; }
        public string Asisten { get; set; }
        public string MenerimaPasien { get; set; }
        public string MenyerahkanPasien { get; set; }
        public string PetugasBacaUlang { get; set; }
        public string PetugasKonfirmasi { get; set; }
        public string TandaTanganDPJP { get; set; }
        public string TandaTanganMenerimaPasien { get; set; }
        public string TandaTanganMenyerahkanPasien { get; set; }
        public string TandaTanganPetugasBacaUlang { get; set; }
        public string TandaTanganPetugasKonfirmasi { get; set; }
    }
}