﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class PersetujuanDirawatICUViewModel
    {

        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Nama { get; set; }
        public int? Umur { get; set; }
        public string Jenis { get; set; }
        public string Alamat { get; set; }
        public bool TujuanPasien { get; set; }
        public bool ResikoDanKomplikasi { get; set; }
        public bool Peringatan { get; set; }
        public bool PerkiraanBiaya { get; set; }
        public string Saya { get; set; }
        public string Dokter { get; set; }
        public string DokterNama { get; set; }
        public string MembuatPernyataan { get; set; }
        public string Saksi1 { get; set; }
        public string Saksi1Nama { get; set; }
        public string Saksi2 { get; set; }
        public string Saksi2Nama { get; set; }
        public string Username { get; set; }

        public bool Jaminan1 { get; set; }

        public bool Jaminan2 { get; set; }

        public bool Jaminan3 { get; set; }

        public bool Jaminan4 { get; set; }

        public string JaminanKesehatan1 { get; set; }
        public string JaminanKesehatan { get; set; }
        public string Terhadap { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        public int SudahRegPegawai { get; set; }
        public int SudahRegPasien { get; set; }
        public int SudahRegSaksi1{ get; set; }
        public int SudahRegSaksi2 { get; set; }

        public string TTDPasien { get; set; }
        public string TTDpetugas { get; set; }
        public string TTDSaksi1 { get; set; }
        public string TTDSaksi2 { get; set; }
        public string TerhadapLainnya { get; set; }
        public bool Umum_1 { get; set; }
        public bool Jaminan_2 { get; set; }
        public bool Jaminan_3 { get; set; }
        public bool Jaminan_4 { get; set; }


        public string NamaPasien { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
        public string JenisKelamin { get; set; }
        public string AlamatPasien { get; set; }
    }
}