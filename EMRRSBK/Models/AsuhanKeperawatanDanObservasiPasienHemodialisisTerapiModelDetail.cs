﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class AsuhanKeperawatanDanObservasiPasienHemodialisisTerapiModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }
        public string Obat { get; set; }
        public string ObatNama { get; set; }
        public string Dosis { get; set; }
        public string CaraPemberian { get; set; }
        public string Waktu { get; set; }
        public string Perawat1 { get; set; }
        public string Perawat1Nama { get; set; }
        public string Perawat2 { get; set; }
        public string Perawat2Nama { get; set; }
        public string Username { get; set; }
    }
}