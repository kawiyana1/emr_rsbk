﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class AssesmentIlmuPenyakitGynecologyViewModel
    {
        //public ListDetail<RencanaKerjaDokterHiperbarikViewModelDetail> RKDHiperbark_List { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public bool Rujukan_Ya { get; set; }
        public string Rujukan_Ya_Ket { get; set; }
        public string Rujukan_Ya_Dx { get; set; }
        public bool Rujukan_Tidak { get; set; }
        public string NamaKeluarga { get; set; }
        public string AlamatKeluarga { get; set; }
        public string TeleponKeluarga { get; set; }
        public string Transportasi { get; set; }
        public string Transportasi_Ket { get; set; }
        public string Riwayat_Alergi { get; set; }
        public string Nyeri { get; set; }
        public string Lokasi { get; set; }
        public string Intensitas { get; set; }
        public string Jenis { get; set; }
        public string Riwayat_Alergi_Ket { get; set; }
        public string KeluhanUtama { get; set; }
        public string RiwayatPenyakitSekarang { get; set; }
        public bool RiwayatMenstruasi_Menarche { get; set; }
        public string RiwayatMenstruasi_Menarche_Ket { get; set; }
        public bool RiwayatMenstruasi_Menstruasi { get; set; }
        public string RiwayatMenstruasi_Menstruasi_Ket { get; set; }
        public bool RiwayatMenstruasi_Siklus { get; set; }
        public string RiwayatMenstruasi_Siklus_Ket { get; set; }
        public bool RiwayatMenstruasi_Teratur { get; set; }
        public string RiwayatMenstruasi_Teratur_Ket { get; set; }
        public bool RiwayatMenstruasi_Lamanya { get; set; }
        public string RiwayatMenstruasi_Lamanya_Ket { get; set; }
        public bool RiwayatMenstruasi_Banyaknya { get; set; }
        public string RiwayatMenstruasi_Banyaknya_Ket { get; set; }
        public bool RiwayatMenstruasi_Keluhan { get; set; }
        public string RiwayatMenstruasi_Keluhan_Ket { get; set; }
        public bool RiwayatMenstruasi_HCG { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> RiwayatMenstruasi_HCG_Ket { get; set; }
        public bool RiwayatMenstruasi_Kontrasepsi { get; set; }
        public string RiwayatMenstruasi_Kontrasepsi_Ket { get; set; }
        public string RiwayatPerkawinan_Status { get; set; }
        public string RiwayatPerkawinan_Kawin { get; set; }
        public string RiwayatPerkawinan_UmurKawin { get; set; }
        public string RiwayatPerkawinan_YangKe { get; set; }
        public string RiwayatPerkawinan_YangKe_Ket { get; set; }
        public string RiwayatKehamilan { get; set; }
        public bool RiwayatPenyakitDahulu_Hepertensi { get; set; }
        public bool RiwayatPenyakitDahulu_KencingManis { get; set; }
        public bool RiwayatPenyakitDahulu_Jantung { get; set; }
        public bool RiwayatPenyakitDahulu_Asthma { get; set; }
        public bool RiwayatPenyakitDahulu_Jiwa { get; set; }
        public bool RiwayatPenyakitDahulu_Varises { get; set; }
        public bool RiwayatPenyakitDahulu_Tumor { get; set; }
        public string RiwayatPenyakitDahulu_Tumor_Ket { get; set; }
        public bool RiwayatPenyakitDahulu_Operasi { get; set; }
        public string RiwayatPenyakitDahulu_Operasi_Ket { get; set; }
        public string RiwayatPenyakitDahulu_Pernah_Ket { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> RiwayatPenyakitDahulu_Pernah_Tgl { get; set; }
        public bool RiwayatPenyakitDahulu_Lainnya { get; set; }
        public string RiwayatPenyakitDahulu_Lainnya_Ket { get; set; }
        public bool RiwayatPenyakitKeluarga_Hepertensi { get; set; }
        public bool RiwayatPenyakitKeluarga_KencingManis { get; set; }
        public bool RiwayatPenyakitKeluarga_Jantung { get; set; }
        public bool RiwayatPenyakitKeluarga_Asthma { get; set; }
        public bool RiwayatPenyakitKeluarga_Tumor { get; set; }
        public string RiwayatPenyakitKeluarga_Tumor_Ket { get; set; }
        public bool RiwayatPenyakitKeluarga_Lainnya { get; set; }
        public string RiwayatPenyakitKeluarga_Lainnya_Ket { get; set; }
        public string RiwayatSosial { get; set; }
        public string KeadaanUmum { get; set; }
        public string Gizi { get; set; }
        public string GCS_E { get; set; }
        public string GCS_M { get; set; }
        public string GCS_V { get; set; }
        public string Resusitas { get; set; }
        public string BB { get; set; }
        public string TB { get; set; }
        public string Tensi { get; set; }
        public string SuhuAxila { get; set; }
        public string SuhuRectal { get; set; }
        public string Nadi { get; set; }
        public string Respirasi { get; set; }
        public string Saturasi { get; set; }
        public string TandaVitalPada { get; set; }
        public string Pemeriksaan_Kepala { get; set; }
        public string Pemeriksaan_Mata { get; set; }
        public string Pemeriksaan_Gigi { get; set; }
        public string Pemeriksaan_Tiroid { get; set; }
        public string Pemeriksaan_Payudara { get; set; }
        public string Pemeriksaan_Jantung { get; set; }
        public string Pemeriksaan_Paru { get; set; }
        public string Pemeriksaan_Perut { get; set; }
        public string Pemeriksaan_Pelvic { get; set; }
        public string Pemeriksaan_TungkaiAtas { get; set; }
        public string Pemeriksaan_TungkaiBawah { get; set; }
        public string Pemeriksaan_KelenjarLimfe { get; set; }
        public string Pemeriksaan_Vulva { get; set; }
        public string Pemeriksaan_Vaginal { get; set; }
        public string Pemeriksaan_Cervix { get; set; }
        public string Pemeriksaan_Uterus { get; set; }
        public string Pemeriksaan_Adnexa { get; set; }
        public string Pemeriksaan_Rectum { get; set; }
        public string Pemeriksaan_Lainnya { get; set; }
        public string Komentar { get; set; }
        public string Laboratorium { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Hispatologi { get; set; }
        public string NoPa { get; set; }
        public string Diagnosa { get; set; }
        public string Terapi { get; set; }
        public bool Disposisi_BolehPulang { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> BolehPulangJam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> BolehPulangTgl { get; set; }
        public string KontrolPoliklinik { get; set; }
        public string KontrolPoliklinik_Ket { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> KontrolPoliklinik_Tanggal { get; set; }
        public bool Disposisi_Dirawat { get; set; }
        public string Disposisi_Dirawat_Ket { get; set; }
        public string RuanganLain { get; set; }
        public string Catatan { get; set; }
        public string DokterBPJP { get; set; }
        public string DokterBPJPNama { get; set; }
        public string Username { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        public int SudahRegDokter { get; set; }
        public string TandaTanganDPJP { get; set; }
    }
}