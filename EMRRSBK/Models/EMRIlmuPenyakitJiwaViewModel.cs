﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EMRRSBK.Models
{
    public class EMRIlmuPenyakitJiwaViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public bool Rujukan_Ya { get; set; }
        public bool Rujukan_RS { get; set; }
        public string Rujukan_RS_Ket { get; set; }
        public bool Rujukan_Dr { get; set; }
        public string Rujukan_Dr_Ket { get; set; }
        public bool Rujukan_Puskesmas { get; set; }
        public string Rujukan_Puskesmas_Ket { get; set; }
        public bool Rujukan_Lainnya { get; set; }
        public string Rujukan_Lainnya_Ket { get; set; }
        public bool DxRujukan { get; set; }
        public string DxRujukan_Ket { get; set; }
        public string Tidak_DatangSendiri_Diantar { get; set; }
        public string Tidak_DatangSendiri_Diantar_Ket { get; set; }
        public string Asuransi { get; set; }
        public string NamaKeluarga { get; set; }
        public string TelpKeluarga { get; set; }
        public string AlamatKeluarga { get; set; }
        public string TranspotasiDatang { get; set; }
        public string TranspotasiDatang_Ket { get; set; }
        public string RiwayatAlergi { get; set; }
        public string RiwayatAlergi_Obat { get; set; }
        public string RiwayatAlergi_Obat_Gejala { get; set; }
        public string RiwayatAlergi_Makanan { get; set; }
        public string RiwayatAlergi_Makanan_Gejala { get; set; }
        public string RiwayatAlergi_Lainlain { get; set; }
        public string RiwayatAlergi_Lainlain_Gejala { get; set; }
        public string Nyeri { get; set; }
        public string Nyeri_Lokasi { get; set; }
        public string Nyeri_Intensitas { get; set; }
        public string Nyeri_Jenis { get; set; }
        public string Wawancara { get; set; }
        public string KeluhanUtama { get; set; }
        public string RiwayatPenyakitSekarang { get; set; }
        public string AutoAnamnesa { get; set; }
        public string HeteroAnamnesa { get; set; }
        public string FaktorPencetus { get; set; }
        public string FaktorKeluarga { get; set; }
        public string FungsiKerja { get; set; }
        public string RiwayatNAPZA { get; set; }
        public bool LamaPakai { get; set; }
        public string LamaPakai_Ket { get; set; }
        public bool JenisZat { get; set; }
        public string JenisZat_Ket { get; set; }
        public bool CaraPakai { get; set; }
        public string CaraPakai_Ket { get; set; }
        public bool LatarBelakangPakai { get; set; }
        public string LatarBelakangPakai_Ket { get; set; }
        public string FaktorPremorbid { get; set; }
        public string FaktorOrganik { get; set; }
        public string KesanUmum { get; set; }
        public string Kesadaran { get; set; }
        public string Mood { get; set; }
        public string ProsesPikir { get; set; }
        public string Penerapan { get; set; }
        public string Intelegensi { get; set; }
        public string DoronganInsting { get; set; }
        public string Psikomotor { get; set; }
        public string KeadaanUmum { get; set; }
        public string Gizi { get; set; }
        public string Tensi { get; set; }
        public string Suhu { get; set; }
        public string Nadi { get; set; }
        public string Respirasi { get; set; }
        public string BeratBadan { get; set; }
        public string StatusNeorologi { get; set; }
        public string Terapi { get; set; }
        public string DiagnosaKerja { get; set; }
        public string RencanaKerja { get; set; }
        public bool BolehPulang { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> BolehPulang_Jam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> BolehPulang_Tanggal { get; set; }
        public string KontrolPoli { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> KontrolPoli_Tanggal { get; set; }
        public string Dokter { get; set; }
        public string DokterNama { get; set; }
        public string DPJP { get; set; }
        public string DPJPNama { get; set; }
        public string Username { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
        public string TandaTanganDPJP { get; set; }
        public string TandaTanganDokter { get; set; }

        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public List<SelectItemListPenyakitJiwa> ListTemplate { get; set; }
        // disable all form
        public int MODEVIEW { get; set; } 
    }

    public class SelectItemListPenyakitJiwa
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}