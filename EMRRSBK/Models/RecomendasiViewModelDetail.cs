﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class RecomendasiViewModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }
        public string Terapi { get; set; }
        public string Dosis { get; set; }
        public string Username { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamPemberian { get; set; }
    }
}