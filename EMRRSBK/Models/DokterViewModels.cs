﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class DokterViewModels
    {
        public string DokterID { get; set; }
        public string NamaDOkter { get; set; }
        public string NamaDokter { get; set; }
        public string Alamat { get; set; }
        public string NoKontak { get; set; }
        public string SpesialisasiID { get; set; }
        public string SubSpesialisasiID { get; set; }
        public Nullable<bool> Tetap { get; set; }
        public Nullable<double> THT { get; set; }
        public Nullable<double> HonorDefault { get; set; }
        public Nullable<double> KomisiDefault { get; set; }
        public string IDPersonal { get; set; }
        public string KodeKategoriVendor { get; set; }
        public Nullable<double> Pajak { get; set; }
        public bool Active { get; set; }
        public string DokterIdHaiMed { get; set; }
        public string Foto { get; set; }
        public Nullable<bool> DokterPraktek { get; set; }
        public Nullable<bool> DokterIdEMR { get; set; }
        public string NamaProfesi { get; set; }
        public string Alias { get; set; }
    }
}