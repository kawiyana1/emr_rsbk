﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class RencanaKeperawatanIntoleransiAktivitasViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public bool Ketidakseimbangan { get; set; }
        public bool NyreiKronis { get; set; }
        public bool TirahBaring { get; set; }
        public bool KelemahanUmum { get; set; }
        public bool StatusNutrisi { get; set; }
        public bool Anemia { get; set; }
        public bool ProsesPenyakit { get; set; }
        public bool DS_Pernyataan { get; set; }
        public bool DS_Ketidaknyamanan { get; set; }
        public bool DS_Riwayat { get; set; }
        public bool DO_Dispnea { get; set; }
        public bool DO_VitalSign { get; set; }
        public bool DO_Aritmia { get; set; }
        public bool DO_KondisiMenurun { get; set; }
        public bool DO_Ketidakmampuan { get; set; }
        public string TindakanKeperawatan1 { get; set; }
        public string TindakanKeperawatan2 { get; set; }
        public bool Mandiri_1 { get; set; }
        public bool Mandiri_2 { get; set; }
        public bool Mandiri_3 { get; set; }
        public bool Mandiri_4 { get; set; }
        public bool Mandiri_5 { get; set; }
        public bool Mandiri_6 { get; set; }
        public bool Mandiri_7 { get; set; }
        public bool Mandiri_8 { get; set; }
        public bool Kolaborasi_1 { get; set; }
        public bool Kolaborasi_2 { get; set; }
        public string KodeDokter { get; set; }
        public string KodeDokterNama { get; set; }
        public string Username { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
    }
}