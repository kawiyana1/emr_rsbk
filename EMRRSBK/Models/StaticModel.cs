﻿using EMRRSBK.Entities;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Web.Mvc;

namespace EMRRSBK.Models
{

    public static class StaticModel
    {
        public static string DbEntityValidationExceptionToString(DbEntityValidationException ex)
        {
            var msgs = new List<string>();
            foreach (var eve in ex.EntityValidationErrors)
            {
                var msg = $"Entity of type \"{eve.Entry.Entity.GetType().Name}\" in state \"{eve.Entry.State}\" has the following validation errors:";
                foreach (var ve in eve.ValidationErrors) { msg += $"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\""; }
                msgs.Add(msg);
            }
            return string.Join("\n", msgs.ToArray());
        }

        #region ===== M D 5
        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
        #endregion

        public static List<SelectListItem> ListJenisKerjaSama
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIM_Entities())
                {
                    r = s.SIMmJenisKerjasama.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.JenisKerjasama,
                        Value = x.JenisKerjasama
                    });
                }
                return r;
            }
        }

        public static List<SelectListItem> OrientasiPsnBruRwtInap
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Ya", Value = "Ya" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                return result;
            }
        }
        public static string ListSectionCustom
        {
            get
            {
                var r = "";
                using (var s = new SIM_Entities())
                {
                    var h = s.Pelayanan_TipePelayanan_Get_New.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.NamaPelayanan,
                        Value = x.TipePelayanan
                    });
                    var m = s.SIMmSection.Where(x => x.StatusAktif == true).ToList();
                    foreach (var group in h)
                    {
                        var optionGroup = new SelectListGroup() { Name = group.Text };
                        r += $"<optgroup label=\"{group.Text}\">";
                        foreach (var item in m.Where(x => x.TipePelayanan == group.Value).ToList())
                        {
                            r += $"<option value=\"{item.SectionID}\">{item.SectionName}</option>";
                        }
                        r += $"</optgroup>";
                    }
                }
                return r;
            }
        }
        public static List<SelectListItem> ListTipePelayanan
        {
            get
            {
                //var m = new List<SelectListItem>()
                //{
                //    new SelectListItem(){ Text = "", Value = "" },
                //};
                List<SelectListItem> r;
                using (var s = new SIM_Entities())
                {
                    r = s.Pelayanan_TipePelayanan_Get_New.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.NamaPelayanan,
                        Value = x.TipePelayanan
                    });
                }
                
                return r;
            }
        }

        public static List<SelectListItem> ListKetPakai
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIM_Entities())
                {
                    r = s.mKetRacikan.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama,
                        Value = x.Nama
                    });
                }
                r.Insert(0, new SelectListItem());
                return r;
            }
        }

        public static List<SelectListItem> ListStatusRacik
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIM_Entities())
                {
                    r = s.mStatusRacikan.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama,
                        Value = x.Nama
                    });
                }
                r.Insert(0, new SelectListItem());
                return r;
            }
        }

        public static List<SelectListItem> ListSectionFarmasi
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    //var m = s.SIMmSection.Where(x => x.TipePelayanan == "FARMASI" & x.NoIP == GetLocalIPAddress).ToList();
                    var m = s.SIMmSection.Where(x => x.TipePelayanan == "FARMASI").ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.SectionID.ToString(),
                            Text = item.SectionName
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListSatuanPuyer
        {
            get
            {
                var r = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "KAPSUL", Value = "KAPSUL"},
                    new SelectListItem(){ Text = "PUYER", Value = "PUYER"},
                    new SelectListItem(){ Text = "CREAME", Value = "CREAME"},
                };
                return r;
            }
        }

        public static List<SelectListItem> ListPilihan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Pasien", Value = "Pasien" });
                result.Add(new SelectListItem() { Text = "Keluarga", Value = "Keluarga" });
                result.Add(new SelectListItem() { Text = "Penanggung Jawab", Value = "Penanggung Jawab" });
                return result;
            }
        }

        public static List<SelectListItem> ListSiapayangedukasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Pasien", Value = "Pasien" });
                result.Add(new SelectListItem() { Text = "Ayah / Ibu", Value = "Ayah / Ibu" });
                result.Add(new SelectListItem() { Text = "Suami / Istri", Value = "Suami / Istri" });
                result.Add(new SelectListItem() { Text = "Anak", Value = "Anak" });
                return result;
            }
        }

        public static List<SelectListItem> ListMetode
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Diskusi", Value = "Diskusi" });
                result.Add(new SelectListItem() { Text = "Demonstrasi", Value = "Demonstrasi" });
                result.Add(new SelectListItem() { Text = "Leatlef", Value = "Leatlef" });
                result.Add(new SelectListItem() { Text = "Audio Visual", Value = "Audio Visual" });
                return result;
            }
        }

        public static List<SelectListItem> ListRespon
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak respon sama sekali(Tidak antusias dan keinginan belajar)", Value = "Tidak respon sama sekali(Tidak antusias dan keinginan belajar)" });
                result.Add(new SelectListItem() { Text = "Tidak Paham (ingin belajar tapi kesulitan mengerti)", Value = "Tidak Paham (ingin belajar tapi kesulitan mengerti)" });
                result.Add(new SelectListItem() { Text = "Paham hal yang diajarkan, tapi tidak bisa menjelaskan sendiri", Value = "Paham hal yang diajarkan, tapi tidak bisa menjelaskan sendiri" });
                result.Add(new SelectListItem() { Text = "Dapat menjelaskan apa yang telah diajarkan tapi harus dibantu educator", Value = "Dapat menjelaskan apa yang telah diajarkan tapi harus dibantu educator" });
                result.Add(new SelectListItem() { Text = "Dapat menjelaskan apa yang telah diajarkan tapi tanpa dibantu", Value = "Dapat menjelaskan apa yang telah diajarkan tapi tanpa dibantu" });
                return result;
            }
        }

        public static List<SelectListItem> ListBahasa
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Aktif", Value = "Aktif" });
                result.Add(new SelectListItem() { Text = "Pasif", Value = "Pasif" });
                return result;
            }
        }

        public static List<SelectListItem> ListBicara
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Serangan awal gangguan bicara", Value = "Serangan awal gangguan bicara" });
                return result;
            }
        }

        public static List<SelectListItem> ListTeraturTidakteratur
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Teratur", Value = "Teratur" });
                result.Add(new SelectListItem() { Text = "Tidak Teratur", Value = "Tidak Teratur" });
                return result;
            }
        }
        public static List<SelectListItem> ListDukunganSosial
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Suami", Value = "Suami" });
                result.Add(new SelectListItem() { Text = "Orang tua", Value = "Orang tua" });
                result.Add(new SelectListItem() { Text = "Keluarga", Value = "Keluarga" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListSumberData
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Pasien", Value = "Pasien" });
                result.Add(new SelectListItem() { Text = "Keluarga", Value = "Keluarga" });
                result.Add(new SelectListItem() { Text = "Teman", Value = "Teman" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListMemberi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Memberi", Value = "Memberi" });
                result.Add(new SelectListItem() { Text = "Tidak Memberi", Value = "Tidak Memberi" });
                return result;
            }
        }

        public static List<SelectListItem> ListPengobatanKepada
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Seluruh Keluarga", Value = "Seluruh Keluarga" });
                result.Add(new SelectListItem() { Text = "Keluarga Tertentu", Value = "Keluarga Tertentu" });
                return result;
            }
        }

        public static List<SelectListItem> ListMemiliki
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Memiliki", Value = "Tidak Memiliki" });
                result.Add(new SelectListItem() { Text = "Tidak Menggunakan", Value = "Tidak Menggunakan" });
                return result;
            }
        }

        public static List<SelectListItem> ListKendaraan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Ambulance", Value = "Ambulance" });
                result.Add(new SelectListItem() { Text = "Kendaraan Pribadi", Value = "Mulai Pribadi" });
                result.Add(new SelectListItem() { Text = "Kendaraan Lainnya", Value = "Kendaraan Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListKontrolResikoInfeksiStatus
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Diketahui", Value = "Tidak Diketahui" });
                result.Add(new SelectListItem() { Text = "Suspect", Value = "Suspect" });
                return result;
            }
        }

        public static List<SelectListItem> ListKategoriKasus
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Non Bedah", Value = "Non Bedah" });
                result.Add(new SelectListItem() { Text = "Bedah", Value = "Bedah" });
                result.Add(new SelectListItem() { Text = "Medis", Value = "Medis" });
                result.Add(new SelectListItem() { Text = "KLL", Value = "KLL" });
                result.Add(new SelectListItem() { Text = "K. Kerja", Value = "K. Kerja" });
                result.Add(new SelectListItem() { Text = "K. Olahraga", Value = "K. Olahraga" });
                return result;
            }
        }

        public static List<SelectListItem> Listcacad
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Cukup Sehat", Value = "Cukup Sehat" });
                result.Add(new SelectListItem() { Text = "Tidak Sehat", Value = "Tidak Sehat" });
                result.Add(new SelectListItem() { Text = "Cacad", Value = "Cacad" });
                return result;
            }
        }

        public static List<SelectListItem> ListAdaTidakKelainan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Ada", Value = "Ada" });
                result.Add(new SelectListItem() { Text = "Tidak Ada Kelainan", Value = "Tidak Ada Kelainan" });
                result.Add(new SelectListItem() { Text = "Tidak Dilakukan Pemeriksaan", Value = "Tidak Dilakukan Pemeriksaan" });
                return result;
            }
        }

        public static List<SelectListItem> ListSKIstirhat
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Istirahat", Value = "Istirahat" });
                result.Add(new SelectListItem() { Text = "Kerja Ringan", Value = "Kerja Ringan" });
                result.Add(new SelectListItem() { Text = "Bebas Olahraga", Value = "Bebas Olahraga" });
                result.Add(new SelectListItem() { Text = "Bebas Latihan", Value = "Bebas Latihan" });
                result.Add(new SelectListItem() { Text = "Bebas Dinas Malam", Value = "Bebas Dinas Malam" });
                result.Add(new SelectListItem() { Text = "Bebas Memakai Sepatu Karena Sakit", Value = "Bebas Memakai Sepatu Karena Sakit" });
                return result;
            }
        }

        public static List<SelectListItem> ListBayiLahir
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Hidup", Value = "Hidup" });
                result.Add(new SelectListItem() { Text = "Mati", Value = "Mati" });
                result.Add(new SelectListItem() { Text = "Lelaki", Value = "Lelaki" });
                result.Add(new SelectListItem() { Text = "Perempuan", Value = "Perempuan" });
                return result;
            }
        }

        public static List<SelectListItem> ListAsuransi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "JKN", Value = "JKN" });
                result.Add(new SelectListItem() { Text = "JKBM", Value = "JKBM" });
                result.Add(new SelectListItem() { Text = "Umum", Value = "Umum" });
                result.Add(new SelectListItem() { Text = "Umum", Value = "Umum" });
                result.Add(new SelectListItem() { Text = "DLL", Value = "DLL" });
                return result;
            }
        }

        public static List<SelectListItem> ListBidanDokterPerawat
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Dokter", Value = "Dokter" });
                result.Add(new SelectListItem() { Text = "Bidan", Value = "Bidan" });
                result.Add(new SelectListItem() { Text = "Perawat", Value = "Perawat" });
                return result;
            }
        }

        public static List<SelectListItem> ListMembedakanWarna
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Buta Warna Partial", Value = "Buta Warna Partial" });
                result.Add(new SelectListItem() { Text = "Buta Warna Total", Value = "Buta Warna Total" });
                return result;
            }
        }

        public static List<SelectListItem> ListStakesResume
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "1", Value = "1" });
                result.Add(new SelectListItem() { Text = "2", Value = "2" });
                result.Add(new SelectListItem() { Text = "3", Value = "3" });
                result.Add(new SelectListItem() { Text = "4", Value = "4" });
                return result;
            }
        }

        public static List<SelectListItem> ListJenisAnestesi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Umum", Value = "Umum" });
                result.Add(new SelectListItem() { Text = "Regional", Value = "Regional" });
                result.Add(new SelectListItem() { Text = "Sedasi", Value = "Sedasi" });
                result.Add(new SelectListItem() { Text = "Lokal", Value = "Lokal" });
                return result;
            }
        }

        public static List<SelectListItem> ListKlarifikasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Cito", Value = "Cito" });
                result.Add(new SelectListItem() { Text = "Elektif", Value = "Elektif" });
                return result;
            }
        }

        public static List<SelectListItem> ListGolonganOperasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Khusus", Value = "Khusus" });
                result.Add(new SelectListItem() { Text = "Besar", Value = "Besar" });
                result.Add(new SelectListItem() { Text = "Sedang", Value = "Sedang" });
                result.Add(new SelectListItem() { Text = "Kecil", Value = "Kecil" });
                return result;
            }
        }

        public static List<SelectListItem> ListStakes
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "1", Value = "1" });
                result.Add(new SelectListItem() { Text = "2", Value = "2" });
                result.Add(new SelectListItem() { Text = "3", Value = "3" });
                result.Add(new SelectListItem() { Text = "3P", Value = "3P" });
                return result;
            }
        }

        public static List<SelectListItem> ListCaraPembayaran
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Pribadi", Value = "Pribadi" });
                result.Add(new SelectListItem() { Text = "Asuransi", Value = "Asuransi" });
                result.Add(new SelectListItem() { Text = "BPJS", Value = "BPJS" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListAdaTidakAda
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Ada", Value = "Tidak Ada" });
                result.Add(new SelectListItem() { Text = "Ada", Value = "Ada" });
                return result;
            }
        }

        public static List<SelectListItem> ListHemo
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();       
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Cito", Value = "Cito" });
                result.Add(new SelectListItem() { Text = "Efektif", Value = "Efektif" });
                result.Add(new SelectListItem() { Text = "Pre-Operatif", Value = "Pre-Operatif" });
                return result;
            }
        }

        public static List<SelectListItem> ListDMpenyakit
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "DM", Value = "DM" });
                result.Add(new SelectListItem() { Text = "Kemoterapi", Value = "Kemoterapi" });
                result.Add(new SelectListItem() { Text = "Hemodialisa", Value = "Hemodialisa" });
                result.Add(new SelectListItem() { Text = "Penurunan Imunitas", Value = "Penurunan Imunitas" });
                result.Add(new SelectListItem() { Text = "Lain-Lain", Value = "Lain-Lain" });
                return result;
            }
        }

        public static List<SelectListItem> ListAgama
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Hindu", Value = "Hindu" });
                result.Add(new SelectListItem() { Text = "Islam", Value = "Islam" });
                result.Add(new SelectListItem() { Text = "Budha", Value = "Budha" });
                result.Add(new SelectListItem() { Text = "Kristen", Value = "Kristen" });
                result.Add(new SelectListItem() { Text = "Khatolik", Value = "Khatolik" });
                result.Add(new SelectListItem() { Text = "Kong Hu Cu", Value = "Kong Hu Cu" });
                return result;
            }
        }

        public static string ListSurat
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "AAAaaaAAA", Value = "AAAaaaAAA" },
                    new SelectListItem(){ Text = "AAAaaaAAA", Value = "AAAaaaAAA" },
                    new SelectListItem(){ Text = "AAAaaaAAA", Value = "AAAaaaAAA" },
                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static List<SelectListItem> ListAEWS_Respirasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "25", Value = "25" });
                result.Add(new SelectListItem() { Text = "21", Value = "21" });
                result.Add(new SelectListItem() { Text = "18", Value = "18" });
                result.Add(new SelectListItem() { Text = "15", Value = "15" });
                result.Add(new SelectListItem() { Text = "12", Value = "12" });
                result.Add(new SelectListItem() { Text = "9", Value = "9" });
                return result;
            }
        }

        public static List<SelectListItem> ListAEWS_Skala1
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "96", Value = "96" });
                result.Add(new SelectListItem() { Text = "94", Value = "94" });
                result.Add(new SelectListItem() { Text = "92", Value = "92" });
                return result;
            }
        }

        

        public static List<SelectListItem> ListJenisDializer
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Polyflux 1,7 m2", Value = "Polyflux 1,7 m2" });
                result.Add(new SelectListItem() { Text = "Revaclear 1,4 m2", Value = "Revaclear 1,4 m2" });
                result.Add(new SelectListItem() { Text = "B- Braun 1,6 m2", Value = "B- Braun 1,6 m2" });
                return result;
            }
        }

        public static List<SelectListItem> ListAEWS_Skala2
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "97cnO2", Value = "97cnO2" });
                result.Add(new SelectListItem() { Text = "95cnO2", Value = "95cnO2" });
                result.Add(new SelectListItem() { Text = "93cnO2", Value = "93cnO2" });
                result.Add(new SelectListItem() { Text = ">= 93cnO2", Value = ">= 93cnO2" });
                result.Add(new SelectListItem() { Text = "88", Value = "88" });
                result.Add(new SelectListItem() { Text = "86", Value = "86" });
                result.Add(new SelectListItem() { Text = "84", Value = "84" });
                return result;
            }
        }

        public static List<SelectListItem> ListAEWS_Udara
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Air", Value = "Air" });
                result.Add(new SelectListItem() { Text = "O2 L/min", Value = "O2 L/min" });
                result.Add(new SelectListItem() { Text = "Device", Value = "Device" });
                return result;
            }
        }

        public static List<SelectListItem> ListAEWS_TDSistolik
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "220", Value = "220" });
                result.Add(new SelectListItem() { Text = "201", Value = "201" });
                result.Add(new SelectListItem() { Text = "181", Value = "181" });
                result.Add(new SelectListItem() { Text = "161", Value = "161" });
                result.Add(new SelectListItem() { Text = "141", Value = "141" });
                result.Add(new SelectListItem() { Text = "121", Value = "121" });
                result.Add(new SelectListItem() { Text = "111", Value = "111" });
                result.Add(new SelectListItem() { Text = "101", Value = "101" });
                result.Add(new SelectListItem() { Text = "91", Value = "91" });
                result.Add(new SelectListItem() { Text = "81", Value = "81" });
                result.Add(new SelectListItem() { Text = "71", Value = "71" });
                result.Add(new SelectListItem() { Text = "61", Value = "61" });
                result.Add(new SelectListItem() { Text = "51", Value = "51" });
                result.Add(new SelectListItem() { Text = "41", Value = "41" });
                result.Add(new SelectListItem() { Text = "31", Value = "31" });
                result.Add(new SelectListItem() { Text = "30", Value = "30" });
                return result;
            }
        }

        public static List<SelectListItem> ListAEWS_Nadi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "131", Value = "131" });
                result.Add(new SelectListItem() { Text = "121", Value = "121" });
                result.Add(new SelectListItem() { Text = "111", Value = "111" });
                result.Add(new SelectListItem() { Text = "101", Value = "101" });
                result.Add(new SelectListItem() { Text = "91", Value = "91" });
                result.Add(new SelectListItem() { Text = "81", Value = "81" });
                result.Add(new SelectListItem() { Text = "71", Value = "71" });
                result.Add(new SelectListItem() { Text = "61", Value = "61" });
                result.Add(new SelectListItem() { Text = "51", Value = "51" });
                result.Add(new SelectListItem() { Text = "41", Value = "41" });
                result.Add(new SelectListItem() { Text = "31", Value = "31" });
                result.Add(new SelectListItem() { Text = "30", Value = "30" });
                return result;
            }
        }

        public static List<SelectListItem> ListAEWS_Kesadaran
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "A", Value = "A" });
                result.Add(new SelectListItem() { Text = "C", Value = "C" });
                result.Add(new SelectListItem() { Text = "V", Value = "V" });
                result.Add(new SelectListItem() { Text = "P", Value = "P" });
                result.Add(new SelectListItem() { Text = "U", Value = "U" });
                return result;
            }
        }

        public static List<SelectListItem> ListMasalahPerkawinan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Cerai", Value = "Cerai" });
                result.Add(new SelectListItem() { Text = "Istri Baru", Value = "Istri Baru" });
                result.Add(new SelectListItem() { Text = "Simpanan", Value = "Simpanan" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListStatusPerkawinan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Kawin", Value = "Kawin" });
                result.Add(new SelectListItem() { Text = "Belum Kawin", Value = "Belum Kawin" });
                result.Add(new SelectListItem() { Text = "Janda", Value = "Janda" });
                return result;
            }
        }

        public static List<SelectListItem> ListADL
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Mandiri", Value = "Mandiri" });
                result.Add(new SelectListItem() { Text = "Dibantu", Value = "Dibantu" });
                return result;
            }
        }

        public static List<SelectListItem> ListFrekuensi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "1x", Value = "1x" });
                result.Add(new SelectListItem() { Text = "2x", Value = "2x" });
                result.Add(new SelectListItem() { Text = "3x", Value = "3x" });
                return result;
            }
        } 

        public static List<SelectListItem> ListJumlahNikah
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "1x", Value = "1x" });
                result.Add(new SelectListItem() { Text = "2x", Value = "2x" });
                result.Add(new SelectListItem() { Text = ">2x", Value = ">2x" });
                return result;
            }
        }

        public static List<SelectListItem> ListMenerimaTidak
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Menerima", Value = "Menerima" });
                result.Add(new SelectListItem() { Text = "Tidak Menerima", Value = "Tidak Menerima" });
                return result;
            }
        }

        public static List<SelectListItem> ListHidupMati
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Hidup", Value = "Hidup" });
                result.Add(new SelectListItem() { Text = "Mati", Value = "Mati" });
                return result;
            }
        }

        public static List<SelectListItem> ListKebutuhanSosial
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "single", Value = "single" });
                result.Add(new SelectListItem() { Text = "Menikah", Value = "Menikah" });
                result.Add(new SelectListItem() { Text = "Bercerai", Value = "Bercerai" });
                result.Add(new SelectListItem() { Text = "Janda/Duda", Value = "Janda/Duda" });
                return result;
            }
        }

        public static List<SelectListItem> ListKonjunctiva
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Pucat", Value = "Pucat" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                return result;
            }
        }

        public static List<SelectListItem> ListSclera
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Putih", Value = "Putih" });
                result.Add(new SelectListItem() { Text = "Kuning", Value = "Kuning" });
                result.Add(new SelectListItem() { Text = "Merah", Value = "Merah" });
                return result;
            }
        }

        public static List<SelectListItem> ListBentuk
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Simetris", Value = "Simetris" });
                result.Add(new SelectListItem() { Text = "Asimetris", Value = "Asimetris" });
                return result;
            }
        }

        public static List<SelectListItem> ListPutingSusu
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Menonjol", Value = "Menonjol" });
                result.Add(new SelectListItem() { Text = "Datar", Value = "Datar" });
                result.Add(new SelectListItem() { Text = "Masuk", Value = "Masuk" });
                return result;
            }
        }
        public static List<SelectListItem> ListPengeluaran
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Colostrum", Value = "Colostrum" });
                result.Add(new SelectListItem() { Text = "ASI", Value = "ASI" });
                result.Add(new SelectListItem() { Text = "Nanah", Value = "Nanah" });
                result.Add(new SelectListItem() { Text = "Darah", Value = "Darah" });
                return result;
            }
        }

        public static List<SelectListItem> ListKelainan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Lecet", Value = "Lecet" });
                result.Add(new SelectListItem() { Text = "Bengkak", Value = "Bengkak" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListTerabaTidak
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Teraba", Value = "Tidak Teraba" });
                result.Add(new SelectListItem() { Text = "Teraba", Value = "Teraba" });
                return result;
            }
        }

        public static List<SelectListItem> ListTerabaNormal
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Teraba", Value = "Teraba" });
                return result;
            }
        }

        public static List<SelectListItem> ListNegatifMinimalPermagna
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Negatif", Value = "Negatif" });
                result.Add(new SelectListItem() { Text = "Minimal", Value = "Minimal" });
                result.Add(new SelectListItem() { Text = "Permagna", Value = "Permagna" });
                return result;
            }
        }

        public static List<SelectListItem> ListLetakPunggung
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Punggung Kanan", Value = "Punggung Kanan" });
                result.Add(new SelectListItem() { Text = "Punggung Kiri", Value = "Punggung Kiri" });
                return result;
            }
        }

        public static List<SelectListItem> ListBaikLembek
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Baik", Value = "Baik" });
                result.Add(new SelectListItem() { Text = "Lembek", Value = "Lembek" });
                return result;
            }
        }

        public static List<SelectListItem> ListSolidKistik
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Solid", Value = "Solid" });
                result.Add(new SelectListItem() { Text = "Kistik", Value = "Kistik" });
                return result;
            }
        }

        public static List<SelectListItem> ListPresentasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Kepala", Value = "Kepala" });
                result.Add(new SelectListItem() { Text = "Bokong", Value = "Bokong" });
                result.Add(new SelectListItem() { Text = "Kosong", Value = "Kosong" });
                return result;
            }
        }

        public static List<SelectListItem> ListKelainanAda
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Bandl", Value = "Bandl" });
                result.Add(new SelectListItem() { Text = "Distensi", Value = "Distensi" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListMinPlus
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "+", Value = "+" });
                result.Add(new SelectListItem() { Text = "-", Value = "-" });

                return result;
            }
        }

        public static List<SelectListItem> ListCukupKurang
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Cukup", Value = "Cukup" });
                result.Add(new SelectListItem() { Text = "Kurang", Value = "Kurang" });
                return result;
            }
        }

        public static List<SelectListItem> ListPendidikan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Belum Sekolah", Value = "Belum Sekolah" });
                result.Add(new SelectListItem() { Text = "SD", Value = "SD" });
                result.Add(new SelectListItem() { Text = "SMP", Value = "SMP" });
                result.Add(new SelectListItem() { Text = "SMA", Value = "SMA" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListWargaNegara
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "WNI", Value = "WNI" });
                result.Add(new SelectListItem() { Text = "WNA", Value = "WNA" });
                return result;
            }
        }

        public static List<SelectListItem> ListAnemisNormal
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Anemis", Value = "Anemis" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                return result;
            }
        }

        public static List<SelectListItem> ListNormalMelebar
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Melebar", Value = "Melebar" });
                return result;
            }
        }

        public static List<SelectListItem> ListIcterisNormal
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Icteris", Value = "Icteris" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                return result;
            }
        }

        public static List<SelectListItem> ListIsocoreAnisocore
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Isocore", Value = "Isocore" });
                result.Add(new SelectListItem() { Text = "Anisocore", Value = "Anisocore" });
                return result;
            }
        }

        public static List<SelectListItem> ListSistolikDiastolik
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Sistolik", Value = "Sistolik" });
                result.Add(new SelectListItem() { Text = "Diastolik", Value = "Diastolik" });
                return result;
            }
        }

        public static List<SelectListItem> ListPekerjaan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "PNS", Value = "PNS" });
                result.Add(new SelectListItem() { Text = "Swasta", Value = "Swasta" });
                result.Add(new SelectListItem() { Text = "TNI/Polri", Value = "TNI/Polri" });
                result.Add(new SelectListItem() { Text = "Tidak Bekerja", Value = "Tidak Bekerja" });
                return result;
            }
        }

        public static List<SelectListItem> ListPekerjaanPoliUmum
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "PNS", Value = "PNS" });
                result.Add(new SelectListItem() { Text = "WIrausaha", Value = "WIrausaha" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListAgamaPoliUmum
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Islam", Value = "Islam" });
                result.Add(new SelectListItem() { Text = "Hindu", Value = "Hindu" });
                result.Add(new SelectListItem() { Text = "Katolik", Value = "Katolik" });
                result.Add(new SelectListItem() { Text = "Budha", Value = "Budha" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListSifat
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Akut", Value = "Akut" });
                result.Add(new SelectListItem() { Text = "Kronis", Value = "Kronis" });
                return result;
            }
        }

        public static List<SelectListItem> ListTinggalBersama
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Suami/Istri", Value = "Suami/Istri" });
                result.Add(new SelectListItem() { Text = "Anak", Value = "Anak" });
                result.Add(new SelectListItem() { Text = "Orangtua", Value = "Orangtua" });
                result.Add(new SelectListItem() { Text = "Sendiri", Value = "Sendiri" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListIbadahKegiatan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Selalu", Value = "Selalu" });
                result.Add(new SelectListItem() { Text = "Kadang", Value = "Kadang" });
                result.Add(new SelectListItem() { Text = "Tidak Pernah", Value = "Tidak Pernah" });
                return result;
            }
        }

        public static List<SelectListItem> ListPernahTidakPernah
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Pernah", Value = "Pernah" });
                result.Add(new SelectListItem() { Text = "Tidak Pernah", Value = "Tidak Pernah" });
                return result;
            }
        }

        public static List<SelectListItem> ListPerkawinan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Cerai", Value = "Cerai" });
                result.Add(new SelectListItem() { Text = "Istri Baru", Value = "Istri Baru" });
                result.Add(new SelectListItem() { Text = "Simpanan", Value = "Simpanan" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListAdaKontrasepsi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Menggunakan", Value = "Tidak Menggunakan" });
                result.Add(new SelectListItem() { Text = "Menggunakan", Value = "Menggunakan" });
                return result;
            }
        }


        public static List<SelectListItem> ListStatus
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Belum Kawin", Value = "Belum Kawin" });
                result.Add(new SelectListItem() { Text = "Cerai", Value = "Cerai" });
                result.Add(new SelectListItem() { Text = "Kawin", Value = "Kawin" });
                return result;
            }
        }


        public static List<SelectListItem> ListAEWS_Temperatur
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "39,1", Value = "39,1" });
                result.Add(new SelectListItem() { Text = "38,1", Value = "38,1" });
                result.Add(new SelectListItem() { Text = "37,1", Value = "37,1" });
                result.Add(new SelectListItem() { Text = "36,1", Value = "36,1" });
                result.Add(new SelectListItem() { Text = "35,1", Value = "35,1" });
                return result;
            }
        }

        public static List<SelectListItem> ListMEWS_Respirasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "35", Value = "35" });
                result.Add(new SelectListItem() { Text = "30", Value = "30" });
                result.Add(new SelectListItem() { Text = "25", Value = "25" });
                result.Add(new SelectListItem() { Text = "20", Value = "20" });
                result.Add(new SelectListItem() { Text = "15", Value = "15" });
                result.Add(new SelectListItem() { Text = "10", Value = "10" });
                result.Add(new SelectListItem() { Text = "5", Value = "5" });
                return result;
            }
        }

        public static List<SelectListItem> ListMEWS_TDSistolik
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "201", Value = "201" });
                result.Add(new SelectListItem() { Text = "200", Value = "200" });
                result.Add(new SelectListItem() { Text = "190", Value = "190" });
                result.Add(new SelectListItem() { Text = "180", Value = "180" });
                result.Add(new SelectListItem() { Text = "170", Value = "170" });
                result.Add(new SelectListItem() { Text = "160", Value = "160" });
                result.Add(new SelectListItem() { Text = "150", Value = "150" });
                result.Add(new SelectListItem() { Text = "140", Value = "140" });
                result.Add(new SelectListItem() { Text = "130", Value = "130" });
                result.Add(new SelectListItem() { Text = "120", Value = "120" });
                result.Add(new SelectListItem() { Text = "110", Value = "110" });
                result.Add(new SelectListItem() { Text = "100", Value = "100" });
                result.Add(new SelectListItem() { Text = "90", Value = "90" });
                result.Add(new SelectListItem() { Text = "80", Value = "80" });
                result.Add(new SelectListItem() { Text = "70", Value = "70" });
                result.Add(new SelectListItem() { Text = "60", Value = "60" });
                result.Add(new SelectListItem() { Text = "50", Value = "50" });
                result.Add(new SelectListItem() { Text = "40", Value = "40" });
                result.Add(new SelectListItem() { Text = "30", Value = "30" });
                return result;
            }
        }

        public static List<SelectListItem> RiwayatOpname
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                result.Add(new SelectListItem() { Text = "Ya", Value = "Ya" });
                return result;
            }
        }
        public static List<SelectListItem> RiwayatOperasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                result.Add(new SelectListItem() { Text = "Ya", Value = "Ya" });
                return result;
            }
        }
        public static List<SelectListItem> Femoral
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                result.Add(new SelectListItem() { Text = "Ya", Value = "Ya" });
                return result;
            }
        }

        public static List<SelectListItem> RiwayatAlergi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                result.Add(new SelectListItem() { Text = "Ya", Value = "Ya" });
                return result;
            }
        }

        public static List<SelectListItem> AsupanMakananDirumah
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Kurang", Value = "Kurang" });
                result.Add(new SelectListItem() { Text = "Baik", Value = "Baik" });
                return result;
            }
        }

        public static List<SelectListItem> Makan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "0-1 kali", Value = "0-1 kali" });
                result.Add(new SelectListItem() { Text = "1-2 kali", Value = "1-2 kali" });
                result.Add(new SelectListItem() { Text = "2-3 kali", Value = "2-3 kali" });
                return result;
            }
        }


        public static List<SelectListItem> AsupanMakananDiRs
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Kurang", Value = "Kurang" });
                result.Add(new SelectListItem() { Text = "Baik", Value = "Baik" });
                return result;
            }
        }

        public static List<SelectListItem> PantanganMakan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Ya", Value = "Ya" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                return result;
            }
        }

        public static List<SelectListItem> SuplementGizi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tanpa Suplemen Gizi", Value = "Tanpa Suplemen Gizi" });
                result.Add(new SelectListItem() { Text = "Dengan Suplemen Gizi", Value = "Dengan Suplemen Gizi" });
                return result;
            }
        }

        public static List<SelectListItem> ListMEWS_Nadi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "130", Value = "130" });
                result.Add(new SelectListItem() { Text = "120", Value = "120" });
                result.Add(new SelectListItem() { Text = "110", Value = "110" });
                result.Add(new SelectListItem() { Text = "105", Value = "105" });
                result.Add(new SelectListItem() { Text = "100", Value = "100" });
                result.Add(new SelectListItem() { Text = "90", Value = "90" });
                result.Add(new SelectListItem() { Text = "80", Value = "80" });
                result.Add(new SelectListItem() { Text = "75", Value = "75" });
                result.Add(new SelectListItem() { Text = "70", Value = "70" });
                result.Add(new SelectListItem() { Text = "60", Value = "60" });
                result.Add(new SelectListItem() { Text = "50", Value = "50" });
                result.Add(new SelectListItem() { Text = "40", Value = "40" });
                return result;
            }
        }

        public static List<SelectListItem> ListMEWS_Temperatur
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "39,5", Value = "39,5" });
                result.Add(new SelectListItem() { Text = "39", Value = "39" });
                result.Add(new SelectListItem() { Text = "38,5", Value = "38,5" });
                result.Add(new SelectListItem() { Text = "38", Value = "38" });
                result.Add(new SelectListItem() { Text = "37,5", Value = "37,5" });
                result.Add(new SelectListItem() { Text = "37", Value = "37" });
                result.Add(new SelectListItem() { Text = "36", Value = "36" });
                result.Add(new SelectListItem() { Text = "35", Value = "35" });
                return result;
            }
        }

        public static List<SelectListItem> ListMEWS_SPO2
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = ">= 95", Value = ">= 95" });
                result.Add(new SelectListItem() { Text = "90-94", Value = "90-94" });
                result.Add(new SelectListItem() { Text = "88-89", Value = "88-89" });
                result.Add(new SelectListItem() { Text = "< 88", Value = "< 88" });
                return result;
            }
        }

        public static List<SelectListItem> ListMEWS_Kesadaran
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Alert", Value = "Alert" });
                result.Add(new SelectListItem() { Text = "Voice", Value = "Voice" });
                result.Add(new SelectListItem() { Text = "(Confuses/Agitated)", Value = "(Confuses/Agitated)" });
                result.Add(new SelectListItem() { Text = "(Unconsious/Pain)", Value = "(Unconsious/Pain)" });
                return result;
            }
        }

        public static List<SelectListItem> ListMEWS_Urine
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = ">= 30 cc/jam", Value = ">= 30 cc/jam" });
                result.Add(new SelectListItem() { Text = "<= 50 ml/2 jam", Value = "<= 50 ml/2 jam" });
                result.Add(new SelectListItem() { Text = "<= 100 ml/4 jam", Value = "<= 100 ml/4 jam" });
                result.Add(new SelectListItem() { Text = "<= 10 ml/jam", Value = "<= 10 ml/jam" });
                return result;
            }
        }

        public static List<SelectListItem> ListMEWS_TanpaKateter
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "BAK spontan", Value = "BAK spontan" });
                result.Add(new SelectListItem() { Text = "4 jam tanpa produksi urine dan kandung kemih kosong", Value = "4 jam tanpa produksi urine dan kandung kemih kosong" });
                result.Add(new SelectListItem() { Text = "8 jam tanpa produksi urine dan kandung kemih kosong", Value = "8 jam tanpa produksi urine dan kandung kemih kosong" });
                result.Add(new SelectListItem() { Text = "10 jam tanpa produksi urine", Value = "10 jam tanpa produksi urine" });
                return result;
            }
        }

        public static List<SelectListItem> ListPEWS_Pernafasan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "80", Value = "80" });
                result.Add(new SelectListItem() { Text = "70", Value = "70" });
                result.Add(new SelectListItem() { Text = "60", Value = "60" });
                result.Add(new SelectListItem() { Text = "50", Value = "50" });
                result.Add(new SelectListItem() { Text = "40", Value = "40" });
                result.Add(new SelectListItem() { Text = "30", Value = "30" });
                result.Add(new SelectListItem() { Text = "20", Value = "20" });
                result.Add(new SelectListItem() { Text = "10", Value = "10" });
                result.Add(new SelectListItem() { Text = "< 10", Value = "< 10" });
                return result;
            }
        }

        public static List<SelectListItem> ListIntensitas
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "0", Value = "0" });
                result.Add(new SelectListItem() { Text = "1", Value = "1" });
                result.Add(new SelectListItem() { Text = "2", Value = "2" });
                result.Add(new SelectListItem() { Text = "3", Value = "3" });
                result.Add(new SelectListItem() { Text = "4", Value = "4" });
                result.Add(new SelectListItem() { Text = "5", Value = "5" });
                result.Add(new SelectListItem() { Text = "6", Value = "6" });
                result.Add(new SelectListItem() { Text = "7", Value = "7" });
                result.Add(new SelectListItem() { Text = "8", Value = "8" });
                result.Add(new SelectListItem() { Text = "9", Value = "9" });
                result.Add(new SelectListItem() { Text = "10", Value = "10" });
                return result;
            }
        }

        public static List<SelectListItem> ListFarmakologi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "1: Dingin", Value = "1" });
                result.Add(new SelectListItem() { Text = "2: Panas", Value = "2" });
                result.Add(new SelectListItem() { Text = "3: Posisi", Value = "3" });
                result.Add(new SelectListItem() { Text = "4: Pijat", Value = "4" });
                result.Add(new SelectListItem() { Text = "5: Musik", Value = "5" });
                result.Add(new SelectListItem() { Text = "6: TENS", Value = "6" });
                result.Add(new SelectListItem() { Text = "7: Relaksasi dan Pernafasan", Value = "7" });
                return result;
            }
        }

        public static List<SelectListItem> ListWaktuKajiUlang
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "30 Menit  setelah intervensi obat injeksi", Value = "30 Menit  setelah intervensi obat injeksi" });
                result.Add(new SelectListItem() { Text = "1 jam setelah intervensi obat oral/lainnya", Value = "1 jam setelah intervensi obat oral/lainnya" });
                result.Add(new SelectListItem() { Text = "1x /shirt bila skor nyeri 1-3", Value = "1x /shirt bila skor nyeri 1-3" });
                result.Add(new SelectListItem() { Text = "Setiap 3 jam bila skor nyeri 4-6", Value = "Setiap 3 jam bila skor nyeri 4-6" });
                result.Add(new SelectListItem() { Text = "Setiap 1 jam bila skor nyeri 7-10", Value = "Setiap 1 jam bila skor nyeri 7-10" });
                result.Add(new SelectListItem() { Text = "Dihentikan bila skor nyeri 0", Value = "Dihentikan bila skor nyeri 0" });
                return result;
            }
        }

        public static List<SelectListItem> ListPEWS_Sirkulasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "> 180", Value = "> 180" });
                result.Add(new SelectListItem() { Text = "170", Value = "170" });
                result.Add(new SelectListItem() { Text = "160", Value = "160" });
                result.Add(new SelectListItem() { Text = "150", Value = "150" });
                result.Add(new SelectListItem() { Text = "140", Value = "140" });
                result.Add(new SelectListItem() { Text = "130", Value = "130" });
                result.Add(new SelectListItem() { Text = "120", Value = "120" });
                result.Add(new SelectListItem() { Text = "110", Value = "110" });
                result.Add(new SelectListItem() { Text = "110", Value = "110" });
                result.Add(new SelectListItem() { Text = "100", Value = "100" });
                result.Add(new SelectListItem() { Text = "95", Value = "95" });
                result.Add(new SelectListItem() { Text = "90", Value = "90" });
                result.Add(new SelectListItem() { Text = "85", Value = "85" });
                result.Add(new SelectListItem() { Text = "80", Value = "80" });
                result.Add(new SelectListItem() { Text = "75", Value = "75" });
                result.Add(new SelectListItem() { Text = "70", Value = "70" });
                result.Add(new SelectListItem() { Text = "65", Value = "65" });
                result.Add(new SelectListItem() { Text = "60", Value = "60" });
                result.Add(new SelectListItem() { Text = "55", Value = "55" });
                result.Add(new SelectListItem() { Text = "50", Value = "50" });
                result.Add(new SelectListItem() { Text = "< 45", Value = "< 45" });
                return result;
            }
        }

        public static List<SelectListItem> ListNEWS_Respirasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "85", Value = "85" });
                result.Add(new SelectListItem() { Text = "80", Value = "80" });
                result.Add(new SelectListItem() { Text = "75", Value = "75" });
                result.Add(new SelectListItem() { Text = "70", Value = "70" });
                result.Add(new SelectListItem() { Text = "65", Value = "65" });
                result.Add(new SelectListItem() { Text = "60", Value = "60" });
                result.Add(new SelectListItem() { Text = "55", Value = "55" });
                result.Add(new SelectListItem() { Text = "50", Value = "50" });
                result.Add(new SelectListItem() { Text = "45", Value = "45" });
                result.Add(new SelectListItem() { Text = "40", Value = "40" });
                result.Add(new SelectListItem() { Text = "35", Value = "35" });
                result.Add(new SelectListItem() { Text = "30", Value = "30" });
                result.Add(new SelectListItem() { Text = "25", Value = "25" });
                result.Add(new SelectListItem() { Text = "20", Value = "20" });
                
                return result;
            }
        }

        public static List<SelectListItem> ListNEWS_Warna
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Merah Muda (>94%)", Value = "Merah Muda (>94%)" });
                result.Add(new SelectListItem() { Text = "90-94%", Value = "90-94%" });
                result.Add(new SelectListItem() { Text = "Biru / Kehitaman (<90%)", Value = "Biru / Kehitaman (<90%)" });
                
                return result;
            }
        }

        public static List<SelectListItem> ListNEWS_Nadi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "200", Value = "200" });
                result.Add(new SelectListItem() { Text = "190", Value = "190" });
                result.Add(new SelectListItem() { Text = "180", Value = "180" });
                result.Add(new SelectListItem() { Text = "170", Value = "170" });
                result.Add(new SelectListItem() { Text = "160", Value = "160" });
                result.Add(new SelectListItem() { Text = "150", Value = "150" });
                result.Add(new SelectListItem() { Text = "140", Value = "140" });
                result.Add(new SelectListItem() { Text = "130", Value = "130" });
                result.Add(new SelectListItem() { Text = "120", Value = "120" });
                result.Add(new SelectListItem() { Text = "110", Value = "110" });
                result.Add(new SelectListItem() { Text = "100", Value = "100" });
                result.Add(new SelectListItem() { Text = "90", Value = "90" });
                result.Add(new SelectListItem() { Text = "80", Value = "80" });
                result.Add(new SelectListItem() { Text = "70", Value = "70" });
                result.Add(new SelectListItem() { Text = "60", Value = "60" });
                return result;
            }
        }

        public static List<SelectListItem> ListNEWS_Neuro
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Refleks Hisap Aktif", Value = "Refleks Hisap Aktif" });
                result.Add(new SelectListItem() { Text = "Gelisah / Rewel", Value = "Gelisah / Rewel" });
                result.Add(new SelectListItem() { Text = "Lungla / Sulit Dibangunkan", Value = "Lungla / Sulit Dibangunkan" });
                result.Add(new SelectListItem() { Text = "Kejang", Value = "Kejang" });
                
                return result;
            }
        }

        public static List<SelectListItem> ListNEWS_Temperatur
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "39,0", Value = "39,0" });
                result.Add(new SelectListItem() { Text = "38.0", Value = "38.0" });
                result.Add(new SelectListItem() { Text = "37,0", Value = "37,0" });
                result.Add(new SelectListItem() { Text = "36,0", Value = "36,0" });
                result.Add(new SelectListItem() { Text = "35,0", Value = "35,0" });
                return result;
            }
        }

        public static List<SelectListItem> ListNEWS_Skor
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Merah", Value = "Merah" });
                result.Add(new SelectListItem() { Text = "Kuning", Value = "Kuning" });
                return result;
            }
        }

        public static List<SelectListItem> ListSkor_Sedasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "1: Bangun dan Sadar", Value = "1" });
                result.Add(new SelectListItem() { Text = "2: Agak Mengantuk, Mudah dibangunkan", Value = "2" });
                result.Add(new SelectListItem() { Text = "3: Sering mengantuk, bisa dibangunkan, mudah tertidur saat sedang bicara", Value = "3" });
                result.Add(new SelectListItem() { Text = "4: Somnolent, minimal/tidak respon terhadap rangsangan fisik", Value = "4" });
                result.Add(new SelectListItem() { Text = "5: Tidur, mudah dibangunkan", Value = "5" });
                return result;
            }
        }

        public static List<SelectListItem> ListIramaNafas
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Teratur", Value = "Teratur" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                return result;
            }
        }

        public static List<SelectListItem> ListPernahTIdakPernah
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Pernah", Value = "Pernah" });
                result.Add(new SelectListItem() { Text = "Tidak Pernah", Value = "Tidak Pernah" });
                return result;
            }
        }

        public static List<SelectListItem> ListNadiiii
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Teratur", Value = "Teratur" });
                result.Add(new SelectListItem() { Text = "Tidak Teratur", Value = "Tidak Teratur" });
                return result;
            }
        }

        public static List<SelectListItem> Listbantunafas
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Retraksi", Value = "Retraksi" });
                result.Add(new SelectListItem() { Text = "Cuping hidung", Value = "Cuping hidung" });
                return result;
            }
        }

        public static List<SelectListItem> ListPerluTidakPerlu
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Perlu", Value = "Perlu" });
                result.Add(new SelectListItem() { Text = "Tidak Perlu", Value = "Tidak Perlu" });
                return result;
            }
        }

        public static List<SelectListItem> ListJenisPernafasan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Konsumsi", Value = "Konsumsi" });
                result.Add(new SelectListItem() { Text = "Diet", Value = "Diet" });
                return result;
            }
        }

        public static List<SelectListItem> List_Jenis
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Akut", Value = "Akut" });
                result.Add(new SelectListItem() { Text = "Kronis", Value = "Kronis" });
                return result;
            }
        }

        public static List<SelectListItem> ListNyeri
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Jarang", Value = "Jarang" });
                result.Add(new SelectListItem() { Text = "Hilang Timbul", Value = "Hilang Timbul" });
                result.Add(new SelectListItem() { Text = "Terus Menerus", Value = "Terus Menerus" });
                return result;
            }
        }

        public static List<SelectListItem> ListKualitasNyeri
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Nyeri Tajam", Value = "Nyeri Tajam" });
                result.Add(new SelectListItem() { Text = "Nyeri Tumpul", Value = "Nyeri Tumpul" });
                result.Add(new SelectListItem() { Text = "Rasa Panas/Terkabar", Value = "Rasa Panas/Terkabar" });
                return result;
            }
        }

        public static List<SelectListItem> Listfungsional
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "0", Value = "Mandiri" });
                result.Add(new SelectListItem() { Text = "1", Value = "Dibantu dengan air" });
                result.Add(new SelectListItem() { Text = "2", Value = "Dibantu orang lain" });
                result.Add(new SelectListItem() { Text = "3", Value = "Dibantu alat dan orang lain" });
                result.Add(new SelectListItem() { Text = "4", Value = "ketergantungan parnah" });
                return result;
            }
        }

        public static List<SelectListItem> ListAkral
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Hangat", Value = "Hangat" });
                result.Add(new SelectListItem() { Text = "Dingin", Value = "Dingin" });
                return result;
            }
        }

        public static List<SelectListItem> ListTemperaturKulit
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Hangat", Value = "Hangat" });
                result.Add(new SelectListItem() { Text = "Panas", Value = "Panas" });
                result.Add(new SelectListItem() { Text = "Dingin", Value = "Dingin" });
                return result;
            }
        }

        public static List<SelectListItem> ListGambaranKulit
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Kering", Value = "Kering" });
                result.Add(new SelectListItem() { Text = "Lembab/Basah", Value = "Lembab/Basah" });
                return result;
            }
        }

        public static List<SelectListItem> ListNormalMeningakatMenurun
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Meningkat", Value = "Meningkat" });
                result.Add(new SelectListItem() { Text = "Menurun", Value = "Menurun" });
                return result;
            }
        }

        public static List<SelectListItem> ListTergangguTidakTerganggu
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Terganggu", Value = "Terganggu" });
                result.Add(new SelectListItem() { Text = "Tidak Terganggu", Value = "Tidak Terganggu" });
                result.Add(new SelectListItem() { Text = "Menurun", Value = "Menurun" });
                return result;
            }
        }

        public static List<SelectListItem> ListPengisianKapiler
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "<2 Detik", Value = "<2 Detik" });
                result.Add(new SelectListItem() { Text = ">2 Detik", Value = ">2 Detik" });
                return result;
            }
        }

        public static List<SelectListItem> ListNadi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Teraba", Value = "Teraba" });
                result.Add(new SelectListItem() { Text = "Tidak Teraba", Value = "Tidak Teraba" });
                return result;
            }
        }

        public static List<SelectListItem> ListIsi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Kuat", Value = "Kuat" });
                result.Add(new SelectListItem() { Text = "Lemah", Value = "Lemah" });
                return result;
            }
        }

        public static List<SelectListItem> ListTurgor
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Kurang", Value = "Kurang" });
                return result;
            }
        }

        public static List<SelectListItem> ListNutrisi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Mual", Value = "Mual" });
                result.Add(new SelectListItem() { Text = "Muntah", Value = "Muntah" });
                return result;
            }
        }

        public static List<SelectListItem> ListEdema
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Ada", Value = "Ada" });
                result.Add(new SelectListItem() { Text = "Tidak Ada", Value = "Tidak Ada" });
                return result;
            }
        }

        public static List<SelectListItem> ListPupil
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Isokor", Value = "Isokor" });
                result.Add(new SelectListItem() { Text = "An Isokor", Value = "An Isokor" });
                return result;
            }
        }

        public static List<SelectListItem> ListLuteralisasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Ada", Value = "Ada" });
                result.Add(new SelectListItem() { Text = "Tidak Ada", Value = "Tidak Ada" });
                return result;
            }
        }

        public static List<SelectListItem> ListRTrauma
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "+", Value = "+" });
                result.Add(new SelectListItem() { Text = "-", Value = "-" });
                return result;
            }
        }

        public static List<SelectListItem> ListYadansudahTidak
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Ya dan sudah direncanakan pemasangan infus line", Value = "Ya dan sudah direncanakan pemasangan infus line" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                return result;
            }
        }

        public static List<SelectListItem> ListYaTidak
        {
            get 
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Ya", Value = "Ya" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                return result;
            }
        }

        public static List<SelectListItem> ListOKTidak
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "OK", Value = "OK" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                return result;
            }
        }

        public static List<SelectListItem> ListJenisVaskuler
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Aneurisma", Value = "Aneurisma" });
                result.Add(new SelectListItem() { Text = "Stenosis", Value = "Stenosis" });
                result.Add(new SelectListItem() { Text = "Infeksi exit site", Value = "Infeksi exit site" });
                result.Add(new SelectListItem() { Text = "Infeksi Tunnel", Value = "Infeksi Tunnel" });
                result.Add(new SelectListItem() { Text = "Lain lain", Value = "Lain lain" });
                return result;
            }
        }

        public static List<SelectListItem> ListEvaluasiDialisis
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Pasien Hemodialisis", Value = "Pasien Hemodialisis" });
                result.Add(new SelectListItem() { Text = "Pasien Peritoneal dialisis", Value = "Pasien Peritoneal dialisis" });
                return result;
            }
        }

        public static List<SelectListItem> ListBaikBerkomplikasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Baik", Value = "Baik" });
                result.Add(new SelectListItem() { Text = "Berkomplikasi", Value = "Berkomplikasi" });
                return result;
            }
        }

        public static List<SelectListItem> LisEvaluasiHD
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Hemodialisis", Value = "Hemodialisis" });
                result.Add(new SelectListItem() { Text = "Peritoneal dialisis", Value = "Peritoneal dialisis" });
                return result;
            }
        }

        public static List<SelectListItem> ListBanyakCairan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "< 3 cangkir", Value = "< 3 cangkir" });
                result.Add(new SelectListItem() { Text = "3-5 cangkir", Value = "3-5 cangkir" });
                result.Add(new SelectListItem() { Text = "> 5 cangkir", Value = "> 5 cangkir" });
                return result;
            }
        }

        public static List<SelectListItem> ListPositifNegatif
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Positif", Value = "Positif" });
                result.Add(new SelectListItem() { Text = "Negatif", Value = "Negatif" });
                return result;
            }
        }

        public static List<SelectListItem> ListAlatTansport
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Brancard", Value = "Brancard" });
                result.Add(new SelectListItem() { Text = "Kursi Roda", Value = "Kursi Roda" });
                return result;
            }
        }

        public static List<SelectListItem> ListPendamping
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Paramedis", Value = "Paramedis" });
                result.Add(new SelectListItem() { Text = "care giver/POS", Value = "care giver/POS" });
                return result;
            }
        }

        public static List<SelectListItem> ListJenisMembranFlux
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "High Flux", Value = "High Flux" });
                result.Add(new SelectListItem() { Text = "Low Flux", Value = "Low Flux" });
                return result;
            }
        }

        public static List<SelectListItem> ListAlarmTest
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "OK", Value = "OK" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                return result;
            }
        }
        public static List<SelectListItem> ListMandiriDibantuKetergantungan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Mandiri", Value = "Mandiri" });
                result.Add(new SelectListItem() { Text = "Dibantu", Value = "Dibantu" });
                result.Add(new SelectListItem() { Text = "Ketergantungan Penuh", Value = "Ketergantungan Penuh" });
                return result;
            }
        }

        public static List<SelectListItem> ListYaTidakTidakYakin
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                result.Add(new SelectListItem() { Text = "Tidak yakin (tidak ada tanda baju menjadi lebih longgar, tidak tampak lebih kurus)", Value = "Tidak yakin (tidak ada tanda baju menjadi lebih longgar, tidak tampak lebih kurus)" });
                result.Add(new SelectListItem() { Text = "Tidak yakin (ada tanda baju menjadi lebih longgar, tampak lebih kurus)", Value = "Tidak yakin (ada tanda baju menjadi lebih longgar, tampak lebih kurus)" });
                result.Add(new SelectListItem() { Text = "Ya, ada penurunan berat badan sebanyak", Value = "Ya, ada penurunan berat badan sebanyak" });
                return result;
            }
        }

        public static List<SelectListItem> ListBeratSkrining
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "1-5 kg", Value = "1-5 kg" });
                result.Add(new SelectListItem() { Text = "6-10 kg", Value = "6-10 kg" });
                result.Add(new SelectListItem() { Text = "11-15 kg", Value = "11-15 kg" });
                result.Add(new SelectListItem() { Text = "> 15 kg", Value = "> 15 kg" });
                return result;
            }
        }

        public static List<SelectListItem> ListTeknikAnestesi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Sedasi", Value = "Sedasi" });
                result.Add(new SelectListItem() { Text = "RA", Value = "RA" });
                result.Add(new SelectListItem() { Text = "GA", Value = "GA" });
                result.Add(new SelectListItem() { Text = "Lokal", Value = "Lokal" });
                result.Add(new SelectListItem() { Text = "Tanpa Anastesi", Value = "Tanpa Anastesi" });
                return result;
            }
        }

        public static List<SelectListItem> ListCukupKurangBerlebihan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Cukup", Value = "Cukup" });
                result.Add(new SelectListItem() { Text = "Kurang", Value = "Kurang" });
                result.Add(new SelectListItem() { Text = "Berlebihan", Value = "Berlebihan" });
                return result;
            }
        }

        public static List<SelectListItem> ListSadarBelumTidur
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Sadar Betul", Value = "Sadar Betul" });
                result.Add(new SelectListItem() { Text = "Belum sadar betul", Value = "Belum sadar betul" });
                result.Add(new SelectListItem() { Text = "Tidur dalam", Value = "Tidur dalam" });
                return result;
            }
        }

        public static List<SelectListItem> ListAutoAllo
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Auto", Value = "Auto" });
                result.Add(new SelectListItem() { Text = "Allo", Value = "Allo" });
                return result;
            }
        }

        public static List<SelectListItem> ListMeningkatMenurun
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Meningkatkan", Value = "Meningkatkan" });
                result.Add(new SelectListItem() { Text = "Menurun", Value = "Menurun" });
                return result;
            }
        }

        public static List<SelectListItem> ListTandaTandaTransfusi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "1. Urtikaria", Value = "1. Urtikaria" });
                result.Add(new SelectListItem() { Text = "2. Demam", Value = "2. Demam" });
                result.Add(new SelectListItem() { Text = "3. Gatal", Value = "3. Gatal" });
                result.Add(new SelectListItem() { Text = "4. Hemoglobinuria", Value = "4. Hemoglobinuria" });
                result.Add(new SelectListItem() { Text = "5. Nyeri Sendi", Value = "5. Nyeri Sendi" });
                result.Add(new SelectListItem() { Text = "6. Nyeri Kepala", Value = "6. Nyeri Kepala" });
                result.Add(new SelectListItem() { Text = "7. Sesak", Value = "7. Sesak" });
                result.Add(new SelectListItem() { Text = "8. Syok", Value = "8. Syok" });
                return result;
            }
        }

        public static List<SelectListItem> ListSudahBelum
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Sudah", Value = "Sudah" });
                result.Add(new SelectListItem() { Text = "Belum", Value = "Belum" });
                return result;
            }
        }


        public static List<SelectListItem> ListMstYaTidak
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Ya", Value = "Ya" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                result.Add(new SelectListItem() { Text = "Tidak Yakin", Value = "Tidak Yakin" });
                return result;
            }
        }

        public static List<SelectListItem> ListRuangRawat
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Rawat Jalan", Value = "Rawat Jalan" });
                result.Add(new SelectListItem() { Text = "Rawat Inap", Value = "Rawat Inap" });
                result.Add(new SelectListItem() { Text = "HCU", Value = "HCU" });
                return result;
            }
        }

        public static List<SelectListItem> ListDiketehauisuspectTidak
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Diketahui", Value = "Tidak Diketahui" });
                result.Add(new SelectListItem() { Text = "Suspect", Value = "Suspect" });
                result.Add(new SelectListItem() { Text = "Diketahui", Value = "Diketahui" });
                return result;
            }
        }

        public static List<SelectListItem> ListRiwayatAlergi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Belum Diketahui", Value = "Belum Diketahui" });
                result.Add(new SelectListItem() { Text = "Tidak Ada", Value = "Tidak Ada" });
                result.Add(new SelectListItem() { Text = "Alergi Jenis", Value = "Alergi Jenis" });
                return result;
            }
        }

        public static List<SelectListItem> ListMstYaSkor
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "1-5 kg", Value = "1-5 kg" }); 
                result.Add(new SelectListItem() { Text = "6-10 kg", Value = "6-10 kg" });
                result.Add(new SelectListItem() { Text = "10-15 kg", Value = "10-15 kg" });
                result.Add(new SelectListItem() { Text = "<=15 kg", Value = "<=15 kg" });
                return result;
            }
        }

        public static List<SelectListItem> ListPRJAnak
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Resiko Ringan(7-11)", Value = "Resiko Ringan(7-11)" });
                result.Add(new SelectListItem() { Text = "Resiko Tinggi(>12)", Value = "Resiko Tinggi(>12)" });
                return result;
            }
        }

        public static List<SelectListItem> ListPRJDewasa
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Resiko Ringan(0-24)", Value = "Resiko Ringan(0-24)" });
                result.Add(new SelectListItem() { Text = "Resiko Sedang(25-44)", Value = "Resiko Sedang(25-44)" });
                result.Add(new SelectListItem() { Text = "Resiko Tinggi(>45)", Value = "Resiko Tinggi(>45)" });
                return result;
            }
        }

        public static List<SelectListItem> ListPRJLansia
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Resiko Rendah(0-5)", Value = "Resiko Rendah(0-5)" });
                result.Add(new SelectListItem() { Text = "Resiko Sedang(6-15)", Value = "Resiko Sedang(6-15)" });
                result.Add(new SelectListItem() { Text = "Resiko Tinggi(17-30)", Value = "Resiko Tinggi(17-30)" });
                return result;
            }
        }

        public static List<SelectListItem> ListPenampilan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Rapi", Value = "Rapi" });
                result.Add(new SelectListItem() { Text = "Bersih", Value = "Bersih" });
                result.Add(new SelectListItem() { Text = "Harum / Wangi", Value = "Harum / Wangi" });
                result.Add(new SelectListItem() { Text = "Berantakan", Value = "Berantakan" });
                result.Add(new SelectListItem() { Text = "Dekil", Value = "Dekil" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListKategoriNyeri
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Nyeri", Value = "Tidak Nyeri" });
                result.Add(new SelectListItem() { Text = "Ringan", Value = "Ringan" });
                result.Add(new SelectListItem() { Text = "Sedang", Value = "Sedang" });
                result.Add(new SelectListItem() { Text = "Berat", Value = "Berat" });
                result.Add(new SelectListItem() { Text = "Hebat", Value = "Hebat" });
                result.Add(new SelectListItem() { Text = "Sangat Hebat", Value = "Sangat Hebat" });
                return result;
            }
        }

        public static List<SelectListItem> ListEkpresiWajah
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Gelisah", Value = "Gelisah" });
                result.Add(new SelectListItem() { Text = "Tegang", Value = "Tegang" });
                result.Add(new SelectListItem() { Text = "Binggung", Value = "Binggung" });
                result.Add(new SelectListItem() { Text = "Santay", Value = "Santay" });
                result.Add(new SelectListItem() { Text = "Tidak Selaras", Value = "Tidak Selaras" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListPerasaanSuasanaHati
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Marah", Value = "Marah" });
                result.Add(new SelectListItem() { Text = "Frustasi", Value = "Frustasi" });
                result.Add(new SelectListItem() { Text = "Cemas", Value = "Cemas" });
                result.Add(new SelectListItem() { Text = "Antusias", Value = "Antusias" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListTingkahLaku
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Kompulsif", Value = "Kompulsif" });
                result.Add(new SelectListItem() { Text = "Menarik Diri", Value = "Menarik Diri" });
                result.Add(new SelectListItem() { Text = "Berbohong", Value = "Berbohong" });
                result.Add(new SelectListItem() { Text = "Dependen Pada Orang Lain", Value = "Dependen Pada Orang Lain" });
                result.Add(new SelectListItem() { Text = "Membiarkan Diri Dimanfaatkan Orang Lain", Value = "Membiarkan Diri Dimanfaatkan Orang Lain" });
                result.Add(new SelectListItem() { Text = "Mencuri", Value = "Mencuri" });
                result.Add(new SelectListItem() { Text = "Reaktif", Value = "Reaktif" });
                result.Add(new SelectListItem() { Text = "Memegang Kendali", Value = "Memegang Kendali" });
                result.Add(new SelectListItem() { Text = "Menggunakan Napza", Value = "Menggunakan Napza" });
                result.Add(new SelectListItem() { Text = "Menghidar", Value = "Menghidar" });
                result.Add(new SelectListItem() { Text = "Menyakiti Diri", Value = "Menyakiti Diri" });
                result.Add(new SelectListItem() { Text = "Hiperaktif", Value = "Hiperaktif" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Hiperaktif" });
                return result;
            }
        }

        public static List<SelectListItem> ListFungsiIntelektual
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Di Atas Rata - Rata", Value = "Di Atas Rata - Rata" });
                result.Add(new SelectListItem() { Text = "Rata - Rata", Value = "Rata - Rata" });
                result.Add(new SelectListItem() { Text = "Di Bawah Rata - Rata", Value = "Di Bawah Rata - Rata" });
                return result;
            }
        }

        public static List<SelectListItem> ListPengalamanKerja
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Hambatan Relasi Dengan Atasan", Value = "Hambatan Relasi Dengan Atasan" });
                result.Add(new SelectListItem() { Text = "Membolos", Value = "Membolos" });
                result.Add(new SelectListItem() { Text = "Perasaan Negatif Pada Pekerjaan", Value = "Perasaan Negatif Pada Pekerjaan" });
                result.Add(new SelectListItem() { Text = "Hambatan Relasi Dengan Rekan Kerja", Value = "Hambatan Relasi Dengan Rekan Kerja" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListDelusi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Waham Kebesaran", Value = "Waham Kebesaran" });
                result.Add(new SelectListItem() { Text = "Waham Somatic", Value = "Waham Somatic" });
                result.Add(new SelectListItem() { Text = "Waham Kendali", Value = "Waham Kendali" });
                result.Add(new SelectListItem() { Text = "Delusi Erotomanik", Value = "Delusi Erotomanik" });
                result.Add(new SelectListItem() { Text = "Waham Lainnya", Value = "Waham Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListProsesPikiran
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Blocking", Value = "Blocking" });
                result.Add(new SelectListItem() { Text = "Asosiasi Longgar", Value = "Asosiasi Longgar" });
                result.Add(new SelectListItem() { Text = "Berunding", Value = "Berunding" });
                result.Add(new SelectListItem() { Text = "Ide Berlompatan", Value = "Ide Berlompatan" });
                result.Add(new SelectListItem() { Text = "Ide Membunuh", Value = "Ide Membunuh" });
                result.Add(new SelectListItem() { Text = "Inkoherensi", Value = "Inkoherensi" });
                result.Add(new SelectListItem() { Text = "Obsesi", Value = "Obsesi" });
                result.Add(new SelectListItem() { Text = "Ide Bunuh Diri", Value = "Ide Bunuh Diri" });
                result.Add(new SelectListItem() { Text = "Tekun", Value = "Tekun" });
                result.Add(new SelectListItem() { Text = "Depersonalisasi", Value = "Depersonalisasi" });
                return result;
            }
        }

        public static List<SelectListItem> ListHalusinasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Ada Penciuman", Value = "Tidak Ada Penciuman" });
                result.Add(new SelectListItem() { Text = "Perabaan", Value = "Perabaan" });
                result.Add(new SelectListItem() { Text = "Pendengaran", Value = "Pendengaran" });
                result.Add(new SelectListItem() { Text = "Pengecapan", Value = "Pengecapan" });
                result.Add(new SelectListItem() { Text = "Penglihatan", Value = "Penglihatan" });
                result.Add(new SelectListItem() { Text = "Somatis", Value = "Somatis" });
                return result;
            }
        }

        public static List<SelectListItem> ListAfek
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Datar", Value = "Datar" });
                result.Add(new SelectListItem() { Text = "Kaya", Value = "Kaya" });
                result.Add(new SelectListItem() { Text = "Tidak Pas", Value = "Tidak Pas" });
                result.Add(new SelectListItem() { Text = "Minim / Terbatas", Value = "Minim / Terbatas" });
                result.Add(new SelectListItem() { Text = "Labil", Value = "Labil" });
                return result;
            }
        }

        public static List<SelectListItem> ListBaikCukupBuruk
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Baik", Value = "Baik" });
                result.Add(new SelectListItem() { Text = "Cukup", Value = "Cukup" });
                result.Add(new SelectListItem() { Text = "Buruk", Value = "Buruk" });
                return result;
            }
        }

        public static List<SelectListItem> ListPerhatian
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Mudah Terdistraksi", Value = "Mudah Terdistraksi" });
                result.Add(new SelectListItem() { Text = "Sangat Waspada", Value = "Sangat Waspada" });
                return result;
            }
        }

        public static List<SelectListItem> ListKesadaran
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Compos Mentis", Value = "Compos Mentis" });
                result.Add(new SelectListItem() { Text = "Apatis", Value = "Apatis" });
                result.Add(new SelectListItem() { Text = "Delirium", Value = "Delirium" });
                result.Add(new SelectListItem() { Text = "Somnolen", Value = "Somnolen" });
                result.Add(new SelectListItem() { Text = "Stupor", Value = "Stupor" });
                result.Add(new SelectListItem() { Text = "Comatose", Value = "Comatose" });
                return result;
            }
        }

        public static List<SelectListItem> ListSpontanCaesar
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Spontan", Value = "Spontan" });
                result.Add(new SelectListItem() { Text = "Caesar", Value = "Caesar" });
                return result;
            }
        }

        public static List<SelectListItem> ListRegularInklusiSLB
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Reguler", Value = "Reguler" });
                result.Add(new SelectListItem() { Text = "Inklusi", Value = "Inklusi" });
                result.Add(new SelectListItem() { Text = "Sekolah Luar Biasa", Value = "Sekolah Luar Biasa" });
                return result;
            }
        }

        public static List<SelectListItem> ListSeriusKurangSangat
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Sangat Seriusa", Value = "Sangat Seriusa" });
                result.Add(new SelectListItem() { Text = "Serius", Value = "Serius" });
                result.Add(new SelectListItem() { Text = "Kurang Serius", Value = "Kurang Serius" });
                return result;
            }
        }

        public static List<SelectListItem> ListKananKiri
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Kanan", Value = "Kanan" });
                result.Add(new SelectListItem() { Text = "Kiri", Value = "Kiri" });
                return result;
            }
        }

        public static List<SelectListItem> ListSarafOtak
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "I", Value = "I" });
                result.Add(new SelectListItem() { Text = "II", Value = "II" });
                result.Add(new SelectListItem() { Text = "III", Value = "III" });
                result.Add(new SelectListItem() { Text = "IV", Value = "IV" });
                result.Add(new SelectListItem() { Text = "V", Value = "V" });
                result.Add(new SelectListItem() { Text = "VI", Value = "VI" });
                result.Add(new SelectListItem() { Text = "VII", Value = "VII" });
                result.Add(new SelectListItem() { Text = "VIII", Value = "VIII" });
                result.Add(new SelectListItem() { Text = "IX", Value = "IX" });
                result.Add(new SelectListItem() { Text = "X", Value = "X" });
                result.Add(new SelectListItem() { Text = "XI", Value = "XI" });
                result.Add(new SelectListItem() { Text = "XII", Value = "XII" });
                return result;
            }
        }

        public static List<SelectListItem> ListMotorik
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tenaga", Value = "Tenaga" });
                result.Add(new SelectListItem() { Text = "Tonus", Value = "Tonus" });
                result.Add(new SelectListItem() { Text = "Koordinasi", Value = "Koordinasi" });
                result.Add(new SelectListItem() { Text = "Gerakan Involunter", Value = "Gerakan Involunter" });
                result.Add(new SelectListItem() { Text = "Langka", Value = "Langka" });
                result.Add(new SelectListItem() { Text = "Gaya Jalan", Value = "Gaya Jalan" });
                return result;
            }
        }

        public static List<SelectListItem> ListReflek
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Fisiologik", Value = "Fisiologik" });
                result.Add(new SelectListItem() { Text = "Patologik", Value = "Patologik" });
                return result;
            }
        }

        public static List<SelectListItem> ListSensorik
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Permukaan", Value = "Permukaan" });
                result.Add(new SelectListItem() { Text = "Dalam", Value = "Dalam" });
                return result;
            }
        }

        public static List<SelectListItem> ListFungsiLuhur
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Kesadaran", Value = "Kesadaran" });
                result.Add(new SelectListItem() { Text = "Reaksi Emosi", Value = "Reaksi Emosi" });
                result.Add(new SelectListItem() { Text = "Fungsi Intelek", Value = "Fungsi Intelek" });
                result.Add(new SelectListItem() { Text = "Proses Berfikir", Value = "Proses Berfikir" });
                result.Add(new SelectListItem() { Text = "Fungsi Psikomotorik", Value = "Fungsi Psikomotorik" });
                result.Add(new SelectListItem() { Text = "Fungsi Psikosensorik", Value = "Fungsi Psikosensorik" });
                return result;
            }
        }

        public static List<SelectListItem> ListTandaKemunduranMental
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Reflek Memegang", Value = "Reflek Memegang" });
                result.Add(new SelectListItem() { Text = "Reaksi Menetek", Value = "Reaksi Menetek" });
                result.Add(new SelectListItem() { Text = "Snout Refleks", Value = "Snout Refleks" });
                result.Add(new SelectListItem() { Text = "Reflek (Glabola)", Value = "Reflek (Glabola)" });
                result.Add(new SelectListItem() { Text = "Refleks Palmomental", Value = "Refleks Palmomental" });
                result.Add(new SelectListItem() { Text = "Refleks Korneomandibular", Value = "Refleks Korneomandibular" });
                result.Add(new SelectListItem() { Text = "Refleks Kaki Tonik", Value = "Refleks Kaki Tonik" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListThroaks
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Simetris", Value = "Simetris" });
                result.Add(new SelectListItem() { Text = "Asimetris", Value = "Asimetris" });
                return result;
            }
        }

        public static List<SelectListItem> ListRegularIlegural
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Regular", Value = "Regular" });
                result.Add(new SelectListItem() { Text = "Ilegural", Value = "Ilegural" });
                return result;
            }
        }

        public static List<SelectListItem> ListBebasTersumbat
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Bebas", Value = "Bebas" });
                result.Add(new SelectListItem() { Text = "Tersumbat", Value = "Tersumbat" });
                return result;
            }
        }

        public static List<SelectListItem> ListFrekwensiPernafasan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "0", Value = "0" });
                result.Add(new SelectListItem() { Text = "10 - 25", Value = "10 - 25" });
                result.Add(new SelectListItem() { Text = "25 - 35", Value = "25 - 35" });
                result.Add(new SelectListItem() { Text = ">35", Value = ">35" });
                result.Add(new SelectListItem() { Text = "<10", Value = "<10" });
                return result;
            }
        }

        public static List<SelectListItem> ListUsahaBernafas
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Dangkal", Value = "Dangkal" });
                return result;
            }
        }
        
        public static List<SelectListItem> ListTekananDarahScore
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "0", Value = "0" });
                result.Add(new SelectListItem() { Text = "> 89 mmHg", Value = "> 89 mmHg" });
                result.Add(new SelectListItem() { Text = "70 - 89 mmHg", Value = "70 - 89 mmHg" });
                result.Add(new SelectListItem() { Text = "50 - 69 mmHg", Value = "50 - 69 mmHg" });
                result.Add(new SelectListItem() { Text = "1 - 49 mmHg", Value = "1 - 49 mmHg" });
                return result;
            }
        }

        public static List<SelectListItem> ListPengisianKapilerScore
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "< 2 Dtk", Value = "< 2 Dtk" });
                result.Add(new SelectListItem() { Text = "> 2 Dtk", Value = "> 2 Dtk" });
                result.Add(new SelectListItem() { Text = "Tidak Ada", Value = "Tidak Ada" });
                return result;
            }
        }

        public static List<SelectListItem> ListGCS
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "14 - 15", Value = "14 - 15" });
                result.Add(new SelectListItem() { Text = "11 - 13", Value = "11 - 13" });
                result.Add(new SelectListItem() { Text = "8 - 10", Value = "8 - 10" });
                result.Add(new SelectListItem() { Text = "5 - 7", Value = "5 - 7" });
                result.Add(new SelectListItem() { Text = "3 - 4", Value = "3 - 4" });
                return result;
            }
        }

        public static List<SelectListItem> ListYaTidakDiperlukan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Ya", Value = "Ya" });
                result.Add(new SelectListItem() { Text = "Tidak diperlukan", Value = "Tidak diperlukan" });
                return result;
            }
        }

        public static List<SelectListItem> ListTinggiRendah
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tinggi", Value = "Tinggi" });
                result.Add(new SelectListItem() { Text = "Rendah", Value = "Rendah" });
                return result;
            }
        }

        public static List<SelectListItem> ListTransportasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Ambulans RS Bhayangkara Denpasar", Value = "Ambulans RS Bhayangkara Denpasar" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

       

        public static List<SelectListItem> ListGolonganDarah
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "A", Value = "A" });
                result.Add(new SelectListItem() { Text = "B", Value = "B" });
                result.Add(new SelectListItem() { Text = "AB", Value = "AB" });
                result.Add(new SelectListItem() { Text = "O", Value = "O" });
                return result;
            }
        }

        public static List<SelectListItem> ListStatusFisik
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "1", Value = "1" });
                result.Add(new SelectListItem() { Text = "2", Value = "2" });
                result.Add(new SelectListItem() { Text = "3", Value = "3" });
                result.Add(new SelectListItem() { Text = "4", Value = "4" });
                result.Add(new SelectListItem() { Text = "5", Value = "5" });
                result.Add(new SelectListItem() { Text = "E", Value = "E" });
                return result;
            }
        }

        public static List<SelectListItem> ListSetujuTidakSetuju
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Setuju", Value = "Setuju" });
                result.Add(new SelectListItem() { Text = "Tidak Setuju", Value = "Tidak Setuju" });
                return result;
            }
        }

        public static List<SelectListItem> ListPengemudi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Pengemudi", Value = "Pengemudi" });
                result.Add(new SelectListItem() { Text = "Penumpang", Value = "Penumpang" });
                return result;
            }
        }

        public static List<SelectListItem> ListOcclussi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal Bite", Value = "Normal Bite" });
                result.Add(new SelectListItem() { Text = "Cross Bite", Value = "Cross Bite" });
                result.Add(new SelectListItem() { Text = "Deep Bite", Value = "Deep Bite" });
                return result;
            }
        }

        public static List<SelectListItem> ListTurosPalatinus
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Ada", Value = "Tidak Ada" });
                result.Add(new SelectListItem() { Text = "Kecil", Value = "Kecil" });
                result.Add(new SelectListItem() { Text = "Sedang", Value = "Sedang" });
                result.Add(new SelectListItem() { Text = "Besar", Value = "Besar" });
                result.Add(new SelectListItem() { Text = "Multiple", Value = "Multiple" });
                return result;
            }
        }

        public static List<SelectListItem> ListTurosMandibula
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Ada", Value = "Tidak Ada" });
                result.Add(new SelectListItem() { Text = "Sisi Kiri", Value = "Sisi Kiri" });
                result.Add(new SelectListItem() { Text = "Sisi Kanan", Value = "Sisi Kanan" });
                result.Add(new SelectListItem() { Text = "Keduasisi", Value = "Keduasisi" });
                return result;
            }
        }

        public static List<SelectListItem> ListTurosPalatum
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Dalam", Value = "Dalam" });
                result.Add(new SelectListItem() { Text = "Sedang", Value = "Sedang" });
                result.Add(new SelectListItem() { Text = "Rendah", Value = "Rendah" });
                return result;
            }
        }

        public static List<SelectListItem> ListTekananDarah
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Hipertensi", Value = "Hipertensi" });
                result.Add(new SelectListItem() { Text = "Hipotensi", Value = "Hipotensi" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                return result;
            }
        }

        public static List<SelectListItem> ListPersalinan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Vacum", Value = "Vacum" });
                result.Add(new SelectListItem() { Text = "Forceps", Value = "Forceps" });
                result.Add(new SelectListItem() { Text = "SC", Value = "SC" });
                return result;
            }
        }

        public static List<SelectListItem> Listkeadaanlahir
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Segera menangis", Value = "Segera menangis" });
                result.Add(new SelectListItem() { Text = "Tidak segera mengangis", Value = "Tidak segera mengangis" });
               
                return result;
            }
        }

        public static List<SelectListItem> ListSkalaNortonKondisiFisik
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Bagus", Value = "Bagus" });
                result.Add(new SelectListItem() { Text = "Kurang", Value = "Kurang" });
                result.Add(new SelectListItem() { Text = "Jelek", Value = "Jelek" });
                result.Add(new SelectListItem() { Text = "Sangat Jelek", Value = "Sangat Jelek" });
                return result;
            }
        }

        public static List<SelectListItem> ListSkalaNortonKondisiMental
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Sadar", Value = "Sadar" });
                result.Add(new SelectListItem() { Text = "Apatis", Value = "Apatis" });
                result.Add(new SelectListItem() { Text = "Binggung", Value = "Binggung" });
                result.Add(new SelectListItem() { Text = "Stupor", Value = "Stupor" });
                return result;
            }
        }

        public static List<SelectListItem> ListSkalaNortonAktifitas
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Mobilisasi Baik", Value = "Mobilisasi Baik" });
                result.Add(new SelectListItem() { Text = "Berpindah dengan bantuan", Value = "Berpindah dengan bantuan" });
                result.Add(new SelectListItem() { Text = "Menggunakan kursi roda", Value = "Menggunakan kursi roda" });
                result.Add(new SelectListItem() { Text = "Menggunakan brancard", Value = "Menggunakan brancard" });
                return result;
            }
        }

        public static List<SelectListItem> ListSkalaNortonMobilisasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Bebas", Value = "Bebas" });
                result.Add(new SelectListItem() { Text = "Ada Keterbatasan", Value = "Ada Keterbatasan" });
                result.Add(new SelectListItem() { Text = "Sangat Terbatas", Value = "Sangat Terbatas" });
                result.Add(new SelectListItem() { Text = "Tidak Bisa Bergerak", Value = "Tidak Bisa Bergerak" });
                return result;
            }
        }

        public static List<SelectListItem> ListSkalaNortonGgnPerkemihan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Ada Gangguan", Value = "Tidak Ada Gangguan" });
                result.Add(new SelectListItem() { Text = "Hilang Timbul", Value = "Hilang Timbul" });
                result.Add(new SelectListItem() { Text = "Frekuensi Urine", Value = "Frekuensi Urine" });
                result.Add(new SelectListItem() { Text = "Besar", Value = "Besar" });
                return result;
            }
        }

        public static List<SelectListItem> ListDirawat
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Intensif", Value = "Intensif" });
                result.Add(new SelectListItem() { Text = "Ruangan Lain", Value = "Ruangan Lain" });
                return result;
            }
        }

        public static List<SelectListItem> ListUsia
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Kurang Dari 60 Tahun", Value = "Kurang Dari 60 Tahun" });
                result.Add(new SelectListItem() { Text = "Lebih Dari 60 Tahun", Value = "Lebih Dari 60 Tahun" });
                result.Add(new SelectListItem() { Text = "Lebih Dari 80 Tahun", Value = "Lebih Dari 80 Tahun" });
                return result;
            }
        }

        public static List<SelectListItem> ListDifisit
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Kacamata Bukan Bifokal", Value = "Kacamata Bukan Bifokal" });
                result.Add(new SelectListItem() { Text = "Kacamata bifokal", Value = "Kacamata bifokal" });
                result.Add(new SelectListItem() { Text = "Gangguan pendengaran", Value = "Gangguan pendengaran" });
                result.Add(new SelectListItem() { Text = "Kacamata Multifokal", Value = "Kacamata Multifokal" });
                result.Add(new SelectListItem() { Text = "Katarak/ Glaukoma", Value = "Katarak/ Glaukoma" });
                result.Add(new SelectListItem() { Text = "Hampir Tidak Melihat/ Buta", Value = "Hampir Tidak Melihat/ Buta" });
                return result;
            }
        }

        public static List<SelectListItem> ListAktivitas
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Mandiri", Value = "Mandiri" });
                result.Add(new SelectListItem() { Text = "ADL Dibantu Sebagian", Value = "ADL Dibantu Sebagian" });
                result.Add(new SelectListItem() { Text = "ADL Dibantu Penuh", Value = "ADL Dibantu Penuh" });
                return result;
            }
        }

        public static List<SelectListItem> ListRiwayatjatuh
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Pernah", Value = "Tidak Pernah" });
                result.Add(new SelectListItem() { Text = "Jatuh Kurang 1 Bulan", Value = "Jatuh Kurang 1 Bulan" });
                result.Add(new SelectListItem() { Text = "Jatuh Kurang Dari 1 Tahun", Value = "Jatuh Kurang Dari 1 Tahun" });
                result.Add(new SelectListItem() { Text = "Jatuh Pada Saat Dirawat Sekarang", Value = "Jatuh Pada Saat Dirawat Sekarang" });
                return result;
            }
        }

        public static List<SelectListItem> ListKognisi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Orientasi Baik", Value = "Orientasi Baik" });
                result.Add(new SelectListItem() { Text = "Kesulitan Mengerti Perintah", Value = "Kesulitan Mengerti Perintah" });
                result.Add(new SelectListItem() { Text = "Gangguan Memori", Value = "Gangguan Memori" });
                result.Add(new SelectListItem() { Text = "Kebingunan", Value = "Kebingunan" });
                result.Add(new SelectListItem() { Text = "Disorientasi", Value = "Disorientasi" });
                return result;
            }
        }

        public static List<SelectListItem> ListPengobatan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = ">4 Jenis", Value = ">4 Jenis" });
                result.Add(new SelectListItem() { Text = "Anti Hipertensi/ Hipoglikemik/ Anti Depresan", Value = "Anti Hipertensi/ Hipoglikemik/ Anti Depresan" });
                result.Add(new SelectListItem() { Text = "Sedatif/ Psikotropika/ Narkotika", Value = "Sedatif/ Psikotropika/ Narkotika" });
                result.Add(new SelectListItem() { Text = "Infus Epidural/ Spinal/ DC/ Traksi", Value = "Infus Epidural/ Spinal/ DC/ Traksi" });
                return result;
            }
        }

        public static List<SelectListItem> ListMobilitas
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Mandiri", Value = "Mandiri" });
                result.Add(new SelectListItem() { Text = "Menggunakan Alat Bantu Berpindah", Value = "Menggunakan Alat Bantu Berpindah" });
                result.Add(new SelectListItem() { Text = "Koordinasi/ Keseimbangan Buruk", Value = "Koordinasi/ Keseimbangan Buruk" });
                result.Add(new SelectListItem() { Text = "Dibantu Sebagian", Value = "Dibantu Sebagian" });
                result.Add(new SelectListItem() { Text = "Dibantu penuh", Value = "Dibantu penuh" });
                return result;
            }
        }

        public static List<SelectListItem> ListPola
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Teratur", Value = "Teratur" });
                result.Add(new SelectListItem() { Text = "Inkontinesia Urine/ Feses", Value = "Inkontinesia Urine/ Feses" });
                result.Add(new SelectListItem() { Text = "Nokturia", Value = "Nokturia" });
                result.Add(new SelectListItem() { Text = "Ugensi/ Frekuensi", Value = "Ugensi/ Frekuensi" });
                return result;
            }
        }

        public static List<SelectListItem> ListKomorbitas
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Diabetes/Penyakit Jantung/ Stroke/ ISK", Value = "Diabetes/Penyakit Jantung/ Stroke/ ISK" });
                result.Add(new SelectListItem() { Text = "Gangguan Saraf Pusat/ Parkinson", Value = "Gangguan Saraf Pusat/ Parkinson" });
                result.Add(new SelectListItem() { Text = "Pasca bedah 0-24 jam", Value = "Pasca bedah 0-24 jam" });
                return result;
            }
        }

        public static List<SelectListItem> ListBerpindahTempat
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Mampu", Value = "Tidak Mampu" });
                result.Add(new SelectListItem() { Text = "Perlu banyak bantuan untuk bisa duduk", Value = "Perlu banyak bantuan untuk bisa duduk" });
                result.Add(new SelectListItem() { Text = "Bantuan 1 orang", Value = "Bantuan 1 orang" });
                result.Add(new SelectListItem() { Text = "Mandiri", Value = "Mandiri" });
                return result;
            }
        }

        public static List<SelectListItem> ListMobilisasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Mampu", Value = "Tidak Mampu" });
                result.Add(new SelectListItem() { Text = "Dengan Kursi Roda", Value = "Dengan Kursi Roda" });
                result.Add(new SelectListItem() { Text = "Bantuan 1 orang", Value = "Bantuan 1 orang" });
                result.Add(new SelectListItem() { Text = "Mandiri", Value = "Mandiri" });
                return result;
            }
        }

        public static List<SelectListItem> ListBerpakaian
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tergantung orang lain", Value = "Tergantung orang lain" });
                result.Add(new SelectListItem() { Text = "Sebagian dibantu(misal mengancing baju)", Value = "Sebagian dibantu(misal mengancing baju)" });
                result.Add(new SelectListItem() { Text = "Mandiri", Value = "Mandiri" });
                return result;
            }
        }

        public static List<SelectListItem> ListNaikTurun
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Mampu", Value = "Tidak Mampu" });
                result.Add(new SelectListItem() { Text = "Butuh pertolongan", Value = "Butuh pertolongan" });
                result.Add(new SelectListItem() { Text = "Mandiri", Value = "Mandiri" });
                return result;
            }
        }

        public static List<SelectListItem> ListMandi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tergantung orang lain", Value = "Tergantung orang lain" });
                result.Add(new SelectListItem() { Text = "Mandiri", Value = "Mandiri" });
                return result;
            }
        }

        public static List<SelectListItem> ListMakan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Mampu", Value = "Tidak Mampu" });
                result.Add(new SelectListItem() { Text = "Perlu seseorang menolong memotong makanan", Value = "Perlu seseorang menolong memotong makanan" });
                result.Add(new SelectListItem() { Text = "Mandiri", Value = "Mandiri" });
                return result;
            }
        }

        public static List<SelectListItem> ListMengontrolBAB
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Inkontinen/ tidak teratur (perlu enema)", Value = "Inkontinen/ tidak teratur (perlu enema)" });
                result.Add(new SelectListItem() { Text = "Kadang Inkontinen (1 x Seminggu)", Value = "Kadang Inkontinen (1 x Seminggu)" });
                result.Add(new SelectListItem() { Text = "Kontinen Teratur", Value = "Kontinen Teratur" });
                return result;
            }
        }

        public static List<SelectListItem> ListMengontrolBAK
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Inkontinen/ Pakai kateter dan tidak terkontrol", Value = "Inkontinen/ Pakai kateter dan tidak terkontrol" });
                result.Add(new SelectListItem() { Text = "Kadang Inkontinen (maksimal 1 x24 jam)", Value = "Kadang Inkontinen (maksimal 1 x24 jam)" });
                result.Add(new SelectListItem() { Text = "Mandiri", Value = "Mandiri" });
                return result;
            }
        }

        public static List<SelectListItem> ListMembersihkanDiri
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Butuh Pertolongan Orang Lain", Value = "Butuh Pertolongan Orang Lain" });
                result.Add(new SelectListItem() { Text = "Mandiri", Value = "Mandiri" });
                return result;
            }
        }

        public static List<SelectListItem> ListPenggunaaanToilet
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tergantung pertolongan orang lain", Value = "Tergantung pertolongan orang lain" });
                result.Add(new SelectListItem() { Text = "Perlu pertolongan pada beberapa aktifitas terapi, dapat mengerjakan sendiri beberapa aktitivitas lain", Value = "Perlu pertolongan pada beberapa aktifitas terapi, dapat mengerjakan sendiri beberapa aktitivitas lain" });
                return result;
            }
        }

        public static List<SelectListItem> ListYaTidakTidakyakin
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Ya", Value = "Ya" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                result.Add(new SelectListItem() { Text = "Tidak Yakin", Value = "Tidak Yakin" });
                return result;
            }
        }

        public static List<SelectListItem> ListPenurunanBB
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "1-5kg", Value = "1-5kg" });
                result.Add(new SelectListItem() { Text = "6-10kg", Value = "6-10kg" });
                result.Add(new SelectListItem() { Text = "11-15 kg", Value = "11-15 kg" });
                result.Add(new SelectListItem() { Text = ">15 kg", Value = ">15 kg" });
                return result;
            }
        }

        public static List<SelectListItem> Listalatbantu
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tanpa Alat Bantu", Value = "Tanpa Alat Bantu" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListPerkembanganPenyakit
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Membaik", Value = "Membaik" });
                result.Add(new SelectListItem() { Text = "Stabil", Value = "Stabil" });
                result.Add(new SelectListItem() { Text = "Memburuk", Value = "Memburuk" });
                result.Add(new SelectListItem() { Text = "Komplikasi", Value = "Komplikasi" });
                return result;
            }
        }

        public static List<SelectListItem> ListKeadaanUmum
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Baik", Value = "Baik" });
                result.Add(new SelectListItem() { Text = "Sedang", Value = "Sedang" });
                result.Add(new SelectListItem() { Text = "Lemah", Value = "Lemah" });
                result.Add(new SelectListItem() { Text = "Jelek", Value = "Jelek" });
                return result;
            }
        }

        public static List<SelectListItem> ListLevelAsa
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "1", Value = "1" });
                result.Add(new SelectListItem() { Text = "2", Value = "2" });
                result.Add(new SelectListItem() { Text = "3", Value = "3" });
                result.Add(new SelectListItem() { Text = "4", Value = "4" });
                result.Add(new SelectListItem() { Text = "5", Value = "5" });
                result.Add(new SelectListItem() { Text = "6", Value = "6" });
                return result;
            }
        }

        public static List<SelectListItem> ListMallampati
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "I", Value = "I" });
                result.Add(new SelectListItem() { Text = "II", Value = "II" });
                result.Add(new SelectListItem() { Text = "III", Value = "III" });
                result.Add(new SelectListItem() { Text = "IV", Value = "IV" });
                return result;
            }
        }



        public static List<SelectListItem> ListNormalAbnormal
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Abnormal", Value = "Abnormal" });
                return result;
            }
        }

        public static List<SelectListItem> ListVulva
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Condyloma", Value = "Condyloma" });
                result.Add(new SelectListItem() { Text = "Lesi", Value = "Lesi" });
                return result;
            }
        }

        public static List<SelectListItem> ListVagina
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Inflamasi", Value = "Inflamasi" });
                result.Add(new SelectListItem() { Text = "Discharge", Value = "Discharge" });
                return result;
            }
        }

        public static List<SelectListItem> ListCervix
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Inflamasi", Value = "Inflamasi" });
                result.Add(new SelectListItem() { Text = "Lesi", Value = "Lesi" });
                return result;
            }
        }

        public static List<SelectListItem> ListUterus
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Firoid", Value = "Firoid" });
                return result;
            }
        }

        public static List<SelectListItem> ListAdnexa
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Normal", Value = "Normal" });
                result.Add(new SelectListItem() { Text = "Masa", Value = "Masa" });
                return result;
            }
        }

        public static List<SelectListItem> ListGizi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Baik", Value = "Baik" });
                result.Add(new SelectListItem() { Text = "Sedang", Value = "Sedang" });
                result.Add(new SelectListItem() { Text = "Kurang", Value = "Kurang" });
                result.Add(new SelectListItem() { Text = "Buruk", Value = "Buruk" });
                return result;
            }
        }

        public static List<SelectListItem> ListKondisisaatkeluar
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Sembuh", Value = "Sembuh" });
                result.Add(new SelectListItem() { Text = "Membaik", Value = "Membaik" });
                result.Add(new SelectListItem() { Text = "Tidak sembuh", Value = "Tidak sembuh" });
                result.Add(new SelectListItem() { Text = "Meninggal >= 48 Jam", Value = "Meninggal >= 48 Jam" });
                result.Add(new SelectListItem() { Text = "Meninggal <= 48 Jam", Value = "Meninggal <= 48 Jam" });
                return result;
            }
        }

        public static List<SelectListItem> Listprognosis
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Baik(Dubius ad bonam)", Value = "Baik(Dubius ad bonam)" });
                result.Add(new SelectListItem() { Text = "Buruk(ad malam)", Value = "Buruk(ad malam)" });
           
                return result;
            }
        }

        public static List<SelectListItem> ListKewaspadaan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Standart", Value = "Standart" });
                result.Add(new SelectListItem() { Text = "Contact", Value = "Contact" });
                result.Add(new SelectListItem() { Text = "Contact", Value = "Contact" });
                result.Add(new SelectListItem() { Text = "Airbone", Value = "Airbone" });
                result.Add(new SelectListItem() { Text = "Droplet", Value = "Droplet" });
                return result;
            }
        }

        public static List<SelectListItem> ListJenisObat
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Obat Oral", Value = "Obat Oral" });
                result.Add(new SelectListItem() { Text = "Obat Injeksi", Value = "Obat Injeksi" });
                return result;
            }
        }

        public static List<SelectListItem> Listn2001air
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "N2O", Value = "N2O" });
                result.Add(new SelectListItem() { Text = "O2", Value = "O2" });
                result.Add(new SelectListItem() { Text = "Air", Value = "Air" });
                return result;
            }
        }

        public static List<SelectListItem> ListR
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "28", Value = "28" });
                result.Add(new SelectListItem() { Text = "20", Value = "20" });
                result.Add(new SelectListItem() { Text = "18", Value = "18" });
                result.Add(new SelectListItem() { Text = "12", Value = "12" });
                result.Add(new SelectListItem() { Text = "8", Value = "8" });
                return result;
            }
        }

        public static List<SelectListItem> ListN
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "180", Value = "180" });
                result.Add(new SelectListItem() { Text = "160", Value = "160" });
                result.Add(new SelectListItem() { Text = "140", Value = "140" });
                result.Add(new SelectListItem() { Text = "120", Value = "120" });
                result.Add(new SelectListItem() { Text = "100", Value = "100" });
                result.Add(new SelectListItem() { Text = "80", Value = "80" });
                result.Add(new SelectListItem() { Text = "60", Value = "60" });
                return result;
            }
        }
        public static List<SelectListItem> ListTD
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "220", Value = "220" });
                result.Add(new SelectListItem() { Text = "200", Value = "200" });
                result.Add(new SelectListItem() { Text = "180", Value = "180" });
                result.Add(new SelectListItem() { Text = "160", Value = "160" });
                result.Add(new SelectListItem() { Text = "140", Value = "140" });
                result.Add(new SelectListItem() { Text = "120", Value = "120" });
                result.Add(new SelectListItem() { Text = "100", Value = "100" });
                result.Add(new SelectListItem() { Text = "80", Value = "80" });
                result.Add(new SelectListItem() { Text = "60", Value = "60" });
                result.Add(new SelectListItem() { Text = "40", Value = "40" });
                result.Add(new SelectListItem() { Text = "20", Value = "20" });
                result.Add(new SelectListItem() { Text = "20", Value = "20" });
                result.Add(new SelectListItem() { Text = "0", Value = "0" });
                return result;
            }
        }

        public static List<SelectListItem> ListVitalSign
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "&bull; N", Value = "N" });
                result.Add(new SelectListItem() { Text = "&or; Sis", Value = "Sis" });
                result.Add(new SelectListItem() { Text = "&and; Dias", Value = "Dias" });
                result.Add(new SelectListItem() { Text = "+ RR", Value = "RR" });
                result.Add(new SelectListItem() { Text = "&#11205; infus", Value = "Infus" });
                return result;
            }
        }
        public static List<SelectListItem> ListGas
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Hal", Value = "Hal" });
                result.Add(new SelectListItem() { Text = "Enf", Value = "Enf" });
                result.Add(new SelectListItem() { Text = "Isof", Value = "Isof" });
                result.Add(new SelectListItem() { Text = "Sevo", Value = "Sevo" });
                result.Add(new SelectListItem() { Text = "Des%", Value = "Des%" });
                return result;
            }
        }

        public static List<SelectListItem> ListJenisKelamin
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Laki - Laki", Value = "Laki - Laki" });
                result.Add(new SelectListItem() { Text = "Perempuan", Value = "Perempuan" });
                return result;
            }
        }

        public static List<SelectListItem> ListDiantar
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Datang Sendiri", Value = "Datang Sendiri" });
                result.Add(new SelectListItem() { Text = "Diantar", Value = "Diantar" });
                return result;
            }
        }

        public static List<SelectListItem> ListPupilKaki
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "R.Pupil", Value = "R.Pupil" });
                result.Add(new SelectListItem() { Text = "Ka/Ki", Value = "Ka/Ki" });
                return result;
            }
        }

        public static List<SelectListItem> ListSoap
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "S:NYERI PINGGANG," + "\n" + "O: NT : SLR : +, A: LBP EC HNP, P: TENS SWD EXC", Value = "S:NYERI PINGGANG," + "\n" + "O: NT : SLR : +," + "\n" + "A: LBP EC HNP," + "\n" + "P: TENS SWD EXC" });
                result.Add(new SelectListItem() { Text = "S:NYERI PINGGUL, O: NT :+, PIRIFORMIS TEST :+, A: PIRIFORMIS SYNDROM, P: TENS SWD EXC", Value = "S:NYERI PINGGUL," + "\n" + "O: NT :+, PIRIFORMIS TEST :+," + "\n" + "A: PIRIFORMIS SYNDROM," + "\n" + "P: TENS SWD EXC" });
                result.Add(new SelectListItem() { Text = "S:kaku BAHU, O:rom terbatas, A:FROZEN SHOULDER, P: TENS USD EXC", Value = "S:kaku BAHU," + "\n" + "O:rom terbatas," + "\n" + "A:FROZEN SHOULDER," + "\n" + "P: TENS USD EXC" });
                result.Add(new SelectListItem() { Text = "S:KAKI BENGKOK, O:rom terbatas, A:FROZEN SHOULDER, P: TENS USD EXC", Value = "S:KAKI BENGKOK," + "\n" + "O:rom terbatas," + "\n" + "A:FROZEN SHOULDER," + "\n" + "P: TENS USD EXC" });
                result.Add(new SelectListItem() { Text = "S:NYERI LEHER, O:SPURLING : +, A:CRS, P: TENS SWD EXC", Value = "S:NYERI LEHER," + "\n" + "O:SPURLING : +," + "\n" + "A:CRS," + "\n" + "P: TENS SWD EXC" });
                result.Add(new SelectListItem() { Text = "S:NYERI LEHER, O:SPURLING : -, A:Cervicalgia, P: TENS SWD EXC", Value = "S:NYERI LEHER," + "\n" + "O:SPURLING : -," + "\n" + "A:Cervicalgia," + "\n" + "P: TENS SWD EXC" });
                result.Add(new SelectListItem() { Text = "S:kaku siku, O:rom terbatas, A:stiffnes elbow, P: TENS USD EXC", Value = "S:kaku siku," + "\n" + "O:rom terbatas," + "\n" + "A:stiffnes elbow," + "\n" + "P: TENS USD EXC" });
                result.Add(new SelectListItem() { Text = "S:NYERI PINGGANG, O: NT : +SLR : -, A: LBP , P: TENS SWD EXC", Value = "S:NYERI PINGGANG," + "\n" + "O: NT : +SLR : -," + "\n" + "A: LBP ," + "\n" + "P: TENS SWD EXC" });
                result.Add(new SelectListItem() { Text = "S:NYERI SIKU, O: NT :+ , PHALEN TEST :+, A: TENNIS ELBOW, P: TENS SWD EXC", Value = "S:NYERI SIKU," + "\n" + "O: NT :+ , PHALEN TEST :+," + "\n" + "A: TENNIS ELBOW," + "\n" + "P: TENS SWD EXC" });
                result.Add(new SelectListItem() { Text = "S:KAKU BAHU, O: ROM TERBATAS, A: STIFFNES SHOULDER, P: TENS USD EXC", Value = "S:KAKU BAHU," + "\n" + "O: ROM TERBATAS," + "\n" + "A: STIFFNES SHOULDER," + "\n" + "P: TENS USD EXC" });
                result.Add(new SelectListItem() { Text = "S:KAKU PERGELANGAN TANGAN, O: ROM TERBATAS, A: STIFFNES HAND, P: TENS USD EXC", Value = "S:KAKU PERGELANGAN TANGAN," + "\n" + "O: ROM TERBATAS," + "\n" + "A: STIFFNES HAND," + "\n" + "P: TENS USD EXC" });
                result.Add(new SelectListItem() { Text = "S:LEMAH SEPARUH TUBUH, O: MMT : LEMAH, A: SNH, P: TENS IR EXC", Value = "S:LEMAH SEPARUH TUBUH," + "\n" + "O: MMT : LEMAH," + "\n" + "A: SNH," + "\n" + "P: TENS IR EXC" });
                result.Add(new SelectListItem() { Text = "S:LEMAH OTOT WAJAH, O: MMT : LEMAH, A: BELL PALSY, P: TENS IR EXC", Value = "S:LEMAH OTOT WAJAH," + "\n" + "O: MMT : LEMAH," + "\n" + "A: BELL PALSY," + "\n" + "P: TENS IR EXC" });
                result.Add(new SelectListItem() { Text = "S:kaku LUTUT, O:rom terbatas, A:stiffnes KNEE, P: TENS USD/IR EXC", Value = "S:kaku LUTUT," + "\n" + "O:rom terbatas," + "\n" + "A:stiffnes KNEE," + "\n" + "P: TENS USD/IR EXC" });
                result.Add(new SelectListItem() { Text = "S:kaku PERGELANGAN KAKI, O:rom terbatas, A:stiffnes ANKLE, P: TENS USD/IR EXC", Value = "S:kaku PERGELANGAN KAKI," + "\n" + "O:rom terbatas," + "\n" + "A:stiffnes ANKLE," + "\n" + "P: TENS USD/IR EXC" });
                result.Add(new SelectListItem() { Text = "S:kaku PINGGUL, O:rom terbatas, A:stiffnes HIP, P: TENS USD/IR EXC", Value = "S:kaku PINGGUL," + "\n" + "O:rom terbatas," + "\n" + "A:stiffnes HIP," + "\n" + "P: TENS USD/IR EXC" });
                result.Add(new SelectListItem() { Text = "S:NYERI PUNGGUNG, O: NT :+ SPASME :+, A: MYALGIA, P: TENS SWD EXC", Value = "S:NYERI PUNGGUNG," + "\n" + "O: NT :+ SPASME :+," + "\n" + "A: MYALGIA," + "\n" + "P: TENS SWD EXC" });
                result.Add(new SelectListItem() { Text = "S:NYERI BAHU, O: NT :+ , HAWKIN : +, A: IMPINGMENT SHOULDER, P: TENS SWD EXC", Value = "S:NYERI BAHU," + "\n" + "O: NT :+ , HAWKIN : +," + "\n" + "A: IMPINGMENT SHOULDER," + "\n" + "P: TENS SWD EXC" });
                result.Add(new SelectListItem() { Text = "S:NYERI BAHU, O: NT :+ 90- 120 DERAJAT DROP ARM TEST : +, A: ROTATTOR CUFFT INJURY, P: TENS SWD EXC", Value = "S:NYERI BAHU," + "\n" + "O: NT :+ 90- 120 DERAJAT DROP ARM TEST : +," + "\n" + "A: ROTATTOR CUFFT INJURY," + "\n" + "P: TENS SWD EXC" });
                result.Add(new SelectListItem() { Text = "S:NYERI TELAPAK KAKI, O: NT :+ TALARTIL : +, A: PLANTAR FASCITIS, P: TENS USD EXC", Value = "S:NYERI TELAPAK KAKI," + "\n" + "O: NT :+ TALARTIL : +," + "\n" + "A: PLANTAR FASCITIS," + "\n" + "P: TENS USD EXC" });
                result.Add(new SelectListItem() { Text = "S:NYERI LUTUT, O: LACHMAN : +, FLEKSI MENURUN, A: ACL, P: TENS USD EXC", Value = "S:NYERI LUTUT," + "\n" + "O: LACHMAN : +, FLEKSI MENURUN," + "\n" + "A: ACL," + "\n" + "P: TENS USD EXC" });
                result.Add(new SelectListItem() { Text = "S:NYERI LUTUT, O: APLEY TEST : +, A: MENISCUS INJURY, P: TENS USD EXC", Value = "S:NYERI LUTUT," + "\n" + "O: APLEY TEST : +," + "\n" + "A: MENISCUS INJURY," + "\n" + "P: TENS USD EXC" });
                result.Add(new SelectListItem() { Text = "S:NYERI PAHA, O: GASTROC : + NYERO, A: ACHILES TENDINITIS, P: TENS USD EXC", Value = "S:NYERI PAHA," + "\n" + "O: GASTROC : + NYERO," + "\n" + "A: ACHILES TENDINITIS," + "\n" + "P: TENS USD EXC" });
                return result;
            }
        }

        public static List<SelectListItem> ListDiagnosa
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "LBP", Value = "LBP" });
                result.Add(new SelectListItem() { Text = "HNP", Value = "HNP" });
                result.Add(new SelectListItem() { Text = "STIFFNES SHOULDER", Value = "STIFFNES SHOULDER" });
                result.Add(new SelectListItem() { Text = "STIFFNES HAND", Value = "STIFFNES HAND" });
                result.Add(new SelectListItem() { Text = "STIFFNES WRIST", Value = "STIFFNES WRIST" });
                result.Add(new SelectListItem() { Text = "CRS", Value = "CRS" });
                result.Add(new SelectListItem() { Text = "CERVICALGIA MUSCLE SPASME", Value = "CERVICALGIA MUSCLE SPASME" });
                result.Add(new SelectListItem() { Text = "STIFFNES HIP", Value = "STIFFNES HIP" });
                result.Add(new SelectListItem() { Text = "FROZEN SHOULDER", Value = "FROZEN SHOULDER" });
                result.Add(new SelectListItem() { Text = "STIFFNES ELBOW", Value = "STIFFNES ELBOW" });
                result.Add(new SelectListItem() { Text = "SNH", Value = "SNH" });
                result.Add(new SelectListItem() { Text = "ACL", Value = "ACL" });
                result.Add(new SelectListItem() { Text = "MENISKUS INJURY", Value = "MENISKUS INJURY" });
                result.Add(new SelectListItem() { Text = "ACHILLES TENDINITIS", Value = "ACHILLES TENDINITIS" });
                result.Add(new SelectListItem() { Text = "STIFFNES KNEE", Value = "STIFFNES KNEE" });
                result.Add(new SelectListItem() { Text = "STIFFNES HIP", Value = "STIFFNES HIP" });
                result.Add(new SelectListItem() { Text = "PLANTAR FASCITIS", Value = "PLANTAR FASCITIS" });
                result.Add(new SelectListItem() { Text = "STIFFNES ANKLE", Value = "STIFFNES ANKLE" });
                result.Add(new SelectListItem() { Text = "BELL PALSY", Value = "BELL PALSY" });
                result.Add(new SelectListItem() { Text = "PIRIFORMIS SYNDROM", Value = "PIRIFORMIS SYNDROM" });
                return result;
            }
        }

        public static List<SelectListItem> ListIntruksiterapi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "TENS SWD EXC", Value = "TENS SWD EXC" });
                result.Add(new SelectListItem() { Text = "TENS USD EXC", Value = "TENS USD EXC" });
                result.Add(new SelectListItem() { Text = "ESWT", Value = "ESWT" });
                result.Add(new SelectListItem() { Text = "TENS IR EXC", Value = "TENS IR EXC" });
                result.Add(new SelectListItem() { Text = "TENS", Value = "TENS" });
                result.Add(new SelectListItem() { Text = "TRAKSI CERVICAL - LUMBAL", Value = "TRAKSI CERVICAL - LUMBAL" });
                result.Add(new SelectListItem() { Text = "TENS SWD TRAKSI", Value = "TENS SWD TRAKSI" });
                result.Add(new SelectListItem() { Text = "USD TENS EXC", Value = "USD TENS EXC" });
                result.Add(new SelectListItem() { Text = "MWD TENS EXC", Value = "MWD TENS EXC" });
                return result;
            }
        }

        public static List<SelectListItem> ListDiagnosaPoliUmum
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Sehat Tidak Buta Warna", Value = "Sehat Tidak Buta Warna" });
                result.Add(new SelectListItem() { Text = "Sehat tidak berpenyakit menular ( hepatitis)", Value = "Sehat tidak berpenyakit menular ( hepatitis)" });
                result.Add(new SelectListItem() { Text = "Sehat Tidak Berpenyakit Menular ( HIV)", Value = "Sehat Tidak Berpenyakit Menular ( HIV)" });
                result.Add(new SelectListItem() { Text = "Surat Sehat tidak bertindik dan bertatto", Value = "Surat Sehat tidak bertindik dan bertatto" });
                return result;
            }
        }

        public static List<SelectListItem> ListSoapIlmuDalam
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Diagnosa : DM TYPE 2", Value = "S: Kontrol," + "\n" + "O : Tekanan Darah: 120/80  mmHg, BSN :   mg/dl, GDP :    mg/dl, GD 2 jam PP :   mg/dl" + "\n" + "A : DM TYPE 2" + "\n" + "P : " + "\n" + "konsumsi obat teratur sesuai instruksi dokter" + "\n" + "Diet DM dan olahraga teratur" + "\n" + "Kontrol 1 bulan lagi atau sewaktu-waktu bila ada keluhan" });
                result.Add(new SelectListItem() { Text = "Diagnosa : HHD", Value = "S: Kontrol," + "\n" + "O : Tekanan Darah:       mmHg" + "\n" + "A : HHD" + "\n" + "P : " + "\n" + "konsumsi obat teratur sesuai instruksi dokter" + "\n" + "Diet rendah garam dan olahraga teratur" + "\n" + "Kontrol 1 bulan lagi atau sewaktu-waktu bila ada keluhan" });
                result.Add(new SelectListItem() { Text = "Diagnosa : HT", Value = "S: Kontrol," + "\n" + "O : Tekanan Darah:       mmHg" + "\n" + "A : HT" + "\n" + "P : " + "\n" + "konsumsi obat teratur sesuai instruksi dokter" + "\n" + "Diet rendah garam dan olahraga teratur" + "\n" + "Kontrol 1 bulan lagi atau sewaktu-waktu bila ada keluhan" });
                result.Add(new SelectListItem() { Text = "Diagnosa : HIPERTIROID", Value = "S: Kontrol," + "\n" + "O : Tekanan Darah:       mmHg" + "\n" + "A : HIPERTIROID" + "\n" + "P : " + "\n" + "konsumsi obat teratur sesuai instruksi dokter" + "\n" + "Kontrol 1 bulan lagi atau sewaktu-waktu bila ada keluhan" + "\n" });
                result.Add(new SelectListItem() { Text = "Diagnosa : CKD", Value = "S: Kontrol," + "\n" + "O : Tekanan Darah:       mmHg" + "\n" + "A : CKD" + "\n" + "P : " + "\n" + "konsumsi obat teratur sesuai instruksi dokter" + "\n" + "Hemodialisis rutin" + "\n" + "Kontrol 1 bulan lagi atau sewaktu-waktu bila ada keluhan" });
                result.Add(new SelectListItem() { Text = "Diagnosa : Abdominal Pain", Value = "S: nyeri perut sejak    hari," + "\n" + "O : Tekanan Darah:       mmHg" + "\n" + "A : Abdominal Pain" + "\n" + "P : " + "\n" + "konsumsi obat teratur sesuai instruksi dokter" + "\n" + "Diet rendah garam dan olahraga teratur" + "\n" + "Kontrol 1 bulan lagi atau sewaktu-waktu bila ada keluhan" });
                result.Add(new SelectListItem() { Text = "Diagnosa : Dyspepsia", Value = "S: mual () muntah () nyeri ulu hati ()" + "\n" + "O : Tekanan Darah:       mmHg" + "\n" + "A : Dyspepsia" + "\n" + "P : " + "\n" + "konsumsi obat teratur sesuai instruksi dokter" + "\n" + "Diet rendah garam dan olahraga teratur" + "\n" + "Kontrol 1 bulan lagi atau sewaktu-waktu bila ada keluhan" });
                return result;
            }
        }

        public static List<SelectListItem> ListKesadaranHD
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Sadar", Value = "Sadar" });
                result.Add(new SelectListItem() { Text = "Apatis", Value = "Apatis" });
                result.Add(new SelectListItem() { Text = "Tidak Sadar", Value = "Tidak Sadar" });
                return result;
            }
        }

        public static List<SelectListItem> ListResikoJatuhHD
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Skala Morse", Value = "Skala Morse" });
                result.Add(new SelectListItem() { Text = "Skala Humpty Dumpty", Value = "Skala Humpty Dumpty" });
                result.Add(new SelectListItem() { Text = "Skala Edmonson", Value = "Skala Edmonson" });
                result.Add(new SelectListItem() { Text = "Rendah 0 - 7", Value = "Rendah 0 - 7" });
                result.Add(new SelectListItem() { Text = "Rendah 7 - 11", Value = "Rendah 7 - 11" });
                result.Add(new SelectListItem() { Text = "Tidak Beresiko < 90", Value = "Tidak Beresiko < 90" });
                result.Add(new SelectListItem() { Text = "Tinggi 8 - 13", Value = "Tinggi 8 - 13" });
                result.Add(new SelectListItem() { Text = "Tinggi >= 12", Value = "Tinggi >= 12" });
                result.Add(new SelectListItem() { Text = "Beresiko >= 90", Value = "Beresiko >= 90" });
                result.Add(new SelectListItem() { Text = "Sangat Tinggi >= 14", Value = "Sangat Tinggi >= 14" });
                return result;
            }
        }

        public static List<SelectListItem> ListSoapVaksinCenter
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Layak Vaksin", Value = "Pasien datang untuk melakukan vaksinasi meningitis" + "\n" + "Keluhan saat ini: tidak ada" + "\n" + "Riwayat penyakit: (-)" + "\n" + "Riwayat alergi: (-)" + "\n" + "Hamil: (-)" + "\n" + "TD: 120/80 mmHg" + "\n" + "N: 76 x/menit" + "\n" + "T: 36,5 C" + "\n" + "Pemeriksaan fisik: dbn" + "\n" + "\n" + "Ceklist vaksinasi: dalam batas normal" });
                return result;
            }
        }

        public static List<SelectListItem> ListMorseUsia
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Kurang Dari 60 Tahun", Value = "Kurang Dari 60 Tahun" });
                result.Add(new SelectListItem() { Text = "Lebih dari 60 Tahun", Value = "Lebih dari 60 Tahun" });
                result.Add(new SelectListItem() { Text = "Lebih Dari 80 Tahun", Value = "Lebih Dari 80 Tahun" });
                return result;
            }
        }

        public static List<SelectListItem> ListMorseDefisit
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Kacamata Bukan bifokal", Value = "Kacamata Bukan bifokal" });
                result.Add(new SelectListItem() { Text = "Kacamata bifokal", Value = "Kacamata bifokal" });
                result.Add(new SelectListItem() { Text = "Gangguan Pendengaran", Value = "Gangguan Pendengaran" });
                result.Add(new SelectListItem() { Text = "Kacamata Multifokal", Value = "Kacamata Multifokal" });
                result.Add(new SelectListItem() { Text = "Katarak/ Glaucoma", Value = "Katarak/ Glaucoma" });
                result.Add(new SelectListItem() { Text = "Hampir Tidak Melihat/ Buta", Value = "Hampir Tidak Melihat/ Buta" });
                return result;
            }
        }

        public static List<SelectListItem> ListMorseAktivitas
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Mandiri", Value = "Mandiri" });
                result.Add(new SelectListItem() { Text = "ADL Dibantu Sebagian", Value = "ADL Dibantu Sebagian" });
                result.Add(new SelectListItem() { Text = "ADL Dibantu Penuh", Value = "ADL Dibantu Penuh" });
                return result;
            }
        }

        public static List<SelectListItem> ListMorseRiwayatJatuh
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak Pernah", Value = "Tidak Pernah" });
                result.Add(new SelectListItem() { Text = "Jatuh < 1 Tahun", Value = "Jatuh < 1 Tahun" });
                result.Add(new SelectListItem() { Text = "Jatuh < 1 Bulan", Value = "Jatuh < 1 Bulan" });
                result.Add(new SelectListItem() { Text = "Jatuh Pada Saat Dirawat Sekarang", Value = "Jatuh Pada Saat Dirawat Sekarang" });
                return result;
            }
        }

        public static List<SelectListItem> ListMorseKognisi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Orientasi Baik", Value = "Orientasi Baik" });
                result.Add(new SelectListItem() { Text = "Kesulitan Mengerti Perintah", Value = "Kesulitan Mengerti Perintah" });
                result.Add(new SelectListItem() { Text = "Gangguan Memori", Value = "Gangguan Memori" });
                result.Add(new SelectListItem() { Text = "Kebingungan", Value = "Kebingungan" });
                result.Add(new SelectListItem() { Text = "Disorientasi", Value = "Disorientasi" });
                return result;
            }
        }

        public static List<SelectListItem> ListMorsePengobatan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "> 4 Jenis Pengobatan", Value = "> 4 Jenis Pengobatan" });
                result.Add(new SelectListItem() { Text = "Antihipertensi/ Hipoglikemik/ Antidepresan", Value = "Antihipertensi/ Hipoglikemik/ Antidepresan" });
                result.Add(new SelectListItem() { Text = "Sedasi/ Psikotropika/ Narkotika", Value = "Sedasi/ Psikotropika/ Narkotika" });
                result.Add(new SelectListItem() { Text = "Infus/ Epidural/ Spinal/ Downer/ Catheter", Value = "Infus/ Epidural/ Spinal/ Downer/ Catheter" });
                return result;
            }
        }

        public static List<SelectListItem> ListMorsePolaBAB
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Teratur", Value = "Teratur" });
                result.Add(new SelectListItem() { Text = "Inkontinesia Urine/ Feses", Value = "Inkontinesia Urine/ Feses" });
                result.Add(new SelectListItem() { Text = "NokRuria", Value = "NokRuria" });
                result.Add(new SelectListItem() { Text = "Urgensi/ Frekuensi", Value = "Urgensi/ Frekuensi" });
                return result;
            }
        }

        public static List<SelectListItem> ListMorseKomorbiditas
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Diabetes/ Penyakit Jantung/ Stroke/ ISK", Value = "Diabetes/ Penyakit Jantung/ Stroke/ ISK" });
                result.Add(new SelectListItem() { Text = "Gangguan Saraf Pusat/ Parkison", Value = "Gangguan Saraf Pusat/ Parkison" });
                result.Add(new SelectListItem() { Text = "Pasca Bedah 0 - 24 Jam", Value = "Pasca Bedah 0 - 24 Jam" });
                return result;
            }
        }

        public static List<SelectListItem> ListMorseMobilitas
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Mandiri", Value = "Mandiri" });
                result.Add(new SelectListItem() { Text = "Menggunakan Alat Bantu Berpindah", Value = "Menggunakan Alat Bantu Berpindah" });
                result.Add(new SelectListItem() { Text = "Koordinasi/ Keseimbangan Buruk", Value = "Koordinasi/ Keseimbangan Buruk" });
                result.Add(new SelectListItem() { Text = "Dibantu Sebagai", Value = "Dibantu Sebagai" });
                result.Add(new SelectListItem() { Text = "Dibantu Penuh/ Bedrest/ Nurse Assist", Value = "Dibantu Penuh/ Bedrest/ Nurse Assist" });
                result.Add(new SelectListItem() { Text = "Lingukungan Dengan Banyak Furniture", Value = "Lingukungan Dengan Banyak Furniture" });
                return result;
            }
        }

        public static List<SelectListItem> ListGiziGeriatri
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak", Value = "Tidak" });
                result.Add(new SelectListItem() { Text = "Tidak Yakin (Tidak ada tanda baju menjadi lebih longgar, tidak tanpak lebih kurus)", Value = "Tidak Yakin (Tidak ada tanda baju menjadi lebih longgar, tidak tanpak lebih kurus)" });
                result.Add(new SelectListItem() { Text = "Tidak Yakin (Ada tanda baju menjadi longgar, tampak lebih kurus)", Value = "Tidak Yakin (Ada tanda baju menjadi longgar, tampak lebih kurus)" });
                result.Add(new SelectListItem() { Text = "Ya, Ada Penurunan berat badan sebanyak :", Value = "Ya, Ada Penurunan berat badan sebanyak :" });
                return result;
            }
        }

        public static List<SelectListItem> ListGiziGeriatriYa
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "1 - 5 kg", Value = "1 - 5 kg" });
                result.Add(new SelectListItem() { Text = "6 - 10 kg", Value = "6 - 10 kg" });
                result.Add(new SelectListItem() { Text = "11 - 15 kg", Value = "11 - 15 kg" });
                result.Add(new SelectListItem() { Text = "> 15", Value = "> 15" });
                return result;
            }
        }

        public static List<SelectListItem> ListResikoMalnutrisi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Rendah (0-1)", Value = "Rendah (0-1)" });
                result.Add(new SelectListItem() { Text = "Sedang (2-3)", Value = "Sedang (2-3)" });
                result.Add(new SelectListItem() { Text = "Tinggi (4-5)", Value = "Tinggi (4-5)" });
                return result;
            }
        }

        public static List<SelectListItem> ListAsuhanGizi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak perlu asuhan gizi karena resiko malnutrisi rendah (0-1), jika kemudian terdapat kondisi khusus lakukan asuhan gizi; jika tidak, lakukan monitoring asupan dan skrining lanjut tiap 7 hari", Value = "Tidak perlu asuhan gizi karena resiko malnutrisi rendah (0-1), jika kemudian terdapat kondisi khusus lakukan asuhan gizi; jika tidak, lakukan monitoring asupan dan skrining lanjut tiap 7 hari" });
                result.Add(new SelectListItem() { Text = "Perlu asuhan gizi karena :", Value = "Perlu asuhan gizi karena :" });
                return result;
            }
        }

        public static List<SelectListItem> ListAsuhanGiziKet
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Terdapat kondisi khusus", Value = "Terdapat kondisi khusus" });
                result.Add(new SelectListItem() { Text = "Resiko malnutrisi tinggi (4-5)", Value = "Resiko malnutrisi tinggi (4-5)" });
                result.Add(new SelectListItem() { Text = "Resiko malnutrisi sedang (2-3), jika pertanyaan pada parameter 1 dijawab ya ada penurunan berat badan dan total skor(2-3)", Value = "Resiko malnutrisi sedang (2-3), jika pertanyaan pada parameter 1 dijawab ya ada penurunan berat badan dan total skor(2-3)" });
                return result;
            }
        }

        public static List<SelectListItem> ListStatusGiziHD
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Baik", Value = "Baik" });
                result.Add(new SelectListItem() { Text = "Kurang", Value = "Kurang" });
                result.Add(new SelectListItem() { Text = "Buruk", Value = "Buruk" });
                result.Add(new SelectListItem() { Text = "Lebih / Obesitas", Value = "Lebih / Obesitas" });
                return result;
            }
        }

        public static List<SelectListItem> ListKesimpulanHD
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Perlu asuhan gizi jika status gizi kurang/ buruk/ lebih", Value = "Perlu asuhan gizi jika status gizi kurang/ buruk/ lebih" });
                result.Add(new SelectListItem() { Text = "Tidak perlu asuhan gizi karena resiko malnutrisi rendah (0-1), jika kemudian terdapat kondisi khusus lakukan asuhan gizi; jika tidak, lakukan monitoring asupan dan skrining lanjut tiap 7 hari", Value = "Tidak perlu asuhan gizi karena resiko malnutrisi rendah (0-1), jika kemudian terdapat kondisi khusus lakukan asuhan gizi; jika tidak, lakukan monitoring asupan dan skrining lanjut tiap 7 hari" });
                return result;
            }
        }

        public static List<SelectListItem> JenisTindakan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Cito", Value = "Cito" });
                result.Add(new SelectListItem() { Text = "Elektif", Value = "Elektif" });
                result.Add(new SelectListItem() { Text = "Reguler", Value = "Reguler" });
                return result;
            }
        }

        public static List<SelectListItem> LuasMembran
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Low Flux", Value = "Low Flux" });
                result.Add(new SelectListItem() { Text = "High Flux", Value = "High Flux" });
                return result;
            }
        }

        public static SIMtrDataRegPasien ResumeMedis(string noreg, int nomor)
        {
            SIMtrDataRegPasien r;
            using (var s = new SIM_Entities())
            {
                r = s.SIMtrDataRegPasien.FirstOrDefault(x => x.NoReg == noreg && x.Nomor == nomor);
            }
            return r;
        }

        //public static SelectListItem FindDiagnosa(string id)
        //{
        //    SelectListItem h;
        //    using (var s = new SIM_Entities())
        //    {
        //        var m = s.mICD.FirstOrDefault(x => x.KodeICD == id);
        //        if (m == null)
        //        {
        //            h = new SelectListItem();
        //        }
        //        else
        //        {
        //            h = new SelectListItem()
        //            {
        //                Value = m.KodeICD,
        //                Text = m.Descriptions
        //            };
        //        }
        //    }
        //    return h;
        //}

        public static SelectListItem FindDokter(string id)
        {
            SelectListItem h;
            using (var s = new SIM_Entities())
            {
                var m = s.mDokter.FirstOrDefault(x => x.DokterID == id);
                h = new SelectListItem()
                {
                    Value = m.DokterID,
                    Text = m.NamaDOkter
                };
            }
            return h;
        }


        public static List<SelectListItem> ListTemplate
        {
            get
            {
                var r = new List<SelectListItem>();
                //r.Add(new SelectListItem() { Text = "", Value = "" });
                using (var s = new EMR_Entities())
                {
                    r = s.Tem_IlmuPenyakitDalam.OrderByDescending(x => x.ID).ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama_Template,
                        Value = x.ID.ToString()
                    });
                }
                return r;
            }
        }

        //public static List<SelectListItem> ListTem_Knjungan_Rehab
        //{
        //    get
        //    {
        //        var r = new List<SelectListItem>();
        //        //r.Add(new SelectListItem() { Text = "", Value = "" });
        //        using (var s = new EMR_Entities())
        //        {
        //            r = s.Tem_PoliRehab_KunjunganPoliklinik.OrderByDescending(x => x.NoTemp).ToList().ConvertAll(x => new SelectListItem()
        //            {
        //                Text = x.NamaTemp,
        //                Value = x.NoTemp.ToString()
        //            });
        //        }
        //        return r;
        //    }
        //}

        public static List<SelectListItem> ListTemplatePoliUmum
        {
            get
            {
                var r = new List<SelectListItem>();
                //r.Add(new SelectListItem() { Text = "", Value = "" });
                using (var s = new EMR_Entities())
                {
                    r = s.Tmp_PoliUmum.OrderByDescending(x => x.Id).ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Tmp,
                        Value = x.Id.ToString()
                    });
                }
                return r;
            }
        }

        public static List<SelectListItem> ListFilterSection
        {
            get
            {
                var r = new List<SelectListItem>();
                r.Add(new SelectListItem() { Text = "All", Value = "" });
                using (var s = new SIM_Entities())
                {
                    var h = s.Pelayanan_TipePelayanan_Get_New.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.NamaPelayanan,
                        Value = x.TipePelayanan
                    });
                    var m = s.SIMmSection.ToList();
                    foreach (var group in h)
                    {
                        var optionGroup = new SelectListGroup() { Name = group.Text };
                        foreach (var item in m.Where(x => x.TipePelayanan == group.Value).ToList())
                        {
                            r.Add(new SelectListItem()
                            {
                                Value = item.SectionName,
                                Text = item.SectionName,
                                Group = optionGroup
                            });
                        }
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListSection
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    var h = s.Pelayanan_TipePelayanan_Get_New.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.NamaPelayanan,
                        Value = x.TipePelayanan
                    });
                    var m = s.SIMmSection.ToList();
                    foreach (var group in h)
                    {
                        var optionGroup = new SelectListGroup() { Name = group.Text };

                        foreach (var item in m.Where(x => x.TipePelayanan == group.Value).ToList())
                        {
                            r.Add(new SelectListItem()
                            {
                                Value = item.SectionID.ToString(),
                                Text = item.SectionName,
                                Group = optionGroup
                            });
                        }

                    }

                    var optionGroup2 = new SelectListGroup() { Name = "REGISTRASI" };
                    foreach (var item in m.Where(x => x.SectionID == "SEC000").ToList())
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.SectionID.ToString(),
                            Text = item.SectionName,
                            Group = optionGroup2
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListSectionResep
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    var myInClause = new string[] { "Penunjang", "Penunjang2"};
                    var m = s.SIMmSection.Where(x => myInClause.Contains(x.TipePelayanan) && x.StatusAktif == true).ToList();
                    //var m = s.SIMmSection.Where(x => (x.TipePelayanan == "Penunjang" || x.TipePelayanan == "Penunjang2") && x.StatusAktif = true).ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.SectionID.ToString(),
                            Text = item.SectionName
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListSectionPenunjang
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMmSection.Where(x => x.TipePelayanan == "PENUNJANG" && x.StatusAktif == true).ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.SectionID.ToString(),
                            Text = item.SectionName
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListProfilRingkasan_Evaluasi
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "KU Membaik", Value = "KU Membaik" });
                result.Add(new SelectListItem() { Text = "KU Memburuk", Value = "KU Memburuk" });
                result.Add(new SelectListItem() { Text = "KU Tetap", Value = "KU Tetap" });
                return result;
            }
        }

        public static List<SelectListItem> SelectListDokumenAssesmenUtama(string section, string group)
        {
            var dokumen = new List<DokumenViewModel>();
            var r = new List<SelectListItem>();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var tipe = sim.SIMmSection.FirstOrDefault(e => e.SectionID == section);
                    var get_section_all = s.mDokumen.Where(z => z.SectionID == tipe.TipePelayanan).ToList();
                    foreach (var x in get_section_all)
                    {
                        dokumen.Add(new DokumenViewModel()
                        {
                            Value = x.DokumenID,
                            Text = x.NamaDokumen
                        });
                    }

                    r = dokumen.OrderBy("Value ASC").ToList().ConvertAll(x => new SelectListItem()
                    {
                        Value = x.Value,
                        Text = x.Text
                    });
                }
            }
            return r;
        }

        public static List<SelectListItem> ListHumptyDumpty_Usia
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Dibawah 3 Tahun", Value = "Dibawah 3 Tahun" });
                result.Add(new SelectListItem() { Text = "3 - 7 Tahun", Value = "3 - 7 Tahun" });
                result.Add(new SelectListItem() { Text = "7 -13 Tahun", Value = "7 -13 Tahun" });
                result.Add(new SelectListItem() { Text = "> 13 Tahun", Value = "> 13 Tahun" });
                return result;
            }
        }

        public static List<SelectListItem> ListHumptyDumpty_Diagnosa
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Diagnosa Neurologi", Value = "Diagnosa Neurologi" });
                result.Add(new SelectListItem() { Text = "Perubahan dalam oksigenasi(Masalah Saluran Nafas, Dehidrasi, Anemia, Anoreksia, Sinkop/Sakit Kepala, dll)", Value = "Perubahan dalam oksigenasi(Masalah Saluran Nafas, Dehidrasi, Anemia, Anoreksia, Sinkop/Sakit Kepala, dll)" });
                result.Add(new SelectListItem() { Text = "Kelainan Psiskis/ Prilaku", Value = "Kelainan Psiskis/ Prilaku" });
                result.Add(new SelectListItem() { Text = "Diagnosa Lain", Value = "Diagnosa Lain" });
                return result;
            }
        }

        public static List<SelectListItem> ListHumptyDumpty_GangguanKognitif
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Tidak sadar terhadap keterbatasan dirinya", Value = "Tidak sadar terhadap keterbatasan dirinya" });
                result.Add(new SelectListItem() { Text = "Lupa keterbatasan", Value = "Lupa keterbatasan" });
                result.Add(new SelectListItem() { Text = "Mengetahui kemampuan diri", Value = "Mengetahui kemampuan diri" });
                return result;
            }
        }

        public static List<SelectListItem> ListHumptyDumpty_FaktorLingkungan
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Riwayat jatuh dari tempat tidur saat bayi/ anak", Value = "Riwayat jatuh dari tempat tidur saat bayi/ anak" });
                result.Add(new SelectListItem() { Text = "Pasien menggunakan alat bantu atau box atau mebel", Value = "Pasien menggunakan alat bantu atau box atau mebel" });
                result.Add(new SelectListItem() { Text = "Pasien berada di tempat tidur", Value = "Pasien berada di tempat tidur" });
                result.Add(new SelectListItem() { Text = "Diluar ruang rawat", Value = "Diluar ruang rawat" });
                return result;
            }
        }

        public static List<SelectListItem> ListHumptyDumpty_PenggunaanObat
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Bermacam macam obat yang digunakan : obat sedatif (kecuali pasien ICU yang menggunakan sedasi dan paralisis),Hipnotik, Barbiturat, Fenotiazin, Antidepresan, Laksans/Diuretikan, Narkotika ", Value = "Bermacam macam obat yang digunakan : obat sedatif (kecuali pasien ICU yang menggunakan sedasi dan paralisis),Hipnotik, Barbiturat, Fenotiazin, Antidepresan, Laksans/Diuretikan, Narkotika " });
                result.Add(new SelectListItem() { Text = "Salah satu dari pengobatan di atas", Value = "Salah satu dari pengobatan di atas" });
                result.Add(new SelectListItem() { Text = "Pengobatan Lain", Value = "Pengobatan Lain" });
                return result;
            }
        }

        public static List<SelectListItem> ListHumptyDumpty_Respon
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Dalam 24 Jam", Value = "Dalam 24 Jam" });
                result.Add(new SelectListItem() { Text = "Dalam 48 jam riwayat jatuh", Value = "Dalam 48 jam riwayat jatuh" });
                result.Add(new SelectListItem() { Text = "> 48 Jam", Value = "> 48 Jam" });
                return result;
            }
        }

        public static List<SelectListItem> ListEvaluasi_A
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Belum Teratasi", Value = "Belum Teratasi" });
                result.Add(new SelectListItem() { Text = "Teratasi sebagai", Value = "Teratasi sebagai" });
                result.Add(new SelectListItem() { Text = "Teratasi", Value = "Teratasi" });
                return result;
            }
        }

        public static List<SelectListItem> ListEvaluasi_P 
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Pertahankan", Value = "Pertahankan" });
                result.Add(new SelectListItem() { Text = "Lanjutan Renpra", Value = "Lanjutan Renpra" });
                return result;
            }
        }

        public static List<SelectListItem> ListDiriSendiri
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Diri sendiri", Value = "Diri sendiri" });
                result.Add(new SelectListItem() { Text = "Suami", Value = "Suami" });
                result.Add(new SelectListItem() { Text = "Istri", Value = "Istri" });
                result.Add(new SelectListItem() { Text = "Ayah", Value = "Ayah" });
                result.Add(new SelectListItem() { Text = "Ibu", Value = "Ibu" });
                result.Add(new SelectListItem() { Text = "Anak", Value = "Anak" });
                result.Add(new SelectListItem() { Text = "Kakak", Value = "Kakak" });
                result.Add(new SelectListItem() { Text = "Adik", Value = "Adik" });
                result.Add(new SelectListItem() { Text = "Teman", Value = "Teman" });
                result.Add(new SelectListItem() { Text = "Kerabat", Value = "Kerabat" });
                result.Add(new SelectListItem() { Text = "Lainnya", Value = "Lainnya" });
                return result;
            }
        }

        public static List<SelectListItem> ListPenanggungJawab
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Diri sendiri", Value = "Diri sendiri" });
                result.Add(new SelectListItem() { Text = "Suami", Value = "Suami" });
                result.Add(new SelectListItem() { Text = "Istri", Value = "Istri" });
                result.Add(new SelectListItem() { Text = "Ayah", Value = "Ayah" });
                result.Add(new SelectListItem() { Text = "Ibu", Value = "Ibu" });
                result.Add(new SelectListItem() { Text = "Anak", Value = "Anak" });
                result.Add(new SelectListItem() { Text = "Kakak", Value = "Kakak" });
                result.Add(new SelectListItem() { Text = "Adik", Value = "Adik" });
                result.Add(new SelectListItem() { Text = "Teman", Value = "Teman" });
                result.Add(new SelectListItem() { Text = "Kerabat", Value = "Kerabat" });
                return result;
            }
        }

        public static List<SelectListItem> ListPenanggungJawabPersetujuanNew
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem() { Text = "", Value = "" });
                result.Add(new SelectListItem() { Text = "Seluruh Keluarga", Value = "Seluruh Keluarga" });
                result.Add(new SelectListItem() { Text = "Suami & Anak", Value = "Suami & Anak" });
                result.Add(new SelectListItem() { Text = "Istri & Anak", Value = "Istri & Anak" });
                result.Add(new SelectListItem() { Text = "Suami", Value = "Suami" });
                result.Add(new SelectListItem() { Text = "Ibu", Value = "Ibu" });
                result.Add(new SelectListItem() { Text = "Istri", Value = "Istri" });
                result.Add(new SelectListItem() { Text = "Orang Tua", Value = "Orang Tua" });
                result.Add(new SelectListItem() { Text = "Anak", Value = "Anak" });
                result.Add(new SelectListItem() { Text = "Tidak Seorangpun Keluarga", Value = "Tidak Seorangpun Keluarga" });
                result.Add(new SelectListItem() { Text = "Kerabat & Teman", Value = "Kerabat & Teman" });
                return result;
            }
        }

        //public static List<SelectListItem> ListSection
        //{
        //    get
        //    {
        //        List<SelectListItem> h;
        //        using (var s = new SIM_Entities())
        //        {
        //            h = s.Pelayanan_TipePelayanan_Get_New.ToList().ConvertAll(x => new SelectListItem()
        //            {
        //                Text = x.NamaPelayanan,
        //                Value = x.TipePelayanan
        //            });
        //        }
        //        var r = new List<SelectListItem>();
        //        using (var s = new SIM_Entities())
        //        {
        //            var m = s.SIMmSection.ToList();
        //            foreach (var x in m)
        //            {
        //                var _h = h.FirstOrDefault(y => y.Value == x.TipePelayanan);
        //                if (_h != null)
        //                {
        //                    r.Add(new SelectListItem()
        //                    {
        //                        Text = x.SectionName,
        //                        Value = x.SectionID,
        //                        Group = new SelectListGroup() { Name = _h.Text }
        //                    });
        //                }
        //            }
        //        }
        //        return r;
        //    }
        //}

    }
}