﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class UploadPenunjangViewModel
    {
        public string NoBukti { get; set; }
        public string HasilBaca { get; set; }
        public string Id_mJenisPenunjang { get; set; }
        public string NoReg { get; set; }
        public string Nomor { get; set; }
        public string SectionID { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public Nullable<System.DateTime> Jam { get; set; }
        public string Pemeriksaan { get; set; }
        public string NRM { get; set; }
        public string SampleID { get; set; }
        public string NoBuktiBilling { get; set; }
        public string NoBuktiPermintaan { get; set; }

        public HttpPostedFileBase[] files { get; set; }
    }
}