﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class CatatanPemakaianObatDanAlkesKamarOperasiDokterBedahViewModel
    {
        public ListDetail<CatatanPemakaianObatDanAlkesKamarOperasiDokterBedahModelDetail> ObtAlks_List { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string DokterBedah { get; set; }
        public string DokterBedahNama { get; set; }
        public string Diagnosa { get; set; }
        public string PetugasOK { get; set; }
        public string PetugasOKNama { get; set; }
        public string Username { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        public string JenisPasien { get; set; }
        public string NamaPasien { get; set; }
        public int SudahRegDokter { get; set; }
        public int SudahRegPetugas { get; set; }
        public int SudahRegPasien { get; set; }
        public string TandaTanganDokter { get; set; }
        public string TandaTanganPetugas { get; set; }
        public string TandaTangan { get; set; }
    }
}