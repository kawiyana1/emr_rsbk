﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class RegSuratPernyataanKenaikanKelasViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Nama { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
        public string Alamat { get; set; }
        public string NoTelp { get; set; }
        public string HubunganDenganPasien { get; set; }
        public string Pasien { get; set; }
        public string PasienNama { get; set; }
        public string Perawat { get; set; }
        public string PerawatNama { get; set; }
        public string Username { get; set; }
        public string AlamatPasien { get; set; }
        public int SudahRegPegawai { get; set; }
        public int SudahRegPasien { get; set; }
        public bool checkedpasien { get; set; }
        public string TTDPetugas { get; set; }
        public string TTDPasien { get; set; }
    }
}