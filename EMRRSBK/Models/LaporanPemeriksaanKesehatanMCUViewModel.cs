﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class LaporanPemeriksaanKesehatanMCUViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; } 
        public string NRM { get; set; }
        public string Username { get; set; }
        public string NRP { get; set; }
        public string MaksudPemeriksaan { get; set; }
        public string Jabatan { get; set; }
        public string TempatRIK { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglRIK { get; set; }
        public string Suku { get; set; }
        public string MasaKerja { get; set; }
        public string Anamnesa { get; set; }
        public string KML { get; set; }
        public string THT { get; set; }
        public string Mata { get; set; }
        public string Paru { get; set; }
        public string Jantung { get; set; }
        public string Hernia { get; set; }
        public string Anus { get; set; }
        public string Endokrin { get; set; }
        public string Genito { get; set; }
        public string E_Atas { get; set; }
        public string E_Bawah { get; set; }
        public string Kulit { get; set; }
        public string Columna { get; set; }
        public string Neurologi { get; set; }
        public string Psikiatris { get; set; }
        public string Gambar { get; set; }
        public string Catatan { get; set; }
        public string DMF { get; set; }
        public string KelainanMulut { get; set; }
        public string KebersihanMulut { get; set; }
        public string XRay { get; set; }
        public string Diagnosis { get; set; }
        public string Rontgen { get; set; }
        public string EKG { get; set; }
        public string PemeriksaanPsikiatris { get; set; }
        public string TinggiBadan { get; set; }
        public string Ow { get; set; }
        public string BeratBadan { get; set; }
        public string StakesGigi { get; set; }
        public string BentukBadan { get; set; }
        public string Tensi { get; set; }
        public string Nadi { get; set; }
        public string Temp { get; set; }
        public string Visus_OD { get; set; }
        public string Visus_OS { get; set; }
        public string Koreksi_OD { get; set; }
        public string Koreksi_OS { get; set; }
        public string MembedakanWarna { get; set; }
        public string LingkarPerut { get; set; }
        public string LemakTubuh { get; set; }
        public string Expirasi { get; set; }
        public string Inspirasi { get; set; }
        public string Spirometri { get; set; }
        public string TandaIdentifikasi { get; set; }
        public string SuaraBisikan_AD { get; set; }
        public string SuaraBisikan_AS { get; set; }
        public string Audiometri_AD { get; set; }
        public string Audiometri_AS { get; set; }
        public string SpesialisLain { get; set; }
        public string DarahRutin { get; set; }
        public string DarahKimia { get; set; }
        public string UrineRutin { get; set; }
        public string UrineKehamilan { get; set; }
        public string UrineNarkoba { get; set; }
        public string HBsAg { get; set; }
        public string HBeAg { get; set; }
        public string AntiHIV { get; set; }
        public string Laboratorium { get; set; }
        public string GolDar { get; set; }
        public string Resume { get; set; }
        public string StakesResume { get; set; }
        public string Rekomendasi { get; set; }
        public string DokterPemeriksa { get; set; }
        public string DokterPemeriksaNama { get; set; }
        public string DiketahuiOleh { get; set; }
        public string DiketahuiOlehNama { get; set; }
        public string Kualifikasi { get; set; }
        public string Kuantitatif { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
        public int Report1 { get; set; }
        public string NamaPasien { get; set; }
        public string TempatLahir { get; set; }

        public string TglLahir { get; set; }
        public int? UmurThn { get; set; }
        public string JenisKelamin { get; set; }
        public string Agama { get; set; }
        public string Alamat { get; set; }
        public string Phone { get; set; }
        public int SudahRegDokterPemeriksa { get; set; }
        public int SudahRegDiketahuiOleh { get; set; }
        public string TandaTanganDokterPemeriksa { get; set; }
        public string TandaTanganDiketahuiOleh { get; set; }

        public string DokumenID { get; set; }
        public string Template { get; set; }
        public bool save_template { get; set; }
        public string nama_template { get; set; }
    }
}
