﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class CheckListKesiapanAnestesiViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalTindakan { get; set; }
        public string JenisOperasi { get; set; }
        public string TeknikAnestesi { get; set; }
        public bool Listrik_Mesin { get; set; }
        public bool Listrik_Layar { get; set; }
        public bool Listrik_Syinge { get; set; }
        public bool Listrik_Defibrilator { get; set; }
        public bool Gas_Selang { get; set; }
        public bool Gas_FlowMeterO2 { get; set; }
        public bool Gas_Compressed { get; set; }
        public bool Gas_FlowMeterAir { get; set; }
        public bool Gas_N2O { get; set; }
        public bool Gas_FlowMeterN2O { get; set; }
        public bool Mesin_Power { get; set; }
        public bool Mesin_Self { get; set; }
        public bool Mesin_TidakAda { get; set; }
        public bool Mesin_ZatVolatil { get; set; }
        public bool Mesin_Absorber { get; set; }
        public bool Manjemen_Sungkup { get; set; }
        public bool Manjemen_Oropharygeal { get; set; }
        public bool Manjemen_Batang { get; set; }
        public bool Manjemen_Gagang { get; set; }
        public bool Manjemen_ETT { get; set; }
        public bool Manjemen_Stilet { get; set; }
        public bool Manjemen_Semprit { get; set; }
        public bool Manjemen_Forceps { get; set; }
        public bool Pemantauan_Kabel { get; set; }
        public bool Pemantauan_Elektroda { get; set; }
        public bool Pemantauan_NIBP { get; set; }
        public bool Pemantauan_SPO2 { get; set; }
        public bool Pemantauan_Kapnografi { get; set; }
        public bool Pemantauan_PemantauSuhu { get; set; }
        public bool Lain_Stetoskop { get; set; }
        public bool Lain_Suction { get; set; }
        public bool Lain_Selang { get; set; }
        public bool Lain_Plester { get; set; }
        public bool Lain_Hemotherm { get; set; }
        public bool Lain_BlanketRoll { get; set; }
        public bool Lain_Lidokaine { get; set; }
        public bool Lain_Defibrillator { get; set; }
        public bool Obat_Epinefrin { get; set; }
        public bool Obat_Atropin { get; set; }
        public bool Obat_Sedatif { get; set; }
        public bool Obat_Opiat { get; set; }
        public bool Obat_PelumpuhOtot { get; set; }
        public bool Obat_Antibiotika { get; set; }
        public bool Obat_Lain { get; set; }
        public string Obat_Lain_Ket { get; set; }
        public string DokterAnestesi { get; set; }
        public string DokterAnestesiNama { get; set; }
        public string PerawatAnestesi { get; set; }
        public string PerawatAnestesiNama { get; set; }
        public string Username { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        public string NamaPasien { get; set; }
        public string Ruangan { get; set; }
        public string JenisKelamin { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
        public string TandaTanganDokter { get; set; }
        public string TandaTanganPerawat { get; set; }
        public int SudahRegDokter { get; set; }
        public int SudahRegPerawat { get; set; }
    }
}