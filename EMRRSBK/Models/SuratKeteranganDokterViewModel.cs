﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class SuratKeteranganDokterViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tgl { get; set; }
        public string NoPol { get; set; }
        public string Pangkat { get; set; }
        public string Kesatuan { get; set; }
        public string PadaWaktuDiperiksa { get; set; }
        public string PadaWaktuDiperiksa_Untuk { get; set; }
        public string TinggiBadan { get; set; }
        public string BeratBadan { get; set; }
        public string PemeriksaanSinarTembus { get; set; }
        public string PemeriksaanLaboratorium { get; set; }
        public string GolonganDarah { get; set; }
        public string ButaWarna { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tgl_SuratKeteranganDokter { get; set; }
        public string Username { get; set; }
        public int Report { get; set; }
        public string NamaPasien { get; set; }
        public int? UmurThn { get; set; }
        public string JenisKelamin { get; set; }
        public string Alamat { get; set; }
        public string DokterNama { get; set; }
        public string Dokter { get; set; }
        public int SudahRegDokter { get; set; }
        public string TandaTanganDokter { get; set; }
        public string TandaTangan { get; set; }
    }
}