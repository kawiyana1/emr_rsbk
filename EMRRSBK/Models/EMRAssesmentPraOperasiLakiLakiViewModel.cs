﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class EMRAssesmentPraOperasiLakiLakiViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public bool Anamnesa_Batuk { get; set; }
        public bool Anamnesa_Pusing { get; set; }
        public bool Anamnesa_SesakNafas { get; set; }
        public bool Anamnesa_Pilek { get; set; }
        public bool Anamnesa_Mual { get; set; }
        public bool Anamnesa_Puasa { get; set; }
        public string Anamnesa_Puasa_Ket { get; set; }
        public bool Anamnesa_Gigi { get; set; }
        public bool Anamnesa_Lainnya { get; set; }
        public string Anamnesa_Lainnya_Ket { get; set; }
        public bool RiwayatPenyakit_DM { get; set; }
        public bool RiwayatPenyakit_TBParu { get; set; }
        public bool RiwayatPenyakit_Hepatitis { get; set; }
        public bool RiwayatPenyakit_Hipertensi { get; set; }
        public bool RiwayatPenyakit_AMI { get; set; }
        public bool RiwayatPenyakit_HIV { get; set; }
        public bool RiwayatPenyakit_Ashtma { get; set; }
        public bool RiwayatPenyakit_CHF { get; set; }
        public bool RiwayatPenyakit_Lainnya { get; set; }
        public string RiwayatPenyakit_Lainnya_Ket { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Pemeriksaan_Jam1 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Pemeriksaan_Jam2 { get; set; }
        public string TekananDarah { get; set; }
        public string FrekuensiNafas { get; set; }
        public string Nadi { get; set; }
        public string SuhuAxilaris { get; set; }
        public string Inspeksi { get; set; }
        public string Auskultasi { get; set; }
        public string Palpasi { get; set; }
        public string Perkusi { get; set; }
        public bool Hasil_FotoRontgen { get; set; }
        public bool Hasil_CTScan { get; set; }
        public bool Hasil_MRI { get; set; }
        public bool Hasil_USG { get; set; }
        public bool Hasil_Laboratorium { get; set; }
        public bool Hasil_Lainnya { get; set; }
        public string Hasil_Lainnya_Ket { get; set; }
        public string RiwayatPenyakitSebelumnnya { get; set; }
        public bool Dokumen_InformasiTindakanBedah { get; set; }
        public bool Dokumen_InformasiTindakanAnestesi { get; set; }
        public bool Dokumen_InformasiSedasi { get; set; }
        public bool Dokumen_Lainnya { get; set; }
        public string Dokumen_Lainnya_Ket { get; set; }
        public string Catatan { get; set; }
        public string Diagnosa { get; set; }
        public string RencanaOperasi { get; set; }
        public string LevelAsa { get; set; }
        public string Mallampati { get; set; }
        public string PosisiDalamOperasi { get; set; }
        public string RencanaAnastesi { get; set; }
        public string AlatKhusus { get; set; }
        public string Profilaksis { get; set; }
        public string PerkiraanLamaOperasi { get; set; }
        public string DPJPOperator { get; set; }
        public string DPJPOperatorNama { get; set; }
        public string DPJPAnastesi { get; set; }
        public string DPJPAnastesiNama { get; set; }
        public string Prosedur { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalProsedur { get; set; }
        public string Gambar1Badan { get; set; }
        public string Gambar2KepalaKiriKana { get; set; }
        public string Gambar3KepalaDepanBelakang { get; set; }
        public string Gambar4TanganPalmar { get; set; }
        public string Gmabar5TanganDorsal { get; set; }
        public string Gambar6Kaki { get; set; }
        public string NamaPasien { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
        public string TandaTanganDPJPAnestesi { get; set; }
        public string TandaTanganDPJPOperator { get; set; }
        public string TandaTangan { get; set; }

        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public List<SelectItemListAsesmentPraOperasiLaki> ListTemplate { get; set; }
        // disable all form
        public int MODEVIEW { get; set; }
    }

    public class SelectItemListAsesmentPraOperasiLaki
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}