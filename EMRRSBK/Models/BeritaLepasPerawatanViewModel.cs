﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class BeritaLepasPerawatanViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Kepada { get; set; }
        public string Diberitahukan { get; set; }

        public string Nama { get; set; }
        public int? Umur { get; set; }
        public string JenisKelamin { get; set; }
        public string Pekrjaan { get; set; }

        public string Alamat { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Masuk_Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Masuk_Jam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Lepas_Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Lepas_Jam { get; set; }
        public string DenganCatatan { get; set; }
        public string KodeDokter { get; set; }
        public string Username { get; set; }

        public int Report { get; set; }
    }
}