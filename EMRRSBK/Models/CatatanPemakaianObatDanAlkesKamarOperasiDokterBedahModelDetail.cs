﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class CatatanPemakaianObatDanAlkesKamarOperasiDokterBedahModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]

        public Nullable<System.DateTime> Jam { get; set; }
        public string KodeObat { get; set; }
        public string KodeObatNama { get; set; }
        public int Amrahan { get; set; }
        public int Terpakai { get; set; }
        public int Retur { get; set; }
        public string Keterangan { get; set; }
        public string Username { get; set; }
    }
}
