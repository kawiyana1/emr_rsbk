﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class RencanaKeperawatanDiareViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public bool TingkatStres { get; set; }
        public bool Efek { get; set; }
        public bool Penyalahgunan { get; set; }
        public bool ProsesInfeksi { get; set; }
        public bool Malabsorpsi { get; set; }
        public bool Parasit { get; set; }
        public bool Subjektif_abdomen { get; set; }
        public bool Subjektif_BAB { get; set; }
        public bool Subjektif_Lainnya { get; set; }
        public string Subjektif_Lainnya_Ket { get; set; }
        public bool Objektif_Bising { get; set; }
        public bool Objektif_Turgor { get; set; }
        public bool Objektif_Muntah { get; set; }
        public string Objektif_Muntah_Ket { get; set; }
        public bool Objektif_Mata { get; set; }
        public bool Objektif_Ubun { get; set; }
        public bool Objektif_Penurunan { get; set; }
        public string AsuhanKeperawatan { get; set; }
        public bool Mandiri_1 { get; set; }
        public bool Mandiri_2 { get; set; }
        public bool Mandiri_3 { get; set; }
        public bool Mandiri_4 { get; set; }
        public bool Mandiri_5 { get; set; }
        public bool Mandiri_6 { get; set; }
        public bool Mandiri_7 { get; set; }
        public bool Mandiri_8 { get; set; }
        public bool Kolaborasi_1 { get; set; }
        public bool Kolaborasi_2 { get; set; }
        public bool Kolaborasi_3 { get; set; }
        public string KodeDokter { get; set; }
        public string Username { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
    }
}