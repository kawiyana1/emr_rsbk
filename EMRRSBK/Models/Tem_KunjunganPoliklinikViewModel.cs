﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class Tem_KunjunganPoliklinikViewModel
    {
        public int ID { get; set; }
        public string Nama { get; set; }
        public string DokterID { get; set; }
        public string SectionID { get; set; }
        public string SOAP { get; set; }
        public string Diagnosa { get; set; }
        public string Terapi { get; set; }
    }
}