﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class AssesmentIKeperawatanHiperbarikViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public bool Rujukan_Ya_Tidak { get; set; }
        public bool Rujukan_RS { get; set; }
        public string Rujukan_RS_Ket { get; set; }
        public bool Rujukan_Dr { get; set; }
        public string Rujukan_Dr_Ket { get; set; }
        public bool Rujukan_Puskesmas { get; set; }
        public string Rujukan_Puskesmas_Ket { get; set; }
        public bool Rujukan_Lainnya { get; set; }
        public string Rujukan_Lainnya_Ket { get; set; }
        public string DxRujukan { get; set; }
        public bool Rujukan_Tidak { get; set; }
        public bool Rujukan_Tidak_DatangSendiri { get; set; }
        public bool Rujukan_Tidak_Diantar { get; set; }
        public string Rujukan_Tidak_DiantarKet { get; set; }
        public string SumberData { get; set; }
        public string KeluhanUtama { get; set; }
        public string RiwayatPenyakitDahulu { get; set; }
        public string RiwayatPenyakitDahulu_Ket { get; set; }
        public string RiwayatOperasi { get; set; }
        public string RiwayatOperasi_Ket { get; set; }
        public string RiwayatPenyakitKeluarga { get; set; }
        public string RiwayatPenyakitKeluarga_Ket { get; set; }
        public string RiwayatAlergi { get; set; }
        public string RiwayatAlergi_Ket { get; set; }
        public string TekananDarah { get; set; }
        public string Nadi { get; set; }
        public string Respirasi { get; set; }
        public string Suhu { get; set; }
        public string KeadaanUmum { get; set; }
        public string GCS_E { get; set; }
        public string GCS_V { get; set; }
        public string GCS_M { get; set; }
        public string Nyeri { get; set; }
        public string Skala { get; set; }
        public string Lokasi { get; set; }
        public string Jenis { get; set; }
        public string FrekuensiNyeri { get; set; }
        public string LamaNyeri { get; set; }
        public string Menjalar { get; set; }
        public string Menjalar_Ket { get; set; }
        public string KualitasNyeri { get; set; }
        public string NyeriBertambah { get; set; }
        public string NyeriBerkurang { get; set; }
        public string MasalahPerkawinan { get; set; }
        public string MasalahPerkawinan_Ket { get; set; }
        public string MasalahPerkawinan_KetLainnya { get; set; }
        public string KekerasanFisik { get; set; }
        public string KekerasanFisik_Mencederai { get; set; }
        public string TraumaDalamKehidupan { get; set; }
        public string TraumaDalamKehidupan_Ket { get; set; }
        public string GangguanTidur { get; set; }
        public string KonsultasiPsikolog { get; set; }
        public bool Kebiasaan_Merokok { get; set; }
        public bool Kebiasaan_Alkohol { get; set; }
        public bool Kebiasaan_Lainnya { get; set; }
        public string Kebiasaan_Lainnya_Ket { get; set; }
        public string JenisDanJumlah { get; set; }
        public string Agama { get; set; }
        public string PerluRohaniawan { get; set; }
        public string NilaiDanKepercayaan { get; set; }
        public string BBbiasanya { get; set; }
        public string BBsekarang { get; set; }
        public string TinggiBadan { get; set; }
        public string BBMenurun { get; set; }
        public string BB1 { get; set; }
        public string BB2 { get; set; }
        public string Nafsu1 { get; set; }
        public string NafsuMakanBerkurang { get; set; }
        public int TotalSkor { get; set; }
        public string Catatan { get; set; }
        public string NilaiMST { get; set; }
        public string MengontrolBAB { get; set; }
        public string MengontrolBAK { get; set; }
        public string MembersihkanDiri { get; set; }
        public string PenggunaanToilet { get; set; }
        public string Makan { get; set; }
        public string BerpindahTempat { get; set; }
        public string Mobilisasi { get; set; }
        public string Berpakaian { get; set; }
        public string NaikTurunTangga { get; set; }
        public string Mandi { get; set; }
        public int Total { get; set; }
        public string Keterangan { get; set; }
        public string Usia { get; set; }
        public int Usia_JmlSkor { get; set; }
        public string DefisitSensoris { get; set; }
        public int DefisitSensoris_JmlSkor { get; set; }
        public string Aktivitas { get; set; }
        public int Aktivitas_JmlSkor { get; set; }
        public string RiwayatJatuh { get; set; }
        public int RiwayatJatuh_JmlSkor { get; set; }
        public string Kognisi { get; set; }
        public int Kognisi_JmlSkor { get; set; }
        public string Pengobatan { get; set; }
        public int Pengobatan_JmlSkor { get; set; }
        public string Mobilitas { get; set; }
        public int Mobilitas_JmlSkor { get; set; }
        public string Pola { get; set; }
        public int Pola_JmlSkor { get; set; }
        public string Komorbiditas { get; set; }
        public int Komorbiditas_JmlSkor { get; set; }
        public int TotalKeseluruhanSkor { get; set; }
        public string KriteriaResiko { get; set; }
        public bool Diagnosa_Hambatan { get; set; }
        public bool Diagnosa_NyeriAkut { get; set; }
        public bool Diagnosa_Hipertermia { get; set; }
        public bool Diagnosa_KurangPengetahuan { get; set; }
        public bool Diagnosa_Ansietas { get; set; }
        public bool Diagnosa_Kesiapan { get; set; }
        public bool Diagnosa_Resiko { get; set; }
        public bool Diagnosa_Ketidakefektifan { get; set; }
        public bool Diagnosa_Lainnya1 { get; set; }
        public string Diagnosa_Lainnya1_Ket { get; set; }
        public bool Diagnosa_Lainnya2 { get; set; }
        public string Diagnosa_Lainnya2_Ket { get; set; }
        public string Tujuan { get; set; }
        public bool Rencana_Latih { get; set; }
        public bool Rencana_Istirahatkan { get; set; }
        public bool Rencana_BerikanInformasi { get; set; }
        public bool Rencana_BantuPasien { get; set; }
        public bool Rencana_Observasi { get; set; }
        public bool Rencana_AjarkanTeknik { get; set; }
        public bool Rencana_Anjurkan1 { get; set; }
        public bool Rencana_Anjurkan2 { get; set; }
        public bool Rencana_Anjurkan3 { get; set; }
        public bool Rencana_Identifikasi { get; set; }
        public bool Rencana_BerikanPengetahuan { get; set; }
        public bool Rencana_JelaskanProsedur { get; set; }
        public bool Rencana_DekatiPasien { get; set; }
        public bool Rencana_DengarkanPasien { get; set; }
        public bool Rencana_KajiIntegritas { get; set; }
        public bool Rencana_KurangiPerdarahan { get; set; }
        public bool Rencana_MonitorKelembaban { get; set; }
        public bool Rencana_MonitorKadar { get; set; }
        public bool Rencana_BerikanLatihan { get; set; }
        public bool Rencana_Lainnya1 { get; set; }
        public string Rencana_Lainnya1_Ket { get; set; }
        public bool Rencana_Lainnya2 { get; set; }
        public string Rencana_Lainnya2_Ket { get; set; }
        public bool Rencana_Lainnya3 { get; set; }
        public string Rencana_Lainnya3_Ket { get; set; }
        public bool Rencana_Lainnya4 { get; set; }
        public string Rencana_Lainnya4_Ket { get; set; }
        public bool Rencana_Lainnya5 { get; set; }
        public string Rencana_Lainnya5_Ket { get; set; }
        public bool Rencana_Lainnya6 { get; set; }
        public string Rencana_Lainnya6_Ket { get; set; }
        public bool BolehPulang { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> BolehPulang_JamKeluar { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> BolehPulang_Tanggal { get; set; }
        public string KontrolPoli { get; set; }
        public string KontrolPoli_Ket { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> KontrolPoli_Tanggal { get; set; }
        public bool Dirawat { get; set; }
        public string Dirawat_Ket { get; set; }
        public string RuangLain_Ket { get; set; }
        public string Kelas { get; set; }
        public bool Dirujuk { get; set; }
        public string Dirujuk_Ket { get; set; }
        public string Transportasi { get; set; }
        public string Pendamping { get; set; }
        public string Pendamping_Ket { get; set; }
        public string Username { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
        public string Template { get; set; }
        public bool save_template { get; set; }
        public string nama_template { get; set; }
        public string DokumenID { get; set; }
    }
}