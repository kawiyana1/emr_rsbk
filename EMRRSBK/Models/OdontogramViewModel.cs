﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class OdontogramViewModel
    {
        public List<PenyakitViewModel> Penyakit { get; set; }
        public string Tes { get; set; }
        public string OdontogramHtml { get; set; }
        public RegistrasiPasienViewModel Reg { get; set; }
        public string CekData { get; set; }
        public ListDetail<OdontogramDetailViewModel> Detail_List { get; set; }
    }

    public class PenyakitViewModel
    {
        public int Id { get; set; }
        public string NamaPenyakit { get; set; }
    }

    public class OdontogramDetailViewModel
    {
        public int Id { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int? Id_Penyakit { get; set; }
        public string NamaPenyakit { get; set; }
        public int? GigiNomor { get; set; }
        public string Cof { get; set; }
    }

}