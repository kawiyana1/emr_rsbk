﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class PemantauanPEWSViewModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Pernafasan { get; set; }
        public string JumlahOksigen { get; set; }
        public string UsahaNafas { get; set; }
        public string PEWScorePernafasan { get; set; }
        public string Sirkulasi { get; set; }
        public string Crt { get; set; }
        public string PEWScoreSirkulasi { get; set; }
        public string Suhu { get; set; }
        public string Beheviour_Alert { get; set; }
        public string Beheviour_Verbal { get; set; }
        public string Beheviour_Pain { get; set; }
        public string Beheviour_Unrespon { get; set; }
        public string PEWScoreBehaviour { get; set; }
        public string TotalPEWScore { get; set; }
        public string SkalaNyeri { get; set; }
        public string BAB { get; set; }
        public string BAK { get; set; }
        public string Nama { get; set; }
        public string NamaNama { get; set; }
        public string RentangSkor { get; set; }
        public Nullable<int> NomorReport { get; set; }
        public Nullable<int> NomorReport_Group { get; set; }
        public string Username { get; set; }
        public int SudahRegDkt { get; set; }
        public string TandaTanganDokter { get; set; }
        public string Viewbag { get; set; }
    }
}