﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class EMR_VIEW_PENGKAJIAN_AWAL_ViewModel
    {
        [Display(Name = "No")]
        public int No { get; set; }

        [Display(Name = "Tanggal Update")]
        public System.DateTime TanggalUpdate { get; set; }
        public string TanggalUpdate_View { get; set; }

        [Display(Name = "Jam Update")]
        public System.DateTime JamUpdate { get; set; }
        public string JamUpdate_View { get; set; }

        [Display(Name = "Kode")]
        public string Kode { get; set; }

        [Display(Name = "Nama")]
        public string Nama { get; set; }

        [Display(Name = "Section")]
        public string SectionName { get; set; }

        [Display(Name = "NRM")]
        public string NRM { get; set; }

        [Display(Name = "Range")]
        public int? Range { get; set; }

        public string KodeUrl { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
    }
}