﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class EWSScorePemantauanViewModel
    {

        public ListDetail<PemantauanAEWSViewModelDetail> AEWS_List { get; set; }
        public ListDetail<PemantauanMEWSViewModelDetail> MEWS_List { get; set; }
        public ListDetail<PemantauanPEWSViewModelDetail> PEWS_List { get; set; }
        public ListDetail<PemantauanNEWSViewModelDetail> NEWS_List { get; set; }
        public ListDetail<SIMtrRiwayatAlergiViewModelDetail> AlergiObat_List { get; set; }
        public ListDetail<ProfilRingkasanMedisRawatJalanViewModel> ProfilRingkasan_List { get; set; }
        public ListDetail<DaftarDokterPenanggungJawabPelayananViewModelDetail> DftrDokter_List { get; set; }
        public ListDetail<CatatanPerkembanganTerintegrasiViewModelDetail> CttPerkembanganTerintegrasi_List { get; set; }
        public ListDetail<ImplementasiViewModelDetail> Implmnts_List { get; set; }
        public ListDetail<EvaluasiKeperawatanModelDetail> EvlsiKprwat_List { get; set; }
        public ListDetail<CatatanTandaTandaVitalModelDetail> CttVital_List { get; set; }
        public ListDetail<ObservasiKeseimbanganCairanModelDetail> ObsrvsKsmbngnCairn_List { get; set; }
        public ListDetail<EMRSkalaHumptyDumptyViewModel> SkalaHumptyDumpty_List { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal_View { get; set; }

        

    }
}