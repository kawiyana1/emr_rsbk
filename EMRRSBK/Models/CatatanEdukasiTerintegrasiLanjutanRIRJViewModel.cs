﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class CatatanEdukasiTerintegrasiLanjutanRIRJViewModel
    {

        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string EdukasiYangDiberikan { get; set; }
        public string YangDiedukasi { get; set; }
        public string Tempat { get; set; }
        public string MetodeEdukasi { get; set; }
        public string Respon { get; set; }
        public string NamaEducator { get; set; }
        public string NamaEducatorNama { get; set; }
        public string Username { get; set; }
        public int Nomor { get; set; }
        public int NomorReport { get; set; }
        public int NomorReport_Group { get; set; } 
        public string Tanggal_View { get; set; } 
        public string Jam_View { get; set; } 
        public int SudahRegEdukasi { get; set; } 
        public string TandaTanganEdukator { get; set; } 
        public string Viewbag { get; set; } 
    }
}
