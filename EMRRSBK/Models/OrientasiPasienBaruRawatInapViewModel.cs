﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace EMRRSBK.Models
{
    public class OrientasiPasienBaruRawatInapViewModel
    {

        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglOrientasi { get; set; }
        public string PerawatDPJP { get; set; }
        public string JamVisite { get; set; }
        public string WaktuBerkunjung { get; set; }
        public string LayananObat { get; set; }
        public string JadwalObat { get; set; }
        public string AdministrasiPembayaran { get; set; }
        public string PenggunaanGelang { get; set; }
        public string KamarMandi { get; set; }
        public string KipasAngin { get; set; }
        public string Wastafel { get; set; }
        public string Lemari { get; set; }
        public string AC { get; set; }
        public string Television { get; set; }
        public string Telepon { get; set; }
        public string TempatSampah { get; set; }
        public string KotakSaran { get; set; }
        public string TataTertib { get; set; }
        public string LayananPengaduan { get; set; }
        public string KeadaanDarurat { get; set; }
        public string LaranganMerokok { get; set; }
        public string LainnyaKet_1 { get; set; }
        public string LainnyaPil_1 { get; set; }
        public string LainnyaKet_2 { get; set; }
        public string LainnyaPil_2 { get; set; }
        public string LainnyaKet_3 { get; set; }
        public string LainnyaPil_3 { get; set; }
        public string LainnyaKet_4 { get; set; }
        public string LainnyaPil_4 { get; set; }
        public string LainnyaKet_5 { get; set; }
        public string LainnyaPil_5 { get; set; }
        public string LainnyaKet_6 { get; set; }
        public string LainnyaPil_6 { get; set; }
        public string LainnyaKet_7 { get; set; }
        public string LainnyaPil_7 { get; set; }
        public string LainnyaKet_8 { get; set; }
        public string LainnyaPil_8 { get; set; }
        public string KodePemberiOrientasi { get; set; }
        public string KodePemberiOrientasiNama { get; set; }
        public string PenerimaOrientasi { get; set; }
        public string Petugas { get; set; }
        public string PetugasNama { get; set; }
        public string Keluarga { get; set; }
        public string Username { get; set; }
        public string Nama { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set;}

        public string JenisKelamin { get; set; }

        public string Ruangan { get; set;}
        
        public string Kamar { get; set;}

        public int Report { get; set; }


    }
}