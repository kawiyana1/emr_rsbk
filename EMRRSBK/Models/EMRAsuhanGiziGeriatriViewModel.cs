﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
namespace EMRRSBK.Models
{
    public class EMRAsuhanGiziGeriatriViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }

        public string Antropometri_BeratBadan { get; set; }
        public string Antropometri_TinggiBadan { get; set; }
        public string Antropometri_EstimasiTinggiBadan { get; set; }
        public string Antropometri_BeratBadanIdeal { get; set; }
        public string Antropometri_BMI { get; set; }
        public string Antropometri_StatusGizi { get; set; }
        public string Antropometri_LingkarLenganAtas { get; set; }
        public bool Biokimia_Albumin { get; set; }
        public bool Biokimia_Hb { get; set; }
        public bool Biokimia_Kolesterol { get; set; }
        public bool Biokimia_GlukosaDarah { get; set; }
        public bool Biokimia_RiwayatDm { get; set; }
        public bool Biokimia_Cbx_LainLainTerkaitGizi { get; set; }
        public string Biokimia_LainLainTerkaitGizi { get; set; }
        public bool Biokimia_AsamUrat { get; set; }
        public bool Biokimia_Kreatinin { get; set; }
        public bool Biokimia_Bun { get; set; }
        public bool Biokimia_Sgot { get; set; }
        public bool Biokimia_Sgpt { get; set; }
        public bool Biokimia_Kalium { get; set; }
        public bool Biokimia_Natrium { get; set; }
        public bool KlinisFisik_Mual { get; set; }
        public bool KlinisFisik_KesulitanMenean { get; set; }
        public bool KlinisFisik_Cbx_LainLain { get; set; }
        public string KlinisFisik_LainLain { get; set; }
        public bool KlinisFisik_Muntah { get; set; }
        public bool KlinisFisik_Anoreksia { get; set; }
        public bool KlinisFisik_Diare { get; set; }
        public bool KlinisFisik_Sesak { get; set; }
        public bool KlinisFisik_Konstipasi { get; set; }
        public bool KlinisFisik_HamilMenyusui { get; set; }
        public bool KlinisFisik_KesulitanMengunyah { get; set; }
        public bool KlinisFisik_EdemaAsitesHipertensi { get; set; }
        public string PolaMakan { get; set; }
        public bool AsupanMakan_AsupanMakanOralDiRumah { get; set; }
        public bool AsupanMakan_AsupanMakanAwalDiRumahSakit { get; set; }
        public bool AsupanMakan_AdaPerubahanJenisBentukMakanan { get; set; }
        public string AsupanMakan_DiRumah_KurangAtauBaik { get; set; }
        public string AsupanMakan_DiRumahSakit_KurangAtauBaik { get; set; }
        public string PantanganMakanan_TidakAtauAda { get; set; }
        public string PantanganMakanan_AdaJelaskan { get; set; }
        public bool AlergyMakanan_TidakAda { get; set; }
        public bool AlergyMakanan_KacangKedelaiTanah { get; set; }
        public bool AlergyMakanan_Udang { get; set; }
        public bool AlergyMakanan_Cbx_LainLain { get; set; }
        public string AlergyMakanan_LainLain { get; set; }
        public bool AlergyMakanan_Telur { get; set; }
        public bool AlergyMakanan_Ikan { get; set; }
        public bool AlergyMakanan_SusuSapiProduk { get; set; }
        public bool AlergyMakanan_GlutenGandum { get; set; }
        public bool AlergyMakanan_HazelnutAlmond { get; set; }
        public string SuplemenGizi_TanpaAtauDenganSuplemenGizi { get; set; }
        public string RiwayatPersonal { get; set; }
        public string DiagnoseMedis { get; set; }
        public string DiagnoseGizi { get; set; }
        public string JenisDiet { get; set; }
        public string TujuanPemberianDiet { get; set; }
        public string KebutuhanNutrisi_Energi { get; set; }
        public string KebutuhanNutrisi_Lemak { get; set; }
        public string KebutuhanNutrisi_Protein { get; set; }
        public string KebutuhanNutrisi_Kh { get; set; }
        public string KebutuhanNutrisi_ZatGiziLain { get; set; }
        public bool Implementasi_PemberianNutrisiSesuaiKebutuhan { get; set; }
        public bool Implementasi_Cbx_PemberianNutrisiBertahap { get; set; }
        public string Implementasi_PemberianNutrisiBertahap { get; set; }
        public bool BentukMakanan_Nasi { get; set; }
        public bool BentukMakanan_BuburTimCincang { get; set; }
        public bool BentukMakanan_BuburSaring { get; set; }
        public bool BentukMakanan_Cbx_CairSondeFormula { get; set; }
        public string BentukMakanan_CairSondeFormula { get; set; }
        public bool CaraPemberian_Oral { get; set; }
        public bool CaraPemberian_Enteral { get; set; }
        public string EdukasiKonsultasiGizi { get; set; }
        public string RencanaMonitoringDanEvaluasi { get; set; }
        public string AhliGizi { get; set; }
        public string AhliGiziNama { get; set; }
        public string Username { get; set; }
        public string BerapaKali { get; set; }
        public bool KonsumsiBahanMakanan1 { get; set; }
        public string KonsumsiBahanMakanan1Ket { get; set; }
        public bool KonsumsiBahanMakanan2 { get; set; }
        public string KonsumsiBahanMakanan2Ket { get; set; }
        public bool KonsumsiBahanMakanan3 { get; set; }
        public string KonsumsiBahanMakanan3Ket { get; set; }
        public string Konsumsi { get; set; }
        public string BanyakCairan { get; set; }
        public bool B_1 { get; set; }
        public bool B_2 { get; set; }
        public bool B_3 { get; set; }

        public string Ruangan { get; set; }

        public int Report { get; set; }

        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public List<SelectItemListAGiziGeriatri> ListTemplate { get; set; }
        // disable all form
        public int MODEVIEW { get; set; }

    }

    public class SelectItemListAGiziGeriatri
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}