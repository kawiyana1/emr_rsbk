﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class EMRIlmuPenyakitKulitDanKelaminViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public bool Rujukan_Ya { get; set; }
        public string Rujukan_Ya_Ket { get; set; }
        public string Rujukan_Ya_Dx { get; set; }
        public bool Rujukan_Tidak { get; set; }
        public string NamaKeluarga { get; set; }
        public string AlamatKeluarga { get; set; }
        public string TeleponKeluarga { get; set; }
        public string Transportasi { get; set; }
        public string Transportasi_Ket { get; set; }
        public string Nyeri { get; set; }
        public string Lokasi { get; set; }
        public string Intensitas { get; set; }
        public string Jenis { get; set; }
        public string Riwayat_Alergi { get; set; }
        public string Riwayat_Alergi_Ket { get; set; }
        public string KeluhanUtama { get; set; }
        public string Riwayat_PenyakitSekarang { get; set; }
        public bool PenyakitDahulu_Hipertensi { get; set; }
        public bool PenyakitDahulu_Diabetes { get; set; }
        public bool PenyakitDahulu_Jantung { get; set; }
        public bool PenyakitDahulu_Asthma { get; set; }
        public bool PenyakitDahulu_Lain { get; set; }
        public string PenyakitDahulu_Lain_Ket { get; set; }
        public string Riwayat_Pengobatan { get; set; }
        public bool PenyakitKeluarga_Hipertensi { get; set; }
        public bool PenyakitKeluarga_Diabetes { get; set; }
        public bool PenyakitKeluarga_Jantung { get; set; }
        public bool PenyakitKeluarga_Asthma { get; set; }
        public bool PenyakitKeluarga_Lain { get; set; }
        public string PenyakitKeluarga_Lain_Ket { get; set; }
        public bool RiwayatSosial_Merokok { get; set; }
        public bool RiwayatSosial_Alkohol { get; set; }
        public bool RiwayatSosial_Lain { get; set; }
        public string RiwayatSosial_Lain_Ket { get; set; }
        public bool Lokalisasi { get; set; }
        public string Lokalisasi_Ket { get; set; }
        public bool BentukKelamin { get; set; }
        public string BentukKelamin_Ket { get; set; }
        public bool StigmataAtopik { get; set; }
        public string StigmataAtopik_Ket { get; set; }
        public bool Mukosa { get; set; }
        public string Mukosa_Ket { get; set; }
        public bool Rambut { get; set; }
        public string Rambut_Ket { get; set; }
        public bool Kuku { get; set; }
        public string Kuku_Ket { get; set; }
        public bool FungsiKelenjar { get; set; }
        public string FungsiKelenjar_Ket { get; set; }
        public bool KelenjarLimfe { get; set; }
        public string KelenjarLimfe_Ket { get; set; }
        public bool Syaraf { get; set; }
        public string Syaraf_Ket { get; set; }
        public string Diagnosa { get; set; }
        public string DokterBPJP { get; set; }
        public string DokterBPJPNama { get; set; }
        public string Username { get; set; }
        public string Gambar1 { get; set; }
        public string TandaTanganDPJP { get; set; }
        public string TandaTangan { get; set; }

        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public List<SelectItemListIlmuKelamin> ListTemplate { get; set; }
        // disable all form
        public int MODEVIEW { get; set; } 
    }

    public class SelectItemListIlmuKelamin
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}