﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class EMRIlmuPenyakitBedahTraumaViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public bool Rujukan_Ya { get; set; }
        public string Rujukan_Ya_Ket { get; set; }
        public string DxRujukan { get; set; }
        public bool Rujukan_Tidak { get; set; }
        public string NamaKeluarga { get; set; }
        public string AlamatKeluarga { get; set; }
        public string TeleponKeluarga { get; set; }
        public string TransportasiWaktuDatang { get; set; }
        public string TransportasiWaktuDatang_Ket { get; set; }
        public string KeluhanUtama { get; set; }
        public bool KecelakaanLaluLintas { get; set; }
        public bool PejalanKaki { get; set; }
        public string PejalanKaki_Ket { get; set; }
        public bool SepedaGayung { get; set; }
        public string SepedaGayung_Ket { get; set; }
        public bool SepedaMotor { get; set; }
        public string SepedaMotor_Ket { get; set; }
        public bool Mobil { get; set; }
        public string Mobil_Ket { get; set; }
        public bool KecelakaanLainnya { get; set; }
        public bool Jatuh { get; set; }
        public string Jatuh_Ket { get; set; }
        public bool Jatuh_Pohon { get; set; }
        public bool Jatuh_Gedung { get; set; }
        public bool Jatuh_Lainnya { get; set; }
        public string Jatuh_Lainnya_Ket { get; set; }
        public bool LukaTembak { get; set; }
        public bool LukaTusuk { get; set; }
        public bool LukaHancur { get; set; }
        public bool LukaBakar { get; set; }
        public string LukaLainnya { get; set; }
        public bool Mekanisme_Mobil { get; set; }
        public string Mekanisme_Mobil_Ket { get; set; }
        public string Mobil_SabukPengaman { get; set; }
        public bool Mekanisme_SepedaMotor { get; set; }
        public string Mekanisme_SepedaMotor_Ket { get; set; }
        public string SepedaMotor_Helm { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalKejadian { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamKejadian { get; set; }
        public string TempatKejadian { get; set; }
        public bool Alergi { get; set; }
        public string Alergi_Ket { get; set; }
        public bool Medikasi { get; set; }
        public string Medikasi_Ket { get; set; }
        public bool PenyakitLain { get; set; }
        public string PenyakitLain_Ket { get; set; }
        public bool MakanTerakhir { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MakanTerakhir_Jam { get; set; }
        public string PengaruhNapza { get; set; }
        public string PengaruhNapza_Jenis { get; set; }
        public bool Suntikan { get; set; }
        public string Suntikan_Ket { get; set; }
        public bool Hamil { get; set; }
        public string Hamil_Ket { get; set; }
        public string UmurKehamilan { get; set; }
        public string MenstruasiTerakhir { get; set; }
        public bool KejadianLain { get; set; }
        public string KejadianLain_Ket { get; set; }
        public bool CSphine { get; set; }
        public string CSphine_Ya_Tidak { get; set; }
        public string CSphine_Ket { get; set; }
        public bool AirwayDevice { get; set; }
        public string AirwayDevice_Ya_Tidak { get; set; }
        public string AirwayDevice_Ket { get; set; }
        public bool IVLine { get; set; }
        public string IVLine_Ya_Tidak { get; set; }
        public string IVLine_Ket { get; set; }
        public bool Medications { get; set; }
        public string Medications_Ya_Tidak { get; set; }
        public string Medications_Ket { get; set; }
        public bool LainLain { get; set; }
        public string LainLain_Ket { get; set; }
        public string Airway { get; set; }
        public string Airway_Trachea { get; set; }
        public string Airway_Resusitasi { get; set; }
        public string Airway_Reevaluasi { get; set; }
        public string Breathing_Dada { get; set; }
        public string Breathing_SesakNafas { get; set; }
        public string Breathing_Respirasi { get; set; }
        public string Breathing_Krepitasi { get; set; }
        public string Suara_Kanan { get; set; }
        public bool Suara_Kanan_Jelas { get; set; }
        public bool Suara_Kanan_Menurun { get; set; }
        public bool Suara_Kanan_Ronchi { get; set; }
        public bool Suara_Kanan_Wheezing { get; set; }
        public string Suara_Kiri { get; set; }
        public bool Suara_Kiri_Jelas { get; set; }
        public bool Suara_Kiri_Menurun { get; set; }
        public bool Suara_Kiri_Ronchi { get; set; }
        public bool Suara_Kiri_Wheezing { get; set; }
        public string Breathing_Saturasi { get; set; }
        public bool Saturasi_SuhuRuangan { get; set; }
        public bool Saturasi_NasalCanule { get; set; }
        public bool Saturasi_NRB { get; set; }
        public bool Saturasi_Lainnya { get; set; }
        public string Saturasi_Lainnya_Ket { get; set; }
        public string Breathing_Assesment { get; set; }
        public string Breathing_Resusitasi { get; set; }
        public string Breathing_Reevaluasi { get; set; }
        public string Circulation_Tensi { get; set; }
        public string Circulation_Nadi { get; set; }
        public bool Nadi_Kuat { get; set; }
        public bool Nadi_Lemah { get; set; }
        public bool Nadi_Regular { get; set; }
        public bool Nadi_Irregular { get; set; }
        public string Circulation_SuhuAxilla { get; set; }
        public string Circulation_SuhuRectal { get; set; }
        public string Circulation_TemperaturKulit { get; set; }
        public string Circulation_GambaranKulit { get; set; }
        public string Circulation_Assesment { get; set; }
        public string Circulation_Resusitasi { get; set; }
        public string Circulation_Reevaluasi { get; set; }
        public bool Disability_Alert { get; set; }
        public bool Disability_VerbalResponse { get; set; }
        public bool Disability_PainResponse { get; set; }
        public bool Disability_Unresponsive { get; set; }
        public string FrekuensiPernafasan { get; set; }
        public string UsahaBernafas { get; set; }
        public string TekananDarah { get; set; }
        public string PengisianKapiler { get; set; }
        public string GCS { get; set; }
        public string TotalTraumaScore { get; set; }
        public bool ReaksiPupil_Cepat_Kanan { get; set; }
        public string ReaksiPupil_Cepat_Kanan_Ket { get; set; }
        public bool ReaksiPupil_Cepat_Kiri { get; set; }
        public string ReaksiPupil_Cepat_Kiri_Ket { get; set; }
        public bool ReaksiPupil_Konstriksi_Kanan { get; set; }
        public string ReaksiPupil_Konstriksi_Kanan_Ket { get; set; }
        public bool ReaksiPupil_Konstriksi_Kiri { get; set; }
        public string ReaksiPupil_Konstriksi_Kiri_Ket { get; set; }
        public bool ReaksiPupil_Lambat_Kanan { get; set; }
        public string ReaksiPupil_Lambat_Kanan_Ket { get; set; }
        public bool ReaksiPupil_Lambat_Kiri { get; set; }
        public string ReaksiPupil_Lambat_Kiri_Ket { get; set; }
        public bool ReaksiPupil_Dilatasi_Kanan { get; set; }
        public string ReaksiPupil_Dilatasi_Kanan_Ket { get; set; }
        public bool ReaksiPupil_Dilatasi_Kiri { get; set; }
        public string ReaksiPupil_Dilatasi_Kiri_Ket { get; set; }
        public bool ReaksiPupil_TakBereaksi_Kanan { get; set; }
        public string ReaksiPupil_TakBereaksi_Kanan_Ket { get; set; }
        public bool ReaksiPupil_TakBereaksi_Kiri { get; set; }
        public string ReaksiPupil_TakBereaksi_Kiri_Ket { get; set; }
        public bool SecondarySurvey_Kepala { get; set; }
        public string SecondarySurvey_Kepala_Ket { get; set; }
        public bool SecondarySurvey_Maxillofacial { get; set; }
        public string SecondarySurvey_Maxillofacial_Ket { get; set; }
        public bool SecondarySurvey_CSpine { get; set; }
        public string SecondarySurvey_CSpine_Ket { get; set; }
        public bool SecondarySurvey_Chest { get; set; }
        public string SecondarySurvey_Chest_Ket { get; set; }
        public bool SecondarySurvey_Abdomen { get; set; }
        public string SecondarySurvey_Abdomen_Ket { get; set; }
        public bool SecondarySurvey_Genital { get; set; }
        public string SecondarySurvey_Genital_Ket { get; set; }
        public bool SecondarySurvey_Rectal { get; set; }
        public string SecondarySurvey_Rectal_Ket { get; set; }
        public bool SecondarySurvey_Musculoskeletal { get; set; }
        public string SecondarySurvey_Musculoskeletal_Ket { get; set; }
        public string Gambar1 { get; set; }
        public string Gambar2 { get; set; }
        public string HasilPemeriksaanPenunjang { get; set; }
        public string RencanaKerja { get; set; }
        public string Terapi { get; set; }
        public string DiagnosaKerja { get; set; }
        public string ICD { get; set; }
        public string Rekomendasi { get; set; }
        public string Disposisi_Hidup_Mati { get; set; }
        public bool Disposisi_Hidup_BolehPulang { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Disposisi_Hidup_BolehPulang_Jam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Disposisi_Hidup_BolehPulang_Tanggal { get; set; }
        public string Disposisi_Hidup_KontrolPoliklinik_Ya_Tidak { get; set; }
        public string Disposisi_Hidup_KontrolPoliklinik_Ya_Ket { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Disposisi_Hidup_KontrolPoliklinik_Tanggal { get; set; }
        public bool Disposisi_Hidup_Dirawat { get; set; }
        public bool Disposisi_Hidup_Dirawat_Intensif { get; set; }
        public bool Disposisi_Hidup_Dirawat_RuangLain { get; set; }
        public string Disposisi_Hidup_Dirawat_RuangLain_Ket { get; set; }
        public bool Disposisi_Mati_DeathOnArrival { get; set; }
        public bool Disposisi_Mati_SetelahResusitasi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Disposisi_Mati_SetelahResusitasi_Jam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Disposisi_Mati_SetelahResusitasi_Tanggal { get; set; }
        public string Disposisi_Mati_PenyebabKematian { get; set; }
        public string CatatanPenting { get; set; }
        public string Dokter { get; set; }
        public string DokterNama { get; set; }
        public string Username { get; set; }
        public string TandaTangan { get; set; }
        public string TandaTanganDokter { get; set; }

        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public List<SelectItemListPenyakitBedah> ListTemplate { get; set; }
        // disable all form
        public int MODEVIEW { get; set; } 
    }

    public class SelectItemListPenyakitBedah
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}