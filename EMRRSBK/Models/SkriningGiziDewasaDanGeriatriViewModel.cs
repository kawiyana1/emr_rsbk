﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class SkriningGiziDewasaDanGeriatriViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string PenurunanBB { get; set; }
        public string PenurunanBB_Ket { get; set; }
        public string AsupanMakan { get; set; }
        public string TotalSkor { get; set; }
        public string PasienPenyakit { get; set; }
        public bool PasienPenyakit_Diabetes { get; set; }
        public bool PasienPenyakit_Sirosis { get; set; }
        public bool PasienPenyakit_Ginjal { get; set; }
        public bool PasienPenyakit_Hemodialisa { get; set; }
        public bool PasienPenyakit_Kanker { get; set; }
        public bool PasienPenyakit_Stroke { get; set; }
        public bool PasienPenyakit_PPOK { get; set; }
        public bool PasienPenyakit_Pneumonia { get; set; }
        public bool PasienPenyakit_LukaBakar { get; set; }
        public bool PasienPenyakit_Cedera { get; set; }
        public bool PasienPenyakit_Transplantasi { get; set; }
        public bool PasienPenyakit_Jantung { get; set; }
        public string KodePerawat { get; set; }
        public string ResikoMalnutrisi { get; set; }
        public string AsuhanGizi { get; set; }
        public string AsuhanGizi_Ket { get; set; }
        public string LilaAktual { get; set; }
        public string LilaStandar { get; set; }
        public string ULNA { get; set; }
        public string BB { get; set; }
        public string BBI { get; set; }
        public string TB { get; set; }
        public string IMT { get; set; }
        public string StatusGizi { get; set; }
        public string LILAPersen { get; set; }
        public string Kesimpulan { get; set; }
        public string LainLain { get; set; }
        public string KodeAhliGizi { get; set; }
        public string KodeAhliGiziNama { get; set; }
        public string Perawat { get; set; }
        public string PerawatNama { get; set; }
        public string Username { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        public string Skor1 { get; set; }
        public string Skor2 { get; set; }
    }
}