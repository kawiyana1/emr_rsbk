﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class RegistrasiPasienViewModel
    {
        // --MODEL PENGKAJIAN MEDIS  NEW
        public AssesmentAwalMedisNewViewModel AssAwalMedisNew { get; set; }
        public AssesmentAwalPerawatNewViewModel AssAwalPerawatNew { get; set; }

        // --MODEL PENGKAJIAN MEDIS IGD
        public HistoryPasienViewModel HistoryPasien { get; set; }

        // --MODEL PENGKAJIAN MEDIS IGD
        public PengkajianMedisViewModel PengkajianMedis { get; set; }
        public AssesmentPengkajianTerintetegrasiGawatDaruratViewModel PKGawatDarurat { get; set; }
        public AsesmenPengkajianTerintegrasiGawatDarurat_IIViewModel PKGawatDaruratII { get; set; }
        public LembarObservasiViewModel LembarObservasi { get; set; }

        // --MODEL PERSETUJUAN UMUM REGISTRASI

        public RegPersetujuanUmumViewModel RegPersetujuanUmum { get; set; }
        public RegSuratPernyataanViewModel SuratPernyataan { get; set; }
        public RegSuratPernyataanKenaikanKelasViewModel SuratPernyataanKenaikanKelas { get; set; }

        // --MODEL GENERAL IGD

        public EWSScorePemantauanViewModel EWSScore { get; set; }
        public CatatanPemindahanPasienAntarRSViewModelDetail CPPARS { get; set; }
        public ResumeMedisIGDViewModel rsmmedisIGD { get; set; }
        public ResumeMedisRJViewModel rsmmedisRJ { get; set; }
        public TindakanKedokteranIGDViewModel tndkkedokteranIGD { get; set; }
        public LaporanVisumETRepertumViewModel LPVisumETRepertum { get; set; }
        public SuratPersetujuanPemeriksaanPenyidikanViewModel SPPemeriksaanKepentinganPenyidikan { get; set; }
        public ResumeMedisBPJSViewModel ResumeMedisBPJS { get; set; }
        public RujukanPenderitaViewModel RjkPenderita { get; set; }
        public FormulirKeinginanPasienMemilihDPJPViewModel FormPasienDPJP { get; set; }
        public SuratKeteranganRujukanAmbulanViewModel RujukanAmbulan { get; set; }
        public JanganDilakukanResusitasiViewModel JngnRisusitasi { get; set; }
        public SuratPernyataanJanganDilakukanResusitasiViewModel SrtJngnRisusitasi { get; set; }
        public PengantarRIViewModel PengantarRawatInap { get; set; }
        public PenolakanRIViewModel PenolakanRawatInap { get; set; }
        public PersetujuanDirawatICUViewModel PDirawatICU { get; set; }
        public RMKasusPenderaAnakViewModel RMKPanak { get; set; }
        public RMDomesticViolenceViewModel RMDmsticViolnce { get; set; }
        public SuratKeteranganMasihDalamPerawatanViewModel SrtKetMasihDlmPerawatan { get; set; }

        // --MODEL IMLU PENYAKIT MATA 
        public AssesmentIlmuPenyakitMataViewModel AssMata { get; set; }

        // --MODEL GENERAL POLI MATA
        public CatatanEdukasiTerintegrasiRJViewModel CETRJ { get; set; }

        public GeneralPoliMataViewModel GMata { get; set; }

        // --MODEL POLI HIPERBARIK
        public AssesmentIKeperawatanHiperbarikViewModel AssHpbrk { get; set; }
        public AssesmentIKeperawatanHiperbarik2ViewModel AssHpbrk2 { get; set; }

        // --MODEL POLI KELAMIN DAN KULIT
        public AssesmentIlmuKulitDanKelaminViewModel AssKelaminKulit { get; set; }


        // --MODEL POLI KEBIDANAN
        public PengkajianAwalKebidananKandunganViewModel PAwalKbdnKndng { get; set; }
        public AssesmentIlmuPenyakitGynecologyViewModel Assgynclogy { get; set; }
        public AssesmentIlmuPenyakitObsetriViewModel Assobstri { get; set; }

        // --MODEL POLI GIGI
        public LembarPoliGigiViewModel LbrPGigi { get; set; }
        public CheckListKeselamatanPasienOperasiViewModel ChckKslmtnPasien { get; set; }

        // --MODEL POLI BEDAH
        public AssesmentIlmuPenyakitBedahNonTraumaViewModel AssBdhNonTraum { get; set; }
        public AssesmentIlmuPenyakitBedahTraumaViewModel AssBdhTraum { get; set; }

        // --MODEL POLI ANAK
        public AssesmentIlmuPenyakitAnakViewModel AssAnk { get; set; }

        // --MODEL POLI SARAF
        public AssesmentIlmuPenyakitSarafViewModel AssSrf { get; set; }

        // --MODEL POLI THT
        public AssesmentIlmuPenyakitTHTViewModel AssTHT { get; set; }

        // --MODEL POLI JIWA
        public AssesmentIlmuPenyakitJiwaViewModel AssJiwa { get; set; }

        // --MODEL POLI PENYAKIT DALAM
        public AssesmentIlmunPenyakitDalamViewModel AssDalam { get; set; }

        // --MODEL POLI PENYAKIT JANTUNG
        public AssesmentIlmuPenyakitJantungViewModel AssJantung { get; set; }

        // --MODEL POLI PENYAKIT PSIKOLOGI
        public AssesmentIlmuPenyakitPsikologiAnakRemajaViewModel AssPsiklogAnkRmj { get; set; }
        public AssesmentIlmuPenyakitPsikologiDewasaViewModel AssPsiklogDws { get; set; }
        public LembarPersetujuanViewModel LmbrPrstjuan { get; set; }

        // --MODEL POLI UMUM
        public AssesmentIPoliumumViewModel AssPUmum { get; set; }

        // --MODEL POLI MCU
        public KesimpulanHasilPemeriksaanKesehatanViewModel KsmplHslPmrksaanKshtn { get; set; }
        public LaporanPemeriksaanKesehatanMCUViewModel LprnPmksaanMCU { get; set; }

        // --MODEL POLI FISIOTHERAPI
        public LembarFormulirPoliFisiotherapiViewModel LmbrFormFisio { get; set; }
        public CatatanFisiotherapiViewModel CttFisio { get; set; }

        // -- MODEL OK
        public CatatanPemakaianObatDanAlkesKamarOperasiDokterBedahViewModel CttObatDanAlksKmr { get; set; }
        public LembarObservasiAnastesiLokalViewModel LmbrObsrvsiAnestesi { get; set; }
        public FormPerhitunganIntraoperatifViewModel FrmPhtnganIntraoper { get; set; }
        public AssesmentPraAnestesiDanSedasiViewModel AssPraAnesDanSedasi { get; set; }
        public LaporanOperasiViewModel LprnOprs { get; set; }
        public AssesmentIPraOperasiPerempuanViewModel AssPraOprsPrmpn { get; set; }
        public AssesmentIPraOperasiLakiLakiViewModel AssPraOprsLaki { get; set; }
        public CheckListKslamatanPasienOKViewModel ChckListKslmtnPsienOprs { get; set; }
        public CheckListKesiapanAnestesiViewModel ChckListKsiapanAnste { get; set; }
        public CheckListSerahTerimaPasienPreDanPostOperasiViewModel ChckListSrhtrmpasien { get; set; }
        public AsuhanKeperawatanPeriOperatifViewModel AshnPreOpera { get; set; }
        public AsuhanKeperawatanPostOperatifViewModel AshnPostOpera { get; set; }
        public AsuhanKeperawatanIntraOperatifViewModel AshnIntraOpera { get; set; }
        public EvaluasiPraOperasiViewModel EvPraOpr { get; set; }
        public LaporanPersalinanViewModel LprnPersln { get; set; }
        public SuratKeteranganIstirahatViewModel SKIstirahat { get; set; }
        public SuratKeteranganDokterViewModel SKDokter { get; set; }
        public PengkajianKeperawatanViewModel PnkjKeperwt { get; set; }


        // --MODEL RAWAT INAP
        public CatatanPelayananPasienViewModel CttPlyananPsien { get; set; }
        public CatatanVisiteDokterViewModel CttVstDkter { get; set; }
        public StrongkidsViewModel StrngKds { get; set; }
        public MonitorTransfusiDarahViewModel MntrTrnfsiDrh { get; set; }
        public CatatanPemberianObatViewModel CttPmbrianObt { get; set; }
        public TimPengendalianInfeksiNosokomialViewModel TimPngdlInfkNskmia { get; set; }
        public RencanaKeperawatanCemasViewModel RncanaKprwtCems { get; set; }
        public RencanaKeperawatanNyeriViewModel RncanaKprwtNyri { get; set; }
        public AssesmenUlangNyeriViewModel AssUlngNyri { get; set; }
        public RencanaKeperawatanDiareViewModel RncanaKprwtDiar { get; set; }
        public RencanaKeperawatanRisikoKekuranganVolumeCairanViewModel RncanaKprwtRskKrngCair { get; set; }
        public RencanaKeperawatanGangguanKekuranganNutrisiViewModel RncanaKprwtKkranganNtrs { get; set; }
        public RencanaKeperawatanHipertermiViewModel RncanaKprwtHptrmi { get; set; }
        public RencanaKeperawatanIntoleransiAktivitasViewModel RncanaKprwtIntolrnsi { get; set; }
        public RencanaKeperawatanPolaNafasTidakEfektifViewModel RncanaKprwtPolaNfsTdkEfektf { get; set; }
        public RencanaKeperawatanBersihanJalanNafasTidakEfektifViewModel RncanaKprwtBrshJlnNfs { get; set; }
        public RencanaKeperawatanPerubahanPerfusiJaringanSerebralViewModel RncanaKprwtPerfusi { get; set; }
        public CatatanAnastesiSedasiViewModel CttSedasiAnes { get; set; }
        public CatatanPemindahanPasienAntarRuanganViewModelDetail CPARuangan { get; set; }
        public RencanaPemulanganPasienViewModel RcnaPmlngn { get; set; }
        public FormulirDPJPViewModel FrmDPJP { get; set; }

        // --MODEL HEMODIALISA
        public AsuhanKeperawatanDanObservasiPasienHemodialisisViewModel AshnKprwtnObsrvsiHD { get; set; }
        public PengkajianAwalPasienDialisisViewModel PengkajianDialisisHD { get; set; }
        public AsuhanGiziTerapiMonitoringEvaluasiViewModel AsuhanGiziTrpEvlsi { get; set; }
        public SkriningGiziDewasaDanGeriatriViewModel GiziDanGeriatri { get; set; }
        public PeresapanHemodialisisViewModel PeresapanHD { get; set; }
        public SuratJawabanKonsulViewModel SuratJwbKonsul { get; set; }
        public AssesmentAwalPasienHemodialisisViewModel AsmntAwlPasienHemodialisis { get; set; }
        public StatusHemodialisViewModel StatusHemo { get; set; }
        public AsuhanGiziDewasaViewModel AsuhanGiziDewasa { get; set; }
        public BeritaMasukPerawatanViewModel BeritaMskPerawatan { get; set; }
        public BeritaLepasPerawatanViewModel BeritaLpsPerawatan { get; set; }
        public OrientasiPasienBaruRawatInapViewModel OrentasiPsnBaruRawatInap { get; set; }
        public AsuhanGiziAnakViewModel AshnGiziAnak { get; set; }
        public IdentitasPasienViewModel IdentitasPasien { get; set; }
        public FormulirRekonsiliasiObatViewModel FRekonsiliasiObat { get; set; }
        public PersetujuanUmumNewViewModel PU { get; set; }
        public EMRtrPengkajianAwalKeperawatanPasienHemodialisisViewModel PAKep_HD { get; set; }


        // --MODEL CPPT
        public SOAPViewModel CPPT { get; set; }
        public KunjunganPoliklinikViewModel KuPo { get; set; }
        public Renpra_RJViewModel R_RJ { get; set; }
        public AsesmenLanjutanKeperawatanViewModel AssLanjut_Kep { get; set; }
        public AsesmenLanjutanKebidananViewModel AssLanjut_Keb { get; set; }

        public string StatusBayar { get; set; }
        public string NoReg { get; set; }
        public string Deskripsi { get; set; }
        public System.DateTime TglReg { get; set; }
        public string TglReg_View { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }
        public string JenisKerjasama { get; set; }
        public string Nama_Customer { get; set; }
        public string NoKartu { get; set; }
        public bool RawatInap { get; set; }
        public int NomorNomor { get; set; }
        public string NamaKelas { get; set; }
        public string NamaDOkter { get; set; }
        public string NoKontak { get; set; }
        public string SpesialisName { get; set; }
        public bool SudahPeriksa { get; set; }
        public string NoBill { get; set; }
        public short NoAntri { get; set; }
        public string DokterID { get; set; }
        public bool Batal { get; set; }
        public string SectionAsalID { get; set; }
        public Nullable<int> WaktuID { get; set; }
        public bool MCU { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public System.DateTime Jam { get; set; }
        public string Jam_View { get; set; }
        public string JamMasuk { get; set; }
        public Nullable<bool> VIP { get; set; }
        public string VIPKeterangan { get; set; }
        public Nullable<bool> PasienAsuransi { get; set; }
        public string Nama_Asuransi { get; set; }
        public Nullable<decimal> CustomerKerjasamaID { get; set; }
        public string Pertanggungankeduacompany_name { get; set; }
        public Nullable<bool> PertanggunganKeduaHC { get; set; }
        public Nullable<bool> PertanggunganKeduaIKS { get; set; }
        public Nullable<bool> BPJSExecutive { get; set; }
        public string Alamat { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public string SectionAsalName { get; set; }
        public string Kamar { get; set; }
        public string NoBed { get; set; }
        public int Nomor { get; set; }
        public bool Active { get; set; }
        public bool Out { get; set; }
        public string AlasanBatal { get; set; }
        public string KelasID { get; set; }
        public string COmpanyID { get; set; }
        public bool PasienKTP { get; set; }
        public string Keterangan { get; set; }
        public string JabatanDiPerusahaan { get; set; }
        public string JenisPasien { get; set; }
        public string KdKelas { get; set; }
        public string Kode_Customer { get; set; }
        public int NoUrut { get; set; }
        public string Persetujuan { get; set; }
        public string Telp { get; set; }
        public bool RJ { get; set; }
        public Nullable<int> UmurThn { get; set; }
        public Nullable<int> UmurBln { get; set; }
        public Nullable<double> Berat { get; set; }
        public Nullable<byte> UmurHr { get; set; }
        public bool DariMutasi { get; set; }
        public bool RuangLamaDipakai { get; set; }
        public string KategoriPlafon { get; set; }
        public string NamaKasus { get; set; }
        public Nullable<int> JmlRIThnIni { get; set; }
        public Nullable<int> JmlRIOpnameIni { get; set; }
        public Nullable<bool> Titip { get; set; }
        public string KelasAsalID { get; set; }
        public Nullable<bool> Kerjasama { get; set; }
        public string KamarAsal { get; set; }
        public string NoBedAsal { get; set; }
        public Nullable<double> MarkUp { get; set; }
        public bool TermasukObat { get; set; }
        public string AsuransiID { get; set; }
        public string AssCompanyID_MA { get; set; }
        public Nullable<bool> Paket { get; set; }
        public string PaketJasaID { get; set; }
        public string Nama_Asisting { get; set; }
        public Nullable<decimal> ExcessFee { get; set; }
        public string NoAsuransi { get; set; }
        public Nullable<bool> BayiSAkit { get; set; }
        public Nullable<System.DateTime> TglLahir { get; set; }
        public bool TglLahirDiketahui { get; set; }
        public string KdKelasPertanggungan { get; set; }
        public Nullable<int> KelasPertanggungan_Nomor { get; set; }
        public Nullable<bool> JKN { get; set; }
        public Nullable<bool> BlokJKN { get; set; }
        public Nullable<int> KelasPertanggunganNomor { get; set; }
        public Nullable<bool> KelasPertanggungan_JKN { get; set; }
        public Nullable<bool> NONKELAS { get; set; }
        public Nullable<int> KelasPelayananNomor { get; set; }
        public Nullable<bool> KelasPelayananJKN { get; set; }
        public string PertanggunganKeduaCompanyID { get; set; }
        public string PertanggunganKeduaNoKartu { get; set; }
        public Nullable<int> PertanggunganKeduaCustomerKerjasamaID { get; set; }
        public Nullable<double> Beratbadan { get; set; }
        public Nullable<System.DateTime> TglRes { get; set; }
        public string Diagnosa { get; set; }
        public Nullable<bool> NaikKelas { get; set; }
        public string Phone { get; set; }
        public string NoregTemporary { get; set; }
        public string SectionID_Temporary { get; set; }
        public string KodeFormRI { get; set; }
        public string StatusLengkap { get; set; }
        public string NoIdentitas { get; set; }
        public string PenanggungNama { get; set; }
        public string PenanggungPhone { get; set; }
        public string Agama { get; set; }
        public string TempatLahir { get; set; }
        public int DeleteMfinger { get; set; }
        public string Template { get; set; }
        public string WargaNegara { get; set; }
        public string Pendidikan { get; set; }
        public string Pangkat { get; set; }
        public string Kesatuan { get; set; }
        public string Pekerjaan { get; set; }
        public string NIP { get; set; }
        public string StatusKawin { get; set; }
        public string NoBPJS { get; set; }
        public string HubunganPasien { get; set; }
        public string Desa { get; set; }
        public string Kecamatan { get; set; }
        public string Kabupaten { get; set; }
        public string Provinsi { get; set; }
        public string CekEdukasiRIRJ { get; set; }
        public string CekEdukasiTerintegrasi { get; set; }
        public string KeluhanUtama { get; set; }
        public string RIwayatPenyakitSekarang { get; set; }
        public string RiwayatPenyakitSebelumnya { get; set; }
        public string GCS_E { get; set; }
        public string GCS_V { get; set; }
        public string GCS_M { get; set; }
        public string BB { get; set; }
        public string PB { get; set; }
        public string TB { get; set; }
        public string TandaVital_Suhu { get; set; }
        public string TandaVital_Pernafasan { get; set; }
        public string TandaVital_Nadi { get; set; }
        public string TandaVital_TekananDarah { get; set; }
        public string PemeriksaanFisik { get; set; }
        public string Kepala { get; set; }
        public string Mata { get; set; }
        public string THT { get; set; }
        public string Leher { get; set; }
        public string Dada { get; set; }
        public string Abdomen { get; set; }
        public string Punggung { get; set; }
        public string Genetalia { get; set; }
        public string Ekstremitas { get; set; }
        public string DiagnosaMedis { get; set; }
        public string Instruksi { get; set; }
        public string DariTanggal { get; set; }
        public string SampaiTanggal { get; set; }
        public string StatusCPPTHD { get; set; }
        public bool SudahIsiAsesmen { get; set; }
        public bool SudahIsiGeneral { get; set; }
        public bool SudahIsiKunjunganPoliklinik { get; set; }
        public string NoKamar { get; set; }
        public string InputGeneral1 { get; set; }
        public string InputGeneral2 { get; set; }
        public string InputGeneral3 { get; set; }
        public string InputGeneral4 { get; set; }
        public string InputGeneral5 { get; set; }
        public string InputGeneral6 { get; set; }
        public string InputGeneral7 { get; set; }
        public string InputGeneral8 { get; set; }
        public string InputGeneral9 { get; set; }
        public string InputGeneral10 { get; set; }
        public string InputGeneral11 { get; set; }
        public string InputGeneral12 { get; set; }
        public string InputGeneral13 { get; set; }
        public string InputGeneral14 { get; set; }
        public string InputGeneral15 { get; set; }
        public string InputGeneral16 { get; set; }
        public string InputGeneral17 { get; set; }
        public string InputGeneral18 { get; set; }
        public string InputGeneral19 { get; set; }
        public string InputGeneral20 { get; set; }
        public string InputGeneral21 { get; set; }
        public string InputGeneral22 { get; set; }
        public string InputGeneral23 { get; set; }
        public string InputGeneral24 { get; set; }
        public string InputGeneral25 { get; set; }
        public string InputGeneral26 { get; set; }
        public string InputGeneral27 { get; set; }
        public string InputGeneral28 { get; set; }
        public string InputGeneral29 { get; set; }
        public string InputGeneral30 { get; set; }
        public string InputGeneral31 { get; set; }
        public string InputGeneral32 { get; set; }
        public string InputGeneral33 { get; set; }
        public string InputGeneral34 { get; set; }
        public string InputGeneral35 { get; set; }
        public string InputGeneral36 { get; set; }
        public string InputGeneral37 { get; set; }
        public string InputGeneral38 { get; set; }
        public string InputGeneral39 { get; set; }
        public string InputGeneral40 { get; set; }
        public string InputGeneral41 { get; set; }
        public string InputGeneral42 { get; set; }
        public string InputGeneral43 { get; set; }
        public string InputGeneral44 { get; set; }
        public string InputGeneral45 { get; set; }
        public string InputGeneral46 { get; set; }
        public string InputGeneral47 { get; set; }
        public string InputGeneral48 { get; set; }
        public string _METHOD { get; set; }
    }
}