﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class AssesmentIlmuPenyakitPsikologiDewasaViewModel
    {
        public ListDetail<AssesmentIlmuPenyakitPsikologiIdentitasModelDetail> IdentitasPsikol_List { get; set; }
        public ListDetail<AssesmentIlmuPenyakitPsikologiTesModelDetail> TestPsikol_List { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string StatusPerkawinan { get; set; }
        public string NamaPerujuk { get; set; }
        public string HubunganPerujuk { get; set; }
        public string KontakKeluarga { get; set; }
        public string NamaPasangan { get; set; }
        public string UmurPasangan { get; set; }
        public string PendidikanPasangan { get; set; }
        public string PekerjaanPasangan { get; set; }
        public string AgamaPasangan { get; set; }
        public string AnakKePasangan { get; set; }
        public string AnakDariPasangan { get; set; }
        public string PerkawinanKePasangan { get; set; }
        public string PerkawinanDariPasangan { get; set; }
        public string AlamatPasangan { get; set; }
        public string NoHPPasangan { get; set; }
        public string KeteranganPasangan { get; set; }
        public string KeluhanUtamaPasangan { get; set; }
        public bool Keluhan_Depresi { get; set; }
        public bool Keluhan_Kecemasan { get; set; }
        public bool Keluhan_MenyalahkanOrang { get; set; }
        public bool Keluhan_Menangis { get; set; }
        public bool Keluhan_Ketakutan { get; set; }
        public bool Keluhan_MerasakanTidakAda { get; set; }
        public bool Keluhan_GangguanTidur { get; set; }
        public bool Keluhan_Gemetaran { get; set; }
        public bool Keluhan_Harapan { get; set; }
        public bool Keluhan_GangguanNafsuMakan { get; set; }
        public bool Keluhan_Jantung { get; set; }
        public bool Keluhan_Marah { get; set; }
        public bool Keluhan_KesulitanKonsentrasi { get; set; }
        public bool Keluhan_Berkeringat { get; set; }
        public bool Keluhan_Depersonalisasi { get; set; }
        public bool Keluhan_Ingatan { get; set; }
        public bool Keluhan_Derealisasi { get; set; }
        public bool Keluhan_IsolasiSosial { get; set; }
        public bool Keluhan_Pusing { get; set; }
        public bool Keluhan_Obsesif { get; set; }
        public bool Keluhan_MenarikDiri { get; set; }
        public bool Keluhan_Kelelahan { get; set; }
        public bool Keluhan_PermasalahanRelasi { get; set; }
        public bool Keluhan_SakitKepala { get; set; }
        public bool Keluhan_MudahKesal { get; set; }
        public bool Keluhan_PermasalahanKeluarga { get; set; }
        public bool Keluhan_PermasalahanTerkait { get; set; }
        public bool Keluhan_IsuKehilangan { get; set; }
        public bool Keluhan_SaluranPencernaan { get; set; }
        public bool Keluhan_Stress { get; set; }
        public bool Keluhan_IdeBunuhDiri { get; set; }
        public bool Keluhan_Asma { get; set; }
        public bool Keluhan_PermasalahanPekerjaan { get; set; }
        public bool Keluhan_IdeMembunuh { get; set; }
        public bool Keluhan_SangatWaspada { get; set; }
        public bool Keluhan_PermasalahanLegal { get; set; }
        public bool Keluhan_Kekerasan { get; set; }
        public bool Keluhan_Pikiran { get; set; }
        public bool Keluhan_Hiperaktif { get; set; }
        public bool Keluhan_GangguanMakan { get; set; }
        public bool Keluhan_Mania { get; set; }
        public bool Keluhan_MalanggarAturan { get; set; }
        public bool Keluhan_PermasalahanSekolah { get; set; }
        public bool Keluhan_AduArgumen { get; set; }
        public bool Keluhan_Mendendam { get; set; }
        public bool Keluhan_MenggunakanNapza { get; set; }
        public bool Keluhan_BahasaCabul { get; set; }
        public bool Keluhan_MembuatJengkel { get; set; }
        public bool Keluhan_PermalahanSomatis { get; set; }
        public bool Keluhan_MinumBeralkohol { get; set; }
        public bool Keluhan_Lainnya { get; set; }
        public string Keluhan_Lainnya_Ket { get; set; }
        public string Wawancara_SudahBerapaLama { get; set; }
        public string Wawancara_AlasanMencariBantuan { get; set; }
        public string Wawancara_PermasalahanSaatIni { get; set; }
        public string Wawancara_PermasalahanSaatIni_Ket { get; set; }
        public string Wawancara_PerubahanDariDirisendiri { get; set; }
        public string Wawancara_PerubahanDariKeluarga { get; set; }
        public string MasaKanakKanak { get; set; }
        public string MasaRemaja { get; set; }
        public string MasaDewasa { get; set; }
        public string RiwayatPenyakitFisik { get; set; }
        public string RiwayatPengobatan { get; set; }
        public string Penampilan { get; set; }
        public string EkspresiWajah { get; set; }
        public string PerasaanSuasanaHati { get; set; }
        public string TingkahLaku { get; set; }
        public string FungsiUmum { get; set; }
        public string FungsiUmum_Ket { get; set; }
        public string FungsiIntelektual { get; set; }
        public string PengalamanKerja { get; set; }
        public string ObservasiLainnya { get; set; }
        public string Delusi { get; set; }
        public string Delusi_Lainnya { get; set; }
        public string ProsesPikiran { get; set; }
        public string Halusinasi { get; set; }
        public string Afek { get; set; }
        public string Insight { get; set; }
        public string Kesadaran { get; set; }
        public string OrientasiWaktu { get; set; }
        public string OrientasiTempat { get; set; }
        public string OrientasiRuang { get; set; }
        public string Perhatian { get; set; }
        public string Penilaian { get; set; }
        public string KontrolTerhadapImpus { get; set; }
        public string DinamikaPsikologs { get; set; }
        public string KesanAwal { get; set; }
        public string DiagnosaUtama { get; set; }
        public string DiagnosaBanding { get; set; }
        public string FungsiPsikologis { get; set; }
        public string Prognosis { get; set; }
        public bool PsikoterapiSingkat { get; set; }
        public bool EvaluasiMedikasi { get; set; }
        public bool PsikoterapiSuportif { get; set; }
        public bool RujukanMedis { get; set; }
        public bool MenurunkanGejala { get; set; }
        public bool MeningkatkanKoping { get; set; }
        public bool MenstabilkanKondisi { get; set; }
        public bool MengoptimalkanSumberDaya { get; set; }
        public bool RestrukturisasiKognitig { get; set; }
        public bool PerlindunganAnak { get; set; }
        public bool MelatihKeterampilanSosial { get; set; }
        public bool ProgramDukunganKelompok { get; set; }
        public bool ResolusiKonflik { get; set; }
        public bool KetergantunganZat { get; set; }
        public bool ManajemenStres { get; set; }
        public bool MeningkatkanHargaDiri { get; set; }
        public bool ModifikasiPrilaku { get; set; }
        public bool KonselingOrangtua { get; set; }
        public bool ManajemenNyeri { get; set; }
        public bool ResolusiKedukaan { get; set; }
        public bool KewaspadaanBunuhDiri { get; set; }
        public bool ProgramRawatInap { get; set; }
        public bool KewaspadaanHukum { get; set; }
        public string PertemuanSelanjutnya { get; set; }
        public string CatatanTambahan { get; set; }
        public string DokterPemeriksa { get; set; }
        public string DokterPemeriksaNama { get; set; }
        public string STR { get; set; }
        public string SIPPK { get; set; }
        public string Username { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        public string NamaPasien { get; set; }
        public string TempatLahir { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
        public int? UmurThn { get; set; }
        public string JenisKelamin { get; set; }
        public string Pendidikan { get; set; }
        public string Pekerjaan { get; set; }
        public string StatusKawin { get; set; }
        public string KontakKluarga { get; set; }
        public int SudahRegDokterPemeriksa { get; set; }
        public string TandaTanganDokterPemeriksa { get; set; }
    }
}