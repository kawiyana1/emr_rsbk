﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class EMRHeaderViewDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int Nomor { get; set; }
        public int MODEVIEW { get; set; }
        public ListDetail<EMRAsesmenUlangNyeriViewModel> RIAssUlangNyeri_List { get; set; }
    }
}