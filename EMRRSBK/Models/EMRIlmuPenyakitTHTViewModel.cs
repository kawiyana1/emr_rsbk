﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EMRRSBK.Models
{
    public class EMRIlmuPenyakitTHTViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public bool Rujukan_Ya { get; set; }
        public bool Rujukan_RS { get; set; }
        public string Rujukan_RS_Ket { get; set; }
        public bool Rujukan_Dr { get; set; }
        public string Rujukan_Dr_Ket { get; set; }
        public bool Rujukan_Puskesmas { get; set; }
        public string Rujukan_Puskesmas_Ket { get; set; }
        public bool Rujukan_Lainnya { get; set; }
        public string Rujukan_Lainnya_Ket { get; set; }
        public bool Rujukan_Dx { get; set; }
        public string Rujukan_Dx_Ket { get; set; }
        public bool Rujukan_Tidak { get; set; }
        public bool Rujukan_DatangSendiri { get; set; }
        public bool Rujukan_Diantar { get; set; }
        public string Rujukan_Diantar_Ket { get; set; }
        public string Asuransi { get; set; }
        public string NamaKeluarga { get; set; }
        public string AlamatKeluarga { get; set; }
        public string TeleponKeluarga { get; set; }
        public bool Tranportasi_AbulanRS { get; set; }
        public bool Tranportasi_AbulanLain { get; set; }
        public string Tranportasi_AbulanLain_Ket { get; set; }
        public bool Tranportasi_KendaranLain { get; set; }
        public string Tranportasi_KendaranLain_Ket { get; set; }
        public string Riwayat_Alergi { get; set; }
        public string AlergiObat { get; set; }
        public string AlergiObat_Ket { get; set; }
        public string AlergiMakanan { get; set; }
        public string AlergiMakanan_Ket { get; set; }
        public string AlergiLainnya { get; set; }
        public string AlergiLainnya_Ket { get; set; }
        public string Nyeri { get; set; }
        public string Lokasi { get; set; }
        public string Intensitas { get; set; }
        public string Jenis { get; set; }
        public string KeluhanUtama { get; set; }
        public string KeluhanTambahan { get; set; }
        public bool TelingaSekret { get; set; }
        public string TelingaSekret_Kanan { get; set; }
        public string TelingaSekret_Kiri { get; set; }
        public bool TelingaTuli { get; set; }
        public string TelingaTuli_Kanan { get; set; }
        public string TelingaTuli_Kiri { get; set; }
        public bool TelingaTumor { get; set; }
        public string TelingaTumor_Kanan { get; set; }
        public string TelingaTumor_Kiri { get; set; }
        public bool TelingaTunitas { get; set; }
        public string TelingaTunitas_Kanan { get; set; }
        public string TelingaTunitas_Kiri { get; set; }
        public bool TelingaSakit { get; set; }
        public string TelingaSakit_Kanan { get; set; }
        public string TelingaSakit_Kiri { get; set; }
        public bool TelingaKorpus { get; set; }
        public string TelingaKorpus_Kanan { get; set; }
        public string TelingaKorpus_Kiri { get; set; }
        public bool TelingaVertigo { get; set; }
        public string TelingaVertigo_Kanan { get; set; }
        public string TelingaVertigo_Kiri { get; set; }
        public bool HidungSekret { get; set; }
        public string HidungSekret_Kanan { get; set; }
        public string HidungSekret_Kiri { get; set; }
        public bool HidungTersembat { get; set; }
        public string HidungTersembat_Kanan { get; set; }
        public string HidungTersembat_Kiri { get; set; }
        public bool HidungTumor { get; set; }
        public string HidungTumor_Kanan { get; set; }
        public string HidungTumor_Kiri { get; set; }
        public bool HidungPilek { get; set; }
        public string HidungPilek_Kanan { get; set; }
        public string HidungPilek_Kiri { get; set; }
        public bool HidungSakit { get; set; }
        public string HidungSakit_Kanan { get; set; }
        public string HidungSakit_Kiri { get; set; }
        public bool HidungKorpus { get; set; }
        public string HidungKorpus_Kanan { get; set; }
        public string HidungKorpus_Kiri { get; set; }
        public bool HidungBersin { get; set; }
        public string HidungBersin_Kanan { get; set; }
        public string HidungBersin_Kiri { get; set; }
        public bool TenggorokanRiak { get; set; }
        public string TenggorokanRiak_Ket { get; set; }
        public bool TenggorokanGangguan { get; set; }
        public string TenggorokanGangguan_Ket { get; set; }
        public bool TenggorokanSuara { get; set; }
        public string TenggorokanSuara_Ket { get; set; }
        public bool TenggorokanTumor { get; set; }
        public string TenggorokanTumor_Ket { get; set; }
        public bool TenggorokanBatuk { get; set; }
        public string TenggorokanBatuk_Ket { get; set; }
        public bool TenggorokanKorpus { get; set; }
        public string TenggorokanKorpus_Ket { get; set; }
        public bool TenggorokanSesak { get; set; }
        public string TenggorokanSesak_Ket { get; set; }
        public string KeadaanUmum { get; set; }
        public string TekananDarah { get; set; }
        public string Nadi { get; set; }
        public string Respirasi { get; set; }
        public string Suhu { get; set; }
        public string Cor { get; set; }
        public string Pulmo { get; set; }
        public string Hepar { get; set; }
        public string StatusTHT { get; set; }
        public bool DaunTelingga { get; set; }
        public string DaunTelingga_Kanan { get; set; }
        public string DaunTelingga_Kiri { get; set; }
        public bool LiangTelingga { get; set; }
        public string LiangTelingga_Kanan { get; set; }
        public string LiangTelingga_Kiri { get; set; }
        public bool TelinggaDischarge { get; set; }
        public string TelinggaDischarge_Kanan { get; set; }
        public string TelinggaDischarge_Kiri { get; set; }
        public bool MembranTimpani { get; set; }
        public string MembranTimpani_Kanan { get; set; }
        public string MembranTimpani_Kiri { get; set; }
        public bool TelinggaTumor { get; set; }
        public string TelinggaTumor_Kanan { get; set; }
        public string TelinggaTumor_Kiri { get; set; }
        public bool Mastoid { get; set; }
        public string Mastoid_Kanan { get; set; }
        public string Mastoid_Kiri { get; set; }
        public bool Berbisik { get; set; }
        public string Berbisik_Kanan { get; set; }
        public string Berbisik_Kiri { get; set; }
        public bool Weber { get; set; }
        public string Weber_Kanan { get; set; }
        public string Weber_Kiri { get; set; }
        public bool Rinne { get; set; }
        public string Rinne_Kanan { get; set; }
        public string Rinne_Kiri { get; set; }
        public bool Schwabach { get; set; }
        public string Schwabach_Kanan { get; set; }
        public string Schwabach_Kiri { get; set; }
        public bool BOA { get; set; }
        public string BOA_Kanan { get; set; }
        public string BOA_Kiri { get; set; }
        public bool Tympanometri { get; set; }
        public string Tympanometri_Kanan { get; set; }
        public string Tympanometri_Kiri { get; set; }
        public bool TelingaAudiometri { get; set; }
        public string TelingaAudiometri_Kanan { get; set; }
        public string TelingaAudiometri_Kiri { get; set; }
        public bool TelinggaNadaMurni { get; set; }
        public string TelinggaNadaMurni_Kanan { get; set; }
        public string TelinggaNadaMurni_Kiri { get; set; }
        public bool TelinggaBERA { get; set; }
        public string TelinggaBERA_Kanan { get; set; }
        public string TelinggaBERA_Kiri { get; set; }
        public bool TelinggaOAE { get; set; }
        public string TelinggaOAE_Kanan { get; set; }
        public string TelinggaOAE_Kiri { get; set; }
        public bool TestAlat { get; set; }
        public string Keseimbangan_Kanan { get; set; }
        public string Keseimbangan_Kiri { get; set; }
        public bool HidungLuar { get; set; }
        public string HidungLuar_Kanan { get; set; }
        public string HidungLuar_Kiri { get; set; }
        public bool KanvumNasi { get; set; }
        public string KanvumNasi_Kanan { get; set; }
        public string KanvumNasi_Kiri { get; set; }
        public bool Septum { get; set; }
        public string Septum_Kanan { get; set; }
        public string Septum_Kiri { get; set; }
        public bool HidungDischarge { get; set; }
        public string HidungDischarge_Kanan { get; set; }
        public string HidungDischarge_Kiri { get; set; }
        public bool Mukosa { get; set; }
        public string Mukosa_Kanan { get; set; }
        public string Mukosa_Kiri { get; set; }
        public bool TumorHidung { get; set; }
        public string TumorHidung_Kanan { get; set; }
        public string TumorHidung_Kiri { get; set; }
        public bool Konka { get; set; }
        public string Konka_Kanan { get; set; }
        public string Konka_Kiri { get; set; }
        public bool Sinus { get; set; }
        public string Sinus_Kanan { get; set; }
        public string Sinus_Kiri { get; set; }
        public bool Koana { get; set; }
        public string Koana_Kanan { get; set; }
        public string Koana_Kiri { get; set; }
        public bool Naso { get; set; }
        public string Naso_Kanan { get; set; }
        public string Naso_Kiri { get; set; }
        public bool TenggorokanDispenu { get; set; }
        public string TenggorokanDispenu_Ket { get; set; }
        public bool TenggorokanSianosis { get; set; }
        public string TenggorokanSianosis_Ket { get; set; }
        public bool TenggorokanMucosa { get; set; }
        public string TenggorokanMucosa_Ket { get; set; }
        public bool TenggorokanDinding { get; set; }
        public string TenggorokanDinding_Ket { get; set; }
        public bool TenggorokanStridor { get; set; }
        public string TenggorokanStridor_Ket { get; set; }
        public bool SuaraTenggorokan { get; set; }
        public string SuaraTenggorokan_Ket { get; set; }
        public bool TenggorokanTonsil { get; set; }
        public string TenggorokanTonsil_Ket { get; set; }
        public bool LaringEpiglotis { get; set; }
        public string LaringEpiglotis_Ket { get; set; }
        public bool LaringAritenoid { get; set; }
        public string LaringAritenoid_Ket { get; set; }
        public bool LaringVentrikuloris { get; set; }
        public string LaringVentrikuloris_Ket { get; set; }
        public bool LaringEndoskopi { get; set; }
        public string LaringEndoskopi_Ket { get; set; }
        public bool LaringVokalis { get; set; }
        public string LaringVokalis_Ket { get; set; }
        public bool LaringRimaglotis { get; set; }
        public string LaringRimaglotis_Ket { get; set; }
        public string Diagnosa { get; set; }
        public string Kelenjar { get; set; }
        public string Terapi { get; set; }
        public bool Disposisi_BolehPulang { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> BolehPulangJam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> BolehPulangTgl { get; set; }
        public string Kontrol { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Kontrol_Tanggal { get; set; }
        public string Catatan { get; set; }
        public string DokterBPJP { get; set; }
        public string DokterBPJPNama { get; set; }
        public string Username { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }

        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public List<SelectItemListPenyakitTHT> ListTemplate { get; set; }
        // disable all form
        public int MODEVIEW { get; set; } 
    }

    public class SelectItemListPenyakitTHT
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}