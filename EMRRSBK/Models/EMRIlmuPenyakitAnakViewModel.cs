﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class EMRIlmuPenyakitAnakViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public bool Rujukan_Ya { get; set; }
        public string Rujukan_Ya_Ket { get; set; }
        public string Rujukan_Ya_Dx { get; set; }
        public bool Rujukan_Tidak { get; set; }
        public string NamaKeluarga { get; set; }
        public string AlamatKeluarga { get; set; }
        public string TeleponKeluarga { get; set; }
        public string Transportasi { get; set; }
        public string Transportasi_Ket { get; set; }
        public string Riwayat_Alergi { get; set; }
        public string Riwayat_Alergi_Ket { get; set; }
        public string KeluhanUtama { get; set; }
        public string RiwayatPenyakitSekarang { get; set; }
        public bool RiwayatPenyakitDahulu_Hepertensi { get; set; }
        public bool RiwayatPenyakitDahulu_Jantung { get; set; }
        public bool RiwayatPenyakitDahulu_Asthma { get; set; }
        public bool RiwayatPenyakitDahulu_Lainnya { get; set; }
        public string RiwayatPenyakitDahulu_Lainnya_Ket { get; set; }
        public string RiwayatPenyakitPengobatan { get; set; }
        public bool RiwayatPenyakitKeluarga_Hepertensi { get; set; }
        public bool RiwayatPenyakitKeluarga_DM { get; set; }
        public bool RiwayatPenyakitKeluarga_Jantung { get; set; }
        public bool RiwayatPenyakitKeluarga_Asthma { get; set; }
        public bool RiwayatPenyakitKeluarga_Lainnya { get; set; }
        public string RiwayatPenyakitKeluarga_Lainnya_Ket { get; set; }
        public string RiwayatSosial { get; set; }
        public bool RiwayatImunisasai_BCG { get; set; }
        public bool RiwayatImunisasai_Polio { get; set; }
        public string RiwayatImunisasai_Polio_Ket { get; set; }
        public bool RiwayatImunisasai_HepatitisB { get; set; }
        public string RiwayatImunisasai_HepatitisB_Ket { get; set; }
        public bool RiwayatImunisasai_DPT { get; set; }
        public string RiwayatImunisasai_DPT_Ket { get; set; }
        public bool RiwayatImunisasai_MR { get; set; }
        public string RiwayatImunisasai_MR_Ket { get; set; }
        public bool RiwayatImunisasai_IPV { get; set; }
        public string RiwayatImunisasai_IPV_Ket { get; set; }
        public bool RiwayatImunisasai_JE { get; set; }
        public string RiwayatImunisasai_JE_Ket { get; set; }
        public bool RiwayatImunisasai_Lainnya { get; set; }
        public string RiwayatImunisasai_Lainnya_Ket { get; set; }
        public string RiwayatPersalinan { get; set; }
        public bool Ditolong_Dokter { get; set; }
        public bool Ditolong_Bidan { get; set; }
        public bool Ditolong_Lainnya { get; set; }
        public string Ditolong_Lainnya_Ket { get; set; }
        public string RiwayatPersalinan_Berat { get; set; }
        public string RiwayatPersalinan_Panjang { get; set; }
        public string RiwayatPersalinan_LingkarKepala { get; set; }
        public string KeadaanLahir { get; set; }
        public bool ASI_Eksklusif { get; set; }
        public string ASI_Eksklusif_Ket { get; set; }
        public bool ASI_Durasi { get; set; }
        public string ASI_Durasi_Ket { get; set; }
        public bool ASI_Frekuensi { get; set; }
        public string ASI_Frekuensi_Ket { get; set; }
        public bool Susu_SejakUsia { get; set; }
        public string Susu_SejakUsia_Ket { get; set; }
        public bool Susu_Frekuensi { get; set; }
        public string Susu_Frekuensi_Ket { get; set; }
        public bool Bubur_SejakUsia { get; set; }
        public string Bubur_SejakUsia_Ket { get; set; }
        public bool Bubur_Frekuensi { get; set; }
        public string Bubur_Frekuensi_Ket { get; set; }
        public bool Nasi_SejakUsia { get; set; }
        public string Nasi_SejakUsia_Ket { get; set; }
        public bool Nasi_Frekuensi { get; set; }
        public string Nasi_Frekuensi_Ket { get; set; }
        public bool Makanan_SejakUsia { get; set; }
        public string Makanan_SejakUsia_Ket { get; set; }
        public bool Makanan_Frekuensi { get; set; }
        public string Makanan_Frekuensi_Ket { get; set; }
        public bool RiwayatTumbuh_Menegak { get; set; }
        public string RiwayatTumbuh_Menegak_Ket { get; set; }
        public bool RiwayatTumbuh_Membalik { get; set; }
        public string RiwayatTumbuh_Membalik_Ket { get; set; }
        public bool RiwayatTumbuh_Duduk { get; set; }
        public string RiwayatTumbuh_Duduk_Ket { get; set; }
        public bool RiwayatTumbuh_Merangkak { get; set; }
        public string RiwayatTumbuh_Merangkak_Ket { get; set; }
        public bool RiwayatTumbuh_Berdiri { get; set; }
        public string RiwayatTumbuh_Berdiri_Ket { get; set; }
        public bool RiwayatTumbuh_Berjalan { get; set; }
        public string RiwayatTumbuh_Berjalan_Ket { get; set; }
        public bool RiwayatTumbuh_Bicara { get; set; }
        public string RiwayatTumbuh_Bicara_Ket { get; set; }
        public string RiwayatOperasi { get; set; }
        public string RiwayatOperasi_Ket { get; set; }
        public string RiwayatTranfusi { get; set; }
        public string ReaksiTranfusi { get; set; }
        public string ReaksiTranfusi_Ket { get; set; }
        public string Nyeri { get; set; }
        public string Lokasi { get; set; }
        public string Intensitas { get; set; }
        public string Jenis { get; set; }
        public string KeadaanUmum { get; set; }
        public string Gizi { get; set; }
        public string GCS_E { get; set; }
        public string GCS_M { get; set; }
        public string GCS_V { get; set; }
        public string Resusitas { get; set; }
        public string BB { get; set; }
        public string TB { get; set; }
        public string Tensi { get; set; }
        public string SuhuAxila { get; set; }
        public string SuhuRectal { get; set; }
        public string Nadi { get; set; }
        public string Respirasi { get; set; }
        public string Saturasi { get; set; }
        public bool Pada_SuhuRuangan { get; set; }
        public bool Pada_Nasal { get; set; }
        public bool Pada_NRB { get; set; }
        public bool Kelapa_Normal { get; set; }
        public bool Kelapa_Mikrosefali { get; set; }
        public bool Kelapa_Makrosefali { get; set; }
        public bool Kelapa_Lainnya { get; set; }
        public string Kelapa_Lainnya_Ket { get; set; }
        public string Konjungtiva { get; set; }
        public string Hiperemi { get; set; }
        public string Sekret { get; set; }
        public string Seklera { get; set; }
        public string Pupil { get; set; }
        public string Reflek { get; set; }
        public string Edema { get; set; }
        public bool THT_Telinga { get; set; }
        public string THT_Telinga_Ket { get; set; }
        public bool THT_Hidung { get; set; }
        public string THT_Hidung_Ket { get; set; }
        public bool THT_Tenggorokan { get; set; }
        public string Faring { get; set; }
        public string Tonsil { get; set; }
        public bool THT_Lidah { get; set; }
        public string THT_Lidah_Ket { get; set; }
        public bool THT_Bibir { get; set; }
        public string THT_Bibir_Ket { get; set; }
        public bool Leher_JVP { get; set; }
        public string Leher_JVP_Ket { get; set; }
        public bool Leher_Kelenjar { get; set; }
        public string Leher_Kelenjar_Ket { get; set; }
        public string Ukuran { get; set; }
        public bool Leher_Tunggal { get; set; }
        public bool Leher_Multipel { get; set; }
        public bool Leher_Kaku { get; set; }
        public string Leher_Kaku_Ket { get; set; }
        public bool Leher_Lainnya { get; set; }
        public string Leher_Lainnya_Ket { get; set; }
        public string Thoraks { get; set; }
        public string Thoraks_Ket { get; set; }
        public bool Cor_S1S2 { get; set; }
        public string Cor_S1S2_Ket { get; set; }
        public bool Cor_Murmur { get; set; }
        public string Cor_Murmur_Ket { get; set; }
        public bool Cor_Lainnya { get; set; }
        public string Cor_Lainnya_Ket { get; set; }
        public bool Pulmo_SuaraNafas { get; set; }
        public string Pulmo_SuaraNafas_Ket { get; set; }
        public bool Pulmo_Ronchi { get; set; }
        public string Pulmo_Ronchi_Ket { get; set; }
        public bool Pulmo_Wheezing { get; set; }
        public string Pulmo_Wheezing_Ket { get; set; }
        public bool Pulmo_Lainnya { get; set; }
        public string Pulmo_Lainnya_Ket { get; set; }
        public bool Abdomen_Distensi { get; set; }
        public string Abdomen_Distensi_Ket { get; set; }
        public bool Abdomen_Nyeri { get; set; }
        public string Abdomen_Nyeri_Ket { get; set; }
        public string Abdomen_Nyeri_Lokasi { get; set; }
        public bool Abdomen_Meteorismus { get; set; }
        public string Abdomen_Meteorismus_Ket { get; set; }
        public bool Abdomen_Peristaltik { get; set; }
        public string Abdomen_Peristaltik_Ket { get; set; }
        public bool Abdomen_Turgor { get; set; }
        public string Abdomen_Turgor_Ket { get; set; }
        public bool Abdomen_Asites { get; set; }
        public string Abdomen_Asites_Ket { get; set; }
        public string Hepar { get; set; }
        public string Lien { get; set; }
        public string Massa { get; set; }
        public bool Ekstremitas_HangatDingin { get; set; }
        public string Ekstremitas_HangatDingin_Ket { get; set; }
        public bool Ekstremitas_Edema { get; set; }
        public string Ekstremitas_Edema_Ket { get; set; }
        public bool Ekstremitas_Capillary { get; set; }
        public string Ekstremitas_Capillary_Ket { get; set; }
        public bool Ekstremitas_Lainnya { get; set; }
        public string Ekstremitas_Lainnya_Ket { get; set; }
        public string Kulit { get; set; }
        public string Genitalia { get; set; }
        public bool Pubertas_Perempuan { get; set; }
        public string Perempuan_M { get; set; }
        public string Perempuan_P { get; set; }
        public bool Pubertas_LakiLaki { get; set; }
        public string LakiLaki_G { get; set; }
        public string LakiLaki_P { get; set; }
        public string StatusAntropometri { get; set; }
        public string Antropometri_BB { get; set; }
        public string Antropometri_PB { get; set; }
        public string Antropometri_TB { get; set; }
        public string Antropometri_BBI { get; set; }
        public string Penunjang { get; set; }
        public string RencanaKerja { get; set; }
        public string Diagnosa { get; set; }
        public string Terapi { get; set; }
        public bool Disposisi_BolehPulang { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> BolehPulangJam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> BolehPulangTgl { get; set; }
        public string KontrolPoliklinik { get; set; }
        public string KontrolPoliklinik_Ket { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> KontrolPoliklinik_Tanggal { get; set; }
        public bool Disposisi_Dirawat { get; set; }
        public string Disposisi_Dirawat_Ket { get; set; }
        public string RuanganLain { get; set; }
        public string DokterBPJP { get; set; }
        public string DokterBPJPNama { get; set; }
        public string Username { get; set; }
        public string S1RegularIregular { get; set; }
        public string Lku { get; set; }
        public string PeningkatanBB { get; set; }
        public bool Pada_Sungkup { get; set; }
        public string TandaTanganDPJP { get; set; }
        public string TandaTangan { get; set; }
        
        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public List<SelectItemListPenyakitAnak> ListTemplate { get; set; }
        // disable all form
        public int MODEVIEW { get; set; } 
    }

    public class SelectItemListPenyakitAnak
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}