﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class PenolakanRIViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Nama { get; set; }
        public string Umur { get; set; }
        public string Jenis { get; set; }
        public string Alamat { get; set; }
        public bool TujuanPasien { get; set; }
        public bool Resiko { get; set; }
        public bool Peringatan { get; set; }
        public bool Perkiraan { get; set; }
        public string Terhadap { get; set; }
        public string TerhadapLainnya { get; set; }
        public string Dokter { get; set; }
        public string DokterNama { get; set; }
        public string MembuatPernyataan { get; set; }
        public string Saksi1 { get; set; }
        public string Saksi2 { get; set; }
        public string Username { get; set; }
        public string NamaPasien { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
        public string JenisKelamin { get; set; }
        public string AlamatPasien { get; set; }
        public int SudahRegDokter { get; set; }
        public string TandaTanganDokter { get; set; }
        public string TandaTangan { get; set; }
        public string TTD_Saksi1 { get; set; }
        public string TTD_Saksi2 { get; set; }
    }
}