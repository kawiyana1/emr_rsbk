﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class GeneralHemodialisaViewModel
    { 
        public ListDetail<PeresepanHemodialisaRJViewModelDetail> PeresepanRJ_List { get; set; }
        public ListDetail<PerjalananTerapiViewModelDetail> PerjalananTerapi_List { get; set; }
        public ListDetail<CatatanPerkembanganPasienRJViewModelDetail> CttPerkembanganPasien_List { get; set; }
        public ListDetail<CatatanTranfusiDarahSelamaHDViewModelDetail> CttTranfusiHD_List { get; set; }
        public ListDetail<PemakaianDializerViewModelDetail> PemakaianDializer_List { get; set; }
        public ListDetail<ResumeTindakanHDViewModelDetail> ResumeTndkanHD_List { get; set; }
        public ListDetail<PengkajianResikoJatuhDewasaViewModelDetail> SkalaMorse_List { get; set; }
        public ListDetail<MonitorDanEvaluasiPelaksanaanProtokolResikoJatuhViewModelDetail> Monitor_List { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        public string Dokter { get; set; }
        public string DokterNama { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal_View { get; set; }

        

    }
}