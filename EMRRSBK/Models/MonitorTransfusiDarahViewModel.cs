﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class MonitorTransfusiDarahViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string JenisKomponenDarah { get; set; }
        public string NomorKantongDarah { get; set; }
        public string VolumeTransfusi { get; set; }
        public string GolonganDarah { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglKomponenDarah { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Waktu_Pengirim { get; set; }
        public string Nama_Pengirim { get; set; }
        public string TandaTangan_Pengirim { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Waktu_Penerima { get; set; }
        public string Nama_Penerima { get; set; }
        public string TandaTangan_Penerima { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Waktu_SebelumTransfusi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Waktu_SetelahTransfusi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Waktu_TransfusiBerakhir { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Waktu_Keluhan { get; set; }
        public string KeadaanUmum_SebelumTransfusi { get; set; }
        public string KeadaanUmum_SetelahTransfusi { get; set; }
        public string KeadaanUmum_TransfusiBerakhir { get; set; }
        public string KeadaanUmum_Keluhan { get; set; }
        public string Suhu_SebelumTransfusi { get; set; }
        public string Suhu_SetelahTransfusi { get; set; }
        public string Suhu_TransfusiBerakhir { get; set; }
        public string Suhu_Keluhan { get; set; }
        public string Nadi_SebelumTransfusi { get; set; }
        public string Nadi_SetelahTransfusi { get; set; }
        public string Nadi_TransfusiBerakhir { get; set; }
        public string Nadi_Keluhan { get; set; }
        public string TekananDarah_SebelumTransfusi { get; set; }
        public string TekananDarah_SetelahTransfusi { get; set; }
        public string TekananDarah_TransfusiBerakhir { get; set; }
        public string TekananDarah_Keluhan { get; set; }
        public string RespiratoryRate_SebelumTransfusi { get; set; }
        public string RespiratoryRate_SetelahTransfusi { get; set; }
        public string RespiratoryRate_TransfusiBerakhir { get; set; }
        public string RespiratoryRate_Keluhan { get; set; }
        public string ProduksiUrineWarna_SebelumTransfusi { get; set; }
        public string ProduksiUrineWarna_SetelahTransfusi { get; set; }
        public string ProduksiUrineWarna_TransfusiBerakhir { get; set; }
        public string ProduksiUrineWarna_Keluhan { get; set; }
        public string TandaReaksiTransfusi_SebelumTransfusi { get; set; }
        public string TandaReaksiTransfusi_SetelahTransfusi { get; set; }
        public string TandaReaksiTransfusi_TransfusiBerakhir { get; set; }
        public string TandaReaksiTransfusi_Keluhan { get; set; }
        public string DoubleCheckPerawatDokter_SebelumTransfusi { get; set; }
        public string DoubleCheckPerawatDokter_SebelumTransfusiNama { get; set; }
        public string DoubleCheckPerawatDokter_SetelahTransfusi { get; set; }
        public string DoubleCheckPerawatDokter_SetelahTransfusiNama { get; set; }
        public string DoubleCheckPerawatDokter_TransfusiBerakhir { get; set; }
        public string DoubleCheckPerawatDokter_TransfusiBerakhirNama { get; set; }
        public string DoubleCheckPerawatDokter_Keluhan { get; set; }
        public string DoubleCheckPerawatDokter_KeluhanNama { get; set; }
        public string DoubleCheckPerawatDokter_SebelumTransfusi1 { get; set; }
        public string DoubleCheckPerawatDokter_SebelumTransfusi1Nama { get; set; }
        public string DoubleCheckPerawatDokter_SetelahTransfusi1 { get; set; }
        public string DoubleCheckPerawatDokter_SetelahTransfusi1Nama { get; set; }
        public string DoubleCheckPerawatDokter_TransfusiBerakhir1 { get; set; }
        public string DoubleCheckPerawatDokter_TransfusiBerakhir1Nama { get; set; }
        public string DoubleCheckPerawatDokter_Keluhan1 { get; set; }
        public string DoubleCheckPerawatDokter_Keluhan1Nama { get; set; }
        public string TandaTandaReaksiTransfusi { get; set; }
        public string Username { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
    }
}