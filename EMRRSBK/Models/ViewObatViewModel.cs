﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class ViewObatViewModel
    {
        public string NoBukti { get; set; }
        public string NoResep { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string NamaDOkter { get; set; }
        public string DeskripsiObat { get; set; }
        public string NoReg { get; set; }

        public List<ViewObatDetailViewModel> Detail_List { get; set; }
    }

    public partial class ViewObatDetailViewModel
    {
        public string NoBukti { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string NamaDOkter { get; set; }
        public string NoResep { get; set; }
        public string DeskripsiObat { get; set; }
        public string NoReg { get; set; }
        public string Kode_Barang { get; set; }
        public string Nama_Barang { get; set; }
        public double JmlObat { get; set; }
        public string Dosis { get; set; }
    }
}