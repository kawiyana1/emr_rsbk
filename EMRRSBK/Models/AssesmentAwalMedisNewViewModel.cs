﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class AssesmentAwalMedisNewViewModel
    {
        public ListDetail<RencanaKerjaMedisDetailModel> RencanaKerja_List { get; set; }
         
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamKedatangan { get; set; }
        public string Rujukan { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> WaktuPengkajian { get; set; }
        public bool IRJ { get; set; }
        public bool IGD { get; set; }
        public bool LuarRS { get; set; }
        public string LuarRS_Ket { get; set; }
        public string RiwayatPenyakitDahulu { get; set; }
        public string Poliklinik { get; set; }
        public string RsPengirim { get; set; }
        public string KeluhanUtama { get; set; }
        public string RiwayatPenyakit { get; set; }
        public string RiwayatPengobatan { get; set; }
        public string RiwayatAlergi { get; set; }
        public string RiwayatKeluarga { get; set; }
        public string E { get; set; }
        public string V { get; set; }
        public string M { get; set; }
        public string TB { get; set; }
        public string BB { get; set; }
        public string TandaVital { get; set; }
        public string TekananDarah { get; set; }
        public string Nadi { get; set; }
        public string Pernafasan { get; set; }
        public string Suhu { get; set; }
        public string Saturasi { get; set; }
        public string PemeriksaanFisik { get; set; }
        public string Kepala { get; set; }
        public string Mata { get; set; }
        public string THT { get; set; }
        public string Leher { get; set; }
        public string Dada { get; set; }
        public string Abdomen { get; set; }
        public string Punggung { get; set; }
        public string Genetalia { get; set; }
        public string Ekstremitas { get; set; }
        public string HasilPenunjang { get; set; }
        public string DiagnosaMedis { get; set; }
        public string Intruksi { get; set; }
        public string KesimpulanHasil { get; set; }
        public string KondisiDipulangkan { get; set; }
        public string IntruksiLayanan { get; set; }
        public bool KIE { get; set; }
        public bool ObatPulang { get; set; }
        public bool FotoRontgen { get; set; }
        public bool Laboratorium { get; set; }
        public bool KontrolPoli { get; set; }
        public string DiKonsultasikan { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MeninggalPukul { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MinggatPukul { get; set; }
        public bool LaporMOD { get; set; }
        public bool LaporPiket { get; set; }
        public string NamaDokter { get; set; }
        public string NamaDokterNama { get; set; }
        public string TTDDokter { get; set; }
        public string SkalaNyeri { get; set; }
        public string PasienRisikoTinggi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> DipulangkanPukul { get; set; }

        public bool Airway_ObsTotal { get; set; }
        public bool Airway_ObsPartial { get; set; }
        public bool Airway_ResikoObs { get; set; }
        public bool Airway_AirwayBebas { get; set; }
        public bool Airway_Bebas { get; set; }
        public bool Breathing_HentiNafas { get; set; }
        public bool Breathing_DistresNafas { get; set; }
        public bool Breathing_PenggunaanObat { get; set; }
        public bool Breathing_SianosisAkut { get; set; }
        public bool Breathing_DistresNafasRingan { get; set; }
        public bool Breathing_PenggunaanOtot { get; set; }
        public bool Breathing_KulitMerahMuda { get; set; }
        public bool Breathing_RR { get; set; }
        public bool Breathing_Spo2 { get; set; }
        public bool Breathing_TidakAdaGangguan { get; set; }
        public bool Breathing_TidakAdaRespirasi { get; set; }
        public bool Circulation_HentiJantung { get; set; }
        public bool Circulation_GangguanHemodinamikSedang { get; set; }
        public bool Circulation_SampaiBerat { get; set; }
        public bool Circulation_Syok { get; set; }
        public bool Circulation_AkralDingin { get; set; }
        public bool Circulation_Pucat { get; set; }
        public bool Circulation_HR_60 { get; set; }
        public bool Circulation_CRT_3 { get; set; }
        public bool Circulation_DehidrasiBerat { get; set; }
        public bool Circulation_GangguanHemodinamik { get; set; }
        public bool Circulation_NadiKuat { get; set; }
        public bool Circulation_HR_110 { get; set; }
        public bool Circulation_AkralHangat { get; set; }
        public bool Circulation_Pucat_Dehidrasi { get; set; }
        public bool Circulation_DehidrasiSedang { get; set; }
        public bool Circulation_TidakAdaBekas { get; set; }
        public bool Disability_GCS_9 { get; set; }
        public bool Disability_Kejang { get; set; }
        public bool Disability_Parestesi { get; set; }
        public bool Disability_NyeriBetul { get; set; }
        public bool Disability_GCS_9_12 { get; set; }
        public bool Disability_Letargi { get; set; }
        public bool Disability_LainBilaTerganggu { get; set; }
        public bool Disability_NyeriSedang { get; set; }
        public bool Disability_GCS_15 { get; set; }
        public bool Disability_PenurunanAktivitas { get; set; }
        public bool Disability_KontakMata { get; set; }
        public bool Disability_HilangnyaRespon { get; set; }
        public string Omet { get; set; }
        public string Scale { get; set; }
        public string Proveking { get; set; }
        public string Timing { get; set; }
        public string Regso { get; set; }
        public string Quality { get; set; }
        public string Gambar { get; set; }
        public string Keluarga { get; set; }
        public string DokterInterenship { get; set; }

        public int Report { get; set; }
        public int Nomor { get; set; }
        public string Template { get; set; }
        public bool save_template { get; set; }
        public string nama_template { get; set; }
        public string DokumenID { get; set; }
        public string KategoriKasus { get; set; }
        public bool DischargePlanning { get; set; }
    }
}