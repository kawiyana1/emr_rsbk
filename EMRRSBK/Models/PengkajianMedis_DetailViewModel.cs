﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class PengkajianMedis_DetailViewModel
    {
        public string NoReg { get; set; }
        public string NRM { get; set; }
        public string SectionID { get; set; }
        
        [DataType(DataType.Date)]
        public System.DateTime Tanggal { get; set; }
        public int No { get; set; }
        public string DaftarMasalah { get; set; }
        public string RencanaIntervensi { get; set; }
        public string Target { get; set; }
        public string Username { get; set; }
    }
}