﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class DaftarDokterPenanggungJawabPelayananViewModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }
        public string Username { get; set; } 
        public string Diagnosa { get; set; }
        public string DPJP { get; set; }
        public string DPJPNama { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> DPJP_TglAwal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> DPJP_TglAkhir { get; set; }
        public string DPJP_Utama { get; set; }
        public string DPJP_UtamaNama { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> DPJP_Utama_TglAwal { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> DPJP_Utama_TglAkhir { get; set; }
        public string Keterangan { get; set; }
    }
}