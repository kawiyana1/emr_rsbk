﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class AssesmentAwalPasienHemodialisisViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string DataUmumPasien_TempatLahir { get; set; }
        public string DataUmumPasien_AlamatSesuaiKTP { get; set; }
        public string DataUmumPasien_AlamatTempatTinggalSekarang { get; set; }
        public bool DataSosialEkonomi_Personal { get; set; }
        public bool DataSosialEkonomi_BPJS { get; set; }
        public bool DataSosialEkonomi_JKBM { get; set; }
        public bool DataSosialEkonomi_Lainnya_1 { get; set; }
        public bool DataSosialEkonomi_OrangTua { get; set; }
        public bool DataSosialEkonomi_SuamiIstri { get; set; }
        public bool DataSosialEkonomi_Sendiri { get; set; }
        public bool DataSosialEkonomi_Lainnya_2 { get; set; }
        public string DataSosialEkonomi_StatusPerkawinan { get; set; }
        public string DataKesehatan_KeluhanSaatIni { get; set; }
        public bool DataKesehatan_DM { get; set; }
        public bool DataKesehatan_Hepatitis { get; set; }
        public bool DataKesehatan_Ginjal { get; set; }
        public bool DataKesehatan_Jantung { get; set; }
        public bool DataKesehatan_Hipertensi { get; set; }
        public bool DataKesehatan_TBC { get; set; }
        public bool DataKesehatan_Kanker { get; set; }
        public bool DataKesehatan_Stroke { get; set; }
        public bool DataKesehatan_Lainnya { get; set; }
        public string DataKesehatan_RiwayatOpname { get; set; }
        public string DataKesehatan_RiwayatOpnameYaDi { get; set; }
        public string DataKesehatan_RiwayatOperus { get; set; }
        public string DataKesehatan_RiwayatOperusYaSebutkan { get; set; }
        public string Username { get; set; }

        

        public string NamaPasien { get; set; }
        public int? Umur { get; set; }
        public int Report { get; set; }

        public string Pekerjaan { get; set; }
        public string Agama { get; set; }

        public string PenanggungNama { get; set; }

        public string Phone { get; set; }

    }
}