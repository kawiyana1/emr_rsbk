﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class AssesmentIPoliumumViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglKunjungan { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamKunjungan { get; set; }
        public string Rujukan { get; set; }
        public string DokterRujukan { get; set; }
        public string RumahSakitRujukan { get; set; }
        public string LainRujukan { get; set; }
        public string DiagRujukan { get; set; }
        public string TV_TekDarah { get; set; }
        public string TV_FreNapas { get; set; }
        public string TV_Suhu { get; set; }
        public string TV_FreNadi { get; set; }
        public string FSL_AlatBantu { get; set; }
        public string FSL_CacatTbh { get; set; }
        public string FSL_ADL { get; set; }
        public bool PES_StatusEmosional_Normal { get; set; }
        public bool PES_StatusEmosional_TidakSemangat { get; set; }
        public bool PES_StatusEmosional_Tertekan { get; set; }
        public bool PES_StatusEmosional_Depresi { get; set; }
        public bool PES_StatusEmosional_Cemas { get; set; }
        public bool PES_StatusEmosional_SulitTidur { get; set; }
        public string PES_StaKawin { get; set; }
        public string PES_JumIstri { get; set; }
        public string PES_JumSuami { get; set; }
        public string PES_Pekerjaan { get; set; }
        public string PES_CaraBayar { get; set; }
        public string PES_Agama { get; set; }
        public string PES_UsiaKawin { get; set; }
        public string PES_KetTingglDengan { get; set; }
        public string PES_KetPekerjaan { get; set; }
        public string PES_KetCaraBayar { get; set; }
        public string PES_KetAgama { get; set; }
        public bool PES_StaNormal { get; set; }
        public bool PES_StaTidakSemangat { get; set; }
        public bool PES_StaTertekan { get; set; }
        public bool PES_StaDepresi { get; set; }
        public bool PES_StaCemas { get; set; }
        public bool PES_StaSulitTidur { get; set; }
        public bool PES_TDOrangTua { get; set; }
        public bool PES_TDSuami { get; set; }
        public bool PES_TDAnak { get; set; }
        public bool PES_TDSendiri { get; set; }
        public bool PES_TDLain { get; set; }
        public string PES_BudayaPola { get; set; }
        public string PES_KetBudayaPola { get; set; }
        public string PNyeri_Nyeri { get; set; }
        public string PNyeri_NyeriKet { get; set; }
        public string PNyeri_PenyebabNyeri { get; set; }
        public bool PNyeri_KQMenekan { get; set; }
        public bool PNyeri_KQMenusuk { get; set; }
        public bool PNyeri_KQBerdenyut { get; set; }
        public bool PNyeri_KQMenyebar { get; set; }
        public bool PNyeri_KQMenyengat { get; set; }
        public bool PNyeri_KQPedih { get; set; }
        public string PNyeri_Lokasi { get; set; }
        public bool PNyeri_IntenWBFP { get; set; }
        public bool PNyeri_IntenNRS { get; set; }
        public bool PNyeri_IntenFLACC { get; set; }
        public string PNyeri_Score { get; set; }
        public string PNyeri_KategoriNyeri { get; set; }
        public string PNyeri_MulaiKpn { get; set; }
        public string PNyeri_BrpLama { get; set; }
        public string PNyeri_FrekDurasi { get; set; }
        public string PNyeri_HilangJika { get; set; }
        public string PN_BB { get; set; }
        public string PN_TB { get; set; }
        public string PN_LILA { get; set; }
        public string PN_IMT { get; set; }
        public string NamaJudul { get; set; }
        public string NamaPasien { get; set; }
        public string TglLahir { get; set; }
        public string PN_BrtBadan { get; set; }
        public string PN_BrtBadanYa { get; set; }
        public string PN_AsupMakan { get; set; }
        public string PN_TotalSkor { get; set; }
        public string PN_TotalSkorKet { get; set; }
        public string PRJ_Usia { get; set; }
        public string PRJ_Anak { get; set; }
        public string PRJ_Dewasa { get; set; }
        public string PRJ_Lansia { get; set; }
        public string PRJ_ScrAnak { get; set; }
        public string PRJ_ScrDewasa { get; set; }
        public string PRJ_ScrLansia { get; set; }
        public string RK_RiwayatAlergi { get; set; }
        public bool RK_RPD_TidakAda { get; set; }
        public bool RK_RPD_Hipertensi { get; set; }
        public bool RK_RPD_TbParu { get; set; }
        public bool RK_RPD_Lain { get; set; }
        public string RK_RPD_LainKet { get; set; }
        public bool RK_RPK_TidakAda { get; set; }
        public bool RK_RPK_Hipertensi { get; set; }
        public bool RK_RPK_TbParu { get; set; }
        public bool RK_RPK_Lain { get; set; }
        public string RK_RPK_LainKet { get; set; }
        public string RK_JenisAlergi { get; set; }
        public string RK_Reaksi { get; set; }
        public string RK_KetRPD { get; set; }
        public string RK_KetRPK { get; set; }
        public string RK_KetRKS { get; set; }
        public string KetPemeriksaanFisik { get; set; }
        public string KetDiagnosa { get; set; }
        public string KetRencanaTerapi { get; set; }
        public string NamaDokter { get; set; }
        public string NamaDokterNama { get; set; }
        public string Username { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        public int SudahRegNamaDokter { get; set; }
        public string TandaTanganNamaDokter { get; set; }
        public string Nama_tamplet { get; set; }

        public string DokumenID { get; set; }
        public string Template { get; set; }
        public bool save_template { get; set; }
        public string nama_template { get; set; }
    }
}