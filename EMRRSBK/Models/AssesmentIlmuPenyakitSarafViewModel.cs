﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class AssesmentIlmuPenyakitSarafViewModel
    {

        public ListDetail<RiwayatPenggobatanIlmuSarafViewModelDetail> ObatSrf_List { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public bool Rujukan_Ya { get; set; }
        public string Rujukan_Ya_Ket { get; set; }
        public string DxRujukan { get; set; }
        public bool Rujukan_Tidak { get; set; }
        public string NamaKeluarga { get; set; }
        public string AlamatKeluarga { get; set; }
        public string Telepon { get; set; }
        public string Transportasi { get; set; }
        public string Transportasi_Ket { get; set; }
        public string RiwayatAlergi { get; set; }
        public string RiwayatAlergi_Ket { get; set; }
        public string Nyeri { get; set; }
        public string LokasiNyeri { get; set; }
        public string Intesitas { get; set; }
        public string Jenis { get; set; }
        public string KeluhanUtama { get; set; }
        public string RiwayatPenyakitSekarang { get; set; }
        public bool Riwayat_Hipertensi { get; set; }
        public bool Riwayat_DM { get; set; }
        public bool Riwayat_Jantung { get; set; }
        public bool Riwayat_Asthma { get; set; }
        public bool Riwayat_Stroke { get; set; }
        public bool Riwayat_Liver { get; set; }
        public bool Riwayat_Ginjal { get; set; }
        public bool Riwayat_TBCParu { get; set; }
        public bool Riwayat_Lainlain { get; set; }
        public string Riwayat_Lainlain_Ket { get; set; }
        public bool Keluarga_Hipertensi { get; set; }
        public bool Keluarga_DM { get; set; }
        public bool Keluarga_Jantung { get; set; }
        public bool Keluarga_Asthma { get; set; }
        public bool Keluarga_Lainlain { get; set; }
        public string Keluarga_Lainlain_Ket { get; set; }
        public bool Sosial_Merokok { get; set; }
        public bool Sosial_Alkohol { get; set; }
        public bool Sosial_Lainlain { get; set; }
        public string Sosial_Lainlain_Ket { get; set; }
        public string KeadaanUmum { get; set; }
        public string Gizi { get; set; }
        public string GCS_E { get; set; }
        public string GCS_M { get; set; }
        public string GCS_V { get; set; }
        public string TindakanResusitasi { get; set; }
        public string BB { get; set; }
        public string TB { get; set; }
        public string Tensi { get; set; }
        public string SuhuAxilla { get; set; }
        public string SuhuRectal { get; set; }
        public string Nadi { get; set; }
        public string Respirasi { get; set; }
        public string Saturasi { get; set; }
        public bool Saturasi_SuhuRuangan { get; set; }
        public bool Saturasi_NasalCanule { get; set; }
        public bool Saturasi_NRB { get; set; }
        public bool Mata_Anemis { get; set; }
        public string Mata_Anemis_Ket { get; set; }
        public bool Mata_Icterus { get; set; }
        public string Mata_Icterus_Ket { get; set; }
        public bool Mata_ReflexPupil { get; set; }
        public string Mata_ReflexPupil_Ket { get; set; }
        public string Mata_Ukuran { get; set; }
        public bool Mata_Isocore_unisocore { get; set; }
        public bool Mata_Lainlain { get; set; }
        public string Mata_Lainlain_Ket { get; set; }
        public bool THT_Tonsil { get; set; }
        public string THT_Tonsil_Ket { get; set; }
        public bool THT_Pharing { get; set; }
        public string THT_Pharing_Ket { get; set; }
        public bool THT_Lidah { get; set; }
        public string THT_Lidah_Ket { get; set; }
        public bool THT_Bibir { get; set; }
        public string THT_Bibir_Ket { get; set; }
        public bool Leher_JVP { get; set; }
        public string Leher_JVP_Ket { get; set; }
        public bool Leher_PembesaranKelenjar { get; set; }
        public string Leher_PembesaranKelenjar_Ket { get; set; }
        public bool Thoraks_Simetris_Asimetris { get; set; }
        public string Thoraks_Ket { get; set; }
        public bool Cor_S1 { get; set; }
        public string Cor_S1_Ket { get; set; }
        public string Cor_Reguler_Ireguler { get; set; }
        public bool Cor_Murmur { get; set; }
        public string Cor_Murmur_Ket { get; set; }
        public bool Cor_Besar { get; set; }
        public string Cor_Besar_Ket { get; set; }
        public bool Pulmo_SuaraNapas { get; set; }
        public string Pulmo_SuaraNapas_Ket { get; set; }
        public bool Pulmo_Ronchi { get; set; }
        public string Pulmo_Ronchi_Ket { get; set; }
        public bool Pulmo_Wheezing { get; set; }
        public string Pulmo_Wheezing_Ket { get; set; }
        public bool Abdomen_Hepar { get; set; }
        public string Abdomen_Hepar_Ket { get; set; }
        public bool Abdomen_Lien { get; set; }
        public string Abdomen_Lien_Ket { get; set; }
        public bool Ekstremitas_Hangat_Dingin { get; set; }
        public string Ekstremitas_Hangat_Dingin_Ket { get; set; }
        public bool Extremitas_Odema { get; set; }
        public string Extremitas_Odema_Ket { get; set; }
        public string Kranium { get; set; }
        public string KorpusVertebra { get; set; }
        public bool KakuKuduk { get; set; }
        public string KakuKuduk_Ket { get; set; }
        public bool KernigSign { get; set; }
        public string KernigSign_Ket { get; set; }
        public bool BrudzinskiNeck { get; set; }
        public string BrudzinskiNeck_Ket { get; set; }
        public bool BrudzinskiLeg { get; set; }
        public string BrudzinskiLeg_Ket { get; set; }
        public bool Persangsangan_Lainlain { get; set; }
        public string Persangsangan_Lainlain_Ket { get; set; }
        public string SarafOtak { get; set; }
        public string SarafOtak_Kanan_Kiri { get; set; }
        public string Motorik { get; set; }
        public string Motorik_Kanan_Kiri { get; set; }
        public string Refleks { get; set; }
        public string Refleks_Kanan_Kiri { get; set; }
        public string Sensorik { get; set; }
        public string Sensorik_Kanan_Kiri { get; set; }
        public string Vegetatif { get; set; }
        public string FungsiLuhur { get; set; }
        public string TandaKemunduranMental { get; set; }
        public string NyeriTekanSaraf { get; set; }
        public string TandaLaseque { get; set; }
        public string Lainlain { get; set; }
        public bool Neurologik_Extremitas_Hangat_Dingin { get; set; }
        public bool Neurologik_Extremitas_Odema { get; set; }
        public string Neurologik_Extremitas_Odema_Ket { get; set; }
        public bool Neurologik_Extremitas_Lainlain { get; set; }
        public string Neurologik_Extremitas_Lainlain_Ket { get; set; }
        public string Kulit { get; set; }
        public string GenitaliaEksterna { get; set; }
        public bool StatusPubertas_Perempuan { get; set; }
        public string StatusPubertas_Perempuan_M { get; set; }
        public string StatusPubertas_Perempuan_P { get; set; }
        public bool StatusPubertas_Laki { get; set; }
        public string StatusPubertas_Laki_G { get; set; }
        public string StatusPubertas_Laki_P { get; set; }
        public string StatusAntropometri { get; set; }
        public string Neurologik_BB { get; set; }
        public string Neurologik_TB { get; set; }
        public string Neurologik_BB_TB { get; set; }
        public string Neurologik_BBI { get; set; }
        public string HasilPemeriksaan_Lab { get; set; }
        public string HasilPemeriksaan_EKG { get; set; }
        public string HasilPemeriksaan_XRay { get; set; }
        public string DiagnosaKerja { get; set; }
        public string Terapi { get; set; }
        public string RencanaKerja { get; set; }
        public bool BolehPulang { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> BolehPulang_Jam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> BolehPulang_Tanggal { get; set; }
        public string KontrolPoli { get; set; }
        public string KontrolPoli_Ket { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> KontrolPoli_Tanggal { get; set; }
        public bool Dirawat { get; set; }
        public bool Dirawat_Intensif { get; set; }
        public bool Dirawat_RuangLain { get; set; }
        public string Dirawat_RuangLain_Ket { get; set; }
        public string DPJP { get; set; }
        public string DPJPNama { get; set; }
        public string Username { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        public string Tonus { get; set; }
        public string Tonus_Kanan_Kiri { get; set; }
        public string Kordinasi { get; set; }
        public string Kordinasi_Kanan_Kiri { get; set; }
        public string Involunter { get; set; }
        public string Involunter_Kanan_Kiri { get; set; }
        public string GayaJalan { get; set; }
        public string Fisiologi { get; set; }
        public string Fisiologi_Kanan_Kiri { get; set; }
        public string Patologis { get; set; }
        public int SudahRegDokter { get; set; }
        public string TandaTanganDPJP { get; set; }
        public string Template { get; set; }
        public bool save_template { get; set; }
        public string nama_template { get; set; }
        public string DokumenID { get; set; }
    }
}