﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class SuratPernyataanJanganDilakukanResusitasiViewModel 
    {
        public string NoReg { get; set; }

        public string SectionID { get; set; }

        public string NRM { get; set; }

        [DataType(DataType.Date)]

        public Nullable<System.DateTime> Tanggal { get; set; }

        public string NamaBertandaTangan { get; set; }

        [DataType(DataType.Date)]

        public Nullable<System.DateTime> TglLahir { get; set; }

        public string JenisKelamin { get; set; }

        public string TerhadapDiriSaya { get; set; }
        public string TerhadapDiriSayaLainnya { get; set; }

        public string NamaMembuatKeputusan { get; set; }

        [DataType(DataType.Date)]

        public Nullable<System.DateTime> TglLahirMembuatKeputusan { get; set; }

        public string JKMembuatkeputusan { get; set; }

        [DataType(DataType.Date)]

        public Nullable<System.DateTime> TanggalPeninjauanulang1 { get; set; }

        public bool DnrBerlaku1 { get; set; }

        public bool DnrDibatalkan1 { get; set; }

        [DataType(DataType.Date)]

        public Nullable<System.DateTime> TanggalPeninjauanulang2 { get; set; }

        public bool DnrBerlaku2 { get; set; }

        public bool DnrDIbatalkan2 { get; set; }

        public string YangMenyatakan { get; set; }

        public string Saksi1 { get; set; }

        public string Saksi2 { get; set; }
    }
}