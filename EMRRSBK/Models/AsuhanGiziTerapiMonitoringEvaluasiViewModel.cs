﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class AsuhanGiziTerapiMonitoringEvaluasiViewModel
    {
        public ListDetail<AsuhanGiziTerapiMonitoringEvaluasiDetailModelDetail> Detail_List { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public string Username { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string DietAwal { get; set; }
        public bool Oral { get; set; }
        public bool NGT { get; set; }
        public bool Puasa { get; set; }
        public string ZatGizi { get; set; }
        public string Energi { get; set; }
        public string Protein { get; set; }
        public string BB { get; set; }
        public string Lila { get; set; }
        public string Biokimia { get; set; }
        public bool Mual { get; set; }
        public bool Muntah { get; set; }
        public bool Diare { get; set; }
        public bool Konstipasi { get; set; }
        public bool Mengunyah { get; set; }
        public bool Menelan { get; set; }
        public bool Hipertensi { get; set; }
        public bool Odema { get; set; }
        public bool Lain { get; set; }
        public string Lain_Ket { get; set; }
        public string Dietary { get; set; }
        public string Evaluasi { get; set; }
        public string AhliGizi { get; set; }
        public string AhliGiziNama { get; set; }
        public int Report { get; set; }
    }
}