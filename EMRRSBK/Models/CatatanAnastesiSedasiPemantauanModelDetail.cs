﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class CatatanAnastesiSedasiPemantauanModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }
        public string Username { get; set; }
        public string SPO { get; set; }
        public string FIO { get; set; }
        public string CairanInfus { get; set; }
        public string Darah { get; set; }
        public string Urine { get; set; }
        public string Pendarahan { get; set; }
    }
}