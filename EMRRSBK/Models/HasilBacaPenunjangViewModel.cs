﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using EMRRSBK.Models;

namespace EMRRSBK.Models
{
    public class HasilBacaPenunjangViewModel
    {
        public ListDetail<HasilBacaPenunjangDetailViewModel> FileUpload { get; set; }
        public string NoBukti { get; set; }
        public string HasilBaca { get; set; }
        public string Id_mJenisPenunjang { get; set; }
        [DataType(DataType.Date)]
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        [DataType(DataType.Time)]
        public System.DateTime Jam { get; set; }
        public string Jam_View { get; set; }
        public string NoReg { get; set; }
        public string NRM { get; set; }
        public string SectionID { get; set; }
        public string NamaPasien { get; set; }
        public string Alamat { get; set; }
        public string NoIdentitas { get; set; }
        public Nullable<System.DateTime> TglLahir { get; set; }
        public Nullable<int> UmurThn { get; set; }
        public Nullable<int> UmurBln { get; set; }
        public string Pekerjaan { get; set; }
        public string JenisKelamin { get; set; }
        public IEnumerable<HttpPostedFileBase> files { get; set; }
        public string Id_mJenisPenunjang_Text { get; set; }

        public string Pemeriksaan { get; set; }
        public string PemeriksaanNama { get; set; }
        public string SampleID { get; set; }
        public string UserIDWeb { get; set; }
        public bool TemplateBaru { get; set; }
        public string TemplateBaru_Nama { get; set; }
        public string Template_Name { get; set; }
        public string Template { get; set; }
        public string NoBill { get; set; }
        public string NoBuktiHasil { get; set; }
        public string NoBuktiPermintaan { get; set; }
        public string NoOrder { get; set; }
        public string DiagnosaIndikasi { get; set; }
        public string PemeriksaanTambahan { get; set; }
        public string Hasil { get; set; }
        public string Tipe { get; set; }
        public string HasilBaca_2 { get; set; }
        public string HasilBaca_3 { get; set; }
        public string HasilBaca_4 { get; set; }
        public string HasilBaca_5 { get; set; }
        public bool Hasil_1 { get; set; }
        public bool Hasil_2 { get; set; }
        public bool Hasil_3 { get; set; }
        public bool Hasil_4 { get; set; }
        public bool Hasil_5 { get; set; }
        public bool TemplateBaru_2 { get; set; }
        public string TemplateBaru_Nama_2 { get; set; }
        public string Template_Name_2 { get; set; }
        public string Template_2 { get; set; }
        public bool TemplateBaru_3 { get; set; }
        public string TemplateBaru_Nama_3 { get; set; }
        public string Template_Name_3 { get; set; }
        public string Template_3 { get; set; }
        public bool TemplateBaru_4 { get; set; }
        public string TemplateBaru_Nama_4 { get; set; }
        public string Template_Name_4 { get; set; }
        public string Template_4 { get; set; }
        public bool TemplateBaru_5 { get; set; }
        public string TemplateBaru_Nama_5 { get; set; }
        public string Template_Name_5 { get; set; }
        public string Template_5 { get; set; }

    }
}