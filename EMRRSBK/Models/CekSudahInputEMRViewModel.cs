﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class CekSudahInputEMRViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public string Form { get; set; }
    }
}