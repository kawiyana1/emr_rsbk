﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class RencanaPemulanganPasienViewModel
    {
        public ListDetail<RencanaPemulanganPasienModelDetail> RcnaPmlngn_List { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string DiagnosaMedis { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalMRS { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamMRS { get; set; }
        public string AlasanMRS { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalPerencanaan { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamPerencanaan { get; set; }
        
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> EstimasiTanggalPulang { get; set; }
        public string KodePerawat { get; set; }
        public string KodePerawatNama { get; set; }
        public string PasienDanKeluarga { get; set; }
        public string Pekerjaan { get; set; }
        public string Keuangan { get; set; }
        public string AntisipasiMasalahPulang { get; set; }
        public string AntisipasiMasalahPulang_Ket { get; set; }
        public bool Bantuan_MinumObat { get; set; }
        public bool Bantuan_Mandi { get; set; }
        public bool Bantuan_Makan { get; set; }
        public bool Bantuan_Diet { get; set; }
        public bool Bantuan_MenyiapkanMakanan { get; set; }
        public bool Bantuan_Berpakaian { get; set; }
        public bool Bantuan_EdukasiKesehatan { get; set; }
        public bool Bantuan_Transportasi { get; set; }
        public string AdakahYangBantu { get; set; }
        public string AdakahYangBantu_Ket { get; set; }
        public string ApakahPasienHidup { get; set; }
        public string ApakahPasienHidup_Ket { get; set; }
        public string PasienMenggunakanAlatMedis { get; set; }
        public string PasienMenggunakanAlatMedis_Ket { get; set; }
        public string PasienPerluAlat { get; set; }
        public string PasienPerluAlat_Ket { get; set; }
        public string ApakahPerluBantuan { get; set; }
        public string ApakahPerluBantuan_Ket { get; set; }
        public string PasienBermasalah { get; set; }
        public string PasienBermasalah_Ket { get; set; }
        public string PasienMemilikiNyeri { get; set; }
        public string PasienMemilikiNyeri_Ket { get; set; }
        public string PasienPerluEdukasi { get; set; }
        public string PasienPerluEdukasi_Ket { get; set; }
        public string PasienPerluKeterampilan { get; set; }
        public string PasienPerluKeterampilan_Ket { get; set; }
        public string Username { get; set; }
        public int Report { get; set; }
    }
}