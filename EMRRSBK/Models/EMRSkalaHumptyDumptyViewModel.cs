﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class EMRSkalaHumptyDumptyViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; } 
        public int No { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Usia { get; set; }
        public string Usia_Skor { get; set; }
        public string JenisKelamin { get; set; }
        public string JenisKelamin_Skor { get; set; }
        public string Diagnosa { get; set; }
        public string Diagnosa_Skor { get; set; }
        public string GangguanKognitif { get; set; }
        public string GangguanKognitif_Skor { get; set; }
        public string FaktorLingkungan { get; set; }
        public string FaktorLingkungan_Skor { get; set; }
        public string Respon { get; set; }
        public string Respon_Skor { get; set; }
        public string PenggunaanObat { get; set; }
        public string PenggunaanObat_Skor { get; set; }
        public string TotalSkor { get; set; }
        public string Keterangan { get; set; }
        public string TotalSkorKeterangan { get; set; }
        public string Paraf { get; set; }
        public string ParafNama { get; set; }
        public string Username { get; set; }

    }
}