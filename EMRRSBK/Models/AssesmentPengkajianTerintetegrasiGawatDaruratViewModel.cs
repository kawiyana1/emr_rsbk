﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class AssesmentPengkajianTerintetegrasiGawatDaruratViewModel
    {
        //public ListDetail<TindakanKeperawatanViewModelDetail> TKepewaratan_List { get; set; }
        //public ListDetail<MelaksanakanAlergiTestViewModelDetail> Alergi_List { get; set; }
        //public ListDetail<JenisObatViewModelDetail> Obat_List { get; set; }
        //public ListDetail<DiagnosaViewModelDetail> Diagnosa_List { get; set; }
        //public ListDetail<ObservasiKomprehensifViewModelDetail> ObsesKom_List { get; set; }
        //public ListDetail<ObservasiCairanViewModelDetail> ObsesCair_List { get; set; } 
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> DatangRS_Tgl { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> PukulTriage { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> WaktuPengkajian { get; set; }
        public string Kendaraan { get; set; }
        public string Kendaraan_Ket { get; set; }
        public string KeluhanUtama { get; set; }
        public string RIwayatPenyakitSekarang { get; set; }
        public string RiwayatPenyakitSebelumnya { get; set; }
        public string RiwayatPenyakitKeluarga { get; set; }
        public string RiwayatPenyakitKeturunan { get; set; }
        public string RiwayatPenyakitKeturunan_Ket { get; set; }
        public string RiwayatPenyakitMenular { get; set; }
        public string RiwayatPenyakitMenular_Ket { get; set; }
        public string RiwayatAlergi { get; set; }
        public string RiwayatAlergi_Ket { get; set; }
        public string RiwayatPenggunaanObat { get; set; }
        public string RiwayatPenggunaanObat_Ket { get; set; }
        public bool Airway_ObsTotal { get; set; }
        public bool Airway_ObsPartial { get; set; }
        public bool Airway_ResikoObs { get; set; }
        public bool Airway_AirwayBebas { get; set; }
        public bool Airway_Bebas { get; set; }
        public bool Breathing_HentiNafas { get; set; }
        public bool Breathing_DistresNafas { get; set; }
        public bool Breathing_PenggunaanObat { get; set; }
        public bool Breathing_SianosisAkut { get; set; }
        public bool Breathing_DistresNafasRingan { get; set; }
        public bool Breathing_PenggunaanOtot { get; set; }
        public bool Breathing_KulitMerahMuda { get; set; }
        public bool Breathing_RR { get; set; }
        public bool Breathing_Spo2 { get; set; }
        public bool Breathing_TidakAdaGangguan { get; set; }
        public bool Breathing_TidakAdaRespirasi { get; set; }
        public bool Circulation_HentiJantung { get; set; }
        public bool Circulation_GangguanHemodinamikSedang { get; set; }
        public bool Circulation_SampaiBerat { get; set; }
        public bool Circulation_Syok { get; set; }
        public bool Circulation_AkralDingin { get; set; }
        public bool Circulation_Pucat { get; set; }
        public bool Circulation_HR_60 { get; set; }
        public bool Circulation_CRT_3 { get; set; }
        public bool Circulation_DehidrasiBerat { get; set; }
        public bool Circulation_GangguanHemodinamik { get; set; }
        public bool Circulation_NadiKuat { get; set; }
        public bool Circulation_HR_110 { get; set; }
        public bool Circulation_AkralHangat { get; set; }
        public bool Circulation_Pucat_Dehidrasi { get; set; }
        public bool Circulation_DehidrasiSedang { get; set; }
        public bool Circulation_TidakAdaBekas { get; set; }
        public bool Disability_GCS_9 { get; set; }
        public bool Disability_Kejang { get; set; }
        public bool Disability_Parestesi { get; set; }
        public bool Disability_NyeriBetul { get; set; }
        public bool Disability_GCS_9_12 { get; set; }
        public bool Disability_Letargi { get; set; }
        public bool Disability_LainBilaTerganggu { get; set; }
        public bool Disability_NyeriSedang { get; set; }
        public bool Disability_GCS_15 { get; set; }
        public bool Disability_PenurunanAktivitas { get; set; }
        public bool Disability_KontakMata { get; set; }
        public bool Disability_HilangnyaRespon { get; set; }
        public string Gambar1 { get; set; }
        public string Omet { get; set; }
        public string Scale { get; set; }
        public string Proveking { get; set; }
        public string Timing { get; set; }
        public string Regso { get; set; }
        public string Quality { get; set; }
        public bool Airway_Clear { get; set; }
        public bool Airway_TidakPaten { get; set; }
        public bool Airway_Pangkal { get; set; }
        public bool Airway_LidahJatuh { get; set; }
        public bool Airway_Spasme { get; set; }
        public bool Airway_Darah { get; set; }
        public bool Airway_BendaAsing { get; set; }
        public bool Airway_Sputum { get; set; }
        public bool Airway_SuaraNafas { get; set; }
        public bool Airway_Normal { get; set; }
        public bool Airway_Stidor { get; set; }
        public bool Airway_Gurgling { get; set; }
        public bool Airway_Scicring { get; set; }
        public bool Breathing_Clear { get; set; }
        public bool Breathing_Apneu { get; set; }
        public bool Breathing_Dyspneu { get; set; }
        public bool Breathing_Bradipneu { get; set; }
        public bool Breathing_Takhipneu { get; set; }
        public bool Breathing_Orthopneu { get; set; }
        public bool Breathing_Vesikuler { get; set; }
        public bool Breathing_Wheezing { get; set; }
        public bool Breathing_Ronchi { get; set; }
        public string Breathing_IramaNafas { get; set; }
        public string Breathing_PenggunaanOtotBantu { get; set; }
        public string Breathing_RR_Ket { get; set; }
        public string Breathing_JenisPernapasan { get; set; }
        public bool Circulation_Clear { get; set; }
        public string Circulation_Akral { get; set; }
        public string Circulation_PengisianKapiler { get; set; }
        public string Circulation_Nadi { get; set; }
        public string Circulation_Frekuensi { get; set; }
        public string Circulation_Isi { get; set; }
        public string Circulation_TD { get; set; }
        public bool Circulation_Muntah { get; set; }
        public bool Circulation_Diare { get; set; }
        public bool Circulation_LukaBakar { get; set; }
        public bool Circulation_Pendarahan { get; set; }
        public string Circulation_LuasLukaBakar { get; set; }
        public string Circulation_Grade { get; set; }
        public string Circulation_Turgor { get; set; }
        public string Circulation_Nutrisi { get; set; }
        public string Circulation_Nutrisi_Ket { get; set; }
        public string Circulation_Edema { get; set; }
        public bool Disability_Clear { get; set; }
        public string Disability_GCS_E { get; set; }
        public string Disability_GCS_V { get; set; }
        public string Disability_GCS_M { get; set; }
        public bool Disability_Pupil { get; set; }
        public string Disability_Pupil_Ket { get; set; }
        public bool Disability_Luteralisasi { get; set; }
        public string Disability_Luteralisasi_Ket { get; set; }
        public bool Disability_KekuatanOtot { get; set; }
        public string Disability_KekuatanOtot_Ket { get; set; }
        public bool Disability_Reflek { get; set; }
        public string Disability_Reflek_Ket { get; set; }
        public bool Disability_StatusPsikologis { get; set; }
        public bool Disability_TenangCemas { get; set; }
        public bool Disability_TakutMarah { get; set; }
        public bool Disability_SedihAgitasi { get; set; }
        public bool Disability_KecendrunganBunuhDiri { get; set; }
        public bool Exposure_Clear { get; set; }
        public bool Exposure_RiwayatTrauma { get; set; }
        public string Exposure_RiwayatTrauma_Ket { get; set; }
        public bool Exposure_JejasLuka { get; set; }
        public string Exposure_JejasLuka_Ket { get; set; }
        public bool Exposure_Lokasi { get; set; }
        public string Exposure_Lokasi_Ket { get; set; }
        public bool Exposure_LuasLuka { get; set; }
        public string Exposure_Panjang { get; set; }
        public string Exposure_Dalam { get; set; }
        public string Exposure_Lebar { get; set; }
        public string Exposure_SPO2 { get; set; }
        public string Exposure_Suhu { get; set; }
        public string SkriningPasien_a { get; set; }
        public string SkriningPasien_b { get; set; }
        public string SkriningPasien_TotalSkor { get; set; }
        public string StatusFungsional_PerawatanDiri { get; set; }
        public string StatusFungsional_MelakukanROM { get; set; }
        public string StatusFungsional_Mobilisasi { get; set; }
        public string StatusFungsional_Toileting { get; set; }
        public string StatusFungsional_Mandi { get; set; }
        public string StatusFungsional_Berpindah { get; set; }
        public string StatusFungsional_Berpakaian { get; set; }
        public string StatusFungsional_Makan { get; set; }
        public string PemeriksaanFisik_KeadaanUmum { get; set; }
        public string PemeriksaanFisik_Leher { get; set; }
        public string PemeriksaanFisik_Thoraks { get; set; }
        public string PemeriksaanFisik_Abdomen { get; set; }
        public string PemeriksaanFisik_Genito { get; set; }
        public string PemeriksaanFisik_Ekstremitas { get; set; }
        public string PemeriksaanKhusus { get; set; }
        public string Gambar2 { get; set; }
        public string PemeriksaanPenunjang { get; set; }
        public string DiagnosaMedis { get; set; }
        public string RencanaKerja { get; set; }
        public string Instruksi { get; set; }
        public string DokterPengkaji { get; set; }
        public string DokterPengkajiNama { get; set; }
        public string EdukasiPetugas { get; set; }
        public bool PenyakitYBS { get; set; }
        public bool RencanaPenanganan { get; set; }
        public bool RencanaPemeriksaan { get; set; }
        public bool DokterYangMerawat { get; set; }
        public bool DokterYangMerawatNama { get; set; }
        public bool HalYangPerluDiawasi { get; set; }
        public bool Lainlain { get; set; }
        public string Lainlain_Ket { get; set; }
        public string Petugas { get; set; }
        public string PetugasNama { get; set; }
        public string Keluarga { get; set; }
        public string Dipulangkan { get; set; }
        public bool Dipulangkan_KIE { get; set; }
        public bool Dipulangkan_ObatPulang { get; set; }
        public bool Dipulangkan_FotoRontgen { get; set; }
        public bool Dipulangkan_Laboratorium { get; set; }
        public bool Dipulangkan_KontrolPoli { get; set; }
        public bool Dipulangkan_KondisiPulang { get; set; }
        public string Dikonsulkan { get; set; }
        public bool Dikonsulkan_MRS { get; set; }
        public string Dikonsulkan_MRS_Ket { get; set; }
        public bool Dikonsulkan_DokterSpesialis { get; set; }
        public string Dikonsulkan_DokterSpesialis_Ket { get; set; }
        public string Meninggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Meninggal_JamMeninggal { get; set; }
        public string Minggat { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Minggat_JamMinggat { get; set; }
        public bool Minggat_Satpam { get; set; }
        public bool Minggat_MOD { get; set; }
        public bool Minggat_Supervisi { get; set; }
        public bool Minggat_Humas { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TindakanKeperawatan_Tgl { get; set; }
        public bool MengukurTandaVital { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MengukurTandaVital_Jam { get; set; }
        public bool MemberiObat { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MemberiObat_Jam { get; set; }
        public bool MemasangInfus { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MemasangInfus_Jam { get; set; }
        public bool MerawatLuka { get; set; }
        public string MerawatLuka_Ket { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MerawatLuka_Jam { get; set; }
        public bool MemasangDowerKateter { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MemasangDowerKateter_Jam { get; set; }
        public bool MemasangNGT { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MemasangNGT_Jam { get; set; }
        public bool MelaksanakanGastic { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MelaksanakanGastic_Jam { get; set; }
        public bool MelaksanakanFisioterapi { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MelaksanakanFisioterapi_Jam { get; set; }
        public bool MemberiKompres { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MemberiKompres_Jam { get; set; }
        public bool MemberiPosisi { get; set; }
        public string MemberiPosisi_Ket { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MemberiPosisi_Jam { get; set; }
        public bool MemberikanOksigenasi { get; set; }
        public string MemberikanOksigenasi_Ket { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MemberikanOksigenasi_Jam { get; set; }
        public bool MelaksanakanSuction { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MelaksanakanSuction_Jam { get; set; }
        public bool MemberikanLatihanNafas { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MemberikanLatihanNafas_Jam { get; set; }
        public bool MelatihTeknikRelaksasi { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MelatihTeknikRelaksasi_Jam { get; set; }
        public bool MemeriksaEKG { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MemeriksaEKG_Jam { get; set; }
        public bool MelakukanRJP { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MelakukanRJP_Jam { get; set; }
        public bool MemasangTraksi { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MemasangTraksi_Jam { get; set; }
        public bool MelakukanPenjahitanLuka { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MelakukanPenjahitanLuka_Jam { get; set; }
        public bool MemasangBalut { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MemasangBalut_Jam { get; set; }
        public bool PengambilanSampel { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> PengambilanSampel_Jam { get; set; }
        public bool EnterpasiKuku { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> EnterpasiKuku_Jam { get; set; }
        public bool Penyuluhan { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Penyuluhan_Jam { get; set; }
        public bool MengambilSampel { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MengambilSampel_Jam { get; set; }
        public bool MemasukkanObat { get; set; }
        public string MemasukkanObat_Ket { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MemasukkanObat_Jam { get; set; }
        public bool MemasangSering { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MemasangSering_Jam { get; set; }
        public bool CekGula { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> CekGula_Jam { get; set; }
        public bool NT { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> NT_Jam { get; set; }
        public bool MengambilSampelDarah { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MengambilSampelDarah_Jam { get; set; }
        public bool MemberiObatTetes { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MemberiObatTetes_Jam { get; set; }
        public bool MemberiObatMelalui { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MemberiObatMelalui_Jam { get; set; }
        public bool PemasanganBed { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> PemasanganBed_Jam { get; set; }
        public bool SpolingIrigasi { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> SpolingIrigasi_Jam { get; set; }
        public bool Diagnosa_KetidakefektifanBersihan { get; set; }
        public bool Diagnosa_Risiko { get; set; }
        public bool Diagnosa_KetidakefektifanPola { get; set; }
        public bool Diagnosa_GangguanPertukaran { get; set; }
        public bool Diagnosa_PenurunanCurahJantung { get; set; }
        public bool Diagnosa_KetidakefektifanPerfusi { get; set; }
        public bool Diagnosa_KekuranganVolumeCairan { get; set; }
        public bool Diagnosa_KelebihanVolumenCairan { get; set; }
        public bool Diagnosa_RisikoKekurangan { get; set; }
        public bool Diagnosa_Diare { get; set; }
        public bool Diagnosa_RetensisUrin { get; set; }
        public bool Diagnosa_NyeriAkut { get; set; }
        public bool Diagnosa_Hipertermia { get; set; }
        public bool Diagnosa_KerusakanMobilitas { get; set; }
        public bool Diagnosa_RisikoInfeksi { get; set; }
        public bool Diagnosa_Konstipasi { get; set; }
        public bool Rencana_LakukanManuever { get; set; }
        public bool Rencana_KeluarkanBendaAsing { get; set; }
        public bool Rencana_PasangOPA { get; set; }
        public bool Rencana_BerikanBantuan { get; set; }
        public bool Rencana_BerikanO2 { get; set; }
        public bool Rencana_MonitorSaO2 { get; set; }
        public bool Rencana_MonitorTandaVital { get; set; }
        public bool Rencana_MonitorTingkatKesadaran { get; set; }
        public bool Rencana_MonitorEKG { get; set; }
        public bool Rencana_PasangInfus { get; set; }
        public bool Rencana_HentikanPendarahan { get; set; }
        public bool Rencana_BerikanPosisiSemiflower { get; set; }
        public bool Rencana_PasangDowerCateter { get; set; }
        public bool Rencana_BerikanCairanIntravena { get; set; }
        public bool Rencana_KajiTurgorKulit { get; set; }
        public bool Rencana_AwasiTetesanCairan { get; set; }
        public bool Rencana_PasangNGT { get; set; }
        public bool Rencana_AtasiNyeri { get; set; }
        public bool Rencana_LakukanPerawatanLuka { get; set; }
        public bool Rencana_BerikanKompresHangat { get; set; }
        public bool Rencana_BerikanPosisiSemiflowerBila { get; set; }
        public bool Rencana_Delegatif { get; set; }
        public bool Rencana_MonitorIntake { get; set; }
        public bool Rencana_PasangPengaman { get; set; }
        public bool Rencana_KajiTanda { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Observasi_Tgl { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Observasi_Jam { get; set; }
        public string Observasi_Nadi { get; set; }
        public Nullable<double> Observasi_Tensi { get; set; }
        public Nullable<double> Observasi_Suhu { get; set; }
        public string Observasi_Respirasi { get; set; }
        public string Observasi_GCS_E { get; set; }
        public string Observasi_GCS_V { get; set; }
        public string Observasi_GCS_M { get; set; }
        public string Observasi_GCS_Total { get; set; }
        public string Observasi_Ka_Ki { get; set; }
        public string KondisiTerakhirSaatPulang { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamSaatPulang { get; set; }
        public string Username { get; set; }
        public int SudahRegPasien { get; set; }
        public int SudahRegPegawai { get; set; }
        public int SudahRegDokter { get; set; }
        public string TandaTangan { get; set; }
        public string TandaTanganPetugas { get; set; }
        public string TandaTanganDokterPengkaji { get; set; }
        public string Template { get; set; }
        public bool save_template { get; set; }
        public string nama_template { get; set; }
        public string DokumenID { get; set; }
        public string KategoriKasus { get; set; }
        public bool DischargePlanning { get; set; }
    }
}