﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class CatatanVisiteDetailModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglKonsul { get; set; }
        public string Kamar { get; set; }
        public string Konsul { get; set; }
        public string Paraf { get; set; }
        public string ParafNama { get; set; }
        public string Verifikator { get; set; }
        public string VerifikatorNama { get; set; }
        public string Username { get; set; }
    }
}
