﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class EMRRingkasanRiwayatMasukDanKeluarViewModel
    {
        public ListDetail<RingkasanRiwayatMasukDanKeluar_OperasiViewModelDetail> Operasi_List { get; set; }
        public ListDetail<RingkasanRiwayatMasukDanKeluar_SekunderViewModelDetail> Sekunder_List { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string DiagnosisMasuk { get; set; }
        public string Utama { get; set; }
        public string Utama_ICD { get; set; }
        public string Kekerasan { get; set; }
        public string Kekerasan_ICD { get; set; }
        public string Komplikasi { get; set; }
        public string Komplikasi_ICD { get; set; }
        public string PatologiAnatomi { get; set; }
        public string PatologiAnatomi_ICD { get; set; }
        public bool XRay { get; set; }
        public string XRay_Ket { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> XRay_Tgl { get; set; }
        public string XRay_ICD { get; set; }
        public bool CTScan { get; set; } 
        public string CTScan_Ket { get; set; }
        public Nullable<System.DateTime> CTScan_Tgl { get; set; }
        public string CTScan_ICD { get; set; }
        public bool ECG { get; set; }
        public string ECG_Ket { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> ECG_Tgl { get; set; }
        public string ECG_ICD { get; set; }
        public bool USG { get; set; }
        public string USG_Ket { get; set; }
        [DataType(DataType.Date)]

        public Nullable<System.DateTime> USG_Tgl { get; set; }
        public string USG_ICD { get; set; }
        public bool Endoscopy { get; set; }
        public string Endoscopy_Ket { get; set; }
        [DataType(DataType.Date)]

        public Nullable<System.DateTime> Endoscopy_Tgl { get; set; }
        public string Endoscopy_ICD { get; set; }
        public bool PatalogiKlinik { get; set; }
        [DataType(DataType.Date)]

        public Nullable<System.DateTime> PatalogiKlinik_Tgl { get; set; }
        public bool PatalogiAnatomi { get; set; }
        [DataType(DataType.Date)]

        public Nullable<System.DateTime> PatalogiAnatomi_Tgl { get; set; }
        public bool Mikrobiologi { get; set; }
        [DataType(DataType.Date)]

        public Nullable<System.DateTime> Mikrobiologi_Tgl { get; set; }
        public bool Lainnya1 { get; set; }
        public string Lainnya1_Ket { get; set; }
        [DataType(DataType.Date)]

        public Nullable<System.DateTime> Lainnya1_Tgl { get; set; }
        public bool Lainnya2 { get; set; }
        public string Lainnya2_Ket { get; set; }

        [DataType(DataType.Date)]

        public Nullable<System.DateTime> Lainnya2_Tgl { get; set; }
        [DataType(DataType.Date)]

        public Nullable<System.DateTime> TglMasuk { get; set; }
        [DataType(DataType.Time)]

        public Nullable<System.DateTime> JamMasuk { get; set; }
        public string LamaDirawat { get; set; }
        [DataType(DataType.Date)]

        public Nullable<System.DateTime> TglKeluar { get; set; }
        [DataType(DataType.Time)]

        public Nullable<System.DateTime> JamKeluar { get; set; }
        public string BBLahir { get; set; }
        public bool Keadaan_Sembuh { get; set; }
        public bool Keadaan_Membaik { get; set; }
        public bool Keadaan_BelumSembuh { get; set; }
        public bool Keadaan_MeninggalKurang48 { get; set; }
        public bool Keadaan_MeninggalLebih48 { get; set; }
        public bool Cara_Diijinkan { get; set; }
        public bool Cara_PulangPaksa { get; set; }
        public bool Cara_Lari { get; set; }
        public bool Cara_Pindah { get; set; }
        public bool Cara_Dirujuk { get; set; }
        public string Cara_Dirujuk_Ket { get; set; }
        public string TransfusiDarah { get; set; }
        public string GolonganDarah { get; set; }
        public string Rhesus { get; set; }
        public string TransfusiAlbumin { get; set; }
        public string Radioterapi { get; set; }
        public string InfeksiNosokomial { get; set; }
        public string PenyebabInfeksi { get; set; }
        public string KodeDokterDPJP { get; set; }
        public string KodeDokterDPJPNama { get; set; }
        public string PenyebabKematian_A { get; set; }
        public string PenyebabKematian_A_AkibatDari { get; set; }
        public string PenyebabKematian_B { get; set; }
        public string PenyebabKematian_B_AkibatDari { get; set; }
        public string PenyebabKematian_C { get; set; }
        public string Lamanya_A { get; set; }
        public string Lamanya_B { get; set; }
        public string Lamanya_C { get; set; }
        public string PenyebabKematian_2 { get; set; }
        public string Lamanya_2 { get; set; }
        public string SebabKematian_A { get; set; }
        public string SebabKematian_B { get; set; }
        public string SebabKematian_C { get; set; }
        public string SebabKematian_D { get; set; }
        public string SebabKematian_E { get; set; }
        public string KodePemberiKeterangan { get; set; }
        public string KodePemberiKeteranganNama { get; set; }
        public string Username { get; set; }
        public string DiagnosaAkhir { get; set; }
        public string DiagnosaAkhir_ICD { get; set; }

        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public List<SelectItemLisRngksnRwytMasukKeluar> ListTemplate { get; set; }
        // disable all form
        public int MODEVIEW { get; set; }

    }

    public class SelectItemLisRngksnRwytMasukKeluar
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}