﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class AsuhanKeperawatanDanObservasiPasienHemodialisisViewModel
    {
        public ListDetail<AsuhanKeperawatanDanObservasiPasienHemodialisisModelDetail> Pelaksanaan_List { get; set; }
        public ListDetail<AsuhanKeperawatanDanObservasiPasienHemodialisisTerapiModelDetail> Terapi_List { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public string Username { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public bool JenisPasien_RJ { get; set; }
        public bool JenisPasien_TravelingHD { get; set; }
        public bool JenisPasien_RI { get; set; }
        public bool JenisTindakan_Reguler { get; set; }
        public bool JenisTindakan_Elektif { get; set; }
        public bool JenisTindakan_Cito { get; set; }
        public string KeluhanUtama { get; set; }
        public string KeluhanSaatDikaji { get; set; }
        public string Kesadaran { get; set; }
        public string KeadaanUmum1 { get; set; }
        public string TekananDarah { get; set; }
        public string Suhu1 { get; set; }
        public bool Reguler { get; set; }
        public bool Ireguler { get; set; }
        public string Nadi1 { get; set; }
        public bool Respirasi_Frekwensi { get; set; }
        public string Respirasi_Frekwensi_Ket { get; set; }
        public bool Respirasi_Ronchi { get; set; }
        public bool Respirasi_Dispnea { get; set; }
        public bool Respirasi_Kusmaul { get; set; }
        public bool Konjungtiva_Anemis { get; set; }
        public bool Konjungtiva_PalpebraEdema { get; set; }
        public bool Konjungtiva_SkleraIkterik { get; set; }
        public bool Ekstremitas_TidakEdema { get; set; }
        public bool Ekstremitas_Edema { get; set; }
        public bool Ekstremitas_Pucat { get; set; }
        public bool AksesVaskuler_AVFistula { get; set; }
        public bool AksesVaskuler_Femoral { get; set; }
        public bool AksesVaskuler_DoubleLument { get; set; }
        public bool DoubleLument_Jugularis { get; set; }
        public bool DoubleLument_Subclavia { get; set; }
        public bool DoubleLument_Femoral { get; set; }
        public string ResikoJatuh { get; set; }
        public string PemeriksaanFisik_Nyeri { get; set; }
        public string Lokasi { get; set; }
        public string Jenis { get; set; }
        public string FrekuensiNyeri { get; set; }
        public string LamaNyeri { get; set; }
        public string Menjalar { get; set; }
        public string MenjalarKet { get; set; }
        public string KualitasiNyeri { get; set; }
        public string NyeriBertambah { get; set; }
        public string NyeriBerkurang { get; set; }
        public string BBKering { get; set; }
        public string BBPraHD { get; set; }
        public string Cunductivity { get; set; }
        public string SuhuMesin { get; set; }
        public string DialistFlow { get; set; }
        public string LuarMembrane { get; set; }
        public string ValumePriming { get; set; }
        public string JenisMembran { get; set; }
        public bool Heparin { get; set; }
        public bool WeightHeparin { get; set; }
        public string WeightHeparinJenis { get; set; }
        public string WeightHeparinDosis { get; set; }
        public string Petugas1 { get; set; }
        public string Petugas1Nama { get; set; }
        public string DosisAwal { get; set; }
        public string DosisPemulihan { get; set; }
        public bool TanpaHeparin { get; set; }
        public string Petugas2 { get; set; }
        public string Petugas2Nama { get; set; }
        public bool Hipervolemia { get; set; }
        public bool ResikoPendarahan { get; set; }
        public bool ResikoInfeksi { get; set; }
        public bool KetidakseimbanganVolumeCairan { get; set; }
        public bool KetidakseimbanganElektrolit { get; set; }
        public bool Ansietas { get; set; }
        public bool ResikoDefisit { get; set; }
        public bool DiagnosaKeperawatanLainnya { get; set; }
        public string DiagnosaKeperawatanLainnyaKet { get; set; }
        public string BerikanEdukasi_Ket { get; set; }
        public string Keluhan { get; set; }
        public string KeadaanUmum2 { get; set; }
        public string TekananDarah2 { get; set; }
        public string Nadi2 { get; set; }
        public string Respirasi { get; set; }
        public string Suhu2 { get; set; }
        public string SisaPriming { get; set; }
        public string TranfusiObat { get; set; }
        public string WashOut { get; set; }
        public string Minum { get; set; }
        public string JumlahCairanMasuk { get; set; }
        public string Ultrafiltrasi { get; set; }
        public string Kencing { get; set; }
        public string Muntah { get; set; }
        public string Drain { get; set; }
        public string JumlahCairanKeluar { get; set; }
        public string TotalBalance { get; set; }
        public string BBPostHD { get; set; }
        public string BloodTreatedTime { get; set; }
        public string BloodTreatedValume { get; set; }
        public string PermasalahanBesar { get; set; }
        public string PendarahanAkses { get; set; }
        public string KriteriaPulang { get; set; }
        public string DPJP { get; set; }
        public string DPJPNama { get; set; }
        public string KondisiPulang { get; set; }
        public string PrawatPunksi { get; set; }
        public string PrawatPunksiNama { get; set; }
        public string PrawatHD { get; set; }
        public string PrawatHDNama { get; set; }
        public string PrawatPenanggungJawab { get; set; }
        public string PrawatPenanggungJawabNama { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        public bool RencanaTindakan_1 { get; set; }
        public bool RencanaTindakan_2 { get; set; }
        public bool RencanaTindakan_3 { get; set; }
        public bool RencanaTindakan_4 { get; set; }
        public bool RencanaTindakan_5 { get; set; }
        public bool RencanaTindakan_6 { get; set; }
        public bool RencanaTindakan_7 { get; set; }
        public bool RencanaTindakan_8 { get; set; }
        public bool RencanaTindakan_9 { get; set; }
        public bool RencanaTindakan_10 { get; set; }
        public bool RencanaTindakan_11 { get; set; }
        public bool RencanaTindakan_12 { get; set; }
        public bool RencanaTindakan_13 { get; set; }
        public bool RencanaTindakan_14 { get; set; }
        public bool JenisDializer { get; set; }
        public bool JenisDializerReuse { get; set; }
        public string JenisDializerReuseKe { get; set; }
        public string JenisMembranHigh { get; set; }
        public bool HasilTes_1 { get; set; }
        public bool HasilTes_2 { get; set; }
        public bool HasilTes_3 { get; set; }
        public string HasilTes_Petugas { get; set; }
        public string HasilTes_PetugasNama { get; set; }
        public bool HasilTes_0 { get; set; }
        public bool AksesVaskuler_Sinistra { get; set; }
        public bool AksesVaskuler_Dextra { get; set; }
        public bool DoubleLument_Femoral2 { get; set; }
        public string AlarmTest { get; set; }
    }
}