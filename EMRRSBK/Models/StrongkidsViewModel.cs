﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class StrongkidsViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string OperasaiMayor { get; set; }
        public string KekuranganGizi { get; set; }
        public string SalahSatuKondisi { get; set; }
        public string PenurunanBB { get; set; }
        public int TotalSkor { get; set; }
        public string TotalSkorKet { get; set; }
        public string UserName { get; set; }
        public string AhliGizi { get; set; }
        public string AhliGiziNama { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }

        public bool Anoreksia { get; set; }
        public bool Dismaturitas { get; set; }
        public bool PenyakitHati { get; set; }
        public bool PenyakitMetabolik { get; set; }
        public bool LukaBakar { get; set; }
        public bool PenyakitJantung { get; set; }
        public bool PenyakitGinjal { get; set; }
        public bool Trauma { get; set; }
        public bool Displasia { get; set; }
        public bool PenyakitInfeksiHIV { get; set; }
        public bool Pankreatitis { get; set; }
        public bool RetardasiMental { get; set; }
        public bool PenyakitCeliac { get; set; }
        public bool Inflammatory { get; set; }
        public bool ShortBowelSyndrome { get; set; }
        public bool CysticFibrosis { get; set; }
        public bool Kanker { get; set; }
        public bool PenyakitOtot { get; set; }
        public bool PenyakitLainnya { get; set; }
        public string PenyakitLainnyaKet { get; set; }
    }
}