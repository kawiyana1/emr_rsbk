﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class CheckListKslamatanPasienOKViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tgl_SignIn { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam_SignIn { get; set; }
        public string IdentitasPasien { get; set; }
        public string RencananTindakan { get; set; }
        public string PersetujuanTindakan { get; set; }
        public string AreaYangDioperasi { get; set; }
        public string MesinAnestesi { get; set; }
        public string PasienMemakaiPulse { get; set; }
        public string RiwayatAlergi { get; set; }
        public string GangguanPernafasan { get; set; }
        public string RisikoPerdarahan { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tgl_TimeOut { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam_TimeOut { get; set; }
        public string PastikanTimOperasi { get; set; }
        public string BacaUlang { get; set; }
        public string ApakahProfilaksis { get; set; }
        public string ApakahAdaRisiko { get; set; }
        public string BerapaLamaPerkiraan { get; set; }
        public string SudahAntisipasi { get; set; }
        public string HalKhusus { get; set; }
        public string KesterilanPeralatan { get; set; }
        public string MasalahPeralatan { get; set; }
        public string HasilRadiologi { get; set; }
        public string PlatSudahTerpasang { get; set; }
        public string SelangSuctionBerfungsi { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tgl_SignOut { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam_SignOut { get; set; }
        public string NamaTindakan { get; set; }
        public string KelengkapanAlat { get; set; }
        public string JumlahKassa { get; set; }
        public string JumlahJarum { get; set; }
        public string PelabelanSpesimen { get; set; }
        public string PelabelanPlasenta { get; set; }
        public string AdaMasalahAlat { get; set; }
        public string CatatanKhusus { get; set; }
        public string DokterAnestesi { get; set; }
        public string DokterAnestesiNama { get; set; }
        public string PerawatAnestesi { get; set; }
        public string PerawatAnestesiNama { get; set; }
        public string PerawatSirkuler { get; set; }
        public string PerawatSirkulerNama { get; set; }
        public string OnLoop { get; set; }
        public string OnLoopNama { get; set; }
        public string Operator { get; set; }
        public string OperatorNama { get; set; }
        public string Asisten { get; set; }
        public string AsistenNama { get; set; }
        public string Instrumen { get; set; }
        public string InstrumenNama { get; set; }
        public string DokterAnak { get; set; }
        public string DokterAnakNama { get; set; }
        public string Username { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
        public int SudahRegDokterAnestesi { get; set; }
        public int SudahRegDokterAnak { get; set; }
        public int SudahRegPerawatAnestesi { get; set; }
        public int SudahRegPerawatSirkuler { get; set; }
        public int SudahRegOperator { get; set; }
        public int SudahRegInstrumen { get; set; }
        public int SudahRegAsisten { get; set; }
        public string TandaTanganDokterAnestesi { get; set; } 
        public string TandaTanganDokterAnak { get; set; }
        public string TandaTanganPerawatAnestesi { get; set; }
        public string TandaTanganPerawatSirkuler { get; set; }
        public string TandaTanganOperator { get; set; }
        public string TandaTanganInstrumen { get; set; }
        public string TandaTanganAsisten { get; set; }
    }
}