﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class PersetujuanUmumNewViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Berpartisipasi { get; set; }
        public string BerpartisipasiKet { get; set; }
        public string PrivasiKhusus { get; set; }
        public string PrivasiKhususKet { get; set; }
        public string JenisInformasi_1 { get; set; }
        public string JenisInformasi_2 { get; set; }
        public string JenisInformasi_3 { get; set; }
        public string JenisInformasi_4 { get; set; }
        public string SemuaSituasi_1 { get; set; }
        public string SemuaSituasi_2 { get; set; }
        public string SemuaSituasi_3 { get; set; }
        public string SemuaSituasi_4 { get; set; }
        public string KeluargaPihakLain_4 { get; set; }
        public string PemberiInformasi { get; set; }
        public string PemberiInformasiNama { get; set; }
        public string PasienPenanggungJawab { get; set; }
        public string TTDPemberiInformasi { get; set; }
        public string TTDPasienPenanggungJawab { get; set; }
        public int Report { get; set; }
        public string Point3Select { get; set; }
        public string Point3 { get; set; }
        public string Persetujuan { get; set; }
    }
}