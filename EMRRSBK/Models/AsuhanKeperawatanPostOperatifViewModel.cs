﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class AsuhanKeperawatanPostOperatifViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Username { get; set; }
        public bool DSubyektif_PasienMelaporkanNyeri { get; set; }
        public bool DSubyektif_PasienMenyatakanDingin { get; set; }
        public bool DSubyektif_PasienMengeluhMual { get; set; }
        public bool DSubyektif_PasienKakiKesemutan { get; set; }
        public bool DSubyektif_PasienKakiTerasaHilang { get; set; }
        public bool DObyektif_PasienTampakGelisah { get; set; }
        public bool DObyektif_PerubahanHemodinamik { get; set; }
        public bool DObyektif_SekretMenumpukOral { get; set; }
        public bool DObyektif_AdaNafasTambahan { get; set; }
        public bool DObyektif_PasienSudahTerekstubasi { get; set; }
        public bool DObyektif_PasienBelumSadar { get; set; }
        public bool DObyektif_PenurunanInspirasiEkspirasi { get; set; }
        public bool DObyektif_Sianosis { get; set; }
        public bool DObyektif_PerubahanFrekuensiNafas { get; set; }
        public bool DObyektif_PenurunanSuaraNafas { get; set; }
        public bool DObyektif_KulitDinginSuhu { get; set; }
        public bool DObyektif_PeningkatanSalivasi { get; set; }
        public bool DObyektif_Menggigil { get; set; }
        public bool DObyektif_PasienTakTerkontrol { get; set; }
        public bool DObyektif_PasienBelumBisaMenggerakkanKaki { get; set; }
        public bool DObyektif_Muntah { get; set; }
        public bool DObyektif_TD { get; set; }
        public string DObyektif_KetTD { get; set; }
        public bool DObyektif_N { get; set; }
        public string DObyektif_KetN { get; set; }
        public bool DObyektif_R { get; set; }
        public string DObyektif_KetR { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_DiagnosaKeperawatan { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_DiagnosaKeperawatanResiko { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_DiagnosaKeperawatanBersihan { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_DiagnosaKeperawatanHipotermi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_DiagnosaKeperawatanResikoKecelakaan { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_DiagnosaKeperawatanResikoGangguan{ get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_DiagnosaKeperawatanResikoHambatan { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_DiagnosaKeperawatanResikoKurangnya { get; set; }
        public bool DiagnosaKeperawatan_NyeriAkut { get; set; }
        public bool DiagnosaKeperawatan_ResikoEspirasi { get; set; }
        public bool DiagnosaKeperawatan_BersihanJalanNafas { get; set; }
        public bool DiagnosaKeperawatan_Hipotermia { get; set; }
        public bool TujuanKeperawatan_SetelahPerawatanSatuKali { get; set; }
        public bool TujuanKeperawatan_SetelahAskepTidakTerjadiAspirasi { get; set; }
        public bool TujuanKeperawatan_SetelahPasienSadar { get; set; }
        public bool TujuanKeperawatan_SetelahAskepPasien { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_PerencanaanImplementasi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_PerencanaanImplementasi1 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_PerencanaanImplementasi2 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_PerencanaanImplementasi3 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_PerencanaanImplementasi4 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_PerencanaanImplementasi5 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_PerencanaanImplementasi6 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_PerencanaanImplementasi7 { get; set; }
        public bool PerencanaanImplementasi_KajiDerajatLokasi { get; set; }
        public bool PerencanaanImplementasi_GunakanTeknikKomunikasi { get; set; }
        public bool PerencanaanImplementasi_KolaborasiDenganDokter_1 { get; set; }
        public bool PerencanaanImplementasi_AwasiTandaVital { get; set; }
        public bool PerencanaanImplementasi_AturPosisiPasien_1 { get; set; }
        public bool PerencanaanImplementasi_PantauTandaAspirasi { get; set; }
        public bool PerencanaanImplementasi_PantauTingkatKesadaran { get; set; }
        public bool PerencanaanImplementasi_PantauStatusParu { get; set; }
        public bool PerencanaanImplementasi_BersihkanJalanNafas { get; set; }
        public bool PerencanaanImplementasi_KolaborasiDenganDokter_2 { get; set; }
        public bool PerencanaanImplementasi_AturPosisiPasien_2 { get; set; }
        public bool PerencanaanImplementasi_PantauTandaKetidakEfektifan { get; set; }
        public bool PerencanaanImplementasi_AjarkanBatukEfektif { get; set; }
        public bool PerencanaanImplementasi_PantauRespirasi { get; set; }
        public bool PerencanaanImplementasi_BukaJalanNafas { get; set; }
        public bool PerencanaanImplementasi_BersihkanSekresi { get; set; }
        public bool PerencanaanImplementasi_BeriHyperoksigenasi { get; set; }
        public bool PerencanaanImplementasi_AjarkanNafasDalam { get; set; }
        public bool PerencanaanImplementasi_AuskultasiSuaraNafas { get; set; }
        public bool PerencanaanImplementasi_PantauStatusOksigenasi { get; set; }
        public bool PerencanaanImplementasi_MempertahankanSuhuTubuh { get; set; }
        public bool PerencanaanImplementasi_PantauTandaVital { get; set; }
        public bool PerencanaanImplementasi_BeriPenghangat { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_Evaluasi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_EvaluasiAspirasi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_EvaluasiJalanNafas { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_EvaluasiJalanPasien { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_EvaluasiJalanPasienAman { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_EvaluasiJalanPerasaanPasien { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_EvaluasiJalanPasienMenyatakanKakinya { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_EvaluasiJalanPasienMenyatakanTdkPusing { get; set; }
        public string Evaluasi_PasienMenyatakanNyeri_TD { get; set; }
        public string Evaluasi_PasienMenyatakanNyeri_N { get; set; }
        public string Evaluasi_PasienMenyatakanNyeri_RR { get; set; }
        public string Evaluasi_PasienMenyatakanNyeri_A { get; set; }
        public string Evaluasi_PasienMenyatakanNyeri_P { get; set; }
        public string Evaluasi_AspirasiTerhindar_S { get; set; }
        public string Evaluasi_AspirasiTerhindar_A { get; set; }
        public string Evaluasi_AspirasiTerhindar_P { get; set; }
        public string Evaluasi_JalanNafasEfektif_S { get; set; }
        public string Evaluasi_JalanNafasEfektif_TD { get; set; }
        public string Evaluasi_JalanNafasEfektif_N { get; set; }
        public string Evaluasi_JalanNafasEfektif_R { get; set; }
        public string Evaluasi_JalanNafasEfektif_A { get; set; }
        public string Evaluasi_JalanNafasEfektif_P { get; set; }
        public string Evaluasi_MerasakanTubuhnyaHangat_Suhu { get; set; }
        public string Evaluasi_MerasakanTubuhnyaHangat_R { get; set; }
        public string Evaluasi_MerasakanTubuhnyaHangat_N { get; set; }
        public string Evaluasi_MerasakanTubuhnyaHangat_Kesadaran { get; set; }
        public string Evaluasi_MerasakanTubuhnyaHangat_A { get; set; }
        public string Evaluasi_MerasakanTubuhnyaHangat_P { get; set; }
        public bool Diagnosakeperawatan_ResikoKecelakaanCidera { get; set; }
        public bool Diagnosakeperawatan_GangguanRasaNyaman { get; set; }
        public bool Diagnosakeperawatan_HambatanMobilitasErek { get; set; }
        public bool Diagnosakeperawatan_KurangnyaVolumeCairan { get; set; }
        public bool TujuanKeperawatan_PasienAmanSelama { get; set; }
        public bool TujuanKeperawatan_SetelahAskepMual { get; set; }
        public bool TujuanKeperawatan_SetelahPerawatanTigaSampaiEmpat { get; set; }
        public bool TujuanKeperawatan_SetelahAsuhanKeperawatan { get; set; }
        public bool PerencanaanImplementasi_TingkatkanKeamananKetajaman { get; set; }
        public bool PerencanaanImplementasi_JagaPosisiImobil { get; set; }
        public bool PerencanaanImplementasi_UbahTempatTubuh { get; set; }
        public bool PerencanaanImplementasi_CegahInjuriJatuh { get; set; }
        public bool PerencanaanImplementasi_PasangPengamanTempat { get; set; }
        public bool PerencanaanImplementasi_PantauPenggunaanObatAnestesi { get; set; }
        public bool PerencanaanImplementasi_AturPosisiPasien_3 { get; set; }
        public bool PerencanaanImplementasi_MeningkatkanKeseimbanganCairan { get; set; }
        public bool PerencanaanImplementasi_PantauVitalSign { get; set; }
        public bool PerencanaanImplementasi_PantauGejalaMual { get; set; }
        public bool PerencanaanImplementasi_PantauJumlahMuntah { get; set; }
        public bool PerencanaanImplementasi_PantauTurgorKulit { get; set; }
        public bool PerencanaanImplementasi_PantauMasukanKeluaranCairan { get; set; }
        public bool PerencanaanImplementasi_KolaborasiDenganDokter_3 { get; set; }
        public bool PerencanaanImplementasi_AturPosisiPasien_4 { get; set; }
        public bool PerencanaanImplementasi_BantuPergerakanEkstremitas { get; set; }
        public bool PerencanaanImplementasi_AjarkanProsesPergerakan { get; set; }
        public bool PerencanaanImplementasi_AjarkanLatihanPergerakan { get; set; }
        public bool PerencanaanImplementasi_AjarkanPergerakanAman { get; set; }
        public bool PerencanaanImplementasi_LatihanAngkatGerakan { get; set; }
        public bool PerencanaanImplementasi_LakukanPenilaianBromage { get; set; }
        public bool PerencanaanImplementasi_AwasiTNR { get; set; }
        public bool PerencanaanImplementasi_MonitorStatusDehidrasi { get; set; }
        public bool PerencanaanImplementasi_KolaborasiPemberianCairan { get; set; }
        public bool PerencanaanImplementasi_MonitorIntakeOutput { get; set; }
        public bool PerencanaanImplementasi_MonitorTingkatHB { get; set; }
        public string Evaluasi_PasienAmanSelama_S { get; set; }
        public string Evaluasi_PasienAmanSelama_A { get; set; }
        public string Evaluasi_PasienAmanSelama_P { get; set; }
        public string Evaluasi_PerasaanPasienLega_TD { get; set; }
        public string Evaluasi_PerasaanPasienLega_N { get; set; }
        public string Evaluasi_PerasaanPasienLega_R { get; set; }
        public string Evaluasi_PerasaanPasienLega_A { get; set; }
        public string Evaluasi_PerasaanPasienLega_P { get; set; }
        public string Evaluasi_PasienMenyatakanKakinya_A { get; set; }
        public string Evaluasi_PasienMenyatakanKakinya_P { get; set; }
        public string Evaluasi_PasienMenyatakanTidakPusing_A { get; set; }
        public string Evaluasi_PasienMenyatakanTidakPusing_P { get; set; }
        public string DiagnosaKeperawatan_ResikoEspirasi_DS { get; set; }
        public string DiagnosaKeperawatan_BersihanJalanNafas_DS { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        public string NamaPasien { get; set; }
        public int? Umur { get; set; }
        public string Alamat { get; set; }
        public string JenisKelamin { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
    }
}