﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class EMRIlmuPenyakitMataViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Username { get; set; }
        public bool Rujukan_Ya { get; set; }
        public bool Rujukan_Ya_RS { get; set; }
        public string Rujukan_Ya_RS_Ket { get; set; }
        public bool Rujukan_Ya_Dr { get; set; }
        public string Rujukan_Ya_Dr_Ket { get; set; }
        public bool Rujukan_Ya_Puskesmas { get; set; }
        public string Rujukan_Ya_Puskesmas_Ket { get; set; }
        public bool Rujukan_Ya_Rujukan { get; set; }
        public string Rujukan_Ya_Rujukan_Ket { get; set; }
        public bool Rujukan_Ya_Lainya { get; set; }
        public string Rujukan_Ya_Lainya_Ket { get; set; }
        public bool Rujukan_Tidak { get; set; }
        public bool Rujukan_Tidak_Sendiri { get; set; }
        public bool Rujukan_Tidak_Diantar { get; set; }
        public string Rujukan_Tidak_Diantar_Ket { get; set; }
        public string Asuransi { get; set; }
        public string Asuransi_Ket { get; set; }
        public string NamaKeluarga { get; set; }
        public string NoHP_Keluarga { get; set; }
        public string Alamat { get; set; }
        public string RiwayatAlergi { get; set; }
        public string RiwayatAlergi_Obat { get; set; }
        public string RiwayatAlergi_Obat_Ket { get; set; }
        public string RiwayatAlergi_Makanan { get; set; }
        public string RiwayatAlergi_Makanan_Ket { get; set; }
        public string RiwayatAlergi_Lain { get; set; }
        public string RiwayatAlergi_Lain_Ket { get; set; }
        public string Nyeri { get; set; }
        public string Lokasi { get; set; }
        public string Intensitas { get; set; }
        public string Jenis { get; set; }
        public string KeluhanUtama { get; set; }
        public string RiwayatPenyakitSekarang { get; set; }
        public string RiwayatPenyakitDahulu { get; set; }
        public string VisusAwal_OD { get; set; }
        public string VisusAwal_OS { get; set; }
        public string Kacamata_OD { get; set; }
        public string Kacamata_OS { get; set; }
        public string Palpebra_OD { get; set; }
        public string Palpebra_OS { get; set; }
        public string Kanjungtiva_OD { get; set; }
        public string Kanjungtiva_OS { get; set; }
        public string Kornea_OD { get; set; }
        public string Kornea_OS { get; set; }
        public string BilikMata_OD { get; set; }
        public string BilikMata_OS { get; set; }
        public string Iris_OD { get; set; }
        public string Iris_OS { get; set; }
        public string Pupil_OD { get; set; }
        public string Pupil_OS { get; set; }
        public string Lensa_OD { get; set; }
        public string Lensa_OS { get; set; }
        public string Vitreus_OD { get; set; }
        public string Vitreus_OS { get; set; }
        public string Fundskopi_OD { get; set; }
        public string Fundskopi_OS { get; set; }
        public string Tekanan_OD { get; set; }
        public string Tekanan_OS { get; set; }
        public string Schiotz_OD { get; set; }
        public string Schiotz_OS { get; set; }
        public string Aplanasi_OD { get; set; }
        public string Aplanasi_OS { get; set; }
        public string TestAnel { get; set; }
        public string TestButaWarna { get; set; }
        public string PemeriksaanLain { get; set; }
        public string Diagnosa { get; set; }
        public string Terapi { get; set; }
        public string Komplikasi { get; set; }
        public bool BolehPulang { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamKeluar { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglKeluar { get; set; }
        public string Kontrol { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> KontrolTanggal { get; set; }
        public string Catatan { get; set; }
        public string DPJP { get; set; }
        public string DPJPNama { get; set; }
        public string ADD_PD_OD { get; set; }
        public string ADD_PD_OS { get; set; }
        public string TandaTanganDokter { get; set; }
        public string TandaTangan { get; set; }
        public int Nomor { get; set; }

        public string nama_template { get; set; }
        public bool save_template { get; set; }
        public int dokumenid { get; set; }
        public List<SelectItemListPoliMata> ListTemplate { get; set; }
        // disable all form
        public int MODEVIEW { get; set; }
    }

    public class SelectItemListPoliMata
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}