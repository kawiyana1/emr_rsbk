//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSBK.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class EMR_VIEW_LABORATORIUM
    {
        public string NoSystem { get; set; }
        public string RegNo { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string DokterPengirim { get; set; }
        public string SectionName { get; set; }
        public string PenanggungJawab { get; set; }
        public string Analis { get; set; }
        public string NRM { get; set; }
        public string Deskripsi { get; set; }
    }
}
