//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSBK.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class AsuhanKeperawatanPreOperatif
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string UserName { get; set; }
        public string DiagnosaPreOp { get; set; }
        public Nullable<System.DateTime> MulaiAnastesi { get; set; }
        public Nullable<System.DateTime> SelesaiAnastesi { get; set; }
        public string TeknikAnastesi { get; set; }
        public string BB { get; set; }
        public bool DSubyektif_PasienBelumTahu { get; set; }
        public bool DSubyektif_MulutTerasaPahit { get; set; }
        public bool DSubyektif_Haus { get; set; }
        public bool DSubyektif_PuasaLama { get; set; }
        public bool DSubyektif_PasienMenyatakanNyeri { get; set; }
        public bool DObyektif_PasienTampakGelisah { get; set; }
        public bool DObyektif_BerkeringatDingin { get; set; }
        public string DObyektif_TD { get; set; }
        public string DObyektif_N { get; set; }
        public string DObyektif_R { get; set; }
        public bool DObyektif_ProduksiUrine { get; set; }
        public string DObyektif_KetProduksiUrine { get; set; }
        public bool DObyektif_BibirKering { get; set; }
        public bool DObyektif_KukuSianosis { get; set; }
        public bool DObyektif_Merintih { get; set; }
        public bool DObyektif_Menangis { get; set; }
        public Nullable<System.DateTime> JAM_DiagnosaKeperawatan { get; set; }
        public Nullable<System.DateTime> JAM_DiagnosaKeperawatanResiko { get; set; }
        public Nullable<System.DateTime> JAM_DiagnosaKeperawatanNyeri { get; set; }
        public bool DiagnosaKeperawatan_CemasKurangPengetahuan { get; set; }
        public bool DiagnosaKeperawatan_RisikoDefisitvolumeCairan { get; set; }
        public bool DiagnosaKeperawatan_NyeriProsesPenyakitnya { get; set; }
        public bool TujuanKeperawatan_SetelahAskepCemas { get; set; }
        public bool TujuanKeperawatan_SetelahAskepKeseimbangan { get; set; }
        public bool TujuanKeperawatan_SetelahPerawatan { get; set; }
        public Nullable<System.DateTime> JAM_PerencanaanImplementasi { get; set; }
        public Nullable<System.DateTime> JAM_PerencanaanImplementasiKajitingkat { get; set; }
        public Nullable<System.DateTime> JAM_PerencanaanImplementasiKajiderajat { get; set; }
        public bool PerencanaanImplementasi_KajiTingkatKecemasan { get; set; }
        public bool PerencanaanImplementasi_OrientasikanDenganTim { get; set; }
        public bool PerencanaanImplementasi_JelaskanJenisProsedur { get; set; }
        public bool PerencanaanImplementasi_BeriDoronganPasien { get; set; }
        public bool PerencanaanImplementasi_DampingPasienUntuk { get; set; }
        public bool PerencanaanImplementasi_AjarkanTeknikRelaksasi { get; set; }
        public bool PerencanaanImplementasi_KolaborasiPemberianObat { get; set; }
        public bool PerencanaanImplementasi_KajiTingkatKekurangan { get; set; }
        public bool PerencanaanImplementasi_KolaborasiPemberianCairan { get; set; }
        public bool PerencanaanImplementasi_MonitorMasukanKeluaran { get; set; }
        public bool PerencanaanImplementasi_MonitorHaemodinamik { get; set; }
        public bool PerencanaanImplementasi_MonitorPendarahan { get; set; }
        public bool PerencanaanImplementasi_KajiDerajatLokasi { get; set; }
        public bool PerencanaanImplementasi_GunakanTeknikKomunikasi { get; set; }
        public bool PerencanaanImplementasi_KolaborasiDenganDokter { get; set; }
        public Nullable<System.DateTime> JAM_Evaluasi { get; set; }
        public Nullable<System.DateTime> JAM_EvaluasiPasienMengatakan { get; set; }
        public Nullable<System.DateTime> JAM_EvaluasiPasienMelaporkan { get; set; }
        public string Evaluasi_PasienTampakTenang_TD { get; set; }
        public string Evaluasi_PasienTampakTenang_N { get; set; }
        public string Evaluasi_PasienTampakTenang_R { get; set; }
        public string Evaluasi_PasienTampakTenang_A { get; set; }
        public string Evaluasi_PasienTampakTenang_P { get; set; }
        public string Evaluasi_CairanMasuk { get; set; }
        public string Evaluasi_CairanKeluar { get; set; }
        public string Evaluasi_KebutuhanCairanSeimbang_TD { get; set; }
        public string Evaluasi_KebutuhanCairanSeimbang_N { get; set; }
        public string Evaluasi_KebutuhanCairanSeimbang_R { get; set; }
        public string Evaluasi_KebutuhanCairanSeimbang_A { get; set; }
        public string Evaluasi_KebutuhanCairanSeimbang_P { get; set; }
        public string Evaluasi_PasienMelaporkanNyeri_A { get; set; }
        public string Evaluasi_PasienMelaporkanNyeri_P { get; set; }
        public Nullable<System.DateTime> Jam { get; set; }
    }
}
