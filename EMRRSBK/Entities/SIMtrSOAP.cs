//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSBK.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class SIMtrSOAP
    {
        public int No { get; set; }
        public string NoReg { get; set; }
        public string NRM { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public int Nomor { get; set; }
        public System.DateTime Tanggal { get; set; }
        public System.TimeSpan Jam { get; set; }
        public System.DateTime TanggalInput { get; set; }
        public string DokterID { get; set; }
        public string DPJP { get; set; }
        public string Soap_S { get; set; }
        public string Soap_O { get; set; }
        public string Soap_A { get; set; }
        public string Soap_P { get; set; }
        public string Instruksi { get; set; }
        public string Bebas { get; set; }
        public bool CPPTDokter { get; set; }
        public Nullable<bool> ValidasiDokter { get; set; }
        public Nullable<bool> ValidasiDPJP { get; set; }
        public string Username { get; set; }
        public string NamaProfesi { get; set; }
        public string Tipe { get; set; }
        public Nullable<System.TimeSpan> T_Jam { get; set; }
        public Nullable<System.DateTime> T_Tanggal { get; set; }
        public string T_Inisial { get; set; }
        public Nullable<System.TimeSpan> B_Jam { get; set; }
        public Nullable<System.DateTime> B_Tanggal { get; set; }
        public string B_Inisial { get; set; }
        public Nullable<System.TimeSpan> K_Jam { get; set; }
        public Nullable<System.DateTime> K_Tanggal { get; set; }
        public string K_Inisial { get; set; }
        public string Adime_A { get; set; }
        public string Adime_D { get; set; }
        public string Adime_I { get; set; }
        public string Adime_ME { get; set; }
        public string Sbar_S { get; set; }
        public string Sbar_B { get; set; }
        public string Sbar_A { get; set; }
        public string Sbar_R { get; set; }
        public Nullable<bool> ValidasiInisial_T { get; set; }
        public Nullable<bool> ValidasiInisial_B { get; set; }
        public Nullable<bool> ValidasiInisial_K { get; set; }
        public Nullable<bool> Penanda_DPJP { get; set; }
        public Nullable<System.DateTime> PenandaWaktu_DPJP { get; set; }
        public string Menyerahkan_Shift { get; set; }
        public string Menyerahkan_Petugas { get; set; }
        public Nullable<bool> Menyerahkan_Validasi { get; set; }
        public string Menerima_Shift { get; set; }
        public string Menerima_Petugas { get; set; }
        public Nullable<bool> Menerima_Validasi { get; set; }
        public string KaRU_Shift { get; set; }
        public string KaRU_Petugas { get; set; }
        public Nullable<bool> KaRU_Validasi { get; set; }
        public string Profesi { get; set; }
        public byte[] TandaTanganDPJP { get; set; }
        public byte[] TandaTangan { get; set; }
        public Nullable<System.DateTime> TanggalBacaUlang { get; set; }
        public Nullable<System.DateTime> TanggalKonfirmasi { get; set; }
        public Nullable<System.DateTime> JamBacaUlang { get; set; }
        public Nullable<System.DateTime> JamKonfirmasi { get; set; }
        public string PetugasKonfirmasi { get; set; }
        public string PetugasBacaUlang { get; set; }
        public string MenerimaPasien { get; set; }
        public string MenyerahkanPasien { get; set; }
        public byte[] TandaTanganPetugasKonfirmasi { get; set; }
        public byte[] TandaTanganPetugasBacaUlang { get; set; }
        public byte[] TandaTanganMenerimaPasien { get; set; }
        public byte[] TandaTanganMenyerahkanPasien { get; set; }
        public string Ruangan { get; set; }
        public string Kelas { get; set; }
        public Nullable<bool> Kritis { get; set; }
        public Nullable<bool> VerifikasiDPJP { get; set; }
    }
}
