﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class EMRAssesmentPraOperasiPerempuanController : Controller
    {
        // GET: EMRAssesmentPraOperasiPerempuan
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string noreg, string nrm, int view = 0)
        {
            var model = new EMRAssesmentPraOperasiPerempuanViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var dokumen = s.AssesmentPraOperasiPerempuan.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRAssesmentPraOperasiPerempuanViewModel>(dokumen);
                        var dpjpop = sim.mDokter.FirstOrDefault(x => x.DokterID == model.DPJPOperator);
                        if (dpjpop != null)
                        {
                            model.DPJPOperatorNama = dpjpop.NamaDOkter;
                        }
                        var dpjpanestesi = sim.mDokter.FirstOrDefault(x => x.DokterID == model.DPJPAnastesi);
                        if (dpjpanestesi != null)
                        {
                            model.DPJPAnastesiNama = dpjpanestesi.NamaDOkter;
                        }

                        model.MODEVIEW = view;
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                    }
                    else
                    {
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                        model.NRM = nrm;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                    }

                    var dokumen_temp = s.trTemplate.Where(e => e.DokumenID == "Assesment Pra Operasi Perempuan").ToList();

                    ViewBag.DokumenID = "Assesment Pra Operasi Perempuan";
                    ViewBag.NamaDokumen = "Assesment Pra Operasi Perempuan";

                    model.ListTemplate = new List<SelectItemListAsesmentPraOperasiPerempuan>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListAsesmentPraOperasiPerempuan()
                        {
                            Value = list.NoReg,
                            Text = list.NamaTemplate
                        });
                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string noreg, string copy, string nrm)
        {
            var model = new EMRAssesmentPraOperasiPerempuanViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var dokumen = s.AssesmentPraOperasiPerempuan.FirstOrDefault(x => x.NoReg == copy);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRAssesmentPraOperasiPerempuanViewModel>(dokumen);
                        model.NoReg = noreg;
                        model.NRM = nrm;
                        model.SectionID = sectionid;

                        var dpjpop = sim.mDokter.FirstOrDefault(x => x.DokterID == model.DPJPOperator);
                        if (dpjpop != null)
                        {
                            model.DPJPOperatorNama = dpjpop.NamaDOkter;
                        }
                        var dpjpanestesi = sim.mDokter.FirstOrDefault(x => x.DokterID == model.DPJPAnastesi);
                        if (dpjpanestesi != null)
                        {
                            model.DPJPAnastesiNama = dpjpanestesi.NamaDOkter;
                        }

                    }
                    else
                    {
                        model.NRM = nrm;
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                    }
                    model.MODEVIEW = 0;

                    var dokumen_temp = s.trTemplate.Where(e => e.DokumenID == "Assesment Pra Operasi Perempuan").ToList();

                    ViewBag.DokumenID = "Assesment Pra Operasi Perempuan";
                    ViewBag.NamaDokumen = "Assesment Pra Operasi Perempuan";

                    model.ListTemplate = new List<SelectItemListAsesmentPraOperasiPerempuan>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListAsesmentPraOperasiPerempuan()
                        {
                            Value = list.NoReg,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {

            if (ModelState.IsValid)
            {
                var req = Request.Form[""];
            }
            try
            {
                var item = new EMRAssesmentPraOperasiPerempuanViewModel();

                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var model = s.AssesmentPraOperasiPerempuan.FirstOrDefault(x => x.NoReg == item.NoReg);
                    var activity = "";
                    var dokumenid = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<AssesmentPraOperasiPerempuan>(item);
                        s.AssesmentPraOperasiPerempuan.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoReg == item.NoReg);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Create Assesmen Umum";
                    }
                    else
                    {
                        model = IConverter.Cast<AssesmentPraOperasiPerempuan>(item);
                        s.AssesmentPraOperasiPerempuan.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoReg == item.NoReg);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Update Assesmen Umum";
                    }

                    if (item.save_template == true && item.nama_template != null)
                    {
                        var temp = new trTemplate();
                        temp.NoReg = item.NoReg;
                        temp.NamaTemplate = item.nama_template;
                        temp.DokterID = item.DPJPOperator;
                        temp.DokumenID = "Assesment Pra Operasi Perempuan";
                        temp.SectionID = sectionid;
                        s.trTemplate.Add(temp);
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoReg}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}