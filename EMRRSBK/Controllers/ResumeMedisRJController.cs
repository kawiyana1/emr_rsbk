﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class ResumeMedisRJController : Controller
    {
        // GET: ResumeMedisRJ
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();
            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    model.NoReg = noreg;
                    model.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.ResumeMedisRJ.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == model.SectionID);
                        if (item != null)
                        {
                            model.rsmmedisRJ = IConverter.Cast<ResumeMedisRJViewModel>(item);
                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.DokterID);
                            if (dokter != null)
                            {
                                model.rsmmedisRJ.DokterIDNama = dokter.NamaDOkter;
                            }
                            model.rsmmedisRJ.Poliklinik = Request.Cookies["SectionNamaPelayanan"].Value;
                            model.rsmmedisRJ.Alamat = m.Alamat;
                            model.rsmmedisRJ.Kewarganegaraan = m.Nationality;
                            model.rsmmedisRJ.NoReg = noreg;
                            model.rsmmedisRJ.SectionID = Request.Cookies["SectionIDPelayanan"].Value;

                            //var fingerdoktermerawat = eEMR.ResumeMedisRJ.Where(x => x.DokterRawat == model.rsmmedisRJ.DokterRawat && x.TandaTanganDokter == null && x.NRM == model.NRM).FirstOrDefault();
                            //if (fingerdoktermerawat != null)
                            //{
                            //    model.rsmmedisRJ.SudahRegDokter = 1;
                            //}
                            //else
                            //{
                            //    model.rsmmedisRJ.SudahRegDokter = 0;
                            //}
                            //if (model.rsmmedisRJ.DokterRawat != null)
                            //{
                            //    ViewBag.UrlFingerDokterResume = GetEncodeUrlDokterResume(model.rsmmedisRJ.DokterRawat, model.rsmmedisRJ.NoReg, model.SectionID);
                            //}
                        }
                        else
                        {
                            item = new ResumeMedisRJ();
                            model.rsmmedisRJ = IConverter.Cast<ResumeMedisRJViewModel>(item);
                            model.rsmmedisRJ.TglKeluar = DateTime.Today;
                            model.rsmmedisRJ.Poliklinik = Request.Cookies["SectionNamaPelayanan"].Value;
                            model.rsmmedisRJ.Alamat = m.Alamat;
                            model.rsmmedisRJ.Kewarganegaraan = m.Nationality;
                            model.rsmmedisRJ.NoReg = m.NoReg;
                            model.rsmmedisRJ.SectionID = Request.Cookies["SectionIDPelayanan"].Value;

                            //ViewBag.UrlFingerDokterResume = GetEncodeUrlDokterResume(model.rsmmedisRJ.DokterRawat, model.rsmmedisRJ.NoReg, model.SectionID);
                            //model.rsmmedisRJ.Tanggal_KLR = DateTime.Today;
                            //model.rsmmedisRJ.Tanggal_MRS = DateTime.Today;
                            //model.rsmmedisRJ.Dokter1_Kontrol = DateTime.Today;
                            //model.rsmmedisRJ.NoReg = noreg;
                            //model.rsmmedisRJ.NRM = model.NRM;
                            //model.rsmmedisRJ.SectionID = sectionid;
                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string ResumeMedisRJ_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            item.rsmmedisRJ.NoReg = item.rsmmedisRJ.NoReg;
                            item.rsmmedisRJ.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                            //item.rsmmedisRJ.NRM = item.rsmmedisRJ.NRM;
                            //item.rsmmedisRJ.Tanggal = DateTime.Now;
                            var model = eEMR.ResumeMedisRJ.FirstOrDefault(x => x.SectionID == item.rsmmedisRJ.SectionID && x.NoReg == item.rsmmedisRJ.NoReg);

                            if (model == null)
                            {
                                model = new ResumeMedisRJ();
                                var o = IConverter.Cast<ResumeMedisRJ>(item.rsmmedisRJ);
                                eEMR.ResumeMedisRJ.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<ResumeMedisRJ>(item.rsmmedisRJ);
                                eEMR.ResumeMedisRJ.AddOrUpdate(model);
                            }

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFResumeMedisRJ(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["SectionNamaPelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"AssResumeMedis.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"AssResumeMedis", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"AssResumeMedis;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }

        public string GetEncodeUrlDokterResume(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifResumeMedis/get_keydokterresume";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }
    }
}