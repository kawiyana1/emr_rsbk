﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class CatatanAnestesiDanSedasiController : Controller
    {
        // GET: CatatanAnestesiDanSedasi
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.CatatanAnastesiSedasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.CttSedasiAnes = IConverter.Cast<CatatanAnastesiSedasiViewModel>(item);

                            var penata = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.PenataAnestesi);
                            if (penata != null)
                            {
                                model.CttSedasiAnes.PenataAnestesiNama = penata.NamaDOkter;
                            }

                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.DokterOperator);
                            if (dokter != null)
                            {
                                model.CttSedasiAnes.DokterOperatorNama = dokter.NamaDOkter;
                            }

                            var spesialis = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.SpsesialisAnastesi);
                            if (spesialis != null)
                            {
                                model.CttSedasiAnes.SpsesialisAnastesiNama = spesialis.NamaDOkter;
                            }

                            model.CttSedasiAnes.NamaPasien = m.NamaPasien;
                            model.CttSedasiAnes.NoReg = noreg;
                            model.CttSedasiAnes.NRM = model.NRM;
                            model.CttSedasiAnes.SectionID = sectionid;
                            model.CttSedasiAnes.JenisKelamin = m.JenisKelamin;
                            model.CttSedasiAnes.Report = 1;
                        }
                        else
                        {
                            item = new CatatanAnastesiSedasi();
                            model.CttSedasiAnes = IConverter.Cast<CatatanAnastesiSedasiViewModel>(item);
                            model.CttSedasiAnes.NamaPasien = m.NamaPasien;
                            model.CttSedasiAnes.NoReg = noreg;
                            model.CttSedasiAnes.NRM = model.NRM;
                            model.CttSedasiAnes.SectionID = sectionid;
                            model.CttSedasiAnes.JenisKelamin = m.JenisKelamin;
                            model.CttSedasiAnes.KeluarRuang_Jam = DateTime.Now;
                            model.CttSedasiAnes.MasukRuang_Jam = DateTime.Now;
                        }

                        #region ===Detail Vital Sign
                        var ListVital = eEMR.CatatanAnastesiSedasi_VitalSign.Where(x => x.SectionID == sectionid && x.NoReg == noreg).ToList();
                        if (ListVital != null)
                        {

                            model.CttSedasiAnes.Vitalsign_List = new ListDetail<CatatanAnastesiSedasiVitalSignModelDetail>();

                            foreach (var x in ListVital)
                            {
                                var y = IConverter.Cast<CatatanAnastesiSedasiVitalSignModelDetail>(x);
                                model.CttSedasiAnes.Vitalsign_List.Add(false, y);
                            }

                        }
                        #endregion

                        #region ===Detail Pemantauan
                        var ListPemantauan = eEMR.CatatanAnastesiSedasi_Pemantauan.Where(x => x.SectionID == sectionid && x.NoReg == noreg).ToList();
                        if (ListPemantauan != null)
                        {

                            model.CttSedasiAnes.Pemantauan_List = new ListDetail<CatatanAnastesiSedasiPemantauanModelDetail>();

                            foreach (var x in ListPemantauan)
                            {
                                var y = IConverter.Cast<CatatanAnastesiSedasiPemantauanModelDetail>(x);
                                model.CttSedasiAnes.Pemantauan_List.Add(false, y);
                            }

                        }
                        #endregion

                        #region ===Detail Obat
                        var ListObat = eEMR.CatatanAnastesiSedasi_Obat.Where(x => x.SectionID == sectionid && x.NoReg == noreg).ToList();
                        if (ListObat != null)
                        {

                            model.CttSedasiAnes.Obat_List = new ListDetail<CatatanAnastesiSedasiObatModelDetail>();

                            foreach (var x in ListObat)
                            {
                                var y = IConverter.Cast<CatatanAnastesiSedasiObatModelDetail>(x);
                                var namaobat = eSIM.mBarang.FirstOrDefault(z => z.Kode_Barang == y.Obat);
                                y.ObatNama = namaobat == null ? "" : namaobat.Nama_Barang;
                                model.CttSedasiAnes.Obat_List.Add(false, y);
                            }

                        }
                        #endregion

                        #region ===Detail Infus Perifer
                        var ListInfusPer = eEMR.CatatanAnastesiSedasi_InfusPerifer.Where(x => x.SectionID == sectionid && x.NoReg == noreg).ToList();
                        if (ListInfusPer != null)
                        {

                            model.CttSedasiAnes.InfusPerifer_List = new ListDetail<CatatanAnastesiSedasiInfusPeriModelDetail>();

                            foreach (var x in ListObat)
                            {
                                var y = IConverter.Cast<CatatanAnastesiSedasiInfusPeriModelDetail>(x);
                                model.CttSedasiAnes.InfusPerifer_List.Add(false, y);
                            }

                        }
                        #endregion
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string CatatanAnestesiDanSedasi_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.CttSedasiAnes.NoReg = item.CttSedasiAnes.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.CttSedasiAnes.SectionID = sectionid;
                            item.CttSedasiAnes.NRM = item.CttSedasiAnes.NRM;
                            var model = eEMR.CatatanAnastesiSedasi.FirstOrDefault(x => x.SectionID == sectionid && x.NoReg == item.CttSedasiAnes.NoReg);

                            if (model == null)
                            {
                                model = new CatatanAnastesiSedasi();
                                var o = IConverter.Cast<CatatanAnastesiSedasi>(item.CttSedasiAnes);
                                eEMR.CatatanAnastesiSedasi.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<CatatanAnastesiSedasi>(item.CttSedasiAnes);
                                eEMR.CatatanAnastesiSedasi.AddOrUpdate(model);
                            }

                            #region ===== Detail Vital

                            if (item.CttSedasiAnes.Vitalsign_List == null) item.CttSedasiAnes.Vitalsign_List = new ListDetail<CatatanAnastesiSedasiVitalSignModelDetail>();
                            item.CttSedasiAnes.Vitalsign_List.RemoveAll(x => x.Remove);
                            foreach (var x in item.CttSedasiAnes.Vitalsign_List)
                            {
                                x.Model.SectionID = sectionid;
                                x.Model.NoReg = item.CttSedasiAnes.NoReg;
                                x.Model.NRM = item.CttSedasiAnes.NRM;
                                //x.Model.Tanggal = DateTime.Today;
                                //x.Model.Jam = DateTime.Now;

                            }
                            var new_List = item.CttSedasiAnes.Vitalsign_List;
                            var real_List = eEMR.CatatanAnastesiSedasi_VitalSign.Where(x => x.SectionID == item.CttSedasiAnes.SectionID && x.NoReg == item.CttSedasiAnes.NoReg).ToList();
                            foreach (var x in real_List)
                            {
                                var z = new_List.FirstOrDefault(y => y.Model.No == x.No);
                                if (z == null)
                                {
                                    eEMR.CatatanAnastesiSedasi_VitalSign.Remove(x);
                                }
                            }
                            foreach (var x in new_List)
                            {

                                var _m = real_List.FirstOrDefault(y => y.No == x.Model.No);

                                if (_m == null)
                                {
                                    _m = new CatatanAnastesiSedasi_VitalSign();
                                    eEMR.CatatanAnastesiSedasi_VitalSign.Add(IConverter.Cast<CatatanAnastesiSedasi_VitalSign>(x.Model));
                                }

                                else
                                {
                                    _m = IConverter.Cast<CatatanAnastesiSedasi_VitalSign>(x.Model);
                                    eEMR.CatatanAnastesiSedasi_VitalSign.AddOrUpdate(_m);
                                }

                            }

                            #endregion

                            #region ===== Detail Pemantauan

                            if (item.CttSedasiAnes.Pemantauan_List == null) item.CttSedasiAnes.Pemantauan_List = new ListDetail<CatatanAnastesiSedasiPemantauanModelDetail>();
                            item.CttSedasiAnes.Pemantauan_List.RemoveAll(x => x.Remove);
                            foreach (var x in item.CttSedasiAnes.Pemantauan_List)
                            {
                                x.Model.SectionID = sectionid;
                                x.Model.NoReg = item.CttSedasiAnes.NoReg;
                                x.Model.NRM = item.CttSedasiAnes.NRM;
                                //x.Model.Tanggal = DateTime.Today;
                                //x.Model.Jam = DateTime.Now;

                            }
                            var new_ListPemantauan = item.CttSedasiAnes.Pemantauan_List;
                            var real_ListPemantauan = eEMR.CatatanAnastesiSedasi_Pemantauan.Where(x => x.SectionID == item.CttSedasiAnes.SectionID && x.NoReg == item.CttSedasiAnes.NoReg).ToList();
                            foreach (var x in real_ListPemantauan)
                            {
                                var z = new_ListPemantauan.FirstOrDefault(y => y.Model.No == x.No);
                                if (z == null)
                                {
                                    eEMR.CatatanAnastesiSedasi_Pemantauan.Remove(x);
                                }
                            }
                            foreach (var x in new_ListPemantauan)
                            {

                                var _m = real_ListPemantauan.FirstOrDefault(y => y.No == x.Model.No);

                                if (_m == null)
                                {
                                    _m = new CatatanAnastesiSedasi_Pemantauan();
                                    eEMR.CatatanAnastesiSedasi_Pemantauan.Add(IConverter.Cast<CatatanAnastesiSedasi_Pemantauan>(x.Model));
                                }

                                else
                                {
                                    _m = IConverter.Cast<CatatanAnastesiSedasi_Pemantauan>(x.Model);
                                    eEMR.CatatanAnastesiSedasi_Pemantauan.AddOrUpdate(_m);
                                }

                            }

                            #endregion

                            #region ===== Detail Obat

                            if (item.CttSedasiAnes.Obat_List == null) item.CttSedasiAnes.Obat_List = new ListDetail<CatatanAnastesiSedasiObatModelDetail>();
                            item.CttSedasiAnes.Obat_List.RemoveAll(x => x.Remove);
                            foreach (var x in item.CttSedasiAnes.Obat_List)
                            {
                                x.Model.SectionID = sectionid;
                                x.Model.NoReg = item.CttSedasiAnes.NoReg;
                                x.Model.NRM = item.CttSedasiAnes.NRM;
                                //x.Model.Tanggal = DateTime.Today;
                                //x.Model.Jam = DateTime.Now;

                            }
                            var new_ListObat = item.CttSedasiAnes.Obat_List;
                            var real_ListObat = eEMR.CatatanAnastesiSedasi_Obat.Where(x => x.SectionID == item.CttSedasiAnes.SectionID && x.NoReg == item.CttSedasiAnes.NoReg).ToList();
                            foreach (var x in real_ListObat)
                            {
                                var z = new_ListObat.FirstOrDefault(y => y.Model.No == x.No);
                                if (z == null)
                                {
                                    eEMR.CatatanAnastesiSedasi_Obat.Remove(x);
                                }
                            }
                            foreach (var x in new_ListObat)
                            {
                                var _m = real_ListObat.FirstOrDefault(y => y.No == x.Model.No);

                                if (_m == null)
                                {
                                    _m = new CatatanAnastesiSedasi_Obat();
                                    eEMR.CatatanAnastesiSedasi_Obat.Add(IConverter.Cast<CatatanAnastesiSedasi_Obat>(x.Model));
                                }
                                else
                                {
                                    _m = IConverter.Cast<CatatanAnastesiSedasi_Obat>(x.Model);
                                    eEMR.CatatanAnastesiSedasi_Obat.AddOrUpdate(_m);
                                }

                            }

                            #endregion

                            #region ===== Detail Infus Perifer

                            if (item.CttSedasiAnes.InfusPerifer_List == null) item.CttSedasiAnes.InfusPerifer_List = new ListDetail<CatatanAnastesiSedasiInfusPeriModelDetail>();
                            item.CttSedasiAnes.InfusPerifer_List.RemoveAll(x => x.Remove);
                            foreach (var x in item.CttSedasiAnes.InfusPerifer_List)
                            {
                                x.Model.SectionID = sectionid;
                                x.Model.NoReg = item.CttSedasiAnes.NoReg;
                                x.Model.NRM = item.CttSedasiAnes.NRM;
                                //x.Model.Tanggal = DateTime.Today;
                                //x.Model.Jam = DateTime.Now;

                            }
                            var new_ListInfus = item.CttSedasiAnes.InfusPerifer_List;
                            var real_ListInfus = eEMR.CatatanAnastesiSedasi_InfusPerifer.Where(x => x.SectionID == item.CttSedasiAnes.SectionID && x.NoReg == item.CttSedasiAnes.NoReg).ToList();
                            foreach (var x in real_ListInfus)
                            {
                                var z = new_ListInfus.FirstOrDefault(y => y.Model.No == x.No);
                                if (z == null)
                                {
                                    eEMR.CatatanAnastesiSedasi_InfusPerifer.Remove(x);
                                }
                            }
                            foreach (var x in new_ListInfus)
                            {
                                var _m = real_ListInfus.FirstOrDefault(y => y.No == x.Model.No);

                                if (_m == null)
                                {
                                    _m = new CatatanAnastesiSedasi_InfusPerifer();
                                    eEMR.CatatanAnastesiSedasi_InfusPerifer.Add(IConverter.Cast<CatatanAnastesiSedasi_InfusPerifer>(x.Model));
                                }
                                else
                                {
                                    _m = IConverter.Cast<CatatanAnastesiSedasi_InfusPerifer>(x.Model);
                                    eEMR.CatatanAnastesiSedasi_InfusPerifer.AddOrUpdate(_m);
                                }

                            }

                            #endregion

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFCatatanAnestesiSedasi(string noreg, string sectionid)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_CatatanAnastesiSedasi.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand("Rpt_CatatanAnastesiSedasi", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables["Rpt_CatatanAnastesiSedasi;1"].SetDataSource(ds[i].Tables[0]);

                i++;
                cmd.Add(new SqlCommand($"Rpt_CatatanAnastesiSedasi_InfusPerifer", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_CatatanAnastesiSedasi_InfusPerifer;1"].SetDataSource(ds[i].Tables[0]);

                i++;
                cmd.Add(new SqlCommand($"Rpt_CatatanAnastesiSedasi_Obat", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_CatatanAnastesiSedasi_Obat;1"].SetDataSource(ds[i].Tables[0]);

                i++;
                cmd.Add(new SqlCommand($"Rpt_CatatanAnastesiSedasi_Pemantauan", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_CatatanAnastesiSedasi_Pemantauan;1"].SetDataSource(ds[i].Tables[0]);

                i++;
                cmd.Add(new SqlCommand($"Rpt_CatatanAnastesiSedasi_VitalSign", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_CatatanAnastesiSedasi_VitalSign;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}