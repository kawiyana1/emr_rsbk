﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class AsuhanKeperawatanPostOperatifController : Controller
    {
        // GET: AsuhanKeperawatanPostOperatif
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var sectionnama = Request.Cookies["SectionNamaPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.AsuhanKeperawatanPostOperatif.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.AshnPostOpera = IConverter.Cast<AsuhanKeperawatanPostOperatifViewModel>(item);

                            model.AshnPostOpera.NamaPasien = m.NamaPasien;
                            model.AshnPostOpera.JenisKelamin = m.JenisKelamin;
                            model.AshnPostOpera.Umur = m.UmurThn;
                            model.AshnPostOpera.Report = 1;
                        }
                        else
                        {
                            item = new AsuhanKeperawatanPostOperatif();
                            model.AshnPostOpera = IConverter.Cast<AsuhanKeperawatanPostOperatifViewModel>(item);
                            model.AshnPostOpera.NoReg = noreg;
                            model.AshnPostOpera.NamaPasien = m.NamaPasien;
                            model.AshnPostOpera.JenisKelamin = m.JenisKelamin;
                            model.AshnPostOpera.Umur = m.UmurThn;
                            model.AshnPostOpera.NRM = m.NRM;
                            model.AshnPostOpera.SectionID = sectionid;
                            model.AshnPostOpera.Tanggal = DateTime.Today;
                            model.AshnPostOpera.JAM_DiagnosaKeperawatan = DateTime.Now;
                            model.AshnPostOpera.JAM_DiagnosaKeperawatanBersihan = DateTime.Now;
                            model.AshnPostOpera.JAM_DiagnosaKeperawatanHipotermi = DateTime.Now;
                            model.AshnPostOpera.JAM_DiagnosaKeperawatanResiko = DateTime.Now;
                            model.AshnPostOpera.JAM_DiagnosaKeperawatanResikoGangguan = DateTime.Now;
                            model.AshnPostOpera.JAM_DiagnosaKeperawatanResikoHambatan = DateTime.Now;
                            model.AshnPostOpera.JAM_DiagnosaKeperawatanResikoKecelakaan = DateTime.Now;
                            model.AshnPostOpera.JAM_DiagnosaKeperawatanResikoKurangnya = DateTime.Now;
                            model.AshnPostOpera.JAM_Evaluasi = DateTime.Now;
                            model.AshnPostOpera.JAM_EvaluasiAspirasi = DateTime.Now;
                            model.AshnPostOpera.JAM_EvaluasiJalanNafas = DateTime.Now;
                            model.AshnPostOpera.JAM_EvaluasiJalanPasien = DateTime.Now;
                            model.AshnPostOpera.JAM_EvaluasiJalanPasienAman = DateTime.Now;
                            model.AshnPostOpera.JAM_EvaluasiJalanPasienMenyatakanKakinya = DateTime.Now;
                            model.AshnPostOpera.JAM_EvaluasiJalanPasienMenyatakanTdkPusing = DateTime.Now;
                            model.AshnPostOpera.JAM_EvaluasiJalanPerasaanPasien = DateTime.Now;
                            model.AshnPostOpera.JAM_PerencanaanImplementasi = DateTime.Now;
                            model.AshnPostOpera.JAM_PerencanaanImplementasi1 = DateTime.Now;
                            model.AshnPostOpera.JAM_PerencanaanImplementasi2 = DateTime.Now;
                            model.AshnPostOpera.JAM_PerencanaanImplementasi3 = DateTime.Now;
                            model.AshnPostOpera.JAM_PerencanaanImplementasi4 = DateTime.Now;
                            model.AshnPostOpera.JAM_PerencanaanImplementasi5 = DateTime.Now;
                            model.AshnPostOpera.JAM_PerencanaanImplementasi6 = DateTime.Now;
                            model.AshnPostOpera.JAM_PerencanaanImplementasi7 = DateTime.Now;
                            model.AshnPostOpera.Report = 0;

                        }

                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string AsuhanKeperawatanPostOperatif_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == item.AshnPostOpera.NoReg);
                            item.AshnPostOpera.NoReg = item.AshnPostOpera.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.AshnPostOpera.SectionID = sectionid;
                            item.AshnPostOpera.NRM = item.AshnPostOpera.NRM;
                            var model = eEMR.AsuhanKeperawatanPostOperatif.FirstOrDefault(x => x.SectionID == item.AshnPostOpera.SectionID && x.NoReg == item.AshnPostOpera.NoReg);

                            if (model == null)
                            {
                                model = new AsuhanKeperawatanPostOperatif();
                                var o = IConverter.Cast<AsuhanKeperawatanPostOperatif>(item.AshnPostOpera);
                                eEMR.AsuhanKeperawatanPostOperatif.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<AsuhanKeperawatanPostOperatif>(item.AshnPostOpera);
                                eEMR.AsuhanKeperawatanPostOperatif.AddOrUpdate(model);
                            }



                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFAsuhanKeperawatanPostOperatif(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_AsuhanKeperawatanPostOperatif.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"Rpt_AsuhanKeperawatanPostOperatif", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_AsuhanKeperawatanPostOperatif;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}