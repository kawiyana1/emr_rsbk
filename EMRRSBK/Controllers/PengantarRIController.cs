﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
namespace EMRRSBK.Controllers
{
    public class PengantarRIController : Controller
    {
        // GET: PengantarRI
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    model.NoReg = noreg;
                    model.SectionID = sectionid;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.PengantarRawatInap.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.PengantarRawatInap = IConverter.Cast<PengantarRIViewModel>(item);
                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Perawat);
                            if (dokter != null)
                            {
                                model.PengantarRawatInap.PerawatNama = dokter.NamaDOkter;
                            }
                            model.PengantarRawatInap.Pasien = m.NamaPasien;

                            var fingerperawat = eEMR.PengantarRawatInap.Where(x => x.Perawat == model.PengantarRawatInap.Perawat && x.TTDpetugas == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerperawat != null)
                            {
                                model.PengantarRawatInap.SudahRegPegawai = 1;
                            }
                            else
                            {
                                model.PengantarRawatInap.SudahRegPegawai = 0;
                            }

                            var fingerpasien = eEMR.PengantarRawatInap.Where(x => x.NRM == model.PengantarRawatInap.NRM && x.TTDPasien == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerpasien != null)
                            {
                                model.PengantarRawatInap.SudahRegPasien = 1;
                            }
                            else
                            {
                                model.PengantarRawatInap.SudahRegPasien = 0;
                            }

                            if (model.PengantarRawatInap.Perawat != null)
                            {
                                ViewBag.UrlFingerPengantarRawatInapDokterPemeriksa = GetEncodeUrlPengantarRI(model.PengantarRawatInap.Perawat, model.PengantarRawatInap.NoReg, sectionid);
                            }

                            ViewBag.UrlFingerPengantarRawatInapPasien = GetEncodeUrlPengantarRI(model.PengantarRawatInap.NRM, model.PengantarRawatInap.NoReg, sectionid);

                        }
                        else
                        {
                            item = new PengantarRawatInap();
                            model.PengantarRawatInap = IConverter.Cast<PengantarRIViewModel>(item);
                            ViewBag.UrlFingerPengantarRawatInapDokterPemeriksa = GetEncodeUrlPengantarRI(model.PengantarRawatInap.Perawat, model.PengantarRawatInap.NoReg, sectionid);
                            ViewBag.UrlFingerPengantarRawatInapPasien = GetEncodeUrlPengantarRI(model.PengantarRawatInap.NRM, model.PengantarRawatInap.NoReg, sectionid);
                            model.PengantarRawatInap.Tanggal = DateTime.Now;
                            model.PengantarRawatInap.Tanggal = DateTime.Now;
                            model.PengantarRawatInap.TglMRS = DateTime.Now;
                            model.PengantarRawatInap.TglTindakan = DateTime.Now;
                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string PengantarRawatInap_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.PengantarRawatInap.NoReg = noreg;
                            item.PengantarRawatInap.SectionID = Request.Cookies["SectionIDPelayanan"].Value; ;
                            item.PengantarRawatInap.NRM = m.NRM;
                            item.PengantarRawatInap.Tanggal = DateTime.Now;
                            var model = eEMR.PengantarRawatInap.FirstOrDefault(x => x.SectionID == item.PengantarRawatInap.SectionID && x.NoReg == item.NoReg);

                            if (model == null)
                            {
                                model = new PengantarRawatInap();
                                var o = IConverter.Cast<PengantarRawatInap>(item.PengantarRawatInap);
                                eEMR.PengantarRawatInap.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<PengantarRawatInap>(item.PengantarRawatInap);
                                eEMR.PengantarRawatInap.AddOrUpdate(model);
                            }

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDF(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["SectionNamaPelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"PengantarRawatInap_IGD.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"PengantarRawatInap_IGD", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"PengantarRawatInap_IGD;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }

        public string GetEncodeUrlPengantarRI(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifPengatarRawatInap/get_keyskdpjp";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }
    }
}