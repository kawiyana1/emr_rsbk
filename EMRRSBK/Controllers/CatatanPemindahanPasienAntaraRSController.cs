﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class CatatanPemindahanPasienAntaraRSController : Controller
    {
        // GET: CatatanPemindahanPasienAntaraRS
        
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    model.SectionID = sectionid;
                    model.NoReg = m.NoReg;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.CatatanPemindahanPasienAntarRumahSakit.FirstOrDefault(x => x.NoReg == noreg);
                        if (item != null)
                        {
                            model.CPPARS = IConverter.Cast<CatatanPemindahanPasienAntarRSViewModelDetail>(item);
                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.DokterMerawat);
                            if (dokter != null)
                            {
                                model.CPPARS.DokterMerawatNama = dokter.NamaDOkter;
                            }

                            var dokter1 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.NamaDPJP);
                            if (dokter1 != null)
                            {
                                model.CPPARS.NamaDPJPNama = dokter1.NamaDOkter;
                            }

                            var dokter2 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.NamaPerawat1);
                            if (dokter2 != null)
                            {
                                model.CPPARS.NamaPerawat1Nama = dokter2.NamaDOkter;
                            }

                            var dokter3 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.NamaPerawat2);
                            if (dokter3 != null)
                            {
                                model.CPPARS.NamaPerawat2Nama = dokter3.NamaDOkter;
                            }
                            model.CPPARS.InfusTanggalPemasangan = DateTime.Today;
                            model.CPPARS.PemindahanPasien_Tanggal = DateTime.Today;
                            model.CPPARS.ProcedurPembedahan_Tanggal = DateTime.Today;
                            
                        }
                        else
                        {
                            item = new CatatanPemindahanPasienAntarRumahSakit();
                            model.CPPARS = IConverter.Cast<CatatanPemindahanPasienAntarRSViewModelDetail>(item);
                            model.CPPARS.InfusTanggalPemasangan = DateTime.Today;
                            model.CPPARS.PemindahanPasien_Tanggal = DateTime.Today;
                            model.CPPARS.ProcedurPembedahan_Tanggal = DateTime.Today;
                            model.CPPARS.PemindahanPasien_Jam = DateTime.Now;
                            model.CPPARS.ObservasiPukul = DateTime.Now;
                            model.CPPARS.InfusTanggalPemasangan = DateTime.Today;
                            model.CPPARS.JamKondisiPasien = DateTime.Now;
                            model.CPPARS.TglPemasangan = DateTime.Today;

                        }

                        var List = eEMR.Recomendasi_Detail.Where(x => x.NoReg == noreg).ToList();
                        if (List != null)
                        {

                            model.CPPARS.Rekom_List = new ListDetail<RecomendasiViewModelDetail>();

                            foreach (var x in List)
                            {
                                var y = IConverter.Cast<RecomendasiViewModelDetail>(x);
                                model.CPPARS.Rekom_List.Add(false, y);
                            }

                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string CatatanPemindahanPasienAntarRS_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.CPPARS.NoReg = noreg;
                            item.CPPARS.SectionID = sectionid;
                            item.CPPARS.NRM = item.NRM;
                            item.CPPARS.InfusTanggalPemasangan = DateTime.Now;
                            item.CPPARS.PemindahanPasien_Tanggal = DateTime.Now;
                            item.CPPARS.ProcedurPembedahan_Tanggal = DateTime.Now;
                            var model = eEMR.CatatanPemindahanPasienAntarRumahSakit.FirstOrDefault(x => x.NoReg == item.NoReg);

                            if (model == null)
                            {
                                model = new CatatanPemindahanPasienAntarRumahSakit();
                                var o = IConverter.Cast<CatatanPemindahanPasienAntarRumahSakit>(item.CPPARS);
                                eEMR.CatatanPemindahanPasienAntarRumahSakit.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<CatatanPemindahanPasienAntarRumahSakit>(item.CPPARS);
                                eEMR.CatatanPemindahanPasienAntarRumahSakit.AddOrUpdate(model);
                            }

                            #region ===== DETAIL

                            if (item.CPPARS.Rekom_List == null) item.CPPARS.Rekom_List = new ListDetail<RecomendasiViewModelDetail>();
                            item.CPPARS.Rekom_List.RemoveAll(x => x.Remove);


                            foreach (var x in item.CPPARS.Rekom_List)
                            {
                                x.Model.SectionID = sectionid;
                                x.Model.NoReg = noreg;
                                x.Model.NRM = item.NRM;
                                //x.Model.Username = User.Identity.GetUserName();

                            }
                            var new_List = item.CPPARS.Rekom_List;
                            var real_List = eEMR.Recomendasi_Detail.Where(x => x.NoReg == noreg).ToList();
                            // delete | delete where (real_list not_in new_list)
                            foreach (var x in real_List)
                            {
                                var z = new_List.FirstOrDefault(y => y.Model.No == x.No);
                                if (z == null)
                                {
                                    eEMR.Recomendasi_Detail.Remove(x);
                                }

                            }

                            foreach (var x in new_List)
                            {

                                var _m = real_List.FirstOrDefault(y => y.No == x.Model.No);

                                if (_m == null)
                                {

                                    _m = new Recomendasi_Detail();
                                    eEMR.Recomendasi_Detail.Add(IConverter.Cast<Recomendasi_Detail>(x.Model));
                                }
                                // edit | where (new_list in raal_list)
                                else
                                {
                                    //update
                                    //_m.NoReg = x.Model.NoReg;
                                    //_m.NRM = x.Model.NRM;
                                    //_m.SectionID = x.Model.SectionID;
                                    //_m.Tanggal = x.Model.Tanggal;
                                    //_m.HB = x.Model.HB;
                                    //_m.Trombosit = x.Model.Trombosit;
                                    //eEMR.SaveChanges();
                                    _m = IConverter.Cast<Recomendasi_Detail>(x.Model);
                                    eEMR.Recomendasi_Detail.AddOrUpdate(_m);


                                }

                            }

                            #endregion


                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFCatatanPemindahanPasienAntaraRS(string noreg, string sectionid)
        {
            var tipepelayanan = Request.Cookies["SectionNamaPelayanan"].Value;
            var rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"CatatanPemindahanPasien_AntarRumahSakit.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand("CatatanPemindahanPasien_AntarRumahSakit", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables["CatatanPemindahanPasien_AntarRumahSakit;1"].SetDataSource(ds[i].Tables[0]);

                i++;
                cmd.Add(new SqlCommand($"RecomendasiDetail", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"RecomendasiDetail;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }

    }
}