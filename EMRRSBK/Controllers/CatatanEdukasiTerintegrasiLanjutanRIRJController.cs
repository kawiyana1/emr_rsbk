﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class CatatanEdukasiTerintegrasiLanjutanRIRJController : Controller
    {
        // GET: CatatanEdukasiTerintegrasiLanjutanRIRJ
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new GeneralPoliMataViewModel();

            try
            {

                using (var s = new SIM_Entities())
                {
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var m = s.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    model.SectionID = sectionid;
                }

                model.SectionID = sectionid;
                model.NoReg = noreg;
                model.Tanggal_View = DateTime.Today;

                using (var s = new EMR_Entities())
                {
                    using (var eSIM = new SIM_Entities())
                    {

                        //DETAIL 1
                        var List = s.CatatanEdukasiTerintegrasiLanjutanRIdanRJ.Where(x => x.NoReg == model.NoReg).ToList();
                        if (List != null)
                        {
                            model.CETLRIRJ_List = new ListDetail<CatatanEdukasiTerintegrasiLanjutanRIRJViewModel>();

                            foreach (var x in List)
                            {
                                var y = IConverter.Cast<CatatanEdukasiTerintegrasiLanjutanRIRJViewModel>(x);
                                var cek = s.mFinger.Where(xx => xx.userid == x.NamaEducator).FirstOrDefault();
                                if (cek != null)
                                {
                                    if (y.TandaTanganEdukator == Convert.ToBase64String(cek.Gambar1) && y.NamaEducator == cek.userid)
                                    {
                                        y.SudahRegEdukasi = 0;
                                    }
                                    else
                                    {
                                        y.SudahRegEdukasi = 1;
                                    }
                                }
                                else
                                {
                                    y.SudahRegEdukasi = 1;
                                }

                                if (y.NamaEducator != null)
                                {
                                    ViewBag.UrlFingerCatatanEdukasi = GetEncodeUrlEdukasi(y.NamaEducator, y.NoReg, sectionid, y.No);
                                    y.Viewbag = ViewBag.UrlFingerCatatanEdukasi;
                                }
                                //y.Tanggal = DateTime.Now; 
                                var petugas = eSIM.mDokter.FirstOrDefault(d => d.DokterID == x.NamaEducator);
                                if (petugas != null)
                                {
                                    y.NamaEducatorNama = petugas.NamaDOkter;
                                }
                                model.CETLRIRJ_List.Add(false, y);

                            }

                        }
                    }
                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("CatatanEdukasiTerintegrasiLanjutanRIRJ")]
        public string CatatanEdukasiTerintegrasiLanjutanRIRJ_Post()
        {
            try
            {
                var item = new GeneralPoliMataViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            #region ==== D E T A I L
                            if (item.CETLRIRJ_List == null) item.CETLRIRJ_List = new ListDetail<CatatanEdukasiTerintegrasiLanjutanRIRJViewModel>();
                            item.CETLRIRJ_List.RemoveAll(x => x.Remove);

                            foreach (var x in item.CETLRIRJ_List)
                            {
                                x.Model.Username = User.Identity.GetUserName();
                                x.Model.SectionID = sectionid;
                                x.Model.NoReg = item.NoReg;
                                x.Model.NRM = item.NRM;
                                x.Model.Tanggal = DateTime.Now;
                            }

                            var new_List = item.CETLRIRJ_List;
                            var real_List = eEMR.CatatanEdukasiTerintegrasiLanjutanRIdanRJ.Where(x => x.SectionID == sectionid && x.NRM == item.NRM).ToList();
                            // delete | delete where (real_list not_in new_list)
                            foreach (var x in real_List)
                            {
                                var m = new_List.FirstOrDefault(y => y.Model.No == x.No);
                                if (m == null)
                                {
                                    eEMR.CatatanEdukasiTerintegrasiLanjutanRIdanRJ.Remove(x);
                                }

                            }


                            foreach (var x in new_List)
                            {

                                var _m = real_List.FirstOrDefault(y => y.No == x.Model.No);
                                //new DataColumn("Tanggal", typeof(DateTime));
                                //new DataColumn("Jam", typeof(TimeSpan));
                                // add | add where (new_list not_in raal_list)
                                if (_m == null)
                                {

                                    _m = new CatatanEdukasiTerintegrasiLanjutanRIdanRJ();
                                    eEMR.CatatanEdukasiTerintegrasiLanjutanRIdanRJ.Add(IConverter.Cast<CatatanEdukasiTerintegrasiLanjutanRIdanRJ>(x.Model));
                                    eEMR.SaveChanges();
                                }
                                else
                                {

                                    _m = IConverter.Cast<CatatanEdukasiTerintegrasiLanjutanRIdanRJ>(x.Model);
                                    eEMR.CatatanEdukasiTerintegrasiLanjutanRIdanRJ.AddOrUpdate(_m);
                                    eEMR.SaveChanges();
                                }

                            }
                            #endregion

                            //var nomorreport = eEMR.CatatanEdukasiTerintegrasiLanjutanRIdanRJ.Where(y => y.NRM == item.NRM).ToList();
                            //if (nomorreport != null)
                            //{
                            //    var ii = 1;
                            //    var zz = 1;
                            //    foreach (var x in nomorreport)
                            //    {
                            //        if (ii == 6)
                            //        {
                            //            ii = 1;
                            //            ++zz;
                            //        }
                            //        x.NomorReport = ii;
                            //        x.NomorReport_Group = zz;
                            //        ++ii;
                            //    }
                            //}

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFCatatanEdukasiTerintegrasiLanjutanRIRJ(string noreg)
        {
            var tipepelayanan = Request.Cookies["SectionNamaPelayanan"].Value;
            var rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_CatatanEdukasiTerintegrasiLanjutanRIdanRJ.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand($"Rpt_CatatanEdukasiTerintegrasiLanjutanRIdanRJ", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_CatatanEdukasiTerintegrasiLanjutanRIdanRJ;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                //cmd.Add(new SqlCommand("ImplementasiRisikoJatuhAnak", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NRM", nrm));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["ImplementasiRisikoJatuhAnak;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }

        #region ===== Cek Verif Detail Diagnosa List
        [HttpGet]
        public string CheckBerhasilCatatanLanjutanTerintegrasi(string noreg, string section, string id, int no)
        {
            var result = new ReturnFingerViewModel();
            section = Request.Cookies["SectionIDPelayanan"].Value;
            using (var s = new EMR_Entities())
            {
                var finger = s.mFinger.Where(x => x.userid == id).FirstOrDefault();
                if (finger != null)
                {
                    var m = s.CatatanEdukasiTerintegrasiLanjutanRIdanRJ.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section && x.No == no);

                    if (m != null)
                    {
                        if (m.TandaTanganEdukator != null)
                        {
                            result.Dokter = "1";
                        }
                        else
                        {
                            result.Dokter = "0";
                        }
                    }
                }
                else
                {
                    result.Dokter = "0";
                }
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result
            });
        }

        public string GetEncodeUrlEdukasi(string id, string noreg, string section, int no)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCatatanEdukasiTerintegrasiLanjutanRIRJ/get_keyEdukasi";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section + "&no=" + no);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;
            }
            //var cek = System.Convert.FromBase64String(url);
            //var cek2 = Encoding.UTF8.GetString(cek);

        }
        #endregion

        // GET: History
        #region ===== T A B L E  H I S T O R Y

        [HttpPost]
        public string ListHistoryCatatanEdukasiTerintegrasiRIRJ(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMR_Entities())
                    {
                        IQueryable<CatatanEdukasiTerintegrasiLanjutanRIdanRJ> p = s.CatatanEdukasiTerintegrasiLanjutanRIdanRJ;
                        p = p.Where($"NRM.Contains(@0)", filter[1]);
                        var totalcount = p.Count();
                        var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}").Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                        result = new ResultSS(models.Length, null, totalcount, pageIndex);
                        var datas = new List<CatatanEdukasiTerintegrasiLanjutanRIRJViewModel>();
                        foreach (var x in models.ToList())
                        {
                            var dokter = sim.mDokter.Where(xx => xx.DokterID == x.NamaEducator).FirstOrDefault();
                            var m = IConverter.Cast<CatatanEdukasiTerintegrasiLanjutanRIRJViewModel>(x);
                            m.Tanggal_View = x.Tanggal == null ? "-" : x.Tanggal.Value.ToString("yyyy/MM/dd");
                            //m.Jam_View = x.Jam == null ? "" : x.Jam.Value.ToString("hh':'mm");
                            if (dokter == null)
                            {
                                m.NamaEducatorNama = "";
                            }
                            else
                            {
                                m.NamaEducatorNama = dokter.NamaDOkter;
                            }
                            datas.Add(m);
                        }
                        result.Data = datas;
                    }
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion
    }
}