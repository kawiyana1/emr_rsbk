﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class EMRRingkasanRiwayatMasukDanKeluarController : Controller
    {
        // GET: EMRRingkasanRiwayatMasukDanKeluar
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string noreg, string nrm, int view = 0)
        {
            var model = new EMRRingkasanRiwayatMasukDanKeluarViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var dokumen = s.RingkasanRiwayatMasukDanKeluar.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRRingkasanRiwayatMasukDanKeluarViewModel>(dokumen);
                        var pemeberiKet = sim.mDokter.FirstOrDefault(x => x.DokterID == model.KodePemberiKeterangan);
                        if (pemeberiKet != null)
                        {
                            model.KodePemberiKeteranganNama = pemeberiKet.NamaDOkter;
                        }

                        model.MODEVIEW = view;
                    }
                    else
                    {
                       
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                    }
                    model.NoReg = noreg;
                    model.SectionID = sectionid;
                    model.NRM = nrm;

                    var dokumen_temp = s.trTemplate.Where(e => e.DokumenID == "Ringkasan Riwayat Masuk Keluar").ToList();
                    ViewBag.DokumenID = "Ringkasan Riwayat Masuk Keluar";
                    ViewBag.NamaDokumen = "Ringkasan Riwayat Masuk Keluar";
                    model.ListTemplate = new List<SelectItemLisRngksnRwytMasukKeluar>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemLisRngksnRwytMasukKeluar()
                        {
                            Value = list.NoReg,
                            Text = list.NamaTemplate
                        });
                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string noreg, string copy, string nrm)
        {
            var model = new EMRRingkasanRiwayatMasukDanKeluarViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var dokumen = s.RingkasanRiwayatMasukDanKeluar.FirstOrDefault(x => x.NoReg == copy);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRRingkasanRiwayatMasukDanKeluarViewModel>(dokumen);
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                        model.NRM = nrm;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;

                        var pemeberiKet = sim.mDokter.FirstOrDefault(x => x.DokterID == model.KodePemberiKeterangan);
                        if (pemeberiKet != null)
                        {
                            model.KodePemberiKeteranganNama = pemeberiKet.NamaDOkter;
                        }

                        var dpjp = sim.mDokter.FirstOrDefault(x => x.DokterID == model.KodeDokterDPJP);
                        if (dpjp != null)
                        {
                            model.KodeDokterDPJPNama = dpjp.NamaDOkter;
                        }

                        #region === Detail Operasi
                        model.Operasi_List = new ListDetail<RingkasanRiwayatMasukDanKeluar_OperasiViewModelDetail>();
                        var detail_1 = s.RingkasanRiwayatMasukDanKeluar_Operasi.Where(x => x.NoReg == noreg).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<RingkasanRiwayatMasukDanKeluar_OperasiViewModelDetail>(x);
                            //var dokterdetail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Dokter);
                            //if (dokterdetail != null)
                            //{
                            //    y.DokterNama = dokterdetail.NamaDOkter;
                            //}
                            model.Operasi_List.Add(false, y);
                        }
                        #endregion

                        #region === Detail Operasi
                        model.Sekunder_List = new ListDetail<RingkasanRiwayatMasukDanKeluar_SekunderViewModelDetail>();
                        var detail_2 = s.RingkasanRiwayatMasukDanKeluar_Sekunder.Where(x => x.NoReg == noreg).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<RingkasanRiwayatMasukDanKeluar_SekunderViewModelDetail>(x);
                            //var dokterdetail = sim.mDokter.FirstOrDefault(e => e.DokterID == y.Dokter);
                            //if (dokterdetail != null)
                            //{
                            //    y.DokterNama = dokterdetail.NamaDOkter;
                            //}
                            model.Sekunder_List.Add(false, y);
                        }
                        #endregion
                    }
                    else
                    {
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                        model.NRM = nrm;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                    }
                    model.MODEVIEW = 0;
                    var dokumen_temp = s.trTemplate.Where(e => e.DokumenID == "Ringkasan Riwayat Masuk Keluar").ToList();
                    ViewBag.DokumenID = "Ringkasan Riwayat Masuk Keluar";
                    ViewBag.NamaDokumen = "Ringkasan Riwayat Masuk Keluar";
                    model.ListTemplate = new List<SelectItemLisRngksnRwytMasukKeluar>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemLisRngksnRwytMasukKeluar()
                        {
                            Value = list.NoReg,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {

            if (ModelState.IsValid)
            {
                var req = Request.Form[""];
            }
            try
            {
                var item = new EMRRingkasanRiwayatMasukDanKeluarViewModel();

                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var model = s.RingkasanRiwayatMasukDanKeluar.FirstOrDefault(x => x.NoReg == item.NoReg);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<RingkasanRiwayatMasukDanKeluar>(item);
                        s.RingkasanRiwayatMasukDanKeluar.Add(o);
                        activity = "Create Ringkasan Riwayat Masuk Keluar";
                    }
                    else
                    {
                        model = IConverter.Cast<RingkasanRiwayatMasukDanKeluar>(item);
                        s.RingkasanRiwayatMasukDanKeluar.AddOrUpdate(model);
                        activity = "Update Ringkasan Riwayat Masuk Keluar";
                    }

                    if (item.save_template == true && item.nama_template != null)
                    {
                        var temp = new trTemplate();
                        temp.NoReg = item.NoReg;
                        temp.NamaTemplate = item.nama_template;
                        temp.DokterID = item.KodeDokterDPJP;
                        temp.DokumenID = "Ringkasan Riwayat Masuk Keluar";
                        temp.SectionID = sectionid;
                        s.trTemplate.Add(temp);
                    }

                    #region === Detail Operasi
                    if (item.Operasi_List == null) item.Operasi_List = new ListDetail<RingkasanRiwayatMasukDanKeluar_OperasiViewModelDetail>();
                    item.Operasi_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Operasi_List)
                    {
                        x.Model.NoReg = item.NoReg;
                        x.Model.NRM = item.NRM;
                        x.Model.SectionID = item.SectionID;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.Operasi_List;
                    var real_list = s.RingkasanRiwayatMasukDanKeluar_Operasi.Where(x => x.NoReg == item.NoReg).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.RingkasanRiwayatMasukDanKeluar_Operasi.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.RingkasanRiwayatMasukDanKeluar_Operasi.Add(IConverter.Cast<RingkasanRiwayatMasukDanKeluar_Operasi>(x.Model));
                        }
                        else
                        {
                            _m.No = x.Model.No;
                            _m.Tanggal = x.Model.Tanggal;
                            _m.Jam = x.Model.Jam;
                            _m.TglOperasi = x.Model.TglOperasi;
                            _m.Operasi = x.Model.Operasi;
                            _m.Jenis = x.Model.Jenis;
                            _m.ICD = x.Model.ICD;
                        }
                    }
                    #endregion

                    #region === Detail Sekunder
                    if (item.Sekunder_List == null) item.Sekunder_List = new ListDetail<RingkasanRiwayatMasukDanKeluar_SekunderViewModelDetail>();
                    item.Sekunder_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Sekunder_List)
                    {
                        x.Model.NoReg = item.NoReg;
                        x.Model.NRM = item.NRM;
                        x.Model.SectionID = item.SectionID;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list1 = item.Sekunder_List;
                    var real_list1 = s.RingkasanRiwayatMasukDanKeluar_Sekunder.Where(x => x.NoReg == item.NoReg).ToList();
                    foreach (var x in real_list1)
                    {
                        var m = new_list1.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.RingkasanRiwayatMasukDanKeluar_Sekunder.Remove(x);
                    }

                    foreach (var x in new_list1)
                    {
                        var _m = real_list1.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.RingkasanRiwayatMasukDanKeluar_Sekunder.Add(IConverter.Cast<RingkasanRiwayatMasukDanKeluar_Sekunder>(x.Model));
                        }
                        else
                        {
                            _m.No = x.Model.No;
                            _m.Tanggal = x.Model.Tanggal;
                            _m.Jam = x.Model.Jam;
                            _m.Sekunder = x.Model.Sekunder;
                            _m.ICD = x.Model.ICD;
                        }
                    }
                    #endregion

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoReg}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}