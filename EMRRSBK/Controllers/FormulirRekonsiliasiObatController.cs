﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class FormulirRekonsiliasiObatController : Controller
    {
        // GET: FormulirRekonsiliasiObat
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.FormulirRekonsiliasiObat.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        var getkamar = eSIM.VW_DataPasienReg.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.FRekonsiliasiObat = IConverter.Cast<FormulirRekonsiliasiObatViewModel>(item);
                            model.FRekonsiliasiObat.Report = 1;
                        }
                        else
                        {
                            item = new FormulirRekonsiliasiObat();
                            model.FRekonsiliasiObat = IConverter.Cast<FormulirRekonsiliasiObatViewModel>(item);
                            model.FRekonsiliasiObat.NoReg = noreg;
                            model.FRekonsiliasiObat.NRM = model.NRM;
                            model.FRekonsiliasiObat.SectionID = sectionid;
                            model.FRekonsiliasiObat.Tanggal = DateTime.Today;
                            model.FRekonsiliasiObat.Report = 0;
                            //model.FRekonsiliasiObat.Ruangan = Request.Cookies["SectionNamaPelayanan"].Value;
                            //model.FRekonsiliasiObat.Kamar = (getkamar == null ? "-" : getkamar.Kamar);
                        }

                        var List = eEMR.FormulirRekonsiliasiObat_AlergiObat.Where(x => x.SectionID == sectionid && x.NoReg == noreg).ToList();
                        if (List != null)
                        {

                            model.FRekonsiliasiObat.Alergi_List = new ListDetail<FormulirRekonsiliasiObat_AlergiObatModelDetail>();

                            foreach (var x in List)
                            {
                                var y = IConverter.Cast<FormulirRekonsiliasiObat_AlergiObatModelDetail>(x);
                                var obat = eSIM.mBarang.FirstOrDefault(z => z.Kode_Barang.ToString() == y.KodeObat);
                                if (obat != null)
                                {
                                    y.KodeObatNama = obat.Nama_Barang;
                                }

                                //var paraf = eSIM.mDokter.FirstOrDefault(z => z.DokterID.ToString() == y.Paraf);
                                //if (paraf != null)
                                //{
                                //    y.ParafNama = paraf.NamaDOkter;
                                //}
                                model.FRekonsiliasiObat.Alergi_List.Add(false, y);
                            }

                        }

                        var List2 = eEMR.FormulirRekonsiliasiObat_ObatDigunakan.Where(x => x.SectionID == sectionid && x.NoReg == noreg).ToList();
                        if (List2 != null)
                        {

                            model.FRekonsiliasiObat.Obat_List = new ListDetail<FormulirRekonsiliasiObat_ObatDigunakanModelDetail>();

                            foreach (var x in List)
                            {
                                var y = IConverter.Cast<FormulirRekonsiliasiObat_ObatDigunakanModelDetail>(x);
                                var obat = eSIM.mBarang.FirstOrDefault(z => z.Kode_Barang.ToString() == y.KodeObat);
                                if (obat != null)
                                {
                                    y.KodeObatNama = obat.Nama_Barang;
                                }

                                //var paraf = eSIM.mDokter.FirstOrDefault(z => z.DokterID.ToString() == y.Paraf);
                                //if (paraf != null)
                                //{
                                //    y.ParafNama = paraf.NamaDOkter;
                                //}
                                model.FRekonsiliasiObat.Obat_List.Add(false, y);
                            }

                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string FormulirRonsiliasiObat_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.FRekonsiliasiObat.NoReg = item.FRekonsiliasiObat.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.FRekonsiliasiObat.SectionID = sectionid;
                            item.FRekonsiliasiObat.NRM = item.FRekonsiliasiObat.NRM;
                            var model = eEMR.FormulirRekonsiliasiObat.FirstOrDefault(x => x.SectionID == sectionid && x.NoReg == item.FRekonsiliasiObat.NoReg);

                            if (model == null)
                            {
                                model = new FormulirRekonsiliasiObat();
                                var o = IConverter.Cast<FormulirRekonsiliasiObat>(item.FRekonsiliasiObat);
                                eEMR.FormulirRekonsiliasiObat.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<FormulirRekonsiliasiObat>(item.FRekonsiliasiObat);
                                eEMR.FormulirRekonsiliasiObat.AddOrUpdate(model);
                            }

                            #region ===== DETAIL ALERGI OBAT

                            if (item.FRekonsiliasiObat.Alergi_List == null) item.FRekonsiliasiObat.Alergi_List = new ListDetail<FormulirRekonsiliasiObat_AlergiObatModelDetail>();
                            item.FRekonsiliasiObat.Alergi_List.RemoveAll(x => x.Remove);
                            foreach (var x in item.FRekonsiliasiObat.Alergi_List)
                            {
                                x.Model.Username = User.Identity.GetUserName();
                                x.Model.SectionID = sectionid;
                                x.Model.NoReg = item.FRekonsiliasiObat.NoReg;
                                x.Model.NRM = item.FRekonsiliasiObat.NRM;

                            }
                            var new_List = item.FRekonsiliasiObat.Alergi_List;
                            var real_List = eEMR.FormulirRekonsiliasiObat_AlergiObat.Where(x => x.SectionID == sectionid && x.NoReg == item.FRekonsiliasiObat.NoReg).ToList();
                            foreach (var x in real_List)
                            {
                                var z = new_List.FirstOrDefault(y => y.Model.No == x.No);
                                if (z == null)
                                {
                                    eEMR.FormulirRekonsiliasiObat_AlergiObat.Remove(x);
                                }

                            }
                            foreach (var x in new_List)
                            {

                                var _m = real_List.FirstOrDefault(y => y.No == x.Model.No);

                                if (_m == null)
                                {

                                    _m = new FormulirRekonsiliasiObat_AlergiObat();
                                    eEMR.FormulirRekonsiliasiObat_AlergiObat.Add(IConverter.Cast<FormulirRekonsiliasiObat_AlergiObat>(x.Model));
                                }

                                else
                                {

                                    _m = IConverter.Cast<FormulirRekonsiliasiObat_AlergiObat>(x.Model);
                                    eEMR.FormulirRekonsiliasiObat_AlergiObat.AddOrUpdate(_m);


                                }

                            }

                            #endregion

                            #region ===== DETAIL OBAT

                            if (item.FRekonsiliasiObat.Obat_List == null) item.FRekonsiliasiObat.Obat_List = new ListDetail<FormulirRekonsiliasiObat_ObatDigunakanModelDetail>();
                            item.FRekonsiliasiObat.Obat_List.RemoveAll(x => x.Remove);
                            foreach (var x in item.FRekonsiliasiObat.Obat_List)
                            {
                                x.Model.Username = User.Identity.GetUserName();
                                x.Model.SectionID = sectionid;
                                x.Model.NoReg = item.FRekonsiliasiObat.NoReg;
                                x.Model.NRM = item.FRekonsiliasiObat.NRM;

                            }
                            var new_List1 = item.FRekonsiliasiObat.Obat_List;
                            var real_List1 = eEMR.FormulirRekonsiliasiObat_ObatDigunakan.Where(x => x.SectionID == sectionid && x.NoReg == item.FRekonsiliasiObat.NoReg).ToList();
                            foreach (var x in real_List1)
                            {
                                var z = new_List1.FirstOrDefault(y => y.Model.No == x.No);
                                if (z == null)
                                {
                                    eEMR.FormulirRekonsiliasiObat_ObatDigunakan.Remove(x);
                                }

                            }
                            foreach (var x in new_List1)
                            {

                                var _m = real_List1.FirstOrDefault(y => y.No == x.Model.No);

                                if (_m == null)
                                {

                                    _m = new FormulirRekonsiliasiObat_ObatDigunakan();
                                    eEMR.FormulirRekonsiliasiObat_ObatDigunakan.Add(IConverter.Cast<FormulirRekonsiliasiObat_ObatDigunakan>(x.Model));
                                }

                                else
                                {

                                    _m = IConverter.Cast<FormulirRekonsiliasiObat_ObatDigunakan>(x.Model);
                                    eEMR.FormulirRekonsiliasiObat_ObatDigunakan.AddOrUpdate(_m);


                                }

                            }

                            #endregion

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFRekonsiliatiObat(string noreg, string sectionid)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_RekonsiliasiObat.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand("Rpt_RekonsiliasiObat", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables["Rpt_RekonsiliasiObat;1"].SetDataSource(ds[i].Tables[0]);

                i++;
                cmd.Add(new SqlCommand($"Rpt_RekonsiliasiObat_AlergiObat", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_RekonsiliasiObat_AlergiObat;1"].SetDataSource(ds[i].Tables[0]);

                i++;
                cmd.Add(new SqlCommand($"Rpt_RekonsiliasiObat_ObatDigunakan", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_RekonsiliasiObat_ObatDigunakan;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}