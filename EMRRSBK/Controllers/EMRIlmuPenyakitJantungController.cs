﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class EMRIlmuPenyakitJantungController : Controller
    {
        // GET: EMRIlmuPenyakitJantung
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string noreg, string nrm, int view = 0)
        {
            var model = new EMRIlmuPenyakitJantungViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var dokumen = s.IlmuPenyakitJantung.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRIlmuPenyakitJantungViewModel>(dokumen);
                        var dktpolimata = sim.mDokter.FirstOrDefault(x => x.DokterID == model.DokterBPJP);
                        if (dktpolimata != null)
                        {
                            model.DokterBPJPNama = dktpolimata.NamaDOkter;
                        }

                        #region === Detail 
                        model.IPJntung_List = new ListDetail<AssesmentIlmuPenyakitJantungModelDetail>();
                        var detail_2 = s.IlmuPenyakitJantung_Detail.Where(x => x.NoReg == noreg).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<AssesmentIlmuPenyakitJantungModelDetail>(x);
                            var namaobat = sim.mBarang.FirstOrDefault(z => z.Kode_Barang == y.NamaObat);
                            y.NamaObatNama = namaobat == null ? "" : namaobat.Nama_Barang;
                            model.IPJntung_List.Add(false, y);
                        }
                        #endregion

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                        model.NRM = nrm;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                    }

                    var dokumen_temp = s.trTemplate.Where(e => e.DokumenID == "Assesment Poli Jantung").ToList();

                    ViewBag.DokumenID = "Assesment Poli Jantung";
                    ViewBag.NamaDokumen = "Assesment Poli Jantung";

                    model.ListTemplate = new List<SelectItemListPenyakitJantung>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListPenyakitJantung()
                        {
                            Value = list.NoReg,
                            Text = list.NamaTemplate
                        });
                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string noreg, string copy, string nrm)
        {
            var model = new EMRIlmuPenyakitJantungViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var dokumen = s.IlmuPenyakitJantung.FirstOrDefault(x => x.NoReg == copy);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRIlmuPenyakitJantungViewModel>(dokumen);
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                        model.SectionID = nrm;

                        var DokterBPJP = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterBPJP);
                        if (DokterBPJP != null) model.DokterBPJPNama = DokterBPJP.NamaDOkter;

                    }
                    else
                    {
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                        model.SectionID = nrm;
                    }
                    model.MODEVIEW = 0;

                    var dokumen_temp = s.trTemplate.Where(e => e.DokumenID == "Assesment Poli Jantung").ToList();

                    ViewBag.DokumenID = "Assesment Poli Jantung";
                    ViewBag.NamaDokumen = "Assesment Poli Jantung";

                    model.ListTemplate = new List<SelectItemListPenyakitJantung>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListPenyakitJantung()
                        {
                            Value = list.NoReg,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {

            if (ModelState.IsValid)
            {
                var req = Request.Form[""];
            }
            try
            {
                var item = new EMRIlmuPenyakitJantungViewModel();

                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var model = s.IlmuPenyakitJantung.FirstOrDefault(x => x.NoReg == item.NoReg);
                    var activity = "";
                    var dokumenid = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<IlmuPenyakitJantung>(item);
                        s.IlmuPenyakitJantung.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoReg == item.NoReg);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Create Assesmen Jantung";
                    }
                    else
                    {
                        model = IConverter.Cast<IlmuPenyakitJantung>(item);
                        s.IlmuPenyakitJantung.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoReg == item.NoReg);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Update Assesmen Jantung";
                    }

                    if (item.save_template == true && item.nama_template != null)
                    {
                        var temp = new trTemplate();
                        temp.NoReg = item.NoReg;
                        temp.NamaTemplate = item.nama_template;
                        temp.DokterID = item.DokterBPJP;
                        temp.DokumenID = "Assesment Poli Jantung";
                        temp.SectionID = sectionid;
                        s.trTemplate.Add(temp);
                    }

                    #region === Detail Keperawatan
                    if (item.IPJntung_List == null) item.IPJntung_List = new ListDetail<AssesmentIlmuPenyakitJantungModelDetail>();
                    item.IPJntung_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.IPJntung_List)
                    {
                        x.Model.NoReg = item.NoReg;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.IPJntung_List;
                    var real_list = s.IlmuPenyakitJantung_Detail.Where(x => x.NoReg == item.NoReg).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.IlmuPenyakitJantung_Detail.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.IlmuPenyakitJantung_Detail.Add(IConverter.Cast<IlmuPenyakitJantung_Detail>(x.Model));
                        }
                        else
                        {
                            _m.No = x.Model.No;
                            _m.Tanggal = DateTime.Now;
                            _m.Jam = DateTime.Now;
                            _m.NamaObat = x.Model.NamaObat;
                            _m.Dosis = x.Model.Dosis;
                            _m.Lamanya = x.Model.Lamanya;
                        }
                    }
                    #endregion

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoReg}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}