﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class IdentitasPasienController : Controller
    {
        // GET: IdentitasPasien
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var m = eSIM.VW_IdentitasPasien.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    model.NoReg = noreg;
                    model.SectionID = sectionid;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.IdentitasPasien.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.IdentitasPasien = IConverter.Cast<IdentitasPasienViewModel>(item);
                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Petugas);
                            if (dokter != null)
                            {
                                model.IdentitasPasien.PetugasNama = dokter.NamaDOkter;
                            }

                            //var fingerdokter = eEMR.IdentitasPasien.Where(x => x.Dokter == model.IdentitasPasien.Dokter && x.TandaTanganDokter == null && x.NRM == model.NRM).FirstOrDefault();
                            //if (fingerdokter != null)
                            //{
                            //    model.IdentitasPasien.SudahRegDokter = 1;
                            //}
                            //else
                            //{
                            //    model.IdentitasPasien.SudahRegDokter = 0;
                            //}

                            //if (model.IdentitasPasien.Dokter != null)
                            //{
                            //    ViewBag.UrlFingerIdentitasPasienDokterPemeriksa = GetEncodeUrlPenolakanRI(model.IdentitasPasien.Dokter, model.IdentitasPasien.NoReg, sectionid);
                            //}
                        }
                        else
                        {
                            item = new IdentitasPasien();
                            model.IdentitasPasien = IConverter.Cast<IdentitasPasienViewModel>(item);
                            //ViewBag.UrlFingerIdentitasPasienDokterPemeriksa = GetEncodeUrlPenolakanRI(model.IdentitasPasien.Dokter, model.IdentitasPasien.NoReg, sectionid);
                            model.IdentitasPasien.NRM = m.NRM;
                            model.IdentitasPasien.Nama = m.NamaPasien;
                            model.IdentitasPasien.Tempat = m.TempatLahir;
                            model.IdentitasPasien.TanggalLahir = m.TglLahir.ToString();
                            model.IdentitasPasien.JenisKelamin = m.JenisKelamin;
                            model.IdentitasPasien.Agama = m.Agama;
                            model.IdentitasPasien.Alamat = m.Alamat;
                            model.IdentitasPasien.Kabupaten = m.Kabupaten;
                            model.IdentitasPasien.Provensi = m.Provinsi;
                            model.IdentitasPasien.Kewarganegaraan = m.Nationality;
                            model.IdentitasPasien.Pendidikan = m.Pendidikan;
                            model.IdentitasPasien.Pekerjaan = m.Pekerjaan;
                            model.IdentitasPasien.PangkatKesatuan = m.Pangkat;
                            model.IdentitasPasien.StatusKawin = m.StatusPerkawinan;
                            model.IdentitasPasien.Nama = m.PenanggungHubungan;
                            model.IdentitasPasien.TempatPenganggungJawab = m.PenanggungTempatLahir;
                            model.IdentitasPasien.AlamatLahirPenganggungJawab = m.PenanggungAlamat;
                            model.IdentitasPasien.KabupatenLahirPenganggungJawab = m.PenanggungKabupaten;
                            model.IdentitasPasien.NoTelpLahirPenganggungJawab = m.PenanggungPhone;
                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string IdentitasPasien_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.IdentitasPasien.NoReg = noreg;
                            item.IdentitasPasien.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                            item.IdentitasPasien.NRM = m.NRM;
                            var model = eEMR.IdentitasPasien.FirstOrDefault(x => x.SectionID == item.IdentitasPasien.SectionID && x.NoReg == item.NoReg);

                            if (model == null)
                            {
                                model = new IdentitasPasien();
                                var o = IConverter.Cast<IdentitasPasien>(item.IdentitasPasien);
                                eEMR.IdentitasPasien.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<IdentitasPasien>(item.IdentitasPasien);
                                eEMR.IdentitasPasien.AddOrUpdate(model);
                            }

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFPenolakan(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["SectionNamaPelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"IdentitasPasien_IGD.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"IdentitasPasien_IGD", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"IdentitasPasien_IGD;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }

        public string GetEncodeUrlPenolakanRI(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifIdentitasPasien/get_keyskdpjp";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }
    }
}