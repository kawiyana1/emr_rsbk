﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class HasilPenunjangController : Controller
    {
        // GET: HasilPenunjang
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMR_Entities())
                    {
                        var fromdate = DateTime.Parse(filter[34]);
                        var todate = DateTime.Parse(filter[35]);
                        var section = Request.Cookies["SectionIDPelayanan"].Value;
                        IQueryable<EMR_GetListPxPenunjang_Result> p = sim.EMR_GetListPxPenunjang(section, fromdate, todate);
                        var totalcount = p.Count();
                        if (!string.IsNullOrEmpty(filter[28]))
                            p = p.Where($"NoBill.Contains(@0)", filter[28]);
                        if (!string.IsNullOrEmpty(filter[8]))
                            p = p.Where($"NoReg.Contains(@0)", filter[8]);
                        if (!string.IsNullOrEmpty(filter[31]))
                            p = p.Where($"NRM.Contains(@0)", filter[31]);
                        if (!string.IsNullOrEmpty(filter[9]))
                            p = p.Where($"NRM.Contains(@0)", filter[9]);
                        if (!string.IsNullOrEmpty(filter[11]))
                            p = p.Where($"NamaPasien.Contains(@0)", filter[11]);
                        if (!string.IsNullOrEmpty(filter[32]))
                            p = p.Where($"Tipe.Contains(@0)", filter[32]);
                        totalcount = p.Count();
                        var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                            .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                        result = new ResultSS(models.Length, null, totalcount, pageIndex);
                        var datas = new List<HasilBacaPenunjangViewModel>();
                        foreach (var x in models.ToList())
                        {
                            var m = IConverter.Cast<HasilBacaPenunjangViewModel>(x);
                            m.Tanggal_View = x.Tanggal.Value.ToString("dd/MM/yyyy");
                            m.UserIDWeb = User.Identity.GetUserId();
                            if (m.NoBuktiHasil == null)
                            {
                                m.Hasil = "Belum";
                            }
                            else
                            {
                                m.Hasil = "Sudah";
                            }
                            datas.Add(m);
                        }
                        result.Data = datas;

                        return JsonConvert.SerializeObject(new TableList(result));
                    }
                }
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #region ==== C R E A T E
        public string AutoId()
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var m = s.AutoNumber_HasilPenunjang().FirstOrDefault();
                    if (m == null) return JsonHelper.JsonMsgError("Data tidak ditemukan");
                    result = new ResultSS(m) { IsSuccess = true };
                }
                return JsonConvert.SerializeObject(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListPemeriksa(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<mDokter> proses = s.mDokter;
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(mDokter.DokterID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(mDokter.NamaDOkter)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<mDokter>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string nrm, string nobill, string noorder, string noreg)
        {
            var model = new HasilBacaPenunjangViewModel();
            {
                using (var eEMR = new EMR_Entities())
                {
                    using (var eSIM = new SIM_Entities())
                    {
                        var userclaim = (System.Security.Claims.ClaimsIdentity)User.Identity;
                        var username = userclaim.GetUserName();
                        var iduser = User.Identity.GetUserId();
                        var section = Request.Cookies["SectionIDPelayanan"].Value;
                        var datapasien = eSIM.mPasien.FirstOrDefault(x => x.NRM == nrm);
                        if (noorder == "null" || noorder == null) return Content("<script language='javascript' type='text/javascript'>alertpopup('Data Transaksi Tidak ditemukan','danger'); $('.modal').on('shown.bs.modal', function(){ $('.modal.in').modal('hide'); }); </script>");
                        var klinis = eSIM.EMR_GET_ListOrderPenunjang(nrm).FirstOrDefault(x => x.NoBuktiMemo == noorder);
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                        model.NRM = datapasien == null ? "" : datapasien.NRM;
                        model.JenisKelamin = datapasien == null ? "" : datapasien.JenisKelamin;
                        model.Alamat = datapasien == null ? "" : datapasien.Alamat;
                        model.NamaPasien = datapasien == null ? "" : datapasien.NamaPasien;
                        model.NoBill = nobill;
                        model.NoReg = noreg;
                        model.NoBuktiPermintaan = noorder;
                        //model.DiagnosaIndikasi = klinis == null ? "" : klinis.Klinis;
                        model.PemeriksaanTambahan = klinis == null ? "" : klinis.OrderTindakan;

                        //if (section == "SEC005")
                        //{
                        //    model.Id_mJenisPenunjang_Text = eEMR.mJenisPenunjang.FirstOrDefault(x => x.Id == 1).JenisPenunjang;
                        //    model.Id_mJenisPenunjang = 1;
                        //}
                        //else if (section == "SEC006")
                        //{
                        //    model.Id_mJenisPenunjang_Text = eEMR.mJenisPenunjang.FirstOrDefault(x => x.Id == 2).JenisPenunjang;
                        //    model.Id_mJenisPenunjang = 2;
                        //}


                        ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
                        if (Request.IsAjaxRequest())

                            return PartialView(model);
                        else
                            return View(model);
                    }
                }
            }
        }

        [HttpPost, ValidateInput(false)]
        [ActionName("Create")]
        public string Create_Post(IEnumerable<HttpPostedFileBase> files)
        {
            try
            {

                var item = new HasilBacaPenunjangViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var sim = new SIM_Entities())
                    {
                        using (var s = new EMR_Entities())
                        {

                            var m = IConverter.Cast<trHasilBacaPenunjang>(item);
                            m.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                            m.NoBuktiBilling = item.NoBill;
                            sim.trHasilBacaPenunjang.Add(m);

                            var i = 1;
                            foreach (HttpPostedFileBase file in item.files)
                            {
                                string subPath = Request.Cookies["SectionIDPelayanan"].Value;

                                bool exists = System.IO.Directory.Exists(Server.MapPath("~/FileManager/" + item.NRM + "/" + subPath));


                                if (!exists)
                                    System.IO.Directory.CreateDirectory(Server.MapPath("~/FileManager/" + item.NRM + "/" + subPath));


                                if (file != null)
                                {
                                    var InputFileName = Path.GetFileName(file.FileName);
                                    var fullfilename = /*item.NRM + "_" +*/ i + "_" + InputFileName;
                                    var ServerSavePath = Path.Combine(Server.MapPath($"~/FileManager/{item.NRM}/{subPath}/") + fullfilename);
                                    file.SaveAs(ServerSavePath);

                                    sim.trHasilBacaPenunjangDetail.Add(new trHasilBacaPenunjangDetail()
                                    {
                                        NoBukti = item.NoBukti,
                                        NoUrut = Convert.ToInt16(i),
                                        PathDokumen = ServerSavePath,
                                        FileName = fullfilename,
                                    });

                                    i++;
                                }

                            }



                            result = new ResultSS(s.SaveChanges());
                            return new JavaScriptSerializer().Serialize(new
                            {
                                IsSuccess = true,
                                Data = m

                            });
                        }
                    }

                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ==== D E T A I L
        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail_Get(string id)
        {

            var model = new HasilBacaPenunjangViewModel();

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.Vw_HasilBacaPenunjang.FirstOrDefault(x => x.NoBukti == id);
                    var data = eSIM.trHasilBacaPenunjang.FirstOrDefault(x => x.NoBukti == id);



                    if (data != null)
                    {
                        model = IConverter.Cast<HasilBacaPenunjangViewModel>(data);
                        model.JenisKelamin = (m.JenisKelamin == "Perempuan") ? "F" : "M";
                        var datapasien = eSIM.mPasien.FirstOrDefault(x => x.NRM == data.NRM);
                        if (datapasien != null)
                        {
                            model.NamaPasien = datapasien.NamaPasien;
                            model.Alamat = datapasien.Alamat;
                        }

                        var a = eSIM.mDokter.FirstOrDefault(x => x.DokterID == m.Pemeriksaan);
                        if (a != null)
                        {
                            model.PemeriksaanNama = a.NamaDOkter;
                        }
                    }



                    model.FileUpload = new ListDetail<HasilBacaPenunjangDetailViewModel>();
                    var file_upload = eSIM.trHasilBacaPenunjangDetail.Where(x => x.NoBukti == id).ToList();
                    foreach (var x in file_upload)
                    {
                        var y = IConverter.Cast<HasilBacaPenunjangDetailViewModel>(x);
                        model.FileUpload.Add(false, y);
                    }
                }

            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id)
        {

            var model = new HasilBacaPenunjangViewModel();

            using (var s = new SIM_Entities())
            {
                var m = s.Vw_HasilBacaPenunjang.FirstOrDefault(x => x.NoBukti == id);
                model = IConverter.Cast<HasilBacaPenunjangViewModel>(m);
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Delete")]
        public string Delete(string id)
        {

            try
            {
                ResultSS result;
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMR_Entities())
                    {
                        var model = new HasilBacaPenunjangViewModel();
                        var dokumen = sim.trHasilBacaPenunjangDetail.Where(x => x.NoBukti == id);
                        var item = sim.trHasilBacaPenunjang.FirstOrDefault(x => x.NoBukti == id);
                        var jenis = Request.Cookies["SectionIDPelayanan"].Value;
                        if (dokumen.Count() != 0)
                        {
                            foreach (var file in dokumen)
                            {

                                //bool exists = System.IO.Directory.Exists(Server.MapPath(file.PathDokumen));
                                bool exists = System.IO.File.Exists(Server.MapPath("~/FileManager/" + item.NRM + "/" + jenis + "/" + file.FileName));

                                if (exists)
                                    System.IO.File.Delete(Server.MapPath("~/FileManager/" + item.NRM + "/" + jenis + "/" + file.FileName));

                                sim.trHasilBacaPenunjangDetail.Remove(file);
                            }
                        }
                        sim.trHasilBacaPenunjang.Remove(item);
                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Hasil Penunjang delete {id}"
                        };
                        UserActivity.InsertUserActivity(userActivity);

                    }
                }
                return JsonHelper.JsonMsgDelete(result, -1);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }

        }

        #endregion

        #region ==== TEMPLATE

        //[HttpPost]
        //public string ListTemplate(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        //{

        //    try
        //    {
        //        ResultSS result;
        //        using (var s = new EMREntities())
        //        {
        //            IQueryable<HasilBacaPenunjang_Temp> proses = s.HasilBacaPenunjang_Temp;
        //            if (!string.IsNullOrEmpty(filter[1]))
        //                proses = proses.Where($"Judul.Contains(@0)", filter[1]);
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
        //                .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
        //            result = new ResultSS(models.Length, models, totalcount, pageIndex);
        //            var m = models.ToList().ConvertAll(x => new StandardViewModel()
        //            {
        //                Kode = x.Judul,
        //                Nama = x.Judul
        //            });
        //            result.Data = m;
        //        }

        //        return JsonConvert.SerializeObject(new TableList(result));
        //    }
        //    catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }


        //}

        //public string LoadTemplate(string id)
        //{
        //    try
        //    {
        //        using (var s = new EMREntities())
        //        {
        //            var model = s.HasilBacaPenunjang_Temp.FirstOrDefault(x => x.Judul == id);
        //            if (model == null) throw new Exception("Template tidak ditemukan");
        //            return new JavaScriptSerializer().Serialize(new
        //            {
        //                IsSuccess = true,
        //                Data = model
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
        //    catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        //}

        #endregion

        [HttpGet]
        [ActionName("UploadPenunjang")]
        public ActionResult UploadPenunjang_Get(string noreg, string sectionid, string nrm, string nomor)
        {
            var model = new UploadPenunjangViewModel();
            model.NoReg = noreg;
            model.SectionID = sectionid;
            model.NRM = nrm;
            model.Nomor = nomor;
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();

        }

        [HttpPost, ValidateInput(false)]
        [ActionName("UploadPenunjang")]
        public ActionResult UploadPenunjang_Post(HttpPostedFileBase[] files)
        {
            string noreg = "";
            string nomor = "";
            try
            {

                var item = new UploadPenunjangViewModel();
                TryUpdateModel(item);

                noreg = item.NoReg;
                nomor = item.Nomor;
                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new EMR_Entities())
                    {

                        using (var eSIM = new SIM_Entities())
                        {
                            var user = User.Identity.GetUserId();
                            var nrm = eSIM.VW_DataPasienReg.FirstOrDefault(x => x.NoReg == noreg);
                            var m = IConverter.Cast<trHasilBacaPenunjang>(item);
                            m.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                            m.Tanggal = DateTime.Today;
                            m.Jam = DateTime.Now;
                            m.NRM = nrm == null ? "" : nrm.NRM;
                            eSIM.trHasilBacaPenunjang.Add(m);

                            var i = 1;
                            foreach (HttpPostedFileBase file in files)
                            {
                                string subPath = Request.Cookies["SectionIDPelayanan"].Value;

                                bool exists = System.IO.Directory.Exists(Server.MapPath("~/FileManager/" + m.NRM + "/" + subPath));


                                if (!exists)
                                    System.IO.Directory.CreateDirectory(Server.MapPath("~/FileManager/" + m.NRM + "/" + subPath));


                                if (file != null)
                                {
                                    var InputFileName = Path.GetFileName(file.FileName);
                                    var fullfilename = /*m.NRM + "_" +*/ i + "_" + InputFileName;
                                    var ServerSavePath = Path.Combine(Server.MapPath($"~/FileManager/{m.NRM}/{subPath}/") + fullfilename);
                                    file.SaveAs(ServerSavePath);

                                    eSIM.trHasilBacaPenunjangDetail.Add(new trHasilBacaPenunjangDetail()
                                    {
                                        NoBukti = item.NoBukti,
                                        NoUrut = Convert.ToInt16(i),
                                        PathDokumen = ServerSavePath,
                                        FileName = fullfilename,
                                    });

                                    i++;
                                }

                            }



                            result = new ResultSS(s.SaveChanges());
                            ViewBag.AlertStatus = new
                            {
                                isStatus = true,
                                name = "success",
                                mssg = "Hasil Baca Penunjang berhasil disimpan"
                            };
                            return RedirectToAction("Index", "MedicalRecord", new { noreg = item.NoReg, nomor = item.Nomor });
                        }
                    }

                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                ViewBag.AlertStatus = new
                {
                    isStatus = true,
                    name = "error",
                    mssg = "Hasil Baca Penunjang gagal disimpan"
                };
                return RedirectToAction("Index", "MedicalRecord", new { noreg = item.NoReg, nomor = item.Nomor });
            }
            catch (SqlException ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            catch (DbEntityValidationException ex)
            {
                TempData["Status"] = "danger";
                TempData["Message"] = string.Join("|",
                    ex.EntityValidationErrors.First().ValidationErrors.Select(x => x.ErrorMessage));
            }
            catch (Exception ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            ViewBag.AlertStatus = new
            {
                isStatus = true,
                name = "error",
                mssg = "Hasil Baca Penunjang gagal disimpan"
            };
            return RedirectToAction("Index", "MedicalRecord", new { noreg = noreg, nomor = nomor });

        }

        [HttpPost]
        public string ListPasien(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<mPasien> proses = s.mPasien;
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(mPasien.NRM)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(mPasien.NamaPasien)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(mPasien.TglLahir)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(mPasien.Alamat)}.Contains(@0)", filter[3]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PasienViewModel>(x));
                    foreach (var x in m)
                    {
                        x.TanggalLahir = x.TglLahir.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}