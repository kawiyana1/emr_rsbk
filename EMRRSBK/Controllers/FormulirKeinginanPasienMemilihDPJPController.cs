﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class FormulirKeinginanPasienMemilihDPJPController : Controller
    {
        // GET: FormulirKeinginanPasienMemilihDPJP
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    model.NoReg = noreg;
                    model.SectionID = sectionid;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.FormulirKeinginanPasienMemilihDPJP.FirstOrDefault(x => x.NoReg == noreg );
                        if (item != null)
                        {
                            model.FormPasienDPJP = IConverter.Cast<FormulirKeinginanPasienMemilihDPJPViewModel>(item);
                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Dokter);
                            if (dokter != null)
                            {
                                model.FormPasienDPJP.DokterNama = dokter.NamaDOkter;
                            }
                            var petugas = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Petugas);
                            if (petugas != null)
                            {
                                model.FormPasienDPJP.PetugasNama = petugas.NamaDOkter;
                            }
                            var dpjp = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.DPJP);
                            if (dpjp != null)
                            {
                                model.FormPasienDPJP.DPJPNama = dpjp.NamaDOkter;
                            }
                            model.FormPasienDPJP.Tanggal = DateTime.Now;
                            model.FormPasienDPJP.TanggalLahir = DateTime.Now;
                            model.FormPasienDPJP.NamaPasien = m.NamaPasien;
                            model.FormPasienDPJP.TempatLahirPasien = m.TempatLahir;
                            model.FormPasienDPJP.TglLahirPasien = m.TglLahir;
                            model.FormPasienDPJP.NoRM = m.NRM;
                            var fingerpetugas = eEMR.FormulirKeinginanPasienMemilihDPJP.Where(x => x.Petugas == model.FormPasienDPJP.Petugas && x.TandaTanganPetugas == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerpetugas != null)
                            {
                                model.FormPasienDPJP.SudahRegPetugas = 1;
                            }
                            else
                            {
                                model.FormPasienDPJP.SudahRegPetugas = 0;
                            }

                            var fingerpasien = eEMR.FormulirKeinginanPasienMemilihDPJP.Where(x => x.NRM == model.FormPasienDPJP.NRM && x.TandaTangan == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerpasien != null)
                            {
                                model.FormPasienDPJP.SudahRegPasien = 1;
                            }
                            else
                            {
                                model.FormPasienDPJP.SudahRegPasien = 0;
                            }

                            if (model.FormPasienDPJP.Petugas != null)
                            {
                                ViewBag.UrlFingerFormPasienDPJPDokterPemeriksa = GetEncodeUrlKeinginanDPJP(model.FormPasienDPJP.Petugas, model.FormPasienDPJP.NoReg, sectionid);
                            }

                            ViewBag.UrlFingerFormPasienDPJPPasien = GetEncodeUrlKeinginanDPJP(model.FormPasienDPJP.NRM, model.FormPasienDPJP.NoReg, sectionid);
                        }
                        else
                        {
                            item = new FormulirKeinginanPasienMemilihDPJP();
                            model.FormPasienDPJP = IConverter.Cast<FormulirKeinginanPasienMemilihDPJPViewModel>(item);
                            model.FormPasienDPJP.Tanggal = DateTime.Now;
                            model.FormPasienDPJP.NamaPasien = m.NamaPasien;
                            model.FormPasienDPJP.TempatLahirPasien = m.TempatLahir;
                            model.FormPasienDPJP.TglLahirPasien = m.TglLahir;
                            model.FormPasienDPJP.NoRM = m.NRM;
                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string FormulirKeinginanPasienMemilihDPJP_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.FormPasienDPJP.NoReg = noreg;
                            item.FormPasienDPJP.SectionID = sectionid;
                            item.FormPasienDPJP.NRM = m.NRM;
                            item.FormPasienDPJP.Tanggal = DateTime.Now;
                            var model = eEMR.FormulirKeinginanPasienMemilihDPJP.FirstOrDefault(x => x.NoReg == noreg);

                            if (model == null)
                            {
                                model = new FormulirKeinginanPasienMemilihDPJP();
                                var o = IConverter.Cast<FormulirKeinginanPasienMemilihDPJP>(item.FormPasienDPJP);
                                eEMR.FormulirKeinginanPasienMemilihDPJP.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<FormulirKeinginanPasienMemilihDPJP>(item.FormPasienDPJP);
                                eEMR.FormulirKeinginanPasienMemilihDPJP.AddOrUpdate(model);
                            }

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFFormulirKeinginanPasienMemilihDPJP(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["SectionNamaPelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"FormulirKeinginanPasienMemilihDPJP_IGD.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"FormulirKeinginanPasienMemilihDPJP_IGD", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"FormulirKeinginanPasienMemilihDPJP_IGD;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }

        public string GetEncodeUrlKeinginanDPJP(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifFormulirKeinginanPasienMemilihDPJP/get_keyskdpjp";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }
    }
}