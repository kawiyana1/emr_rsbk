﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class AsuhanGiziAnakController : Controller
    {
        // GET: AsuhanGiziAnak
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();
            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);

                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var sectionnama = Request.Cookies["SectionNamaPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.AsuhanGiziAnak.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.AshnGiziAnak = IConverter.Cast<AsuhanGiziAnakViewModel>(item);

                            model.AshnGiziAnak.SectionID = sectionid;
                            model.AshnGiziAnak.NRM = model.NRM;
                            model.AshnGiziAnak.NoReg = noreg;
                            model.AshnGiziAnak.Report = 1;
                            model.AshnGiziAnak.Ruangan = sectionnama;
                            var ahligizi = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.AhliGizi);
                            if (ahligizi != null)
                            {
                                model.AshnGiziAnak.AhliGiziNama = ahligizi.NamaDOkter;
                            }
                        }
                        else
                        {
                            item = new AsuhanGiziAnak();
                            model.AshnGiziAnak = IConverter.Cast<AsuhanGiziAnakViewModel>(item);
                            model.AshnGiziAnak.SectionID = sectionid;
                            model.AshnGiziAnak.NRM = model.NRM;
                            model.AshnGiziAnak.Tanggal = DateTime.Today;
                            model.AshnGiziAnak.Jam = DateTime.Now;
                            model.AshnGiziAnak.NoReg = noreg;
                            model.AshnGiziAnak.Report = 0;

                            model.AshnGiziAnak.Ruangan = sectionnama;


                        }

                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string AshnGiziAnak_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == item.AshnGiziAnak.NoReg);
                            item.AshnGiziAnak.NoReg = item.AshnGiziAnak.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.AshnGiziAnak.SectionID = sectionid;
                            item.AshnGiziAnak.NRM = item.AshnGiziAnak.NRM;
                            var model = eEMR.AsuhanGiziAnak.FirstOrDefault(x => x.SectionID == item.AshnGiziAnak.SectionID && x.NoReg == item.AshnGiziAnak.NoReg);

                            if (model == null)
                            {
                                model = new AsuhanGiziAnak();
                                var o = IConverter.Cast<AsuhanGiziAnak>(item.AshnGiziAnak);
                                eEMR.AsuhanGiziAnak.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<AsuhanGiziAnak>(item.AshnGiziAnak);
                                eEMR.AsuhanGiziAnak.AddOrUpdate(model);
                            }



                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}