﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class EMRAsuhanKeperawatanObservasiHDController : Controller
    {
        // GET: EMRAsuhanKeperawatanObservasiHD
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string noreg, string nrm, int view = 0)
        {
            var model = new EMRAsuhanKeperawatanObservasiHDViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var dokumen = s.AsuhanKeperawatanDanObservasiPasienHemodialisis.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRAsuhanKeperawatanObservasiHDViewModel>(dokumen);
                        var dokterhd = sim.mDokter.FirstOrDefault(x => x.DokterID == model.PrawatHD);
                        if (dokterhd != null)
                        {
                            model.PrawatHDNama = dokterhd.NamaDOkter;
                        }

                        var dpjp = sim.mDokter.FirstOrDefault(x => x.DokterID == model.DPJP);
                        if (dpjp != null)
                        {
                            model.DPJPNama = dpjp.NamaDOkter;
                        }

                        var bidan = sim.mDokter.FirstOrDefault(x => x.DokterID == model.PrawatPunksi);
                        if (bidan != null)
                        {
                            model.PrawatPunksiNama = bidan.NamaDOkter;
                        }

                        var pemeriksa = sim.mDokter.FirstOrDefault(x => x.DokterID == model.PrawatPenanggungJawab);
                        if (pemeriksa != null)
                        {
                            model.PrawatPenanggungJawabNama = pemeriksa.NamaDOkter;
                        }

                        #region === Detail Pelaksana
                        model.Pelaksanaan_List = new ListDetail<AsuhanKeperawatanDanObservasiPasienHemodialisisModelDetail>();
                        var detail_2 = s.AsuhanKeperawatanDanObservasiPasienHemodialisis_Detail.Where(x => x.NoReg == noreg).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<AsuhanKeperawatanDanObservasiPasienHemodialisisModelDetail>(x);
                            var namadokter = sim.mDokter.FirstOrDefault(z => z.DokterID == y.Petugas);
                            y.PetugasNama = namadokter == null ? "" : namadokter.NamaDOkter;
                            model.Pelaksanaan_List.Add(false, y);
                        }
                        #endregion

                        #region === Detail  Terapi
                        model.Terapi_List = new ListDetail<AsuhanKeperawatanDanObservasiPasienHemodialisisTerapiModelDetail>();
                        var detail_1 = s.AsuhanKeperawatanDanObservasiPasienHemodialisisTerapiIntra_Detail.Where(x => x.NoReg == noreg).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<AsuhanKeperawatanDanObservasiPasienHemodialisisTerapiModelDetail>(x);
                            var perawat1 = sim.mDokter.FirstOrDefault(z => z.DokterID == y.Perawat1);
                            y.Perawat1Nama = perawat1 == null ? "" : perawat1.NamaDOkter;
                            var perawat2 = sim.mDokter.FirstOrDefault(z => z.DokterID == y.Perawat2);
                            y.Perawat2Nama = perawat2 == null ? "" : perawat2.NamaDOkter;
                            var obat = sim.mBarang.FirstOrDefault(z => z.Kode_Barang == y.Obat);
                            y.ObatNama = obat == null ? "" : obat.Nama_Barang;
                            model.Terapi_List.Add(false, y);
                        }
                        #endregion

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                        model.NRM = nrm;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                    }
                    model.NoReg = noreg;
                    model.SectionID = sectionid;
                    model.NRM = nrm;
                    var dokumen_temp = s.trTemplate.Where(e => e.DokumenID == "Asuhan Keperawatan Observasi HD").ToList();

                    ViewBag.DokumenID = "Asuhan Keperawatan Observasi HD";
                    ViewBag.NamaDokumen = "Asuhan Keperawatan Observasi HD";

                    model.ListTemplate = new List<SelectItemListAsuhanKeperawatanObservasiHD>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListAsuhanKeperawatanObservasiHD()
                        {
                            Value = list.NoReg,
                            Text = list.NamaTemplate
                        });
                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string noreg, string copy, string nrm)
        {
            var model = new EMRAsuhanKeperawatanObservasiHDViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var dokumen = s.AsuhanKeperawatanDanObservasiPasienHemodialisis.FirstOrDefault(x => x.NoReg == copy);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRAsuhanKeperawatanObservasiHDViewModel>(dokumen);
                        model.NoReg = noreg;
                        model.NRM = nrm;
                        model.SectionID = sectionid;

                        var dpjp = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DPJP);
                        if (dpjp != null) model.DPJPNama = dpjp.NamaDOkter;

                    }
                    else
                    {
                        model.NRM = nrm;
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                    }
                    model.MODEVIEW = 0;
                    model.NRM = nrm;
                    model.NoReg = noreg;
                    model.SectionID = sectionid;
                    var dokumen_temp = s.trTemplate.Where(e => e.DokumenID == "Asuhan Keperawatan Observasi HD").ToList();

                    ViewBag.DokumenID = "Asuhan Keperawatan Observasi HD";
                    ViewBag.NamaDokumen = "Asuhan Keperawatan Observasi HD";

                    model.ListTemplate = new List<SelectItemListAsuhanKeperawatanObservasiHD>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListAsuhanKeperawatanObservasiHD()
                        {
                            Value = list.NoReg,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRAsuhanKeperawatanObservasiHDViewModel();

                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var model = s.AsuhanKeperawatanDanObservasiPasienHemodialisis.FirstOrDefault(x => x.NoReg == item.NoReg);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<AsuhanKeperawatanDanObservasiPasienHemodialisis>(item);
                        s.AsuhanKeperawatanDanObservasiPasienHemodialisis.Add(o);
                        activity = "Create Asuhan Keperawatan Observasi HD";
                    }
                    else
                    {
                        model = IConverter.Cast<AsuhanKeperawatanDanObservasiPasienHemodialisis>(item);
                        s.AsuhanKeperawatanDanObservasiPasienHemodialisis.AddOrUpdate(model);
                        activity = "Update Asuhan Keperawatan Observasi HD";
                    }

                    if (item.save_template == true && item.nama_template != null)
                    {
                        var temp = new trTemplate();
                        temp.NoReg = item.NoReg;
                        temp.NamaTemplate = item.nama_template;
                        temp.DokterID = item.DPJP;
                        temp.DokumenID = "Asuhan Keperawatan Observasi HD";
                        temp.SectionID = sectionid;
                        s.trTemplate.Add(temp);
                    }

                    #region === Detail Pelaksana
                    if (item.Pelaksanaan_List == null) item.Pelaksanaan_List = new ListDetail<AsuhanKeperawatanDanObservasiPasienHemodialisisModelDetail>();
                    item.Pelaksanaan_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Pelaksanaan_List)
                    {
                        x.Model.NoReg = item.NoReg;
                        x.Model.SectionID = item.SectionID;
                        x.Model.NRM = item.NRM;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.Pelaksanaan_List;
                    var real_list = s.AsuhanKeperawatanDanObservasiPasienHemodialisis_Detail.Where(x => x.NoReg == item.NoReg).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.AsuhanKeperawatanDanObservasiPasienHemodialisis_Detail.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.AsuhanKeperawatanDanObservasiPasienHemodialisis_Detail.Add(IConverter.Cast<AsuhanKeperawatanDanObservasiPasienHemodialisis_Detail>(x.Model));
                        }
                        else
                        {
                            _m.No = x.Model.No;
                            _m.Jam = DateTime.Now;
                            _m.TekananDarah = x.Model.TekananDarah;
                            _m.Nadi = x.Model.Nadi;
                            _m.Blood = x.Model.Blood;
                            _m.Vena = x.Model.Vena;
                            _m.UltraGoal = x.Model.UltraGoal;
                            _m.UltraRate = x.Model.UltraRate;
                            _m.UltraRemove = x.Model.UltraRemove;
                            _m.Tindakan = x.Model.Tindakan;
                        }
                    }
                    #endregion

                    #region === Detail Terapi
                    if (item.Terapi_List == null) item.Terapi_List = new ListDetail<AsuhanKeperawatanDanObservasiPasienHemodialisisTerapiModelDetail>();
                    item.Terapi_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Terapi_List)
                    {
                        x.Model.NoReg = item.NoReg;
                        x.Model.SectionID = item.SectionID;
                        x.Model.NRM = item.NRM;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list1 = item.Terapi_List;
                    var real_list1 = s.AsuhanKeperawatanDanObservasiPasienHemodialisisTerapiIntra_Detail.Where(x => x.NoReg == item.NoReg).ToList();
                    foreach (var x in real_list1)
                    {
                        var m = new_list1.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.AsuhanKeperawatanDanObservasiPasienHemodialisisTerapiIntra_Detail.Remove(x);
                    }

                    foreach (var x in new_list1)
                    {
                        var _m = real_list1.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.AsuhanKeperawatanDanObservasiPasienHemodialisisTerapiIntra_Detail.Add(IConverter.Cast<AsuhanKeperawatanDanObservasiPasienHemodialisisTerapiIntra_Detail>(x.Model));
                        }
                        else
                        {
                            _m.Obat = x.Model.Obat;
                            _m.Dosis = x.Model.Dosis;
                            _m.CaraPemberian = x.Model.CaraPemberian;
                            _m.Waktu = x.Model.Waktu;
                            _m.Perawat1 = x.Model.Perawat1;
                            _m.Perawat2 = x.Model.Perawat2;
                        }
                    }
                    #endregion

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoReg}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}