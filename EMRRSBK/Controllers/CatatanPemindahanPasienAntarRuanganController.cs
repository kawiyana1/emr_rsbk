﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class CatatanPemindahanPasienAntarRuanganController : Controller
    {
        // GET: CatatanPemindahanPasienAntarRuangan
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    model.SectionID = sectionid;
                    model.NoReg = m.NoReg;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.CatatanPemindahanPasienAntarRuangan.FirstOrDefault(x => x.NoReg == noreg);
                        if (item != null)
                        {
                            model.CPARuangan = IConverter.Cast<CatatanPemindahanPasienAntarRuanganViewModelDetail>(item);
                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.DokterRawat_1);
                            if (dokter != null)
                            {
                                model.CPARuangan.DokterRawat_1Nama = dokter.NamaDOkter;
                            }

                            var dokter1 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.DokterRawat_2);
                            if (dokter1 != null)
                            {
                                model.CPARuangan.DokterRawat_2Nama = dokter1.NamaDOkter;
                            }

                            var dokter2 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.DokterRawat_3);
                            if (dokter2 != null)
                            {
                                model.CPARuangan.DokterRawat_3Nama = dokter2.NamaDOkter;
                            }

                            var perawatserah = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Diserahkan_Perawat);
                            if (perawatserah != null)
                            {
                                model.CPARuangan.Diserahkan_PerawatNama = perawatserah.NamaDOkter;
                            }

                            var perawatterima = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Diterima_Perawat);
                            if (perawatterima != null)
                            {
                                model.CPARuangan.Diterima_PerawatNama = perawatterima.NamaDOkter;
                            }
                           
                            model.CPARuangan.NoReg = noreg;
                            model.CPARuangan.NRM = model.NRM;
                            model.CPARuangan.SectionID = sectionid;
                            var fingerperawatserah = eEMR.CatatanPemindahanPasienAntarRuangan.Where(x => x.Diserahkan_Perawat == model.CPARuangan.Diserahkan_Perawat && x.TandaTanganPerawatSerah == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerperawatserah != null)
                            {
                                model.CPARuangan.SudahRegPerawatSerah = 1;
                            }
                            else
                            {
                                model.CPARuangan.SudahRegPerawatSerah = 0;
                            }
                            if (model.CPARuangan.Diserahkan_Perawat != null)
                            {
                                ViewBag.UrlFingerPerawatRuangSerah = GetEncodeUrlPerawatSerah(model.CPARuangan.Diserahkan_Perawat, model.CPARuangan.NoReg, sectionid);
                            }

                            var fingerperawatterima = eEMR.CatatanPemindahanPasienAntarRuangan.Where(x => x.Diterima_Perawat == model.CPARuangan.Diterima_Perawat && x.TandaTanganPerawatTerima == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerperawatterima != null)
                            {
                                model.CPARuangan.SudahRegPerawatTerima = 1;
                            }
                            else
                            {
                                model.CPARuangan.SudahRegPerawatTerima = 0;
                            }
                            if (model.CPARuangan.Diterima_Perawat != null)
                            {
                                ViewBag.UrlFingerPerawatRuangTerima = GetEncodeUrlPerawatTerima(model.CPARuangan.Diterima_Perawat, model.CPARuangan.NoReg, sectionid);
                            }
                        }
                        else
                        {
                            item = new CatatanPemindahanPasienAntarRuangan();
                            model.CPARuangan = IConverter.Cast<CatatanPemindahanPasienAntarRuanganViewModelDetail>(item);
                            ViewBag.UrlFingerPerawatRuangSerah = GetEncodeUrlPerawatSerah(model.CPARuangan.Diserahkan_Perawat, model.CPARuangan.NoReg, sectionid);
                            ViewBag.TandaTanganPerawatTerima = GetEncodeUrlPerawatTerima(model.CPARuangan.Diterima_Perawat, model.CPARuangan.NoReg, sectionid);
                            model.CPARuangan.NoReg = noreg;
                            model.CPARuangan.NRM = model.NRM;
                            model.CPARuangan.SectionID = sectionid;
                            model.CPARuangan.Tanggal = DateTime.Today;
                            model.CPARuangan.InfusTanggal = DateTime.Today;
                            model.CPARuangan.Pemindahan_Tanggal = DateTime.Today;
                            model.CPARuangan.ProsedurPembedahan_Tanggal = DateTime.Today;
                            model.CPARuangan.ObservasiTerakhir = DateTime.Now;
                            model.CPARuangan.TglPemasangan = DateTime.Today;
                            model.CPARuangan.Jam = DateTime.Now;
                            model.CPARuangan.JamSerahTerima = DateTime.Now;
                            model.CPARuangan.Pemindahan_Jam = DateTime.Now;
                        }

                        var List = eEMR.CatatanPemindahanPasienAntarRuangan_Detail.Where(x => x.NoReg == noreg).ToList();
                        if (List != null)
                        {

                            model.CPARuangan.DtailPemindahan_List = new ListDetail<DetailCttPemindahanAntarRuanganViewModelDetail>();

                            foreach (var x in List)
                            {
                                var y = IConverter.Cast<DetailCttPemindahanAntarRuanganViewModelDetail>(x);
                                model.CPARuangan.DtailPemindahan_List.Add(false, y);
                            }

                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string CatatanPemindahanPasienAntarRuangan_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.CPARuangan.NoReg = item.CPARuangan.NoReg;
                            item.CPARuangan.SectionID = sectionid;
                            item.CPARuangan.NRM = item.CPARuangan.NRM;
                            var model = eEMR.CatatanPemindahanPasienAntarRuangan.FirstOrDefault(x => x.NoReg == item.CPARuangan.NoReg);

                            if (model == null)
                            {
                                model = new CatatanPemindahanPasienAntarRuangan();
                                var o = IConverter.Cast<CatatanPemindahanPasienAntarRuangan>(item.CPARuangan);
                                eEMR.CatatanPemindahanPasienAntarRuangan.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<CatatanPemindahanPasienAntarRuangan>(item.CPARuangan);
                                eEMR.CatatanPemindahanPasienAntarRuangan.AddOrUpdate(model);
                            }

                            #region ===== DETAIL

                            if (item.CPARuangan.DtailPemindahan_List == null) item.CPARuangan.DtailPemindahan_List = new ListDetail<DetailCttPemindahanAntarRuanganViewModelDetail>();
                            item.CPARuangan.DtailPemindahan_List.RemoveAll(x => x.Remove);
                            foreach (var x in item.CPARuangan.DtailPemindahan_List)
                            {
                                x.Model.SectionID = sectionid;
                                x.Model.NoReg = item.CPARuangan.NoReg;
                                x.Model.NRM = item.CPARuangan.NRM;
                                x.Model.Tanggal = DateTime.Today;
                                x.Model.Jam = DateTime.Now;

                            }
                            var new_List = item.CPARuangan.DtailPemindahan_List;
                            var real_List = eEMR.CatatanPemindahanPasienAntarRuangan_Detail.Where(x => x.NoReg == item.CPARuangan.NoReg).ToList();
                            foreach (var x in real_List)
                            {
                                var z = new_List.FirstOrDefault(y => y.Model.No == x.No);
                                if (z == null)
                                {
                                    eEMR.CatatanPemindahanPasienAntarRuangan_Detail.Remove(x);
                                }

                            }
                            foreach (var x in new_List)
                            {

                                var _m = real_List.FirstOrDefault(y => y.No == x.Model.No);

                                if (_m == null)
                                {
                                    _m = new CatatanPemindahanPasienAntarRuangan_Detail();
                                    eEMR.CatatanPemindahanPasienAntarRuangan_Detail.Add(IConverter.Cast<CatatanPemindahanPasienAntarRuangan_Detail>(x.Model));
                                }

                                else
                                {
                                    _m = IConverter.Cast<CatatanPemindahanPasienAntarRuangan_Detail>(x.Model);
                                    eEMR.CatatanPemindahanPasienAntarRuangan_Detail.AddOrUpdate(_m);
                                }

                            }

                            #endregion

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFCatatanPemindahanPasienAntarRuangan(string noreg, string sectionid)
        {
            var tipepelayanan = Request.Cookies["SectionNamaPelayanan"].Value;
            var rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_CatatanPemindahanPasienAntarRuangan.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand("Rpt_CatatanPemindahanPasienAntarRuangan", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables["Rpt_CatatanPemindahanPasienAntarRuangan;1"].SetDataSource(ds[i].Tables[0]);

                i++;
                cmd.Add(new SqlCommand($"Rpt_CatatanPemindahanPasienAntarRuangan_Detail", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_CatatanPemindahanPasienAntarRuangan_Detail;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }

        public string GetEncodeUrlPerawatSerah(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifPemindahanPasienAntarRuangan/get_keyperawatserah";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlPerawatTerima(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifPemindahanPasienAntarRuangan/get_keyperawatterima";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }
    }
}