﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class SkriningGiziDewasaDanGeriatriController : Controller
    {
        // GET: SkriningGiziDewasaDanGeriatri
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.SkriningGiziDewasaDanGeriatri.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.GiziDanGeriatri = IConverter.Cast<SkriningGiziDewasaDanGeriatriViewModel>(item);
                            model.GiziDanGeriatri.Report = 1;
                            var ahligizi = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.KodeAhliGizi);
                            if (ahligizi != null)
                            {
                                model.GiziDanGeriatri.KodeAhliGiziNama = ahligizi.NamaDOkter;
                            }

                            var perawat = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Perawat);
                            if (perawat != null)
                            {
                                model.GiziDanGeriatri.PerawatNama = perawat.NamaDOkter;
                            }
                        }
                        else
                        {
                            item = new SkriningGiziDewasaDanGeriatri();
                            model.GiziDanGeriatri = IConverter.Cast<SkriningGiziDewasaDanGeriatriViewModel>(item);
                            model.GiziDanGeriatri.NoReg = noreg;
                            model.GiziDanGeriatri.NRM = model.NRM;
                            model.GiziDanGeriatri.SectionID = sectionid;
                            model.GiziDanGeriatri.Tanggal = DateTime.Today;
                            model.GiziDanGeriatri.Report = 0;
                        }

                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("SkriningGiziDewasaDanGeriatri")]
        public string SkriningGiziDewasaDanGeriatri_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.GiziDanGeriatri.NoReg = item.GiziDanGeriatri.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.GiziDanGeriatri.SectionID = sectionid;
                            item.GiziDanGeriatri.NRM = item.GiziDanGeriatri.NRM;
                            var model = eEMR.SkriningGiziDewasaDanGeriatri.FirstOrDefault(x => x.SectionID == sectionid && x.NoReg == item.GiziDanGeriatri.NoReg);

                            if (model == null)
                            {
                                model = new SkriningGiziDewasaDanGeriatri();
                                var o = IConverter.Cast<SkriningGiziDewasaDanGeriatri>(item.GiziDanGeriatri);
                                eEMR.SkriningGiziDewasaDanGeriatri.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<SkriningGiziDewasaDanGeriatri>(item.GiziDanGeriatri);
                                eEMR.SkriningGiziDewasaDanGeriatri.AddOrUpdate(model);
                            }

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFSkriningGiziDewasaDanGeriatri(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_SkriningGiziDewasaDanGeriatri.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand("Rpt_SkriningGiziDewasaDanGeriatri", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables["Rpt_SkriningGiziDewasaDanGeriatri;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}