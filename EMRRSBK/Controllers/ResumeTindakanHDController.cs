﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class ResumeTindakanHDController : Controller
    {
        // GET: ResumeTindakanHD
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new GeneralHemodialisaViewModel();

            try
            {
                sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                using (var s = new SIM_Entities())
                {
                    var m = s.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                }

                model.NoReg = noreg;
                model.SectionID = sectionid;
                //model.Tanggal_View = DateTime.Today;

                using (var s = new EMR_Entities())
                {
                    using (var eSIM = new SIM_Entities())
                    {
                        var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == model.Dokter);
                        if (dokter != null)
                        {
                            model.DokterNama = dokter.NamaDOkter;
                        }
                        //DETAIL 1
                        var List = s.ResumeTindakanHemodialisis.Where(x => x.NoReg == noreg).ToList();
                        if (List != null)
                        {
                            model.ResumeTndkanHD_List = new ListDetail<ResumeTindakanHDViewModelDetail>();

                            foreach (var x in List)
                            {
                                var y = IConverter.Cast<ResumeTindakanHDViewModelDetail>(x);
                                //var cek = s.mFinger.Where(xx => xx.userid == x.Nama).FirstOrDefault();
                                //if (cek != null)
                                //{
                                //    if (y.TTDDokter == Convert.ToBase64String(cek.Gambar1) && y.Nama == cek.userid)
                                //    {
                                //        y.SudahRegDkt = 0;
                                //    }
                                //    else
                                //    {
                                //        y.SudahRegDkt = 1;
                                //    }
                                //}
                                //else
                                //{
                                //    y.SudahRegDkt = 1;
                                //}

                                //if (y.Nama != null)
                                //{
                                //    ViewBag.UrlFingerAEWS = GetEncodeUrlAEWS_Dokter(y.Nama, y.NoReg, sectionid, y.No);
                                //    y.Viewbag = ViewBag.UrlFingerAEWS;
                                //}
                                var nama = eSIM.mDokter.FirstOrDefault(d => d.DokterID == x.Petugas);
                                if (nama != null)
                                {
                                    y.PetugasNama = nama.NamaDOkter;
                                }

                                var obat = eSIM.mBarang.FirstOrDefault(d => d.Kode_Barang == x.ObatIntraHD);
                                if (obat != null)
                                {
                                    y.ObatIntraHDNama = obat.Nama_Barang;
                                }

                                model.ResumeTndkanHD_List.Add(false, y);

                            }
                        }
                    }
                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("ResumeTindakanHD")]
        public string ResumeTindakanHD_Post()
        {
            try
            {
                var item = new GeneralHemodialisaViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                            #region ==== D E T A I L
                            if (item.ResumeTndkanHD_List == null) item.ResumeTndkanHD_List = new ListDetail<ResumeTindakanHDViewModelDetail>();
                            item.ResumeTndkanHD_List.RemoveAll(x => x.Remove);

                            foreach (var x in item.ResumeTndkanHD_List)
                            {
                                x.Model.Username = User.Identity.GetUserName();
                                x.Model.SectionID = sectionid;
                                x.Model.NoReg = item.NoReg;
                                x.Model.NRM = item.NRM;
                            }

                            var new_List = item.ResumeTndkanHD_List;
                            var real_List = eEMR.ResumeTindakanHemodialisis.Where(x => x.NoReg == item.NoReg).ToList();
                            // delete | delete where (real_list not_in new_list)
                            foreach (var x in real_List)
                            {
                                var m = new_List.FirstOrDefault(y => y.Model.No == x.No);
                                if (m == null)
                                {
                                    eEMR.ResumeTindakanHemodialisis.Remove(x);
                                }

                            }


                            foreach (var x in new_List)
                            {

                                var _m = real_List.FirstOrDefault(y => y.No == x.Model.No);
                                //new DataColumn("Tanggal", typeof(DateTime));
                                //new DataColumn("Jam", typeof(TimeSpan));
                                // add | add where (new_list not_in raal_list)
                                if (_m == null)
                                {
                                    _m = new ResumeTindakanHemodialisis();
                                    eEMR.ResumeTindakanHemodialisis.Add(IConverter.Cast<ResumeTindakanHemodialisis>(x.Model));
                                    eEMR.SaveChanges();
                                }
                                else
                                {
                                    _m = IConverter.Cast<ResumeTindakanHemodialisis>(x.Model);
                                    eEMR.ResumeTindakanHemodialisis.AddOrUpdate(_m);
                                    eEMR.SaveChanges();
                                }

                            }
                            #endregion

                            //var nomorreport = eEMR.PeresepanHemodialisaRJ_Detail.Where(y => y.NRM == item.NRM).ToList();
                            //if (nomorreport != null)
                            //{
                            //    var ii = 1;
                            //    var zz = 1;
                            //    foreach (var x in nomorreport)
                            //    {
                            //        if (ii == 6)
                            //        {
                            //            ii = 1;
                            //            ++zz;
                            //        }
                            //        x.NomorReport = ii;
                            //        x.NomorReport_Group = zz;
                            //        ++ii;
                            //    }
                            //}

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFPeresepanHemodialisaRJ(string noreg, string section)
        {
            var tipepelayanan = Request.Cookies["SectionNamaPelayanan"].Value;
            var rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"PemantauanAEWSDetail.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand($"PemantauanAEWSDetail", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", section));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"PemantauanAEWSDetail;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                //cmd.Add(new SqlCommand("ImplementasiRisikoJatuhAnak", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NRM", nrm));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["ImplementasiRisikoJatuhAnak;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}