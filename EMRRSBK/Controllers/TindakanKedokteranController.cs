﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class TindakanKedokteranController : Controller
    {
        // GET: TindakanKedokteran
        //public ActionResult Index(string noreg, string sectionid)
        //{
        //    var model = new RegistrasiPasienViewModel();

        //    try
        //    {
        //        using (var eSIM = new SIM_Entities())
        //        {
        //            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
        //            var m = eSIM.VW_IdentitasPasien.FirstOrDefault(x => x.NoReg == noreg);
        //            model.NRM = m.NRM;
        //            model.NoReg = noreg;

        //            using (var eEMR = new EMR_Entities())
        //            {
        //                var item = eEMR.AsesmenTindakanKedokteran.FirstOrDefault(x => x.NoReg == noreg );
        //                if (item != null)
        //                {
        //                    model.tndkkedokteranIGD = IConverter.Cast<TindakanKedokteranIGDViewModel>(item);
        //                    var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.DokterPelaksananTindakan);
        //                    if (dokter != null)
        //                    {
        //                        model.tndkkedokteranIGD.DokterPelaksananTindakanNama = dokter.NamaDOkter;
        //                    }

        //                    var menerangkan = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Merangkan);
        //                    if (menerangkan != null)
        //                    {
        //                        model.tndkkedokteranIGD.MerangkanNama = menerangkan.NamaDOkter;
        //                    }
        //                    model.tndkkedokteranIGD.Penerima_Tgl = DateTime.Today;
        //                    model.tndkkedokteranIGD.Penolakan_Tgl = DateTime.Today;
        //                    model.tndkkedokteranIGD.Persetujuan_Tgl = DateTime.Today;
        //                    model.tndkkedokteranIGD.Tanggal = DateTime.Today;
        //                    model.tndkkedokteranIGD.NamaPasien = m.NamaPasien;
        //                    model.tndkkedokteranIGD.NamaPasien1 = m.NamaPasien;
        //                    model.tndkkedokteranIGD.TglLahir = m.TglLahir;
        //                    model.tndkkedokteranIGD.TglLahir1 = m.TglLahir;
        //                    model.tndkkedokteranIGD.JenisKelamin = m.JenisKelamin;
        //                    model.tndkkedokteranIGD.JenisKelamin1 = m.JenisKelamin;
        //                    model.tndkkedokteranIGD.AlamatPasien = m.Alamat;
        //                    model.tndkkedokteranIGD.AlamatPasien1 = m.Alamat;
                            
        //                }
        //                else
        //                {
        //                    item = new AsesmenTindakanKedokteran();
        //                    model.tndkkedokteranIGD = IConverter.Cast<TindakanKedokteranIGDViewModel>(item);
        //                    model.tndkkedokteranIGD.Penerima_Tgl = DateTime.Today;
        //                    model.tndkkedokteranIGD.Penolakan_Tgl = DateTime.Today;
        //                    model.tndkkedokteranIGD.Penolakan_TglLahir = m.TglLahir;
        //                    model.tndkkedokteranIGD.Persetujuan_Tgl = DateTime.Today;
        //                    model.tndkkedokteranIGD.Persetujuan_TglLahir = m.TglLahir;
        //                    model.tndkkedokteranIGD.Tanggal = DateTime.Today;
        //                    model.tndkkedokteranIGD.NamaPasien = m.NamaPasien;
        //                    model.tndkkedokteranIGD.NamaPasien1 = m.NamaPasien;
        //                    model.tndkkedokteranIGD.TglLahir = m.TglLahir;
        //                    model.tndkkedokteranIGD.TglLahir1 = m.TglLahir;
        //                    model.tndkkedokteranIGD.JenisKelamin = m.JenisKelamin;
        //                    model.tndkkedokteranIGD.JenisKelamin1 = m.JenisKelamin;
        //                    model.tndkkedokteranIGD.AlamatPasien = m.Alamat;
        //                    model.tndkkedokteranIGD.AlamatPasien1 = m.Alamat;

        //                }

        //                var dokumen_temp = eEMR.trTemplate.Where(e => e.DokumenID == "TindakanKedokteran").ToList();

        //                ViewBag.DokumenID = "TindakanKedokteran";
        //                ViewBag.NamaDokumen = "Tindakan Kedokteran";
        //                model.tndkkedokteranIGD.ListTemplate = new List<SelectItemListTindakanKedokteran>();
        //                foreach (var list in dokumen_temp)
        //                {
        //                    model.tndkkedokteranIGD.ListTemplate.Add(new SelectItemListTindakanKedokteran()
        //                    {
        //                        Value = list.NoReg,
        //                        Text = list.NamaTemplate
        //                    });
        //                }

        //            }

        //        }
        //    }
        //    catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
        //    catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
        //    if (TempData["Status"] != null)
        //    {
        //        ViewBag.Status = TempData["Status"].ToString();
        //        ViewBag.Message = TempData["Message"].ToString();
        //    }

        //    ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
        //    if (Request.IsAjaxRequest())
        //        return PartialView(model);
        //    else
        //        return View(model);
        //}

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string noreg, string copy)
        {
            var model = new RegistrasiPasienViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var m = sim.VW_IdentitasPasien.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    model.NoReg = noreg;
                    var dokumen = s.AsesmenTindakanKedokteran.FirstOrDefault(x => x.NoReg == copy);
                    if (dokumen != null)
                    {
                        model.tndkkedokteranIGD = IConverter.Cast<TindakanKedokteranIGDViewModel>(dokumen);
                        model.NoReg = noreg;
                        var dokter = sim.mDokter.FirstOrDefault(x => x.DokterID == model.tndkkedokteranIGD.DokterPelaksananTindakan);
                        if (dokter != null)
                        {
                            model.tndkkedokteranIGD.DokterPelaksananTindakanNama = dokter.NamaDOkter;
                        }
                        model.tndkkedokteranIGD.Penerima_Tgl = DateTime.Today;
                        model.tndkkedokteranIGD.Penolakan_Tgl = DateTime.Today;
                        model.tndkkedokteranIGD.Persetujuan_Tgl = DateTime.Today;
                        model.tndkkedokteranIGD.Tanggal = DateTime.Today;
                        model.NamaPasien = m.NamaPasien;
                        model.tndkkedokteranIGD.NamaPasien1 = m.NamaPasien;
                        model.tndkkedokteranIGD.TglLahir = m.TglLahir;
                        model.tndkkedokteranIGD.TglLahir1 = m.TglLahir;
                        model.tndkkedokteranIGD.JenisKelamin = m.JenisKelamin;
                        model.tndkkedokteranIGD.JenisKelamin1 = m.JenisKelamin;
                        model.tndkkedokteranIGD.AlamatPasien = m.Alamat;
                        model.tndkkedokteranIGD.AlamatPasien1 = m.Alamat;

                        model.tndkkedokteranIGD.PemberiInformasi = "";
                        model.tndkkedokteranIGD.DokterPelaksananTindakan = "";
                        model.tndkkedokteranIGD.Persetujuan_Nama = "";
                        model.tndkkedokteranIGD.Persetujuan_JK = "";
                        model.tndkkedokteranIGD.Persetujuan_Alamat = "";
                        model.tndkkedokteranIGD.Persetujuan_Saksi1 = "";
                        model.tndkkedokteranIGD.Persetujuan_Saksi2 = "";
                        model.tndkkedokteranIGD.Penolakan_Nama = "";
                        model.tndkkedokteranIGD.Penolakan_JK = "";
                        model.tndkkedokteranIGD.Penolakan_Pernyataan = "";
                        model.tndkkedokteranIGD.Penolakan_Terhadap = "";
                        model.tndkkedokteranIGD.Penolakan_Saksi1 = "";
                        model.tndkkedokteranIGD.Penolakan_Saksi2 = "";
                    }
                    else
                    {
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                        model.tndkkedokteranIGD.Penerima_Tgl = DateTime.Today;
                        model.tndkkedokteranIGD.Penolakan_Tgl = DateTime.Today;
                        model.tndkkedokteranIGD.Penolakan_TglLahir = m.TglLahir;
                        model.tndkkedokteranIGD.Persetujuan_Tgl = DateTime.Today;
                        model.tndkkedokteranIGD.Persetujuan_TglLahir = m.TglLahir;
                        model.tndkkedokteranIGD.Tanggal = DateTime.Today;
                        model.tndkkedokteranIGD.NamaPasien = m.NamaPasien;
                        model.tndkkedokteranIGD.NamaPasien1 = m.NamaPasien;
                        model.tndkkedokteranIGD.TglLahir = m.TglLahir;
                        model.tndkkedokteranIGD.TglLahir1 = m.TglLahir;
                        model.tndkkedokteranIGD.JenisKelamin = m.JenisKelamin;
                        model.tndkkedokteranIGD.JenisKelamin1 = m.JenisKelamin;
                        model.tndkkedokteranIGD.AlamatPasien = m.Alamat;
                        model.tndkkedokteranIGD.AlamatPasien1 = m.Alamat;

                        model.tndkkedokteranIGD.PemberiInformasi = "";
                        model.tndkkedokteranIGD.DokterPelaksananTindakan = "";
                        model.tndkkedokteranIGD.Persetujuan_Nama = "";
                        model.tndkkedokteranIGD.Persetujuan_JK = "";
                        model.tndkkedokteranIGD.Persetujuan_Alamat = "";
                        model.tndkkedokteranIGD.Persetujuan_Saksi1 = "";
                        model.tndkkedokteranIGD.Persetujuan_Saksi2 = "";
                        model.tndkkedokteranIGD.Penolakan_Nama = "";
                        model.tndkkedokteranIGD.Penolakan_JK = "";
                        model.tndkkedokteranIGD.Penolakan_Pernyataan = "";
                        model.tndkkedokteranIGD.Penolakan_Terhadap = "";
                        model.tndkkedokteranIGD.Penolakan_Saksi1 = "";
                        model.tndkkedokteranIGD.Penolakan_Saksi2 = "";
                    }

                    var dokumen_temp = s.trTemplate.Where(e => e.DokumenID == "TindakanKedokteran").ToList();

                    ViewBag.DokumenID = "TindakanKedokteran";
                    ViewBag.NamaDokumen = "Tindakan Kedokteran";
                    model.tndkkedokteranIGD.ListTemplate = new List<SelectItemListTindakanKedokteran>();
                    foreach (var list in dokumen_temp)
                    {
                        model.tndkkedokteranIGD.ListTemplate.Add(new SelectItemListTindakanKedokteran()
                        {
                            Value = list.NoReg,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("Index")]
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new TindakanKedokteranIGDViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var emr = new EMR_Entities())
                {
                    var sectionida = Request.Cookies["SectionIDPelayanan"].Value;
                    var identitas = sim.VW_IdentitasPasien.FirstOrDefault(x => x.NoReg == noreg);

                    model.NoReg = noreg;
                    model.NamaPasien = identitas.NamaPasien;
                    model.TglLahir = identitas.TglLahir;
                    model.NRM = identitas.NRM;
                    model.Tanggal = DateTime.Today;
                    ViewBag.NoBukti = noreg;
                }

            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        public string ListAsesmenTindakanKedokteran(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMR_Entities())
                    {
                        var noreg = filter[1];
                        IQueryable<AsesmenTindakanKedokteran> p = s.AsesmenTindakanKedokteran.Where(x => x.NoReg == noreg);
                        //p = p.Where($"NoRegistrasi.Contains(@0)", filter[2]);
                        //p = p.Where($"NRM.Contains(@0)", filter[1]);
                        //p = p.Where($"NoResep.Contains(@0)", filter[4]);
                        var totalcount = p.Count();
                        var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.DESC ? "ASC" : "DESC")}").Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                        result = new ResultSS(models.Length, null, totalcount, pageIndex);
                        var datas = new List<TindakanKedokteranIGDViewModel>();
                        foreach (var x in models.ToList())
                        {
                            var m = IConverter.Cast<TindakanKedokteranIGDViewModel>(x);
                            if (x.Persetujuan_Tgl != null)
                            {
                                m.Tanggal_View = x.Persetujuan_Tgl.Value.ToString("yyyy/MM/dd");
                            }
                            else {
                                m.Tanggal_View = "-";
                            }

                            datas.Add(m);
                        }
                        result.Data = datas;
                    }
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public string editInforment(string id, string noreg)
        {
            try
            {
                var model = new TindakanKedokteranIGDViewModel();
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMR_Entities())
                    {
                        int idnya = int.Parse(id);
                        var dokumen = s.AsesmenTindakanKedokteran.FirstOrDefault(x => x.No == idnya && x.NoReg == noreg);
                        model = IConverter.Cast<TindakanKedokteranIGDViewModel>(dokumen);

                        var dokpelaksana = sim.mDokter.FirstOrDefault(x => x.DokterID == model.DokterPelaksananTindakan);
                        if (dokpelaksana != null)
                        {
                            model.DokterPelaksananTindakanNama = dokpelaksana.NamaDOkter;
                        }

                        var pemberi = sim.mDokter.FirstOrDefault(x => x.DokterID == model.PemberiInformasi);
                        if (pemberi != null)
                        {
                            model.PemberiInformasiNama = pemberi.NamaDOkter;
                        }

                        var menerangkan = sim.mDokter.FirstOrDefault(x => x.DokterID == model.Merangkan);
                        if (menerangkan != null)
                        {
                            model.MerangkanNama = menerangkan.NamaDOkter;
                        }
                    }
                }

                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = model,
                    Message = ""

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost, ValidateInput(false)]
        [ActionName("SecondCreate")]
        public string SecondCreate(TindakanKedokteranIGDViewModel item, string img, string imgttd)
        {
            try
            {
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    //if (item.DPJP == null) return JsonHelper.JsonMsgError("Dokter DPJP tidak boleh kosong.");
                    //if (item.DPJP == null) throw new Exception("Dokter tidak boleh kosong.");

                    var model = s.AsesmenTindakanKedokteran.FirstOrDefault(x => x.NoReg == item.NoReg);
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var activity = "";
                    if (item._METHOD == "CREATE")
                    {

                        var o = IConverter.Cast<AsesmenTindakanKedokteran>(item);
                        o.NoReg = item.NoReg;
                        o.NRM = item.NRM;
                        o.SectionID = sectionid;
                        s.AsesmenTindakanKedokteran.Add(o);
                    }
                    else if (item._METHOD == "UPDATE")
                    {
                        model = IConverter.Cast<AsesmenTindakanKedokteran>(item);
                        model.No = item.No;
                        model.NoReg = item.NoReg;
                        model.NRM = item.NRM;
                        model.SectionID = sectionid;
                        s.AsesmenTindakanKedokteran.AddOrUpdate(model);

                        //if (item.templateName != null)
                        //{
                        //    var check_temp = s.trDokumenTemplate.FirstOrDefault(x => x.NamaTemplate == item.templateName);
                        //    if (check_temp == null)
                        //    {
                        //        var temp = new trDokumenTemplate();
                        //        temp.NoBukti = item.NoBukti;
                        //        temp.NamaTemplate = item.templateName;
                        //        temp.DokterID = item.DPJP;
                        //        temp.SectionID = Request.Cookies["EMRSectionIDPelayanan"].Value;
                        //        temp.DokumenID = "CPPT";
                        //        s.trDokumenTemplate.Add(temp);
                        //    }
                        //}
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoReg}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string Batal(int id, string noreg)
        {
            try
            {
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var m = s.AsesmenTindakanKedokteran.FirstOrDefault(x => x.No == id && x.NoReg == noreg);
                    s.AsesmenTindakanKedokteran.Remove(m);
                    result = new ResultSS(s.SaveChanges());

                }
                return JsonHelper.JsonMsgDelete(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFTindakanKedokteran(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["SectionNamaPelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"AssTindakanKedokteran.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"AssTindakanKedokteran", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"AssTindakanKedokteran;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}