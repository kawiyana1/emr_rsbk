﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class DokumenRekamMedisController : Controller
    {
        #region === INDEX & DETAIL
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ManajemenDokumen(string nrm)
        {
            PasienViewModel model;

            using (var S = new SIM_Entities())
            {
                var p = S.mPasien.Where(x => x.NRM == nrm).FirstOrDefault();
                if (p == null) return HttpNotFound();
                model = IConverter.Cast<PasienViewModel>(p);
            }
            return View(model);
        }
        #endregion

        #region ===== LIST PASIEN & DOKUMEN + AUTO ID

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<mPasien> proses = s.mPasien;
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(mPasien.NRM)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(mPasien.NamaPasien)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(mPasien.TglLahir)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(mPasien.Alamat)}.Contains(@0)", filter[3]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PasienViewModel>(x));
                    foreach (var x in m)
                    {
                        x.TanggalLahir = x.TglLahir.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListDokumen(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    IQueryable<vw_DokumenRekamMedis> proses = s.vw_DokumenRekamMedis;
                    proses = proses.Where($"{nameof(vw_DokumenRekamMedis.NRM)}.Contains(@0)", filter[99]);
                    if (filter[14] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[12]))
                        {
                            proses = proses.Where("TglReg >= @0", DateTime.Parse(filter[12]).AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[13]))
                        {
                            proses = proses.Where("TglReg <= @0", DateTime.Parse(filter[13]));
                        }

                    }
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(vw_DokumenRekamMedis.NoBukti)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(vw_DokumenRekamMedis.NoReg)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(vw_DokumenRekamMedis.TglReg)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(vw_DokumenRekamMedis.JenisKerjasama)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(vw_DokumenRekamMedis.SectionName)}.Contains(@0)", filter[4]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<DokumenRekamMedisViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Pelayanan = x.RawatInap == true ? "RAWAT INAP" : "RAWAT JALAN";
                        x.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }



        public string AutoId()
        {
            try
            {
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var m = s.AutoNumber_DokumenRekamMedis().FirstOrDefault();
                    if (m == null) return JsonHelper.JsonMsgError("Data tidak ditemukan");
                    result = new ResultSS(m) { IsSuccess = true };
                }
                return JsonConvert.SerializeObject(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region === CREATE
        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string nrm)
        {
            var model = new DokumenRekamMedisViewModel();

            using (var S = new SIM_Entities())
            {
                var p = S.mPasien.Where(x => x.NRM == nrm).FirstOrDefault();
                if (p == null) return HttpNotFound();
                model.NRM = p.NRM;
                model.NamaPasien = p.NamaPasien;
                model.Alamat = p.Alamat;
                model.NoTelepon = p.Phone;
                model.JenisKelamin = p.JenisKelamin;
                model.TanggalDibuat = DateTime.Today;
                model.TglReg = DateTime.Today;
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public ActionResult Create_Post(HttpPostedFileBase[] FilePDF)
        {
            try
            {

                var item = new DokumenRekamMedisViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var s = new EMR_Entities())
                    {

                        var m = IConverter.Cast<trDokumenRekamMedis>(item);
                        m.RawatInap = (item.Pelayanan == "RI") ? true : false;
                        m.RawatJalan = (item.Pelayanan == "RJ") ? true : false;
                        m.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                        m.JenisKerjasamaID = 1;

                        using (var ss = new SIM_Entities())
                        {
                            var jk = ss.SIMmJenisKerjasama.Where(x => x.JenisKerjasama == item.JenisKerjasama).FirstOrDefault();
                            m.JenisKerjasamaID = jk.JenisKerjasamaID;
                        }

                        s.trDokumenRekamMedis.Add(m);

                        var i = 1;
                        foreach (HttpPostedFileBase file in FilePDF)
                        {
                            string OriginalPath = "~/FileManager/DokumenRM/";

                            string subPath = item.TglReg.Year.ToString() + "/" + item.Pelayanan;

                            bool exists = System.IO.Directory.Exists(Server.MapPath(OriginalPath + subPath));

                            if (!exists)
                                System.IO.Directory.CreateDirectory(Server.MapPath(OriginalPath + subPath));

                            if (file != null)
                            {
                                var InputFileName = Path.GetFileName(file.FileName);
                                var ServerSavePath = Path.Combine(Server.MapPath($"{OriginalPath + subPath}/") + item.NRM + "_" + i + "_" + InputFileName);
                                file.SaveAs(ServerSavePath);

                                s.trDokumenRekamMedisDetail.Add(new trDokumenRekamMedisDetail()
                                {
                                    NoBukti = item.NoBukti,
                                    NoUrut = Convert.ToInt16(i),
                                    FileName = InputFileName,
                                    FilePath = ServerSavePath,
                                    TanggalDibuat = item.TanggalDibuat
                                });

                                i++;
                            }

                        }

                        result = new ResultSS(s.SaveChanges());

                        ViewBag.AlertStatus = new
                        {
                            isStatus = true,
                            name = "success",
                            mssg = "Hasil Baca Penunjang berhasil disimpan"
                        };
                        return RedirectToAction("Index", "DokumenRekamMedis/ManajemenDokumen", new { nrm = item.NRM });
                    }

                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                ViewBag.AlertStatus = new
                {
                    isStatus = true,
                    name = "error",
                    mssg = "Hasil Baca Penunjang gagal disimpan"
                };
                return RedirectToAction("Index", "DokumenRekamMedis/ManajemenDokumen", new { nrm = item.NRM });
            }
            catch (SqlException ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            catch (DbEntityValidationException ex)
            {
                TempData["Status"] = "danger";
                TempData["Message"] = string.Join("|",
                    ex.EntityValidationErrors.First().ValidationErrors.Select(x => x.ErrorMessage));
            }
            catch (Exception ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            ViewBag.AlertStatus = new
            {
                isStatus = true,
                name = "error",
                mssg = "Hasil Baca Penunjang gagal disimpan"
            };
            return RedirectToAction("Index", "DokumenRekamMedis");
        }
        #endregion

        #region === DETAIL
        [HttpGet]
        [ActionName("Detail")]
        public string Detail_Get(string id = "210319DRM-00001")
        {
            try
            {
                var model = new DokumenRekamMedisViewModel();
                using (var s = new EMR_Entities())
                {
                    var m = s.trDokumenRekamMedis.FirstOrDefault(x => x.NoBukti == id);
                    model = IConverter.Cast<DokumenRekamMedisViewModel>(m);
                    model.TanggalView = m.TglReg.Value.ToString("dd/MM/yyyy");
                    model.Pelayanan = m.RawatInap == true ? "RAWAT INAP" : "RAWAT JALAN";
                    model.JenisKerjasama = "UMUM";

                    model.FileUpload = new ListDetail<DokumenRekamMedisDetailViewModel>();
                    var file_upload = s.trDokumenRekamMedisDetail.Where(x => x.NoBukti == id).ToList();

                    var i = 1;
                    foreach (var x in file_upload)
                    {
                        string PathHost = ConfigurationManager.AppSettings["AppUrlName"];
                        string OriginalPath = "/FileManager/DokumenRM/";
                        string OriginalPathYear = m.TglReg.Value.Year.ToString();
                        string OriginalPathPelayanan = m.RawatInap == true ? "RI" : "RJ";
                        string OriginalNameFile = m.NRM + "_" + i + "_";

                        var y = IConverter.Cast<DokumenRekamMedisDetailViewModel>(x);
                        y.FilePath = PathHost + OriginalPath + OriginalPathYear + "/" + OriginalPathPelayanan + "/" + OriginalNameFile + y.FileName;
                        model.FileUpload.Add(false, y);

                        i++;
                    }

                }

                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = model,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion
    }

}