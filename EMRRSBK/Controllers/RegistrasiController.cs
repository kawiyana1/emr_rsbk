﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Configuration;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class RegistrasiController : Controller
    {
        // GET: Registrasi
        public ActionResult Index(string noreg)
        {
            var model = new RegistrasiPasienViewModel();
            var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.VW_Registrasi.FirstOrDefault(x => x.NoReg == noreg);
                    if (m != null)
                    {
                        model = IConverter.Cast<RegistrasiPasienViewModel>(m);
                        model.TglReg_View = model.TglReg.ToString("dd/MM/yyyy");
                        model.Tanggal_View = model.Tanggal_View;
                        model.Jam_View = model.Jam_View;
                        model.NRM = m.NRM;
                        model.NamaPasien = m.NamaPasien;
                        model.TglLahir = m.TglLahir;
                        model.JenisKelamin = m.JenisKelamin;
                        model.Alamat = m.Alamat;
                        model.Telp = m.PenanggungTelp;
                        model.UmurThn = m.UmurThn;
                        //model.JamMasuk = m.JamReg.ToString("HH\":\"mm");
                        //model.JamMasuk = m.JamReg.ToString("HH\":\"mm");
                        //model.Nomor = nomor;
                    }
                }

                using (var s = new EMR_Entities())
                {

                    using (var eSIM = new SIM_Entities())
                    {

                        #region ==== C P P T  G E T
                        model.CPPT = new SOAPViewModel();
                        model.CPPT.SectionID = sectionid;
                        model.CPPT.NoReg = noreg;
                        model.CPPT.Tanggal = DateTime.Today;
                        model.CPPT.Jam = DateTime.Now.TimeOfDay;
                        //model.CPPT.Nomor = nomor;
                        #endregion

                        #region ==== S U R A T  P E R N Y A T A A N 
                        model.SuratPernyataan = new RegSuratPernyataanViewModel();

                        var srtpertnyanreg = s.SuratPernyataanRegistrasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (srtpertnyanreg != null)
                        {
                            model.SuratPernyataan = IConverter.Cast<RegSuratPernyataanViewModel>(srtpertnyanreg);
                            //model.SuratPernyataan.Nomor = nomor;

                            var _dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == model.SuratPernyataan.Perawat);

                            if (_dokter != null)
                            {
                                model.SuratPernyataan.PerawatNama = _dokter.NamaDOkter;
                            }
                            //model.SuratPernyataan.Nomor = nomor;
                            model.SuratPernyataan.Report = 1;
                            model.SuratPernyataan.NoReg = noreg;
                            model.SuratPernyataan.NRM = model.NRM;
                            model.SuratPernyataan.SectionID = sectionid;
                            model.SuratPernyataan.NamaPasien1 = model.NamaPasien;
                            model.SuratPernyataan.NamaPasien = srtpertnyanreg.Nama;
                            model.SuratPernyataan.TglLahir = model.TglLahir;
                            model.SuratPernyataan.JenisKelamin = model.JenisKelamin;
                            model.SuratPernyataan.AlamatPasien = model.Alamat;
                            model.SuratPernyataan.Umur = model.UmurThn;
                            model.SuratPernyataan.NoTelp = model.SuratPernyataan.NoTelp;

                            var fingerpasien = s.SuratPernyataanRegistrasi.Where(x => x.NRM == model.NRM && x.TTDPasien == null).FirstOrDefault();
                            if (fingerpasien != null)
                            {
                                model.SuratPernyataan.SudahRegPasien = 1;
                            }
                            else
                            {
                                model.SuratPernyataan.SudahRegPasien = 0;
                            }

                            var fingerpegawe = s.SuratPernyataanRegistrasi.Where(x => x.NRM == model.NRM && x.TTDpetugas == null).FirstOrDefault();
                            if (fingerpegawe != null)
                            {
                                model.SuratPernyataan.SudahRegPegawai = 1;
                            }
                            else
                            {
                                model.SuratPernyataan.SudahRegPegawai = 0;
                            }

                            ViewBag.UrlFingerSrtPernyataan = GetEncodeUrlSrtPernyataan(model.NRM, model.NoReg, sectionid);
                            if (model.SuratPernyataan.Perawat != null)
                            {
                                ViewBag.UrlFingerSrtPernyataanDokter = GetEncodeUrlSrtPernyataan_Dokter(model.SuratPernyataan.Perawat, model.NoReg, sectionid);
                            }
                        }
                        else
                        {
                            srtpertnyanreg = new SuratPernyataanRegistrasi();
                            model.SuratPernyataan = IConverter.Cast<RegSuratPernyataanViewModel>(srtpertnyanreg);
                            //model.SuratPernyataan.Nomor = nomor;
                            model.SuratPernyataan.SectionID = sectionid;
                            model.SuratPernyataan.Report = 0;
                            model.SuratPernyataan.NoReg = noreg;
                            model.SuratPernyataan.NRM = model.NRM;
                            model.SuratPernyataan.NamaPasien1 = model.NamaPasien;
                            model.SuratPernyataan.TglLahir = model.TglLahir;
                            model.SuratPernyataan.JenisKelamin = model.JenisKelamin;
                            model.SuratPernyataan.AlamatPasien = model.Alamat;
                            model.SuratPernyataan.Umur = model.UmurThn;
                            model.SuratPernyataan.NoTelp = model.Telp;
                            ViewBag.UrlFingerSrtPernyataan = GetEncodeUrlSrtPernyataan(model.NRM, model.NoReg, sectionid);
                            ViewBag.UrlFingerSrtPernyataanDokter = GetEncodeUrlSrtPernyataan_Dokter(model.SuratPernyataan.Perawat, model.NoReg, sectionid);
                        }

                        #endregion

                        #region ==== S U R A T  P E R N Y A T A A N  K E N A I K A N  K E L A S
                        model.SuratPernyataanKenaikanKelas = new RegSuratPernyataanKenaikanKelasViewModel();

                        var srtpertnyannaikklsreg = s.SuratPernyataanKenaikanKelasRegistrasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (srtpertnyannaikklsreg != null)
                        {
                            model.SuratPernyataanKenaikanKelas = IConverter.Cast<RegSuratPernyataanKenaikanKelasViewModel>(srtpertnyannaikklsreg);
                            //model.SuratPernyataanKenaikanKelas.Nomor = nomor;
                            model.SuratPernyataanKenaikanKelas.Report = 1;
                            model.SuratPernyataanKenaikanKelas.Pasien = srtpertnyannaikklsreg.Nama;
                            model.SuratPernyataanKenaikanKelas.TglLahir = model.TglLahir;
                            model.SuratPernyataanKenaikanKelas.AlamatPasien = model.Alamat;
                            model.SuratPernyataanKenaikanKelas.NoTelp = model.SuratPernyataanKenaikanKelas.NoTelp;

                            var _dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == model.SuratPernyataanKenaikanKelas.Perawat);
                            if (_dokter != null)
                            {
                                model.SuratPernyataanKenaikanKelas.PerawatNama = _dokter.NamaDOkter;
                            }

                            var fingerpasien = s.SuratPernyataanKenaikanKelasRegistrasi.Where(x => x.NRM == model.NRM && x.TTDPasien == null).FirstOrDefault();
                            if (fingerpasien != null)
                            {
                                model.SuratPernyataanKenaikanKelas.SudahRegPasien = 1;
                            }
                            else
                            {
                                model.SuratPernyataanKenaikanKelas.SudahRegPasien = 0;
                            }

                            var fingerpegawe = s.SuratPernyataanKenaikanKelasRegistrasi.Where(x => x.NRM == model.NRM && x.TTDPetugas == null).FirstOrDefault();
                            if (fingerpegawe != null)
                            {
                                model.SuratPernyataanKenaikanKelas.SudahRegPegawai = 1;
                            }
                            else
                            {
                                model.SuratPernyataanKenaikanKelas.SudahRegPegawai = 0;
                            }

                            ViewBag.UrlFingerSrtKenaikanKls = GetEncodeUrlSrtPernyataanKenaikanKls(model.NRM, model.NoReg, sectionid);
                            if (model.SuratPernyataanKenaikanKelas.Perawat != null)
                            {
                                ViewBag.UrlFingerSrtKenaikanKlsDokter = GetEncodeUrlSrtPernyataanKenaikanKls_Dokter(model.SuratPernyataanKenaikanKelas.Perawat, model.NoReg, sectionid);
                            }
                        }
                        else
                        {
                            srtpertnyannaikklsreg = new SuratPernyataanKenaikanKelasRegistrasi();
                            model.SuratPernyataanKenaikanKelas = IConverter.Cast<RegSuratPernyataanKenaikanKelasViewModel>(srtpertnyannaikklsreg);
                            //model.SuratPernyataanKenaikanKelas.Nomor = nomor;
                            model.SuratPernyataanKenaikanKelas.NoTelp = model.Telp;
                            model.SuratPernyataanKenaikanKelas.TglLahir = model.TglLahir == null ? null : model.TglLahir;
                            model.SuratPernyataanKenaikanKelas.AlamatPasien = model.Alamat;
                            model.SuratPernyataanKenaikanKelas.Report = 0;
                            model.SuratPernyataanKenaikanKelas.NoReg = noreg;
                            model.SuratPernyataanKenaikanKelas.NRM = model.NRM;
                            model.SuratPernyataanKenaikanKelas.SectionID = sectionid;
                            model.SuratPernyataanKenaikanKelas.Tanggal = DateTime.Today;
                            ViewBag.UrlFingerSrtKenaikanKls = GetEncodeUrlSrtPernyataanKenaikanKls(model.NRM, model.NoReg, sectionid);
                            ViewBag.UrlFingerSrtKenaikanKlsDokter = GetEncodeUrlSrtPernyataanKenaikanKls_Dokter(model.SuratPernyataanKenaikanKelas.Perawat, model.NoReg, sectionid);
                        }

                        #endregion

                        #region ==== P E R S E T U J U A N  R A W A T  I N A P
                        model.PDirawatICU = new PersetujuanDirawatICUViewModel();

                        var persetujuanriadmisi = s.PersetujuanDirawatDiICU.FirstOrDefault(x => x.NoReg == noreg);
                        if (persetujuanriadmisi != null)
                        {
                            model.PDirawatICU = IConverter.Cast<PersetujuanDirawatICUViewModel>(persetujuanriadmisi);
                            //model.PDirawatICU.Nomor = nomor;
                            model.PDirawatICU.Report = 1;
                            model.PDirawatICU.Nama = model.PDirawatICU.Nama;
                            model.PDirawatICU.Umur = model.PDirawatICU.Umur;
                            model.PDirawatICU.JenisKelamin = model.PDirawatICU.JenisKelamin;
                            model.PDirawatICU.Alamat = model.PDirawatICU.Alamat;
                            model.PDirawatICU.NamaPasien = model.NamaPasien;
                            model.PDirawatICU.Umur = model.PDirawatICU.Umur;
                            model.PDirawatICU.Jenis = model.PDirawatICU.Jenis;
                            model.PDirawatICU.AlamatPasien = model.Alamat;
                            model.PDirawatICU.Nama = model.PDirawatICU.Nama;
                            model.PDirawatICU.Umur = model.PDirawatICU.Umur;
                            model.PDirawatICU.JenisKelamin = model.JenisKelamin;
                            model.PDirawatICU.Alamat = model.PDirawatICU.Alamat;
                            model.PDirawatICU.TglLahir = model.TglLahir;

                            var _dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == model.PDirawatICU.Dokter);
                            if (_dokter != null)
                            {
                                model.PDirawatICU.DokterNama = _dokter.NamaDOkter;
                            }

                            var saksi1 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == model.PDirawatICU.Saksi1);
                            if (saksi1 != null)
                            {
                                model.PDirawatICU.Saksi1Nama = saksi1.NamaDOkter;
                            }

                            var saksi2s = eSIM.mDokter.FirstOrDefault(x => x.DokterID == model.PDirawatICU.Saksi2);
                            if (saksi2s != null)
                            {
                                model.PDirawatICU.Saksi2Nama = saksi2s.NamaDOkter;
                            }

                            var fingerpasien = s.PersetujuanDirawatDiICU.Where(x => x.NRM == model.NRM && x.TTDPasien == null).FirstOrDefault();
                            if (fingerpasien != null)
                            {
                                model.PDirawatICU.SudahRegPasien = 1;
                            }
                            else
                            {
                                model.PDirawatICU.SudahRegPasien = 0;
                            }

                            var fingerpegawe = s.PersetujuanDirawatDiICU.Where(x => x.NRM == model.NRM && x.TTDpetugas == null).FirstOrDefault();
                            if (fingerpegawe != null)
                            {
                                model.PDirawatICU.SudahRegPegawai = 1;
                            }
                            else
                            {
                                model.PDirawatICU.SudahRegPegawai = 0;
                            }

                            var fingersaksi1 = s.PersetujuanDirawatDiICU.Where(x => x.NRM == model.NRM && x.TTDSaksi1 == null).FirstOrDefault();
                            if (fingersaksi1 != null)
                            {
                                model.PDirawatICU.SudahRegSaksi1 = 1;
                            }
                            else
                            {
                                model.PDirawatICU.SudahRegSaksi1 = 0;
                            }

                            var fingersaksi2 = s.PersetujuanDirawatDiICU.Where(x => x.NRM == model.NRM && x.TTDSaksi2 == null).FirstOrDefault();
                            if (fingersaksi2 != null)
                            {
                                model.PDirawatICU.SudahRegSaksi2 = 1;
                            }
                            else
                            {
                                model.PDirawatICU.SudahRegSaksi2 = 0;
                            }

                            ViewBag.UrlFingerPersetujuanRanap = GetEncodeUrlSrtPernyataanKenaikanKls(model.NRM, model.NoReg, sectionid);
                            if (model.PDirawatICU.Dokter != null)
                            {
                                ViewBag.UrlFingerPersetujuanRanapDokter = GetEncodeUrlSrtPersetujuanRanap_Dokter(model.PDirawatICU.Dokter, model.NoReg, sectionid);
                            }
                            if (model.PDirawatICU.Saksi1 != null)
                            {
                                ViewBag.UrlFingerPersetujuanRanapSaksi1 = GetEncodeUrlSrtPersetujuanRanap_Saksi1(model.PDirawatICU.Saksi1, model.NoReg, sectionid);
                            }
                            if (model.PDirawatICU.Saksi2 != null)
                            {
                                ViewBag.UrlFingerPersetujuanRanapSaksi2 = GetEncodeUrlSrtPersetujuanRanap_Saksi2(model.PDirawatICU.Saksi2, model.NoReg, sectionid);
                            }
                        }
                        else
                        {
                            persetujuanriadmisi = new PersetujuanDirawatDiICU();
                            model.PDirawatICU = IConverter.Cast<PersetujuanDirawatICUViewModel>(persetujuanriadmisi);
                            //model.PDirawatICU.Nomor = nomor;
                            var _dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == model.PDirawatICU.Dokter);
                            if (_dokter != null)
                            {
                                model.PDirawatICU.DokterNama = _dokter.NamaDOkter;
                            }
                            model.PDirawatICU.TglLahir = model.TglLahir;
                            model.PDirawatICU.Nama = model.PDirawatICU.Nama;
                            model.PDirawatICU.NamaPasien = model.NamaPasien;
                            model.PDirawatICU.Umur = model.UmurThn != 0 ? 0 : model.PDirawatICU.Umur;
                            model.PDirawatICU.JenisKelamin = model.PDirawatICU.JenisKelamin;
                            model.PDirawatICU.Alamat = model.PDirawatICU.Alamat;
                            model.PDirawatICU.JenisKelamin = model.JenisKelamin;
                            model.PDirawatICU.AlamatPasien = model.Alamat;
                            model.PDirawatICU.Report = 0;
                            model.PDirawatICU.NoReg = noreg;
                            model.PDirawatICU.NRM = model.NRM;
                            model.PDirawatICU.SectionID = sectionid;
                            model.PDirawatICU.Tanggal = DateTime.Today;
                            ViewBag.UrlFingerPersetujuanRanap = GetEncodeUrlSrtPersetujuanRanap(model.NRM, model.NoReg, sectionid);
                            ViewBag.UrlFingerPersetujuanRanapDokter = GetEncodeUrlSrtPersetujuanRanap_Dokter(model.PDirawatICU.Dokter, model.NoReg, sectionid);
                            ViewBag.UrlFingerPersetujuanRanapSaksi1 = GetEncodeUrlSrtPersetujuanRanap_Saksi1(model.PDirawatICU.Saksi1, model.NoReg, sectionid);
                            ViewBag.UrlFingerPersetujuanRanapSaksi2 = GetEncodeUrlSrtPersetujuanRanap_Saksi2(model.PDirawatICU.Saksi2, model.NoReg, sectionid);
                        }

                        #endregion

                        #region ==== P E N G A T A R   R A W A T  I N A P
                        model.PengantarRawatInap = new PengantarRIViewModel();

                        var pengantarrawatinap = s.PengantarRawatInap.FirstOrDefault(x => x.NoReg == noreg);
                        if (pengantarrawatinap != null)
                        {
                            model.PengantarRawatInap = IConverter.Cast<PengantarRIViewModel>(pengantarrawatinap);
                            //model.PDirawatICU.Nomor = nomor;
                            model.PengantarRawatInap.Report = 1;
                            model.PengantarRawatInap.No = model.PengantarRawatInap.No;
                            model.PengantarRawatInap.NRM = model.PengantarRawatInap.NRM;
                            model.PengantarRawatInap.NoReg = model.PengantarRawatInap.NoReg;
                            model.PengantarRawatInap.SectionID = model.PengantarRawatInap.SectionID;
                            model.PengantarRawatInap.Dari = model.PengantarRawatInap.Dari;
                            model.PengantarRawatInap.AlasanDirawat_Diagnostik = model.PengantarRawatInap.AlasanDirawat_Diagnostik;
                            model.PengantarRawatInap.AlasanDirawat_Kuratif = model.PengantarRawatInap.AlasanDirawat_Kuratif;
                            model.PengantarRawatInap.AlasanDirawat_Paliatif = model.PengantarRawatInap.AlasanDirawat_Paliatif;
                            model.PengantarRawatInap.AlasanDirawat_Rehabilitatif = model.PengantarRawatInap.AlasanDirawat_Rehabilitatif;
                            model.PengantarRawatInap.Diagnosa = model.PengantarRawatInap.Diagnosa;
                            model.PengantarRawatInap.RencanaTindakan = model.PengantarRawatInap.RencanaTindakan;
                            model.PengantarRawatInap.TglTindakan = model.PengantarRawatInap.TglTindakan;
                            model.PengantarRawatInap.Persiapan_Puasa = model.PengantarRawatInap.Persiapan_Puasa;
                            model.PengantarRawatInap.Persiapan_Diet = model.PengantarRawatInap.Persiapan_Diet;
                            model.PengantarRawatInap.Persiapan_Alat = model.PengantarRawatInap.Persiapan_Alat;
                            model.PengantarRawatInap.Persiapan_Tidakperlu = model.PengantarRawatInap.Persiapan_Tidakperlu;
                            model.PengantarRawatInap.Persiapan_Lain = model.PengantarRawatInap.Persiapan_Lain;
                            model.PengantarRawatInap.Persiapan_Lain_Ket = model.PengantarRawatInap.Persiapan_Lain_Ket;
                            model.PengantarRawatInap.TglMRS = model.PengantarRawatInap.TglMRS;
                            model.PengantarRawatInap.MasukRawat = model.PengantarRawatInap.MasukRawat;
                            model.PengantarRawatInap.NoBpjs = model.PengantarRawatInap.NoBpjs;
                            model.PengantarRawatInap.KeteranganLainnya = model.PengantarRawatInap.KeteranganLainnya;
                            model.PengantarRawatInap.Pasien = model.PengantarRawatInap.Pasien;

                            var _perawat = eSIM.mDokter.FirstOrDefault(x => x.DokterID == model.PengantarRawatInap.Perawat);
                            if (_perawat != null)
                            {
                                model.PengantarRawatInap.PerawatNama = _perawat.NamaDOkter;
                            }

                            var fingerpasien = s.mFinger.Where(x => x.userid == model.NRM).FirstOrDefault();
                            if (fingerpasien != null)
                            {
                                model.PengantarRawatInap.SudahRegPasien = 1;
                            }
                            else
                            {
                                model.PengantarRawatInap.SudahRegPasien = 0;
                            }

                            var fingerperawat = s.mFinger.Where(x => x.userid == model.PengantarRawatInap.Perawat).FirstOrDefault();
                            if (fingerperawat != null)
                            {
                                model.PengantarRawatInap.SudahRegPegawai = 1;
                            }
                            else
                            {
                                model.PengantarRawatInap.SudahRegPegawai = 0;
                            }

                            ViewBag.UrlFingerPersetujuanRanap = GetEncodeUrlPengantarRanap(model.NRM, model.NoReg, sectionid);
                            if (model.PengantarRawatInap.Perawat != null)
                            {
                                ViewBag.UrlFingerPersetujuanRanapPerawat = GetEncodeUrlPengantarRanap_Perawat(model.PengantarRawatInap.Perawat, model.NoReg, sectionid);
                            }
                        }
                        else
                        {
                            pengantarrawatinap = new PengantarRawatInap();
                            model.PengantarRawatInap = IConverter.Cast<PengantarRIViewModel>(pengantarrawatinap);
                            //model.PengantarRawatInap.Nomor = nomor;
                            var _perawat = eSIM.mDokter.FirstOrDefault(x => x.DokterID == model.PengantarRawatInap.Perawat);
                            if (_perawat != null)
                            {
                                model.PengantarRawatInap.PerawatNama = _perawat.NamaDOkter;
                            }
                            //model.PengantarRawatInap.Nama = model.PengantarRawatInap.Nama;
                            //model.PengantarRawatInap.Umur = model.PengantarRawatInap.Umur;
                            //model.PengantarRawatInap.JenisKelamin = model.PengantarRawatInap.JenisKelamin;
                            //model.PengantarRawatInap.Alamat = model.PengantarRawatInap.Alamat;
                            //model.PengantarRawatInap.Umur = model.UmurThn;
                            //model.PengantarRawatInap.JenisKelamin = model.JenisKelamin;
                            //model.PengantarRawatInap.AlamatPasien = model.Alamat;
                            model.PengantarRawatInap.Report = 0;
                            model.PengantarRawatInap.NRM = model.PengantarRawatInap.NRM;
                            model.PengantarRawatInap.NoReg = model.PengantarRawatInap.NoReg;
                            model.PengantarRawatInap.SectionID = model.PengantarRawatInap.SectionID;
                            model.PengantarRawatInap.Tanggal = DateTime.Today;
                            ViewBag.UrlFingerPernyataanRanap = GetEncodeUrlPengantarRanap(model.NRM, model.NoReg, sectionid);
                            ViewBag.UrlFingerPernyataanRanapPerawat = GetEncodeUrlPengantarRanap_Perawat(model.PengantarRawatInap.Perawat, model.NoReg, sectionid);
                        }

                        #endregion

                    }
                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            return View(model);
        }

        #region ==== P O S T  E M R

        #region === S U R A T  P E R N Y A T A A N

        [HttpPost]
        [ActionName("SuratPernyataan")]
        public ActionResult SuratPernyataan_Post()
        {
            try
            {

                var item = new RegistrasiPasienRegistrasiViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var model = eEMR.SuratPernyataanRegistrasi.FirstOrDefault(x => x.SectionID == item.SuratPernyataan.SectionID && x.NoReg == item.SuratPernyataan.NoReg);
                            if (model == null)
                            {
                                model = new SuratPernyataanRegistrasi();
                                var o = IConverter.Cast<SuratPernyataanRegistrasi>(item.SuratPernyataan);
                                o.Tanggal = DateTime.Today;
                                eEMR.SuratPernyataanRegistrasi.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<SuratPernyataanRegistrasi>(item.SuratPernyataan);
                                model.Tanggal = DateTime.Today;
                                eEMR.SuratPernyataanRegistrasi.AddOrUpdate(model);
                            }

                            result = new ResultSS(eEMR.SaveChanges());
                            return RedirectToAction("Index", "Registrasi", new { noreg = item.SuratPernyataan.NoReg });


                        }

                    }

                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return RedirectToAction("Index", "Registrasi");
            }
            catch (SqlException ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            catch (DbEntityValidationException ex)
            {
                TempData["Status"] = "danger";
                TempData["Message"] = string.Join("|",
                    ex.EntityValidationErrors.First().ValidationErrors.Select(x => x.ErrorMessage));
            }
            catch (Exception ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            return RedirectToAction("Index", "Registrasi");
        }
        #endregion

        #region === S U R A T  P E R N Y A T A A N  K E N A I K A N  K E L A S

        [HttpPost]
        [ActionName("SuratPernyataanKenaikanKelas")]
        public ActionResult SuratPernyataanKenaikanKelas_Post()
        {
            try
            {
                var item = new RegistrasiPasienRegistrasiViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var model = eEMR.SuratPernyataanKenaikanKelasRegistrasi.FirstOrDefault(x => x.SectionID == item.SuratPernyataanKenaikanKelas.SectionID && x.NoReg == item.SuratPernyataanKenaikanKelas.NoReg);
                            if (model == null)
                            {
                                model = new SuratPernyataanKenaikanKelasRegistrasi();
                                var o = IConverter.Cast<SuratPernyataanKenaikanKelasRegistrasi>(item.SuratPernyataanKenaikanKelas);
                                o.Tanggal = DateTime.Today;
                                eEMR.SuratPernyataanKenaikanKelasRegistrasi.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<SuratPernyataanKenaikanKelasRegistrasi>(item.SuratPernyataanKenaikanKelas);
                                model.Tanggal = DateTime.Today;
                                eEMR.SuratPernyataanKenaikanKelasRegistrasi.AddOrUpdate(model);
                            }

                            result = new ResultSS(eEMR.SaveChanges());
                            return RedirectToAction("Index", "Registrasi", new { noreg = item.SuratPernyataanKenaikanKelas.NoReg });


                        }

                    }

                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return RedirectToAction("Index", "Registrasi");
            }
            catch (SqlException ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            catch (DbEntityValidationException ex)
            {
                TempData["Status"] = "danger";
                TempData["Message"] = string.Join("|",
                    ex.EntityValidationErrors.First().ValidationErrors.Select(x => x.ErrorMessage));
            }
            catch (Exception ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            return RedirectToAction("Index", "Registrasi");
        }
        #endregion

        #region === S U R A T  P E R S E T U J U A N  R A W A T  I N A P

        [HttpPost]
        [ActionName("PersetujuanRawatInap")]
        public ActionResult PersetujuanRawatInap_Post()
        {
            try
            {

                var item = new RegistrasiPasienRegistrasiViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var model = eEMR.PersetujuanDirawatDiICU.FirstOrDefault(x => x.NoReg == item.PDirawatICU.NoReg);
                            if (model == null)
                            {
                                model = new PersetujuanDirawatDiICU();
                                var o = IConverter.Cast<PersetujuanDirawatDiICU>(item.PDirawatICU);
                                o.Tanggal = DateTime.Today;
                                eEMR.PersetujuanDirawatDiICU.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<PersetujuanDirawatDiICU>(item.PDirawatICU);
                                model.Tanggal = DateTime.Today;
                                eEMR.PersetujuanDirawatDiICU.AddOrUpdate(model);
                            }

                            result = new ResultSS(eEMR.SaveChanges());
                            return RedirectToAction("Index", "Registrasi", new { noreg = item.PDirawatICU.NoReg });


                        }

                    }

                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return RedirectToAction("Index", "Registrasi");
            }
            catch (SqlException ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            catch (DbEntityValidationException ex)
            {
                TempData["Status"] = "danger";
                TempData["Message"] = string.Join("|",
                    ex.EntityValidationErrors.First().ValidationErrors.Select(x => x.ErrorMessage));
            }
            catch (Exception ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            return RedirectToAction("Index", "Registrasi");
        }
        #endregion

        #region === S U R A T  P E R N Y A T A A N   R A W A T  I N A P

        [HttpPost]
        [ActionName("PengantarRawatInap")]
        public ActionResult PengantarRawatInap_Post()
        {
            try
            {

                var item = new RegistrasiPasienRegistrasiViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var model = eEMR.PengantarRawatInap.FirstOrDefault(x => x.NoReg == item.NoReg);
                            if (model == null)
                            {
                                model = new PengantarRawatInap();
                                var o = IConverter.Cast<PengantarRawatInap>(item.PengantarRawatInap);
                                o.Tanggal = DateTime.Today;
                                eEMR.PengantarRawatInap.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<PengantarRawatInap>(item.PengantarRawatInap);
                                model.Tanggal = DateTime.Today;
                                eEMR.PengantarRawatInap.AddOrUpdate(model);
                            }

                            result = new ResultSS(eEMR.SaveChanges());
                            return RedirectToAction("Index", "Registrasi", new { noreg = item.PengantarRawatInap.NoReg });


                        }

                    }

                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return RedirectToAction("Index", "Registrasi");
            }
            catch (SqlException ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            catch (DbEntityValidationException ex)
            {
                TempData["Status"] = "danger";
                TempData["Message"] = string.Join("|",
                    ex.EntityValidationErrors.First().ValidationErrors.Select(x => x.ErrorMessage));
            }
            catch (Exception ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            return RedirectToAction("Index", "Registrasi");
        }
        #endregion

        #endregion

        #region ==== R E P O R T  P D F  E M R 

        #region === PERSETUJUAN UMUM 
        public ActionResult ExportPDFRegPersetujuanUmum(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"PersetujuanUmumNew.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("PersetujuanUmumNew", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"PersetujuanUmumNew;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === SURAT PERNYATAAN KENAIKAN KELAS REGISTRASI
        public ActionResult ExportPDFSuratPernyataanKenaikanKelas(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Surat_PernyataanKenaikanKelasRegistrasi.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Surat_PernyataanKenaikanKelasRegistrasi", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Surat_PernyataanKenaikanKelasRegistrasi;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === SURAT PERNYATAAN REGISTRASI
        public ActionResult ExportPDFSuratPernyataan(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"SuratPernyataan_Registrasi.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("SuratPernyataan_Registrasi", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"SuratPernyataan_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === SURAT PERNYATAAN REGISTRASI
        public ActionResult ExportPDFIdentitasPasien_Registrasi(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"IdentitasPasien_Registrasi.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("IdentitasPasien_Registrasi", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"IdentitasPasien_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === SURAT PERSETUJUAN RAWAT INAP
        public ActionResult ExportPDF(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"PersetujuanDirawatDi_ICU.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("PersetujuanDirawatDi_ICU", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"PersetujuanDirawatDi_ICU;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === SURAT PENGANTAR RAWAT INAP
        public ActionResult ExportPDFPengantarRI(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"PengantarRawatInap_IGD.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("PengantarRawatInap_IGD", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"PengantarRawatInap_IGD;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #endregion

        #region ===== Cek Verif Persetujuan Umum
        [HttpGet]
        public string CheckBerhasil(string noreg, string section, string id)
        {
            var result = new ReturnFingerViewModel();
            using (var s = new EMR_Entities())
            {
                var finger = s.mFinger.Where(x => x.userid == id).FirstOrDefault();
                if (finger != null)
                {
                    var m = s.PersetujuanUmumRegistrasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                    if (m != null)
                    {
                        if (m.TandaTangan != null)
                        {
                            result.Pasien = "1";
                        }
                        else
                        {
                            result.Pasien = "0";
                        }
                        if (m.TandaTanganPetugas != null)
                        {
                            result.Petugas = "1";
                        }
                        else
                        {
                            result.Petugas = "0";
                        }
                    }
                }
                else
                {
                    result.Pasien = "0";
                    result.Petugas = "0";
                }
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result
            });
        }

        public string GetEncodeUrlPersetujuanUmum(string id, string noreg, string section)
        {
            var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/Registrasi/get_key";
            //var a = Server.MapPath("~/RegisterFinger/GetKey");
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
            var url = System.Convert.ToBase64String(plainTextBytes);
            return url;
        }

        public string GetEncodeUrlPersetujuanUmum_Dokter(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/Registrasi/get_key";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }

            //var cek = System.Convert.FromBase64String(url);
            //var cek2 = Encoding.UTF8.GetString(cek);

        }

        [HttpGet]
        public string get_key(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/Registrasi/checkverif";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }

        public string checkverif(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    if (section == "SEC000")
                    {
                        var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                        if (dokter != null)
                        {
                            user_id = dokter.DokterID;
                        }

                        var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                        var f = s.mFinger.Where(x => x.userid == user_id);
                        foreach (var i in f.ToList())
                        {
                            var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                            if (salt.ToUpper() == vStamp.ToUpper())
                            {
                                #region === Verif Persetujuan Umum 

                                var dataspersetujuanumum = s.PersetujuanUmumRegistrasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                                if (dataspersetujuanumum != null)
                                {
                                    if (dokter != null)
                                    {
                                        dataspersetujuanumum.TandaTanganPetugas = i.Gambar1;
                                    }
                                    else
                                    {
                                        dataspersetujuanumum.TandaTangan = i.Gambar1;
                                    }
                                    s.SaveChanges();
                                }
                                #endregion
                                return "empty";
                            };
                        }
                    }

                }

            }
            return "Gagal";
        }
        #endregion

        #region ===== Cek Verif Surat Pernyataan 
        [HttpGet]
        public string CheckBerhasilSrtPernyataan(string noreg, string section, string id)
        {
            var result = new ReturnFingerViewModel();
            using (var s = new EMR_Entities())
            {
                var finger = s.mFinger.Where(x => x.userid == id).FirstOrDefault();
                if (finger != null)
                {
                    var m = s.SuratPernyataanRegistrasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                    if (m != null)
                    {
                        if (m.TTDPasien != null)
                        {
                            result.Pasien = "1";
                        }
                        else
                        {
                            result.Pasien = "0";
                        }
                        if (m.TTDpetugas != null)
                        {
                            result.Petugas = "1";
                        }
                        else
                        {
                            result.Petugas = "0";
                        }
                    }
                }
                else
                {
                    result.Pasien = "0";
                    result.Petugas = "0";
                }
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result
            });
        }

        public string GetEncodeUrlSrtPernyataan(string id, string noreg, string section)
        {
            var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/Registrasi/get_keySrtPernyataan";
            //var a = Server.MapPath("~/RegisterFinger/GetKey");
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
            var url = System.Convert.ToBase64String(plainTextBytes);
            return url;
        }

        public string GetEncodeUrlSrtPernyataan_Dokter(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/Registrasi/get_keySrtPernyataan";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }

            //var cek = System.Convert.FromBase64String(url);
            //var cek2 = Encoding.UTF8.GetString(cek);

        }

        [HttpGet]
        public string get_keySrtPernyataan(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/Registrasi/checkverifSrtPernyataan";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }

        public string checkverifSrtPernyataan(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    if (section == "SEC000")
                    {
                        var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                        if (dokter != null)
                        {
                            user_id = dokter.DokterID;
                        }

                        var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                        var f = s.mFinger.Where(x => x.userid == user_id);
                        foreach (var i in f.ToList())
                        {
                            var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                            if (salt.ToUpper() == vStamp.ToUpper())
                            {
                                var datassrtpenyataan = s.SuratPernyataanRegistrasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                                if (datassrtpenyataan != null)
                                {
                                    if (dokter != null)
                                    {
                                        datassrtpenyataan.TTDpetugas = i.Gambar1;
                                    }
                                    else
                                    {
                                        datassrtpenyataan.TTDPasien = i.Gambar1;
                                    }
                                    s.SaveChanges();
                                }

                                return "empty";
                            };
                        }
                    }

                }

            }
            return "Gagal";
        }
        #endregion

        #region ===== Cek Verif Surat Pernyataan Kenaikan Kelas
        [HttpGet]
        public string CheckBerhasilSrtPernyataanKenaikanKls(string noreg, string section, string id)
        {
            var result = new ReturnFingerViewModel();
            using (var s = new EMR_Entities())
            {
                var finger = s.mFinger.Where(x => x.userid == id).FirstOrDefault();
                if (finger != null)
                {
                    var m = s.SuratPernyataanKenaikanKelasRegistrasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                    if (m != null)
                    {
                        if (m.TTDPasien != null)
                        {
                            result.Pasien = "1";
                        }
                        else
                        {
                            result.Pasien = "0";
                        }

                        if (m.TTDPetugas != null)
                        {
                            result.Petugas = "1";
                        }
                        else
                        {
                            result.Petugas = "0";
                        }
                    }
                }
                else
                {
                    result.Pasien = "0";
                    result.Petugas = "0";
                }
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result
            });
        }

        public string GetEncodeUrlSrtPernyataanKenaikanKls(string id, string noreg, string section)
        {
            var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/Registrasi/get_keySrtPernyataanKenaikanKls";
            //var a = Server.MapPath("~/RegisterFinger/GetKey");
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
            var url = System.Convert.ToBase64String(plainTextBytes);
            return url;
        }

        public string GetEncodeUrlSrtPernyataanKenaikanKls_Dokter(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/Registrasi/get_keySrtPernyataanKenaikanKls";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }

            //var cek = System.Convert.FromBase64String(url);
            //var cek2 = Encoding.UTF8.GetString(cek);

        }

        [HttpGet]
        public string get_keySrtPernyataanKenaikanKls(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/Registrasi/checkverifSrtPernyataanKenaikanKls";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }

        public string checkverifSrtPernyataanKenaikanKls(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    if (section == "SEC000")
                    {
                        var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                        if (dokter != null)
                        {
                            user_id = dokter.DokterID;
                        }

                        var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                        var f = s.mFinger.Where(x => x.userid == user_id);
                        foreach (var i in f.ToList())
                        {
                            var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                            if (salt.ToUpper() == vStamp.ToUpper())
                            {
                                var datassrtpenyataan = s.SuratPernyataanKenaikanKelasRegistrasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                                if (datassrtpenyataan != null)
                                {
                                    if (dokter != null)
                                    {
                                        datassrtpenyataan.TTDPetugas = i.Gambar1;
                                    }
                                    else
                                    {
                                        datassrtpenyataan.TTDPasien = i.Gambar1;
                                    }
                                    s.SaveChanges();
                                }

                                return "empty";
                            };
                        }
                    }

                }

            }
            return "Gagal";
        }
        #endregion

        #region ===== Cek Verif Surat Persetujuan Ranap
        [HttpGet]
        public string CheckBerhasilSrtPersetujuanRanap(string noreg, string section, string id)
        {
            var result = new ReturnFingerViewModel();
            using (var s = new EMR_Entities())
            {
                var finger = s.mFinger.Where(x => x.userid == id).FirstOrDefault();
                if (finger != null)
                {
                    var m = s.PersetujuanDirawatDiICU.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                    if (m != null)
                    {
                        if (m.TTDPasien != null)
                        {
                            result.Pasien = "1";
                        }
                        else
                        {
                            result.Pasien = "0";
                        }

                        if (m.TTDpetugas != null)
                        {
                            result.Petugas = "1";
                        }
                        else
                        {
                            result.Petugas = "0";
                        }

                        if (m.TTDSaksi1 != null)
                        {
                            result.Saksi1 = "1";
                        }
                        else
                        {
                            result.Saksi1 = "0";
                        }

                        if (m.TTDSaksi2 != null)
                        {
                            result.Saksi2 = "1";
                        }
                        else
                        {
                            result.Saksi2 = "0";
                        }
                    }
                }
                else
                {
                    result.Pasien = "0";
                    result.Petugas = "0";
                    result.Saksi1 = "0";
                    result.Saksi2 = "0";
                }
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result
            });
        }

        public string GetEncodeUrlSrtPersetujuanRanap(string id, string noreg, string section)
        {
            var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/Registrasi/get_keySrtPersetujuanRanap";
            //var a = Server.MapPath("~/RegisterFinger/GetKey");
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
            var url = System.Convert.ToBase64String(plainTextBytes);
            return url;
        }

        public string GetEncodeUrlSrtPersetujuanRanap_Dokter(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/Registrasi/get_keySrtPersetujuanRanapDokter";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }

            //var cek = System.Convert.FromBase64String(url);
            //var cek2 = Encoding.UTF8.GetString(cek);

        }

        public string GetEncodeUrlSrtPersetujuanRanap_Saksi1(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/Registrasi/get_keySrtPersetujuanRanapSaksi1";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }

            //var cek = System.Convert.FromBase64String(url);
            //var cek2 = Encoding.UTF8.GetString(cek);

        }

        public string GetEncodeUrlSrtPersetujuanRanap_Saksi2(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/Registrasi/get_keySrtPersetujuanRanapSaksi2";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }

            //var cek = System.Convert.FromBase64String(url);
            //var cek2 = Encoding.UTF8.GetString(cek);

        }
        [HttpGet]
        public string get_keySrtPersetujuanRanapDokter(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                fdata = f.finger_data;
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/Registrasi/checkverifSrtPersetujuanRanapDokter";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }

        [HttpGet]
        public string get_keySrtPersetujuanRanapSaksi1(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/Registrasi/checkverifSrtPersetujuanRanapSaksi1";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }

        [HttpGet]
        public string get_keySrtPersetujuanRanapSaksi2(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/Registrasi/checkverifSrtPersetujuanRanapSaksi2";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }

        public string checkverifSrtPersetujuanRanapDokter(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    if (section == "SEC000")
                    {
                        var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                        if (dokter != null)
                        {
                            user_id = dokter.DokterID;
                        }

                        var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                        var f = s.mFinger.Where(x => x.userid == user_id);
                        foreach (var i in f.ToList())
                        {
                            var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                            if (salt.ToUpper() == vStamp.ToUpper())
                            {
                                var datassrtpenyataan = s.PersetujuanDirawatDiICU.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                                if (datassrtpenyataan != null)
                                {
                                    if (dokter != null)
                                    {
                                        datassrtpenyataan.TTDpetugas = i.Gambar1;
                                    }
                                    else
                                    {
                                        datassrtpenyataan.TTDPasien = i.Gambar1;
                                    }
                                    s.SaveChanges();
                                }
                                return "empty";
                            };
                        }
                    }

                }

            }
            return "Gagal";
        }

        public string checkverifSrtPersetujuanRanapSaksi1(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    if (section == "SEC000")
                    {
                        var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                        if (dokter != null)
                        {
                            user_id = dokter.DokterID;
                        }

                        var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                        var f = s.mFinger.Where(x => x.userid == user_id);
                        foreach (var i in f.ToList())
                        {
                            var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                            if (salt.ToUpper() == vStamp.ToUpper())
                            {
                                var datassrtpenyataan = s.PersetujuanDirawatDiICU.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                                if (datassrtpenyataan != null)
                                {
                                    if (dokter != null)
                                    {
                                        datassrtpenyataan.TTDSaksi1 = i.Gambar1;
                                    }
                                    else
                                    {
                                        datassrtpenyataan.TTDPasien = i.Gambar1;
                                    }
                                    s.SaveChanges();
                                }
                                return "empty";
                            };
                        }
                    }

                }

            }
            return "Gagal";
        }

        public string checkverifSrtPersetujuanRanapSaksi2(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    if (section == "SEC000")
                    {
                        var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                        if (dokter != null)
                        {
                            user_id = dokter.DokterID;
                        }

                        var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                        var f = s.mFinger.Where(x => x.userid == user_id);
                        foreach (var i in f.ToList())
                        {
                            var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                            if (salt.ToUpper() == vStamp.ToUpper())
                            {
                                var datassrtpenyataan = s.PersetujuanDirawatDiICU.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                                if (datassrtpenyataan != null)
                                {
                                    if (dokter != null)
                                    {
                                        datassrtpenyataan.TTDSaksi2 = i.Gambar1;
                                    }
                                    else
                                    {
                                        datassrtpenyataan.TTDPasien = i.Gambar1;
                                    }
                                    s.SaveChanges();
                                }
                                return "empty";
                            };
                        }
                    }

                }

            }
            return "Gagal";
        }
        #endregion

        #region ===== Cek Verif Surat Pengatar Ranap
        [HttpGet]
        public string CheckBerhasilPengantarRanap(string noreg, string section, string id)
        {
            var berhasil = false;
            using (var s = new EMR_Entities())
            {
                var finger = s.mFinger.Where(x => x.userid == id).FirstOrDefault();
                if (finger != null)
                {
                    var m1 = s.PengantarRawatInap.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                    if (m1 != null)
                    {
                        if (m1.TTDPasien != null)
                        {
                            berhasil = true;
                        }
                        if (m1.TTDpetugas != null)
                        {
                            berhasil = true;
                        }
                    }
                }
                else
                {
                    berhasil = false;
                }
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = berhasil,
            });
        }

        public string GetEncodeUrlPengantarRanap(string id, string noreg, string section)
        {
            var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/Registrasi/get_keyPengantarRanap";
            //var a = Server.MapPath("~/RegisterFinger/GetKey");
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
            var url = System.Convert.ToBase64String(plainTextBytes);
            return url;
        }

        public string GetEncodeUrlPengantarRanap_Perawat(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/Registrasi/get_keyPengantarRanapPerawat";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }

            //var cek = System.Convert.FromBase64String(url);
            //var cek2 = Encoding.UTF8.GetString(cek);

        }

        [HttpGet]
        public string get_keyPengantarRanapPerawat(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                fdata = f.finger_data;
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/Registrasi/checkverifPengantarRanapPerawat";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }

        public string checkverifPengantarRanapPerawat(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    if (section == "SEC000")
                    {
                        var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                        if (dokter != null)
                        {
                            user_id = dokter.DokterID;
                        }

                        var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                        var f = s.mFinger.Where(x => x.userid == user_id);
                        foreach (var i in f.ToList())
                        {
                            var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                            if (salt.ToUpper() == vStamp.ToUpper())
                            {
                                var datassrtpenyataan = s.PengantarRawatInap.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                                if (datassrtpenyataan != null)
                                {
                                    if (dokter != null)
                                    {
                                        datassrtpenyataan.TTDpetugas = i.Gambar1;
                                    }
                                    else
                                    {
                                        datassrtpenyataan.TTDPasien = i.Gambar1;
                                    }
                                    s.SaveChanges();
                                }
                                return "empty";
                            };
                        }
                    }

                }

            }
            return "Gagal";
        }
        #endregion

        #region  ==== CekVerif Detail Observatif Komprehensif

        [HttpGet]
        public string get_keydtlobsesif(string userid, string noreg, string section, int no)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/Registrasi/checkverifdtlobsesif";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section + "/" + no};";
        }

        public string checkverifdtlobsesif(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            int no = int.Parse(param[2]);
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    if (section == "SEC002")
                    {
                        var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                        if (dokter != null)
                        {
                            user_id = dokter.DokterID;
                        }

                        var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                        var f = s.mFinger.Where(x => x.userid == user_id);
                        foreach (var i in f.ToList())
                        {
                            var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                            if (salt.ToUpper() == vStamp.ToUpper())
                            {
                                #region === Verif Detail Obsesif

                                var datasdetailobses = s.ObservasiKomprehensif_Detail.Where(x => x.NoReg == noreg && x.SectionID == section && x.No == no);
                                foreach (var xx in datasdetailobses.ToList())
                                    if (datasdetailobses != null)
                                    {
                                        if (dokter != null)
                                        {
                                            xx.TTDDokter = i.Gambar1;
                                        }
                                        else
                                        {
                                            xx.TTDDokter = i.Gambar1;
                                        }
                                        s.SaveChanges();
                                    }
                                #endregion
                                return "empty";
                            };
                        }
                    }

                }

            }
            return "Gagal";
        }
        #endregion

        #region  ==== CekVerif Detail Diagnosa

        [HttpGet]
        public string get_keydtldiagnosa(string userid, string noreg, string section, int no)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/Registrasi/checkverifdtldiagnosa";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section + "/" + no};";
        }

        public string checkverifdtldiagnosa(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            int no = int.Parse(param[2]);
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    if (section == "SEC002")
                    {
                        var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                        if (dokter != null)
                        {
                            user_id = dokter.DokterID;
                        }

                        var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                        var f = s.mFinger.Where(x => x.userid == user_id);
                        foreach (var i in f.ToList())
                        {
                            var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                            if (salt.ToUpper() == vStamp.ToUpper())
                            {
                                #region === Verif Detail Obsesif

                                var datasdetailobses = s.DiagnosaKeperawatan_Detail.Where(x => x.NoReg == noreg && x.SectionID == section && x.No == no);
                                foreach (var xx in datasdetailobses.ToList())
                                    if (datasdetailobses != null)
                                    {
                                        if (dokter != null)
                                        {
                                            xx.TTDDokter = i.Gambar1;
                                        }
                                        else
                                        {
                                            xx.TTDDokter = i.Gambar1;
                                        }
                                        s.SaveChanges();
                                    }
                                #endregion
                                return "empty";
                            };
                        }
                    }

                }

            }
            return "Gagal";
        }
        #endregion


        [HttpPost]
        public string checkOtorisasiUser(string username, string password, string noreg, string section, string form, string dokter)
        {
            try
            {
                object r;
                using (var emr = new EMR_Entities())
                { 
                    using (var sim = new SIM_Entities())
                {
                    var checkOtorisasi = sim.mSupplier.Where(x => x.Kode_Supplier == username && x.Password == password && x.Kode_Supplier == dokter).FirstOrDefault();
                    if (checkOtorisasi != null)
                    {
                        #region === Verif Persetujuan Umum 

                        var dataspersetujuanumum = emr.PersetujuanUmumRegistrasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                            if (dataspersetujuanumum != null)
                            {
                                dataspersetujuanumum.Verifikasi = true;
                            }
                            else {
                                dataspersetujuanumum.Verifikasi = false;
                            }
                            emr.SaveChanges();
                        #endregion

                        r = new
                        {
                            isSuccess = true,
                            Data = checkOtorisasi,
                            Mssg = "Verifikasi Berhasil",
                        };
                    }
                    else
                    {
                        r = new
                        {
                            isSuccess = false,
                            Data = "",
                            Mssg = "Maaf Verifikasi Gagal",
                        };
                    }
                }
                }

                return JsonConvert.SerializeObject(r);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}