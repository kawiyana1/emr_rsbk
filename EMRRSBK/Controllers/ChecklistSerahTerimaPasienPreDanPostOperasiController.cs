﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class ChecklistSerahTerimaPasienPreDanPostOperasiController : Controller
    {
        // GET: ChecklistSerahTerimaPasienPreDanPostOperasi
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var sectionnama = Request.Cookies["SectionNamaPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.CheckListSerahTerimaPasien.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.ChckListSrhtrmpasien = IConverter.Cast<CheckListSerahTerimaPasienPreDanPostOperasiViewModel>(item);
                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Petugas1);
                            if (dokter != null)
                            {
                                model.ChckListSrhtrmpasien.Petugas1Nama = dokter.NamaDOkter;
                            }
                            var dokteranak = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Petugas2);
                            if (dokteranak != null)
                            {
                                model.ChckListSrhtrmpasien.Petugas2Nama = dokteranak.NamaDOkter;
                            }
                            model.ChckListSrhtrmpasien.Report = 1;

                            var fingerpetugas1 = eEMR.CheckListSerahTerimaPasien.Where(x => x.Petugas1 == model.ChckListSrhtrmpasien.Petugas1 && x.TandaTanganPetugas1 == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerpetugas1 != null)
                            {
                                model.ChckListSrhtrmpasien.SudahRegPetugas1 = 1;
                            }
                            else
                            {
                                model.ChckListSrhtrmpasien.SudahRegPetugas1 = 0;
                            }
                            if (model.ChckListSrhtrmpasien.Petugas1 != null)
                            {
                                ViewBag.UrlFingerChckListSrhtrmpasienPetugas1 = GetEncodeUrlChckListSrhtrmpasien_Petugas1(model.ChckListSrhtrmpasien.Petugas1, model.ChckListSrhtrmpasien.NoReg, sectionid);
                            }

                            var fingerpetugas2 = eEMR.CheckListSerahTerimaPasien.Where(x => x.Petugas2 == model.ChckListSrhtrmpasien.Petugas2 && x.TandaTanganPetugas2 == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerpetugas2 != null)
                            {
                                model.ChckListSrhtrmpasien.SudahRegPetugas2 = 1;
                            }
                            else
                            {
                                model.ChckListSrhtrmpasien.SudahRegPetugas2 = 0;
                            }
                            if (model.ChckListSrhtrmpasien.Petugas2 != null)
                            {
                                ViewBag.UrlFingerChckListSrhtrmpasienPetugas2 = GetEncodeUrlChckListSrhtrmpasien_Petugas2(model.ChckListSrhtrmpasien.Petugas2, model.ChckListSrhtrmpasien.NoReg, sectionid);
                            }
                        }
                        else
                        {
                            item = new CheckListSerahTerimaPasien();
                            model.ChckListSrhtrmpasien = IConverter.Cast<CheckListSerahTerimaPasienPreDanPostOperasiViewModel>(item);
                            ViewBag.UrlFingerChckListSrhtrmpasienPetugas1 = GetEncodeUrlChckListSrhtrmpasien_Petugas1(model.ChckListSrhtrmpasien.Petugas1, model.ChckListSrhtrmpasien.NoReg, sectionid);
                            ViewBag.UrlFingerChckListSrhtrmpasienPetugas2 = GetEncodeUrlChckListSrhtrmpasien_Petugas2(model.ChckListSrhtrmpasien.Petugas2, model.ChckListSrhtrmpasien.NoReg, sectionid);
                            model.ChckListSrhtrmpasien.NoReg = noreg;
                            model.ChckListSrhtrmpasien.NRM = m.NRM;
                            model.ChckListSrhtrmpasien.SectionID = sectionid;
                            model.ChckListSrhtrmpasien.Tanggal1 = DateTime.Today;
                            model.ChckListSrhtrmpasien.Tanggal2 = DateTime.Today;
                            model.ChckListSrhtrmpasien.VitalSign = DateTime.Today;
                            model.ChckListSrhtrmpasien.Huknah_Jam = DateTime.Now;
                            model.ChckListSrhtrmpasien.Puasa_jam = DateTime.Now;
                            model.ChckListSrhtrmpasien.Jam1 = DateTime.Now;
                            model.ChckListSrhtrmpasien.Jam2 = DateTime.Now;
                            model.ChckListSrhtrmpasien.HuknahRendah_Jam = DateTime.Now;
                            model.ChckListSrhtrmpasien.Report = 0;

                        }

                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string ChecklistSerahTerimaPasienPreDanPostOperasi_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == item.ChckListSrhtrmpasien.NoReg);
                            item.ChckListSrhtrmpasien.NoReg = item.ChckListSrhtrmpasien.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.ChckListSrhtrmpasien.SectionID = sectionid;
                            item.ChckListSrhtrmpasien.NRM = item.ChckListSrhtrmpasien.NRM;
                            var model = eEMR.CheckListSerahTerimaPasien.FirstOrDefault(x => x.SectionID == item.ChckListSrhtrmpasien.SectionID && x.NoReg == item.ChckListSrhtrmpasien.NoReg);

                            if (model == null)
                            {
                                model = new CheckListSerahTerimaPasien();
                                var o = IConverter.Cast<CheckListSerahTerimaPasien>(item.ChckListSrhtrmpasien);
                                eEMR.CheckListSerahTerimaPasien.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<CheckListSerahTerimaPasien>(item.ChckListSrhtrmpasien);
                                eEMR.CheckListSerahTerimaPasien.AddOrUpdate(model);
                            }



                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFChecklistSerahTerimaPasienPreDanPostOperasi(string noreg, string sectionid)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_CheckListSerahTerimaPasien.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"Rpt_CheckListSerahTerimaPasien", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_CheckListSerahTerimaPasien;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }

        public string GetEncodeUrlChckListSrhtrmpasien_Petugas1(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifSerahTerimaPasien/get_keychckliskslmntpetugas1";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlChckListSrhtrmpasien_Petugas2(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifSerahTerimaPasien/get_keychckliskslmntpetugas2";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }
    }
}