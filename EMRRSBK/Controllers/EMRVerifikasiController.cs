﻿using EMRRSBK.Entities;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class EMRVerifikasiController : Controller
    {
        // GET: EMRVerifikasi
        public string CekVerifikasiUser(string dokter)
        {
            try
            {
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMR_Entities())
                    {
                        var dt = sim.mSupplier.FirstOrDefault(x => x.Kode_Supplier == dokter);
                        var userlogin = User.Identity.GetUserId();
                        //if (dt.TTD == null)
                        //{
                        //    return JsonConvert.SerializeObject(new
                        //    {
                        //        IsSuccess = false,
                        //        Data = "",
                        //        Message = "Tanda tangan masih kosong"

                        //    });
                        //}
                        //if (dt.UserIdWeb == userlogin)
                        //{
                        //    return JsonConvert.SerializeObject(new
                        //    {
                        //        IsSuccess = true,
                        //        Data = true,
                        //        Message = ""

                        //    });
                        //}
                    }
                }

                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = false,
                    Data = "",
                    Message = "User login tidak sesuai"

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}