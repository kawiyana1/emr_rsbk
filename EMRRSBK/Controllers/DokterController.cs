﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class DokterController : Controller
    {
        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<mDokter> proses = s.mDokter.Where(x => x.DokterIdEMR == true);
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(mDokter.DokterID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(mDokter.NamaDOkter)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<DokterViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public string ListObat(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<mBarang> proses = s.mBarang.Where(x => x.Aktif == true && x.Kelompok == "OBAT");
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(mBarang.Kode_Barang)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(mBarang.Nama_Barang)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<BarangViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        
        [HttpPost]
        public string ListSupplier(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<mSupplier> proses = s.mSupplier.Where(x => x.DokterIdEMR == true);
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(mSupplier.Kode_Supplier)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(mSupplier.Nama_Supplier)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<DokterViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListPasien(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<VW_DataPasienReg> proses = s.VW_DataPasienReg.Where(x => x.Tanggal == DateTime.Today);
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(VW_DataPasienReg.NoReg)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(VW_DataPasienReg.NamaPasien)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<RegistrasiPasienViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string SudahInput(string kode, string section, string noreg)
        {
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var h = s.Function_CekFormGeneral(noreg, sectionid).ToList();
                   
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = h,
                    });
                }
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}