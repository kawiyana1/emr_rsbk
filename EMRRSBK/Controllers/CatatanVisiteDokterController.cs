﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class CatatanVisiteDokterController : Controller
    {
        // GET: CatatanVisiteDokter
        public ActionResult Index(string noreg, string sectionid, string kamar)
        {
             var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.CatatanVisiteDokter.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.CttVstDkter = IConverter.Cast<CatatanVisiteDokterViewModel>(item);

                            model.CttVstDkter.NamaPasien = m.NamaPasien;
                            model.CttVstDkter.NoReg = noreg;
                            model.CttVstDkter.NRM = model.NRM;
                            model.CttVstDkter.SectionID = sectionid;
                            model.CttVstDkter.Report = 1;
                            model.CttVstDkter.TglLahir = m.TglLahir;
                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == model.CttVstDkter.NamaDokter);
                            if (dokter != null) {
                                model.CttVstDkter.NamaDokterNama = dokter.NamaDOkter;
                            }
                        }
                        else
                        {
                            item = new CatatanVisiteDokter();
                            model.CttVstDkter = IConverter.Cast<CatatanVisiteDokterViewModel>(item);
                            model.CttVstDkter.Report = 0;
                            model.CttVstDkter.NamaPasien = m.NamaPasien;
                            model.CttVstDkter.NoReg = noreg;
                            model.CttVstDkter.NRM = model.NRM;
                            model.CttVstDkter.SectionID = sectionid;
                            model.CttVstDkter.Tanggal = DateTime.Today;
                            model.CttVstDkter.Jam = DateTime.Now;
                            model.CttVstDkter.TglLahir = m.TglLahir;
                        }

                        #region Detail Visite
                        var List = eEMR.CatatanVisiteDokter_Detail.Where(x => x.SectionID == sectionid && x.NoReg == noreg).ToList();
                        if (List != null)
                        {

                            model.CttVstDkter.VstDetail_List = new ListDetail<CatatanVisiteDetailModelDetail>();

                            foreach (var x1 in List)
                            {
                                var y = IConverter.Cast<CatatanVisiteDetailModelDetail>(x1);
                                var ttdparaf = eSIM.mDokter.FirstOrDefault(z => z.DokterID.ToString() == y.Paraf);

                                if (ttdparaf != null)
                                {
                                    y.ParafNama = ttdparaf.NamaDOkter;
                                }

                                var ttdverifikator = eSIM.mDokter.FirstOrDefault(z => z.DokterID.ToString() == y.Verifikator);

                                if (ttdverifikator != null)
                                {
                                    y.VerifikatorNama = ttdverifikator.NamaDOkter;
                                }
                                model.CttVstDkter.VstDetail_List.Add(false, y);
                            }

                        }
                        #endregion

                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string CatatanVisiteDokter_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.CttVstDkter.NoReg = item.CttVstDkter.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.CttVstDkter.SectionID = sectionid;
                            item.CttVstDkter.NRM = item.CttVstDkter.NRM;
                            var model = eEMR.CatatanVisiteDokter.FirstOrDefault(x => x.SectionID == sectionid && x.NoReg == item.CttVstDkter.NoReg);

                            if (model == null)
                            {
                                model = new CatatanVisiteDokter();
                                var o = IConverter.Cast<CatatanVisiteDokter>(item.CttVstDkter);
                                eEMR.CatatanVisiteDokter.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<CatatanVisiteDokter>(item.CttVstDkter);
                                eEMR.CatatanVisiteDokter.AddOrUpdate(model);
                            }

                            #region ===== Detail Ruangan Perawatan

                            if (item.CttVstDkter.VstDetail_List == null) item.CttVstDkter.VstDetail_List = new ListDetail<CatatanVisiteDetailModelDetail>();
                            item.CttVstDkter.VstDetail_List.RemoveAll(x => x.Remove);
                            foreach (var x in item.CttVstDkter.VstDetail_List)
                            {
                                x.Model.SectionID = sectionid;
                                x.Model.NoReg = item.CttVstDkter.NoReg;
                                x.Model.NRM = item.CttVstDkter.NRM;
                                x.Model.Tanggal = DateTime.Today;
                                x.Model.Jam = DateTime.Now;

                            }
                            var new_List = item.CttVstDkter.VstDetail_List;
                            var real_List = eEMR.CatatanVisiteDokter_Detail.Where(x => x.SectionID == item.CttVstDkter.SectionID && x.NoReg == item.CttVstDkter.NoReg).ToList();
                            foreach (var x in real_List)
                            {
                                var z = new_List.FirstOrDefault(y => y.Model.No == x.No);
                                if (z == null)
                                {
                                    eEMR.CatatanVisiteDokter_Detail.Remove(x);
                                }

                            }
                            foreach (var x in new_List)
                            {

                                var _m = real_List.FirstOrDefault(y => y.No == x.Model.No);

                                if (_m == null)
                                {

                                    _m = new CatatanVisiteDokter_Detail();
                                    eEMR.CatatanVisiteDokter_Detail.Add(IConverter.Cast<CatatanVisiteDokter_Detail>(x.Model));
                                }

                                else
                                {

                                    _m = IConverter.Cast<CatatanVisiteDokter_Detail>(x.Model);
                                    eEMR.CatatanVisiteDokter_Detail.AddOrUpdate(_m);


                                }

                            }

                            #endregion


                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFCatatanVisiteDokter(string noreg, string sectionid)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_CatatanVisiteDokter.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand("Rpt_CatatanVisiteDokter", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables["Rpt_CatatanVisiteDokter;1"].SetDataSource(ds[i].Tables[0]);

                i++;
                cmd.Add(new SqlCommand($"Rpt_CatatanVisiteDokter_Detail", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_CatatanVisiteDokter_Detail;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}