﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class FormPerhitunganIntraoperatifController : Controller
    {

        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.FormPerhitunganIntraoperatif.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.FrmPhtnganIntraoper = IConverter.Cast<FormPerhitunganIntraoperatifViewModel>(item);
                            var instrumen = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Instrumen);
                            if (instrumen != null)
                            {
                                model.FrmPhtnganIntraoper.InstrumenNama = instrumen.NamaDOkter;
                            }
                            model.FrmPhtnganIntraoper.NamaPasien = m.NamaPasien;
                            model.FrmPhtnganIntraoper.NoReg = noreg;
                            model.FrmPhtnganIntraoper.NRM = model.NRM;
                            model.FrmPhtnganIntraoper.SectionID = sectionid;
                            model.FrmPhtnganIntraoper.JenisKelamin = m.JenisKelamin;
                            model.FrmPhtnganIntraoper.Report = 1;

                            var fingerintrumen = eEMR.FormPerhitunganIntraoperatif.Where(x => x.Instrumen == model.FrmPhtnganIntraoper.Instrumen && x.TandaTanganInstrumen == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerintrumen != null)
                            {
                                model.FrmPhtnganIntraoper.SudahRegInstrumen = 1;
                            }
                            else
                            {
                                model.FrmPhtnganIntraoper.SudahRegInstrumen = 0;
                            }
                            if (model.FrmPhtnganIntraoper.Instrumen != null)
                            {
                                ViewBag.UrlFingerFrmPhtnganIntraoperInstrumen = GetEncodeUrlFrmPhtnganIntraoper_Instrumen(model.FrmPhtnganIntraoper.Instrumen, model.FrmPhtnganIntraoper.NoReg, sectionid);
                            }
                        }
                        else
                        {
                            item = new FormPerhitunganIntraoperatif();
                            model.FrmPhtnganIntraoper = IConverter.Cast<FormPerhitunganIntraoperatifViewModel>(item);
                            ViewBag.UrlFingerFrmPhtnganIntraoperInstrumen = GetEncodeUrlFrmPhtnganIntraoper_Instrumen(model.FrmPhtnganIntraoper.Instrumen, model.FrmPhtnganIntraoper.NoReg, sectionid);
                            model.FrmPhtnganIntraoper.Report = 0;
                            model.FrmPhtnganIntraoper.NamaPasien = m.NamaPasien;
                            model.FrmPhtnganIntraoper.NoReg = noreg;
                            model.FrmPhtnganIntraoper.NRM = model.NRM;
                            model.FrmPhtnganIntraoper.SectionID = sectionid;
                            model.FrmPhtnganIntraoper.Umur = m.UmurThn;
                            model.FrmPhtnganIntraoper.JenisKelamin = m.JenisKelamin;
                            model.FrmPhtnganIntraoper.Tanggal = DateTime.Today;
                            model.FrmPhtnganIntraoper.Jam = DateTime.Now;
                        }

                        var List = eEMR.FormPenghitunganIntraoperatif_Detail.Where(x => x.SectionID == sectionid && x.NoReg == noreg).ToList();
                        if (List != null)
                        {

                            model.FrmPhtnganIntraoper.PrhtnganIntra_List = new ListDetail<FormPerhitunganIntraoperatifModelDetail>();

                            foreach (var x1 in List)
                            {
                                var y = IConverter.Cast<FormPerhitunganIntraoperatifModelDetail>(x1);
                                var ttdmasuk = eSIM.mBarang.FirstOrDefault(z => z.Kode_Barang.ToString() == y.NamaItem);

                                if (ttdmasuk != null)
                                {
                                    y.NamaItemNama = ttdmasuk.Nama_Barang;
                                }
                                model.FrmPhtnganIntraoper.PrhtnganIntra_List.Add(false, y);
                            }

                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string FormPerhitunganIntraoperatif_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.FrmPhtnganIntraoper.NoReg = item.FrmPhtnganIntraoper.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.FrmPhtnganIntraoper.SectionID = sectionid;
                            item.FrmPhtnganIntraoper.NRM = item.FrmPhtnganIntraoper.NRM;
                            var model = eEMR.FormPerhitunganIntraoperatif.FirstOrDefault(x => x.SectionID == sectionid && x.NoReg == item.FrmPhtnganIntraoper.NoReg);

                            if (model == null)
                            {
                                model = new FormPerhitunganIntraoperatif();
                                var o = IConverter.Cast<FormPerhitunganIntraoperatif>(item.FrmPhtnganIntraoper);
                                eEMR.FormPerhitunganIntraoperatif.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<FormPerhitunganIntraoperatif>(item.FrmPhtnganIntraoper);
                                eEMR.FormPerhitunganIntraoperatif.AddOrUpdate(model);
                            }

                            #region ===== DETAIL

                            if (item.FrmPhtnganIntraoper.PrhtnganIntra_List == null) item.FrmPhtnganIntraoper.PrhtnganIntra_List = new ListDetail<FormPerhitunganIntraoperatifModelDetail>();
                            item.FrmPhtnganIntraoper.PrhtnganIntra_List.RemoveAll(x => x.Remove);
                            foreach (var x in item.FrmPhtnganIntraoper.PrhtnganIntra_List)
                            {
                                x.Model.SectionID = sectionid;
                                x.Model.NoReg = item.FrmPhtnganIntraoper.NoReg;
                                x.Model.NRM = item.FrmPhtnganIntraoper.NRM;
                                x.Model.Tanggal = DateTime.Today;
                                x.Model.Jam = DateTime.Now;

                            }
                            var new_List = item.FrmPhtnganIntraoper.PrhtnganIntra_List;
                            var real_List = eEMR.FormPenghitunganIntraoperatif_Detail.Where(x => x.SectionID == item.FrmPhtnganIntraoper.SectionID && x.NoReg == item.FrmPhtnganIntraoper.NoReg).ToList();
                            foreach (var x in real_List)
                            {
                                var z = new_List.FirstOrDefault(y => y.Model.No == x.No);
                                if (z == null)
                                {
                                    eEMR.FormPenghitunganIntraoperatif_Detail.Remove(x);
                                }

                            }
                            foreach (var x in new_List)
                            {

                                var _m = real_List.FirstOrDefault(y => y.No == x.Model.No);

                                if (_m == null)
                                {

                                    _m = new FormPenghitunganIntraoperatif_Detail();
                                    eEMR.FormPenghitunganIntraoperatif_Detail.Add(IConverter.Cast<FormPenghitunganIntraoperatif_Detail>(x.Model));
                                }

                                else
                                {

                                    _m = IConverter.Cast<FormPenghitunganIntraoperatif_Detail>(x.Model);
                                    eEMR.FormPenghitunganIntraoperatif_Detail.AddOrUpdate(_m);


                                }

                            }

                            #endregion


                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFFormPerhitunganIntraoperatif(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_FormPerhitunganIntraoperatif.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand("Rpt_FormPerhitunganIntraoperatif", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables["Rpt_FormPerhitunganIntraoperatif;1"].SetDataSource(ds[i].Tables[0]);

                i++;
                cmd.Add(new SqlCommand($"Rpt_FormPerhitunganIntraoperatif_Detail", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_FormPerhitunganIntraoperatif_Detail;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }

        public string GetEncodeUrlFrmPhtnganIntraoper_Instrumen(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifFormPerhitunganIntraoperatif/get_keyformperhitunganinstrumen";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

    }
}