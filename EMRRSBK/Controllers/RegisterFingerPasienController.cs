﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Web.Helpers;
using System.Text;
using System.Configuration;

namespace EMRRSBK.Controllers
{
    public class RegisterFingerPasienController : Controller
    {
        // GET: RegisterFingerPasien
        public ActionResult Index()
        {
            return View();
        }

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    IQueryable<mFinger> proses = s.mFinger;
                    //proses = proses.Where($"{nameof(mFinger.useriddokter)}.Contains(@0)", filter[0]);
                    //proses = proses.Where($"{nameof(mFinger.useridpasien)}.Contains(@0)", filter[1]);
                    //proses = proses.Where($"{nameof(mFinger.finger_id)}.Contains(@0)", filter[2]);
                    //proses = proses.Where($"{nameof(mFinger.vc)}.Contains(@0)", filter[3]);
                    //proses = proses.Where($"{nameof(mFinger.vkey)}.Contains(@0)", filter[4]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<FingerViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== C R E A T E  P A S I E N

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string nrm)
        {
            var model = new FingerViewModel();
            model.id_View = nrm;
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string Create_Post(string RegTemp)
        {
            try
            {
                var data = RegTemp.Split(';');
                var vStamp = data[0];
                var sn = data[1];
                var user_id = data[2];
                var regTemp = data[3];

                using (var s = new EMR_Entities())
                {
                    var m = s.mDevice.Where(x => x.sn == sn).ToArray();

                    var salt = StaticModel.CreateMD5(m[0].ac + m[0].vkey + regTemp + sn + user_id);

                    if (salt.ToUpper() == vStamp.ToUpper())
                    {
                        s.mFinger.Add(new mFinger
                        {
                            finger_data = regTemp,
                            userid = user_id,
                        });

                        s.SaveChanges();
                    }
                }
                return "empty";
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== E D I T

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id)
        {
            DeviceViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var m = s.mDevice.FirstOrDefault(x => x.sn == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<DeviceViewModel>(m);
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post(string id)
        {
            try
            {
                var item = new DeviceViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new EMR_Entities())
                    {
                        var model = s.mDevice.FirstOrDefault(x => x.sn == item.sn);
                        if (model == null) throw new Exception("Data Tidak ditemukan");

                        if (model.sn.ToUpper() != item.ac.ToUpper())
                        {
                            var has = s.mDevice.Where(x => x.sn == item.sn).Count() > 0;
                            if (has) throw new Exception("SN sudah digunakan");
                        }

                        TryUpdateModel(model);
                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Device Edit {model.sn}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E L E T E

        [HttpPost]
        public string Delete(string id)
        {
            try
            {
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var m = s.mDevice.FirstOrDefault(x => x.sn == id);
                    if (m == null) throw new Exception("Data Tidak ditemukan");
                    s.mDevice.Remove(m);
                    result = new ResultSS(s.SaveChanges());

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"Device delete {id}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgDelete(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== G E T  K E Y
        [HttpGet]
        public string get_key(string userid)
        {
            var ac = "";
            var sn = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            var data = userid.Split(';');
            var user = data[0];
            var serialnumber = data[1];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault(x => x.sn == serialnumber);
                ac = m.ac;
                sn = m.sn;
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority +
    Request.ApplicationPath.TrimEnd('/') + "/RegisterFingerPasien/Create";
            return $"{user};SecurityKey;{time};{urlpost};{ac + sn}";
        }

        #endregion

        #region ===== G E T  E N C O D E  U R L
        [HttpGet]
        public string GetEncodeUrl(string id)
        {
            var snclient = Request.Cookies["SerialNumber"] == null ? "" : Request.Cookies["SerialNumber"].Value;
            var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority +
    Request.ApplicationPath.TrimEnd('/') + "/RegisterFingerPasien/get_key";
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + ";" + snclient);
            var url = System.Convert.ToBase64String(plainTextBytes);
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = url,
            }); ;
        }
        #endregion

        #region ===== C E K  B E R H A S I L
        [HttpGet]
        public string CheckBerhasil(string id)
        {
            var berhasil = false;
            using (var s = new EMR_Entities())
            {
                var m = s.mFinger.FirstOrDefault(x => x.userid == id);
                if (m != null)
                {
                    berhasil = true;
                }
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = berhasil,
            }); ;
        }
        #endregion

        #region ===== P A S I E N
        [HttpPost]
        public string ListPasien(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<mPasien> proses = s.mPasien;
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(mPasien.NRM)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(mPasien.NamaPasien)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(mPasien.Alamat)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(mPasien.Phone)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where("TglLahir = @0", DateTime.Parse(filter[4]));
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PasienViewModel>(x));
                    foreach (var x in m)
                    {
                        x.TglLahir_View = x.TglLahir.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== ISERT TTD

        [HttpPost]
        public string IsertTTD(string iduser, string gambar, string kategori)
        {
            bool berhasil = false;
            using (var s = new EMR_Entities())
            {
                var m = s.mFinger.OrderByDescending(x => x.finger_id).FirstOrDefault(x => x.userid == iduser);
                if (m != null)
                {
                    m.Gambar1 = Convert.FromBase64String(gambar);
                    s.SaveChanges();
                    berhasil = true;
                }

            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = berhasil,
            });
        }
        #endregion

        #region ==== INSERT USER & PASSWORD
        [HttpPost]
        public string InsertUserPassTTD(string iduser, string gambar, string kategori)
        {
            bool berhasil = false;
            using (var s = new EMR_Entities())
            {
                var m = s.mFinger.OrderByDescending(x => x.finger_id).FirstOrDefault(x => x.userid == iduser);
                if (m != null)
                {
                    m.Gambar1 = Convert.FromBase64String(gambar);
                    s.SaveChanges();
                    berhasil = true;
                }

            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = berhasil,
            });
        }
        #endregion 
    }
}