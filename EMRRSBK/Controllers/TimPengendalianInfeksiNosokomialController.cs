﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class TimPengendalianInfeksiNosokomialController : Controller
    {
        // GET: TimPengendalianInfeksiNosokomial
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var sectionnama = Request.Cookies["SectionNamaPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.TimPengendalianInfeksiNosokomial.FirstOrDefault(x => x.NoReg == noreg);
                        if (item != null)
                        {
                            model.TimPngdlInfkNskmia = IConverter.Cast<TimPengendalianInfeksiNosokomialViewModel>(item);
                            model.TimPngdlInfkNskmia.NamaPasien = m.NamaPasien;
                            model.TimPngdlInfkNskmia.TglLahir = m.TglLahir;
                            model.TimPngdlInfkNskmia.JenisKelamin = m.JenisKelamin;
                            var petugas = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.PetugasPemeriksa);
                            if (petugas != null)
                            {
                                model.TimPngdlInfkNskmia.PetugasPemeriksaNama = petugas.NamaDOkter;
                            }

                            var oprtr = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Operator);
                            if (oprtr != null)
                            {
                                model.TimPngdlInfkNskmia.OperatorNama = oprtr.NamaDOkter;
                            }

                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.YangMelakukan_Dokter);
                            if (dokter != null)
                            {
                                model.TimPngdlInfkNskmia.YangMelakukan_DokterNama = dokter.NamaDOkter;
                            }

                            var anestesi = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.YangMelakukan_PinataAnastesi);
                            if (anestesi != null)
                            {
                                model.TimPngdlInfkNskmia.YangMelakukan_PinataAnastesiNama = anestesi.NamaDOkter;
                            }

                            var perawat = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.YangMelakukan_Perawat);
                            if (perawat != null)
                            {
                                model.TimPngdlInfkNskmia.YangMelakukan_PerawatNama = perawat.NamaDOkter;
                            }

                            var dokterahli = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.YangMelakukanOperasi_DokterAhli);
                            if (dokterahli != null)
                            {
                                model.TimPngdlInfkNskmia.YangMelakukanOperasi_DokterAhliNama = dokterahli.NamaDOkter;
                            }

                            var asisten1 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.YangMelakukanOperasi_Asisten_1);
                            if (asisten1 != null)
                            {
                                model.TimPngdlInfkNskmia.YangMelakukanOperasi_Asisten_1Nama = asisten1.NamaDOkter;
                            }

                            var asisten2 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.YangMelakukanOperasi_Asisten_2);
                            if (asisten2 != null)
                            {
                                model.TimPngdlInfkNskmia.YangMelakukanOperasi_Asisten_2Nama = asisten2.NamaDOkter;
                            }
                            model.TimPngdlInfkNskmia.Ruangan = sectionnama;
                            model.TimPngdlInfkNskmia.Report = 1;
                            model.TimPngdlInfkNskmia.NoReg = noreg;
                            model.TimPngdlInfkNskmia.SectionID = sectionid;
                        }
                        else
                        {
                            item = new TimPengendalianInfeksiNosokomial();
                            model.TimPngdlInfkNskmia = IConverter.Cast<TimPengendalianInfeksiNosokomialViewModel>(item);
                            model.TimPngdlInfkNskmia.NoReg = noreg;
                            model.TimPngdlInfkNskmia.SectionID = sectionid;
                            model.TimPngdlInfkNskmia.Report = 0;
                            model.TimPngdlInfkNskmia.NRM = model.NRM;
                            model.TimPngdlInfkNskmia.Ruangan = sectionnama;
                            model.TimPngdlInfkNskmia.Tanggal = DateTime.Today;
                            model.TimPngdlInfkNskmia.TglLahir = DateTime.Today;
                            model.TimPngdlInfkNskmia.TanggalMRS = DateTime.Today;
                            model.TimPngdlInfkNskmia.NamaPasien = m.NamaPasien;
                            model.TimPngdlInfkNskmia.TglLahir = m.TglLahir;
                            model.TimPngdlInfkNskmia.JenisKelamin = m.JenisKelamin;
                            model.TimPngdlInfkNskmia.Ruangan = sectionnama;
                        }

                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string TimPengendalianInfeksiNosokomialOK_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == item.TimPngdlInfkNskmia.NoReg);
                            item.TimPngdlInfkNskmia.NoReg = item.TimPngdlInfkNskmia.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.TimPngdlInfkNskmia.SectionID = sectionid;
                            item.TimPngdlInfkNskmia.NRM = item.TimPngdlInfkNskmia.NRM;
                            var model = eEMR.TimPengendalianInfeksiNosokomial.FirstOrDefault(x => x.NoReg == item.TimPngdlInfkNskmia.NoReg);

                            if (model == null)
                            {
                                model = new TimPengendalianInfeksiNosokomial();
                                var o = IConverter.Cast<TimPengendalianInfeksiNosokomial>(item.TimPngdlInfkNskmia);
                                eEMR.TimPengendalianInfeksiNosokomial.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<TimPengendalianInfeksiNosokomial>(item.TimPngdlInfkNskmia);
                                eEMR.TimPengendalianInfeksiNosokomial.AddOrUpdate(model);
                            }



                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFTimPengendalianInfeksiNosokomial(string noreg, string sectionid)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_TimPengendalianInfeksiNosokomial.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"Rpt_TimPengendalianInfeksiNosokomial", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_TimPengendalianInfeksiNosokomial;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}