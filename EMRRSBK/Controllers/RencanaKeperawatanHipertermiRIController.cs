﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class RencanaKeperawatanHipertermiRIController : Controller
    {
        // GET: RencanaKeperawatanHipertermiRI
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.RencanaKeperawatanHipertermi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.RncanaKprwtHptrmi = IConverter.Cast<RencanaKeperawatanHipertermiViewModel>(item);
                            model.RncanaKprwtHptrmi.NoReg = noreg;
                            model.RncanaKprwtHptrmi.NRM = model.NRM;
                            model.RncanaKprwtHptrmi.SectionID = sectionid;
                            model.RncanaKprwtHptrmi.Report = 1;
                            model.RncanaKprwtHptrmi.NamaPasien = m.NamaPasien;
                            model.RncanaKprwtHptrmi.JenisKelamin = m.JenisKelamin;
                            model.RncanaKprwtHptrmi.TglLahir = m.TglLahir;
                        }
                        else
                        {
                            item = new RencanaKeperawatanHipertermi();
                            model.RncanaKprwtHptrmi = IConverter.Cast<RencanaKeperawatanHipertermiViewModel>(item);
                            model.RncanaKprwtHptrmi.Report = 0;
                            model.RncanaKprwtHptrmi.NamaPasien = m.NamaPasien;
                            model.RncanaKprwtHptrmi.JenisKelamin = m.JenisKelamin;
                            model.RncanaKprwtHptrmi.TglLahir = m.TglLahir;
                            model.RncanaKprwtHptrmi.NoReg = noreg;
                            model.RncanaKprwtHptrmi.NRM = model.NRM;
                            model.RncanaKprwtHptrmi.SectionID = sectionid;
                            model.RncanaKprwtHptrmi.Tanggal = DateTime.Today;
                            model.RncanaKprwtHptrmi.Jam = DateTime.Now;
                        }


                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string RencanaKeperawatanHipertermi_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.RncanaKprwtHptrmi.NoReg = item.RncanaKprwtHptrmi.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.RncanaKprwtHptrmi.SectionID = sectionid;
                            item.RncanaKprwtHptrmi.NRM = item.RncanaKprwtHptrmi.NRM;
                            var model = eEMR.RencanaKeperawatanHipertermi.FirstOrDefault(x => x.SectionID == sectionid && x.NoReg == item.RncanaKprwtHptrmi.NoReg);

                            if (model == null)
                            {
                                model = new RencanaKeperawatanHipertermi();
                                var o = IConverter.Cast<RencanaKeperawatanHipertermi>(item.RncanaKprwtHptrmi);
                                eEMR.RencanaKeperawatanHipertermi.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<RencanaKeperawatanHipertermi>(item.RncanaKprwtHptrmi);
                                eEMR.RencanaKeperawatanHipertermi.AddOrUpdate(model);
                            }


                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFRencanaKeperawatanHipertermi(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_RencanaKeperawatanHipertermi.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();
                var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                var i = 0;
                cmd.Add(new SqlCommand("Rpt_RencanaKeperawatanHipertermi", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables["Rpt_RencanaKeperawatanHipertermi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                //cmd.Add(new SqlCommand($"Rpt_RencanaKeperawatanHipertermi_Detail", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables[$"Rpt_RencanaKeperawatanHipertermi_Detail;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}