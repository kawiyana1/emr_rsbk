﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class EMRSkalaHumptyDumptyController : Controller
    {
        // GET: EMRSkalaHumptyDumpty
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string noreg, string nrm, int view = 0)
        {
            var model = new EWSScorePemantauanViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var dokumen = s.SkalaHumptyDumpty.FirstOrDefault(x => x.NoReg == noreg);
                    if (dokumen != null)
                    {
                        #region === Detail 
                        model.SkalaHumptyDumpty_List = new ListDetail<EMRSkalaHumptyDumptyViewModel>();
                        var detail_2 = s.SkalaHumptyDumpty.Where(x => x.NoReg == noreg).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<EMRSkalaHumptyDumptyViewModel>(x);
                            var namadokter = sim.mDokter.FirstOrDefault(z => z.DokterID == y.Paraf);
                            y.ParafNama = namadokter == null ? "" : namadokter.NamaDOkter;
                            model.SkalaHumptyDumpty_List.Add(false, y);
                        }
                        #endregion

                    }
                    else
                    {
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                        model.NRM = nrm;
                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {

            try
            {
                var item = new EWSScorePemantauanViewModel();

                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var model = s.SkalaHumptyDumpty.FirstOrDefault(x => x.NoReg == item.NoReg);
                    var activity = "";
                    if (model == null)
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoReg == item.NoReg);
                        
                        activity = "Create Skala Humpty Dumpty";
                    }
                    else
                    {
                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoReg == item.NoReg);
                        
                        activity = "Update Skala Humpty Dumpty";
                    }

                    #region === Detail Keperawatan
                    if (item.SkalaHumptyDumpty_List == null) item.SkalaHumptyDumpty_List = new ListDetail<EMRSkalaHumptyDumptyViewModel>();
                    item.SkalaHumptyDumpty_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.SkalaHumptyDumpty_List)
                    {
                        x.Model.NoReg = item.NoReg;
                        x.Model.NRM = item.NRM;
                        x.Model.SectionID = sectionid;
                        x.Model.Username = User.Identity.GetUserName();
                    }
                    var new_list = item.SkalaHumptyDumpty_List;
                    var real_list = s.SkalaHumptyDumpty.Where(x => x.NoReg == item.NoReg).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.SkalaHumptyDumpty.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.SkalaHumptyDumpty.Add(IConverter.Cast<SkalaHumptyDumpty>(x.Model));
                        }
                        else
                        {
                            _m.No = x.Model.No;
                            _m.Tanggal = x.Model.Tanggal;
                            _m.Jam = x.Model.Jam;
                            _m.Usia = x.Model.Usia;
                            _m.Usia_Skor = x.Model.Usia_Skor;
                            _m.JenisKelamin = x.Model.JenisKelamin;
                            _m.JenisKelamin_Skor = x.Model.JenisKelamin_Skor;
                            _m.Diagnosa = x.Model.Diagnosa;
                            _m.Diagnosa_Skor = x.Model.Diagnosa_Skor;
                            _m.GangguanKognitif = x.Model.GangguanKognitif;
                            _m.GangguanKognitif_Skor = x.Model.GangguanKognitif_Skor;
                            _m.FaktorLingkungan = x.Model.FaktorLingkungan;
                            _m.FaktorLingkungan_Skor = x.Model.FaktorLingkungan_Skor;
                            _m.Respon = x.Model.Respon;
                            _m.Respon_Skor = x.Model.Respon_Skor;
                            _m.PenggunaanObat = x.Model.PenggunaanObat;
                            _m.PenggunaanObat_Skor = x.Model.PenggunaanObat_Skor;
                            _m.TotalSkor = x.Model.TotalSkor;
                            _m.Keterangan = x.Model.Keterangan;
                            _m.TotalSkorKeterangan = x.Model.TotalSkorKeterangan;
                            _m.Paraf = x.Model.Paraf;
                        }
                    }
                    #endregion

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoReg}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}