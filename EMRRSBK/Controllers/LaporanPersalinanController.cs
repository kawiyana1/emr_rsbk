﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class LaporanPersalinanController : Controller
    {
        // GET: LaporanPersalinan
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.LaporanPersalinan.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.LprnPersln = IConverter.Cast<LaporanPersalinanViewModel>(item);
                            var ditolong = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.DitolongOleh);
                            if (ditolong != null)
                            {
                                model.LprnPersln.DitolongOlehNama = ditolong.NamaDOkter;
                            }
                            model.LprnPersln.NoReg = noreg;
                            model.LprnPersln.NRM = model.NRM;
                            model.LprnPersln.SectionID = sectionid;
                            model.LprnPersln.Report = 1;
                        }
                        else
                        {
                            item = new LaporanPersalinan();
                            model.LprnPersln = IConverter.Cast<LaporanPersalinanViewModel>(item);
                            model.LprnPersln.NoReg = noreg;
                            model.LprnPersln.NRM = model.NRM;
                            model.LprnPersln.SectionID = sectionid;
                            model.LprnPersln.Umur = m.UmurThn;
                            model.LprnPersln.BayiKeadaanJelek = DateTime.Today;
                            model.LprnPersln.BayiLahirTgl = DateTime.Today;
                            model.LprnPersln.BayiLahirJam = DateTime.Now;
                            model.LprnPersln.Report = 0;
                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string LaporanOperasi_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.LprnPersln.NoReg = item.LprnPersln.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.LprnPersln.SectionID = sectionid;
                            item.LprnPersln.NRM = item.LprnPersln.NRM;
                            var model = eEMR.LaporanPersalinan.FirstOrDefault(x => x.SectionID == sectionid && x.NoReg == item.LprnPersln.NoReg);

                            if (model == null)
                            {
                                model = new LaporanPersalinan();
                                var o = IConverter.Cast<LaporanPersalinan>(item.LprnPersln);
                                eEMR.LaporanPersalinan.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<LaporanPersalinan>(item.LprnPersln);
                                eEMR.LaporanPersalinan.AddOrUpdate(model);
                            }
                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFLaporanPersalinan(string noreg, string sectionid)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_LaporanPersalinan.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand("Rpt_LaporanPersalinan", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables["Rpt_LaporanPersalinan;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}