﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class LaporanPemeriksaanVisumETRepertumController : Controller
    {
        // GET: LaporanPemeriksaanVisumETRepertum
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    model.NoReg = m.NoReg;
                    model.SectionID = sectionid;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.LaporanPemeriksaanVisumETRepertum.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.LPVisumETRepertum = IConverter.Cast<LaporanVisumETRepertumViewModel>(item);
                            model.LPVisumETRepertum.Tanggal = DateTime.Today;
                            model.LPVisumETRepertum.NamaPerempuan = m.NamaPasien;
                            model.LPVisumETRepertum.JKPerempuan = m.JenisKelamin;
                            model.LPVisumETRepertum.UmurPerempuan = m.UmurThn.ToString();
                            model.LPVisumETRepertum.AlamatPerempuan = m.Alamat;
                            model.LPVisumETRepertum.NamaLaki = m.NamaPasien;
                            model.LPVisumETRepertum.JKLaki = m.JenisKelamin;
                            model.LPVisumETRepertum.UmurLaki = m.UmurThn.ToString();
                            model.LPVisumETRepertum.AlamatLaki = m.Alamat;
                        }
                        else
                        {
                            item = new LaporanPemeriksaanVisumETRepertum();
                            model.LPVisumETRepertum = IConverter.Cast<LaporanVisumETRepertumViewModel>(item);
                            model.LPVisumETRepertum.Tanggal = DateTime.Today;
                            model.LPVisumETRepertum.NamaPerempuan = m.NamaPasien;
                            model.LPVisumETRepertum.JKPerempuan = m.JenisKelamin;
                            model.LPVisumETRepertum.UmurPerempuan = m.UmurThn.ToString();
                            model.LPVisumETRepertum.AlamatPerempuan = m.Alamat;
                            model.LPVisumETRepertum.NamaLaki = m.NamaPasien;
                            model.LPVisumETRepertum.JKLaki = m.JenisKelamin;
                            model.LPVisumETRepertum.UmurLaki = m.UmurThn.ToString();
                            model.LPVisumETRepertum.AlamatLaki = m.Alamat;
                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string LaporanPemeriksaanVisumETRepertum_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.LPVisumETRepertum.NoReg = noreg;
                            item.LPVisumETRepertum.SectionID = sectionid;
                            item.LPVisumETRepertum.NRM = m.NRM;
                            item.LPVisumETRepertum.Tanggal = DateTime.Now;
                            var model = eEMR.LaporanPemeriksaanVisumETRepertum.FirstOrDefault(x => x.SectionID == item.LPVisumETRepertum.SectionID && x.NoReg == item.NoReg);

                            if (model == null)
                            {
                                model = new LaporanPemeriksaanVisumETRepertum();
                                var o = IConverter.Cast<LaporanPemeriksaanVisumETRepertum>(item.LPVisumETRepertum);
                                eEMR.LaporanPemeriksaanVisumETRepertum.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<LaporanPemeriksaanVisumETRepertum>(item.LPVisumETRepertum);
                                eEMR.LaporanPemeriksaanVisumETRepertum.AddOrUpdate(model);
                            }

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDF(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_LaporanPemeriksaanVisumETRepertum.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"Rpt_LaporanPemeriksaanVisumETRepertum", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_LaporanPemeriksaanVisumETRepertum;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}