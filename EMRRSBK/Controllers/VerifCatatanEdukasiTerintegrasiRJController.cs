﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Configuration;

namespace EMRRSBK.Controllers
{
    public class VerifCatatanEdukasiTerintegrasiRJController : Controller
    {
        // GET: VerifCatatanEdukasiTerintegrasiRJ
        #region === CATATAN EDUKASI TERINTEGRASI RJ DOKTER SPESIALIS
        [HttpGet]
        public string CheckBerhasilDokterSpesialis(string noreg, string section, string id)
        {
            var result = new ReturnFingerViewModel();
            section = Request.Cookies["SectionIDPelayanan"].Value;
            using (var s = new EMR_Entities())
            {
                var finger = s.mFinger.Where(x => x.userid == id).FirstOrDefault();
                if (finger != null)
                {
                    var m = s.CatatanEdukasiTerintegrasiRJ.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                    if (m != null)
                    {
                        if (m.TandaTanganDokterSpesialis_NamaEdukator != null)
                        {
                            result.Dokter = "1";
                        }
                        else
                        {
                            result.Dokter = "0";
                        }

                        if (m.TandaTanganDokterSpesialis_Pasien != null)
                        {
                            result.Pasien = "1";
                        }
                        else
                        {
                            result.Pasien = "0";
                        }
                    }
                }
                else
                {
                    result.Dokter = "0";
                    result.Pasien = "0";
                }
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result
            });
        }
        [HttpGet]
        public string get_keyskdpjpdokterspesialis(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCatatanEdukasiTerintegrasiRJ/checkverifdpjpdokterspesialis";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }
        public string checkverifdpjpdokterspesialis(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                    if (dokter != null)
                    {
                        user_id = dokter.DokterID;
                    }

                    var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                    var f = s.mFinger.Where(x => x.userid == user_id);
                    foreach (var i in f.ToList())
                    {
                        var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                        if (salt.ToUpper() == vStamp.ToUpper())
                        {
                            #region === Verif 
                            var datasdpjp = s.CatatanEdukasiTerintegrasiRJ.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                            if (datasdpjp != null)
                            {
                                if (dokter != null)
                                {
                                    datasdpjp.TandaTanganDokterSpesialis_NamaEdukator = i.Gambar1;
                                }
                                else
                                {
                                    datasdpjp.TandaTanganDokterSpesialis_Pasien = i.Gambar1;
                                }
                                s.SaveChanges();
                            }
                            #endregion
                            return "empty";
                        };
                    }

                }

            }
            return "Gagal";
        }
        #endregion

        #region === CATATAN EDUKASI TERINTEGRASI RJ NUTRISI
        [HttpGet]
        public string CheckBerhasilNutrisi(string noreg, string section, string id)
        {
            var result = new ReturnFingerViewModel();
            section = Request.Cookies["SectionIDPelayanan"].Value;
            using (var s = new EMR_Entities())
            {
                var finger = s.mFinger.Where(x => x.userid == id).FirstOrDefault();
                if (finger != null)
                {
                    var m = s.CatatanEdukasiTerintegrasiRJ.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                    if (m != null)
                    {
                        if (m.TandaTanganNutrisi_Edukator != null)
                        {
                            result.Dokter = "1";
                        }
                        else
                        {
                            result.Dokter = "0";
                        }

                        if (m.TandaTanganNutrisi_Pasien != null)
                        {
                            result.Pasien = "1";
                        }
                        else
                        {
                            result.Pasien = "0";
                        }
                    }
                }
                else
                {
                    result.Dokter = "0";
                    result.Pasien = "0";
                }
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result
            });
        }
        [HttpGet]
        public string get_keyskdpjpnutrisi(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCatatanEdukasiTerintegrasiRJ/checkverifdpjpnutrisi";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }
        public string checkverifdpjpnutrisi(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                    if (dokter != null)
                    {
                        user_id = dokter.DokterID;
                    }

                    var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                    var f = s.mFinger.Where(x => x.userid == user_id);
                    foreach (var i in f.ToList())
                    {
                        var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                        if (salt.ToUpper() == vStamp.ToUpper())
                        {
                            #region === Verif 
                            var datasdpjp = s.CatatanEdukasiTerintegrasiRJ.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                            if (datasdpjp != null)
                            {
                                if (dokter != null)
                                {
                                    datasdpjp.TandaTanganNutrisi_Edukator = i.Gambar1;
                                }
                                else
                                {
                                    datasdpjp.TandaTanganNutrisi_Pasien = i.Gambar1;
                                }
                                s.SaveChanges();
                            }
                            #endregion
                            return "empty";
                        };
                    }

                }

            }
            return "Gagal";
        }
        #endregion

        #region === CATATAN EDUKASI TERINTEGRASI RJ MANAJEMEN NYERI
        [HttpGet]
        public string CheckBerhasilManajemenNyeri(string noreg, string section, string id)
        {
            var result = new ReturnFingerViewModel();
            section = Request.Cookies["SectionIDPelayanan"].Value;
            using (var s = new EMR_Entities())
            {
                var finger = s.mFinger.Where(x => x.userid == id).FirstOrDefault();
                if (finger != null)
                {
                    var m = s.CatatanEdukasiTerintegrasiRJ.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                    if (m != null)
                    {
                        if (m.TandaTanganManajemenNyeri_Edukator != null)
                        {
                            result.Dokter = "1";
                        }
                        else
                        {
                            result.Dokter = "0";
                        }

                        if (m.TandaTanganManajemenNyeri_Pasien != null)
                        {
                            result.Pasien = "1";
                        }
                        else
                        {
                            result.Pasien = "0";
                        }
                    }
                }
                else
                {
                    result.Dokter = "0";
                    result.Pasien = "0";
                }
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result
            });
        }
        [HttpGet]
        public string get_keyskdpjpmanajemennyeri(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCatatanEdukasiTerintegrasiRJ/checkverifdpjpmanajemennyeri";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }
        public string checkverifdpjpmanajemennyeri(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                    if (dokter != null)
                    {
                        user_id = dokter.DokterID;
                    }

                    var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                    var f = s.mFinger.Where(x => x.userid == user_id);
                    foreach (var i in f.ToList())
                    {
                        var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                        if (salt.ToUpper() == vStamp.ToUpper())
                        {
                            #region === Verif 
                            var datasdpjp = s.CatatanEdukasiTerintegrasiRJ.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                            if (datasdpjp != null)
                            {
                                if (dokter != null)
                                {
                                    datasdpjp.TandaTanganManajemenNyeri_Edukator = i.Gambar1;
                                }
                                else
                                {
                                    datasdpjp.TandaTanganManajemenNyeri_Pasien = i.Gambar1;
                                }
                                s.SaveChanges();
                            }
                            #endregion
                            return "empty";
                        };
                    }

                }

            }
            return "Gagal";
        }
        #endregion

        #region === CATATAN EDUKASI TERINTEGRASI RJ ROHANIAWAN
        [HttpGet]
        public string CheckBerhasilRohaniawan(string noreg, string section, string id)
        {
            var result = new ReturnFingerViewModel();
            section = Request.Cookies["SectionIDPelayanan"].Value;
            using (var s = new EMR_Entities())
            {
                var finger = s.mFinger.Where(x => x.userid == id).FirstOrDefault();
                if (finger != null)
                {
                    var m = s.CatatanEdukasiTerintegrasiRJ.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                    if (m != null)
                    {
                        if (m.TandaTanganRohaniawan_Edukator != null)
                        {
                            result.Dokter = "1";
                        }
                        else
                        {
                            result.Dokter = "0";
                        }

                        if (m.TandaTanganRohaniawan_Pasien != null)
                        {
                            result.Pasien = "1";
                        }
                        else
                        {
                            result.Pasien = "0";
                        }
                    }
                }
                else
                {
                    result.Dokter = "0";
                    result.Pasien = "0";
                }
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result
            });
        }
        [HttpGet]
        public string get_keyskdpjprohaniawan(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCatatanEdukasiTerintegrasiRJ/checkverifdpjprohaniawan";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }
        public string checkverifdpjprohaniawan(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                    if (dokter != null)
                    {
                        user_id = dokter.DokterID;
                    }

                    var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                    var f = s.mFinger.Where(x => x.userid == user_id);
                    foreach (var i in f.ToList())
                    {
                        var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                        if (salt.ToUpper() == vStamp.ToUpper())
                        {
                            #region === Verif 
                            var datasdpjp = s.CatatanEdukasiTerintegrasiRJ.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                            if (datasdpjp != null)
                            {
                                if (dokter != null)
                                {
                                    datasdpjp.TandaTanganRohaniawan_Edukator = i.Gambar1;
                                }
                                else
                                {
                                    datasdpjp.TandaTanganRohaniawan_Pasien = i.Gambar1;
                                }
                                s.SaveChanges();
                            }
                            #endregion
                            return "empty";
                        };
                    }

                }

            }
            return "Gagal";
        }
        #endregion

        #region === CATATAN EDUKASI TERINTEGRASI RJ FARMASI
        [HttpGet]
        public string CheckBerhasilFarmasi(string noreg, string section, string id)
        {
            var result = new ReturnFingerViewModel();
            section = Request.Cookies["SectionIDPelayanan"].Value;
            using (var s = new EMR_Entities())
            {
                var finger = s.mFinger.Where(x => x.userid == id).FirstOrDefault();
                if (finger != null)
                {
                    var m = s.CatatanEdukasiTerintegrasiRJ.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                    if (m != null)
                    {
                        if (m.TandaTanganFarmasi_Edukator != null)
                        {
                            result.Dokter = "1";
                        }
                        else
                        {
                            result.Dokter = "0";
                        }

                        if (m.TandaTanganFarmasi_Pasien != null)
                        {
                            result.Pasien = "1";
                        }
                        else
                        {
                            result.Pasien = "0";
                        }
                    }
                }
                else
                {
                    result.Dokter = "0";
                    result.Pasien = "0";
                }
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result
            });
        }
        [HttpGet]
        public string get_keyskdpjpfarmasi(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCatatanEdukasiTerintegrasiRJ/checkverifdpjpfarmasi";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }
        public string checkverifdpjpfarmasi(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                    if (dokter != null)
                    {
                        user_id = dokter.DokterID;
                    }

                    var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                    var f = s.mFinger.Where(x => x.userid == user_id);
                    foreach (var i in f.ToList())
                    {
                        var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                        if (salt.ToUpper() == vStamp.ToUpper())
                        {
                            #region === Verif 
                            var datasdpjp = s.CatatanEdukasiTerintegrasiRJ.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                            if (datasdpjp != null)
                            {
                                if (dokter != null)
                                {
                                    datasdpjp.TandaTanganFarmasi_Edukator = i.Gambar1;
                                }
                                else
                                {
                                    datasdpjp.TandaTanganFarmasi_Pasien = i.Gambar1;
                                }
                                s.SaveChanges();
                            }
                            #endregion
                            return "empty";
                        };
                    }

                }

            }
            return "Gagal";
        }
        #endregion

        #region === CATATAN EDUKASI TERINTEGRASI RJ PERAWAT
        [HttpGet]
        public string CheckBerhasilPerawat(string noreg, string section, string id)
        {
            var result = new ReturnFingerViewModel();
            section = Request.Cookies["SectionIDPelayanan"].Value;
            using (var s = new EMR_Entities())
            {
                var finger = s.mFinger.Where(x => x.userid == id).FirstOrDefault();
                if (finger != null)
                {
                    var m = s.CatatanEdukasiTerintegrasiRJ.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                    if (m != null)
                    {
                        if (m.TandaTanganPerawat_Edukator != null)
                        {
                            result.Dokter = "1";
                        }
                        else
                        {
                            result.Dokter = "0";
                        }

                        if (m.TandaTanganPerawat_Pasien != null)
                        {
                            result.Pasien = "1";
                        }
                        else
                        {
                            result.Pasien = "0";
                        }
                    }
                }
                else
                {
                    result.Dokter = "0";
                    result.Pasien = "0";
                }
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result
            });
        }
        [HttpGet]
        public string get_keyskdpjpperawat(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCatatanEdukasiTerintegrasiRJ/checkverifdpjpperawat";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }
        public string checkverifdpjpperawat(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                    if (dokter != null)
                    {
                        user_id = dokter.DokterID;
                    }

                    var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                    var f = s.mFinger.Where(x => x.userid == user_id);
                    foreach (var i in f.ToList())
                    {
                        var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                        if (salt.ToUpper() == vStamp.ToUpper())
                        {
                            #region === Verif 
                            var datasdpjp = s.CatatanEdukasiTerintegrasiRJ.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                            if (datasdpjp != null)
                            {
                                if (dokter != null)
                                {
                                    datasdpjp.TandaTanganPerawat_Edukator = i.Gambar1;
                                }
                                else
                                {
                                    datasdpjp.TandaTanganPerawat_Pasien = i.Gambar1;
                                }
                                s.SaveChanges();
                            }
                            #endregion
                            return "empty";
                        };
                    }

                }

            }
            return "Gagal";
        }
        #endregion

        #region === CATATAN EDUKASI TERINTEGRASI RJ REHABILITASI
        [HttpGet]
        public string CheckBerhasilRehabilitasi(string noreg, string section, string id)
        {
            var result = new ReturnFingerViewModel();
            section = Request.Cookies["SectionIDPelayanan"].Value;
            using (var s = new EMR_Entities())
            {
                var finger = s.mFinger.Where(x => x.userid == id).FirstOrDefault();
                if (finger != null)
                {
                    var m = s.CatatanEdukasiTerintegrasiRJ.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                    if (m != null)
                    {
                        if (m.TandaTanganRehabilitasi_Edukator != null)
                        {
                            result.Dokter = "1";
                        }
                        else
                        {
                            result.Dokter = "0";
                        }

                        if (m.TandaTanganRehabilitasi_Pasien != null)
                        {
                            result.Pasien = "1";
                        }
                        else
                        {
                            result.Pasien = "0";
                        }
                    }
                }
                else
                {
                    result.Dokter = "0";
                    result.Pasien = "0";
                }
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result
            });
        }
        [HttpGet]
        public string get_keyskdpjprehabilitasi(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCatatanEdukasiTerintegrasiRJ/checkverifdpjprehabilitasi";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }
        public string checkverifdpjprehabilitasi(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                    if (dokter != null)
                    {
                        user_id = dokter.DokterID;
                    }

                    var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                    var f = s.mFinger.Where(x => x.userid == user_id);
                    foreach (var i in f.ToList())
                    {
                        var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                        if (salt.ToUpper() == vStamp.ToUpper())
                        {
                            #region === Verif 
                            var datasdpjp = s.CatatanEdukasiTerintegrasiRJ.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                            if (datasdpjp != null)
                            {
                                if (dokter != null)
                                {
                                    datasdpjp.TandaTanganRehabilitasi_Edukator = i.Gambar1;
                                }
                                else
                                {
                                    datasdpjp.TandaTanganRehabilitasi_Pasien = i.Gambar1;
                                }
                                s.SaveChanges();
                            }
                            #endregion
                            return "empty";
                        };
                    }

                }

            }
            return "Gagal";
        }
        #endregion
    }
}