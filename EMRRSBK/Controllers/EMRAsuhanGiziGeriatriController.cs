﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class EMRAsuhanGiziGeriatriController : Controller
    {
        // GET: EMRAsuhanGiziGeriatri
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string noreg, string nrm, int view = 0)
        {
            var model = new EMRAsuhanGiziGeriatriViewModel();
            var sectionnama = Request.Cookies["SectionNamaPelayanan"].Value;
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var dokumen = s.AsuhanGiziGeriatri.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRAsuhanGiziGeriatriViewModel>(dokumen);
                        var dktpolimata = sim.mDokter.FirstOrDefault(x => x.DokterID == model.AhliGizi);
                        if (dktpolimata != null)
                        {
                            model.AhliGiziNama = dktpolimata.NamaDOkter;
                        }
                        model.Ruangan = sectionnama;
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                        model.NRM = nrm;
                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                        model.NRM = nrm;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                        model.Ruangan = sectionnama;
                    }

                    var dokumen_temp = s.trTemplate.Where(e => e.DokumenID == "Asuhan Gizi Geriatri").ToList();

                    ViewBag.DokumenID = "Asuhan Gizi Geriatri";
                    ViewBag.NamaDokumen = "Asuhan Gizi Geriatri";
                    model.ListTemplate = new List<SelectItemListAGiziGeriatri>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListAGiziGeriatri()
                        {
                            Value = list.NoReg,
                            Text = list.NamaTemplate
                        });
                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string noreg, string copy, string nrm)
        {
            var model = new EMRAsuhanGiziGeriatriViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var dokumen = s.AsuhanGiziGeriatri.FirstOrDefault(x => x.NoReg == copy);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRAsuhanGiziGeriatriViewModel>(dokumen);
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                        model.NRM = nrm;

                        var dpjp = sim.mDokter.FirstOrDefault(e => e.DokterID == model.AhliGizi);
                        if (dpjp != null) model.AhliGiziNama = dpjp.NamaDOkter;

                    }
                    else
                    {
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                        model.NRM = nrm;
                    }
                    model.MODEVIEW = 0;
                    var dokumen_temp = s.trTemplate.Where(e => e.DokumenID == "Asuhan Gizi Geriatri").ToList();

                    ViewBag.DokumenID = "Asuhan Gizi Geriatri";
                    ViewBag.NamaDokumen = "Asuhan Gizi Geriatri";
                    model.ListTemplate = new List<SelectItemListAGiziGeriatri>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListAGiziGeriatri()
                        {
                            Value = list.NoReg,
                            Text = list.NamaTemplate
                        });
                    }
                    model.ListTemplate = new List<SelectItemListAGiziGeriatri>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListAGiziGeriatri()
                        {
                            Value = list.NoReg,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {

            if (ModelState.IsValid)
            {
                var req = Request.Form[""];
            }
            try
            {
                var item = new EMRAsuhanGiziGeriatriViewModel();

                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var model = s.AsuhanGiziGeriatri.FirstOrDefault(x => x.NoReg == item.NoReg);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<AsuhanGiziGeriatri>(item);
                        s.AsuhanGiziGeriatri.Add(o);

                        
                        activity = "Create Assesmen Mata";
                    }
                    else
                    {
                        model = IConverter.Cast<AsuhanGiziGeriatri>(item);
                        s.AsuhanGiziGeriatri.AddOrUpdate(model);


                        activity = "Update Assesmen Mata";
                    }

                    if (item.save_template == true && item.nama_template != null)
                    {
                        var temp = new trTemplate();
                        temp.NoReg = item.NoReg;
                        temp.NamaTemplate = item.nama_template;
                        temp.DokterID = item.AhliGizi;
                        temp.DokumenID = "Asuhan Gizi Geriatri";
                        temp.SectionID = sectionid;
                        s.trTemplate.Add(temp);
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoReg}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}