﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class EditSOAPController : Controller
    {
        [HttpGet]
        [ActionName("Create")]
        public ActionResult EditCPPT_Get(string noreg, string sectionid, int nomortbl, int nomor)
        {
            var model = new SOAPViewModel();

            try
            {

                using (var eSIM = new SIM_Entities())
                {

                    using (var eEMR = new EMR_Entities())
                    {

                        var m = eEMR.SIMtrSOAP.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid && x.No == nomortbl);

                        if (m != null)
                        {
                            model = IConverter.Cast<SOAPViewModel>(m);
                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == model.DokterID);
                            var dpjp = eSIM.mDokter.FirstOrDefault(x => x.DokterID == model.DPJP);
                            var tbk_t = eSIM.mDokter.FirstOrDefault(x => x.DokterID == model.T_Inisial);
                            var tbk_b = eSIM.mDokter.FirstOrDefault(x => x.DokterID == model.B_Inisial);
                            var tbk_k = eSIM.mDokter.FirstOrDefault(x => x.DokterID == model.K_Inisial);
                            var menyerahkan = eSIM.mDokter.FirstOrDefault(x => x.DokterID == model.Menyerahkan_Petugas);
                            var menerima = eSIM.mDokter.FirstOrDefault(x => x.DokterID == model.Menerima_Petugas);
                            var karu = eSIM.mDokter.FirstOrDefault(x => x.DokterID == model.KaRU_Petugas);
                            var petugasbacaulang = eSIM.mDokter.FirstOrDefault(y => y.DokterID == model.PetugasBacaUlang);
                            var petugaskonfirmasi = eSIM.mDokter.FirstOrDefault(y => y.DokterID == model.PetugasKonfirmasi);
                            var menyerahkanpasien = eSIM.mDokter.FirstOrDefault(y => y.DokterID == model.MenyerahkanPasien);
                            var menerimapasien = eSIM.mDokter.FirstOrDefault(y => y.DokterID == model.MenerimaPasien);

                            model.T_InisialNama = tbk_t == null ? "" : tbk_t.NamaDOkter;
                            model.B_InisialNama = tbk_b == null ? "" : tbk_b.NamaDOkter;
                            model.K_InisialNama = tbk_k == null ? "" : tbk_k.NamaDOkter;
                            model.PetugasBacaUlangNama = petugasbacaulang == null ? "" : petugasbacaulang.NamaDOkter;
                            model.PetugasKonfirmasiNama = petugaskonfirmasi == null ? "" : petugaskonfirmasi.NamaDOkter;
                            model.MenyerahkanPasienNama = menyerahkanpasien == null ? "" : menyerahkanpasien.NamaDOkter;
                            model.MenerimaPasienNama = menerimapasien == null ? "" : menerimapasien.NamaDOkter;
                            model.Menyerahkan_Petugas_Nama = menyerahkan == null ? "" : menyerahkan.NamaDOkter;
                            model.Menerima_Petugas = menerima == null ? "" : menerima.NamaDOkter;
                            model.KaRU_Petugas = karu == null ? "" : karu.NamaDOkter;

                            if (dokter != null)
                            {
                                model.NamaDOkter = dokter.NamaDOkter;
                                //model.NamaProfesi = dokter.NamaProfesi == null ? "" : dokter.NamaProfesi;
                            }
                            if (dpjp != null)
                            {
                                model.DPJPNama = dpjp.NamaDOkter;
                            }
                            model.Nomor = nomor;

                            var fingercppt = eEMR.SIMtrSOAP.Where(x => x.DPJP == model.DPJP && x.TandaTanganDPJP == null && x.No == model.No && x.NRM == model.NRM).FirstOrDefault();
                            if (fingercppt != null)
                            {
                                model.SudahRegDPJP = 1;
                            }
                            else
                            {
                                model.SudahRegDPJP = 0;
                            }
                            if (model.DPJP != null)
                            {
                                ViewBag.UrlFingerCPPTDPJP = GetEncodeUrlCPPT_DPJP(model.DPJP, model.NoReg, sectionid, model.No);
                            }

                            var fingermenerimapasien = eEMR.SIMtrSOAP.Where(x => x.MenerimaPasien == model.MenerimaPasien && x.TandaTanganMenerimaPasien == null && x.No == model.No && x.NRM == model.NRM).FirstOrDefault();
                            if (fingermenerimapasien != null)
                            {
                                model.SudahRegMenerimaPasien = 1;
                            }
                            else
                            {
                                model.SudahRegMenerimaPasien = 0;
                            }
                            if (model.MenerimaPasien != null)
                            {
                                ViewBag.UrlFingerCPPTMenerimaPasien = GetEncodeUrlCPPT_MenerimaPasien(model.MenerimaPasien, model.NoReg, sectionid, model.No);
                            }

                            var fingermenyerahkan = eEMR.SIMtrSOAP.Where(x => x.MenyerahkanPasien == model.MenyerahkanPasien && x.TandaTanganMenyerahkanPasien == null && x.No == model.No && x.NRM == model.NRM).FirstOrDefault();
                            if (fingermenyerahkan != null)
                            {
                                model.SudahRegMenyerahkanPasien = 1;
                            }
                            else
                            {
                                model.SudahRegMenyerahkanPasien = 0;
                            }
                            if (model.MenyerahkanPasien != null)
                            {
                                ViewBag.UrlFingerCPPTMenyerahkanPasien = GetEncodeUrlCPPT_MenyerahkanPasien(model.MenyerahkanPasien, model.NoReg, sectionid, model.No);
                            }

                            var fingerpetugasbacaulang = eEMR.SIMtrSOAP.Where(x => x.PetugasBacaUlang == model.PetugasBacaUlang && x.TandaTanganPetugasBacaUlang == null && x.No == model.No && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerpetugasbacaulang != null)
                            {
                                model.SudahRegPetugasBacaUlang = 1;
                            }
                            else
                            {
                                model.SudahRegPetugasBacaUlang = 0;
                            }
                            if (model.PetugasBacaUlang != null)
                            {
                                ViewBag.UrlFingerCPPTPetugasBacaUlang = GetEncodeUrlCPPT_PetugasBacaUlang(model.PetugasBacaUlang, model.NoReg, sectionid, model.No);
                            }

                            var fingerpetugaskonfirmasi = eEMR.SIMtrSOAP.Where(x => x.PetugasKonfirmasi == model.PetugasKonfirmasi && x.TandaTanganPetugasKonfirmasi == null && x.No == model.No && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerpetugaskonfirmasi != null)
                            {
                                model.SudahRegPetugasKonfirmasi = 1;
                            }
                            else
                            {
                                model.SudahRegPetugasKonfirmasi = 0;
                            }
                            if (model.PetugasKonfirmasi != null)
                            {
                                ViewBag.UrlFingerCPPTPetugasKonfirmasi = GetEncodeUrlCPPT_PetugasKonfirmasi(model.PetugasKonfirmasi, model.NoReg, sectionid, model.No);
                            }
                        }

                    }

                }

            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }


            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        #region ===== Verifikasi TTD

        //[HttpPost]
        //[ActionName("ValidasiDokterCPPT")]
        //public string Verifikasi(string deskripsi, string userid, string password, string iddokter)
        //{

        //    try
        //    {
        //        ResultSS result;
        //        using (var e = new EMR_Entities())
        //        {

        //            using (var s = new SIM_Entities())
        //            {

        //                var validasi = e.mOtorisasi.FirstOrDefault(x => x.Username == userid && x.Password == password && x.DokterID == iddokter);
        //                var nama_dokter = s.mDokter.FirstOrDefault(x => x.DokterID == iddokter);

        //                if (deskripsi == "Dokter")
        //                {

        //                    if (validasi == null)
        //                    {
        //                        return JsonConvert.SerializeObject(new
        //                        {
        //                            IsSuccess = false,
        //                        });
        //                    }


        //                }
        //                else if (deskripsi == "DPJP")
        //                {
        //                    if (validasi == null)
        //                    {
        //                        return JsonConvert.SerializeObject(new
        //                        {
        //                            IsSuccess = false,
        //                        });
        //                    }


        //                }
        //                else
        //                {

        //                    return JsonConvert.SerializeObject(new
        //                    {
        //                        IsSuccess = false,
        //                    });
        //                }

        //                var jak = new VerifikasiTTDViewModel()
        //                {
        //                    NamaDokterCPPT = nama_dokter.NamaDOkter
        //                };
        //                result = new ResultSS(1);

        //                //return JsonHelper.JsonMsgCreate(result);
        //                return new JavaScriptSerializer().Serialize(new ResultStatus()
        //                {
        //                    Data = jak,
        //                    IsSuccess = result.IsSuccess,
        //                    Count = 1,
        //                    Status = "success"
        //                });

        //            }

        //        }

        //    }


        //    catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
        //    catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }

        //}

        #endregion

        [HttpPost]
        [ActionName("Create")]
        public string EditCPPT_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new SOAPViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new EMR_Entities())
                    {
                        using (var j = new SIM_Entities())
                        {
                            var idsection = Request.Cookies["SectionIDPelayanan"].Value;
                            var model = s.SIMtrSOAP.FirstOrDefault(x => x.SectionID == idsection && x.NoReg == noreg && x.No == item.No);
                            var z = j.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg);
                            var section = j.SIMmSection.FirstOrDefault(x => x.SectionID == idsection);

                            if (model != null)
                            {

                                //model.DokterID = soapdokter;
                                //model.DPJP = dpjp;
                                //model.S = soaps;
                                //model.A = soapa;
                                //model.O = soapo;
                                //model.P = soapp;
                                //model.Instruksi = instruksi;
                                //model.Tanggal = DateTime.Today;
                                //model.TanggalInput = DateTime.Now;
                                //model.Username = User.Identity.GetUserName();
                                //model.ValidasiDokter = validasidokter;
                                //model.ValidasiDPJP = validasidpjp;

                                model = IConverter.Cast<SIMtrSOAP>(item);
                                DateTime date = model.Tanggal;
                                TimeSpan time = model.Jam;
                                model.TanggalInput = date + time;
                                s.SIMtrSOAP.AddOrUpdate(model);


                            }

                        }
                        result = new ResultSS(s.SaveChanges());
                        return JsonHelper.JsonMsgCreate(result);

                    }

                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public string GetEncodeUrlCPPT_DPJP(string id, string noreg, string section, int no)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCPPT/get_keycppt";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section + "&no=" + no);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlCPPT_MenerimaPasien(string id, string noreg, string section, int no)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCPPT/get_keycpptmenerimapasien";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section + "&no=" + no);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlCPPT_MenyerahkanPasien(string id, string noreg, string section, int no)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCPPT/get_keycpptmenyerahkanpasien";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section + "&no=" + no);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlCPPT_PetugasBacaUlang(string id, string noreg, string section, int no)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCPPT/get_keycpptpetugasbacaulang";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section + "&no=" + no);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlCPPT_PetugasKonfirmasi(string id, string noreg, string section, int no)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCPPT/get_keycpptpetugaskonfirmasi";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section + "&no=" + no);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        [HttpGet]
        public string CheckBerhasilCPPT(string noreg, string section, string id, int no)
        {
            var result = new ReturnFingerViewModel();
            var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
            using (var s = new EMR_Entities())
            {
                var finger = s.mFinger.Where(x => x.userid == id).FirstOrDefault();
                if (finger != null)
                {
                    var m = s.SIMtrSOAP.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid && x.No == no);
                    if (m != null)
                    {
                        if (m.TandaTanganDPJP != null)
                        {
                            result.DokterPengkaji = "1";
                            result.TandaTanganDPJP = Convert.ToBase64String(m.TandaTanganDPJP);
                        }
                        else
                        {
                            result.DokterPengkaji = "0";
                        }

                        if (m.TandaTanganMenerimaPasien != null)
                        {
                            result.MenerimaPasien = "1";
                            result.TandaTanganMenerimaPasien = Convert.ToBase64String(m.TandaTanganMenerimaPasien);
                        }
                        else
                        {
                            result.MenerimaPasien = "0";
                        }

                        if (m.TandaTanganMenyerahkanPasien != null)
                        {
                            result.MenyerahkanPasien = "1";
                            result.TandaTanganMenyerahkanPasien = Convert.ToBase64String(m.TandaTanganMenyerahkanPasien);
                        }
                        else
                        {
                            result.MenyerahkanPasien = "0";
                        }

                        if (m.TandaTanganPetugasBacaUlang != null)
                        {
                            result.PetugasBacaUlang = "1";
                            result.TandaTanganPetugasBacaUlang = Convert.ToBase64String(m.TandaTanganPetugasBacaUlang);
                        }
                        else
                        {
                            result.PetugasBacaUlang = "0";
                        }

                        if (m.TandaTanganPetugasKonfirmasi != null)
                        {
                            result.PetugasKonfirmasi = "1";
                            result.TandaTanganPetugasKonfirmasi = Convert.ToBase64String(m.TandaTanganPetugasKonfirmasi);
                        }
                        else
                        {
                            result.PetugasKonfirmasi = "0";
                        }
                    }
                }
                else
                {
                    result.DokterPengkaji = "0";
                    result.MenerimaPasien = "0";
                    result.MenyerahkanPasien = "0";
                    result.PetugasBacaUlang = "0";
                    result.PetugasKonfirmasi = "0";
                    result.TandaTanganDPJP = "";
                    result.TandaTanganMenerimaPasien = "";
                    result.TandaTanganMenyerahkanPasien = "";
                    result.TandaTanganPetugasBacaUlang = "";
                    result.TandaTanganPetugasKonfirmasi = "";
                }
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result
            });
        }
    }
}