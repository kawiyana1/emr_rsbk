﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    [Authorize(Roles = "Pelayanan")]
    public class MedicalRecordNewController : Controller
    {
        #region ======== E M R   A S S E S M E N T 

        #region ==== I N D E X
        public ActionResult Index(string noreg, int nomor = 0)
        {
            var model = new RegistrasiPasienViewModel();
            var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
            try
            {
                using (var s = new EMR_Entities())
                {
                    using (var eSIM = new SIM_Entities())
                    {
                        var dokter = eSIM.VW_DataPasienReg.FirstOrDefault(x => x.NoReg == noreg);
                        var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                        var alergi = s.EMR_FN_CekRiwayatAlergi().FirstOrDefault(x => x.NRM == m.NRM);
                        if (m != null)
                        {
                            model = IConverter.Cast<RegistrasiPasienViewModel>(m);
                            model.TglReg_View = model.TglReg.ToString("dd/MM/yyyy");
                            model.Tanggal_View = model.Tanggal.ToString("dd/MM/yyyy");
                            model.Jam_View = model.Jam.ToString("HH\":\"mm");
                            model.NRM = m.NRM;
                            model.JamMasuk = m.TglReg.ToString("dd/MM/yyyy");
                            model.Nomor = nomor;
                            model.PenanggungNama = m.PenanggungNama;
                            if (alergi == null)
                            {
                                model.Deskripsi = "Tidak ada";
                            }
                            else { 
                                model.Deskripsi = (alergi.Deskripsi == "" ? "Tidak ada" : alergi.Deskripsi);
                            }
                            model.PenanggungPhone = m.PenanggungPhone;
                            model.Agama = m.Agama;
                            model.TempatLahir = m.TempatLahir;
                            model.Nama_Asuransi = m.JenisKerjasama;
                            model.StatusBayar = m.StatusBayar;
                            model.DokterID = (dokter.DokterID == null ? "" : dokter.DokterID);
                            model.NamaDOkter = (dokter.NamaDOkter == null ? "" : dokter.NamaDOkter);
                            model.WargaNegara = m.Nationality;
                            model.Pendidikan = m.Pendidikan;
                            model.Pangkat = m.Pangkat;
                            model.Kesatuan = m.Kesatuan;
                            model.Pekerjaan = m.Pekerjaan;
                            model.NIP = m.NIP;
                            model.StatusKawin = m.StatusPerkawinan;
                            model.NoBPJS = m.NoKartu;
                            model.HubunganPasien = m.PenanggungHubungan;
                            model.Telp = m.Phone;
                            model.Desa = (m.DesaNama == null ? "-" : m.DesaNama);
                            model.Kecamatan = (m.KecamatanNama == null ? "-" : m.KecamatanNama);
                            model.Kabupaten = (m.Nama_Kabupaten == null ? "-" : m.Nama_Kabupaten);
                            model.Provinsi = (m.Nama_Propinsi == null ? "-" : m.Nama_Propinsi);
                        }

                        #region ==== C P P T  G E T
                        model.CPPT = new SOAPViewModel();
                        var cppt = s.SIMtrSOAP.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        model.CPPT.NRM = m.NRM;
                        model.CPPT.SectionID = sectionid;
                        model.CPPT.NoReg = noreg;
                        model.CPPT.Tanggal = DateTime.Today;
                        model.CPPT.Jam = DateTime.Now.TimeOfDay;
                        model.CPPT.TanggalKonfirmasi = DateTime.Today;
                        model.CPPT.TanggalBacaUlang = DateTime.Today;
                        model.CPPT.Nomor = nomor;
                        #endregion

                        #region ==== K U N J U N G A N  P O L I K L I N I K   G E T
                        model.KuPo = new KunjunganPoliklinikViewModel();
                        model.KuPo.NRM = m.NRM;
                        model.KuPo.SectionID = sectionid;
                        model.KuPo.NoReg = noreg;
                        model.KuPo.Tanggal = DateTime.Today;
                        model.KuPo.DokumenID = "Kunjungan Poliklinik";
                        model.KuPo.JamKunjunganPoliklinik_View = DateTime.Now;
                        #endregion

                        #region ==== A S S E S M E N  L A N J U T A N  K E P E R A W A T A N  G E T
                        model.AssLanjut_Kep = new AsesmenLanjutanKeperawatanViewModel();
                        model.AssLanjut_Kep.NRM = m.NRM;
                        model.AssLanjut_Kep.SectionID = sectionid;
                        model.AssLanjut_Kep.NoReg = noreg;
                        model.AssLanjut_Kep.Tanggal = DateTime.Today;
                        model.AssLanjut_Kep.Jam = DateTime.Now;
                        model.AssLanjut_Kep.DokumenID = "AsesmenLanjutanKeperawatan"; 
                        #endregion

                        #region ==== A S S E S M E N  L A N J U T A N  K E B I D A N A N  G E T
                        model.AssLanjut_Keb = new AsesmenLanjutanKebidananViewModel();
                        model.AssLanjut_Keb.NRM = m.NRM;
                        model.AssLanjut_Keb.SectionID = sectionid;
                        model.AssLanjut_Keb.NoReg = noreg;
                        model.AssLanjut_Keb.Tanggal = DateTime.Today;
                        model.AssLanjut_Keb.Jam = DateTime.Now;
                        model.AssLanjut_Keb.DokumenIDAssLnjutKeb = "AsesmenLanjutanKebidanan";
                        #endregion

                        #region ==== R E N P R A  R J   G E T
                        model.R_RJ = new Renpra_RJViewModel();
                        model.R_RJ.NRM = m.NRM;
                        model.R_RJ.SectionID = sectionid;
                        model.R_RJ.NoReg = noreg;
                        model.R_RJ.Tanggal = DateTime.Today;
                        model.R_RJ.DokumenID = "Renpra_RJ";
                        #endregion

                        #region ==== C E K  S U D A H  I S I  G E N E R A L
                        var cekcatatanedukasirirj = s.CatatanEdukasiTerintegrasiLanjutanRIdanRJ.FirstOrDefault(x => x.NoReg == noreg);
                        var cekcatatanterintegrasi = s.CatatanEdukasiTerintegrasiRJ.FirstOrDefault(x => x.NoReg == noreg);
                        if (cekcatatanedukasirirj == null)
                        {
                            model.CekEdukasiRIRJ = "1";
                        }
                        else
                        {
                            model.CekEdukasiRIRJ = "0";
                        }

                        if (cekcatatanterintegrasi == null)
                        {
                            model.CekEdukasiTerintegrasi = "1";
                        }
                        else
                        {
                            model.CekEdukasiTerintegrasi = "0";
                        }
                        #endregion

                        #region ==== A S E S M E N  A W A L  M E D I S
                        model.AssAwalMedisNew = new AssesmentAwalMedisNewViewModel();

                        var AssAwalMedis = s.AsesmenAwalMedis.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (AssAwalMedis != null)
                        {
                            model.AssAwalMedisNew = IConverter.Cast<AssesmentAwalMedisNewViewModel>(AssAwalMedis);
                            var dktassawalmedis = eSIM.mDokter.FirstOrDefault(x => x.DokterID == AssAwalMedis.NamaDokter);
                            if (dktassawalmedis != null)
                            {
                                model.AssAwalMedisNew.NamaDokterNama = dktassawalmedis.NamaDOkter;
                            }

                            model.AssAwalMedisNew.Report = 1;
                            model.AssAwalMedisNew.Nomor = nomor;
                            model.AssAwalMedisNew.DokumenID = "AssementAwalMedis";
                        }
                        else
                        {
                            AssAwalMedis = new AsesmenAwalMedis();
                            model.AssAwalMedisNew = IConverter.Cast<AssesmentAwalMedisNewViewModel>(AssAwalMedis);
                            model.AssAwalMedisNew.Tanggal = DateTime.Today;
                            model.AssAwalMedisNew.JamKedatangan = DateTime.Now;
                            model.AssAwalMedisNew.WaktuPengkajian = DateTime.Now;
                            model.AssAwalMedisNew.NoReg = noreg;
                            model.AssAwalMedisNew.NRM = model.NRM;
                            model.AssAwalMedisNew.SectionID = sectionid;
                            model.AssAwalMedisNew.Report = 0;
                            model.AssAwalMedisNew.Nomor = nomor;
                            model.AssAwalMedisNew.DokumenID = "AssementAwalMedis";
                        }

                        #region ==== Rencana Kerja
                        model.AssAwalMedisNew.RencanaKerja_List = new ListDetail<RencanaKerjaMedisDetailModel>();
                        var d_RencanaKerja = s.AsesmenAwalMedis_Detail.Where(x => x.NoReg == noreg && x.SectionID == sectionid).ToList();
                        if (d_RencanaKerja != null)
                        {
                            foreach (var x in d_RencanaKerja)
                            {
                                var y = IConverter.Cast<RencanaKerjaMedisDetailModel>(x);
                                model.AssAwalMedisNew.RencanaKerja_List.Add(false, y);
                            }
                        }

                        #endregion

                        #endregion

                        #region ==== A S E S M E N  A W A L  K E P E R A W A T
                        model.AssAwalPerawatNew = new AssesmentAwalPerawatNewViewModel();

                        var AssAwalKprwt = s.AsesmenAwalKeperawatan.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (AssAwalKprwt != null)
                        {
                            model.AssAwalPerawatNew = IConverter.Cast<AssesmentAwalPerawatNewViewModel>(AssAwalKprwt);
                            var perawat = eSIM.mDokter.FirstOrDefault(x => x.DokterID == AssAwalKprwt.Perawat);
                            if (perawat != null)
                            {
                                model.AssAwalPerawatNew.PerawatNama = perawat.NamaDOkter;
                            }

                            model.AssAwalPerawatNew.Report = 1;
                            model.AssAwalPerawatNew.Nomor = nomor;
                            model.AssAwalPerawatNew.DokumenID = "AssementAwalKeperawatan";
                        }
                        else
                        {
                            AssAwalKprwt = new AsesmenAwalKeperawatan();
                            model.AssAwalPerawatNew = IConverter.Cast<AssesmentAwalPerawatNewViewModel>(AssAwalKprwt);
                            model.AssAwalPerawatNew.Tanggal = DateTime.Today;
                            model.AssAwalPerawatNew.JamKedatangan = DateTime.Now;
                            model.AssAwalPerawatNew.WaktuPengkajian = DateTime.Now;
                            model.AssAwalPerawatNew.InfusTanggal = DateTime.Today;
                            model.AssAwalPerawatNew.CentralTanggal = DateTime.Today;
                            model.AssAwalPerawatNew.DowerTanggal = DateTime.Today;
                            model.AssAwalPerawatNew.NGTTanggal = DateTime.Today;
                            model.AssAwalPerawatNew.CystotomyTanggal = DateTime.Today;
                            model.AssAwalPerawatNew.TracheostomyTanggal = DateTime.Today;
                            model.AssAwalPerawatNew.InvasifLainTanggal = DateTime.Today;
                            model.AssAwalPerawatNew.EstimasiTanggal = DateTime.Today;
                            model.AssAwalPerawatNew.NoReg = noreg;
                            model.AssAwalPerawatNew.NRM = model.NRM;
                            model.AssAwalPerawatNew.SectionID = sectionid;
                            model.AssAwalPerawatNew.Report = 0;
                            model.AssAwalPerawatNew.Nomor = nomor;
                            model.AssAwalPerawatNew.DokumenID = "AssementAwalKeperawatan";
                        }
                        #endregion
                        
                        if (sectionid == "SEC002") {
                            #region ==== P E N G K A J I A N  M E D I S
                            model.PengkajianMedis = new PengkajianMedisViewModel();

                            var asspengkajianmedis = s.AsesmenPengkajianMedisSkrining.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                            if (asspengkajianmedis != null)
                            {
                                model.PengkajianMedis = IConverter.Cast<PengkajianMedisViewModel>(asspengkajianmedis);
                                model.PengkajianMedis.Nomor = nomor;
                                model.PengkajianMedis.Report = 1;
                                model.PengkajianMedis.DokumenID = "Pengkajian Medis IGD";

                                var _dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == model.PengkajianMedis.DokterPemeriksa);
                                var _dokter1 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == model.PengkajianMedis.NamaPerawat);
                                var _dokter2 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == model.PengkajianMedis.NamaBidan);
                                if (_dokter != null)
                                {
                                    model.PengkajianMedis.DokterPemeriksaNama = _dokter.NamaDOkter;
                                }
                                if (_dokter1 != null)
                                {
                                    model.PengkajianMedis.NamaPerawatNama = _dokter1.NamaDOkter;
                                }
                                if (_dokter2 != null)
                                {
                                    model.PengkajianMedis.NamaBidanNama = _dokter2.NamaDOkter;
                                }
                            }
                            else
                            {
                                asspengkajianmedis = new AsesmenPengkajianMedisSkrining();
                                model.PengkajianMedis = IConverter.Cast<PengkajianMedisViewModel>(asspengkajianmedis);
                                model.PengkajianMedis.Nomor = nomor;
                                model.PengkajianMedis.Report = 0;
                                model.PengkajianMedis.NoReg = noreg;
                                model.PengkajianMedis.NRM = model.NRM;
                                model.PengkajianMedis.SectionID = sectionid;
                                model.PengkajianMedis.Tanggal = DateTime.Today;
                                model.PengkajianMedis.JamKedatangan = DateTime.Now;
                                model.PengkajianMedis.WaktuPengkajian = DateTime.Now;
                                model.PengkajianMedis.Dipulangkan_Tgl = DateTime.Today;
                                model.PengkajianMedis.Meninggal_JamMeninggal = DateTime.Now;
                                model.PengkajianMedis.Minggat_JamMinggat = DateTime.Now;
                                model.PengkajianMedis.SkriningGizi_Tgl = DateTime.Today;
                                model.PengkajianMedis.Tgl_SkriningTuberkulosis = DateTime.Today;
                                model.PengkajianMedis.DokumenID = "Pengkajian Medis IGD";
                            }

                            #region ==== Pengkajian Medis Detail

                            model.PengkajianMedis.Detail_List = new ListDetail<PengkajianMedis_DetailViewModel>();
                            var d_RencanaKerja1 = s.AsesmenPengkajianMedisSkrining_Detail.Where(x => x.NoReg == noreg && x.SectionID == sectionid).ToList();
                            if (d_RencanaKerja1 != null)
                            {
                                foreach (var x in d_RencanaKerja1)
                                {
                                    var y = IConverter.Cast<PengkajianMedis_DetailViewModel>(x);
                                    model.PengkajianMedis.Detail_List.Add(false, y);
                                }
                            }
                            #endregion

                            #endregion
                        }

                        else if (sectionid == "SEC004" || sectionid == "SEC003")
                        {
                            #region ==== A S S E S M E N T  P R A   A N E S T E S I  D A N  S E D A S I

                            model.AssPraAnesDanSedasi = new AssesmentPraAnestesiDanSedasiViewModel();

                            var AssPraAnestesiSedasi = s.AssesmentPraAnestesiDanSedasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                            if (AssPraAnestesiSedasi != null)
                            {
                                model.AssPraAnesDanSedasi = IConverter.Cast<AssesmentPraAnestesiDanSedasiViewModel>(AssPraAnestesiSedasi);

                                var dokterpranaestesisedasi = eSIM.mDokter.FirstOrDefault(x => x.DokterID == AssPraAnestesiSedasi.NamaDokter);
                                if (dokterpranaestesisedasi != null)
                                {
                                    model.AssPraAnesDanSedasi.NamaDokterNama = dokterpranaestesisedasi.NamaDOkter;
                                }

                                model.AssPraAnesDanSedasi.Report = 1;
                                model.AssPraAnesDanSedasi.Nomor = nomor;
                                model.AssPraAnesDanSedasi.NamaPasien = model.NamaPasien;
                                model.AssPraAnesDanSedasi.JenisKelamin = model.JenisKelamin;

                                var asspraasnesdansedasi = s.AssesmentPraAnestesiDanSedasi.Where(x => x.NamaDokter == model.AssPraAnesDanSedasi.NamaDokter && x.TandaTanganDokter == null && x.NRM == model.NRM).FirstOrDefault();
                                if (asspraasnesdansedasi != null)
                                {
                                    model.AssPraAnesDanSedasi.SudahRegDokter = 1;
                                }
                                else
                                {
                                    model.AssPraAnesDanSedasi.SudahRegDokter = 0;
                                }
                                //if (model.AssPraAnesDanSedasi.NamaDokter != null)
                                //{
                                //    ViewBag.UrlFingerAssPraAnesDanSedasiDokter = GetEncodeUrlAssPraAnesDanSedasi_Dokter(model.AssPraAnesDanSedasi.NamaDokter, model.NoReg, sectionid);
                                //}
                            }
                            else
                            {
                                AssPraAnestesiSedasi = new AssesmentPraAnestesiDanSedasi();
                                model.AssPraAnesDanSedasi = IConverter.Cast<AssesmentPraAnestesiDanSedasiViewModel>(AssPraAnestesiSedasi);
                                //ViewBag.UrlFingerAssPraAnesDanSedasiDokter = GetEncodeUrlAssPraAnesDanSedasi_Dokter(model.AssPraAnesDanSedasi.NamaDokter, model.NoReg, sectionid);
                                model.AssPraAnesDanSedasi.Report = 0;
                                model.AssPraAnesDanSedasi.NoReg = noreg;
                                model.AssPraAnesDanSedasi.NRM = model.NRM;
                                model.AssPraAnesDanSedasi.SectionID = sectionid;
                                model.AssPraAnesDanSedasi.Nomor = nomor;
                                model.AssPraAnesDanSedasi.NamaPasien = model.NamaPasien;
                                model.AssPraAnesDanSedasi.JenisKelamin = model.JenisKelamin;
                                model.AssPraAnesDanSedasi.Tanggal = DateTime.Today;
                                model.AssPraAnesDanSedasi.Jam = DateTime.Now;
                                model.AssPraAnesDanSedasi.JamBerakhirAssesmen = DateTime.Now;
                                model.AssPraAnesDanSedasi.RB_Waktu = DateTime.Now;

                            }

                            #region ==== Saran

                            model.AssPraAnesDanSedasi.Saran_List = new ListDetail<AssesmentPraAnestesiDanSedasiModelDetail>();
                            var d_Saran = s.PraAnestesiDanSedasi_Detail.Where(x => x.NoReg == noreg && x.SectionID == sectionid).ToList();
                            if (d_Saran != null)
                            {
                                foreach (var x in d_Saran)
                                {
                                    var y = IConverter.Cast<AssesmentPraAnestesiDanSedasiModelDetail>(x);
                                    model.AssPraAnesDanSedasi.Saran_List.Add(false, y);
                                }
                            }

                            #endregion

                            #endregion

                            #region ====  A S S E S M E N T  P R A   O P E R A S I  P E R E M P U A N

                            model.AssPraOprsPrmpn = new AssesmentIPraOperasiPerempuanViewModel();

                            var AssPraOprasiPerempuaan = s.AssesmentPraOperasiPerempuan.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                            if (AssPraOprasiPerempuaan != null)
                            {
                                model.AssPraOprsPrmpn = IConverter.Cast<AssesmentIPraOperasiPerempuanViewModel>(AssPraOprasiPerempuaan);

                                var dpjp = eSIM.mDokter.FirstOrDefault(x => x.DokterID == AssPraOprasiPerempuaan.DPJPAnastesi);
                                if (dpjp != null)
                                {
                                    model.AssPraOprsPrmpn.DPJPAnastesiNama = dpjp.NamaDOkter;
                                }

                                var dpjp1 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == AssPraOprasiPerempuaan.DPJPOperator);
                                if (dpjp1 != null)
                                {
                                    model.AssPraOprsPrmpn.DPJPOperatorNama = dpjp1.NamaDOkter;
                                }

                                model.AssPraOprsPrmpn.Report = 1;
                                model.AssPraOprsPrmpn.Nomor = nomor;
                                model.AssPraOprsPrmpn.NamaPasien = model.NamaPasien;
                                var fingerpasien = s.AssesmentPraOperasiPerempuan.Where(x => x.NRM == model.NRM && x.TandaTangan == null).FirstOrDefault();
                                if (fingerpasien != null)
                                {
                                    model.AssPraOprsPrmpn.SudahRegPasien = 1;
                                }
                                else
                                {
                                    model.AssPraOprsPrmpn.SudahRegPasien = 0;
                                }

                                var fingerpraoperasiprpndpjp = s.AssesmentPraOperasiPerempuan.Where(x => x.DPJPAnastesi == model.AssPraOprsPrmpn.DPJPAnastesi && x.TandaTanganDPJPAnestesi == null && x.NRM == model.NRM).FirstOrDefault();
                                if (fingerpraoperasiprpndpjp != null)
                                {
                                    model.AssPraOprsPrmpn.SudahRegDPJPAnestesi = 1;
                                }
                                else
                                {
                                    model.AssPraOprsPrmpn.SudahRegDPJPAnestesi = 0;
                                }
                                var fingerpraoperasiprpnperawat = s.AssesmentPraOperasiPerempuan.Where(x => x.DPJPOperator == model.AssPraOprsPrmpn.DPJPOperator && x.TandaTanganDPJPOperator == null && x.NRM == model.NRM).FirstOrDefault();
                                if (fingerpraoperasiprpnperawat != null)
                                {
                                    model.AssPraOprsPrmpn.SudahRegDPJPOperator = 1;
                                }
                                else
                                {
                                    model.AssPraOprsPrmpn.SudahRegDPJPOperator = 0;
                                }

                                //if (model.AssPraOprsPrmpn.DPJPOperator != null)
                                //{
                                //    ViewBag.UrlFingerAssPraOprPrpnDPJPAnestesi = GetEncodeUrlAssPraOprPrpn_DPJPAnastesi(model.AssPraOprsPrmpn.DPJPAnastesi, model.NoReg, sectionid);
                                //}
                                //if (model.AssPraOprsPrmpn.DPJPOperator != null)
                                //{
                                //    ViewBag.UrlFingerAssPraOprPrpnDPJPOperator = GetEncodeUrlAssPraOprPrpn_DPJPOperasi(model.AssPraOprsPrmpn.DPJPOperator, model.NoReg, sectionid);
                                //}
                                //ViewBag.UrlFingerAssPraOprPrpn = GetEncodeUrlAssPraOprPrpn(model.NRM, model.NoReg, sectionid);
                            }
                            else
                            {
                                AssPraOprasiPerempuaan = new AssesmentPraOperasiPerempuan();
                                model.AssPraOprsPrmpn = IConverter.Cast<AssesmentIPraOperasiPerempuanViewModel>(AssPraOprasiPerempuaan);
                                //ViewBag.UrlFingerAssPraOprPrpnDPJPAnestesi = GetEncodeUrlAssPraOprPrpn_DPJPAnastesi(model.AssPraOprsPrmpn.DPJPAnastesi, model.NoReg, sectionid);
                                //ViewBag.UrlFingerAssPraOprPrpnDPJPOperator = GetEncodeUrlAssPraOprPrpn_DPJPOperasi(model.AssPraOprsPrmpn.DPJPOperator, model.NoReg, sectionid);
                                //ViewBag.UrlFingerAssPraOprPrpn = GetEncodeUrlAssPraOprPrpn(model.NRM, model.NoReg, sectionid);
                                model.AssPraOprsPrmpn.NamaPasien = model.NamaPasien;
                                model.AssPraOprsPrmpn.Report = 0;
                                model.AssPraOprsPrmpn.NoReg = noreg;
                                model.AssPraOprsPrmpn.NRM = model.NRM;
                                model.AssPraOprsPrmpn.SectionID = sectionid;
                                model.AssPraOprsPrmpn.Nomor = nomor;
                                model.AssPraOprsPrmpn.Tanggal = DateTime.Today;
                                model.AssPraOprsPrmpn.Jam = DateTime.Now;
                                model.AssPraOprsPrmpn.Pemeriksaan_Jam1 = DateTime.Now;
                                model.AssPraOprsPrmpn.Pemeriksaan_Jam2 = DateTime.Now;
                                model.AssPraOprsPrmpn.TanggalProsedur = DateTime.Today;

                            }

                            #endregion

                            #region ====  A S S E S M E N T  P R A   O P E R A S I  L A K I  L A K I

                            model.AssPraOprsLaki = new AssesmentIPraOperasiLakiLakiViewModel();

                            var AssPraOprasiLakiLaki = s.AssesmentPraOperasiLakiLaki.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                            if (AssPraOprasiLakiLaki != null)
                            {
                                model.AssPraOprsLaki = IConverter.Cast<AssesmentIPraOperasiLakiLakiViewModel>(AssPraOprasiLakiLaki);

                                var dpjp = eSIM.mDokter.FirstOrDefault(x => x.DokterID == AssPraOprasiLakiLaki.DPJPAnastesi);
                                if (dpjp != null)
                                {
                                    model.AssPraOprsLaki.DPJPAnastesiNama = dpjp.NamaDOkter;
                                }

                                var dpjp1 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == AssPraOprasiLakiLaki.DPJPOperator);
                                if (dpjp1 != null)
                                {
                                    model.AssPraOprsLaki.DPJPOperatorNama = dpjp1.NamaDOkter;
                                }

                                model.AssPraOprsLaki.Report = 1;
                                model.AssPraOprsLaki.Nomor = nomor;
                                model.AssPraOprsLaki.NamaPasien = model.NamaPasien;

                                var fingerpasien = s.AssesmentPraOperasiLakiLaki.Where(x => x.NRM == model.NRM && x.TandaTangan == null).FirstOrDefault();
                                if (fingerpasien != null)
                                {
                                    model.AssPraOprsPrmpn.SudahRegPasien = 1;
                                }
                                else
                                {
                                    model.AssPraOprsPrmpn.SudahRegPasien = 0;
                                }

                                var fingerpraoprlakidpjp = s.AssesmentPraOperasiLakiLaki.Where(x => x.DPJPAnastesi == model.AssPraOprsLaki.DPJPAnastesi && x.TandaTanganDPJPAnestesi == null && x.NRM == model.NRM).FirstOrDefault();
                                if (fingerpraoprlakidpjp != null)
                                {
                                    model.AssPraOprsLaki.SudahRegDPJPAnestesi = 1;
                                }
                                else
                                {
                                    model.AssPraOprsLaki.SudahRegDPJPAnestesi = 0;
                                }
                                var fingerpraoprlakiperawat = s.AssesmentPraOperasiLakiLaki.Where(x => x.DPJPOperator == model.AssPraOprsLaki.DPJPOperator && x.TandaTanganDPJPOperator == null && x.NRM == model.NRM).FirstOrDefault();
                                if (fingerpraoprlakiperawat != null)
                                {
                                    model.AssPraOprsLaki.SudahRegDPJPOperator = 1;
                                }
                                else
                                {
                                    model.AssPraOprsLaki.SudahRegDPJPOperator = 0;
                                }

                                //if (model.AssPraOprsLaki.DPJPOperator != null)
                                //{
                                //    ViewBag.UrlFingerAssPraOprLakiDPJPAnestesi = GetEncodeUrlAssPraOprLaki_DPJPAnastesi(model.AssPraOprsLaki.DPJPAnastesi, model.NoReg, sectionid);
                                //}
                                //if (model.AssPraOprsLaki.DPJPOperator != null)
                                //{
                                //    ViewBag.UrlFingerAssPraOprLakiDPJPOperator = GetEncodeUrlAssPraOprLaki_DPJPOperasi(model.AssPraOprsLaki.DPJPOperator, model.NoReg, sectionid);
                                //}
                                //ViewBag.UrlFingerAssPraOprLaki = GetEncodeUrlAssPraOprLaki(model.NRM, model.NoReg, sectionid);
                            }
                            else
                            {
                                AssPraOprasiLakiLaki = new AssesmentPraOperasiLakiLaki();
                                model.AssPraOprsLaki = IConverter.Cast<AssesmentIPraOperasiLakiLakiViewModel>(AssPraOprasiLakiLaki);
                                //ViewBag.UrlFingerAssPraOprLakiDPJPAnestesi = GetEncodeUrlAssPraOprLaki_DPJPAnastesi(model.AssPraOprsLaki.DPJPAnastesi, model.NoReg, sectionid);
                                //ViewBag.UrlFingerAssPraOprLakiDPJPOperator = GetEncodeUrlAssPraOprLaki_DPJPOperasi(model.AssPraOprsLaki.DPJPOperator, model.NoReg, sectionid);
                                //ViewBag.UrlFingerAssPraOprLaki = GetEncodeUrlAssPraOprLaki(model.NRM, model.NoReg, sectionid);
                                model.AssPraOprsLaki.NamaPasien = model.NamaPasien;
                                model.AssPraOprsLaki.Report = 0;
                                model.AssPraOprsLaki.NoReg = noreg;
                                model.AssPraOprsLaki.NRM = model.NRM;
                                model.AssPraOprsLaki.SectionID = sectionid;
                                model.AssPraOprsLaki.Nomor = nomor;
                                model.AssPraOprsLaki.Tanggal = DateTime.Today;
                                model.AssPraOprsLaki.Jam = DateTime.Now;
                                model.AssPraOprsLaki.Pemeriksaan_Jam1 = DateTime.Now;
                                model.AssPraOprsLaki.Pemeriksaan_Jam2 = DateTime.Now;
                                model.AssPraOprsLaki.TanggalProsedur = DateTime.Today;

                            }

                            #endregion

                            #region ==== P O L I  K E B I D A N A N

                            model.PAwalKbdnKndng = new PengkajianAwalKebidananKandunganViewModel();

                            var PengkajianKebidananan = s.PengkajianAwalKebidananKandungan.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                            if (PengkajianKebidananan != null)
                            {
                                model.PAwalKbdnKndng = IConverter.Cast<PengkajianAwalKebidananKandunganViewModel>(PengkajianKebidananan);
                                var dokterpolibidan = eSIM.mDokter.FirstOrDefault(x => x.DokterID == PengkajianKebidananan.Dokter);
                                if (dokterpolibidan != null)
                                {
                                    model.PAwalKbdnKndng.DokterNama = dokterpolibidan.NamaDOkter;
                                }
                                var dpjp = eSIM.mDokter.FirstOrDefault(x => x.DokterID == PengkajianKebidananan.DPJP);
                                if (dpjp != null)
                                {
                                    model.PAwalKbdnKndng.DPJPNama = dpjp.NamaDOkter;
                                }
                                var bidan = eSIM.mDokter.FirstOrDefault(x => x.DokterID == PengkajianKebidananan.Bidan);
                                if (bidan != null)
                                {
                                    model.PAwalKbdnKndng.BidanNama = bidan.NamaDOkter;
                                }
                                var pemeriksa = eSIM.mDokter.FirstOrDefault(x => x.DokterID == PengkajianKebidananan.Pemeriksaan_Oleh);
                                if (pemeriksa != null)
                                {
                                    model.PAwalKbdnKndng.Pemeriksaan_OlehNama = pemeriksa.NamaDOkter;
                                }

                                var fingerkebidandpjp = s.PengkajianAwalKebidananKandungan.Where(x => x.DPJP == model.PAwalKbdnKndng.DPJP && x.TandaTanganDPJP == null && x.NRM == model.NRM).FirstOrDefault();
                                if (fingerkebidandpjp != null)
                                {
                                    model.PAwalKbdnKndng.SudahRegDPJP = 1;
                                }
                                else
                                {
                                    model.PAwalKbdnKndng.SudahRegDPJP = 0;
                                }
                                var fingerkebidanperawat = s.PengkajianAwalKebidananKandungan.Where(x => x.Bidan == model.PAwalKbdnKndng.Bidan && x.TandaTanganBidan == null && x.NRM == model.NRM).FirstOrDefault();
                                if (fingerkebidanperawat != null)
                                {
                                    model.PAwalKbdnKndng.SudahRegPerawat = 1;
                                }
                                else
                                {
                                    model.PAwalKbdnKndng.SudahRegPerawat = 0;
                                }
                                var fingerkebidanandokter = s.PengkajianAwalKebidananKandungan.Where(x => x.Dokter == model.PAwalKbdnKndng.Dokter && x.TandaTanganDokter == null && x.NRM == model.NRM).FirstOrDefault();
                                if (fingerkebidanandokter != null)
                                {
                                    model.PAwalKbdnKndng.SudahRegDokter = 1;
                                }
                                else
                                {
                                    model.PAwalKbdnKndng.SudahRegDokter = 0;
                                }

                                //if (model.PAwalKbdnKndng.DPJP != null)
                                //{
                                //    ViewBag.UrlFingerPAwalKbdnKndngDPJP = GetEncodeUrlPAwalKbdnKndng_DPJP(model.PAwalKbdnKndng.DPJP, model.NoReg, sectionid);
                                //}
                                //if (model.PAwalKbdnKndng.Bidan != null)
                                //{
                                //    ViewBag.UrlFingerPAwalKbdnKndngBidan = GetEncodeUrlPAwalKbdnKndng_Bidan(model.PAwalKbdnKndng.Bidan, model.NoReg, sectionid);
                                //}
                                //if (model.PAwalKbdnKndng.Dokter != null)
                                //{
                                //    ViewBag.UrlFingerPAwalKbdnKndngDokter = GetEncodeUrlPAwalKbdnKndng_Dokter(model.PAwalKbdnKndng.Dokter, model.NoReg, sectionid);
                                //}

                                model.PAwalKbdnKndng.Report = 1;
                                model.PAwalKbdnKndng.Nomor = nomor;
                            }
                            else
                            {
                                PengkajianKebidananan = new PengkajianAwalKebidananKandungan();
                                model.PAwalKbdnKndng = IConverter.Cast<PengkajianAwalKebidananKandunganViewModel>(PengkajianKebidananan);
                                //ViewBag.UrlFingerPAwalKbdnKndngDPJP = GetEncodeUrlPAwalKbdnKndng_DPJP(model.PAwalKbdnKndng.DPJP, model.NoReg, sectionid);
                                //ViewBag.UrlFingerPAwalKbdnKndngPerawat = GetEncodeUrlPAwalKbdnKndng_Bidan(model.PAwalKbdnKndng.Bidan, model.NoReg, sectionid);
                                //ViewBag.UrlFingerPAwalKbdnKndngDokter = GetEncodeUrlPAwalKbdnKndng_Dokter(model.PAwalKbdnKndng.Dokter, model.NoReg, sectionid);
                                model.PAwalKbdnKndng.Tanggal = DateTime.Today;
                                model.PAwalKbdnKndng.Jam = DateTime.Now;
                                model.PAwalKbdnKndng.PernahDioperasi_Tanggal = DateTime.Today;
                                model.PAwalKbdnKndng.PernahDirawat_Tanggal = DateTime.Today;
                                model.PAwalKbdnKndng.Pemeriksaan_Tgl = DateTime.Today;
                                model.PAwalKbdnKndng.Pemeriksaan_Jam = DateTime.Now;
                                model.PAwalKbdnKndng.NoReg = noreg;
                                model.PAwalKbdnKndng.NRM = model.NRM;
                                model.PAwalKbdnKndng.SectionID = sectionid;
                                model.PAwalKbdnKndng.Report = 0;
                                model.PAwalKbdnKndng.Nomor = nomor;
                            }

                            #region ==== R i w a y a t  K a n d u n g a n  D e t a i l

                            model.PAwalKbdnKndng.RiwayatKehamilan_List = new ListDetail<RiwayatKehamilan_DetailViewModel>();
                            var d_RiwayatKandungan = s.RiwayatKehamilan_Detail.Where(x => x.NoReg == noreg && x.SectionID == sectionid);
                            if (d_RiwayatKandungan != null)
                            {
                                foreach (var x in d_RiwayatKandungan)
                                {
                                    var y = IConverter.Cast<RiwayatKehamilan_DetailViewModel>(x);
                                    model.PAwalKbdnKndng.RiwayatKehamilan_List.Add(false, y);
                                }
                            }

                            #endregion

                            #region ==== D e t a i l  P e n a t a  L a k s a n a  G y n e c o l o g y
                            model.PAwalKbdnKndng.Penataan_List = new ListDetail<PenataLaksanaKebidananViewModelDetail>();
                            var d_RencanaKerja_1 = s.PenataLaksanaKebidanan_Detail.Where(x => x.NoReg == noreg && x.SectionID == sectionid).ToList();
                            if (d_RencanaKerja_1 != null)
                            {
                                foreach (var x in d_RencanaKerja_1)
                                {
                                    var y = IConverter.Cast<PenataLaksanaKebidananViewModelDetail>(x);
                                    var cek = s.mFinger.Where(xx => xx.userid == x.Paraf).FirstOrDefault();
                                    if (cek != null)
                                    {
                                        if (y.TTDDokter == Convert.ToBase64String(cek.Gambar1) && y.Paraf == cek.userid)
                                        {
                                            y.SudahRegDkt = 0;
                                        }
                                        else
                                        {
                                            y.SudahRegDkt = 1;
                                        }
                                    }
                                    else
                                    {
                                        y.SudahRegDkt = 1;
                                    }

                                    //if (y.Paraf != null)
                                    //{
                                    //    ViewBag.UrlFingerDtlPenataDokter = GetEncodeUrlDtlPenataan_Dokter(y.Paraf, y.NoReg, sectionid, y.No);
                                    //    y.Viewbag = ViewBag.UrlFingerDtlPenataDokter;
                                    //}
                                    var namadokter2 = eSIM.mDokter.FirstOrDefault(z => z.DokterID == y.Paraf);
                                    y.ParafNama = namadokter2 == null ? "" : namadokter2.NamaDOkter;
                                    model.PAwalKbdnKndng.Penataan_List.Add(false, y);
                                }
                            }

                            #endregion


                            #endregion

                            #region ==== I L M U  P E N Y A K I T  G Y N E C O L O G Y

                            model.Assgynclogy = new AssesmentIlmuPenyakitGynecologyViewModel();

                            var AssesmentGynecology = s.IlmuPenyakitGynecology.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                            if (AssesmentGynecology != null)
                            {
                                model.Assgynclogy = IConverter.Cast<AssesmentIlmuPenyakitGynecologyViewModel>(AssesmentGynecology);

                                var dpjp = eSIM.mDokter.FirstOrDefault(x => x.DokterID == AssesmentGynecology.DokterBPJP);
                                if (dpjp != null)
                                {
                                    model.Assgynclogy.DokterBPJPNama = dpjp.NamaDOkter;
                                }

                                var fingergynecodpjp = s.IlmuPenyakitGynecology.Where(x => x.DokterBPJP == model.Assgynclogy.DokterBPJP && x.TandaTanganDPJP == null && x.NRM == model.NRM).FirstOrDefault();
                                if (fingergynecodpjp != null)
                                {
                                    model.Assgynclogy.SudahRegDokter = 1;
                                }
                                else
                                {
                                    model.Assgynclogy.SudahRegDokter = 0;
                                }
                                //if (model.Assgynclogy.DokterBPJP != null)
                                //{
                                //    ViewBag.UrlFingerAssgynclogyDPJP = GetEncodeUrlAssgynclogy_DPJP(model.Assgynclogy.DokterBPJP, model.NoReg, sectionid);
                                //}
                                model.Assgynclogy.Report = 1;
                                model.Assgynclogy.Nomor = nomor;
                            }
                            else
                            {
                                AssesmentGynecology = new IlmuPenyakitGynecology();
                                model.Assgynclogy = IConverter.Cast<AssesmentIlmuPenyakitGynecologyViewModel>(AssesmentGynecology);
                                //ViewBag.UrlFingerAssgynclogyDPJP = GetEncodeUrlAssgynclogy_DPJP(model.Assgynclogy.DokterBPJP, model.NoReg, sectionid);
                                model.Assgynclogy.Tanggal = DateTime.Today;
                                model.Assgynclogy.Jam = DateTime.Now;
                                model.Assgynclogy.KontrolPoliklinik_Tanggal = DateTime.Today;
                                model.Assgynclogy.Hispatologi = DateTime.Today;
                                model.Assgynclogy.RiwayatPenyakitDahulu_Pernah_Tgl = DateTime.Today;
                                model.Assgynclogy.RiwayatMenstruasi_HCG_Ket = DateTime.Today;
                                model.Assgynclogy.BolehPulangJam = DateTime.Now;
                                model.Assgynclogy.NoReg = noreg;
                                model.Assgynclogy.NRM = model.NRM;
                                model.Assgynclogy.SectionID = sectionid;
                                model.Assgynclogy.Report = 0;
                                model.Assgynclogy.Nomor = nomor;
                                model.Assgynclogy.NamaKeluarga = model.PenanggungNama;
                                model.Assgynclogy.TeleponKeluarga = model.PenanggungPhone;
                                model.Assgynclogy.AlamatKeluarga = model.Alamat;
                            }
                            #endregion

                            #region ==== I L M U  P E N Y A K I T  O B S E T R I

                            model.Assobstri = new AssesmentIlmuPenyakitObsetriViewModel();

                            var AssesmentObsetrii = s.IlmuPenyakitObsetri.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                            if (AssesmentObsetrii != null)
                            {
                                model.Assobstri = IConverter.Cast<AssesmentIlmuPenyakitObsetriViewModel>(AssesmentObsetrii);

                                var dpjp = eSIM.mDokter.FirstOrDefault(x => x.DokterID == AssesmentObsetrii.DokterBPJP);
                                if (dpjp != null)
                                {
                                    model.Assobstri.DokterBPJPNama = dpjp.NamaDOkter;
                                }

                                var fingerobsetridpjp = s.IlmuPenyakitObsetri.Where(x => x.DokterBPJP == model.Assobstri.DokterBPJP && x.TandaTanganDPJP == null && x.NRM == model.NRM).FirstOrDefault();
                                if (fingerobsetridpjp != null)
                                {
                                    model.Assobstri.SudahRegDokter = 1;
                                }
                                else
                                {
                                    model.Assobstri.SudahRegDokter = 0;
                                }
                                //if (model.Assobstri.DokterBPJP != null)
                                //{
                                //    ViewBag.UrlFingerAssobstriDPJP = GetEncodeUrlAssobstri_DPJP(model.Assobstri.DokterBPJP, model.NoReg, sectionid);
                                //}
                                model.Assobstri.Report = 1;
                                model.Assobstri.Nomor = nomor;
                            }
                            else
                            {
                                AssesmentObsetrii = new IlmuPenyakitObsetri();
                                model.Assobstri = IConverter.Cast<AssesmentIlmuPenyakitObsetriViewModel>(AssesmentObsetrii);
                                //ViewBag.UrlFingerAssobstriDPJP = GetEncodeUrlAssobstri_DPJP(model.Assobstri.DokterBPJP, model.NoReg, sectionid);
                                model.Assobstri.Tanggal = DateTime.Today;
                                model.Assobstri.Jam = DateTime.Now;
                                model.Assobstri.KontrolPoliklinik_Tanggal = DateTime.Today;
                                model.Assobstri.BolehPulangTgl = DateTime.Today;
                                model.Assobstri.BolehPulangJam = DateTime.Now;
                                model.Assobstri.NoReg = noreg;
                                model.Assobstri.NRM = model.NRM;
                                model.Assobstri.SectionID = sectionid;
                                model.Assobstri.Report = 0;
                                model.Assobstri.Nomor = nomor;
                                model.Assobstri.KeluhanUtama = model.KeluhanUtama;
                            }
                            #endregion
                        }

                        else if (sectionid == "SEC062" || sectionid == "SEC063" || sectionid == "SEC064" || sectionid == "SECT0009" || sectionid == "SECT0010")
                        {
                            #region ==== P E N G K A J I A N  A W A L  K E B I D A N A N  &  K A N D U N G A N

                            model.PAwalKbdnKndng = new PengkajianAwalKebidananKandunganViewModel();

                            var PengkajianKebidananan = s.PengkajianAwalKebidananKandungan.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                            if (PengkajianKebidananan != null)
                            {
                                model.PAwalKbdnKndng = IConverter.Cast<PengkajianAwalKebidananKandunganViewModel>(PengkajianKebidananan);
                                var dokterkebidanan = eSIM.mDokter.FirstOrDefault(x => x.DokterID == PengkajianKebidananan.Dokter);
                                if (dokterkebidanan != null)
                                {
                                    model.PAwalKbdnKndng.DokterNama = dokterkebidanan.NamaDOkter;
                                }

                                var dpjp = eSIM.mDokter.FirstOrDefault(x => x.DokterID == PengkajianKebidananan.DPJP);
                                if (dpjp != null)
                                {
                                    model.PAwalKbdnKndng.DPJPNama = dpjp.NamaDOkter;
                                }

                                var bidan = eSIM.mDokter.FirstOrDefault(x => x.DokterID == PengkajianKebidananan.Bidan);
                                if (bidan != null)
                                {
                                    model.PAwalKbdnKndng.BidanNama = bidan.NamaDOkter;
                                }

                                var pemeriksa = eSIM.mDokter.FirstOrDefault(x => x.DokterID == PengkajianKebidananan.Pemeriksaan_Oleh);
                                if (pemeriksa != null)
                                {
                                    model.PAwalKbdnKndng.Pemeriksaan_OlehNama = pemeriksa.NamaDOkter;
                                }


                                model.PAwalKbdnKndng.Report = 1;
                                model.PAwalKbdnKndng.Nomor = nomor;
                            }
                            else
                            {
                                PengkajianKebidananan = new PengkajianAwalKebidananKandungan();
                                model.PAwalKbdnKndng = IConverter.Cast<PengkajianAwalKebidananKandunganViewModel>(PengkajianKebidananan);
                                model.PAwalKbdnKndng.Tanggal = DateTime.Today;
                                model.PAwalKbdnKndng.Jam = DateTime.Now;
                                model.PAwalKbdnKndng.PernahDioperasi_Tanggal = DateTime.Today;
                                model.PAwalKbdnKndng.PernahDirawat_Tanggal = DateTime.Today;
                                model.PAwalKbdnKndng.Pemeriksaan_Tgl = DateTime.Today;
                                model.PAwalKbdnKndng.Pemeriksaan_Jam = DateTime.Now;
                                model.PAwalKbdnKndng.NoReg = noreg;
                                model.PAwalKbdnKndng.NRM = model.NRM;
                                model.PAwalKbdnKndng.SectionID = sectionid;
                                model.PAwalKbdnKndng.Report = 0;
                                model.PAwalKbdnKndng.Nomor = nomor;
                            }

                            #region ==== R i w a y a t  K a n d u n g a n  D e t a i l

                            model.PAwalKbdnKndng.RiwayatKehamilan_List = new ListDetail<RiwayatKehamilan_DetailViewModel>();
                            var d_RiwayatKandungan = s.RiwayatKehamilan_Detail.Where(x => x.NoReg == noreg && x.SectionID == sectionid);
                            if (d_RiwayatKandungan != null)
                            {
                                foreach (var x in d_RiwayatKandungan)
                                {
                                    var y = IConverter.Cast<RiwayatKehamilan_DetailViewModel>(x);
                                    model.PAwalKbdnKndng.RiwayatKehamilan_List.Add(false, y);
                                }
                            }

                            #endregion

                            #region ==== D e t a i l  P e n a t a  L a k s a n a  G y n e c o l o g y
                            model.PAwalKbdnKndng.Penataan_List = new ListDetail<PenataLaksanaKebidananViewModelDetail>();
                            var d_RencanaKerja_1 = s.PenataLaksanaKebidanan_Detail.Where(x => x.NoReg == noreg && x.SectionID == sectionid).ToList();
                            if (d_RencanaKerja_1 != null)
                            {
                                foreach (var x in d_RencanaKerja_1)
                                {
                                    var y = IConverter.Cast<PenataLaksanaKebidananViewModelDetail>(x);
                                    var namadokter2 = eSIM.mDokter.FirstOrDefault(z => z.DokterID == y.Paraf);
                                    y.ParafNama = namadokter2 == null ? "" : namadokter2.NamaDOkter;
                                    model.PAwalKbdnKndng.Penataan_List.Add(false, y);
                                }
                            }

                            #endregion


                            #endregion

                            #region ==== I L M U  P E N Y A K I T  G Y N E C O L O G Y

                            model.Assgynclogy = new AssesmentIlmuPenyakitGynecologyViewModel();

                            var AssesmentGynecology = s.IlmuPenyakitGynecology.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                            if (AssesmentGynecology != null)
                            {
                                model.Assgynclogy = IConverter.Cast<AssesmentIlmuPenyakitGynecologyViewModel>(AssesmentGynecology);

                                var dpjp = eSIM.mDokter.FirstOrDefault(x => x.DokterID == AssesmentGynecology.DokterBPJP);
                                if (dpjp != null)
                                {
                                    model.Assgynclogy.DokterBPJPNama = dpjp.NamaDOkter;
                                }

                                var fingergynecodpjp = s.IlmuPenyakitGynecology.Where(x => x.DokterBPJP == model.Assgynclogy.DokterBPJP && x.TandaTanganDPJP == null && x.NRM == model.NRM).FirstOrDefault();
                                if (fingergynecodpjp != null)
                                {
                                    model.Assgynclogy.SudahRegDokter = 1;
                                }
                                else
                                {
                                    model.Assgynclogy.SudahRegDokter = 0;
                                }
                                //if (model.Assgynclogy.DokterBPJP != null)
                                //{
                                //    ViewBag.UrlFingerAssgynclogyDPJP = GetEncodeUrlAssgynclogy_DPJP(model.Assgynclogy.DokterBPJP, model.NoReg, sectionid);
                                //}
                                model.Assgynclogy.Report = 1;
                                model.Assgynclogy.Nomor = nomor;
                            }
                            else
                            {
                                AssesmentGynecology = new IlmuPenyakitGynecology();
                                model.Assgynclogy = IConverter.Cast<AssesmentIlmuPenyakitGynecologyViewModel>(AssesmentGynecology);
                                //ViewBag.UrlFingerAssgynclogyDPJP = GetEncodeUrlAssgynclogy_DPJP(model.Assgynclogy.DokterBPJP, model.NoReg, sectionid);
                                model.Assgynclogy.Tanggal = DateTime.Today;
                                model.Assgynclogy.Jam = DateTime.Now;
                                model.Assgynclogy.KontrolPoliklinik_Tanggal = DateTime.Today;
                                model.Assgynclogy.Hispatologi = DateTime.Today;
                                model.Assgynclogy.RiwayatPenyakitDahulu_Pernah_Tgl = DateTime.Today;
                                model.Assgynclogy.RiwayatMenstruasi_HCG_Ket = DateTime.Today;
                                model.Assgynclogy.BolehPulangJam = DateTime.Now;
                                model.Assgynclogy.NoReg = noreg;
                                model.Assgynclogy.NRM = model.NRM;
                                model.Assgynclogy.SectionID = sectionid;
                                model.Assgynclogy.Report = 0;
                                model.Assgynclogy.Nomor = nomor;
                                model.Assgynclogy.NamaKeluarga = model.PenanggungNama;
                                model.Assgynclogy.TeleponKeluarga = model.PenanggungPhone;
                                model.Assgynclogy.AlamatKeluarga = model.Alamat;
                            }
                            #endregion

                            #region ==== I L M U  P E N Y A K I T  O B S E T R I

                            model.Assobstri = new AssesmentIlmuPenyakitObsetriViewModel();

                            var AssesmentObsetrii = s.IlmuPenyakitObsetri.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                            if (AssesmentObsetrii != null)
                            {
                                model.Assobstri = IConverter.Cast<AssesmentIlmuPenyakitObsetriViewModel>(AssesmentObsetrii);

                                var dpjp = eSIM.mDokter.FirstOrDefault(x => x.DokterID == AssesmentObsetrii.DokterBPJP);
                                if (dpjp != null)
                                {
                                    model.Assobstri.DokterBPJPNama = dpjp.NamaDOkter;
                                }

                                var fingerobsetridpjp = s.IlmuPenyakitObsetri.Where(x => x.DokterBPJP == model.Assobstri.DokterBPJP && x.TandaTanganDPJP == null && x.NRM == model.NRM).FirstOrDefault();
                                if (fingerobsetridpjp != null)
                                {
                                    model.Assobstri.SudahRegDokter = 1;
                                }
                                else
                                {
                                    model.Assobstri.SudahRegDokter = 0;
                                }
                                //if (model.Assobstri.DokterBPJP != null)
                                //{
                                //    ViewBag.UrlFingerAssobstriDPJP = GetEncodeUrlAssobstri_DPJP(model.Assobstri.DokterBPJP, model.NoReg, sectionid);
                                //}
                                model.Assobstri.Report = 1;
                                model.Assobstri.Nomor = nomor;
                            }
                            else
                            {
                                AssesmentObsetrii = new IlmuPenyakitObsetri();
                                model.Assobstri = IConverter.Cast<AssesmentIlmuPenyakitObsetriViewModel>(AssesmentObsetrii);
                                //ViewBag.UrlFingerAssobstriDPJP = GetEncodeUrlAssobstri_DPJP(model.Assobstri.DokterBPJP, model.NoReg, sectionid);
                                model.Assobstri.Tanggal = DateTime.Today;
                                model.Assobstri.Jam = DateTime.Now;
                                model.Assobstri.KontrolPoliklinik_Tanggal = DateTime.Today;
                                model.Assobstri.BolehPulangTgl = DateTime.Today;
                                model.Assobstri.BolehPulangJam = DateTime.Now;
                                model.Assobstri.NoReg = noreg;
                                model.Assobstri.NRM = model.NRM;
                                model.Assobstri.SectionID = sectionid;
                                model.Assobstri.Report = 0;
                                model.Assobstri.Nomor = nomor;
                                model.Assobstri.KeluhanUtama = model.KeluhanUtama;
                            }
                            #endregion

                            #region ==== A S S E S M E N  U L A N G  N Y E R I  

                            model.AssUlngNyri = new AssesmenUlangNyeriViewModel();

                            var AsesmenUlngnyeri = s.AsesmenUlangNyeri.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                            if (AsesmenUlngnyeri != null)
                            {
                                model.AssUlngNyri = IConverter.Cast<AssesmenUlangNyeriViewModel>(AsesmenUlngnyeri);

                                var dpjp = eSIM.mDokter.FirstOrDefault(x => x.DokterID == AsesmenUlngnyeri.Bidan);
                                if (dpjp != null)
                                {
                                    model.AssUlngNyri.BidanNama = dpjp.NamaDOkter;
                                }

                                model.AssUlngNyri.Report = 1;
                                model.AssUlngNyri.Nomor = nomor;
                            }
                            else
                            {
                                AsesmenUlngnyeri = new AsesmenUlangNyeri();
                                model.AssUlngNyri = IConverter.Cast<AssesmenUlangNyeriViewModel>(AsesmenUlngnyeri);
                                model.AssUlngNyri.Tanggal = DateTime.Today;
                                model.AssUlngNyri.Jam = DateTime.Now;
                                model.AssUlngNyri.NoReg = noreg;
                                model.AssUlngNyri.NRM = model.NRM;
                                model.AssUlngNyri.SectionID = sectionid;
                                model.AssUlngNyri.Report = 0;
                                model.AssUlngNyri.Nomor = nomor;
                            }

                            #region ==== D e t a i l

                            model.AssUlngNyri.DetailAssmnUlang_List = new ListDetail<AssesmenUlangNyeriModelDetail>();
                            var d_DetailAss = s.AsesmenUlangNyeri.Where(x => x.NoReg == noreg && x.SectionID == sectionid);
                            if (d_DetailAss != null)
                            {
                                foreach (var x in d_DetailAss)
                                {
                                    var y = IConverter.Cast<AssesmenUlangNyeriModelDetail>(x);

                                    var namaobatass = eSIM.mBarang.FirstOrDefault(z => z.Kode_Barang == y.Obat);
                                    y.ObatNama = namaobatass == null ? "" : namaobatass.Nama_Barang;

                                    var parafass = eSIM.mDokter.FirstOrDefault(z => z.DokterID == y.Bidan);
                                    y.BidanNama = parafass == null ? "" : parafass.NamaDOkter;

                                    model.AssUlngNyri.DetailAssmnUlang_List.Add(false, y);
                                }
                            }

                            #endregion

                            #endregion
                        }

                    }
                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }
        #endregion

        #region ==== P O S T  E M R

        #region === A S S E M E N T  A W A L  M E D I S
        [HttpPost]
        [ActionName("AssesmentAwalMedis")]
        public ActionResult AssesmentAwalMedis_Post()
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);
                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new EMR_Entities())
                    {
                        var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                        var model = s.AsesmenAwalMedis.FirstOrDefault(x => x.SectionID == sectionid && x.NoReg == item.AssAwalMedisNew.NoReg);

                        if (model == null)
                        {
                            var o = IConverter.Cast<AsesmenAwalMedis>(item.AssAwalMedisNew);
                            //o.Username = User.Identity.GetUserName();
                            s.AsesmenAwalMedis.Add(o);

                        }
                        else
                        {
                            model = IConverter.Cast<AsesmenAwalMedis>(item.AssAwalMedisNew);
                            //model.Username = User.Identity.GetUserName();
                            s.AsesmenAwalMedis.AddOrUpdate(model);
                        }

                        if (item.AssAwalMedisNew.save_template == true && item.AssAwalMedisNew.nama_template != null)
                        {
                            var temp = new trTemplate();
                            temp.NoReg = item.AssAwalMedisNew.NoReg;
                            temp.NamaTemplate = item.AssAwalMedisNew.nama_template;
                            temp.DokterID = "";
                            temp.DokumenID = "AssementAwalMedis";
                            temp.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                            s.trTemplate.Add(temp);
                        }

                        #region Rencana Kerja
                        if (item.AssAwalMedisNew.RencanaKerja_List == null) item.AssAwalMedisNew.RencanaKerja_List = new ListDetail<RencanaKerjaMedisDetailModel>();
                        item.AssAwalMedisNew.RencanaKerja_List.RemoveAll(x => x.Remove);
                        foreach (var x in item.AssAwalMedisNew.RencanaKerja_List)
                        {
                            x.Model.NoReg = item.AssAwalMedisNew.NoReg;
                            x.Model.SectionID = item.AssAwalMedisNew.SectionID;
                            x.Model.NRM = item.AssAwalMedisNew.NRM;
                            x.Model.Username = User.Identity.GetUserName();
                        }
                        var new_listRencana = item.AssAwalMedisNew.RencanaKerja_List;
                        var real_listRencana = s.AsesmenAwalMedis_Detail.Where(x => x.NoReg == item.AssAwalMedisNew.NoReg && x.SectionID == item.AssAwalMedisNew.SectionID).ToList();
                        foreach (var x in real_listRencana)
                        {
                            var m = new_listRencana.FirstOrDefault(y => y.Model.No == x.No);
                            if (m == null) s.AsesmenAwalMedis_Detail.Remove(x);
                        }

                        foreach (var x in new_listRencana)
                        {
                            var _m = real_listRencana.FirstOrDefault(y => y.No == x.Model.No);
                            if (_m == null)
                            {
                                s.AsesmenAwalMedis_Detail.Add(IConverter.Cast<AsesmenAwalMedis_Detail>(x.Model));
                            }
                            else
                            {
                                _m.NoReg = x.Model.NoReg;
                                _m.SectionID = x.Model.SectionID;
                                _m.No = x.Model.No;
                                _m.NRM = x.Model.NRM;
                                _m.DaftarMasalah = x.Model.DaftarMasalah;
                                _m.RencanaIntervensi = x.Model.RencanaIntervensi;
                                _m.Target = x.Model.Target;
                            }
                        }
                        #endregion

                        //s.SaveChanges();

                        result = new ResultSS(s.SaveChanges());
                        return RedirectToAction("Index", "MedicalRecordNew", new { noreg = item.AssAwalMedisNew.NoReg, nomor = item.AssAwalMedisNew.Nomor });

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                TempData["message"] = "Data Gagal Disimpan";
                return RedirectToAction("Index", "MedicalRecordNew");
            }
            catch (SqlException ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            catch (DbEntityValidationException ex)
            {
                TempData["Status"] = "danger";
                TempData["Message"] = string.Join("|",
                    ex.EntityValidationErrors.First().ValidationErrors.Select(x => x.ErrorMessage));
            }
            catch (Exception ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            return RedirectToAction("Index", "MedicalRecordNew");
        }

        #endregion

        #region === A S S E M E N T  A W A L  K E P E R A W A T A N
        [HttpPost]
        [ActionName("AssesmentAwalKeperawatan")]
        public ActionResult AssesmentAwalKeperawatan_Post()
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);
                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new EMR_Entities())
                    {
                        var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                        var model = s.AsesmenAwalKeperawatan.FirstOrDefault(x => x.SectionID == sectionid && x.NoReg == item.AssAwalPerawatNew.NoReg);

                        if (model == null)
                        {
                            var o = IConverter.Cast<AsesmenAwalKeperawatan>(item.AssAwalPerawatNew);
                            //o.Username = User.Identity.GetUserName();
                            s.AsesmenAwalKeperawatan.Add(o);

                        }
                        else
                        {
                            model = IConverter.Cast<AsesmenAwalKeperawatan>(item.AssAwalPerawatNew);
                            //model.Username = User.Identity.GetUserName();
                            s.AsesmenAwalKeperawatan.AddOrUpdate(model);
                        }

                        if (item.AssAwalPerawatNew.save_template == true && item.AssAwalPerawatNew.nama_template != null)
                        {
                            var temp = new trTemplate();
                            temp.NoReg = item.AssAwalPerawatNew.NoReg;
                            temp.NamaTemplate = item.AssAwalPerawatNew.nama_template;
                            temp.DokterID = "";
                            temp.DokumenID = "AssementAwalKeperawatan";
                            temp.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                            s.trTemplate.Add(temp);
                        }

                        //s.SaveChanges();

                        result = new ResultSS(s.SaveChanges());
                        return RedirectToAction("Index", "MedicalRecordNew", new { noreg = item.AssAwalPerawatNew.NoReg, nomor = item.AssAwalPerawatNew.Nomor });

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                TempData["message"] = "Data Gagal Disimpan";
                return RedirectToAction("Index", "MedicalRecordNew");
            }
            catch (SqlException ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            catch (DbEntityValidationException ex)
            {
                TempData["Status"] = "danger";
                TempData["Message"] = string.Join("|",
                    ex.EntityValidationErrors.First().ValidationErrors.Select(x => x.ErrorMessage));
            }
            catch (Exception ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            return RedirectToAction("Index", "MedicalRecordNew");
        }

        #endregion

        #region === I G D P E N G K A J I A N  M E D I S

        [HttpPost]
        [ActionName("PengkajianMedis")]
        public ActionResult PengkajianMedis_Post()
        {
            try
            {

                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var model = eEMR.AsesmenPengkajianMedisSkrining.FirstOrDefault(x => x.SectionID == item.PengkajianMedis.SectionID && x.NoReg == item.PengkajianMedis.NoReg);
                            if (model == null)
                            {
                                model = new AsesmenPengkajianMedisSkrining();
                                var o = IConverter.Cast<AsesmenPengkajianMedisSkrining>(item.PengkajianMedis);
                                o.SkriningGizi_Tgl = DateTime.Today;
                                o.Tgl_SkriningTuberkulosis = DateTime.Today;
                                o.Username = User.Identity.GetUserName();
                                eEMR.AsesmenPengkajianMedisSkrining.Add(o);

                            }
                            else
                            {
                                model = IConverter.Cast<AsesmenPengkajianMedisSkrining>(item.PengkajianMedis);
                                model.SkriningGizi_Tgl = DateTime.Today;
                                model.Tgl_SkriningTuberkulosis = DateTime.Today;
                                model.Username = User.Identity.GetUserName();
                                eEMR.AsesmenPengkajianMedisSkrining.AddOrUpdate(model);
                            }

                            #region D E T A I L  R E N C A N A  K E R J A 
                            if (item.PengkajianMedis.Detail_List == null) item.PengkajianMedis.Detail_List = new ListDetail<PengkajianMedis_DetailViewModel>();
                            item.PengkajianMedis.Detail_List.RemoveAll(x => x.Remove);
                            foreach (var x in item.PengkajianMedis.Detail_List)
                            {
                                x.Model.NoReg = item.PengkajianMedis.NoReg;
                                x.Model.SectionID = item.PengkajianMedis.SectionID;
                                x.Model.NRM = item.PengkajianMedis.NRM;
                                x.Model.Tanggal = DateTime.Now;
                            }
                            var new_list = item.PengkajianMedis.Detail_List;
                            var real_list = eEMR.AsesmenPengkajianMedisSkrining_Detail.Where(x => x.NoReg == item.PengkajianMedis.NoReg && x.SectionID == item.PengkajianMedis.SectionID).ToList();
                            foreach (var x in real_list)
                            {
                                var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                                if (m == null) eEMR.AsesmenPengkajianMedisSkrining_Detail.Remove(x);
                            }

                            foreach (var x in new_list)
                            {
                                var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                                if (_m == null)
                                {
                                    var _model = IConverter.Cast<AsesmenPengkajianMedisSkrining_Detail>(x.Model);
                                    eEMR.AsesmenPengkajianMedisSkrining_Detail.Add(_model);
                                }
                                else
                                {
                                    _m.Tanggal = x.Model.Tanggal;
                                    _m.DaftarMasalah = x.Model.DaftarMasalah;
                                    _m.RencanaIntervensi = x.Model.RencanaIntervensi;
                                    _m.Target = x.Model.Target;
                                }
                            }
                            #endregion

                            if (item.PengkajianMedis.save_template == true && item.PengkajianMedis.nama_template != null)
                            {
                                var temp = new trTemplate();
                                temp.NoReg = item.PengkajianMedis.NoReg;
                                temp.NamaTemplate = item.PengkajianMedis.nama_template;
                                temp.DokterID = item.PengkajianMedis.DokterPemeriksa;
                                temp.DokumenID = "Pengkajian Medis IGD";
                                temp.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                                eEMR.trTemplate.Add(temp);
                            }

                            result = new ResultSS(eEMR.SaveChanges());
                            return RedirectToAction("Index", "MedicalRecordNew", new { noreg = item.PengkajianMedis.NoReg, nomor = item.PengkajianMedis.Nomor });

                        }

                    }

                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return RedirectToAction("Index", "MedicalRecordNew");
            }
            catch (SqlException ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            catch (DbEntityValidationException ex)
            {
                TempData["Status"] = "danger";
                TempData["Message"] = string.Join("|",
                    ex.EntityValidationErrors.First().ValidationErrors.Select(x => x.ErrorMessage));
            }
            catch (Exception ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            return RedirectToAction("Index", "MedicalRecordNew");
        }
        #endregion

        #region === A S S E S M E N T  P R A   A N E S T E S I  D A N  S E D A S I
        [HttpPost]
        [ActionName("AssesmentPraAnestesiDanSedasi")]
        public ActionResult AssesmentPraAnestesiDanSedasi_Post()
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);
                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new EMR_Entities())
                    {
                        var model = s.AssesmentPraAnestesiDanSedasi.FirstOrDefault(x => x.SectionID == item.AssPraAnesDanSedasi.SectionID && x.NoReg == item.AssPraAnesDanSedasi.NoReg);

                        if (model == null)
                        {
                            var o = IConverter.Cast<AssesmentPraAnestesiDanSedasi>(item.AssPraAnesDanSedasi);
                            s.AssesmentPraAnestesiDanSedasi.Add(o);
                        }
                        else
                        {
                            model = IConverter.Cast<AssesmentPraAnestesiDanSedasi>(item.AssPraAnesDanSedasi);
                            s.AssesmentPraAnestesiDanSedasi.AddOrUpdate(model);
                        }

                        #region Detail Saran
                        if (item.AssPraAnesDanSedasi.Saran_List == null) item.AssPraAnesDanSedasi.Saran_List = new ListDetail<AssesmentPraAnestesiDanSedasiModelDetail>();
                        item.AssPraAnesDanSedasi.Saran_List.RemoveAll(xxx => xxx.Remove);
                        foreach (var xxx in item.AssPraAnesDanSedasi.Saran_List)
                        {
                            xxx.Model.NoReg = item.AssPraAnesDanSedasi.NoReg;
                            xxx.Model.SectionID = item.AssPraAnesDanSedasi.SectionID;
                            xxx.Model.NRM = item.AssPraAnesDanSedasi.NRM;
                            xxx.Model.Tanggal = DateTime.Now;
                            xxx.Model.Jam = DateTime.Now;
                            xxx.Model.UserName = User.Identity.GetUserName();
                        }
                        var new_listSaran = item.AssPraAnesDanSedasi.Saran_List;
                        var real_listSaran = s.PraAnestesiDanSedasi_Detail.Where(xxx => xxx.NoReg == item.AssPraAnesDanSedasi.NoReg && xxx.SectionID == item.AssPraAnesDanSedasi.SectionID).ToList();
                        foreach (var xxx in real_listSaran)
                        {
                            var m = new_listSaran.FirstOrDefault(y => y.Model.No == xxx.No);
                            if (m == null) s.PraAnestesiDanSedasi_Detail.Remove(xxx);
                        }

                        foreach (var xxx in new_listSaran)
                        {
                            var _m = real_listSaran.FirstOrDefault(y => y.No == xxx.Model.No);
                            if (_m == null)
                            {
                                s.PraAnestesiDanSedasi_Detail.Add(IConverter.Cast<PraAnestesiDanSedasi_Detail>(xxx.Model));
                            }
                            else
                            {
                                _m.Tanggal = DateTime.Now;
                                _m.Jam = DateTime.Now;
                                _m.Saran = xxx.Model.Saran;
                            }
                        }
                        #endregion

                        result = new ResultSS(s.SaveChanges());
                        return RedirectToAction("Index", "MedicalRecord", new { noreg = item.AssPraAnesDanSedasi.NoReg, nomor = item.AssPraAnesDanSedasi.Nomor });

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                TempData["message"] = "Data Gagal Disimpan";
                return RedirectToAction("Index", "MedicalRecord");
            }
            catch (SqlException ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            catch (DbEntityValidationException ex)
            {
                TempData["Status"] = "danger";
                TempData["Message"] = string.Join("|",
                    ex.EntityValidationErrors.First().ValidationErrors.Select(x => x.ErrorMessage));
            }
            catch (Exception ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            return RedirectToAction("Index", "MedicalRecord");
        }

        #endregion

        #region === A S S E S M E N T  P R A   O P E R A S I  P E R E M P U A N
        [HttpPost]
        [ActionName("AssesmentPraOperasiPerempuan")]
        public ActionResult AssesmentPraOperasiPerempuan_Post()
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);
                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new EMR_Entities())
                    {
                        var model = s.AssesmentPraOperasiPerempuan.FirstOrDefault(x => x.SectionID == item.AssPraOprsPrmpn.SectionID && x.NoReg == item.AssPraOprsPrmpn.NoReg);

                        if (model == null)
                        {
                            var o = IConverter.Cast<AssesmentPraOperasiPerempuan>(item.AssPraOprsPrmpn);
                            s.AssesmentPraOperasiPerempuan.Add(o);
                        }
                        else
                        {
                            model = IConverter.Cast<AssesmentPraOperasiPerempuan>(item.AssPraOprsPrmpn);
                            s.AssesmentPraOperasiPerempuan.AddOrUpdate(model);
                        }

                        result = new ResultSS(s.SaveChanges());
                        return RedirectToAction("Index", "MedicalRecord", new { noreg = item.AssPraOprsPrmpn.NoReg, nomor = item.AssPraOprsPrmpn.Nomor });

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                TempData["message"] = "Data Gagal Disimpan";
                return RedirectToAction("Index", "MedicalRecord");
            }
            catch (SqlException ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            catch (DbEntityValidationException ex)
            {
                TempData["Status"] = "danger";
                TempData["Message"] = string.Join("|",
                    ex.EntityValidationErrors.First().ValidationErrors.Select(x => x.ErrorMessage));
            }
            catch (Exception ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            return RedirectToAction("Index", "MedicalRecord");
        }

        #endregion

        #region === A S S E S M E N T  P R A   O P E R A S I  L A K I  L A K I
        [HttpPost]
        [ActionName("AssesmentPraOperasiLakiLaki")]
        public ActionResult AssesmentPraOperasiLakiLaki_Post()
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);
                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new EMR_Entities())
                    {
                        var model = s.AssesmentPraOperasiLakiLaki.FirstOrDefault(x => x.SectionID == item.AssPraOprsLaki.SectionID && x.NoReg == item.AssPraOprsLaki.NoReg);

                        if (model == null)
                        {
                            var o = IConverter.Cast<AssesmentPraOperasiLakiLaki>(item.AssPraOprsLaki);
                            s.AssesmentPraOperasiLakiLaki.Add(o);
                        }
                        else
                        {
                            model = IConverter.Cast<AssesmentPraOperasiLakiLaki>(item.AssPraOprsLaki);
                            s.AssesmentPraOperasiLakiLaki.AddOrUpdate(model);
                        }

                        result = new ResultSS(s.SaveChanges());
                        return RedirectToAction("Index", "MedicalRecord", new { noreg = item.AssPraOprsLaki.NoReg, nomor = item.AssPraOprsLaki.Nomor });

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                TempData["message"] = "Data Gagal Disimpan";
                return RedirectToAction("Index", "MedicalRecord");
            }
            catch (SqlException ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            catch (DbEntityValidationException ex)
            {
                TempData["Status"] = "danger";
                TempData["Message"] = string.Join("|",
                    ex.EntityValidationErrors.First().ValidationErrors.Select(x => x.ErrorMessage));
            }
            catch (Exception ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            return RedirectToAction("Index", "MedicalRecord");
        }

        #endregion

        #region === A S S E S M E N  U L A N G  N Y E R I
        [HttpPost]
        [ActionName("Assesmenulangnyeri")]
        public ActionResult Assesmenulangnyeri_Post()
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);
                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new EMR_Entities())
                    {

                        #region D E T A I L  P E N A T A L A K S A N A  G Y N E C O L O G Y
                        if (item.AssUlngNyri.DetailAssmnUlang_List == null) item.AssUlngNyri.DetailAssmnUlang_List = new ListDetail<AssesmenUlangNyeriModelDetail>();
                        item.AssUlngNyri.DetailAssmnUlang_List.RemoveAll(xxx => xxx.Remove);
                        foreach (var xxx in item.AssUlngNyri.DetailAssmnUlang_List)
                        {
                            xxx.Model.NoReg = item.AssUlngNyri.NoReg;
                            xxx.Model.SectionID = item.AssUlngNyri.SectionID;
                            xxx.Model.NRM = item.AssUlngNyri.NRM;
                            xxx.Model.Tanggal = DateTime.Now;
                            xxx.Model.Jam = DateTime.Now;
                            xxx.Model.Username = User.Identity.GetUserName();
                        }
                        var new_listAssUlangNyeri = item.AssUlngNyri.DetailAssmnUlang_List;
                        var real_listAssUlangNyeri = s.AsesmenUlangNyeri.Where(xxx => xxx.NoReg == item.AssUlngNyri.NoReg && xxx.SectionID == item.AssUlngNyri.SectionID).ToList();
                        foreach (var xxx in real_listAssUlangNyeri)
                        {
                            var m = new_listAssUlangNyeri.FirstOrDefault(y => y.Model.No == xxx.No);
                            if (m == null) s.AsesmenUlangNyeri.Remove(xxx);
                        }

                        foreach (var xxx in new_listAssUlangNyeri)
                        {
                            var _m = real_listAssUlangNyeri.FirstOrDefault(y => y.No == xxx.Model.No);
                            if (_m == null)
                            {
                                s.AsesmenUlangNyeri.Add(IConverter.Cast<AsesmenUlangNyeri>(xxx.Model));
                            }
                            else
                            {
                                _m.Tanggal = DateTime.Now;
                                _m.Jam = DateTime.Now;
                                _m.SkorNyeri = xxx.Model.SkorNyeri;
                                _m.SkorSedasi = xxx.Model.SkorSedasi;
                                _m.Obat = xxx.Model.Obat;
                                _m.Dosis = xxx.Model.Dosis;
                                _m.Rute = xxx.Model.Rute;
                                _m.Farmakologi = xxx.Model.Farmakologi;
                                _m.Bidan = xxx.Model.Bidan;
                                _m.KajianUlang = DateTime.Now;
                                _m.Keterangan = xxx.Model.Keterangan;
                                _m.Username = xxx.Model.Username;
                            }
                        }
                        #endregion

                        result = new ResultSS(s.SaveChanges());
                        return RedirectToAction("Index", "MedicalRecord", new { noreg = item.AssUlngNyri.NoReg, nomor = item.AssUlngNyri.Nomor });

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                TempData["message"] = "Data Gagal Disimpan";
                return RedirectToAction("Index", "MedicalRecord");
            }
            catch (SqlException ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            catch (DbEntityValidationException ex)
            {
                TempData["Status"] = "danger";
                TempData["Message"] = string.Join("|",
                    ex.EntityValidationErrors.First().ValidationErrors.Select(x => x.ErrorMessage));
            }
            catch (Exception ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            return RedirectToAction("Index", "MedicalRecord");
        }

        #endregion

        #region === A S U H A N  K E P E R A W A T A N  O B S E R V A S I  H D 
        [HttpPost]
        [ActionName("AsuhanKeperawatanObservasiHD")]
        public ActionResult AsuhanKeperawatanObservasiHD_Post()
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);
                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new EMR_Entities())
                    {
                        var model = s.AsuhanKeperawatanDanObservasiPasienHemodialisis.FirstOrDefault(x => x.SectionID == item.AshnKprwtnObsrvsiHD.SectionID && x.NoReg == item.AshnKprwtnObsrvsiHD.NoReg);

                        if (model == null)
                        {
                            var o = IConverter.Cast<AsuhanKeperawatanDanObservasiPasienHemodialisis>(item.AshnKprwtnObsrvsiHD);
                            o.Username = User.Identity.GetUserName();
                            s.AsuhanKeperawatanDanObservasiPasienHemodialisis.Add(o);
                        }
                        else
                        {
                            model = IConverter.Cast<AsuhanKeperawatanDanObservasiPasienHemodialisis>(item.AshnKprwtnObsrvsiHD);
                            model.Username = User.Identity.GetUserName();
                            s.AsuhanKeperawatanDanObservasiPasienHemodialisis.AddOrUpdate(model);
                        }

                        #region Detail Pelaksanaan
                        if (item.AshnKprwtnObsrvsiHD.Pelaksanaan_List == null) item.AshnKprwtnObsrvsiHD.Pelaksanaan_List = new ListDetail<AsuhanKeperawatanDanObservasiPasienHemodialisisModelDetail>();
                        item.AshnKprwtnObsrvsiHD.Pelaksanaan_List.RemoveAll(xxx => xxx.Remove);
                        foreach (var xxx in item.AshnKprwtnObsrvsiHD.Pelaksanaan_List)
                        {
                            xxx.Model.NoReg = item.AshnKprwtnObsrvsiHD.NoReg;
                            xxx.Model.SectionID = item.AshnKprwtnObsrvsiHD.SectionID;
                            xxx.Model.NRM = item.AshnKprwtnObsrvsiHD.NRM;
                            xxx.Model.Jam = DateTime.Now;
                            xxx.Model.Username = User.Identity.GetUserName();
                        }
                        var new_listRencana = item.AshnKprwtnObsrvsiHD.Pelaksanaan_List;
                        var real_listRencana = s.AsuhanKeperawatanDanObservasiPasienHemodialisis_Detail.Where(xxx => xxx.NoReg == item.AshnKprwtnObsrvsiHD.NoReg && xxx.SectionID == item.AshnKprwtnObsrvsiHD.SectionID).ToList();
                        foreach (var xxx in real_listRencana)
                        {
                            var m = new_listRencana.FirstOrDefault(y => y.Model.No == xxx.No);
                            if (m == null) s.AsuhanKeperawatanDanObservasiPasienHemodialisis_Detail.Remove(xxx);
                        }

                        foreach (var xxx in new_listRencana)
                        {
                            var _m = real_listRencana.FirstOrDefault(y => y.No == xxx.Model.No);
                            if (_m == null)
                            {
                                s.AsuhanKeperawatanDanObservasiPasienHemodialisis_Detail.Add(IConverter.Cast<AsuhanKeperawatanDanObservasiPasienHemodialisis_Detail>(xxx.Model));
                            }
                            else
                            {
                                _m.Jam = DateTime.Now;
                                _m.TekananDarah = xxx.Model.TekananDarah;
                                _m.Nadi = xxx.Model.Nadi;
                                _m.Blood = xxx.Model.Blood;
                                _m.Vena = xxx.Model.Vena;
                                _m.UltraGoal = xxx.Model.UltraGoal;
                                _m.UltraRate = xxx.Model.UltraRate;
                                _m.UltraRemove = xxx.Model.UltraRemove;
                                _m.Tindakan = xxx.Model.Tindakan;
                            }
                        }
                        #endregion

                        #region Detail Terapi Intra HD
                        if (item.AshnKprwtnObsrvsiHD.Terapi_List == null) item.AshnKprwtnObsrvsiHD.Terapi_List = new ListDetail<AsuhanKeperawatanDanObservasiPasienHemodialisisTerapiModelDetail>();
                        item.AshnKprwtnObsrvsiHD.Terapi_List.RemoveAll(xxx => xxx.Remove);
                        foreach (var xxx in item.AshnKprwtnObsrvsiHD.Terapi_List)
                        {
                            xxx.Model.NoReg = item.AshnKprwtnObsrvsiHD.NoReg;
                            xxx.Model.SectionID = item.AshnKprwtnObsrvsiHD.SectionID;
                            xxx.Model.NRM = item.AshnKprwtnObsrvsiHD.NRM;
                            xxx.Model.Username = User.Identity.GetUserName();
                        }
                        var new_listRencana2 = item.AshnKprwtnObsrvsiHD.Terapi_List;
                        var real_listRencana2 = s.AsuhanKeperawatanDanObservasiPasienHemodialisisTerapiIntra_Detail.Where(xxx => xxx.NoReg == item.AshnKprwtnObsrvsiHD.NoReg && xxx.SectionID == item.AshnKprwtnObsrvsiHD.SectionID).ToList();
                        foreach (var xxx in real_listRencana2)
                        {
                            var m = new_listRencana2.FirstOrDefault(y => y.Model.No == xxx.No);
                            if (m == null) s.AsuhanKeperawatanDanObservasiPasienHemodialisisTerapiIntra_Detail.Remove(xxx);
                        }

                        foreach (var xxx in new_listRencana2)
                        {
                            var _m = real_listRencana2.FirstOrDefault(y => y.No == xxx.Model.No);
                            if (_m == null)
                            {
                                s.AsuhanKeperawatanDanObservasiPasienHemodialisisTerapiIntra_Detail.Add(IConverter.Cast<AsuhanKeperawatanDanObservasiPasienHemodialisisTerapiIntra_Detail>(xxx.Model));
                            }
                            else
                            {
                                _m.Obat = xxx.Model.Obat;
                                _m.Dosis = xxx.Model.Dosis;
                                _m.CaraPemberian = xxx.Model.CaraPemberian;
                                _m.Waktu = xxx.Model.Waktu;
                                _m.Perawat1 = xxx.Model.Perawat1;
                                _m.Perawat2 = xxx.Model.Perawat2;
                            }
                        }
                        #endregion

                        result = new ResultSS(s.SaveChanges());
                        return RedirectToAction("Index", "MedicalRecord", new { noreg = item.AshnKprwtnObsrvsiHD.NoReg, nomor = item.AshnKprwtnObsrvsiHD.Nomor });

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                TempData["message"] = "Data Gagal Disimpan";
                return RedirectToAction("Index", "MedicalRecord");
            }
            catch (SqlException ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            catch (DbEntityValidationException ex)
            {
                TempData["Status"] = "danger";
                TempData["Message"] = string.Join("|",
                    ex.EntityValidationErrors.First().ValidationErrors.Select(x => x.ErrorMessage));
            }
            catch (Exception ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            return RedirectToAction("Index", "MedicalRecord");
        }

        #endregion

        #region === P E N G K A J I A N  A W A L  D I A L I S I S  H D 
        [HttpPost]
        [ActionName("PengkajianAwalPasienDialisis")]
        public ActionResult PengkajianAwalPasienDialisis_Post()
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);
                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new EMR_Entities())
                    {
                        var model = s.PengkajianAwalPasienDialisis.FirstOrDefault(x => x.SectionID == item.PengkajianDialisisHD.SectionID && x.NoReg == item.PengkajianDialisisHD.NoReg);

                        if (model == null)
                        {
                            var o = IConverter.Cast<PengkajianAwalPasienDialisis>(item.PengkajianDialisisHD);
                            o.Username = User.Identity.GetUserName();
                            s.PengkajianAwalPasienDialisis.Add(o);
                        }
                        else
                        {
                            model = IConverter.Cast<PengkajianAwalPasienDialisis>(item.PengkajianDialisisHD);
                            model.Username = User.Identity.GetUserName();
                            s.PengkajianAwalPasienDialisis.AddOrUpdate(model);
                        }

                        result = new ResultSS(s.SaveChanges());
                        return RedirectToAction("Index", "MedicalRecord", new { noreg = item.PengkajianDialisisHD.NoReg, nomor = item.PengkajianDialisisHD.Nomor });

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                TempData["message"] = "Data Gagal Disimpan";
                return RedirectToAction("Index", "MedicalRecord");
            }
            catch (SqlException ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            catch (DbEntityValidationException ex)
            {
                TempData["Status"] = "danger";
                TempData["Message"] = string.Join("|",
                    ex.EntityValidationErrors.First().ValidationErrors.Select(x => x.ErrorMessage));
            }
            catch (Exception ex) { TempData["Status"] = "danger"; TempData["Message"] = ex.Message; }
            return RedirectToAction("Index", "MedicalRecord");
        }

        #endregion
        #endregion

        #region ==== R E P O R T  P D F  E M R 

        #region === ASSESMENT PRAANESTESI DAN SEDASI
        public ActionResult ExportPDFAssPraAnesDanSedasi(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_AssesmentPraAnestesiDanSedasi.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_AssesmentPraAnestesiDanSedasi", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_AssesmentPraAnestesiDanSedasi;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"Rpt_PraAnestesiDanSedasi_Detail", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_PraAnestesiDanSedasi_Detail;1"].SetDataSource(ds[i].Tables[0]);
                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === ASSESMENT PRAOPERASI PEREMPUAN
        public ActionResult ExportPDFAssPraOprsPerempuan(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_AssesmentPraOperasiPerempuan.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_AssesmentPraOperasiPerempuan", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_AssesmentPraOperasiPerempuan;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === ASSESMENT PRAOPERASI LAKILAKI
        public ActionResult ExportPDFAssPraOprsLaki(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_AssesmentPraOperasiLakiLaki.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_AssesmentPraOperasiLakiLaki", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_AssesmentPraOperasiLakiLaki;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #endregion

        #endregion

        #region ======= H A S I L  B A C A  L A B  R A D I O L O G I  O B A T

        #region ===== T A B L E

        [HttpPost]
        public string ListHasilBacaRadiologi(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var nrm = filter[9];
                    var header = s.EMR_VIEW_RADIOLOGI(nrm).FirstOrDefault();
                    var NoBukti = "";
                    if (header != null)
                    {
                        NoBukti = header.NoBukti;
                    }
                    var proses = s.EMR_VIEW_RADIOLOGI_DETAIL(NoBukti).Where(x => x.NoBukti == NoBukti).ToList();
                    if (filter[12] != "True")
                    {
                        proses.Where("Tanggal >= @0", DateTime.Parse(filter[10]).AddDays(-1));
                        proses.Where("Tanggal <= @0", DateTime.Parse(filter[11]));
                    }
                    if (filter[13] != null && filter[13] != "")
                    {
                        proses.Where($"NamaDOkter.Contains(@0)", filter[13]);
                    }
                    //proses = proses.Where($"{nameof(EMR_VIEW_RADIOLOGI.RegNo)}=@0", filter[11]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<HasilBacaRadiologiViewModel>(x));
                    m.ForEach(x => x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy"));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListHasilBacaLaboratorium(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var nrm = filter[9];
                    IQueryable<EMR_VIEW_LABORATORIUM> proses = s.EMR_VIEW_LABORATORIUM.Where(x => x.NRM == nrm);
                    if (filter[12] != "True")
                    {
                        proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[10]).AddDays(-1));
                        proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[11]));
                    }
                    if (filter[13] != null && filter[13] != "")
                    {
                        proses = proses.Where($"DokterPengirim.Contains(@0)", filter[13]);
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<HasilBacaLaboratoriumViewModel>(x));
                    m.ForEach(x => x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy"));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListViewObat(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var nrm = filter[14];
                    IQueryable<EMR_VIEW_OBAT> proses = s.EMR_VIEW_OBAT.Where(x => x.NRM == nrm);
                    if (filter[12] != "True")
                    {
                        proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[10]).AddDays(-1));
                        proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[11]));
                    }
                    if (filter[13] != null && filter[13] != "")
                    {
                        proses = proses.Where($"NamaDokter.Contains(@0)", filter[13]);
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<ViewObatViewModel>(x));
                    m.ForEach(x => x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy"));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E T A I L

        [HttpGet]
        public ActionResult DetailHasilBacaHadiologi(string id, string nrm, int nomor = 0)
        {
            HasilBacaRadiologiViewModel result;
            try
            {
                using (var service = new SIM_Entities())
                {
                    var m = service.EMR_VIEW_RADIOLOGI(nrm).Where(x => x.Nomor == nomor).FirstOrDefault();
                    var d = service.EMR_VIEW_RADIOLOGI_DETAIL(m.NoBukti).Where(x => x.NoBukti == id && x.Nomor == nomor).ToList();
                    result = IConverter.Cast<HasilBacaRadiologiViewModel>(m);
                    result.Detail_List = new List<HasilBacaRadiologiDetailViewModel>();
                    foreach (var x in d)
                    {
                        var t = IConverter.Cast<HasilBacaRadiologiDetailViewModel>(x);
                        t.Tanggal_View = t.Tanggal.ToString("dd/MM/yyyy");
                        t.TglInput_View = t.TglInput != null ? t.TglInput.Value.ToString("dd/MM/yyyy") : "";
                        result.Detail_List.Add(t);
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
            return PartialView(result);
        }

        [HttpGet]
        public ActionResult DetailLaboratorium(string id, string noreg)
        {
            HasilBacaLaboratoriumViewModel result;
            try
            {
                using (var service = new SIM_Entities())
                {
                    var m = service.EMR_VIEW_LABORATORIUM.Where(x => x.NoSystem == id && x.RegNo == noreg).FirstOrDefault();
                    var d = service.EMR_VIEW_LABORATORIUM_DETAIL(id).Where(x => x.NoSystem == id && x.RegNo == noreg).OrderBy(x => x.KategoriTestNama).ToList();
                    result = IConverter.Cast<HasilBacaLaboratoriumViewModel>(m);
                    result.Detail_List = new List<HasilBacaLaboratoriumDetailViewModel>();
                    foreach (var x in d)
                    {
                        var t = IConverter.Cast<HasilBacaLaboratoriumDetailViewModel>(x);
                        t.Tanggal_View = t.Tanggal.ToString("dd/MM/yyyy");
                        result.Detail_List.Add(t);
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
            return PartialView(result);
        }

        [HttpGet]
        public ActionResult DetailObat(string id, string noreg)
        {
            ViewObatViewModel result;
            try
            {
                using (var service = new SIM_Entities())
                {
                    var m = service.EMR_VIEW_OBAT.Where(x => x.NoBukti == id && x.NoReg == noreg).FirstOrDefault();
                    var d = service.EMR_VIEW_OBAT_DETAIL.Where(x => x.NoBukti == id && x.NoReg == noreg).ToList();
                    result = IConverter.Cast<ViewObatViewModel>(m);
                    result.Detail_List = new List<ViewObatDetailViewModel>();
                    foreach (var x in d)
                    {
                        var t = IConverter.Cast<ViewObatDetailViewModel>(x);
                        t.Tanggal_View = t.Tanggal.ToString("dd/MM/yyyy");
                        result.Detail_List.Add(t);
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
            return PartialView(result);
        }

        #endregion

        #endregion

        #region ===== C P P T

        [HttpPost]
        public string TableSOAP(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var nrm = filter[1];
                    IQueryable<SIMtrSOAP> proses = s.SIMtrSOAP.Where(x => x.NRM == nrm);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<SOAPViewModel>(x));

                    using (var sSIM = new SIM_Entities())
                    {
                        foreach (var x in m)
                        {
                            var KaRU = sSIM.mDokter.FirstOrDefault(y => y.DokterID == x.KaRU_Petugas);
                            var spesialis = sSIM.mDokter.FirstOrDefault(y => y.DokterID == x.DPJP);
                            var dokter = sSIM.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                            var menyerahkan = sSIM.mDokter.FirstOrDefault(y => y.DokterID == x.MenyerahkanPasien);
                            var menerima = sSIM.mDokter.FirstOrDefault(y => y.DokterID == x.MenerimaPasien);
                            var bacaulang = sSIM.mDokter.FirstOrDefault(y => y.DokterID == x.PetugasBacaUlang);
                            var konfirmasi = sSIM.mDokter.FirstOrDefault(y => y.DokterID == x.PetugasKonfirmasi);
                            var section = sSIM.SIMmSection.FirstOrDefault(y => y.SectionID == x.SectionID);
                            
                            if (spesialis != null)
                            {
                                x.DPJPNama = spesialis.NamaDOkter;
                            }
                            x.NamaDOkter = dokter == null ? "" : dokter.NamaDOkter;
                            x.MenyerahkanPasienNama = menyerahkan == null ? "" : menyerahkan.NamaDOkter;
                            x.MenerimaPasienNama = menerima == null ? "" : menerima.NamaDOkter;
                            x.PetugasBacaUlangNama = bacaulang == null ? "" : bacaulang.NamaDOkter;
                            x.PetugasKonfirmasiNama = konfirmasi == null ? "" : konfirmasi.NamaDOkter;
                            x.SectionName = Request.Cookies["SectionNamaPelayanan"].Value;
                            x.TanggalInput_View = x.TanggalInput.ToString("HH:mm");
                            x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                            x.TanggalBacaUlang_View = (x.TanggalBacaUlang == null ? "" : x.TanggalBacaUlang.Value.ToString("dd/MM/yyyy"));
                            x.TanggalKonfirmasi_View = (x.TanggalKonfirmasi == null ? "" : x.TanggalKonfirmasi.Value.ToString("dd/MM/yyyy")); ;

                            if (x.T_Tanggal != null)
                            {
                                x.T_TanggalView = x.T_Tanggal.Value.ToString("dd/MM/yyyy");
                            }
                            if (x.B_Tanggal != null)
                            {
                                x.B_TanggalView = x.B_Tanggal.Value.ToString("dd/MM/yyyy");
                            }

                            if (x.K_Tanggal != null)
                            {
                                x.K_TanggalView = x.K_Tanggal.Value.ToString("dd/MM/yyyy");
                            }
                        }

                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public string GetEncodeUrlCPPT_DPJP(string id, string noreg, string section, int no)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCPPT/get_keycppt";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section + "&no=" + no);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlCPPT_Menerima(string id, string noreg, string section, int no)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCPPT/get_keycpptmenerimapasien";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section + "&no=" + no);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlCPPT_Menyerahkan(string id, string noreg, string section, int no)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCPPT/get_keycpptmenyerahkanpasien";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section + "&no=" + no);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlCPPT_PetugasBacaUlang(string id, string noreg, string section, int no)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCPPT/get_keycpptpetugasbacaulang";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section + "&no=" + no);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlCPPT_PetugasKonfirmasi(string id, string noreg, string section, int no)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCPPT/get_keycpptpetugaskonfirmasi";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section + "&no=" + no);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        #region ==== Report

        public ActionResult ExportPDFCPPT(string nrm, string namapasien)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_SOAP.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand($"Rpt_SOAP", conn));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    cmd[i].Parameters.Add(new SqlParameter("@NRM", nrm));
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[0].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"Rpt_SOAP_Detail", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NRM", nrm));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_SOAP_Detail;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"Rpt_SOAP_Detail2", conn));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    cmd[i].Parameters.Add(new SqlParameter("@NRM", nrm));
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables["Rpt_SOAP_Detail2;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

        }

        public ActionResult ExportPDFObat(string nobukti)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Informasi_Obat.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand($"Informasi_Obat", conn));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    cmd[i].Parameters.Add(new SqlParameter("@NoBukti", nobukti));
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[0].SetDataSource(ds[i].Tables[0]);
                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

        }

        #endregion

        #endregion

        #region ===== T A B L E  H I S T O R Y

        [HttpGet]
        public ActionResult ListHistory(string noreg, string nrm)
        {
            HistoryPasienViewModel result;
            try
            {
                using (var history = new EMR_Entities())
                {
                    var m = history.History.FirstOrDefault();
                    //var d = history.EMR_VIEW_RADIOLOGI_DETAIL(m.NoBukti).Where(x => x.NoBukti == id && x.RegNo == noreg).ToList();
                    result = IConverter.Cast<HistoryPasienViewModel>(m);
                    //result.Detail_List = new List<HasilBacaRadiologiDetailViewModel>();
                    //foreach (var x in d)
                    //{
                    //    var t = IConverter.Cast<HasilBacaRadiologiDetailViewModel>(x);
                    //    t.Tanggal_View = t.Tanggal.ToString("dd/MM/yyyy");
                    //    t.TglInput_View = t.TglInput != null ? t.TglInput.Value.ToString("dd/MM/yyyy") : "";
                    //    result.Detail_List.Add(t);
                    //}
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
            return PartialView(result);
        }
        #endregion

        #region ===== K U N J U N G A N  P O L I K L I N I K 
        [HttpPost]
        public string ListKunjunganPoliklinik(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMR_Entities())
                    {
                        var nrm = filter[0];
                        var noreg = filter[1];
                        var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                        IQueryable<KunjunganPoliklinik> p = s.KunjunganPoliklinik.Where(x => x.NRM == nrm);

                        var totalcount = p.Count();
                        var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}").ToArray();
                        result = new ResultSS(models.Length, null, totalcount, pageIndex);
                        var datas = new List<KunjunganPoliklinikViewModel>();
                        var userId = User.Identity.GetUserId();
                        foreach (var x in models.ToList())
                        {
                            var m = IConverter.Cast<KunjunganPoliklinikViewModel>(x);
                            m.Jam_View = x.Jam.Value.ToString("HH:mm");
                            m.Tanggal_View = x.Tanggal.Value.ToString("dd-MM-yyyy");
                            m.Edit = "0";
                            if (x.NoReg == noreg && x.SectionID == sectionid)
                            {
                                m.Edit = "1";
                            }

                            var dokter = sim.mDokter.FirstOrDefault(xx => xx.DokterID == m.Paraf);
                            if (dokter != null)
                            {
                                m.Paraf = dokter.DokterID;
                                m.ParafNama = dokter.NamaDOkter;
                            }

                            datas.Add(m);
                        }
                        result.Data = datas;

                    }
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string Batal(int id, string noreg)
        {
            try
            {
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var m = s.KunjunganPoliklinik.FirstOrDefault(x => x.No == id && x.NoReg == noreg);
                    s.KunjunganPoliklinik.Remove(m);
                    result = new ResultSS(s.SaveChanges());
                   
                }
                return JsonHelper.JsonMsgDelete(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== R E N P R A  R J
        [HttpPost]
        public string ListRenpra(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMR_Entities())
                    {
                        var nrm = filter[0];
                        var noreg = filter[1];
                        var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                        IQueryable<Renpra_RJ> p = s.Renpra_RJ.Where(x => x.NRM == nrm);

                        var totalcount = p.Count();
                        var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}").ToArray();
                        result = new ResultSS(models.Length, null, totalcount, pageIndex);
                        var datas = new List<Renpra_RJViewModel>();
                        var userId = User.Identity.GetUserId();
                        foreach (var x in models.ToList())
                        {
                            var m = IConverter.Cast<Renpra_RJViewModel>(x);
                            m.Jam_View = x.Jam.Value.ToString("HH:mm");
                            m.Tanggal_View = x.Tanggal.Value.ToString("dd-MM-yyyy");
                            m.Edit = "0";
                            if (x.NoReg == noreg && x.SectionID == sectionid)
                            {
                                m.Edit = "1";
                            }

                            var dokter = sim.mDokter.FirstOrDefault(xx => xx.DokterID == m.Petugas);
                            if (dokter != null)
                            {
                                m.Petugas = dokter.DokterID;
                                m.PetugasNama = dokter.NamaDOkter;
                            }

                            datas.Add(m);
                        }
                        result.Data = datas;

                    }
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string BatalRenpra(int id, string noreg)
        {
            try
            {
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var m = s.Renpra_RJ.FirstOrDefault(x => x.No == id && x.NoReg == noreg);
                    s.Renpra_RJ.Remove(m);
                    result = new ResultSS(s.SaveChanges());

                }
                return JsonHelper.JsonMsgDelete(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== A S S E S M E N T  L A N J U T  M E D I S  K E P E R A W A T A N
        [HttpPost]
        public string ListAssLanjutMedisKeperawatan(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMR_Entities())
                    {
                        var nrm = filter[0];
                        var noreg = filter[1];
                        var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                        IQueryable<AsesmenLanjutanKeperawatan> p = s.AsesmenLanjutanKeperawatan.Where(x => x.NRM == nrm);

                        var totalcount = p.Count();
                        var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}").ToArray();
                        result = new ResultSS(models.Length, null, totalcount, pageIndex);
                        var datas = new List<AsesmenLanjutanKeperawatanViewModel>();
                        var userId = User.Identity.GetUserId();
                        foreach (var x in models.ToList())
                        {
                            var m = IConverter.Cast<AsesmenLanjutanKeperawatanViewModel>(x);
                            m.Jam_View = x.Jam.Value.ToString("HH:mm");
                            m.Tanggal_View = x.Tanggal.Value.ToString("dd-MM-yyyy");
                            m.Edit = "0";
                            if (x.NoReg == noreg && x.SectionID == sectionid)
                            {
                                m.Edit = "1";
                            }

                            var dokter = sim.mDokter.FirstOrDefault(xx => xx.DokterID == m.Petugas);
                            if (dokter != null)
                            {
                                m.Petugas = dokter.DokterID;
                                m.PetugasNama = dokter.NamaDOkter;
                            }

                            datas.Add(m);
                        }
                        result.Data = datas;

                    }
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string BatalAssLanjutMedisKeperawatan(int id, string noreg)
        {
            try
            {
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var m = s.AsesmenLanjutanKeperawatan.FirstOrDefault(x => x.No == id && x.NoReg == noreg);
                    s.AsesmenLanjutanKeperawatan.Remove(m);
                    result = new ResultSS(s.SaveChanges());

                }
                return JsonHelper.JsonMsgDelete(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== A S S E S M E N T  L A N J U T  M E D I S  K E B I D A N A N
        [HttpPost]
        public string ListAssLanjutKebidanan(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMR_Entities())
                    {
                        var nrm = filter[0];
                        var noreg = filter[1];
                        var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                        IQueryable<AsesmenLanjutanKebidanan> p = s.AsesmenLanjutanKebidanan.Where(x => x.NRM == nrm);

                        var totalcount = p.Count();
                        var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}").ToArray();
                        result = new ResultSS(models.Length, null, totalcount, pageIndex);
                        var datas = new List<AsesmenLanjutanKebidananViewModel>();
                        var userId = User.Identity.GetUserId();
                        foreach (var x in models.ToList())
                        {
                            var m = IConverter.Cast<AsesmenLanjutanKebidananViewModel>(x);
                            m.Jam_View = x.Jam.Value.ToString("HH:mm");
                            m.Tanggal_View = x.Tanggal.Value.ToString("dd-MM-yyyy");
                            m.Edit = "0";
                            if (x.NoReg == noreg && x.SectionID == sectionid)
                            {
                                m.Edit = "1";
                            }

                            var dokter = sim.mDokter.FirstOrDefault(xx => xx.DokterID == m.Petugas);
                            if (dokter != null)
                            {
                                m.Petugas = dokter.DokterID;
                                m.PetugasNama = dokter.NamaDOkter;
                            }

                            datas.Add(m);
                        }
                        result.Data = datas;

                    }
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string BatalAssLanjutKebidanan(int id, string noreg)
        {
            try
            {
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var m = s.AsesmenLanjutanKebidanan.FirstOrDefault(x => x.No == id && x.NoReg == noreg);
                    s.AsesmenLanjutanKebidanan.Remove(m);
                    result = new ResultSS(s.SaveChanges());

                }
                return JsonHelper.JsonMsgDelete(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== T A B L E  H I S T O R Y  M O D A L

        #region ===== T A B L E

        [HttpPost]
        public string ListHasilBacaRadiologiModal(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var nrm = filter[9];
                    var header = s.EMR_VIEW_RADIOLOGI(nrm).FirstOrDefault();
                    var NoBukti = "";
                    if (header != null)
                    {
                        NoBukti = header.NoBukti;
                    }
                    var proses = s.EMR_VIEW_RADIOLOGI_DETAIL(NoBukti).Where(x => x.NoBukti == NoBukti).ToList();
                    if (filter[12] != "True")
                    {
                        proses.Where("Tanggal >= @0", DateTime.Parse(filter[10]).AddDays(-1));
                        proses.Where("Tanggal <= @0", DateTime.Parse(filter[11]));
                    }
                    if (filter[13] != null && filter[13] != "")
                    {
                        proses.Where($"NamaDOkter.Contains(@0)", filter[13]);
                    }
                    //proses = proses.Where($"{nameof(EMR_VIEW_RADIOLOGI.RegNo)}=@0", filter[11]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<HasilBacaRadiologiViewModel>(x));
                    m.ForEach(x => x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy"));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListHasilBacaLaboratoriumModal(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<EMR_VIEW_LABORATORIUM> proses = s.EMR_VIEW_LABORATORIUM;
                    if (filter[12] != "True")
                    {
                        proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[10]).AddDays(-1));
                        proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[11]));
                    }
                    if (filter[13] != null && filter[13] != "")
                    {
                        proses = proses.Where($"DokterPengirim.Contains(@0)", filter[13]);
                    }
                    proses = proses.Where($"{nameof(EMR_VIEW_LABORATORIUM.NRM)}=@0", filter[9]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<HasilBacaLaboratoriumViewModel>(x));
                    m.ForEach(x => x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy"));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListViewObatModal(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<EMR_VIEW_OBAT> proses = s.EMR_VIEW_OBAT;
                    if (filter[12] != "True")
                    {
                        proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[10]).AddDays(-1));
                        proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[11]));
                    }
                    if (filter[13] != null && filter[13] != "")
                    {
                        proses = proses.Where($"NamaDokter.Contains(@0)", filter[13]);
                    }
                    proses = proses.Where($"{nameof(EMR_VIEW_OBAT.NoReg)}=@0", filter[9]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<ViewObatViewModel>(x));
                    m.ForEach(x => x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy"));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #endregion

        public ActionResult ChangePasien(string noreg)
        {
            return Redirect(Url.Action("Index", "MedicalRecordNew") + "?noreg=" + noreg);
        }

        [HttpGet]
        public ActionResult showRiwayat(string noreg)
        {
            var model = new RegistrasiPasienViewModel();
            ViewBag.NoReg = noreg;
            model.NoReg = noreg;
            model.DariTanggal = DateTime.Today.ToString("MM/dd/yyyy");
            model.SampaiTanggal = DateTime.Today.ToString("MM/dd/yyyy");
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        #region === ASSESMENT MEDIS AWAL
        public ActionResult ExportPDFAssAwalMedis(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_AsesmenAwalMedis.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_AsesmenAwalMedis", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_AsesmenAwalMedis;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"Rpt_AsesmenAwalMedis_Detail", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_AsesmenAwalMedis_Detail;1"].SetDataSource(ds[i].Tables[0]);
                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public ActionResult ExportPDFAssAwalKeperawatan(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_AsesmenAwalKeperawatan.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_AsesmenAwalKeperawatan", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_AsesmenAwalKeperawatan;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion
    }
}