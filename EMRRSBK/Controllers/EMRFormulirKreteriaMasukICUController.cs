﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class EMRFormulirKreteriaMasukICUController : Controller
    {
        // GET: EMRFormulirKreteriaMasukICU
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string noreg, string nrm, int view = 0)
        {
            var model = new EMRFormulirKreteriaMasukICUViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var dokumen = s.FormulirKreteriaMasukICU.FirstOrDefault(x => x.NoReg == noreg);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRFormulirKreteriaMasukICUViewModel>(dokumen);
                        var dokter = sim.mDokter.FirstOrDefault(x => x.DokterID == model.DPJP);
                        if (dokter != null)
                        {
                            model.DPJPNama = dokter.NamaDOkter;
                        }

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                        model.Jam = DateTime.Now;
                        model.Tanggal = DateTime.Today;
                    }

                    //var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoReg == model.NoReg);
                    //var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    //var dokumen_temp = s.trTemplate.Where(e => e.DokumenID == "Poliklinik Anak").ToList();

                    //ViewBag.DokumenID = dokumen_master.DokumenID;
                    //ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    //model.ListTemplate = new List<SelectItemListFormICU>();
                    //foreach (var list in dokumen_temp)
                    //{
                    //    model.ListTemplate.Add(new SelectItemListFormICU()
                    //    {
                    //        Value = list.NoReg,
                    //        Text = list.NamaTemplate
                    //    });
                    //}
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string noreg, string copy)
        {
            var model = new EMRFormulirKreteriaMasukICUViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var dokumen = s.FormulirKreteriaMasukICU.FirstOrDefault(x => x.NoReg == copy);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRFormulirKreteriaMasukICUViewModel>(dokumen);
                        model.NoReg = noreg;

                        var dpjp = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DPJP);
                        if (dpjp != null) model.DPJPNama = dpjp.NamaDOkter;

                    }
                    else
                    {
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                    }
                    model.MODEVIEW = 0;
                    var dokumen_pasien = s.trDokumenAssesmenPasien.FirstOrDefault(q => q.NoReg == noreg);
                    var dokumen_master = s.mDokumen.FirstOrDefault(d => d.DokumenID == dokumen_pasien.DokumenID);
                    var dokumen_temp = s.trTemplate.Where(e => e.DokumenID == dokumen_pasien.DokumenID && e.SectionID == sectionid).ToList();

                    ViewBag.DokumenID = dokumen_master.DokumenID;
                    ViewBag.NamaDokumen = dokumen_master.NamaDokumen;
                    model.ListTemplate = new List<SelectItemListFormICU>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListFormICU()
                        {
                            Value = list.NoReg,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {

            try
            {
                var item = new EMRFormulirKreteriaMasukICUViewModel();

                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var model = s.FormulirKreteriaMasukICU.FirstOrDefault(x => x.NoReg == item.NoReg);
                    var activity = "";
                    //var dokumenid = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<FormulirKreteriaMasukICU>(item);
                        s.FormulirKreteriaMasukICU.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoReg == item.NoReg);
                        activity = "Create Formulir Kreteria ICU";
                    }
                    else
                    {
                        model = IConverter.Cast<FormulirKreteriaMasukICU>(item);
                        s.FormulirKreteriaMasukICU.AddOrUpdate(model);
                        activity = "Update Formulir Kreteria ICU";
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoReg}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFFormulirKreteriaMasukICU(string noreg, string section)
        {
            var tipepelayanan = Request.Cookies["SectionNamaPelayanan"].Value;
            var rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_Formulir_Kriteria_Masuk_ICU.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand($"Rpt_Formulir_Kriteria_Masuk_ICU", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", section));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_Formulir_Kriteria_Masuk_ICU;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                //cmd.Add(new SqlCommand("Rpt_ObvervasiKeseimbanganCairan", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].Parameters.Add(new SqlParameter("@SectionID", section));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["Rpt_ObvervasiKeseimbanganCairan;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}