﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class CatatanEdukasiTerintegrasiRJController : Controller
    {
        // GET: CatatanEdukasiTerintegrasiRJ
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    model.NoReg = m.NoReg;
                    model.SectionID = sectionid;
                    var agama = eSIM.mPasien.FirstOrDefault(x => x.NRM == model.NRM);
                    var magama = eSIM.mAgama.FirstOrDefault(x => x.AgamaID == agama.Agama);
                    

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.CatatanEdukasiTerintegrasiRJ.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.CETRJ = IConverter.Cast<CatatanEdukasiTerintegrasiRJViewModel>(item);

                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.DokterSpesialis_NamaEdukator);
                            if (dokter != null)
                            {
                                model.CETRJ.DokterSpesialis_NamaEdukatorNama = dokter.NamaDOkter;
                            }

                            var dokter2 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.ManajemenNyeri_Edukator);
                            if (dokter2 != null)
                            {
                                model.CETRJ.ManajemenNyeri_EdukatorNama = dokter2.NamaDOkter;
                            }

                            var dokter3 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Nutrisi_Edukator);
                            if (dokter3 != null)
                            {
                                model.CETRJ.Nutrisi_EdukatorNama = dokter3.NamaDOkter;
                            }

                            var dokter4 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Rohaniawan_Edukator);
                            if (dokter4 != null)
                            {
                                model.CETRJ.Rohaniawan_EdukatorNama = dokter4.NamaDOkter;
                            }

                            var dokter5 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Farmasi_Edukator);
                            if (dokter5 != null)
                            {
                                model.CETRJ.Farmasi_EdukatorNama = dokter5.NamaDOkter;
                            }

                            var dokter6 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Perawat_Edukator);
                            if (dokter6 != null)
                            {
                                model.CETRJ.Perawat_EdukatorNama = dokter6.NamaDOkter;
                            }

                            var dokter7 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Rehabilitasi_Edukator);
                            if (dokter7 != null)
                            {
                                model.CETRJ.Rehabilitasi_EdukatorNama = dokter7.NamaDOkter;
                            }

                            model.CETRJ.JenisKerjasama = m.JenisKerjasama;
                            model.CETRJ.NamaPanggilan = m.NamaPasien;
                            model.CETRJ.Alamat = m.Alamat;
                            model.CETRJ.Phone = m.PenanggungPhone;
                            model.CETRJ.Pendidikan = m.Pendidikan;
                            model.CETRJ.Agama = magama == null ? "-" : magama.Agama;
                            model.CETRJ.DokumenID = "Catatan Terintegrasi RJ";

                            #region === Verif Dokter Spesialis
                            var fingerdokterspesialis = eEMR.CatatanEdukasiTerintegrasiRJ.Where(x => x.DokterSpesialis_NamaEdukator == model.CETRJ.DokterSpesialis_NamaEdukator && x.TandaTanganDokterSpesialis_NamaEdukator == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerdokterspesialis != null)
                            {
                                model.CETRJ.SudahRegDokterSpesialis = 1;
                            }
                            else
                            {
                                model.CETRJ.SudahRegDokterSpesialis = 0;
                            }

                            var fingerpasien = eEMR.CatatanEdukasiTerintegrasiRJ.Where(x => x.NRM == model.CETRJ.NRM && x.TandaTanganDokterSpesialis_Pasien == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerpasien != null)
                            {
                                model.CETRJ.SudahRegPasienSpesialis = 1;
                            }
                            else
                            {
                                model.CETRJ.SudahRegPasienSpesialis = 0;
                            }

                            if (model.CETRJ.DokterSpesialis_NamaEdukator != null)
                            {
                                ViewBag.UrlFingerCETRJDokterSpesialisDokter = GetEncodeUrlDokterSpesialis(model.CETRJ.DokterSpesialis_NamaEdukator, model.CETRJ.NoReg, sectionid);
                            }
                            ViewBag.UrlFingerCETRJDokterSpesialisPasien = GetEncodeUrlDokterSpesialis(model.CETRJ.NRM, model.CETRJ.NoReg, sectionid);
                            #endregion

                            #region === Verif Manajemen Nyeri
                            var fingermanajemennyeri = eEMR.CatatanEdukasiTerintegrasiRJ.Where(x => x.ManajemenNyeri_Edukator == model.CETRJ.ManajemenNyeri_Edukator && x.TandaTanganManajemenNyeri_Edukator == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingermanajemennyeri != null)
                            {
                                model.CETRJ.SudahRegManajemenNyeri = 1;
                            }
                            else
                            {
                                model.CETRJ.SudahRegManajemenNyeri = 0;
                            }

                            var fingerpasienmanajemennyeri = eEMR.CatatanEdukasiTerintegrasiRJ.Where(x => x.NRM == model.CETRJ.NRM && x.TandaTanganManajemenNyeri_Pasien == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerpasienmanajemennyeri != null)
                            {
                                model.CETRJ.SudahRegPasienManajemenNyeri = 1;
                            }
                            else
                            {
                                model.CETRJ.SudahRegPasienManajemenNyeri = 0;
                            }

                            if (model.CETRJ.ManajemenNyeri_Edukator != null)
                            {
                                ViewBag.UrlFingerCETRJManajemenNyeriDokter = GetEncodeUrlManajemenNyeri(model.CETRJ.ManajemenNyeri_Edukator, model.CETRJ.NoReg, sectionid);
                            }
                            ViewBag.UrlFingerCETRJManajemenNyeriPasien = GetEncodeUrlManajemenNyeri(model.CETRJ.NRM, model.CETRJ.NoReg, sectionid);
                            #endregion

                            #region === Verif Nutrisi
                            var fingernutrisiedukator = eEMR.CatatanEdukasiTerintegrasiRJ.Where(x => x.Nutrisi_Edukator == model.CETRJ.Nutrisi_Edukator && x.TandaTanganNutrisi_Edukator == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingernutrisiedukator != null)
                            {
                                model.CETRJ.SudahRegNutrisi = 1;
                            }
                            else
                            {
                                model.CETRJ.SudahRegNutrisi = 0;
                            }

                            var fingerpasiennutrisiedukator = eEMR.CatatanEdukasiTerintegrasiRJ.Where(x => x.NRM == model.CETRJ.NRM && x.TandaTanganNutrisi_Pasien == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerpasiennutrisiedukator != null)
                            {
                                model.CETRJ.SudahRegPasienNutrisi = 1;
                            }
                            else
                            {
                                model.CETRJ.SudahRegPasienNutrisi = 0;
                            }

                            if (model.CETRJ.Nutrisi_Edukator != null)
                            {
                                ViewBag.UrlFingerCETRJNutrisiDokter = GetEncodeUrlNutrisi(model.CETRJ.Nutrisi_Edukator, model.CETRJ.NoReg, sectionid);
                            }
                            ViewBag.UrlFingerCETRJNutrisiPasien = GetEncodeUrlNutrisi(model.CETRJ.NRM, model.CETRJ.NoReg, sectionid);
                            #endregion

                            #region === Verif Rohaniawan
                            var fingerrohaniawan = eEMR.CatatanEdukasiTerintegrasiRJ.Where(x => x.Rohaniawan_Edukator == model.CETRJ.Rohaniawan_Edukator && x.TandaTanganRohaniawan_Edukator == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerrohaniawan != null)
                            {
                                model.CETRJ.SudahRegRohaniawan= 1;
                            }
                            else
                            {
                                model.CETRJ.SudahRegRohaniawan= 0;
                            }

                            var fingerpasienrohaniawan = eEMR.CatatanEdukasiTerintegrasiRJ.Where(x => x.NRM == model.CETRJ.NRM && x.TandaTanganRohaniawan_Pasien == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerpasienrohaniawan != null)
                            {
                                model.CETRJ.SudahRegPasienRohaniawan= 1;
                            }
                            else
                            {
                                model.CETRJ.SudahRegPasienRohaniawan= 0;
                            }

                            if (model.CETRJ.Rohaniawan_Edukator != null)
                            {
                                ViewBag.UrlFingerCETRJRohaniawanDokter = GetEncodeUrlRohaniawan(model.CETRJ.Rohaniawan_Edukator, model.CETRJ.NoReg, sectionid);
                            }
                            ViewBag.UrlFingerCETRJRohaniawanPasien = GetEncodeUrlRohaniawan(model.CETRJ.NRM, model.CETRJ.NoReg, sectionid);
                            #endregion

                            #region === Verif Farmasi
                            var fingerfarmasi = eEMR.CatatanEdukasiTerintegrasiRJ.Where(x => x.Farmasi_Edukator == model.CETRJ.Farmasi_Edukator && x.TandaTanganFarmasi_Edukator == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerfarmasi != null)
                            {
                                model.CETRJ.SudahRegFarmasi = 1;
                            }
                            else
                            {
                                model.CETRJ.SudahRegFarmasi = 0;
                            }

                            var fingerpasienfarmasi = eEMR.CatatanEdukasiTerintegrasiRJ.Where(x => x.NRM == model.CETRJ.NRM && x.TandaTanganFarmasi_Pasien == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerpasienfarmasi != null)
                            {
                                model.CETRJ.SudahRegPasienFarmasi = 1;
                            }
                            else
                            {
                                model.CETRJ.SudahRegPasienFarmasi = 0;
                            }

                            if (model.CETRJ.Farmasi_Edukator != null)
                            {
                                ViewBag.UrlFingerCETRJFarmasiDokter = GetEncodeUrlFarmasi(model.CETRJ.Farmasi_Edukator, model.CETRJ.NoReg, sectionid);
                            }
                            ViewBag.UrlFingerCETRJFarmasiPasien = GetEncodeUrlFarmasi(model.CETRJ.NRM, model.CETRJ.NoReg, sectionid);
                            #endregion

                            #region === Verif Perawat
                            var fingerperawat = eEMR.CatatanEdukasiTerintegrasiRJ.Where(x => x.Perawat_Edukator == model.CETRJ.Perawat_Edukator && x.TandaTanganPerawat_Edukator == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerperawat != null)
                            {
                                model.CETRJ.SudahRegPerawat = 1;
                            }
                            else
                            {
                                model.CETRJ.SudahRegPerawat = 0;
                            }

                            var fingerpasienperawat = eEMR.CatatanEdukasiTerintegrasiRJ.Where(x => x.NRM == model.CETRJ.NRM && x.TandaTanganPerawat_Pasien == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerpasienperawat != null)
                            {
                                model.CETRJ.SudahRegPasienPerawat = 1;
                            }
                            else
                            {
                                model.CETRJ.SudahRegPasienPerawat = 0;
                            }

                            if (model.CETRJ.Perawat_Edukator != null)
                            {
                                ViewBag.UrlFingerCETRJPerawatDokter = GetEncodeUrlPerawat(model.CETRJ.Perawat_Edukator, model.CETRJ.NoReg, sectionid);
                            }
                            ViewBag.UrlFingerCETRJPerawatPasien = GetEncodeUrlPerawat(model.CETRJ.NRM, model.CETRJ.NoReg, sectionid);
                            #endregion

                            #region === Verif Rehabilitasi
                            var fingerrehabilitasi = eEMR.CatatanEdukasiTerintegrasiRJ.Where(x => x.Rehabilitasi_Edukator == model.CETRJ.Rehabilitasi_Edukator && x.TandaTanganRehabilitasi_Edukator == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerrehabilitasi != null)
                            {
                                model.CETRJ.SudahRegRehabilitasi = 1;
                            }
                            else
                            {
                                model.CETRJ.SudahRegRehabilitasi = 0;
                            }

                            var fingerpasienrehabilitasi = eEMR.CatatanEdukasiTerintegrasiRJ.Where(x => x.NRM == model.CETRJ.NRM && x.TandaTanganRehabilitasi_Pasien == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerpasienrehabilitasi != null)
                            {
                                model.CETRJ.SudahRegPasienRehabilitasi = 1;
                            }
                            else
                            {
                                model.CETRJ.SudahRegPasienRehabilitasi = 0;
                            }

                            if (model.CETRJ.Rehabilitasi_Edukator != null)
                            {
                                ViewBag.UrlFingerCETRJRehabilitasiDokter = GetEncodeUrlRehabilitasi(model.CETRJ.Rehabilitasi_Edukator, model.CETRJ.NoReg, sectionid);
                            }
                            ViewBag.UrlFingerCETRJRehabilitasiPasien = GetEncodeUrlRehabilitasi(model.CETRJ.NRM, model.CETRJ.NoReg, sectionid);
                            #endregion
                        }
                        else
                        {
                            item = new CatatanEdukasiTerintegrasiRJ();
                            model.CETRJ = IConverter.Cast<CatatanEdukasiTerintegrasiRJViewModel>(item);
                            model.CETRJ.Tanggal = DateTime.Now;
                            model.CETRJ.DokterSpesialis_Tanggal = DateTime.Today;
                            model.CETRJ.Nutrisi_Tanggal = DateTime.Today;
                            model.CETRJ.ManajemenNyeri_Tanggal = DateTime.Today;
                            model.CETRJ.Rohaniawan_Tanggal = DateTime.Today;
                            model.CETRJ.Farmasi_Tanggal = DateTime.Today;
                            model.CETRJ.Perawat_Tanggal = DateTime.Today;
                            model.CETRJ.Rehabilitasi_Tanggal = DateTime.Today;
                            model.CETRJ.JenisKerjasama = m.JenisKerjasama;
                            model.CETRJ.NamaPanggilan = m.NamaPasien;
                            model.CETRJ.Alamat = m.Alamat;
                            model.CETRJ.Phone = m.PenanggungPhone;
                            model.CETRJ.Pendidikan = m.Pendidikan;
                            model.CETRJ.Agama = magama == null ? "-" : magama.Agama;
                            model.CETRJ.Bahasa_Indonesia = true;
                            model.CETRJ.TidakAdaHambatan = true;
                            model.CETRJ.KesediaanPasien = "Ya";
                            model.CETRJ.KondisiMedis = true;
                            model.CETRJ.RencanaPerawatan = true;
                            model.CETRJ.TeknikRehabilitasi = true;
                            model.CETRJ.EdukasiLainnya1 = true;
                            model.CETRJ.EdukasiLainnya2 = true;
                            model.CETRJ.PerawatanLanjutan = true;
                            model.CETRJ.ManajemenNyeri_DapatMenjelaskan = true;
                            model.CETRJ.Penerjemah = "Tidak Perlu";
                            model.CETRJ.EdukasiLainnya1_Ket = "Memakai Masker, Atur Jarak";
                            model.CETRJ.EdukasiLainnya2_Ket = "Cara mencuci tangan yang baik dan benar";
                            model.CETRJ.DokterSpesialis_Metode = "Diskusi";
                            model.CETRJ.Perawat_Metode = "Demonstrasi";
                            model.CETRJ.DokterSpesialis_MetodeMenit = "51058";
                            model.CETRJ.Perawat_MetodeMenit = "5";
                            model.CETRJ.DokterSpesialis_TanpaDibantu = true;
                            model.CETRJ.Perawat_Mampu = true;
                            model.CETRJ.DokumenID = "Catatan Terintegrasi RJ";
                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string CatatanEdukasiTerintegrasiRJ_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.CETRJ.NoReg = noreg;
                            item.CETRJ.SectionID = sectionid;
                            item.CETRJ.NRM = m.NRM;
                            item.CETRJ.Tanggal = DateTime.Now;
                            var model = eEMR.CatatanEdukasiTerintegrasiRJ.FirstOrDefault(x => x.SectionID == item.CETRJ.SectionID && x.NoReg == item.NoReg);

                            if (model == null)
                            {
                                model = new CatatanEdukasiTerintegrasiRJ();
                                var o = IConverter.Cast<CatatanEdukasiTerintegrasiRJ>(item.CETRJ);
                                eEMR.CatatanEdukasiTerintegrasiRJ.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<CatatanEdukasiTerintegrasiRJ>(item.CETRJ);
                                eEMR.CatatanEdukasiTerintegrasiRJ.AddOrUpdate(model);
                            }

                            if (item.CETRJ.save_template == true && item.CETRJ.nama_template != null)
                            {
                                var temp = new trTemplate();
                                temp.NoReg = item.CETRJ.NoReg;
                                temp.NamaTemplate = item.CETRJ.nama_template;
                                temp.DokterID = item.CETRJ.DokterSpesialis_Pasien;
                                temp.DokumenID = "Catatan Terintegrasi RJ";
                                temp.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                                eEMR.trTemplate.Add(temp);
                            }

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFCatatanEdukasiTerintegrasiRJ(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_CatatanEdukasiTerintegrasiRJ.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"Rpt_CatatanEdukasiTerintegrasiRJ", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_CatatanEdukasiTerintegrasiRJ;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }

        public string GetEncodeUrlDokterSpesialis(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCatatanEdukasiTerintegrasiRJ/get_keyskdpjpdokterspesialis";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlManajemenNyeri(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCatatanEdukasiTerintegrasiRJ/get_keyskdpjpmanajemennyeri";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlNutrisi(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCatatanEdukasiTerintegrasiRJ/get_keyskdpjpnutrisi";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlRohaniawan(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCatatanEdukasiTerintegrasiRJ/get_keyskdpjprohaniawan";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlFarmasi(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCatatanEdukasiTerintegrasiRJ/get_keyskdpjpfarmasi";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlPerawat(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCatatanEdukasiTerintegrasiRJ/get_keyskdpjpperawat";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlRehabilitasi(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCatatanEdukasiTerintegrasiRJ/get_keyskdpjprehabilitasi";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }
    }
}