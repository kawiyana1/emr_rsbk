﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class EMRAssesmentPraAnestesiDanSedasiController : Controller
    {
        // GET: EMRAssesmentPraAnestesiDanSedasi
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string noreg, string nrm, int view = 0)
        {
            var model = new AssesmentPraAnestesiDanSedasiViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var getdatapasien = sim.VW_DataPasienGet.Where(x => x.NRM == nrm).FirstOrDefault();
                    var dokumen = s.AssesmentPraAnestesiDanSedasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<AssesmentPraAnestesiDanSedasiViewModel>(dokumen);
                        var dktpolimata = sim.mDokter.FirstOrDefault(x => x.DokterID == model.NamaDokter);
                        if (dktpolimata != null)
                        {
                            model.NamaDokterNama = dktpolimata.NamaDOkter;
                        }
                    

                        #region === Detail 
                        model.Saran_List = new ListDetail<AssesmentPraAnestesiDanSedasiModelDetail>();
                        var detail_2 = s.PraAnestesiDanSedasi_Detail.Where(x => x.NoReg == noreg).ToList();
                        foreach (var x in detail_2)
                        {
                            var y = IConverter.Cast<AssesmentPraAnestesiDanSedasiModelDetail>(x);
                            model.Saran_List.Add(false, y);
                        }
                        #endregion

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                    }
                    model.NoReg = noreg;
                    model.SectionID = sectionid;
                    model.NRM = nrm;
                    if (getdatapasien != null)
                    {
                        model.NamaPasien = getdatapasien.NamaPasien;
                        model.JenisKelamin = getdatapasien.JenisKelamin;
                    }
                    var dokumen_temp = s.trTemplate.Where(e => e.DokumenID == "Pra Anestesi dan Sedasi").ToList();

                    ViewBag.DokumenID = "Pra Anestesi dan Sedasi";
                    ViewBag.NamaDokumen = "Pra Anestesi dan Sedasi";

                    model.ListTemplate = new List<SelectItemListPraAnestesiSedasi>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListPraAnestesiSedasi()
                        {
                            Value = list.NoReg,
                            Text = list.NamaTemplate
                        });
                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string noreg, string copy, string nrm)
        {
            var model = new AssesmentPraAnestesiDanSedasiViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var dokumen = s.AssesmentPraAnestesiDanSedasi.FirstOrDefault(x => x.NoReg == copy);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<AssesmentPraAnestesiDanSedasiViewModel>(dokumen);
                        model.NoReg = noreg;
                        model.NRM = nrm;
                        model.SectionID = sectionid;

                        var dpjp = sim.mDokter.FirstOrDefault(e => e.DokterID == model.NamaDokter);
                        if (dpjp != null) model.NamaDokterNama = dpjp.NamaDOkter;

                    }
                    else
                    {
                        model.NRM = nrm;
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                    }
                    model.MODEVIEW = 0;

                    var dokumen_temp = s.trTemplate.Where(e => e.DokumenID == "Pra Anestesi dan Sedasi").ToList();

                    ViewBag.DokumenID = "Pra Anestesi dan Sedasi";
                    ViewBag.NamaDokumen = "Pra Anestesi dan Sedasi";

                    model.ListTemplate = new List<SelectItemListPraAnestesiSedasi>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListPraAnestesiSedasi()
                        {
                            Value = list.NoReg,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {

            if (ModelState.IsValid)
            {
                var req = Request.Form[""];
            }
            try
            {
                var item = new AssesmentPraAnestesiDanSedasiViewModel();

                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var model = s.AssesmentPraAnestesiDanSedasi.FirstOrDefault(x => x.NoReg == item.NoReg);
                    var activity = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<AssesmentPraAnestesiDanSedasi>(item);
                        s.AssesmentPraAnestesiDanSedasi.Add(o);
                        activity = "Create Assesmen Pra Anestesi dan Sedasi";
                    }
                    else
                    {
                        model = IConverter.Cast<AssesmentPraAnestesiDanSedasi>(item);
                        s.AssesmentPraAnestesiDanSedasi.AddOrUpdate(model);
                        activity = "Update Assesmen Pra Anestesi dan Sedasi";
                    }

                    if (item.save_template == true && item.nama_template != null)
                    {
                        var temp = new trTemplate();
                        temp.NoReg = item.NoReg;
                        temp.NamaTemplate = item.nama_template;
                        temp.DokterID = item.NamaDokter;
                        temp.DokumenID = "Pra Anestesi dan Sedasi";
                        temp.SectionID = sectionid;
                        s.trTemplate.Add(temp);
                    }

                    #region === Detail 
                    if (item.Saran_List == null) item.Saran_List = new ListDetail<AssesmentPraAnestesiDanSedasiModelDetail>();
                    item.Saran_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.Saran_List)
                    {
                        x.Model.NoReg = item.NoReg;
                    }
                    var new_list = item.Saran_List;
                    var real_list = s.PraAnestesiDanSedasi_Detail.Where(x => x.NoReg == item.NoReg).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.PraAnestesiDanSedasi_Detail.Remove(x);
                    }

                    foreach (var x in new_list)
                    {
                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null)
                        {
                            s.PraAnestesiDanSedasi_Detail.Add(IConverter.Cast<PraAnestesiDanSedasi_Detail>(x.Model));
                        }
                        else
                        {
                            _m.No = x.Model.No;
                            _m.Tanggal = DateTime.Now;
                            _m.Jam = DateTime.Now;
                            _m.Saran = x.Model.Saran;
                        }
                    }
                    #endregion

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoReg}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}