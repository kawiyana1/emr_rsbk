﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class EMRIlmuPenyakitBedahTraumaController : Controller
    {
        // GET: EMRIlmuPenyakitBedahTrauma
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string noreg, string nrm, int view = 0)
        {
            var model = new EMRIlmuPenyakitBedahTraumaViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var m = sim.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var dokumen = s.IlmuPenyakitBedahTrauma.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRIlmuPenyakitBedahTraumaViewModel>(dokumen);
                        var dktpolimata = sim.mDokter.FirstOrDefault(x => x.DokterID == model.Dokter);
                        if (dktpolimata != null)
                        {
                            model.DokterNama = dktpolimata.NamaDOkter;
                        }
                        model.NamaKeluarga = m.PenanggungNama;
                        model.AlamatKeluarga = m.Alamat;
                        model.TeleponKeluarga = m.PenanggungPhone;
                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                        model.NRM = nrm;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                        model.NamaKeluarga = m.PenanggungNama;
                        model.AlamatKeluarga = m.Alamat;
                        model.TeleponKeluarga = m.PenanggungPhone;
                    }

                    var dokumen_temp = s.trTemplate.Where(e => e.DokumenID == "Ilmu Penyakit Bedah Trauma").ToList();

                    ViewBag.DokumenID = "Ilmu Penyakit Bedah Trauma";
                    ViewBag.NamaDokumen = "Ilmu Penyakit Bedah Trauma";
                    model.ListTemplate = new List<SelectItemListPenyakitBedah>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListPenyakitBedah()
                        {
                            Value = list.NoReg,
                            Text = list.NamaTemplate
                        });
                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string noreg, string copy)
        {
            var model = new EMRIlmuPenyakitBedahTraumaViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var m = sim.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var dokumen = s.IlmuPenyakitBedahTrauma.FirstOrDefault(x => x.NoReg == copy);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRIlmuPenyakitBedahTraumaViewModel>(dokumen);
                        model.NoReg = noreg;

                        var Dokter = sim.mDokter.FirstOrDefault(e => e.DokterID == model.Dokter);
                        if (Dokter != null) model.DokterNama = Dokter.NamaDOkter;
                        model.NamaKeluarga = m.PenanggungNama;
                        model.AlamatKeluarga = m.Alamat;
                        model.TeleponKeluarga = m.PenanggungPhone;
                    }
                    else
                    {
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                        model.NamaKeluarga = m.PenanggungNama;
                        model.AlamatKeluarga = m.Alamat;
                        model.TeleponKeluarga = m.PenanggungPhone;
                    }
                    model.MODEVIEW = 0;
                    var dokumen_temp = s.trTemplate.Where(e => e.DokumenID == "Ilmu Penyakit Bedah Trauma").ToList();

                    ViewBag.DokumenID = "Ilmu Penyakit Bedah Trauma";
                    ViewBag.NamaDokumen = "Ilmu Penyakit Bedah Trauma";
                    model.ListTemplate = new List<SelectItemListPenyakitBedah>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListPenyakitBedah()
                        {
                            Value = list.NoReg,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {

            if (ModelState.IsValid)
            {
                var req = Request.Form[""];
            }
            try
            {
                var item = new EMRIlmuPenyakitBedahTraumaViewModel();

                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var model = s.IlmuPenyakitBedahTrauma.FirstOrDefault(x => x.NoReg == item.NoReg);
                    var activity = "";
                    var dokumenid = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<IlmuPenyakitBedahTrauma>(item);
                        s.IlmuPenyakitBedahTrauma.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoReg == item.NoReg);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Create Assesmen Bedah Trauma";
                    }
                    else
                    {
                        model = IConverter.Cast<IlmuPenyakitBedahTrauma>(item);
                        s.IlmuPenyakitBedahTrauma.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoReg == item.NoReg);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Update Assesmen Bedah Trauma";
                    }

                    if (item.save_template == true && item.nama_template != null)
                    {
                        var temp = new trTemplate();
                        temp.NoReg = item.NoReg;
                        temp.NamaTemplate = item.nama_template;
                        temp.DokterID = item.Dokter;
                        temp.DokumenID = "Ilmu Penyakit Bedah Trauma";
                        temp.SectionID = sectionid;
                        s.trTemplate.Add(temp);
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoReg}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}