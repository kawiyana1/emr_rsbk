﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSCB.Controllers
{
    public class CPPTController : Controller
    {
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string noreg, string nrm, string copy = "")
        {
            var model = new CPPTViewModel();
            using (var s = new EMR_Entities())
            {
                if (copy != "")
                {
                    var dokumen = s.SIMtrSOAP.FirstOrDefault(x => x.NoReg == copy);
                    model = IConverter.Cast<CPPTViewModel>(dokumen);
                    model._METHOD = "CREATE";
                    model.NoReg = noreg;
                    model.NRM = nrm;
                }
                else
                {
                    model._METHOD = "CREATE";
                    model.NoReg = noreg;
                    model.NRM = nrm;
                    model.Tanggal = DateTime.Today;
                    model.Jam = DateTime.Now;
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("Update")]
        public ActionResult Update(string id = "")
        {
            var model = new CPPTViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var dokumen = s.SIMtrSOAP.FirstOrDefault(x => x.NoReg == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<CPPTViewModel>(dokumen);
                        model._METHOD = "UPDATE";
                        var dokter = sim.mDokter.FirstOrDefault(x => x.DokterID == model.DPJP);
                        if (dokter != null)
                        {
                            model.DPJPNama = dokter.NamaDOkter;
                        }
                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost, ValidateInput(false)]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new CPPTViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    if (item.DPJP == null) return JsonHelper.JsonMsgError("Dokter DPJP tidak boleh kosong.");

                    var model = s.SIMtrSOAP.FirstOrDefault(x => x.NoReg == item.NoReg);

                    var activity = "";
                    if (item._METHOD == "CREATE")
                    {
                        //var nobukti = s.AutoNumber_EMR().FirstOrDefault();
                       

                        var o = IConverter.Cast<SIMtrSOAP>(item);
                        //o.No = nobukti;
                        s.SIMtrSOAP.Add(o);

                        activity = "Create Assesmen CPPT";

                        if (item.saveTemplate == true)
                        {
                            var temp = new trTemplate();
                            temp.NoReg = item.NoReg;
                            temp.NamaTemplate = item.templateName;
                            temp.DokterID = item.DPJP;
                            temp.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                            temp.DokumenID = "CPPT";
                            s.trTemplate.Add(temp);
                        }
                    }
                    else
                    {
                        model = IConverter.Cast<SIMtrSOAP>(item);
                        s.SIMtrSOAP.AddOrUpdate(model);

                        
                        if (item.saveTemplate == true)
                        {
                            var check_temp = s.trTemplate.FirstOrDefault(x => x.NamaTemplate == item.templateName);
                            if (check_temp == null)
                            {
                                var temp = new trTemplate();
                                temp.NoReg = item.NoReg;
                                temp.NamaTemplate = item.templateName;
                                temp.DokterID = item.DPJP;
                                temp.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                                temp.DokumenID = "CPPT";
                                s.trTemplate.Add(temp);
                            }
                        }
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoBukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost, ValidateInput(false)]
        [ActionName("SecondCreate")]
        public string SecondCreate(SOAPViewModel item, string kritisget)
        {
            try
            {
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    if (item.DPJP == null) throw new Exception("Dokter tidak boleh kosong.");

                    var model = s.SIMtrSOAP.FirstOrDefault(x => x.NoReg == item.NoReg);

                    var activity = "";
                    if (item._METHOD == "CREATE")
                    {
                        //var nobukti = s.AutoNumber_EMR().FirstOrDefault();

                        //item.CPPT_OGambar = Convert.ToBase64String(img);
                        item.Kritis = bool.Parse(kritisget);
                        var o = IConverter.Cast<SIMtrSOAP>(item);
                        o.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                        o.TanggalInput = DateTime.Today;
                        //o.TTDDPJP = Convert.FromBase64String(imgttd);
                        s.SIMtrSOAP.Add(o);

                        activity = "Create Assesmen CPPT";

                        //if (item.templateName != null)
                        //{
                        //    var temp = new trTemplate();
                        //    temp.NoReg = item.NoReg;
                        //    temp.NamaTemplate = item.templateName;
                        //    temp.DokterID = item.DPJP;
                        //    temp.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                        //    temp.DokumenID = "CPPT";
                        //    s.trTemplate.Add(temp);
                        //}
                    }
                    else if (item._METHOD == "UPDATE")
                    {
                        model = IConverter.Cast<SIMtrSOAP>(item);
                        model.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                        model.TanggalInput = DateTime.Today;
                        model.Kritis = bool.Parse(kritisget);
                        s.SIMtrSOAP.AddOrUpdate(model);

                        activity = "Update Assesmen CPPT";

                        //if (item.templateName != null)
                        //{
                        //    var check_temp = s.trTemplate.FirstOrDefault(x => x.NamaTemplate == item.templateName);
                        //    if (check_temp == null)
                        //    {
                        //        var temp = new trTemplate();
                        //        temp.NoReg = item.NoReg;
                        //        temp.NamaTemplate = item.templateName;
                        //        temp.DokterID = item.DPJP;
                        //        temp.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                        //        temp.DokumenID = "CPPT";
                        //        s.trTemplate.Add(temp);
                        //    }
                        //}
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.Username}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public string get_profesi(string dokter)
        {
            try
            {
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMR_Entities())
                    {
                        var dt = sim.mDokter.FirstOrDefault(x => x.DokterID == dokter);
                        if (dt != null)
                        {
                            //var prf = sim.mDokter.FirstOrDefault(x => x.KodeProfesi == dt.KodeProfesi);
                            //if (prf != null)
                            //{
                            //    return JsonConvert.SerializeObject(new
                            //    {
                            //        IsSuccess = true,
                            //        Data = prf.NamaProfesi,
                            //        Message = ""

                            //    });
                            //}
                        }
                    }
                }

                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = false,
                    Data = "",
                    Message = ""

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public string copyCPPT(int id)
        {
            try
            {
                var model = new SOAPViewModel();
                using (var s = new EMR_Entities())
                {
                    var dokumen = s.SIMtrSOAP.FirstOrDefault(x => x.No == id);
                    //if (Request.Cookies["SectionIDPelayanan"].Value == "SEC027") { 
                    //    model.CPPT_OGambar = dokumen.CPPT_OGambar == null ? "/Theme/images/4.jpg" : "data:image/jpeg;base64," + Convert.ToBase64String(dokumen.CPPT_OGambar);
                    //}
                    //model.TTDDPJP = dokumen.TTDDPJP == null ? "/Theme/images/4.jpg" : "data:image/jpeg;base64," + Convert.ToBase64String(dokumen.TTDDPJP);
                    model = IConverter.Cast<SOAPViewModel>(dokumen);
                }

                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = model,
                    Message = ""

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public string tempCPPT(string id)
        {
            try
            {
                var model = new CPPTViewModel();
                using (var s = new EMR_Entities())
                {
                    var tamp = s.trTemplate.Where(xx => xx.NamaTemplate == id).FirstOrDefault();
                    var dokumen = s.SIMtrSOAP.FirstOrDefault(x => x.NoReg == tamp.NoReg);
                    //if (Request.Cookies["SectionIDPelayanan"].Value == "SEC027")
                    //{
                    //    model.CPPT_OGambar = dokumen.CPPT_OGambar == null ? "/Theme/images/4.jpg" : "data:image/jpeg;base64," + Convert.ToBase64String(dokumen.CPPT_OGambar);
                    //}
                    model = IConverter.Cast<CPPTViewModel>(dokumen);
                }

                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = model,
                    Message = ""

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public string editCPPT(int id)
        {
            try
            {
                var model = new SOAPViewModel();
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMR_Entities())
                    {
                        var dokumen = s.SIMtrSOAP.FirstOrDefault(x => x.No == id);
                        model = IConverter.Cast<SOAPViewModel>(dokumen);
                        model.Tanggal_View = model.Tanggal.ToString("yyyy-MM-dd");
                        model.Jam_View = model.Jam.ToString(@"hh\:mm");
                        var dokter = sim.mDokter.FirstOrDefault(x => x.DokterID == model.DPJP);
                        var menyerahkan = sim.mDokter.FirstOrDefault(y => y.DokterID == model.MenyerahkanPasien);
                        var menerima = sim.mDokter.FirstOrDefault(y => y.DokterID == model.MenerimaPasien);
                        var bacaulang = sim.mDokter.FirstOrDefault(y => y.DokterID == model.PetugasBacaUlang);
                        var konfirmasi = sim.mDokter.FirstOrDefault(y => y.DokterID == model.PetugasKonfirmasi);
                        model.DPJPNama = dokter == null ? "" : dokter.NamaDOkter;
                        model.MenyerahkanPasienNama = menyerahkan == null ? "" : menyerahkan.NamaDOkter;
                        model.MenerimaPasienNama = menerima == null ? "" : menerima.NamaDOkter;
                        model.PetugasBacaUlangNama = bacaulang == null ? "" : bacaulang.NamaDOkter;
                        model.PetugasKonfirmasiNama = konfirmasi == null ? "" : konfirmasi.NamaDOkter;
                    }
                }

                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = model,
                    Message = ""

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string getListObat(string noreg)
        {
            try
            {
                var section_id = Request.Cookies["SectionIDPelayanan"].Value;
                var list_obat = "";
                using (var s = new EMR_Entities())
                {
                    //var m_list = s.SIMtrResep.Where(x => x.NoRegistrasi == noreg && x.SectionID == section_id).ToList();

                    //foreach (var y in m_list)
                    //{
                    //    var x_list = s.SIMtrResepDetail.Where(xx => xx.NoResep == y.NoResep).ToList();

                    //    foreach (var x in x_list)
                    //    {
                    //        var b = s.mBarang.Where(ss => ss.Barang_ID == x.Barang_ID).FirstOrDefault();

                    //        list_obat += b.Nama_Barang + " \t ( " + x.Qty + " ) \t" + x.Dosis + " \n";
                    //    }
                    //}

                }

                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = list_obat,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDF(string nrm)
        {
            try
            {
                var section_id = Request.Cookies["SectionIDPelayanan"].Value;
                string ServerPath = "~/Reports/EMR/";
                string reportname = $"Rpt_CPPT";

                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath(ServerPath), $"{reportname}.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand(reportname, conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NRM", nrm));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"{reportname};1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"{reportname}_Detail", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NRM", nrm));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"{reportname}_Detail;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        [HttpPost]
        public string ListTamplet(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter, string dokumenid)
        {
            try
            {
                var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    IQueryable<trTemplate> proses = s.trTemplate.Where(x => x.SectionID == sectionid && x.DokumenID == dokumenid);
                    //if (!string.IsNullOrEmpty(filter[1]))
                    //    proses = proses.Where($"{nameof(trDokumenTemplate.NamaTemplate)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<trTempletViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string Batal(int id, string noreg)
        {
            try
            {
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var m = s.SIMtrSOAP.FirstOrDefault(x => x.No == id );
                    s.SIMtrSOAP.Remove(m);
                    result = new ResultSS(s.SaveChanges());

                }
                return JsonHelper.JsonMsgDelete(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        
    }
}