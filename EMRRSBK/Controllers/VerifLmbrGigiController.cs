﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Configuration;

namespace EMRRSBK.Controllers
{
    public class VerifLmbrGigiController : Controller
    {
        // GET: VerifLmbrGigi
        #region === LEMBAR GIGI
        [HttpGet]
        public string CheckBerhasilLmbrGigi(string noreg, string section, string id)
        {
            var result = new ReturnFingerViewModel();
            using (var s = new EMR_Entities())
            {
                var finger = s.mFinger.Where(x => x.userid == id).FirstOrDefault();
                if (finger != null)
                {
                    var m = s.LembarPoliGigi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                    if (m != null)
                    {
                        if (m.TandaTanganDokter != null)
                        {
                            result.DokterPengkaji = "1";
                        }
                        else
                        {
                            result.DokterPengkaji = "0";
                        }
                    }
                }
                else
                {
                    result.DokterPengkaji = "0";
                }
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result
            });
        }
        [HttpGet]
        public string get_keylmbrgigi(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifLmbrGigi/checkveriflmbrgigi";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }
        public string checkveriflmbrgigi(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    if (section == "SEC008")
                    {
                        var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                        if (dokter != null)
                        {
                            user_id = dokter.DokterID;
                        }

                        var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                        var f = s.mFinger.Where(x => x.userid == user_id);
                        foreach (var i in f.ToList())
                        {
                            var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                            if (salt.ToUpper() == vStamp.ToUpper())
                            {
                                #region === Verif 
                                var dataslmbrgigi = s.LembarPoliGigi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                                if (dataslmbrgigi != null)
                                {
                                    if (dokter != null)
                                    {
                                        dataslmbrgigi.TandaTanganDokter = i.Gambar1;
                                    }
                                    else
                                    {
                                        dataslmbrgigi.TandaTangan = i.Gambar1;
                                    }
                                    s.SaveChanges();
                                }
                                #endregion
                                return "empty";
                            };
                        }
                    }

                }

            }
            return "Gagal";
        }
        #endregion

        #region === CHECKLIST KESELAMATAN PASIEN GIGI
        [HttpGet]
        public string CheckBerhasilChckKslmnt(string noreg, string section, string id)
        {
            var result = new ReturnFingerViewModel();
            using (var s = new EMR_Entities())
            {
                var finger = s.mFinger.Where(x => x.userid == id).FirstOrDefault();
                if (finger != null)
                {
                    var m = s.ChecklistKeselamatanPasienOperasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                    if (m != null)
                    {
                        if (m.TandaTanganDokter != null)
                        {
                            result.DokterPengkaji = "1";
                        }
                        else
                        {
                            result.DokterPengkaji = "0";
                        }

                        if (m.TandaTanganPerawat != null)
                        {
                            result.Perawat = "1";
                        }
                        else
                        {
                            result.Perawat = "0";
                        }
                    }
                }
                else
                {
                    result.DokterPengkaji = "0";
                    result.Perawat = "0";
                }
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result
            });
        }
        [HttpGet]
        public string get_keychckkslmtn(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifLmbrGigi/checkverifchckkslmnt";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }

        public string get_keychckkslmtnperawat(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifLmbrGigi/checkverifchckkslmntperawat";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }
        public string checkverifchckkslmnt(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    if (section == "SEC008")
                    {
                        var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                        if (dokter != null)
                        {
                            user_id = dokter.DokterID;
                        }

                        var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                        var f = s.mFinger.Where(x => x.userid == user_id);
                        foreach (var i in f.ToList())
                        {
                            var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                            if (salt.ToUpper() == vStamp.ToUpper())
                            {
                                #region === Verif 
                                var dataschckkslmtn = s.ChecklistKeselamatanPasienOperasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                                if (dataschckkslmtn != null)
                                {
                                    if (dokter != null)
                                    {
                                        dataschckkslmtn.TandaTanganDokter = i.Gambar1;
                                    }
                                    else
                                    {
                                        dataschckkslmtn.TandaTangan = i.Gambar1;
                                    }
                                    s.SaveChanges();
                                }
                                #endregion
                                return "empty";
                            };
                        }
                    }

                }

            }
            return "Gagal";
        }

        public string checkverifchckkslmntperawat(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    if (section == "SEC008")
                    {
                        var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                        if (dokter != null)
                        {
                            user_id = dokter.DokterID;
                        }

                        var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                        var f = s.mFinger.Where(x => x.userid == user_id);
                        foreach (var i in f.ToList())
                        {
                            var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                            if (salt.ToUpper() == vStamp.ToUpper())
                            {
                                #region === Verif 
                                var dataschckkslmtnperawats = s.ChecklistKeselamatanPasienOperasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                                if (dataschckkslmtnperawats != null)
                                {
                                    if (dokter != null)
                                    {
                                        dataschckkslmtnperawats.TandaTanganPerawat = i.Gambar1;
                                    }
                                    else
                                    {
                                        dataschckkslmtnperawats.TandaTangan = i.Gambar1;
                                    }
                                    s.SaveChanges();
                                }
                                #endregion
                                return "empty";
                            };
                        }
                    }

                }

            }
            return "Gagal";
        }
        #endregion
    }
}