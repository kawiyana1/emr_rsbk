﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class RMDomesticViolenceController : Controller
    {
        // GET: RMDomesticViolence
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    model.SectionID = sectionid;
                    model.NoReg = m.NoReg;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.RekamMedisDomesticViolence.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.RMDmsticViolnce = IConverter.Cast<RMDomesticViolenceViewModel>(item);
                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.DokterKonsulen);
                            if (dokter != null)
                            {
                                model.RMDmsticViolnce.DokterKonsulenNama = dokter.NamaDOkter;
                            }

                            var dokter1 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.DokterPemeriksa);
                            if (dokter1 != null)
                            {
                                model.RMDmsticViolnce.DokterPemeriksaNama = dokter1.NamaDOkter;
                            }

                            var perawat = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Perawat);
                            if (perawat != null)
                            {
                                model.RMDmsticViolnce.PerawatNama = perawat.NamaDOkter;
                            }
                            model.RMDmsticViolnce.NamaKlien = m.NamaPasien;
                            model.RMDmsticViolnce.NamaKorban = m.NamaPasien;
                            model.RMDmsticViolnce.Umur = m.UmurThn;
                            model.RMDmsticViolnce.UmurKorban = m.UmurThn;
                            model.RMDmsticViolnce.JenisKelamin = m.JenisKelamin;
                            model.RMDmsticViolnce.TempatLahir = m.TempatLahir;
                            model.RMDmsticViolnce.TglLahir = m.TglLahir;
                        }
                        else
                        {
                            item = new RekamMedisDomesticViolence();
                            model.RMDmsticViolnce = IConverter.Cast<RMDomesticViolenceViewModel>(item);
                            model.RMDmsticViolnce.Tanggal = DateTime.Today;
                            model.RMDmsticViolnce.NamaKlien = m.NamaPasien;
                            model.RMDmsticViolnce.Umur = m.UmurThn;
                            model.RMDmsticViolnce.JenisKelamin = m.JenisKelamin;
                            model.RMDmsticViolnce.TempatLahir = m.TempatLahir;
                            model.RMDmsticViolnce.TglLahir = m.TglLahir;
                            model.RMDmsticViolnce.Tgl_Pendaftaran = DateTime.Now;
                            model.RMDmsticViolnce.Kawin_Tahun = DateTime.Today;
                            model.RMDmsticViolnce.RiwayatKejadian_Tanggal = DateTime.Now;
                            model.RMDmsticViolnce.WaktuPemeriksaan = DateTime.Now;
                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string RMDomesticViolence_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.RMDmsticViolnce.NoReg = noreg;
                            item.RMDmsticViolnce.SectionID = Request.Cookies["SectionIDPelayanan"].Value; 
                            item.RMDmsticViolnce.NRM = m.NRM;
                            item.RMDmsticViolnce.Tanggal = DateTime.Now;
                            var model = eEMR.RekamMedisDomesticViolence.FirstOrDefault(x => x.SectionID == item.RMDmsticViolnce.SectionID && x.NoReg == item.NoReg);

                            if (model == null)
                            {
                                model = new RekamMedisDomesticViolence();
                                var o = IConverter.Cast<RekamMedisDomesticViolence>(item.RMDmsticViolnce);
                                eEMR.RekamMedisDomesticViolence.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<RekamMedisDomesticViolence>(item.RMDmsticViolnce);
                                eEMR.RekamMedisDomesticViolence.AddOrUpdate(model);
                            }

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDF(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"RekamMedisDomesticViolence_IGD.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"RekamMedisDomesticViolence_IGD", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"RekamMedisDomesticViolence_IGD;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}