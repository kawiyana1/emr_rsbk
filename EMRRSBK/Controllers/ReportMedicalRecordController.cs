﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class ReportMedicalRecordController : Controller
    {
        // GET: ReportMedicalRecord
        #region ==== R E P O R T  P D F  E M R 

        #region === IGD PENGKAJIANMEDIS 
        public ActionResult ExportPDFPengkajianMedis(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"AssPengkajianMedisSkrining.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("AssPengkajianMedisSkrining", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"AssPengkajianMedisSkrining;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"AssPengkajianMedisSkrining_Detail", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"AssPengkajianMedisSkrining_Detail;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === IGD ASESMENT PENGKAJIANTERINTEGRASI GAWAT DARURAT
        public ActionResult ExportPDFPKGawatDarurat(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"AssPengkajianTerintegrasiGawatDarurat.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("AssPengkajianTerintegrasiGawatDarurat", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"AssPengkajianTerintegrasiGawatDarurat;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"DiagnosaKeperawatanDetail", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"DiagnosaKeperawatanDetail;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"JenisObatDetail_ObatInjeksi", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"JenisObatDetail_ObatInjeksi;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"JenisObatDetail_ObatOral", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"JenisObatDetail_ObatOral;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"MelaksanakanAlergiTestDetail", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"MelaksanakanAlergiTestDetail;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"ObservasiCairanDetail", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"ObservasiCairanDetail;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"ObservasiKomprehensifDetail", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"ObservasiKomprehensifDetail;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"TindakanKeperawatanDetail", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"TindakanKeperawatanDetail;1"].SetDataSource(ds[i].Tables[0]);
                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === POLI MATA
        public ActionResult ExportPDFAssMata(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_IlmuPenyakitMata.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_IlmuPenyakitMata", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_IlmuPenyakitMata;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === POLI HIPERBARIK
        public ActionResult ExportPDFAssHpbrk(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_PengkajianAwalMedisDanKeperawatanHiperbarik1_1.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_PengkajianAwalMedisDanKeperawatanHiperbarik1_1", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_PengkajianAwalMedisDanKeperawatanHiperbarik1_1;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === POLI HIPERBARIK 2
        public ActionResult ExportPDFAssHpbrk2(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_PengkajianAwalMedisDanKeperawatanHiperbarik1_2.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_PengkajianAwalMedisDanKeperawatanHiperbarik1_2", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_PengkajianAwalMedisDanKeperawatanHiperbarik1_2;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"Rpt_RencanaKerjaDokter_Detail", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_RencanaKerjaDokter_Detail;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === POLI KULIT DAN KELAMIN
        public ActionResult ExportPDFAssKelaminKulit(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_IlmuPenyakitKulitDanKelamin.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_IlmuPenyakitKulitDanKelamin", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_IlmuPenyakitKulitDanKelamin;1"].SetDataSource(ds[i].Tables[0]);



                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === POLI KEBIDANAN
        public ActionResult ExportPDFPAwalKbdnKndng(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_PengkajianAwalKebidananKandungan.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_PengkajianAwalKebidananKandungan", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_PengkajianAwalKebidananKandungan;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"Rpt_RiwayatKehamilan_Detail", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_RiwayatKehamilan_Detail;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"Rpt_PenataLaksanaKebidanan_Detail", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_PenataLaksanaKebidanan_Detail;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === POLI KEBIDANAN ILMU GYNECOLOGY
        public ActionResult ExportPDFIlmuPenyakitGynecology(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_IlmuPenyakitGynecology.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_IlmuPenyakitGynecology", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_IlmuPenyakitGynecology;1"].SetDataSource(ds[i].Tables[0]);


                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === POLI KEBIDANAN ILMU OBSETRI
        public ActionResult ExportPDFIlmuPenyakitObsetri(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_IlmuPenyakitObsetri.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_IlmuPenyakitObsetri", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_IlmuPenyakitObsetri;1"].SetDataSource(ds[i].Tables[0]);



                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === POLI GIGI LEMBAR POLI GIGI 
        public ActionResult ExportPDFLembarPoliGigi(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_LembarPoliGigi.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_LembarPoliGigi", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_LembarPoliGigi;1"].SetDataSource(ds[i].Tables[0]);



                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === CHECK LIST KESELAMATAN PASIEN OPERASI
        public ActionResult ExportPDFChecklistKeselamatanPasienOperasi(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_ChecklistKeselamatanPasienOperasi.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_ChecklistKeselamatanPasienOperasi", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_ChecklistKeselamatanPasienOperasi;1"].SetDataSource(ds[i].Tables[0]);



                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === ILMU PENYAKIT BEDAH NON TRAUMA
        public ActionResult ExportPDFIlmuPenyakitBedahNonTrauma(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_IlmuPenyakitBedahNonTrauma.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_IlmuPenyakitBedahNonTrauma", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_IlmuPenyakitBedahNonTrauma;1"].SetDataSource(ds[i].Tables[0]);



                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === ILMU PENYAKIT BEDAH TRAUMA
        public ActionResult ExportPDFIlmuPenyakitBedahTrauma(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_IlmuPenyakitBedahTrauma.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_IlmuPenyakitBedahTrauma", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_IlmuPenyakitBedahTrauma;1"].SetDataSource(ds[i].Tables[0]);



                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === ILMU PENYAKIT ANAK
        public ActionResult ExportPDFIlmuPenyakitAnak(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_IlmuPenyakitAnak.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_IlmuPenyakitAnak", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_IlmuPenyakitAnak;1"].SetDataSource(ds[i].Tables[0]);



                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === ILMU PENYAKIT SARAF
        public ActionResult ExportPDFIlmuPenyakitSaraf(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_IlmuPenyakitSaraf.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_IlmuPenyakitSaraf", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_IlmuPenyakitSaraf;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"Rpt_RiwayatPengobatan", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_RiwayatPengobatan;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === ILMU PENYAKIT THT
        public ActionResult ExportPDFIlmuPenyakitTHT(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_IlmuPenyakitTHT.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_IlmuPenyakitTHT", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_IlmuPenyakitTHT;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === ILMU PENYAKIT JIWA
        public ActionResult ExportPDFIlmuPenyakitJiwa(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_IlmuPenyakitJiwa.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_IlmuPenyakitJiwa", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_IlmuPenyakitJiwa;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === ILMU PENYAKIT DALAM
        public ActionResult ExportPDFIlmuPenyakitDalam(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_IlmuPenyakitDalam.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_IlmuPenyakitDalam", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_IlmuPenyakitDalam;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"Rpt_IlmuPenyakitDalam_Detail", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_IlmuPenyakitDalam_Detail;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === ILMU PENYAKIT JANTUNG
        public ActionResult ExportPDFIlmuPenyakitJantung(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_IlmuPenyakitJantung.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_IlmuPenyakitJantung", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_IlmuPenyakitJantung;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"Rpt_RiwayatPengobatan", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_RiwayatPengobatan;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === ILMU PENYAKIT PSIKOLOGI ANAK REMAJA
        public ActionResult ExportPDFIlmuPenyakitPsikologiAnakRemaja(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_RekamPsikologisPasienAnakRemaja.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_RekamPsikologisPasienAnakRemaja", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_RekamPsikologisPasienAnakRemaja;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"Rpt_IdentitasKeluarga_Detail", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_IdentitasKeluarga_Detail;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"Rpt_TesPsikologi_Detail", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_TesPsikologi_Detail;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === ILMU PENYAKIT PSIKOLOGI DEWASA
        public ActionResult ExportPDFIlmuPenyakitPsikologiDewasa(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_RekamPsikologisPasienDewasa.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_RekamPsikologisPasienDewasa", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_RekamPsikologisPasienDewasa;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"Rpt_IdentitasKeluarga_Detail", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_IdentitasKeluarga_Detail;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"Rpt_TesPsikologi_Detail", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_TesPsikologi_Detail;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === LEMBAR PERSETUJUAN
        public ActionResult ExportPDFLembarPersetujuan(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_LembarPersetujuan.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_LembarPersetujuan", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_LembarPersetujuan;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === ASSESMENT POLI UMUM
        public ActionResult ExportPDFAssesmentPoliUmum(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_AsesmenPoliUmum.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_AsesmenPoliUmum", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_AsesmenPoliUmum;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === LAPORAN PEMERIKSAAN KESEHATAN POLI MCU
        public ActionResult ExportPDFLporanPemeriksaanPoliMCU(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_LaporanPemeriksaanKesehatan.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_LaporanPemeriksaanKesehatan", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_LaporanPemeriksaanKesehatan;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === KESIMPULAN HASIL PEMERIKSAAN KESEHATAN POLI MCU
        public ActionResult ExportPDFKesimpulanHasilPemeriksaanPoliMCU(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_KesimpulanHasilPemeriksaanKesehatan.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_KesimpulanHasilPemeriksaanKesehatan", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_KesimpulanHasilPemeriksaanKesehatan;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === LEMBAR FORMULIR POLI FISIOTHERAPI
        public ActionResult ExportPDFLembarFisiotherapi(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_LembarFormulirPoliFisiotherapi.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_LembarFormulirPoliFisiotherapi", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_LembarFormulirPoliFisiotherapi;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"Rpt_ProgramRawatJalan_Detail", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_ProgramRawatJalan_Detail;1"].SetDataSource(ds[i].Tables[0]);
                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === CATATAN FISIOTHERAPI
        public ActionResult ExportPDFCatatanFisiotherapi(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_CatatanFisiotherapi.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_CatatanFisiotherapi", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_CatatanFisiotherapi;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"Rpt_KunjunganFisiotherapi_Detail", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_KunjunganFisiotherapi_Detail;1"].SetDataSource(ds[i].Tables[0]);
                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === ASSESMENT PRAANESTESI DAN SEDASI
        public ActionResult ExportPDFAssPraAnesDanSedasi(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_AssesmentPraAnestesiDanSedasi.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_AssesmentPraAnestesiDanSedasi", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_AssesmentPraAnestesiDanSedasi;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"Rpt_PraAnestesiDanSedasi_Detail", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_PraAnestesiDanSedasi_Detail;1"].SetDataSource(ds[i].Tables[0]);
                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === ASSESMENT PRAOPERASI PEREMPUAN
        public ActionResult ExportPDFAssPraOprsPerempuan(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_AssesmentPraOperasiPerempuan.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_AssesmentPraOperasiPerempuan", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_AssesmentPraOperasiPerempuan;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === ASSESMENT PRAOPERASI LAKILAKI
        public ActionResult ExportPDFAssPraOprsLaki(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_AssesmentPraOperasiLakiLaki.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_AssesmentPraOperasiLakiLaki", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_AssesmentPraOperasiLakiLaki;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === ASSESMENT MEDIS DAN KEPERAWATAN
        public ActionResult ExportPDFAssAwalMedis(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_AsesmenAwalMedis.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_AsesmenAwalMedis", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_AsesmenAwalMedis;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"Rpt_AsesmenAwalMedis_Detail", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_AsesmenAwalMedis_Detail;1"].SetDataSource(ds[i].Tables[0]);
                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public ActionResult ExportPDFAssAwalKeperawatan(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_AsesmenAwalKeperawatan.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_AsesmenAwalKeperawatan", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_AsesmenAwalKeperawatan;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        #endregion

        #region === TRIAGE
        public ActionResult ExportPDFTriage(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Triage.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("AssPengkajianTerintegrasiGawatDarurat", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"AssPengkajianTerintegrasiGawatDarurat;1"].SetDataSource(ds[i].Tables[0]);


                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === ASSESMENT MEDIS DAN KEPERAWATAN
        public ActionResult ExportPDFAsuhanGiziAnak(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_AsuhanGiziAnak.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_AsuhanGiziAnak", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_AsuhanGiziAnak;1"].SetDataSource(ds[i].Tables[0]);
                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        #endregion

        #region === PENGKAJIAN KEPERAWATAN HD
        public ActionResult ExportPDFPengkajianKeperawatanHD(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_PengkajianAwalKeperawatanPasienHemodialisis.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_PengkajianAwalKeperawatanPasienHemodialisis", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_PengkajianAwalKeperawatanPasienHemodialisis;1"].SetDataSource(ds[i].Tables[0]);
                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        #endregion

        #region === PENGKAJIAN MEDIS HD
        public ActionResult ExportPDFPengkajianMedisHD(string noreg, string sectionid)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_PengkajianAwalMedisPasienHemodialisis.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Rpt_PengkajianAwalMedisPasienHemodialisis", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Rpt_PengkajianAwalMedisPasienHemodialisis;1"].SetDataSource(ds[i].Tables[0]);
                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        #endregion
        
        
        #endregion
    }
}