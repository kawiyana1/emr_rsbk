﻿using EMRRSBK.Models;
using EMRRSBK.Entities;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class ServiceController : Controller
    {
        #region LIST TABLE =======================================================================

        [HttpPost]
        public string ListRegistrasiPulang(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<VW_DataPasienReg> p = s.VW_DataPasienReg.Where(x => x.StatusBayar == "Sudah Bayar");

                    if (filter[24] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[22]))
                        {
                            p = p.Where("Tanggal >= @0", DateTime.Parse(filter[22]).AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[23]))
                        {
                            p = p.Where("Tanggal <= @0", DateTime.Parse(filter[23]));
                        }
                    }

                    if (filter[26] != null && filter[26] != "")
                    {
                        p = p.Where($"DokterID.Contains(@0)", filter[26]);
                    }

                    if (!string.IsNullOrEmpty(filter[0]))
                        p = p.Where($"NoAntri = @0", IFilter.F_short(filter[0]));
                    p = p.Where($"NoReg.Contains(@0)", filter[3]);
                    p = p.Where($"NRM.Contains(@0)", filter[6]);
                    p = p.Where($"NamaPasien.Contains(@0)", filter[7]);

                    var totalcount = p.Count();
                    var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, null, totalcount, pageIndex);
                    var datas = new List<RegistrasiPasienViewModel>();
                    foreach (var x in models.ToList())
                    {
                        var m = IConverter.Cast<RegistrasiPasienViewModel>(x);
                        m.Jam_View = x.Jam.ToString("HH\":\"mm");
                        m.Tanggal_View = x.Tanggal.ToString("yyyy-MM-dd");
                        m.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                        datas.Add(m);
                    }
                    result.Data = datas;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListRegistrasi(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var sectionID = "";
                    if (Request.Cookies["SectionID"] != null)
                    {
                        if (!string.IsNullOrEmpty(Request.Cookies["SectionID"].Value))
                        {
                            sectionID = Request.Cookies["SectionID"].Value;
                        }
                    }

                    IQueryable<VW_DataPasienReg> p = s.VW_DataPasienReg;


                    if (filter[24] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[22]))
                        {
                            p = p.Where("Tanggal >= @0", DateTime.Parse(filter[22]).AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[23]))
                        {
                            p = p.Where("Tanggal <= @0", DateTime.Parse(filter[23]));
                        }
                    }
                    //p = p.Where($"SectionID.Contains(@0)", Request.Cookies["SectionIDPelayanan"].Value);
                    if (filter[26] != null && filter[26] != "")
                    {
                        p = p.Where($"DokterID.Contains(@0)", filter[26]);
                    }
                    if (filter[25] == "1")
                        p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", false);
                    else if (filter[25] == "2")
                        p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", true);
                    else if (filter[25] == "3")
                        p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", true);
                    else if (filter[25] == "4")
                        p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", false);
                    if (!string.IsNullOrEmpty(filter[0]))
                        p = p.Where($"NoAntri = @0", IFilter.F_short(filter[0]));
                    p = p.Where($"NoReg.Contains(@0)", filter[3]);
                    p = p.Where($"NRM.Contains(@0)", filter[6]);
                    p = p.Where($"NamaPasien.Contains(@0)", filter[7]);

                    var totalcount = p.Count();
                    var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, null, totalcount, pageIndex);
                    var datas = new List<RegistrasiPasienViewModel>();
                    foreach (var x in models.ToList())
                    {
                        var m = IConverter.Cast<RegistrasiPasienViewModel>(x);
                        m.Jam_View = x.Jam.ToString("HH\":\"mm");
                        m.Tanggal_View = x.Tanggal.ToString("yyyy-MM-dd");
                        m.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                        datas.Add(m);
                    }
                    result.Data = datas;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}