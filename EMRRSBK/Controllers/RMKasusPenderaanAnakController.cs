﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class RMKasusPenderaanAnakController : Controller
    {
        // GET: RMKasusPenderaanAnak
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    model.NoReg = m.NoReg;
                    model.SectionID = sectionid;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.RMKasusPenderaanAnak.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.RMKPanak = IConverter.Cast<RMKasusPenderaAnakViewModel>(item);
                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Dokter);
                            if (dokter != null)
                            {
                                model.RMKPanak.DokterNama = dokter.NamaDOkter;
                            }

                            var perawat = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Perawat);
                            if (perawat != null)
                            {
                                model.RMKPanak.PerawatNama = perawat.NamaDOkter;
                            }
                            model.RMKPanak.Tanggal = DateTime.Today;
                            model.RMKPanak.Jam = DateTime.Now;
                            model.RMKPanak.NRM = m.NRM;
                            model.RMKPanak.TanggalLahirAyah = DateTime.Today;
                            model.RMKPanak.TanggalLahirIbu = DateTime.Today;
                            model.RMKPanak.WaktuKejadianTanggal = DateTime.Today;
                            model.RMKPanak.WaktuKejadianJam = DateTime.Now;
                            model.RMKPanak.NamaLengkap = m.NamaPasien;
                            model.RMKPanak.JenisKelamin = m.JenisKelamin;
                            model.RMKPanak.TempatTglLahir = m.TempatLahir;
                            model.RMKPanak.TglLahir = m.TglLahir;
                            model.RMKPanak.Umur = m.UmurThn;
                        }
                        else
                        {
                            item = new RMKasusPenderaanAnak();
                            model.RMKPanak = IConverter.Cast<RMKasusPenderaAnakViewModel>(item);
                            model.RMKPanak.NamaLengkap = m.NamaPasien;
                            model.RMKPanak.JenisKelamin = m.JenisKelamin;
                            model.RMKPanak.TempatTglLahir = m.TempatLahir;
                            model.RMKPanak.TglLahir = m.TglLahir;
                            model.RMKPanak.Umur = m.UmurThn;
                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string RMKasusPenderaanAnak_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.RMKPanak.NoReg = noreg;
                            item.RMKPanak.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                            item.RMKPanak.NRM = m.NRM;
                            item.RMKPanak.Tanggal = DateTime.Now;
                            var model = eEMR.RMKasusPenderaanAnak.FirstOrDefault(x => x.SectionID == item.RMKPanak.SectionID && x.NoReg == item.NoReg);

                            if (model == null)
                            {
                                model = new RMKasusPenderaanAnak();
                                var o = IConverter.Cast<RMKasusPenderaanAnak>(item.RMKPanak);
                                eEMR.RMKasusPenderaanAnak.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<RMKasusPenderaanAnak>(item.RMKPanak);
                                eEMR.RMKasusPenderaanAnak.AddOrUpdate(model);
                            }

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDF(string noreg, string sectionid)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"RM_KasusPenderaanAnak.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"RM_KasusPenderaanAnak", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"RM_KasusPenderaanAnak;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}