﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class AsuhanGiziTerapiMonitoringEvaluasiController : Controller
    {
        // GET: AsuhanGiziTerapiMonitoringEvaluasi
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    model.NoReg = noreg;
                    model.NRM = m.NRM;
                    model.SectionID = sectionid;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.AsuhanGiziTerapiMonitoringEvaluasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.AsuhanGiziTrpEvlsi = IConverter.Cast<AsuhanGiziTerapiMonitoringEvaluasiViewModel>(item);
                            model.AsuhanGiziTrpEvlsi.NoReg = noreg;
                            model.AsuhanGiziTrpEvlsi.NRM = model.NRM;
                            model.AsuhanGiziTrpEvlsi.SectionID = sectionid;
                            model.AsuhanGiziTrpEvlsi.Report = 1;
                            var ahligizi = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.AhliGizi);
                            if (ahligizi != null)
                            {
                                model.AsuhanGiziTrpEvlsi.AhliGiziNama = ahligizi.NamaDOkter;
                            }

                        }
                        else
                        {
                            item = new AsuhanGiziTerapiMonitoringEvaluasi();
                            model.AsuhanGiziTrpEvlsi = IConverter.Cast<AsuhanGiziTerapiMonitoringEvaluasiViewModel>(item);
                            model.AsuhanGiziTrpEvlsi.NoReg = noreg;
                            model.AsuhanGiziTrpEvlsi.NRM = model.NRM;
                            model.AsuhanGiziTrpEvlsi.SectionID = sectionid;
                            model.AsuhanGiziTrpEvlsi.Tanggal = DateTime.Today;
                            model.AsuhanGiziTrpEvlsi.Jam = DateTime.Now;
                        }

                        #region ===Detail Vital Sign
                        var ListVital = eEMR.AsuhanGiziTerapiMonitoringEvaluasi_Detail.Where(x => x.SectionID == sectionid && x.NoReg == noreg).ToList();
                        if (ListVital != null)
                        {
                            model.AsuhanGiziTrpEvlsi.Detail_List = new ListDetail<AsuhanGiziTerapiMonitoringEvaluasiDetailModelDetail>();

                            foreach (var x in ListVital)
                            {
                                var y = IConverter.Cast<AsuhanGiziTerapiMonitoringEvaluasiDetailModelDetail>(x);
                                model.AsuhanGiziTrpEvlsi.Detail_List.Add(false, y);
                            }
                        }
                        #endregion
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("AsuhanGiziTerapiMonitoringEvaluasi")]
        public string AsuhanGiziTerapiMonitoringEvaluasi_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.AsuhanGiziTrpEvlsi.NoReg = item.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.AsuhanGiziTrpEvlsi.SectionID = sectionid;
                            item.AsuhanGiziTrpEvlsi.NRM = item.NRM;
                            var model = eEMR.AsuhanGiziTerapiMonitoringEvaluasi.FirstOrDefault(x => x.SectionID == sectionid && x.NoReg == item.AsuhanGiziTrpEvlsi.NoReg);

                            if (model == null)
                            {
                                model = new AsuhanGiziTerapiMonitoringEvaluasi();
                                var o = IConverter.Cast<AsuhanGiziTerapiMonitoringEvaluasi>(item.AsuhanGiziTrpEvlsi);
                                eEMR.AsuhanGiziTerapiMonitoringEvaluasi.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<AsuhanGiziTerapiMonitoringEvaluasi>(item.AsuhanGiziTrpEvlsi);
                                eEMR.AsuhanGiziTerapiMonitoringEvaluasi.AddOrUpdate(model);
                            }

                            #region ===== Detail Vital

                            if (item.AsuhanGiziTrpEvlsi.Detail_List == null) item.AsuhanGiziTrpEvlsi.Detail_List = new ListDetail<AsuhanGiziTerapiMonitoringEvaluasiDetailModelDetail>();
                            item.AsuhanGiziTrpEvlsi.Detail_List.RemoveAll(x => x.Remove);
                            foreach (var x in item.AsuhanGiziTrpEvlsi.Detail_List)
                            {
                                x.Model.SectionID = sectionid;
                                x.Model.NoReg = item.NoReg;
                                x.Model.NRM = item.NRM;
                                //x.Model.Tanggal = DateTime.Today;
                                //x.Model.Jam = DateTime.Now;

                            }
                            var new_List = item.AsuhanGiziTrpEvlsi.Detail_List;
                            var real_List = eEMR.AsuhanGiziTerapiMonitoringEvaluasi_Detail.Where(x => x.SectionID == item.AsuhanGiziTrpEvlsi.SectionID && x.NoReg == item.AsuhanGiziTrpEvlsi.NoReg).ToList();
                            foreach (var x in real_List)
                            {
                                var z = new_List.FirstOrDefault(y => y.Model.No == x.No);
                                if (z == null)
                                {
                                    eEMR.AsuhanGiziTerapiMonitoringEvaluasi_Detail.Remove(x);
                                }
                            }
                            foreach (var x in new_List)
                            {
                                var _m = real_List.FirstOrDefault(y => y.No == x.Model.No);

                                if (_m == null)
                                {
                                    _m = new AsuhanGiziTerapiMonitoringEvaluasi_Detail();
                                    eEMR.AsuhanGiziTerapiMonitoringEvaluasi_Detail.Add(IConverter.Cast<AsuhanGiziTerapiMonitoringEvaluasi_Detail>(x.Model));
                                }

                                else
                                {
                                    _m = IConverter.Cast<AsuhanGiziTerapiMonitoringEvaluasi_Detail>(x.Model);
                                    eEMR.AsuhanGiziTerapiMonitoringEvaluasi_Detail.AddOrUpdate(_m);
                                }
                            }

                            #endregion
                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFCatatanAnestesiSedasi(string noreg, string sectionid)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_CatatanAnastesiSedasi.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand("Rpt_CatatanAnastesiSedasi", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables["Rpt_CatatanAnastesiSedasi;1"].SetDataSource(ds[i].Tables[0]);

                i++;
                cmd.Add(new SqlCommand($"Rpt_CatatanAnastesiSedasi_InfusPerifer", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_CatatanAnastesiSedasi_InfusPerifer;1"].SetDataSource(ds[i].Tables[0]);

                i++;
                cmd.Add(new SqlCommand($"Rpt_CatatanAnastesiSedasi_Obat", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_CatatanAnastesiSedasi_Obat;1"].SetDataSource(ds[i].Tables[0]);

                i++;
                cmd.Add(new SqlCommand($"Rpt_CatatanAnastesiSedasi_Pemantauan", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_CatatanAnastesiSedasi_Pemantauan;1"].SetDataSource(ds[i].Tables[0]);

                i++;
                cmd.Add(new SqlCommand($"Rpt_CatatanAnastesiSedasi_VitalSign", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_CatatanAnastesiSedasi_VitalSign;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}