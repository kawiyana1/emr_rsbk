﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class EMRIlmuPenyakitAnakController : Controller
    {
        // GET: EMRIlmuPenyakitAnak
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string noreg, string nrm, int view = 0)
        {
            var model = new EMRIlmuPenyakitAnakViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var dokumen = s.IlmuPenyakitAnak.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRIlmuPenyakitAnakViewModel>(dokumen);
                        var dktpolimata = sim.mDokter.FirstOrDefault(x => x.DokterID == model.DokterBPJP);
                        if (dktpolimata != null)
                        {
                            model.DokterBPJPNama = dktpolimata.NamaDOkter;
                        }

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                        model.NRM = nrm;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                    }

                    var dokumen_temp = s.trTemplate.Where(e => e.DokumenID == "Poliklinik Anak").ToList();

                    ViewBag.DokumenID = "Poliklinik Anak";
                    ViewBag.NamaDokumen = "Poliklinik Anak";
                    model.ListTemplate = new List<SelectItemListPenyakitAnak>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListPenyakitAnak()
                        {
                            Value = list.NoReg,
                            Text = list.NamaTemplate
                        });
                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string noreg, string copy)   
        {
            var model = new EMRIlmuPenyakitAnakViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var dokumen = s.IlmuPenyakitAnak.FirstOrDefault(x => x.NoReg == copy);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRIlmuPenyakitAnakViewModel>(dokumen);
                        model.NoReg = noreg;

                        var dpjp = sim.mDokter.FirstOrDefault(e => e.DokterID == model.DokterBPJP);
                        if (dpjp != null) model.DokterBPJPNama = dpjp.NamaDOkter;

                    }
                    else
                    {
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                    }
                    model.MODEVIEW = 0;

                    var dokumen_temp = s.trTemplate.Where(e => e.DokumenID == "Poliklinik Anak").ToList();

                    ViewBag.DokumenID = "Poliklinik Anak";
                    ViewBag.NamaDokumen = "Poliklinik Anak";
                    model.ListTemplate = new List<SelectItemListPenyakitAnak>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemListPenyakitAnak()
                        {
                            Value = list.NoReg,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {

            if (ModelState.IsValid)
            {
                var req = Request.Form[""];
            }
            try
                {
                var item = new EMRIlmuPenyakitAnakViewModel();

                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var model = s.IlmuPenyakitAnak.FirstOrDefault(x => x.NoReg == item.NoReg);
                    var activity = "";
                    var dokumenid = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<IlmuPenyakitAnak>(item);
                        s.IlmuPenyakitAnak.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoReg == item.NoReg);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Create Assesmen Anak";
                    }
                    else
                    {
                        model = IConverter.Cast<IlmuPenyakitAnak>(item);
                        s.IlmuPenyakitAnak.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoReg == item.NoReg);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Update Assesmen Anak";
                    }

                    if (item.save_template == true && item.nama_template != null)
                    {
                        var temp = new trTemplate();
                        temp.NoReg = item.NoReg;
                        temp.NamaTemplate = item.nama_template;
                        temp.DokterID = item.DokterBPJP;
                        temp.DokumenID = "Poliklinik Anak";
                        temp.SectionID = sectionid;
                        s.trTemplate.Add(temp);
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoReg}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}