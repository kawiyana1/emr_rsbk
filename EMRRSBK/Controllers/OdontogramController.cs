﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class OdontogramController : Controller
    {
        // GET: Odontogram
        public ActionResult Index(string noreg, int nomor = 0)
        {
            var item = new OdontogramViewModel();
            item.Reg = new RegistrasiPasienViewModel();
            var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
            item.Penyakit = new List<PenyakitViewModel>();
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    if (m != null)
                    {
                        item.Reg = IConverter.Cast<RegistrasiPasienViewModel>(m);
                        item.Reg.TglReg_View = item.Reg.TglReg.ToString("dd/MM/yyyy");
                        item.Reg.Tanggal_View = item.Reg.Tanggal.ToString("dd/MM/yyyy");
                        item.Reg.Jam_View = item.Reg.Jam.ToString("HH\":\"mm");
                        item.Reg.NRM = m.NRM;
                        item.Reg.JamMasuk = m.TglReg.ToString("dd/MM/yyyy");
                        item.Reg.Nomor = nomor;
                        item.Reg.PenanggungNama = m.PenanggungNama;
                        item.Reg.PenanggungPhone = m.PenanggungPhone;
                        item.Reg.Agama = m.Agama;
                        item.Reg.TempatLahir = m.TempatLahir;
                        item.Reg.Nama_Asuransi = m.JenisKerjasama;
                        item.Reg.StatusBayar = m.StatusBayar;

                    }
                }

                using (var s = new EMR_Entities())
                {
                    #region === Get Penyakit
                    var m = s.mOdontogram.ToList();
                    item.Penyakit = m.ConvertAll(x => IConverter.Cast<PenyakitViewModel>(x));
                    #endregion

                    #region === Detail Table
                    item.Detail_List = new ListDetail<OdontogramDetailViewModel>();
                    var d_odontopenyakit = s.trOdontogramPenyakit.Where(x => x.NoReg == noreg && x.SectionID == sectionid).ToList();
                    if (d_odontopenyakit != null)
                    {
                        foreach (var x in d_odontopenyakit)
                        {
                            var y = IConverter.Cast<OdontogramDetailViewModel>(x);
                            var penyakit = s.mOdontogram.FirstOrDefault(xx => xx.Id == y.Id_Penyakit);
                            if (penyakit != null) {
                                y.NamaPenyakit = penyakit.NamaPenyakit;
                            }
                            item.Detail_List.Add(false, y);
                        }
                    }
                    #endregion

                    #region === Get Html 
                    var htmlodon = s.trOdontogram.Where(x => x.NoReg == noreg).FirstOrDefault();
                    if (htmlodon != null) { 
                        item.OdontogramHtml = htmlodon.OdontogramHtml;
                    }
                    #endregion
                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            
           
            return View(item);
        }


        [HttpPost]
        [ValidateInput(false)]
        public string InsertOdontogram(string id, string noreg, string nrm,  List<OdontogramDetailViewModel> Detail_List)
        {
            try
            {
                var item = new OdontogramViewModel();
                TryUpdateModel(item);
                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new EMR_Entities())
                    {
                        var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                        var htmlToImageConv = new NReco.ImageGenerator.HtmlToImageConverter();
                        var jpegBytes = htmlToImageConv.GenerateImage(id, "Jpeg");
                        var model = s.trOdontogram.FirstOrDefault(x => x.NRM == nrm);
                        var tr = new trOdontogram();

                        if (model == null)
                        {
                            var m = new trOdontogram()
                            {
                                NoReg = noreg,
                                NRM = nrm,
                                Odontogram = jpegBytes,
                                SectionID = sectionid,
                                OdontogramHtml = id
                            };
                            s.trOdontogram.Add(m);
                        }
                        else
                        {
                            var up = new trOdontogram()
                            {
                                NoReg = noreg,
                                NRM = nrm,
                                Odontogram = jpegBytes,
                                SectionID = sectionid,
                                OdontogramHtml = id
                            };
                            s.trOdontogram.AddOrUpdate(up);
                        }

                        #region ===== DETAIL
                        item.Detail_List = new ListDetail<OdontogramDetailViewModel>();
                        item.Detail_List.RemoveAll(x => x.Remove);
                        foreach (var x in Detail_List)
                        {
                            item.Detail_List.Add(false, new OdontogramDetailViewModel
                            {
                                NoReg = noreg,
                                SectionID = sectionid,
                                NRM = nrm,
                                Id_Penyakit = x.Id_Penyakit,
                                GigiNomor = x.GigiNomor,
                                
                            });
                        }
                        var new_List = item.Detail_List;
                        var real_List = s.trOdontogramPenyakit.Where(x => x.NRM == nrm).ToList();
                        foreach (var x in real_List)
                        {
                            var z = new_List.FirstOrDefault(y => y.Model.Id_Penyakit == x.Id_Penyakit);
                            if (z == null)
                            {
                                s.trOdontogramPenyakit.Remove(x);
                            }
                        }
                        foreach (var x in new_List)
                        {
                            var _m = real_List.FirstOrDefault(y => y.Id_Penyakit == x.Model.Id_Penyakit);
                            if (_m == null)
                            {
                                var _model = IConverter.Cast<trOdontogramPenyakit>(x.Model);
                                _model.NoReg = noreg;
                                _model.SectionID = sectionid;
                                s.trOdontogramPenyakit.Add(_model);
                            }
                            else
                            {
                                _m = IConverter.Cast<trOdontogramPenyakit>(x.Model);
                                _m.GigiNomor = x.Model.GigiNomor;
                                s.trOdontogramPenyakit.AddOrUpdate(_m);
                            }
                        }
                        #endregion
                        result = new ResultSS(s.SaveChanges());
                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"trOdontogram Create {tr.NoReg} {tr.SectionID}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }


        #region ==== G E T  D A T A  P E N Y A K I T
        [HttpGet]
        public string GetDataPenyakit(int id)
        {
            mOdontogram m;
            using (var s = new EMR_Entities())
            {
                var item = new OdontogramViewModel();
                m = s.mOdontogram.FirstOrDefault(x => x.Id == id);
                if (m == null) throw new Exception("Data tidak ditemukan");
            }
            var result = m;

            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result,
            });
        }
        #endregion

        #region ==== G E T  D A T A  P E N Y A K I T  P O S I S I
        [HttpGet]
        public string GetDataPenyakitPosisi(int id)
        {
            mOdontogramLokasi m;
            using (var s = new EMR_Entities())
            {
                m = s.mOdontogramLokasi.FirstOrDefault(x => x.Id == id);
                if (m == null) throw new Exception("Data tidak ditemukan");
            }
            var result = m;
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result,
            });
        }
        #endregion

    }

}