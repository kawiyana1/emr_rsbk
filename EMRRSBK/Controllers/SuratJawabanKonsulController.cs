﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class SuratJawabanKonsulController : Controller
    {
        // GET: SuratJawabanKonsul
        [HttpGet]
        [ActionName("Index")]
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new SuratJawabanKonsulViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var emr = new EMR_Entities())
                {
                    var sectionida = Request.Cookies["SectionIDPelayanan"].Value;
                    var identitas = sim.VW_IdentitasPasien.FirstOrDefault(x => x.NoReg == noreg);

                    model.NoReg = noreg;
                    model.NRM = identitas.NRM;
                    ViewBag.NoBukti = noreg;
                }

            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        public string ListSuratJawabanKonsultasi(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMR_Entities())
                    {
                        var noreg = filter[1];
                        IQueryable<SuratJawabanKonsultasi> p = s.SuratJawabanKonsultasi.Where(x => x.NoReg == noreg);
                        //p = p.Where($"NoRegistrasi.Contains(@0)", filter[2]);
                        //p = p.Where($"NRM.Contains(@0)", filter[1]);
                        //p = p.Where($"NoResep.Contains(@0)", filter[4]);
                        var totalcount = p.Count();
                        var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.DESC ? "ASC" : "DESC")}").Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                        result = new ResultSS(models.Length, null, totalcount, pageIndex);
                        var datas = new List<SuratJawabanKonsulViewModel>();
                        foreach (var x in models.ToList())
                        {
                            var m = IConverter.Cast<SuratJawabanKonsulViewModel>(x);
                            var dokterjawab = sim.mDokter.FirstOrDefault(z => z.DokterID == x.TS_Dr_JawabanKonsul);
                            if (dokterjawab != null) {
                                m.TS_Dr_JawabanKonsulNama = dokterjawab.NamaDOkter;
                            }
                            var dokterkonsul = sim.mDokter.FirstOrDefault(z => z.DokterID == x.Petugas_Jawaban);
                            if (dokterkonsul != null)
                            {
                                m.Petugas_Jawaban = dokterkonsul.NamaDOkter;
                            }
                            var petugasnama = sim.mDokter.FirstOrDefault(z => z.DokterID == x.Petugas);
                            if (petugasnama != null)
                            {
                                m.PetugasNama = petugasnama.NamaDOkter;
                            }
                            var dokterkonsul1 = sim.mDokter.FirstOrDefault(z => z.DokterID == x.TS_Dr_Konsul);
                            if (dokterkonsul1 != null)
                            {
                                m.TS_Dr_KonsulNama = dokterkonsul1.NamaDOkter;
                            }
                            datas.Add(m);
                        }
                        result.Data = datas;
                    }
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public string editInforment(string id, string noreg)
        {
            try
            {
                var model = new SuratJawabanKonsulViewModel();
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMR_Entities())
                    {
                        int idnya = int.Parse(id);
                        var dokumen = s.SuratJawabanKonsultasi.FirstOrDefault(x => x.No == idnya && x.NoReg == noreg);
                        model = IConverter.Cast<SuratJawabanKonsulViewModel>(dokumen);

                        var dokpelaksana = sim.mDokter.FirstOrDefault(x => x.DokterID == model.TS_Dr_JawabanKonsul);
                        if (dokpelaksana != null)
                        {
                            model.TS_Dr_JawabanKonsulNama = dokpelaksana.NamaDOkter;
                        }

                        var pemberi = sim.mDokter.FirstOrDefault(x => x.DokterID == model.Petugas);
                        if (pemberi != null)
                        {
                            model.PetugasNama = pemberi.NamaDOkter;
                        }

                        var menerangkan = sim.mDokter.FirstOrDefault(x => x.DokterID == model.Petugas_Jawaban);
                        if (menerangkan != null)
                        {
                            model.Petugas_JawabanNama = menerangkan.NamaDOkter;
                        }

                        var tskonsul = sim.mDokter.FirstOrDefault(x => x.DokterID == model.TS_Dr_Konsul);
                        if (tskonsul != null)
                        {
                            model.TS_Dr_KonsulNama = tskonsul.NamaDOkter;
                        }
                    }
                }

                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = model,
                    Message = ""

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost, ValidateInput(false)]
        [ActionName("SecondCreate")]
        public string SecondCreate(SuratJawabanKonsulViewModel item, string img, string imgttd)
        {
            try
            {
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    //if (item.DPJP == null) return JsonHelper.JsonMsgError("Dokter DPJP tidak boleh kosong.");
                    //if (item.DPJP == null) throw new Exception("Dokter tidak boleh kosong.");

                    var model = s.SuratJawabanKonsultasi.FirstOrDefault(x => x.NoReg == item.NoReg);
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var activity = "";
                    if (item._METHOD == "CREATE")
                    {

                        var o = IConverter.Cast<SuratJawabanKonsultasi>(item);
                        o.NoReg = item.NoReg;
                        o.NRM = item.NRM;
                        o.SectionID = sectionid;
                        s.SuratJawabanKonsultasi.Add(o);
                    }
                    else if (item._METHOD == "UPDATE")
                    {
                        model = IConverter.Cast<SuratJawabanKonsultasi>(item);
                        model.No = item.No;
                        model.NoReg = item.NoReg;
                        model.NRM = item.NRM;
                        model.SectionID = sectionid;
                        s.SuratJawabanKonsultasi.AddOrUpdate(model);

                        //if (item.templateName != null)
                        //{
                        //    var check_temp = s.trDokumenTemplate.FirstOrDefault(x => x.NamaTemplate == item.templateName);
                        //    if (check_temp == null)
                        //    {
                        //        var temp = new trDokumenTemplate();
                        //        temp.NoBukti = item.NoBukti;
                        //        temp.NamaTemplate = item.templateName;
                        //        temp.DokterID = item.DPJP;
                        //        temp.SectionID = Request.Cookies["EMRSectionIDPelayanan"].Value;
                        //        temp.DokumenID = "CPPT";
                        //        s.trDokumenTemplate.Add(temp);
                        //    }
                        //}
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoReg}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string Batal(int id, string noreg)
        {
            try
            {
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var m = s.SuratJawabanKonsultasi.FirstOrDefault(x => x.No == id && x.NoReg == noreg);
                    s.SuratJawabanKonsultasi.Remove(m);
                    result = new ResultSS(s.SaveChanges());

                }
                return JsonHelper.JsonMsgDelete(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}