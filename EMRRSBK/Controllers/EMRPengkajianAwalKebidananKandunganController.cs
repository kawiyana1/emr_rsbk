﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class EMRPengkajianAwalKebidananKandunganController : Controller
    {
        // GET: EMRPengkajianAwalKebidananKandungan
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string noreg, string nrm, int view = 0)
        {
            var model = new EMRPengkajianAwalKebidananKandunganViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var dokumen = s.PengkajianAwalKebidananKandungan.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRPengkajianAwalKebidananKandunganViewModel>(dokumen);
                        var dokterpolibidan = sim.mDokter.FirstOrDefault(x => x.DokterID == model.Dokter);
                        if (dokterpolibidan != null)
                        {
                            model.DokterNama = dokterpolibidan.NamaDOkter;
                        }
                        var dpjp = sim.mDokter.FirstOrDefault(x => x.DokterID == model.DPJP);
                        if (dpjp != null)
                        {
                            model.DPJPNama = dpjp.NamaDOkter;
                        }
                        var bidan = sim.mDokter.FirstOrDefault(x => x.DokterID == model.Bidan);
                        if (bidan != null)
                        {
                            model.BidanNama = bidan.NamaDOkter;
                        }
                        var pemeriksa = sim.mDokter.FirstOrDefault(x => x.DokterID == model.Pemeriksaan_Oleh);
                        if (pemeriksa != null)
                        {
                            model.Pemeriksaan_OlehNama = pemeriksa.NamaDOkter;
                        }

                        model.MODEVIEW = view;
                    }
                    else
                    {
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                        model.NRM = nrm;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                    }

                    var dokumen_temp = s.trTemplate.Where(e => e.DokumenID == "Poliklinik Kebidanan").ToList();
                    ViewBag.DokumenID = "Poliklinik Kebidanan";
                    ViewBag.NamaDokumen = "Poliklinik Kebidanan";
                    model.ListTemplate = new List<SelectItemLisPKebidananDanKandungan>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemLisPKebidananDanKandungan()
                        {
                            Value = list.NoReg,
                            Text = list.NamaTemplate
                        });
                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string noreg, string copy, string nrm)
        {
            var model = new EMRPengkajianAwalKebidananKandunganViewModel();

            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var dokumen = s.PengkajianAwalKebidananKandungan.FirstOrDefault(x => x.NoReg == copy);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<EMRPengkajianAwalKebidananKandunganViewModel>(dokumen);
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                        model.NRM = nrm;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;

                        var dokterpolibidan = sim.mDokter.FirstOrDefault(x => x.DokterID == model.Dokter);
                        if (dokterpolibidan != null)
                        {
                            model.DokterNama = dokterpolibidan.NamaDOkter;
                        }
                        var dpjp = sim.mDokter.FirstOrDefault(x => x.DokterID == model.DPJP);
                        if (dpjp != null)
                        {
                            model.DPJPNama = dpjp.NamaDOkter;
                        }
                        var bidan = sim.mDokter.FirstOrDefault(x => x.DokterID == model.Bidan);
                        if (bidan != null)
                        {
                            model.BidanNama = bidan.NamaDOkter;
                        }
                        var pemeriksa = sim.mDokter.FirstOrDefault(x => x.DokterID == model.Pemeriksaan_Oleh);
                        if (pemeriksa != null)
                        {
                            model.Pemeriksaan_OlehNama = pemeriksa.NamaDOkter;
                        }

                    }
                    else
                    {
                        model.NoReg = noreg;
                        model.SectionID = sectionid;
                        model.NRM = nrm;
                        model.Tanggal = DateTime.Today;
                        model.Jam = DateTime.Now;
                    }
                    model.MODEVIEW = 0;
                    var dokumen_temp = s.trTemplate.Where(e => e.DokumenID == "Poliklinik Kebidanan").ToList();
                    ViewBag.DokumenID = "Poliklinik Kebidanan";
                    ViewBag.NamaDokumen = "Poliklinik Kebidanan";
                    model.ListTemplate = new List<SelectItemLisPKebidananDanKandungan>();
                    foreach (var list in dokumen_temp)
                    {
                        model.ListTemplate.Add(new SelectItemLisPKebidananDanKandungan()
                        {
                            Value = list.NoReg,
                            Text = list.NamaTemplate
                        });
                    }

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {

            if (ModelState.IsValid)
            {
                var req = Request.Form[""];
            }
            try
            {
                var item = new EMRPengkajianAwalKebidananKandunganViewModel();

                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var model = s.PengkajianAwalKebidananKandungan.FirstOrDefault(x => x.NoReg == item.NoReg);
                    var activity = "";
                    var dokumenid = "";
                    if (model == null)
                    {
                        var o = IConverter.Cast<PengkajianAwalKebidananKandungan>(item);
                        s.PengkajianAwalKebidananKandungan.Add(o);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoReg == item.NoReg);
                        header.Simpan = true;
                        header.TanggalCreate = DateTime.Now;
                        header.CretaedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Created = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Create Pengkajian Kebidanan dan kandaungan";
                    }
                    else
                    {
                        model = IConverter.Cast<PengkajianAwalKebidananKandungan>(item);
                        s.PengkajianAwalKebidananKandungan.AddOrUpdate(model);

                        var header = s.trDokumenAssesmenPasien.FirstOrDefault(x => x.NoReg == item.NoReg);
                        header.Simpan = true;
                        header.TanggalUpdate = DateTime.Now;
                        header.UpdatedBy = User.Identity.GetUserName();
                        header.UserIdWeb_Updated = User.Identity.GetUserId();
                        dokumenid = header.DokumenID;

                        activity = "Update Pengkajian Kebidanan dan kandaungan";
                    }

                    if (item.save_template == true && item.nama_template != null)
                    {
                        var temp = new trTemplate();
                        temp.NoReg = item.NoReg;
                        temp.NamaTemplate = item.nama_template;
                        temp.DokterID = item.DPJP;
                        temp.DokumenID = "Poliklinik Kebidanan";
                        temp.SectionID = sectionid;
                        s.trTemplate.Add(temp);
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoReg}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}