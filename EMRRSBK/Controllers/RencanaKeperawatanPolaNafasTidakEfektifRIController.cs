﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class RencanaKeperawatanPolaNafasTidakEfektifRIController : Controller
    {
        // GET: RencanaKeperawatanPolaNafasTidakEfektifRI
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.RencanaKeperawatanPolaNafasTidakEfektif.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.RncanaKprwtPolaNfsTdkEfektf = IConverter.Cast<RencanaKeperawatanPolaNafasTidakEfektifViewModel>(item);
                            var paraf = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.NamaTerang);
                            if (paraf != null)
                            {
                                model.RncanaKprwtPolaNfsTdkEfektf.NamaTerangNama = paraf.NamaDOkter;
                            }
                            model.RncanaKprwtPolaNfsTdkEfektf.NoReg = noreg;
                            model.RncanaKprwtPolaNfsTdkEfektf.NRM = model.NRM;
                            model.RncanaKprwtPolaNfsTdkEfektf.SectionID = sectionid;
                            model.RncanaKprwtPolaNfsTdkEfektf.Report = 1;
                            model.RncanaKprwtPolaNfsTdkEfektf.NamaPasien = m.NamaPasien;
                            model.RncanaKprwtPolaNfsTdkEfektf.JenisKelamin = m.JenisKelamin;
                            model.RncanaKprwtPolaNfsTdkEfektf.TglLahir = m.TglLahir;
                        }
                        else
                        {
                            item = new RencanaKeperawatanPolaNafasTidakEfektif();
                            model.RncanaKprwtPolaNfsTdkEfektf = IConverter.Cast<RencanaKeperawatanPolaNafasTidakEfektifViewModel>(item);
                            model.RncanaKprwtPolaNfsTdkEfektf.Report = 0;
                            model.RncanaKprwtPolaNfsTdkEfektf.NamaPasien = m.NamaPasien;
                            model.RncanaKprwtPolaNfsTdkEfektf.JenisKelamin = m.JenisKelamin;
                            model.RncanaKprwtPolaNfsTdkEfektf.TglLahir = m.TglLahir;
                            model.RncanaKprwtPolaNfsTdkEfektf.NoReg = noreg;
                            model.RncanaKprwtPolaNfsTdkEfektf.NRM = model.NRM;
                            model.RncanaKprwtPolaNfsTdkEfektf.SectionID = sectionid;
                            model.RncanaKprwtPolaNfsTdkEfektf.Tanggal = DateTime.Today;
                            model.RncanaKprwtPolaNfsTdkEfektf.Jam = DateTime.Now;
                        }


                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string RencanaKeperawatanPolaNafasTidakEfektif_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.RncanaKprwtPolaNfsTdkEfektf.NoReg = item.RncanaKprwtPolaNfsTdkEfektf.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.RncanaKprwtPolaNfsTdkEfektf.SectionID = sectionid;
                            item.RncanaKprwtPolaNfsTdkEfektf.NRM = item.RncanaKprwtPolaNfsTdkEfektf.NRM;
                            var model = eEMR.RencanaKeperawatanPolaNafasTidakEfektif.FirstOrDefault(x => x.SectionID == sectionid && x.NoReg == item.RncanaKprwtPolaNfsTdkEfektf.NoReg);

                            if (model == null)
                            {
                                model = new RencanaKeperawatanPolaNafasTidakEfektif();
                                var o = IConverter.Cast<RencanaKeperawatanPolaNafasTidakEfektif>(item.RncanaKprwtPolaNfsTdkEfektf);
                                eEMR.RencanaKeperawatanPolaNafasTidakEfektif.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<RencanaKeperawatanPolaNafasTidakEfektif>(item.RncanaKprwtPolaNfsTdkEfektf);
                                eEMR.RencanaKeperawatanPolaNafasTidakEfektif.AddOrUpdate(model);
                            }


                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFRencanaKeperawatanPolaNafasTidakEfektif(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_RencanaKeperawatanPolaNafasTidakEfektif.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();
                var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                var i = 0;
                cmd.Add(new SqlCommand("Rpt_RencanaKeperawatanPolaNafasTidakEfektif", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables["Rpt_RencanaKeperawatanPolaNafasTidakEfektif;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                //cmd.Add(new SqlCommand($"Rpt_RencanaKeperawatanPolaNafasTidakEfektif_Detail", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables[$"Rpt_RencanaKeperawatanPolaNafasTidakEfektif_Detail;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}