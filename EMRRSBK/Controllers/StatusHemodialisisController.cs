﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;


namespace EMRRSBK.Models
{
    public class StatusHemodialisisController : Controller
    {
        // GET: StatusHemodialisis
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var sectionnama = Request.Cookies["SectionNamaPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.StatusHemodialisis.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.StatusHemo = IConverter.Cast<StatusHemodialisViewModel>(item);

                            model.StatusHemo.SectionID = sectionid; 
                            model.StatusHemo.NRM = model.NRM;
                            model.StatusHemo.NamaPasien = m.NamaPasien;
                            model.StatusHemo.NoReg = noreg;
                            model.StatusHemo.Alamat = m.Alamat;
                            model.StatusHemo.NoTelp = m.Phone;


                            model.StatusHemo.Report = 1;

                        }
                        else
                        {
                            item = new StatusHemodialisis();
                            model.StatusHemo = IConverter.Cast<StatusHemodialisViewModel>(item);
                            model.StatusHemo.SectionID = sectionid;
                            model.StatusHemo.NRM = model.NRM;
                            model.StatusHemo.NamaPasien = m.NamaPasien;
                            model.StatusHemo.NoReg = noreg;
                            model.StatusHemo.Alamat = m.Alamat;
                            model.StatusHemo.NoTelp = m.Phone;

                            model.StatusHemo.Report = 0;


                        }

                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string StatusHemodialisis_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == item.StatusHemo.NoReg);
                            item.StatusHemo.NoReg = item.StatusHemo.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.StatusHemo.SectionID = sectionid;
                            item.StatusHemo.NRM = item.StatusHemo.NRM;
                            var model = eEMR.StatusHemodialisis.FirstOrDefault(x => x.SectionID == item.StatusHemo.SectionID && x.NoReg == item.StatusHemo.NoReg);

                            if (model == null)
                            {
                                model = new StatusHemodialisis();
                                var o = IConverter.Cast<StatusHemodialisis>(item.StatusHemo);
                                eEMR.StatusHemodialisis.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<StatusHemodialisis>(item.StatusHemo);
                                eEMR.StatusHemodialisis.AddOrUpdate(model);
                            }



                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

    }
}