﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class ResumeMedisBPJSRawatJalanController : Controller
    {
        // GET: ResumeMedisBPJSRawatJalan
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    model.SectionID = sectionid;
                    model.NoReg = m.NoReg;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.ResumeMedisBPJSRJ.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.ResumeMedisBPJS = IConverter.Cast<ResumeMedisBPJSViewModel>(item);
                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.DokterPemeriksa);
                            if (dokter != null)
                            {
                                model.ResumeMedisBPJS.DokterPemeriksaNama = dokter.NamaDOkter;
                            }
                            var obat = eSIM.mBarang.FirstOrDefault(x => x.Kode_Barang == item.Obat);
                            if (obat != null)
                            {
                                model.ResumeMedisBPJS.ObatNama = obat.Nama_Barang;
                            }

                            var fingerdpjp = eEMR.ResumeMedisBPJSRJ.Where(x => x.DokterPemeriksa == model.ResumeMedisBPJS.DokterPemeriksa && x.TandaTanganDokterPemeriksa == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerdpjp != null)
                            {
                                model.ResumeMedisBPJS.SudahRegDokterPemeriksa = 1;
                            }
                            else
                            {
                                model.ResumeMedisBPJS.SudahRegDokterPemeriksa = 0;
                            }

                            var fingerpasien = eEMR.ResumeMedisBPJSRJ.Where(x => x.NRM == model.ResumeMedisBPJS.NRM && x.TandaTangan == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerpasien != null)
                            {
                                model.ResumeMedisBPJS.SudahRegPasien = 1;
                            }
                            else
                            {
                                model.ResumeMedisBPJS.SudahRegPasien = 0;
                            }

                            if (model.ResumeMedisBPJS.DokterPemeriksa != null)
                            {
                                ViewBag.UrlFingerResumeMedisBPJSDokterPemeriksa = GetEncodeUrlResumeMedisBPJS(model.ResumeMedisBPJS.DokterPemeriksa, model.ResumeMedisBPJS.NoReg, sectionid);
                            }

                            ViewBag.UrlFingerResumeMedisBPJSPasien = GetEncodeUrlResumeMedisBPJS(model.ResumeMedisBPJS.NRM, model.ResumeMedisBPJS.NoReg, sectionid);

                        }
                        else
                        {
                            item = new ResumeMedisBPJSRJ();
                            model.ResumeMedisBPJS = IConverter.Cast<ResumeMedisBPJSViewModel>(item);
                            ViewBag.UrlFingerResumeMedisBPJSDokterPemeriksa = GetEncodeUrlResumeMedisBPJS(model.ResumeMedisBPJS.DokterPemeriksa, model.ResumeMedisBPJS.NoReg, sectionid);
                            ViewBag.UrlFingerResumeMedisBPJSPasien = GetEncodeUrlResumeMedisBPJS(model.ResumeMedisBPJS.NRM, model.ResumeMedisBPJS.NoReg, sectionid);
                            model.ResumeMedisBPJS.Tanggal = DateTime.Now;
                            model.ResumeMedisBPJS.TglLahirDataPasien = m.TglLahir;
                            model.ResumeMedisBPJS.NamaDataPasien = m.NamaPasien;
                            model.ResumeMedisBPJS.TglLahirDataPasien = m.TglLahir;
                            model.ResumeMedisBPJS.AlamatDataPasien = m.Alamat;
                            model.ResumeMedisBPJS.NRMDataPasien = m.NRM;
                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string ResumeMedisBPJSRawatJalan_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.ResumeMedisBPJS.NoReg = noreg;
                            item.ResumeMedisBPJS.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                            item.ResumeMedisBPJS.NRM = m.NRM;
                            item.ResumeMedisBPJS.Tanggal = DateTime.Now;
                            var model = eEMR.ResumeMedisBPJSRJ.FirstOrDefault(x => x.SectionID == item.ResumeMedisBPJS.SectionID && x.NoReg == item.NoReg);

                            if (model == null)
                            {
                                model = new ResumeMedisBPJSRJ();
                                var o = IConverter.Cast<ResumeMedisBPJSRJ>(item.ResumeMedisBPJS);
                                eEMR.ResumeMedisBPJSRJ.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<ResumeMedisBPJSRJ>(item.ResumeMedisBPJS);
                                eEMR.ResumeMedisBPJSRJ.AddOrUpdate(model);
                            }

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFResumeMedisBPJSRawatJalan(string noreg, string sectionid)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_ResumeMedisBPJS.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"Rpt_ResumeMedisBPJSRJ", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_ResumeMedisBPJSRJ;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }

        public string GetEncodeUrlResumeMedisBPJS(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifResumeMedisBPJS/get_keyskdpjp";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }
    }
}