﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class AssesmentAwalPasienHemodialisisController : Controller
    {
        // GET: AssesmentAwalPasienHemodialisis
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    var n = eSIM.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var sectionnama = Request.Cookies["SectionNamaPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.AssesmentAwalPasienHemodialisis.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.AsmntAwlPasienHemodialisis = IConverter.Cast<AssesmentAwalPasienHemodialisisViewModel>(item);

                            model.AsmntAwlPasienHemodialisis.NamaPasien = m.NamaPasien;
                            model.AsmntAwlPasienHemodialisis.NoReg = noreg;
                            model.AsmntAwlPasienHemodialisis.NRM = model.NRM;
                            model.AsmntAwlPasienHemodialisis.Tanggal = DateTime.Today;
                            model.AsmntAwlPasienHemodialisis.Jam = DateTime.Now;
                            model.AsmntAwlPasienHemodialisis.SectionID = sectionid;
                            model.AsmntAwlPasienHemodialisis.Umur = m.UmurThn;
                            model.AsmntAwlPasienHemodialisis.Phone = n.Phone;

                            model.AsmntAwlPasienHemodialisis.Pekerjaan = n.Pekerjaan;
                            model.AsmntAwlPasienHemodialisis.Agama = m.Agama;
                            model.AsmntAwlPasienHemodialisis.PenanggungNama = n.PenanggungNama;

                            model.AsmntAwlPasienHemodialisis.Report = 1;

                        }
                        else
                        {
                            item = new AssesmentAwalPasienHemodialisis();
                            model.AsmntAwlPasienHemodialisis = IConverter.Cast<AssesmentAwalPasienHemodialisisViewModel>(item);
                            model.AsmntAwlPasienHemodialisis.NamaPasien = m.NamaPasien;
                            model.AsmntAwlPasienHemodialisis.NoReg = noreg;
                            model.AsmntAwlPasienHemodialisis.Umur = m.UmurThn;
                            model.AsmntAwlPasienHemodialisis.NRM = m.NRM;
                            model.AsmntAwlPasienHemodialisis.SectionID = sectionid;
                            model.AsmntAwlPasienHemodialisis.Tanggal = DateTime.Today;
                            model.AsmntAwlPasienHemodialisis.Jam = DateTime.Now;


                            model.AsmntAwlPasienHemodialisis.Pekerjaan = n.Pekerjaan;
                            model.AsmntAwlPasienHemodialisis.Agama = m.Agama;
                            model.AsmntAwlPasienHemodialisis.PenanggungNama = n.PenanggungNama;

                            model.AsmntAwlPasienHemodialisis.Report = 0;
                           

                        }

                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }


        [HttpPost]
        [ActionName("Create")]
        public string AssesmentAwalPasienHemodialisis_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == item.AsmntAwlPasienHemodialisis.NoReg);
                            item.AsmntAwlPasienHemodialisis.NoReg = item.AsmntAwlPasienHemodialisis.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.AsmntAwlPasienHemodialisis.SectionID = sectionid;
                            item.AsmntAwlPasienHemodialisis.NRM = item.AsmntAwlPasienHemodialisis.NRM;
                            var model = eEMR.AssesmentAwalPasienHemodialisis.FirstOrDefault(x => x.SectionID == item.AsmntAwlPasienHemodialisis.SectionID && x.NoReg == item.AsmntAwlPasienHemodialisis.NoReg);

                            if (model == null)
                            {
                                model = new AssesmentAwalPasienHemodialisis();
                                var o = IConverter.Cast<AssesmentAwalPasienHemodialisis>(item.AsmntAwlPasienHemodialisis);
                                eEMR.AssesmentAwalPasienHemodialisis.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<AssesmentAwalPasienHemodialisis>(item.AsmntAwlPasienHemodialisis);
                                eEMR.AssesmentAwalPasienHemodialisis.AddOrUpdate(model);
                            }



                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}