﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class EMRAsesmenUlangNyeriController : Controller
    {
        // GET: EMRAsesmenUlangNyeri
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string id, string nrm, int view = 0)
        {
            var model = new EMRHeaderViewDetail();

            using (var sim = new SIM_Entities())
            {
                var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                var m = sim.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == id);
                model.NRM = m.NRM;
                model.NoReg = id;
                model.SectionID = sectionid;
                using (var s = new EMR_Entities())
                {
                    var dokumen = s.AsesmenUlangNyeri.Where(x => x.NoReg == id && x.SectionID == sectionid).ToList();
                    if (dokumen != null)
                    {
                        model.RIAssUlangNyeri_List = new ListDetail<EMRAsesmenUlangNyeriViewModel>();
                        var detail_1 = s.AsesmenUlangNyeri.Where(x => x.NoReg == id && x.SectionID == sectionid).ToList();
                        foreach (var x in detail_1)
                        {
                            var y = IConverter.Cast<EMRAsesmenUlangNyeriViewModel>(x);
                            var dokter = sim.mDokter.FirstOrDefault(xx => xx.DokterID == x.Bidan);
                            if (dokter != null)
                            {
                                y.BidanNama = dokter.NamaDOkter;
                            }
                            model.RIAssUlangNyeri_List.Add(false, y);
                        }
                        model.MODEVIEW = view;
                        //model.Report = 1;
                    }
                    model.NoReg = id;
                    model.NRM = nrm;

                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new EMRHeaderViewDetail();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var model = s.AsesmenUlangNyeri.FirstOrDefault(x => x.NoReg == item.NoReg);
                    var activity = "";

                    if (item.RIAssUlangNyeri_List == null) item.RIAssUlangNyeri_List = new ListDetail<EMRAsesmenUlangNyeriViewModel>();
                    item.RIAssUlangNyeri_List.RemoveAll(x => x.Remove);
                    foreach (var x in item.RIAssUlangNyeri_List)
                    {
                        x.Model.NoReg = item.NoReg;
                        x.Model.NRM = item.NRM;
                        x.Model.SectionID = sectionid;
                    }
                    var new_list = item.RIAssUlangNyeri_List;
                    var real_list = s.AsesmenUlangNyeri.Where(x => x.NoReg == item.NoReg).ToList();
                    foreach (var x in real_list)
                    {
                        var m = new_list.FirstOrDefault(y => y.Model.No == x.No);
                        if (m == null) s.AsesmenUlangNyeri.Remove(x);
                    }
                    foreach (var x in new_list)
                    {


                        var _m = real_list.FirstOrDefault(y => y.No == x.Model.No);
                        if (_m == null) s.AsesmenUlangNyeri.Add(IConverter.Cast<AsesmenUlangNyeri>(x.Model));
                        else
                        {
                            _m.NoReg = x.Model.NoReg;
                            _m.No = x.Model.No;
                            _m.Tanggal = x.Model.Tanggal;
                            _m.Jam = x.Model.Jam;
                            _m.SectionID = sectionid;
                            _m.SkorNyeri = x.Model.SkorNyeri;
                            _m.SkorSedasi = x.Model.SkorSedasi;
                            _m.Obat = x.Model.Obat;
                            _m.Dosis = x.Model.Dosis;
                            _m.Rute = x.Model.Rute;
                            _m.Farmakologi = x.Model.Farmakologi;
                            _m.Bidan = x.Model.Bidan;
                            _m.KajianUlang = x.Model.KajianUlang;
                            _m.KajianUlangKet = x.Model.KajianUlangKet;
                        }
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoReg}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}