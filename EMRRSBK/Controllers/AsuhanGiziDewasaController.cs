﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;


namespace EMRRSBK.Controllers
{
    public class AsuhanGiziDewasaController : Controller
    {
        // GET: AsuhanGiziDewasa
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try

            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var sectionnama = Request.Cookies["SectionNamaPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.AsuhanGiziDewasa.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.AsuhanGiziDewasa = IConverter.Cast<AsuhanGiziDewasaViewModel>(item);

                            model.AsuhanGiziDewasa.SectionID = sectionid;
                            model.AsuhanGiziDewasa.NRM = model.NRM;
                            model.AsuhanGiziDewasa.NoReg = noreg;
                            model.AsuhanGiziDewasa.Report = 1;
                            model.AsuhanGiziDewasa.Ruangan = sectionnama;

                            var ahligizi = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.AhliGizi);
                            if (ahligizi != null)
                            {
                                model.AsuhanGiziDewasa.AhliGiziNama = ahligizi.NamaDOkter;
                            }

                        }
                        else
                        {
                            item = new AsuhanGiziDewasa();
                            model.AsuhanGiziDewasa = IConverter.Cast<AsuhanGiziDewasaViewModel>(item);
                            model.AsuhanGiziDewasa.SectionID = sectionid;
                            model.AsuhanGiziDewasa.NRM = model.NRM;
                            model.AsuhanGiziDewasa.Tanggal = DateTime.Today;
                            model.AsuhanGiziDewasa.Jam = DateTime.Now;
                            model.AsuhanGiziDewasa.NoReg = noreg;
                            model.AsuhanGiziDewasa.Report = 0;

                            model.AsuhanGiziDewasa.Ruangan = sectionnama;


                        }

                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string AsuhanGiziDewasa_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == item.AsuhanGiziDewasa.NoReg);
                            item.AsuhanGiziDewasa.NoReg = item.AsuhanGiziDewasa.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.AsuhanGiziDewasa.SectionID = sectionid;
                            item.AsuhanGiziDewasa.NRM = item.AsuhanGiziDewasa.NRM;
                            var model = eEMR.AsuhanGiziDewasa.FirstOrDefault(x => x.SectionID == item.AsuhanGiziDewasa.SectionID && x.NoReg == item.AsuhanGiziDewasa.NoReg);

                            if (model == null)
                            {
                                model = new AsuhanGiziDewasa();
                                var o = IConverter.Cast<AsuhanGiziDewasa>(item.AsuhanGiziDewasa);
                                eEMR.AsuhanGiziDewasa.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<AsuhanGiziDewasa>(item.AsuhanGiziDewasa);
                                eEMR.AsuhanGiziDewasa.AddOrUpdate(model);
                            }



                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

    }
       

}