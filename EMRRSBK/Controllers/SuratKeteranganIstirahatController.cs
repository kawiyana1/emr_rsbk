﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class SuratKeteranganIstirahatController : Controller
    {
        // GET: SuratKeteranganIstirahat
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.SuratKeteranganIstirahat.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.SKIstirahat = IConverter.Cast<SuratKeteranganIstirahatViewModel>(item);
                            var ahligizi = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.KodeDokter);
                            if (ahligizi != null)
                            {
                                model.SKIstirahat.KodeDokterNama = ahligizi.NamaDOkter;
                            }
                            model.SKIstirahat.NoReg = noreg;
                            model.SKIstirahat.NRM = model.NRM;
                            model.SKIstirahat.SectionID = sectionid;
                            model.SKIstirahat.Report = 1;
                            model.SKIstirahat.NamaPasien = m.NamaPasien;
                            model.SKIstirahat.UmurThn = m.UmurThn;

                            var fingerskistirahat = eEMR.SuratKeteranganIstirahat.Where(x => x.KodeDokter == model.SKIstirahat.KodeDokter && x.TandaTanganDokter == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerskistirahat != null)
                            {
                                model.SKIstirahat.SudahRegDokter = 1;
                            }
                            else
                            {
                                model.SKIstirahat.SudahRegDokter = 0;
                            }
                            if (model.SKIstirahat.KodeDokter != null)
                            {
                                ViewBag.UrlFingerSKIstirahatDokter = GetEncodeUrlSKIstirahat_Dokter(model.SKIstirahat.KodeDokter, model.SKIstirahat.NoReg, sectionid);
                            }
                        }
                        else
                        {
                            item = new SuratKeteranganIstirahat();
                            model.SKIstirahat = IConverter.Cast<SuratKeteranganIstirahatViewModel>(item);
                            ViewBag.UrlFingerSKIstirahatDokter = GetEncodeUrlSKIstirahat_Dokter(model.SKIstirahat.KodeDokter, model.SKIstirahat.NoReg, sectionid);
                            model.SKIstirahat.Report = 0;
                            model.SKIstirahat.NamaPasien = m.NamaPasien;
                            model.SKIstirahat.UmurThn = m.UmurThn;
                            model.SKIstirahat.NoReg = noreg;
                            model.SKIstirahat.NRM = model.NRM;
                            model.SKIstirahat.SectionID = sectionid;
                            model.SKIstirahat.Tanggal = DateTime.Today;
                            model.SKIstirahat.Jam = DateTime.Now;
                            model.SKIstirahat.TerhitungTgl = DateTime.Today;
                            model.SKIstirahat.SampaiTgl = DateTime.Today;
                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string SuratKeteranganIstirahat_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.SKIstirahat.NoReg = item.SKIstirahat.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.SKIstirahat.SectionID = sectionid;
                            item.SKIstirahat.NRM = item.SKIstirahat.NRM;
                            var model = eEMR.SuratKeteranganIstirahat.FirstOrDefault(x => x.SectionID == sectionid && x.NoReg == item.SKIstirahat.NoReg);

                            if (model == null)
                            {
                                model = new SuratKeteranganIstirahat();
                                var o = IConverter.Cast<SuratKeteranganIstirahat>(item.SKIstirahat);
                                eEMR.SuratKeteranganIstirahat.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<SuratKeteranganIstirahat>(item.SKIstirahat);
                                eEMR.SuratKeteranganIstirahat.AddOrUpdate(model);
                            }


                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFSuratKeteranganIstirahat(string noreg, string sectionid)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_SuratKeteranganIstirahat.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand("Rpt_SuratKeteranganIstirahat", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables["Rpt_SuratKeteranganIstirahat;1"].SetDataSource(ds[i].Tables[0]);
            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }

        public string GetEncodeUrlSKIstirahat_Dokter(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifSuratKeteranganIstirahat/get_keyskistirahat";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }
    }
}