﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Configuration;

namespace EMRRSBK.Controllers
{
    public class VerifCPPTController : Controller
    {
        // GET: VerifCPPT
        #region === CPPT
        
        [HttpGet]
        public string get_keycppt(string userid, string noreg, string section, int no)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCPPT/checkverifcppt";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section + "/" + no};";
        }

        [HttpGet]
        public string get_keycpptmenerimapasien(string userid, string noreg, string section, int no)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCPPT/checkverifcpptmenerimapasien";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section + "/" + no};";
        }

        [HttpGet]
        public string get_keycpptmenyerahkanpasien(string userid, string noreg, string section, int no)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCPPT/checkverifcpptmenyerahkanpasien";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section + "/" + no};";
        }

        [HttpGet]
        public string get_keycpptpetugasbacaulang(string userid, string noreg, string section, int no)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCPPT/checkverifcpptpetugasbacaulang";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section + "/" + no};";
        }

        [HttpGet]
        public string get_keycpptpetugaskonfirmasi(string userid, string noreg, string section, int no)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCPPT/checkverifcpptpetugaskonfirmasi";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section + "/" + no};";
        }

        public string checkverifcppt(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            int no = int.Parse(param[2]);
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                    if (dokter != null)
                    {
                        user_id = dokter.DokterID;
                    }

                    var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                    var f = s.mFinger.Where(x => x.userid == user_id);
                    foreach (var i in f.ToList())
                    {
                        var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                        if (salt.ToUpper() == vStamp.ToUpper())
                        {
                            #region === Verif 
                            var datascppt = s.SIMtrSOAP.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section && x.No == no);
                            if (datascppt != null)
                            {
                                if (dokter != null)
                                {
                                    datascppt.TandaTanganDPJP = i.Gambar1;
                                }
                                else
                                {
                                    datascppt.TandaTangan = i.Gambar1;
                                }
                                s.SaveChanges();
                            }
                            #endregion
                            return "empty";
                        };
                    }
                }

            }
            return "Gagal";
        }

        public string checkverifcpptmenerimapasien(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            int no = int.Parse(param[2]);
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                    if (dokter != null)
                    {
                        user_id = dokter.DokterID;
                    }

                    var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                    var f = s.mFinger.Where(x => x.userid == user_id);
                    foreach (var i in f.ToList())
                    {
                        var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                        if (salt.ToUpper() == vStamp.ToUpper())
                        {
                            #region === Verif 
                            var datascppt = s.SIMtrSOAP.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section && x.No == no);
                            if (datascppt != null)
                            {
                                if (dokter != null)
                                {
                                    datascppt.TandaTanganMenerimaPasien = i.Gambar1;
                                }
                                else
                                {
                                    datascppt.TandaTangan = i.Gambar1;
                                }
                                s.SaveChanges();
                            }
                            #endregion
                            return "empty";
                        };
                    }
                }

            }
            return "Gagal";
        }

        public string checkverifcpptmenyerahkanpasien(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            int no = int.Parse(param[2]);
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                    if (dokter != null)
                    {
                        user_id = dokter.DokterID;
                    }

                    var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                    var f = s.mFinger.Where(x => x.userid == user_id);
                    foreach (var i in f.ToList())
                    {
                        var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                        if (salt.ToUpper() == vStamp.ToUpper())
                        {
                            #region === Verif 
                            var datascppt = s.SIMtrSOAP.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section && x.No == no);
                            if (datascppt != null)
                            {
                                if (dokter != null)
                                {
                                    datascppt.TandaTanganMenyerahkanPasien = i.Gambar1;
                                }
                                else
                                {
                                    datascppt.TandaTangan = i.Gambar1;
                                }
                                s.SaveChanges();
                            }
                            #endregion
                            return "empty";
                        };
                    }
                }

            }
            return "Gagal";
        }

        public string checkverifcpptpetugasbacaulang(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            int no = int.Parse(param[2]);
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                    if (dokter != null)
                    {
                        user_id = dokter.DokterID;
                    }

                    var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                    var f = s.mFinger.Where(x => x.userid == user_id);
                    foreach (var i in f.ToList())
                    {
                        var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                        if (salt.ToUpper() == vStamp.ToUpper())
                        {
                            #region === Verif 
                            var datascppt = s.SIMtrSOAP.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section && x.No == no);
                            if (datascppt != null)
                            {
                                if (dokter != null)
                                {
                                    datascppt.TandaTanganPetugasBacaUlang = i.Gambar1;
                                }
                                else
                                {
                                    datascppt.TandaTangan = i.Gambar1;
                                }
                                s.SaveChanges();
                            }
                            #endregion
                            return "empty";
                        };
                    }
                }

            }
            return "Gagal";
        }

        public string checkverifcpptpetugaskonfirmasi(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            int no = int.Parse(param[2]);
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                    if (dokter != null)
                    {
                        user_id = dokter.DokterID;
                    }

                    var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                    var f = s.mFinger.Where(x => x.userid == user_id);
                    foreach (var i in f.ToList())
                    {
                        var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                        if (salt.ToUpper() == vStamp.ToUpper())
                        {
                            #region === Verif 
                            var datascppt = s.SIMtrSOAP.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section && x.No == no);
                            if (datascppt != null)
                            {
                                if (dokter != null)
                                {
                                    datascppt.TandaTanganPetugasKonfirmasi = i.Gambar1;
                                }
                                else
                                {
                                    datascppt.TandaTangan = i.Gambar1;
                                }
                                s.SaveChanges();
                            }
                            #endregion
                            return "empty";
                        };
                    }
                }

            }
            return "Gagal";
        }
        #endregion
    }
}