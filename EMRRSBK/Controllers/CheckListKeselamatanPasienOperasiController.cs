﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;


namespace EMRRSBK.Controllers
{
    public class CheckListKeselamatanPasienOperasiController : Controller
    {
        // GET: CheckListKeselamatanPasienOperasi

        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.ChecklistKeselamatanPasienOperasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.ChckKslmtnPasien = IConverter.Cast<CheckListKeselamatanPasienOperasiViewModel>(item);
                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Dokter);
                            if (dokter != null)
                            {
                                model.ChckKslmtnPasien.DokterNama = dokter.NamaDOkter;
                            }
                            var perawat = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Perawat);
                            if (perawat != null)
                            {
                                model.ChckKslmtnPasien.PerawatNama = perawat.NamaDOkter;
                            }
                            model.ChckKslmtnPasien.Report = 1;

                            var fingerdokter = eEMR.ChecklistKeselamatanPasienOperasi.Where(x => x.Dokter == model.ChckKslmtnPasien.Dokter && x.TandaTanganDokter == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerdokter != null)
                            {
                                model.ChckKslmtnPasien.SudahRegDokter = 1;
                            }
                            else
                            {
                                model.ChckKslmtnPasien.SudahRegDokter = 0;
                            }
                            if (model.ChckKslmtnPasien.Dokter != null)
                            {
                                ViewBag.UrlFingerChckKslmtnPasienDokter = GetEncodeUrlChckKslmtnPasien_Dokter(model.ChckKslmtnPasien.Dokter, model.ChckKslmtnPasien.NoReg, sectionid);
                            }

                            var fingerperawat = eEMR.ChecklistKeselamatanPasienOperasi.Where(x => x.Perawat == model.ChckKslmtnPasien.Perawat && x.TandaTanganPerawat == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerperawat != null)
                            {
                                model.ChckKslmtnPasien.SudahRegPerawat = 1;
                            }
                            else
                            {
                                model.ChckKslmtnPasien.SudahRegPerawat = 0;
                            }
                            if (model.ChckKslmtnPasien.Perawat != null)
                            {
                                ViewBag.UrlFingerChckKslmtnPasienPerawat = GetEncodeUrlChckKslmtnPasien_Perawat(model.ChckKslmtnPasien.Perawat, model.ChckKslmtnPasien.NoReg, sectionid);
                            }
                        }
                        else
                        {
                            item = new ChecklistKeselamatanPasienOperasi();
                            model.ChckKslmtnPasien = IConverter.Cast<CheckListKeselamatanPasienOperasiViewModel>(item);
                            ViewBag.UrlFingerChckKslmtnPasienDokter = GetEncodeUrlChckKslmtnPasien_Dokter(model.ChckKslmtnPasien.Dokter, model.ChckKslmtnPasien.NoReg, sectionid);
                            ViewBag.UrlFingerChckKslmtnPasienPerawat = GetEncodeUrlChckKslmtnPasien_Perawat(model.ChckKslmtnPasien.Perawat, model.ChckKslmtnPasien.NoReg, sectionid);
                            model.ChckKslmtnPasien.NoReg = noreg;
                            model.ChckKslmtnPasien.Report = 0;
                            model.ChckKslmtnPasien.NRM = model.NRM;
                            model.ChckKslmtnPasien.SectionID = sectionid;
                            model.ChckKslmtnPasien.Tanggal = DateTime.Today;
                            model.ChckKslmtnPasien.Tgl_SignIn = DateTime.Today;
                            model.ChckKslmtnPasien.Tgl_SignOut = DateTime.Today;
                            model.ChckKslmtnPasien.Tgl_TimeOut = DateTime.Today;
                            model.ChckKslmtnPasien.Jam_SignIn = DateTime.Now;
                            model.ChckKslmtnPasien.Jam_SignOut = DateTime.Now;
                            model.ChckKslmtnPasien.Jam_TimeOut = DateTime.Now;
                           
                        }

                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string CheckListKeselamatanPasienOperasi_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == item.ChckKslmtnPasien.NoReg);
                            item.ChckKslmtnPasien.NoReg = item.ChckKslmtnPasien.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.ChckKslmtnPasien.SectionID = sectionid;
                            item.ChckKslmtnPasien.NRM = item.ChckKslmtnPasien.NRM;
                            var model = eEMR.ChecklistKeselamatanPasienOperasi.FirstOrDefault(x => x.SectionID == item.ChckKslmtnPasien.SectionID && x.NoReg == item.ChckKslmtnPasien.NoReg);

                            if (model == null)
                            {
                                model = new ChecklistKeselamatanPasienOperasi();
                                var o = IConverter.Cast<ChecklistKeselamatanPasienOperasi>(item.ChckKslmtnPasien);
                                eEMR.ChecklistKeselamatanPasienOperasi.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<ChecklistKeselamatanPasienOperasi>(item.ChckKslmtnPasien);
                                eEMR.ChecklistKeselamatanPasienOperasi.AddOrUpdate(model);
                            }



                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFCheckListKeselamatanPasienOperasi(string noreg, string sectionid)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_ChecklistKeselamatanPasienOperasi.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"Rpt_ChecklistKeselamatanPasienOperasi", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_ChecklistKeselamatanPasienOperasi;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }

        public string GetEncodeUrlChckKslmtnPasien_Dokter(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifKeselamatanPasienOperasi/get_keychckliskslmntdokter";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlChckKslmtnPasien_Perawat(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifKeselamatanPasienOperasi/get_keychckliskslmntperawat";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }
    }
}