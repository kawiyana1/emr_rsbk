﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class PersetujuanUmumNewController : Controller
    {
        // GET: PersetujuanUmumNew
        public ActionResult Index(string noreg, string sectionid, string persetujuan)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var m = eSIM.VW_IdentitasPasien.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    model.NoReg = noreg;
                    model.SectionID = sectionid;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.PersetujuanUmumNew.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.PU = IConverter.Cast<PersetujuanUmumNewViewModel>(item);
                            model.PU.Persetujuan = persetujuan;
                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.PemberiInformasi);
                            if (dokter != null)
                            {
                                model.PU.PemberiInformasiNama = dokter.NamaDOkter;
                            }

                        }
                        else
                        {
                            item = new PersetujuanUmumNew();
                            model.PU = IConverter.Cast<PersetujuanUmumNewViewModel>(item);
                            //ViewBag.UrlFingerPersetujuanUmumNewDokterPemeriksa = GetEncodeUrlPenolakanRI(model.PersetujuanUmumNew.Dokter, model.PersetujuanUmumNew.NoReg, sectionid);
                            model.PU.NRM = m.NRM;
                            model.PU.Persetujuan = persetujuan;
                            //model.PU.Nama = m.NamaPasien;
                            //model.PU.Tempat = m.TempatLahir;
                            //model.PU.TanggalLahir = m.TglLahir.ToString();
                            //model.PU.JenisKelamin = m.JenisKelamin;
                            //model.PU.Agama = m.Agama;
                            //model.PU.Alamat = m.Alamat;
                            //model.PU.Kabupaten = m.Kabupaten;
                            //model.PU.Provensi = m.Provinsi;
                            //model.PU.Kewarganegaraan = m.Nationality;
                            //model.PU.Pendidikan = m.Pendidikan;
                            //model.PU.Pekerjaan = m.Pekerjaan;
                            //model.PU.PangkatKesatuan = m.Pangkat;
                            //model.PU.StatusKawin = m.StatusPerkawinan;
                            //model.PU.Nama = m.PenanggungHubungan;
                            //model.PU.TempatPenganggungJawab = m.PenanggungTempatLahir;
                            //model.PU.AlamatLahirPenganggungJawab = m.PenanggungAlamat;
                            //model.PU.KabupatenLahirPenganggungJawab = m.PenanggungKabupaten;
                            //model.PU.NoTelpLahirPenganggungJawab = m.PenanggungPhone;
                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string PersetujuanUmumNew_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.PU.NoReg = noreg;
                            item.PU.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                            item.PU.NRM = m.NRM;
                            item.PU.Persetujuan = m.NRM;
                            var model = eEMR.PersetujuanUmumNew.FirstOrDefault(x => x.SectionID == item.PU.SectionID && x.NoReg == item.NoReg);

                            if (model == null)
                            {
                                model = new PersetujuanUmumNew();
                                var o = IConverter.Cast<PersetujuanUmumNew>(item.PU);
                                eEMR.PersetujuanUmumNew.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<PersetujuanUmumNew>(item.PU);
                                eEMR.PersetujuanUmumNew.AddOrUpdate(model);
                            }

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFPersetujuanUmumNew(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["SectionNamaPelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"PersetujuanUmumNew.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"PersetujuanUmumNew", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"PersetujuanUmumNew;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}