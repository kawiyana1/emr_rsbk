﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using System;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;


namespace EMRRSBK.Controllers
{
    public class OrientasiPasienBaruRawatInapController : Controller
    {
        // GET: OrientasiPasienBaruRawatInap
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var sectionnama = Request.Cookies["SectionNamaPelayanan"].Value;
                    using (var eEMR = new EMR_Entities())
                    {
                        var n = eSIM.VW_DataPasienReg.Where(x => x.NRM == m.NRM && x.SectionID == sectionid).FirstOrDefault();
                        var item = eEMR.OrientasiPasienBaruRawatInap.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.OrentasiPsnBaruRawatInap = IConverter.Cast<OrientasiPasienBaruRawatInapViewModel>(item);
                            var pemberi = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.KodePemberiOrientasi);
                            if (pemberi != null)
                            {
                                model.OrentasiPsnBaruRawatInap.KodePemberiOrientasiNama = pemberi.NamaDOkter;
                            }

                            var petugas = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Petugas);
                            if (petugas != null)
                            {
                                model.OrentasiPsnBaruRawatInap.PetugasNama = petugas.NamaDOkter;
                            }

                            model.OrentasiPsnBaruRawatInap.NoReg = m.NoReg;
                            model.OrentasiPsnBaruRawatInap.NRM = m.NRM;
                            model.OrentasiPsnBaruRawatInap.Tanggal = DateTime.Today;
                            model.OrentasiPsnBaruRawatInap.Jam = DateTime.Now;
                            model.OrentasiPsnBaruRawatInap.Nama = m.NamaPasien;
                            model.OrentasiPsnBaruRawatInap.TglLahir = m.TglLahir;
                            model.OrentasiPsnBaruRawatInap.JenisKelamin = (m.JenisKelamin == "M" ? "Laki-Laki" : "Perempuan");
                            model.OrentasiPsnBaruRawatInap.Report = 1;
                            model.OrentasiPsnBaruRawatInap.Kamar = n.Kamar;
                            model.OrentasiPsnBaruRawatInap.Ruangan = sectionnama;
                        }
                        else
                        {
                            item = new OrientasiPasienBaruRawatInap();
                            model.OrentasiPsnBaruRawatInap = IConverter.Cast<OrientasiPasienBaruRawatInapViewModel>(item);
                            model.OrentasiPsnBaruRawatInap.NoReg = m.NoReg;
                            model.OrentasiPsnBaruRawatInap.NRM = m.NRM;
                            model.OrentasiPsnBaruRawatInap.Tanggal = DateTime.Today;
                            model.OrentasiPsnBaruRawatInap.Jam = DateTime.Now;
                            model.OrentasiPsnBaruRawatInap.Nama = m.NamaPasien;
                            model.OrentasiPsnBaruRawatInap.TglLahir = m.TglLahir;
                            model.OrentasiPsnBaruRawatInap.Kamar = n.Kamar;
                            model.OrentasiPsnBaruRawatInap.Ruangan = sectionnama;
                            model.OrentasiPsnBaruRawatInap.JenisKelamin = (m.JenisKelamin == "M" ? "Laki-Laki" : "Perempuan");
                            model.OrentasiPsnBaruRawatInap.Report = 0;
                            model.OrentasiPsnBaruRawatInap.PerawatDPJP = "Ya";
                            model.OrentasiPsnBaruRawatInap.JamVisite = "Ya";
                            model.OrentasiPsnBaruRawatInap.WaktuBerkunjung = "Ya";
                            model.OrentasiPsnBaruRawatInap.LayananObat = "Ya";
                            model.OrentasiPsnBaruRawatInap.JadwalObat = "Ya";
                            model.OrentasiPsnBaruRawatInap.AdministrasiPembayaran = "Ya";
                            model.OrentasiPsnBaruRawatInap.PenggunaanGelang = "Ya";
                            model.OrentasiPsnBaruRawatInap.KamarMandi = "Ya";
                            model.OrentasiPsnBaruRawatInap.KipasAngin = "Tidak";
                            model.OrentasiPsnBaruRawatInap.Wastafel = "Ya";
                            model.OrentasiPsnBaruRawatInap.Lemari = "Ya";
                            model.OrentasiPsnBaruRawatInap.AC = "Ya";
                            model.OrentasiPsnBaruRawatInap.Television = "Ya";
                            model.OrentasiPsnBaruRawatInap.Telepon = "Ya";
                            model.OrentasiPsnBaruRawatInap.TempatSampah = "Ya";
                            model.OrentasiPsnBaruRawatInap.KotakSaran = "Ya";
                            model.OrentasiPsnBaruRawatInap.TataTertib = "Ya";
                            model.OrentasiPsnBaruRawatInap.LayananPengaduan = "Ya";
                            model.OrentasiPsnBaruRawatInap.KeadaanDarurat = "Ya";
                            model.OrentasiPsnBaruRawatInap.LaranganMerokok = "Ya";
                            model.OrentasiPsnBaruRawatInap.LainnyaKet_1 = "Ya";
                            model.OrentasiPsnBaruRawatInap.LainnyaPil_1 = "Ya";
                            model.OrentasiPsnBaruRawatInap.LainnyaKet_2 = "Ya";
                            model.OrentasiPsnBaruRawatInap.LainnyaPil_2 = "Ya";
                            model.OrentasiPsnBaruRawatInap.LainnyaKet_3 = "Ya";
                            model.OrentasiPsnBaruRawatInap.LainnyaPil_3 = "Ya";
                            model.OrentasiPsnBaruRawatInap.LainnyaKet_4 = "Ya";
                            model.OrentasiPsnBaruRawatInap.LainnyaPil_4 = "Ya";
                            model.OrentasiPsnBaruRawatInap.LainnyaKet_5 = "Ya";
                            model.OrentasiPsnBaruRawatInap.LainnyaPil_5 = "Ya";
                            model.OrentasiPsnBaruRawatInap.LainnyaKet_6 = "Ya";
                            model.OrentasiPsnBaruRawatInap.LainnyaPil_6 = "Ya";
                            model.OrentasiPsnBaruRawatInap.LainnyaKet_7 = "Ya";
                            model.OrentasiPsnBaruRawatInap.LainnyaPil_7 = "Ya";
                            model.OrentasiPsnBaruRawatInap.LainnyaKet_8 = "Ya";
                            model.OrentasiPsnBaruRawatInap.LainnyaPil_8 = "Ya";
                        }
                    }
                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string OrientasiPasienBaruRawatInap_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == item.OrentasiPsnBaruRawatInap.NoReg);
                            item.OrentasiPsnBaruRawatInap.NoReg = item.OrentasiPsnBaruRawatInap.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.OrentasiPsnBaruRawatInap.SectionID = sectionid;
                            item.OrentasiPsnBaruRawatInap.NRM = item.OrentasiPsnBaruRawatInap.NRM;
                            var model = eEMR.OrientasiPasienBaruRawatInap.FirstOrDefault(x => x.SectionID == item.OrentasiPsnBaruRawatInap.SectionID && x.NoReg == item.OrentasiPsnBaruRawatInap.NoReg);

                            if (model == null)
                            {
                                model = new OrientasiPasienBaruRawatInap();
                                var o = IConverter.Cast<OrientasiPasienBaruRawatInap>(item.OrentasiPsnBaruRawatInap);
                                eEMR.OrientasiPasienBaruRawatInap.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<OrientasiPasienBaruRawatInap>(item.OrentasiPsnBaruRawatInap);
                                eEMR.OrientasiPasienBaruRawatInap.AddOrUpdate(model);

                            }



                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}