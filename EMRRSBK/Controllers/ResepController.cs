﻿using CrystalDecisions.CrystalReports.Engine;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Text;
using System.Net.Sockets;

namespace EMRRSBK.Controllers
{
    //[Authorize(Roles = "Pelayanan")]
    public class ResepController : Controller
    {

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string noreg, string section, string nrm,string poliklinik =null, string thiss=null )
        {
            var nomor = 0;
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.Noreg = noreg;
            ViewBag.Section = section;
            ViewBag.Nomor = nomor;
            ViewBag.Poliklinik = poliklinik;
            ViewBag.Thiss = thiss;
            ViewBag.NRM = nrm;
            using (var s = new SIM_Entities())
            {
                var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                var dokter = s.VW_DataPasienReg.Where(x => x.NRM == nrm && x.SectionID == sectionid && x.NoReg == noreg).FirstOrDefault();
                ViewBag.DokterID = dokter.DokterID;
                ViewBag.NamaDokter = dokter.NamaDOkter;
                var alergi = s.SIMtrRiwayatAlergi.Where(x => x.NRM == nrm).FirstOrDefault();
                if (alergi != null) { 
                    ViewBag.Alergi = "1";
                }
            }
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpGet]
        [ActionName("CreateCopy")]
        public ActionResult CreateCopy(string copy, string dokterid, string dokternama, string noreg, string nrm, string thiss = null, string poliklinik = null)
        {
            var nomor = 0;
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.Noreg = noreg;
            ViewBag.Section = Request.Cookies["SectionIDPelayanan"].Value;
            ViewBag.Nomor = nomor;
            ViewBag.Poliklinik = poliklinik;
            ViewBag.Thiss = thiss;
            ViewBag.NRM = nrm;
            ViewBag.DokterID = dokterid;
            ViewBag.NamaDokter = dokternama;
            var model = new ResepViewModel();
            using (var s = new SIM_Entities())
            {
                var dokumen = s.SIMtrResep.OrderBy(x => x.NoRegistrasi).FirstOrDefault(x => x.NoResep == copy);
                if (dokumen != null)
                {
                    model = IConverter.Cast<ResepViewModel>(dokumen);
                    model.NoResep = copy;

                    var dpjp = s.mDokter.FirstOrDefault(e => e.DokterID == dokterid);
                    if (dpjp != null) {
                        ViewBag.DokterID = dpjp.DokterID;
                        ViewBag.NamaDokter = dpjp.NamaDOkter;
                    }

                    #region === Detail 
                    model.Detail_List = new ListDetail<ResepDetailViewModel>();
                    var detail_1 = s.SIMtrResepDetail.Where(x => x.NoResep == dokumen.NoResep).ToList();
                    foreach (var x in detail_1)
                    {
                        var y = IConverter.Cast<ResepDetailViewModel>(x);
                        y.Kode_Barang = s.mBarang.FirstOrDefault(v => v.Barang_ID == x.Barang_ID).Kode_Barang;
                        y.Nama_Barang = s.mBarang.FirstOrDefault(v => v.Barang_ID == x.Barang_ID).Nama_Barang;
                        model.Detail_List.Add(false, y);
                    }
                    #endregion
                }
                else
                {
                    model.NoResep = copy;
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new ResepViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        #region Validation
                        if (item.Detail_List == null) { throw new Exception("Detail tidak boleh kosong"); }

                        item.Detail_List.RemoveAll(x => x.Remove);
                        decimal jumlah = 0;
                        foreach (var x in item.Detail_List)
                        {
                            //var b2 = s.mBarang.FirstOrDefault(y => y.Barang_ID == x.Model.Barang_ID);
                            var b2 = s.Pelayanan_GetListObat(item.Farmasi_SectionID, item.NoRegistrasi).FirstOrDefault(y => y.Barang_ID == x.Model.Barang_ID);
                            //qty * harga
                            x.Model.Harga = b2.Harga_Jual ?? 0;
                            var _harga = b2.Harga_Jual ?? 0;
                            var qty = (decimal)x.Model.Qty;
                            var _subJumlah = qty * _harga;
                            jumlah += _subJumlah;
                        }
                        var datareg = s.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == item.NoRegistrasi);
                        #endregion
                        
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            try
                            {
                                var id = s.SIMtrResep_AutoID().FirstOrDefault();
                                var model = new SIMtrResep()
                                {
                                    NoResep = id,
                                    NoRegistrasi = item.NoRegistrasi,
                                    SectionID = sectionid,
                                    Tanggal = DateTime.Today,
                                    Jam = DateTime.Now,
                                    DokterID = item.DokterID,
                                    Jumlah = jumlah,
                                    KomisiDokter = 0,
                                    Cyto = item.Cyto,
                                    NoBukti = null,
                                    Farmasi_SectionID = item.Farmasi_SectionID,
                                    User_ID = 490,
                                    JenisKerjasamaID = datareg.JenisKerjasamaID.ToString(),
                                    CompanyID = datareg.CustomerKerjasamaID.ToString(),
                                    NRM = datareg.NRM,
                                    NoKartu = datareg.NoKartu,
                                    KelasID = datareg.KdKelas,
                                    KTP = datareg.PasienKTP,
                                    KerjasamaID = datareg.JenisKerjasamaID.ToString(),
                                    //Realisasi = "",
                                    //PerusahaanID = "",
                                    RawatInap = datareg.RawatInap ,
                                    //Batal = "",
                                    Paket = !string.IsNullOrEmpty(item.KodePaket),
                                    KodePaket = item.KodePaket,
                                    AmprahanRutin = false,
                                    IncludeJasa = false,
                                    //NoAntri = auto,
                                    UserNameInput = "",
                                    Keterangan = item.Keterangan,
                                    Puyer = item.ObatRacik,
                                    QtyPuyer = (int?)item.QtyRacik,
                                    SatuanPuyer = item.SatuanPuyer,
                                    CustomerKerjasamaID = (int?)datareg.CustomerKerjasamaID,
                                    RasioObat_ada = false,
                                    Rasio_KelebihanDibayarPasien = false,
                                    RasioUmum_Alert = false,
                                    RasioUmum_Blok = false,
                                    RasioSpesialis_Alert = false,
                                    RasioSPesialis_Blok = false,
                                    RasioSub_Alert = false,
                                    RasioSub_Blok = false,
                                    RasioUmum_nilai = 0,
                                    RasioSpesialis_Nilai = 0,
                                    RasioSub_Nilai = 0,
                                    BeratBadan= item.BeratBadan,
                                    //SedangProses = null,
                                    //SedangProsesPC = null,
                                    //AlasanBatal = null,
                                    //UserID_Batal = null,
                                    //ObatPulang = false,
                                    //Pending = false,
                                    //AlasanPending= null,
                                    //UserID_Pending = null,
                                };
                                if (model.DokterID == "null") throw new Exception("Dokter tidak boleh kosong");
                                s.SIMtrResep.Add(model);

                                #region Detail
                                foreach (var x in item.Detail_List)
                                {
                                    var blokasi = s.mBarangLokasiNew.FirstOrDefault(y => y.Barang_ID == x.Model.Barang_ID);
                                    var d = new SIMtrResepDetail()
                                    {
                                        NoResep = id,
                                        Barang_ID = x.Model.Barang_ID,
                                        Satuan = x.Model.Satuan,
                                        Qty = x.Model.Qty,
                                        Harga_Satuan = x.Model.Harga,
                                        Disc_Persen = 0,
                                        Stok = blokasi.Qty_Stok,
                                        KomisiDokter = 0,
                                        THT = 0,
                                        Racik = x.Model.Racik,
                                        Plafon = 0,
                                        KelebihanPLafon = 0,
                                        KelasID = null,
                                        KTP = null,
                                        JenisKerjasamaID = null,
                                        PerusahaanID = null,
                                        DokterID = null,
                                        SectionID = null,
                                        PPN = 0,
                                        DosisID = 0,
                                        JenisBarangID = 0,
                                        Embalase = 0,
                                        JasaRacik = 0,
                                        Dosis = x.Model.Dosis,
                                        AturanPakai = x.Model.AturanPakai,
                                        KetRacik = x.Model.KetRacik,
                                        StatusRacikan = x.Model.StatusRacikan,
                                        QtyRacik = item.QtyRacik,
                                        JmlRacikan = x.Model.JmlRacikan,
                                        QtyHasilRacik = 0,
                                        TermasukPaket = false,
                                        //NomorUrut = 0
                                    };
                                    s.SIMtrResepDetail.Add(d);
                                }
                                #endregion
                                //var simtrreg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.NoRegistrasi);
                                //simtrreg.StatusResep = "MASUK";

                                result = new ResultSS(s.SaveChanges());
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Resep Create {id}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                                result.Data = (new {id=id,farmasi=model.Farmasi_SectionID, satuanpuyer=model.SatuanPuyer, keterengan=model.Keterangan, thisdetail=item.Detail_List});
                            }
                            catch (DbEntityValidationException ex) { dbContextTransaction.Rollback(); throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex)); }
                            catch (SqlException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                            catch (Exception ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                        }
                    }
                    return JsonHelper.JsonMsgCreate(result,result.Data,-1);
                    //return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E T A I L

        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(string nobukti)
        {
            ResepViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.Pelayanan_SIMtrResep.FirstOrDefault(x => x.NoResep == nobukti);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<ResepViewModel>(m);
                    item.KodePaket = m.KodePaket;
                    //item.NamaPaket = m.paket
                    item.QtyRacik = m.QtyPuyer;
                    item.ObatRacik = m.Puyer ?? false;
                    item.SatuanPuyer = m.SatuanPuyer;
                    item.Jam_View = m.Jam.ToString("HH:mm");
                    item.Tanggal_View = m.Tanggal.ToString("dd/MM/yyyy");
                    item.Jumlah_View = m.Jumlah.ToMoney();
                    var d = s.Pelayanan_SIMtrResepDetail.OrderBy(x => x.Nama_Barang).Where(x => x.NoResep == nobukti).ToList();
                    item.Detail_List = new ListDetail<ResepDetailViewModel>();
                    foreach (var x in d)
                    {
                        var n = IConverter.Cast<ResepDetailViewModel>(x);
                        n.Jumlah_View = ((decimal)(x.Jumlah ?? 0)).ToMoney();
                        n.Harga_View = x.Harga.ToMoney();
                        item.Detail_List.Add(false, n);
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }
        #endregion

        #region ===== D E L E T E

        [HttpPost]
        public string Delete(string nobukti)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMtrResep.FirstOrDefault(x => x.NoResep == nobukti && x.Realisasi == false);
                    if (m == null) throw new Exception("Data Tidak ditemukan");
                    using (var dbContextTransaction = s.Database.BeginTransaction())
                    {
                        try
                        {
                            s.SIMtrResepDetail.RemoveRange(s.SIMtrResepDetail.Where(x => x.NoResep == nobukti));
                            s.SIMtrResep.Remove(m);
                            result = new ResultSS(s.SaveChanges());

                            var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                            {
                                Activity = $"Resep delete {nobukti}"
                            };
                            UserActivity.InsertUserActivity(userActivity);
                            dbContextTransaction.Commit();
                        }
                        catch (DbEntityValidationException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                        catch (SqlException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                        catch (Exception ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                    }
                }
                return JsonHelper.JsonMsgDelete(result, -1);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    IQueryable<Pelayanan_ViewResep> proses = s.Pelayanan_ViewResep;
                    proses = proses.Where($"{nameof(Pelayanan_SIMtrResep.NoRegistrasi)}=@0", filter[0]);
                    proses = proses.Where($"{nameof(Pelayanan_SIMtrResep.SectionID)}=@0", sectionid);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<ResepViewModel>(x));
                    m.ForEach(x => x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy"));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== O T H E R

        [HttpPost]
        public string ListBarang(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var noreg = filter[10];
                    var sectionTujuan = filter[8];
                    IQueryable<VW_Pelayanan_GetListObat> proses = s.VW_Pelayanan_GetListObat.Where(x => x.SectionID == sectionTujuan);
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(VW_Pelayanan_GetListObat.Kode_Barang)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(VW_Pelayanan_GetListObat.Nama_Barang)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(VW_Pelayanan_GetListObat.Satuan_Stok)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(VW_Pelayanan_GetListObat.Qty_Stok)}=@0", IFilter.F_Decimal(filter[3]));
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(VW_Pelayanan_GetListObat.Nama_Sub_Kategori)}.Contains(@0)", filter[4]);
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(VW_Pelayanan_GetListObat.Nama_Kategori)}.Contains(@0)", filter[5]);
                    if (!string.IsNullOrEmpty(filter[6])) proses = proses.Where($"{nameof(VW_Pelayanan_GetListObat.Nama_Kategori)}.Contains(@0)", filter[6].ToUpper() == "Y");
                    proses = proses.Take(25);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<BarangBHPViewModel>(x));
                    foreach (var x in m)
                    {
                        //var harga_jual = s.GetHarga_Barang_Pelayanan(x.Barang_ID, sectionTujuan, "HargaJual", noreg).FirstOrDefault();
                        //if (harga_jual != null)
                        //{
                        //    x.Harga_Jual_View = (harga_jual ?? 0).ToMoney();
                        //    x.Harga_Jual = (decimal)harga_jual;
                        //}
                        //else
                        //{
                        //    x.Harga_Jual_View = "0";
                        //    x.Harga_Jual = (decimal)0;
                        //}

                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E - P A K E T - B H P

        [HttpPost]
        public string ListPaket(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Pelayanan_Data_PaketObat_Result> proses = s.Pelayanan_Data_PaketObat(filter[3], filter[2]).AsQueryable();
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PaketResepViewModel>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string DetailPaket(string kode, string section, string noreg, int nomor)
        {
            try
            {
                using (var s = new SIM_Entities())
                {
                    var h = s.Pelayanan_PaketObat.FirstOrDefault(x => x.KodePaket == kode);
                    var d = s.Pelayanan_PaketObatDetail.Where(x => x.KodePaket == kode).ToList();
                    var simtrreg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noreg);
                    foreach (var x in d)
                    {
                        var y = s.GetHargaObatNew_WithStok(simtrreg.JenisKerjasamaID, "XX", (simtrreg.PasienKTP ? 1 : 0), x.Barang_ID, (int?)simtrreg.CustomerKerjasamaID, section, 0).FirstOrDefault();
                        x.Harga = y.TglHargaBaru == DateTime.Today ? y.Harga_Baru : y.Harga_Lama;
                    }
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Header = h,
                        Data = d.ConvertAll(x => new
                        {
                            Kode = x.KodePaket,
                            NamaPaket = x.NamaPaket,
                            Kode_Barang = x.Kode_Barang,
                            Barang_ID = x.Barang_ID,
                            Nama_Barang = x.Nama_Barang,
                            AturanPakai = x.AturanPakai,
                            Satuan_Stok = x.Nama_Satuan,
                            Qty = x.Qty,
                            Dosis = x.Dosis,
                            Harga_Jual = x.Harga ?? 0,
                            Harga_Jual_View = (x.Harga ?? 0).ToMoney(),
                            Jumlah_View = ((x.Harga ?? 0) * (decimal)(x.Qty)).ToMoney(),
                            Qty_Stok = x.QtyStok ?? 0
                        })
                    });
                }
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string LastResep(string kode, string section, string noreg, int nomor, string nrm)
        {
            try
            {
                using (var s = new SIM_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var h = s.EMR_GetLastResep(sectionid, nrm).FirstOrDefault();
                    if (h == null) { throw new Exception("Belum pernah membuat resep"); }
                    var d = s.EMR_GetLastResepDetail(h.NoResep).ToList();
                    //var simtrreg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noreg);
                    //foreach (var x in d)
                    //{
                    //    var y = s.GetHargaObatNew_WithStok(simtrreg.JenisKerjasamaID, "XX", (simtrreg.PasienKTP ? 1 : 0), x.Barang_ID, (int?)simtrreg.CustomerKerjasamaID, section, 0).FirstOrDefault();
                    //    x.Harga_Satuan = y.TglHargaBaru == DateTime.Today ? y.Harga_Baru : y.Harga_Lama;
                    //}
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Header = h,
                        Data = d.ConvertAll(x => new
                        {
                            //Kode = x.KodePaket,
                            //NamaPaket = x.NamaPaket,
                            Kode_Barang = x.Kode_Barang,
                            Barang_ID = x.Barang_ID,
                            Nama_Barang = x.Nama_Barang,
                            AturanPakai = x.AturanPakai,
                            Satuan_Stok = x.Satuan,
                            Qty = x.Qty,
                            Dosis = x.Dosis,
                            KetRacik = x.KetRacik,
                            StatusRacikan = x.StatusRacikan,
                            JmlRacikan = x.JmlRacikan,
                            //Harga_Satuan = x.Harga_Satuan ?? 0,
                            //Harga_Jual_View = (x.Harga_Satuan ?? 0).ToMoney(),
                            //Jumlah_View = ((x.Harga ?? 0) * (decimal)(x.Qty)).ToMoney(),
                            //Qty_Stok = x.Qty ?? 0
                        })
                    });
                }
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== GET LIST OBAT, ATURAN, REPORT 
        [HttpGet]
        public string getListObat(string noreg)
        {
            try
            {
                var list_obat = "";
                using (var s = new SIM_Entities())
                {
                    var m_list = s.SIMtrResep.Where(x => x.NoRegistrasi == noreg).ToList();

                    foreach (var y in m_list)
                    {
                        var x_list = s.SIMtrResepDetail.Where(xx => xx.NoResep == y.NoResep).ToList();

                        foreach (var x in x_list)
                        {
                            var b = s.mBarang.Where(ss => ss.Barang_ID == x.Barang_ID).FirstOrDefault();

                            list_obat += b.Nama_Barang + " ( " + x.Qty + " ) " + x.Dosis + " \n";
                        }
                    }

                }

                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = list_obat,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string getAturan()
        {
            try
            {
                var model = new List<ResepDetailViewModel>() ;
                model.Add(new ResepDetailViewModel { AturanPakai = "3 x 1" });
                model.Add(new ResepDetailViewModel { AturanPakai = "2 x 1" });
                model.Add(new ResepDetailViewModel { AturanPakai = "1 x 1" });

                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = model,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ExportPDF_EResep(string id, string sectionfarmasi)
        {
            string message;
            message = $"Print_Resep {id}";
            IPAddress ipAddress;
            int port;
            HttpRequestBase h = null;
            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var ip = eSIM.mPrintAutoResepEMR.FirstOrDefault(x => x.SectionIDFarmasi == sectionfarmasi);
                    if (ip != null)
                    {
                        ipAddress = IPAddress.Parse(ip.IP);
                        port = int.Parse(ip.Port);
                    }
                    else
                    {
                        return JsonHelper.JsonMsg("IP printer tidak ditemukan", false, 0);
                    }


                }
            }
            catch
            {
                //Response.Cookies["IPSep"].Value = "127.0.0.1";
                //Response.Cookies["IPSep"].Expires = DateTime.Now.AddYears(100);
                //Response.Cookies["PortSep"].Value = "3024";
                //Response.Cookies["PortSep"].Expires = DateTime.Now.AddYears(100);
                //ipAddress = IPAddress.Parse("127.0.0.1");
                //port = 3024;
                return JsonHelper.JsonMsg("Terjadi Kesalahan pada proses, silahkan hubungi vendor", false, 0);
            }

            var result = ProsesPrint_Eresep(message, ipAddress, port, h);
            return JsonHelper.JsonMsg(result, result == "success");

        }
        #endregion

        #region ==== P R O S E S  P R I N T
        private string ProsesPrint_Eresep(string message, IPAddress ipAddress, int port, HttpRequestBase h = null)
        {
            try
            {
                byte[] bytes = new byte[1024];
                try
                {
                    IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
                    IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);
                    Socket sender = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                    try
                    {
                        IAsyncResult iaresult = sender.BeginConnect(remoteEP, null, null);
                        bool success = iaresult.AsyncWaitHandle.WaitOne(5000, true);
                        if (sender.Connected)
                        {
                            sender.EndConnect(iaresult);
                        }
                        else
                        {
                            sender.Close();
                            throw new ApplicationException("Failed to connect server.");
                        }

                        byte[] msg = Encoding.ASCII.GetBytes(message);
                        int bytesSent = sender.Send(msg);
                        int bytesRec = sender.Receive(bytes);
                        string result = Encoding.ASCII.GetString(bytes, 0, bytesRec);

                        sender.Shutdown(SocketShutdown.Both);
                        sender.Close();
                        return "success";
                    }
                    catch (ArgumentNullException ane)
                    {
                        return $"ArgumentNullException : {ane.Message}";
                    }
                    catch (SocketException se)
                    {
                        return $"SocketException : {se.Message}";
                    }
                    catch (Exception e)
                    {
                        return $"Unexpected exception : {e.Message}";
                    }

                }
                catch (Exception e)
                {
                    return $"Unexpected exception : {e.Message}";
                }
            }
            catch (Exception ex)
            {
                return $"Error : {ex.Message}";
            }
        }
        #endregion
    }
}