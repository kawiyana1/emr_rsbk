﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class PengkajianKeperawatanController : Controller
    {
        // GET: PengkajianKeperawatan
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.PengkajianKeperawatan.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.PnkjKeperwt = IConverter.Cast<PengkajianKeperawatanViewModel>(item);
                            var ahligizi = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Perawat);
                            if (ahligizi != null)
                            {
                                model.PnkjKeperwt.PerawatNama = ahligizi.NamaDOkter;
                            }
                            model.PnkjKeperwt.NoReg = noreg;
                            model.PnkjKeperwt.NRM = model.NRM;
                            model.PnkjKeperwt.SectionID = sectionid;
                            model.PnkjKeperwt.Report = 1;

                            var fingerpngjnkprwtn = eEMR.PengkajianKeperawatan.Where(x => x.Perawat == model.PnkjKeperwt.Perawat && x.TandaTanganPerawat == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerpngjnkprwtn != null)
                            {
                                model.PnkjKeperwt.SudahRegPerawat = 1;
                            }
                            else
                            {
                                model.PnkjKeperwt.SudahRegPerawat = 0;
                            }
                            if (model.PnkjKeperwt.Perawat != null)
                            {
                                ViewBag.UrlFingerPnkjKeperwtPerawat = GetEncodeUrlPnkjKeperwt_Perawat(model.PnkjKeperwt.Perawat, model.PnkjKeperwt.NoReg, sectionid);
                            }
                        }
                        else
                        {
                            item = new PengkajianKeperawatan();
                            model.PnkjKeperwt = IConverter.Cast<PengkajianKeperawatanViewModel>(item);
                            ViewBag.UrlFingerPnkjKeperwtPerawat = GetEncodeUrlPnkjKeperwt_Perawat(model.PnkjKeperwt.Perawat, model.PnkjKeperwt.NoReg, sectionid);

                            model.PnkjKeperwt.Report = 0;
                            model.PnkjKeperwt.NoReg = noreg;
                            model.PnkjKeperwt.NRM = model.NRM;
                            model.PnkjKeperwt.SectionID = sectionid;
                            model.PnkjKeperwt.Tanggal = DateTime.Today;
                            model.PnkjKeperwt.Jam = DateTime.Now;
                            model.PnkjKeperwt.TglPengkajianKeperawatan = DateTime.Today;
                            model.PnkjKeperwt.ProsedurInvasif_LainLain_Tanggal = DateTime.Today;
                            model.PnkjKeperwt.ProsedurInvasif_Tracheostomy_Tanggal = DateTime.Today;
                            model.PnkjKeperwt.ProsedurInvasif_CystostomyChateter_Tanggal = DateTime.Today;
                            model.PnkjKeperwt.ProsedurInvasif_SelangNgt_Tanggal = DateTime.Today;
                            model.PnkjKeperwt.ProsedurInvasif_DowerChatoler_Tanggal = DateTime.Today;
                            model.PnkjKeperwt.ProsedurInvasif_CentralLine_Tanggal = DateTime.Today;
                            model.PnkjKeperwt.ProsedurInvasif_InfusIntravena_Tanggal = DateTime.Today;
                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string PengkajianKeperawatan_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.PnkjKeperwt.NoReg = item.PnkjKeperwt.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.PnkjKeperwt.SectionID = sectionid;
                            item.PnkjKeperwt.NRM = item.PnkjKeperwt.NRM;
                            var model = eEMR.PengkajianKeperawatan.FirstOrDefault(x => x.SectionID == sectionid && x.NoReg == item.PnkjKeperwt.NoReg);

                            if (model == null)
                            {
                                model = new PengkajianKeperawatan();
                                var o = IConverter.Cast<PengkajianKeperawatan>(item.PnkjKeperwt);
                                eEMR.PengkajianKeperawatan.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<PengkajianKeperawatan>(item.PnkjKeperwt);
                                eEMR.PengkajianKeperawatan.AddOrUpdate(model);
                            }


                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFPengkajianKeperawatan(string noreg, string sectionid)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_PengkajianKeperawatan.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand("Rpt_PengkajianKeperawatan", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables["Rpt_PengkajianKeperawatan;1"].SetDataSource(ds[i].Tables[0]);
            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }

        public string GetEncodeUrlPnkjKeperwt_Perawat(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifPengkajianKeperawatan/get_keypngkjnkprwtn";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }
    }
}