﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Configuration;

namespace EMRRSBK.Controllers
{
    public class VerifPemantauanNEWSController : Controller
    {
        // GET: VerifPemantauanNEWS
        #region  ==== CekVerif Pemantauan NEWS

        [HttpGet]
        public string get_keyNEWS(string userid, string noreg, string section, int no)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifPemantauanNEWS/checkverifNEWS";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section + "/" + no};";
        }

        public string checkverifNEWS(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            int no = int.Parse(param[2]);
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    if (section == "SEC002")
                    {
                        var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                        if (dokter != null)
                        {
                            user_id = dokter.DokterID;
                        }

                        var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                        var f = s.mFinger.Where(x => x.userid == user_id);
                        foreach (var i in f.ToList())
                        {
                            var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                            if (salt.ToUpper() == vStamp.ToUpper())
                            {
                                #region === Verif Detail 

                                var datasdetaiNEWS = s.PemantauanNEWS_Detail.Where(x => x.NoReg == noreg && x.SectionID == section && x.No == no);
                                foreach (var xx in datasdetaiNEWS.ToList())
                                    if (datasdetaiNEWS != null)
                                    {
                                        if (dokter != null)
                                        {
                                            xx.TandaTanganDokter = i.Gambar1;
                                        }
                                        else
                                        {
                                            xx.TandaTangan = i.Gambar1;
                                        }
                                        s.SaveChanges();
                                    }
                                #endregion
                                return "empty";
                            };
                        }
                    }

                }

            }
            return "Gagal";
        }
        #endregion
    }
}